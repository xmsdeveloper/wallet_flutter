// @dart=2.9
import 'package:wallet_flutter/services/entity/entity.dart';

import '_json_serialize.dart';

class TokenDetailData implements JsonSerialize {
  String name;
  String website;
  String description;
  String explorer;
  String type;
  String symbol;
  int decimals;
  String status;
  String id;

  TokenDetailData(
      {this.name,
      this.website,
      this.description,
      this.explorer,
      this.type,
      this.symbol,
      this.decimals,
      this.status,
      this.id});

  /// 当前token是否对应一个合约，若对应合约，视作代币，反之视作主币
  bool get isContract => (id != null && id.length > 0);

  TokenBlog blog() => TokenBlog()..fromJson(toJson());

  void fromJson(Map<String, dynamic> json) {
    name = json['name'];
    website = json['website'];
    description = json['description'];
    explorer = json['explorer'];
    type = json['type'];
    symbol = json['symbol'];
    decimals = json['decimals'];
    status = json['status'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['website'] = this.website;
    data['description'] = this.description;
    data['explorer'] = this.explorer;
    data['type'] = this.type;
    data['symbol'] = this.symbol;
    data['decimals'] = this.decimals;
    data['status'] = this.status;
    data['id'] = this.id;
    return data;
  }

  @override
  int get hashCode => 6;

  @override
  operator ==(dynamic other) => other is TokenDetailData && id == other.id;
}
