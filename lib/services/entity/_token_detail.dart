// @dart=2.9
import 'package:wallet_flutter/services/entity/entity.dart';

import '_json_serialize.dart';

class TokenDetail implements JsonSerialize {
  String protocol;
  String symbol;
  String address;
  String name;
  int decimals;
  String state;
  String logo;
  String email;
  String whitepaper;
  String website;
  String publishedOn;
  // Map<String, dynamic> overview;
  Map<String, dynamic> links;

  TokenDetail({
    this.symbol,
    this.address,
    this.name,
    this.decimals,
    this.state,
    this.logo,
    this.email,
    this.whitepaper,
    this.website,
    this.publishedOn,
    // this.overview,
    this.links,
    this.protocol,
  });

  /// 当前token是否对应一个合约，若对应合约，视作代币，反之视作主币
  bool get isContract => (address != null && address.length > 0);

  TokenBlog blog() => TokenBlog()..fromJson(toJson());

  void fromJson(Map<String, dynamic> json) {
    protocol = json['protocol'];
    symbol = json['symbol'];
    address = json['address'];
    name = json['name'];
    decimals = json['decimals'];
    logo = json['logo'];
    // overview = json['overview'] as Map;
    email = json['email'];
    whitepaper = json['whitepaper'];
    website = json['website'];
    state = json['state'];
    publishedOn = json['published_on'];
    links = json['links'] as Map;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['protocol'] = this.protocol;
    data['symbol'] = this.symbol;
    data['address'] = this.address;
    data['name'] = this.name;
    data['decimals'] = this.decimals;
    data['logo'] = this.logo;
    // data['overview'] = this.overview;
    data['email'] = this.email;
    data['whitepaper'] = this.whitepaper;
    data['website'] = this.website;
    data['state'] = this.state;
    data['published_on'] = this.publishedOn;
    data['links'] = this.links;
    return data;
  }

  @override
  int get hashCode => 6;

  @override
  operator ==(dynamic other) =>
      other is TokenDetail && address == other.address;
}
