// @dart=2.9
import 'package:hive/hive.dart';

part '_recipient_account_blog.g.dart';

@HiveType(typeId: 11)
class RecipientAccountBlog extends HiveObject {
  @HiveField(0)
  final String address;

  @HiveField(1)
  final String name;

  @HiveField(2)
  final String avatarPath;

  @override
  int get hashCode => 11;

  String get addressBlog =>
      address.replaceRange(10, address.length - 10, '...');

  @override
  operator ==(dynamic other) =>
      other is RecipientAccountBlog && address == other.address;

  RecipientAccountBlog(this.address, this.name, this.avatarPath);
}
