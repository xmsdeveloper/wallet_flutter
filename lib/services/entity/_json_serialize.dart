// @dart=2.9
mixin JsonSerialize {
  void fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson();
}
