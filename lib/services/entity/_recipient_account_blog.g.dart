// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of '_recipient_account_blog.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RecipientAccountBlogAdapter extends TypeAdapter<RecipientAccountBlog> {
  @override
  final int typeId = 11;

  @override
  RecipientAccountBlog read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RecipientAccountBlog(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, RecipientAccountBlog obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.address)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.avatarPath);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RecipientAccountBlogAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
