// @dart=2.9
import 'package:hive/hive.dart';

import '_json_serialize.dart';

part '_token_blog.g.dart';

@HiveType(typeId: 6)
class TokenBlog extends HiveObject implements JsonSerialize {
  @HiveField(0)
  String symbol;

  @HiveField(1)
  String address;

  @HiveField(2)
  String name;

  @HiveField(3)
  int decimals;

  @HiveField(4)
  String logo;

  @HiveField(5)
  String protocol;

  TokenBlog({
    this.symbol,
    this.address,
    this.name,
    this.decimals,
    this.logo,
    this.protocol = '20',
  });

  /// 当前token是否对应一个合约，若对应合约，视作代币，反之视作主币
  bool get isContract => (address != null &&
      address.length > 0 &&
      address != '0x0000000000000000000000000000000000000000' &&
      address != '0000000000000000000000000000000000000000');

  void fromJson(Map<String, dynamic> json) {
    symbol = json['symbol'];
    address = json['address'];
    name = json['name'];
    decimals = json['decimals'];
    logo = json['logoURI'];
    protocol = json['protocol'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['symbol'] = this.symbol;
    data['address'] = this.address;
    data['name'] = this.name;
    data['decimals'] = this.decimals;
    data['logoURI'] = this.logo;
    data['protocol'] = this.protocol;

    return data;
  }

  @override
  int get hashCode => 6;

  @override
  operator ==(dynamic other) => other is TokenBlog && address == other.address;
}
