// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of '_token_blog.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TokenBlogAdapter extends TypeAdapter<TokenBlog> {
  @override
  final int typeId = 6;

  @override
  TokenBlog read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TokenBlog(
      symbol: fields[0] as String,
      address: fields[1] as String,
      name: fields[2] as String,
      decimals: fields[3] as int,
      logo: fields[4] as String,
      protocol: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, TokenBlog obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.symbol)
      ..writeByte(1)
      ..write(obj.address)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.decimals)
      ..writeByte(4)
      ..write(obj.logo)
      ..writeByte(5)
      ..write(obj.protocol);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TokenBlogAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
