//@dart=2.9

import 'dart:io';
import 'package:sqlite3/sqlite3.dart';

abstract class DataBase {
  /// 建表的SQL
  String createDataBase();

  Future<bool> insert();
  Future<bool> insertAll();
  Future<bool> delete();
  Future<Object> select();
}
