// @dart=2.9
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as path;
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqlite3/sqlite3.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/engine/_booter.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

import '../../scan_api_service/ethereum/scan_api_service.dart';

String _logsServiceStorageKey(
  /// 区块浏览器地址
  String host,

  /// 地址
  String ownerAddress,
) =>
    bytesToHex(
      sha256
          .convert(utf8.encode(host.toLowerCase() + ownerAddress.toLowerCase()))
          .bytes,
    );

class LogsService extends ChangeNotifier {
  Completer<Database> dbComplater = Completer();

  final String storageKey;

  final String fromAddress;
  final String emitContractAddress;
  final ScanAPIService scanAPIService;
  final BigInt lastKblockHeight;
  final networkName;

  /// 防止多次执行了数据加载操作
  bool _fetchDataLocked = false;

  ValueNotifier<LogInfo> _logsListenable = ValueNotifier<LogInfo>(null);
  ValueNotifier<LogInfo> get dataSourceListenable => _logsListenable;

  LogsService({
    @required this.fromAddress,
    @required this.scanAPIService,
    @required this.emitContractAddress,
    this.networkName,
    this.lastKblockHeight,
  }) : this.storageKey = _logsServiceStorageKey(
          scanAPIService.hostURL,
          emitContractAddress,
        ) {
    getApplicationDocumentsDirectory().then((docurmentDir) async {
      Directory dataBaseRoot = Directory(docurmentDir.path + '/database');

      if (!dataBaseRoot.existsSync()) {
        dataBaseRoot.createSync();
      }

      final dataBase = sqlite3.open(
        path.join(
          docurmentDir.path,
          'database/$storageKey.db3',
        ),
      );

      dataBase.execute('''
        CREATE TABLE IF NOT EXISTS logs (
          address          STRING,
          topics           STRING,
          data             STRING,
          blockNumber      INTEGER,
          timeStamp        INTEGER,
          gasPrice         STRING,
          gasUsed          STRING,
          logIndex         INTEGER,
          transactionHash  STRING UNIQUE ON CONFLICT IGNORE,
          transactionIndex STRING
        );
        ''');

      // dataBase.execute('DELETE FROM logs;');

      dbComplater.complete(dataBase);
    });
  }

  Future<int> selectCount() async {
    final db = await dbComplater.future;
    final resultSet = db.select('SELECT COUNT(*) from logs;');

    return resultSet.first.values.first as int;
  }

  /// 当前记录下最大区块高度
  Future<int> latestBlockNumber() async {
    final db = await dbComplater.future;

    try {
      final resultSet = db.select(
        'SELECT blockNumber FROM logs ORDER BY blockNumber DESC LIMIT 1',
      );

      if (resultSet.isEmpty) {
        return 0;
      }

      return resultSet.single['blockNumber'] as int;
    } catch (e) {
      print(e);
      return 0;
    }
  }

  Future<int> earliestBlockNumber() async {
    final db = await dbComplater.future;
    final resultSet =
        db.select('SELECT blockNumber FROM logs ORDER BY blockNumber LIMIT 1');

    if (resultSet.isEmpty) {
      return 0;
    }

    return resultSet.single['blockNumber'] as int;
  }

  /// 查询数据库中的记录
  Future<List<Log>> selectLogs(
    int offset,
    int limit,
  ) async {
    final db = await dbComplater.future;

    final resultSet = db.select(
      'SELECT * FROM logs ORDER BY timeStamp DESC LIMIT ${limit.abs()} OFFSET ${offset.abs()};',
    );

    return resultSet.map((r) {
      return Log(
        logIndex: (r['logIndex'] as int).toString(),
        topics: r['topics'].toString().split(','),
        data: r['data'].toString(),
        address: r['address'].toString(),
        transactionHash: r['transactionHash'].toString().split('_').first,
        blockNumber: (r['blockNumber'] as int).toString(),
        timeStamp: (r['timeStamp'] as int).toString(),
        transactionIndex: r['transactionIndex'].toString(),
        gasPrice: r['gasPrice'].toString(),
        gasUsed: r['gasUsed'].toString(),
      );
    }).toList();
  }

  Future<int> mergeInsert(List<Log> logs) async {
    final db = await dbComplater.future;

    final statement = db.prepare(
        'INSERT INTO logs VALUES(${(',?' * 10).replaceFirst(',', '')})');

    try {
      logs.forEach(
        (e) => statement.execute(
          [
            /// address
            e.address,

            /// topics
            e.topics.join(','),

            /// data
            e.data,

            /// blockNumber
            int.parse(e.blockNumber.replaceFirst('0x', ''), radix: 16),

            /// timeStamp
            int.parse(e.timeStamp.replaceFirst('0x', ''), radix: 16),

            /// gasPrice
            e.gasPrice,

            /// gasUsed
            e.gasUsed,

            /// logIndex
            int.parse(e.logIndex.replaceFirst('0x', '0'), radix: 16),

            /// transactionHash
            e.transactionHash + '_' + e.logIndex,

            /// transactionIndex
            e.transactionIndex,
          ],
        ),
      );

      return db.getUpdatedRows();
    } catch (e) {
      print(e);
      return 0;
    }
  }

  /// 对app接口,由于可能存在异步的过程，比如拉取到本地数据后，区块浏览器中获得了新到数据，
  /// 所以本函数使用流传递
  Future<List<Log>> refresh({
    String topic_0,
    String topic_1,
    String topic_2,
    String topic_3,
    String topicOpr01,
    String topicOpr02,
    String topicOpr03,
    String topicOpr12,
    String topicOpr13,
    String topicOpr23,
    BigInt lastBlockHeight2,
  }) async {
    if (_fetchDataLocked) {
      return Future.value(null);
    }
    _fetchDataLocked = true;

    /// Index字段最多为4个，尝试获取地址所有相关Event
    try {
      /// 尝试拉取本地没有的最新数据
      return scanAPIService
          .getLogs(
            lastBlockHeight: lastBlockHeight2,
            emitContractAddress: this.emitContractAddress,
            topic_0: topic_0,
            topic_1: topic_1,
            topic_2: topic_2,
            topic_3: topic_3,
            topicOpr01: topicOpr01,
            topicOpr02: topicOpr02,
            topicOpr03: topicOpr03,
            topicOpr12: topicOpr12,
            topicOpr13: topicOpr13,
            topicOpr23: topicOpr23,
            fromBlockNumber: BigInt.from(await latestBlockNumber()),
          )
          .then(
            (txs) => txs.length > 0
                ? mergeInsert(txs).then(
                    (updateRows) => updateRows == 0 ? [] : txs,
                  )
                : null,
          );
    } catch (e) {
      rethrow;
    } finally {
      _fetchDataLocked = false;
    }
  }

  /// 对app接口,由于可能存在异步的过程，比如拉取到本地数据后，区块浏览器中获得了新到数据，
  /// 所以本函数使用流传递
  Future<List<Log>> refreshAll({
    String topic_0,
    String topic_1,
    String topic_2,
    String topic_3,
    String topicOpr01,
    String topicOpr02,
    String topicOpr03,
    String topicOpr12,
    String topicOpr13,
    String topicOpr23,
    BigInt lastBlockHeight2,
  }) async {
    if (_fetchDataLocked) {
      return Future.value(null);
    }
    _fetchDataLocked = true;

    /// Index字段最多为4个，尝试获取地址所有相关Event
    try {
      /// 尝试拉取本地没有的最新数据
      return scanAPIService
          .getLogsAll(
            lastBlockHeight: lastBlockHeight2,
            emitContractAddress: this.emitContractAddress,
            topic_0: topic_0,
            topic_1: topic_1,
            topic_2: topic_2,
            topic_3: topic_3,
            topicOpr01: topicOpr01,
            topicOpr02: topicOpr02,
            topicOpr03: topicOpr03,
            topicOpr12: topicOpr12,
            topicOpr13: topicOpr13,
            topicOpr23: topicOpr23,
            fromBlockNumber: BigInt.from(await latestBlockNumber()),
          )
          .then(
            (txs) => txs.length > 0
                ? mergeInsert(txs).then(
                    (updateRows) => updateRows == 0 ? [] : txs,
                  )
                : null,
          );
    } catch (e) {
      rethrow;
    } finally {
      _fetchDataLocked = false;
    }
  }

  Future<List<Log>> more({
    BigInt lastBlockHeight2,
    String topic_0,
    String topic_1,
    String topic_2,
    String topic_3,
    String topicOpr01,
    String topicOpr02,
    String topicOpr03,
    String topicOpr12,
    String topicOpr13,
    String topicOpr23,
  }) async {
    if (_fetchDataLocked) {
      return Future.value(null);
    }
    _fetchDataLocked = true;

    /// Index字段最多为4个，尝试获取地址所有相关Event
    try {
      /// 尝试拉取本地没有的最新数据

      return scanAPIService
          .getLogs(
            lastBlockHeight: lastBlockHeight2,
            emitContractAddress: this.emitContractAddress,
            topic_0: topic_0,
            topic_1: topic_1,
            topic_2: topic_2,
            topic_3: topic_3,
            topicOpr01: topicOpr01,
            topicOpr02: topicOpr02,
            topicOpr03: topicOpr03,
            topicOpr12: topicOpr12,
            topicOpr13: topicOpr13,
            topicOpr23: topicOpr23,
            fromBlockNumber: BigInt.zero,
            toBlockNumber: BigInt.from(await latestBlockNumber()),
          )
          .then(
            (txs) => txs.length > 0
                ? mergeInsert(txs).then(
                    (updateRows) => updateRows == 0 ? [] : txs,
                  )
                : null,
          );
    } catch (e) {
      rethrow;
    } finally {
      _fetchDataLocked = false;
    }
  }
}
