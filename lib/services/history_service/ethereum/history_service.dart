// @dart=2.9
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as path;
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:sqlite3/sqlite3.dart';

import '../../scan_api_service/ethereum/scan_api_service.dart';

String _historyServiceStorageKey(
  /// 区块浏览器地址
  String host,

  /// 地址
  String ownerAddress,
) =>
    bytesToHex(
      sha256
          .convert(utf8.encode(host.toLowerCase() + ownerAddress.toLowerCase()))
          .bytes,
    );

class HistoryService extends ChangeNotifier {
  Completer<Database> dbComplater = Completer();

  final String fromAddress;
  final int pageSize = 50;
  final String storageKey;

  final ScanAPIService scanAPIService;

  /// 防止多次执行了数据加载操作
  bool _fetchDataLocked = false;

  HistoryService({
    @required this.fromAddress,
    @required this.scanAPIService,
  }) : this.storageKey = _historyServiceStorageKey(
          scanAPIService.hostURL,
          fromAddress,
        ) {
    getApplicationDocumentsDirectory().then((docurmentDir) {
      Directory dataBaseRoot = Directory(docurmentDir.path + '/database');

      if (!dataBaseRoot.existsSync()) {
        dataBaseRoot.createSync();
      }

      final dataBase = sqlite3.open(
        path.join(
          docurmentDir.path,
          'database/$storageKey.db3',
        ),
      );

      dataBase.execute('''
          CREATE TABLE IF NOT EXISTS txs (
              blockNumber       INTEGER,
              timeStamp         INTEGER,
              hash              STRING UNIQUE ON CONFLICT IGNORE,
              nonce             INTEGER,
              blockHash         STRING,
              transactionIndex  STRING,
              fromAddress       STRING,
              toAddress         STRING,
              value             STRING,
              gas               STRING,
              gasPrice          STRING,
              isError           STRING,
              txreceiptStatus   STRING,
              input             STRING,
              contractAddress   STRING,
              cumulativeGasUsed STRING,
              gasUsed           STRING,
              confirmations     STRING
          );
        ''');

      dbComplater.complete(dataBase);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<int> selectCount() async {
    final db = await dbComplater.future;
    final resultSet = db.select('SELECT COUNT(*) from txs;');

    return resultSet.first.values.first as int;
  }

  /// 查询数据库中的记录
  Future<List<Transaction>> selectTransaction(
    int offset,
    int limit,
  ) async {
    final db = await dbComplater.future;

    final resultSet = db.select(
        'SELECT * FROM txs ORDER BY timeStamp DESC LIMIT ${limit.abs()} OFFSET ${offset.abs()};');

    return resultSet.map((r) {
      return Transaction(
        blockNumber: (r['blockNumber'] as int).toString(),
        timeStamp: (r['timeStamp'] as int).toString(),
        nonce: (r['blockNumber'] as int).toString(),
        hash: r['hash'].toString(),
        blockHash: r['blockHash'].toString(),
        transactionIndex: r['transactionIndex'].toString(),
        from: r['fromAddress'].toString(),
        to: r['toAddress'].toString(),
        value: r['value'].toString(),
        gas: r['gas'].toString(),
        gasPrice: r['gasPrice'].toString(),
        isError: r['isError'].toString(),
        txreceiptStatus: r['txreceiptStatus'].toString(),
        input: r['input'].toString(),
        contractAddress: r['contractAddress'].toString(),
        cumulativeGasUsed: r['cumulativeGasUsed'].toString(),
        gasUsed: r['gasUsed'].toString(),
        confirmations: r['confirmations'].toString(),
      );
    }).toList();
  }

  Future<int> mergeInsert(List<Transaction> txs) async {
    final db = await dbComplater.future;

    final statement = db.prepare(
        'INSERT INTO txs VALUES(${(',?' * 18).replaceFirst(',', '')})');

    try {
      txs.forEach(
        (e) => statement.execute(
          [
            /// blockNumber       INTEGER,
            int.parse(e.blockNumber),

            /// timeStamp         INTEGER,
            int.parse(e.timeStamp),

            /// hash              STRING   UNIQUE ON CONFLICT IGNORE,
            e.hash,

            /// nonce             INTEGER,
            int.parse(e.nonce),

            /// blockHash         STRING,
            e.blockHash,

            /// transactionIndex  STRING,
            e.transactionIndex,

            /// [from]            STRING,
            e.from,

            /// [to]              STRING,
            e.to,

            /// value             STRING,
            e.value,

            /// gas               STRING,
            e.gas,

            /// gasPrice          STRING,
            e.gasPrice,

            /// isError           STRING,
            e.isError,

            /// txreceiptStatus   STRING,
            e.txreceiptStatus,

            /// input             STRING,
            e.input,

            /// contractAddress   STRING,
            e.contractAddress,

            /// cumulativeGasUsed STRING,
            e.cumulativeGasUsed,

            /// gasUsed           STRING,
            e.gasUsed,

            ///confirmations     STRING
            e.confirmations,
          ],
        ),
      );

      return db.getUpdatedRows();
    } catch (e) {
      return 0;
    }
  }

  /// 追加数据
  Future<List<Transaction>> more() async {
    if (_fetchDataLocked) {
      return Future.value(null);
    }

    _fetchDataLocked = true;

    try {
      /// 拉取更早的数据
      /// 按照当前数据的量按照20一页，计算到重复数据最少的部分
      /// 注意区块浏览器接口pageNumber是从1开始的。
      /// 注意合并数据
      return scanAPIService
          .accountTxList(
            fromAddress: this.fromAddress,
            pageNumber: (await selectCount()) ~/ 20 + 1,
            pageSize: pageSize,
            orderByDesc: true,
          )
          .then(
            (txs) => txs.length > 0
                ? mergeInsert(txs).then(
                    (updateRows) => updateRows == 0 ? [] : txs,
                  )
                : null,
          );
    } catch (e) {
      rethrow;
    } finally {
      _fetchDataLocked = false;
    }
  }

  /// 对app接口,由于可能存在异步的过程，比如拉取到本地数据后，区块浏览器中获得了新到数据，
  /// 所以本函数使用流传递
  Future<List<Transaction>> refresh() {
    if (_fetchDataLocked) {
      return Future.value(null);
    }
    _fetchDataLocked = true;

    try {
      /// 尝试拉取本地没有的最新数据
      return scanAPIService
          .accountTxList(
            fromAddress: this.fromAddress,
            pageNumber: 1,
            pageSize: pageSize,
            orderByDesc: true,
          )
          .then(
            (txs) => txs.length > 0
                ? mergeInsert(txs).then(
                    (updateRows) => updateRows == 0 ? [] : txs,
                  )
                : null,
          );
    } catch (e) {
      rethrow;
    } finally {
      _fetchDataLocked = false;
    }
  }
}
