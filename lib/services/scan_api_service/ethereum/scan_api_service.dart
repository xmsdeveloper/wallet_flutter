// @dart=2.9

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/ethereum.dart';

import 'entity/entity.dart';
export 'entity/entity.dart';

enum ScanAPIServiceError {
  ResponseStateError,
}

class ScanAPIService {
  final String hostURL;
  final String apiKey;
  final http.Client httpClient;
  final String networkName;

  ScanAPIService({
    @required this.hostURL,
    @required this.apiKey,
    @required this.httpClient,
    this.networkName,
  });

  Future<List<Transaction>> accountTxList({
    @required String fromAddress,
    int pageNumber = 1,
    int pageSize = 20,
    bool orderByDesc = true,
  }) async {
    print(
      '$hostURL/api?'
      'module=account&action=txlist'
      '&address=$fromAddress'
      '&page=$pageNumber'
      '&offset=$pageSize'
      '&sort=${orderByDesc ? "desc" : "asc"}'
      '&apikey=$apiKey'
      '&startblock=0'
      '&endblock=99999999',
    );
    final response = await httpClient.get(
      '$hostURL/api?'
      'module=account&action=txlist'
      '&address=$fromAddress'
      '&page=$pageNumber'
      '&offset=$pageSize'
      '&sort=${orderByDesc ? "desc" : "asc"}'
      '&apikey=$apiKey'
      '&startblock=0'
      '&endblock=99999999',
    );

    final responseJson = json.decode(response.body);

    if (responseJson['status'].toString() != '1') {
      throw ScanAPIServiceError.ResponseStateError;
    }

    final List txListJson = responseJson['result'];

    return txListJson.map((e) => Transaction.fromJson(e)).toList();
  }

  Future<GasOracle> gasTrackerGasOracle() async {
    final response = await httpClient
        .get(
          '$hostURL/api?'
          'module=gastracker&action=gasoracle'
          '&apikey=$apiKey',
        )
        .timeout(Duration(milliseconds: 2000));
    final responseJson = json.decode(response.body);

    if (responseJson['status'].toString() != '1') {
      throw ScanAPIServiceError.ResponseStateError;
    }

    return GasOracle.fromJson(responseJson['result']);
  }

  Future<Duration> gasTrackerTimeEstimate(int gwei) async {
    final response = await httpClient.get(
      '$hostURL/api?'
      'module=gastracker&action=gasestimate'
      '&gasprice=${(BigInt.from(gwei) * BigInt.from(10).pow(9)).toString()}'
      '&apikey=$apiKey',
    );

    final responseJson = json.decode(response.body);

    if (responseJson['status'] != '1') {
      throw ScanAPIServiceError.ResponseStateError;
    }

    return Duration(seconds: int.parse(responseJson['result'], radix: 10));
  }

  Future<List<Log>> getLogs({
    @required String emitContractAddress,
    @required BigInt fromBlockNumber,
    BigInt lastBlockHeight,
    BigInt toBlockNumber,
    String topic_0,
    String topic_1,
    String topic_2,
    String topic_3,
    String topicOpr01,
    String topicOpr02,
    String topicOpr03,
    String topicOpr12,
    String topicOpr13,
    String topicOpr23,
  }) async {
    if (this.networkName == "HECO") {
      int endNum = lastBlockHeight.toInt();
      int reNum = lastBlockHeight.toInt() - 3000;

      int moNum = lastBlockHeight.toInt() - 10000;
      List data = [];

      while (endNum > 0) {
        final parmas = {
          'address': emitContractAddress,

          // 'fromBlock': fromBlockNumber == null
          //     ? (endNum - 1000).toString()
          //     : fromBlockNumber.toString(),
          // 'toBlock': toBlockNumber == null
          //     ? endNum.toString()
          //     : toBlockNumber.toString(),

          'fromBlock': fromBlockNumber == null
              ? (endNum - 1000).toString()
              : (endNum - 1000).toString(),
          'toBlock':
              toBlockNumber == null ? endNum.toString() : endNum.toString(),
          if (topic_0 != null) 'topic0': topic_0,
          if (topic_1 != null) 'topic1': topic_1,
          if (topic_2 != null) 'topic2': topic_2,
          if (topic_3 != null) 'topic3': topic_3,
          if (topicOpr01 != null) 'topic0_1_opr': topicOpr01,
          if (topicOpr02 != null) 'topic0_2_opr': topicOpr02,
          if (topicOpr03 != null) 'topic0_3_opr': topicOpr03,
          if (topicOpr12 != null) 'topic1_2_opr': topicOpr12,
          if (topicOpr13 != null) 'topic1_3_opr': topicOpr13,
          if (topicOpr23 != null) 'topic2_3_opr': topicOpr23,
        };

        final parmasJoined = parmas.keys
            .toList()
            .map((e) => '$e=${parmas[e].toLowerCase()}')
            .join('&');

        final response = await httpClient.get(
          '$hostURL/api?'
          'module=logs&action=getLogs'
          '&$parmasJoined'
          '&apikey=$apiKey',
        );

        print(
          '$hostURL/api?'
          'module=logs&action=getLogs'
          '&$parmasJoined'
          '&apikey=$apiKey',
        );

        final responseJson = json.decode(response.body);

        // if (responseJson['status'].toString() != '1') {
        //   throw ScanAPIServiceError.ResponseStateError;
        // }

        final List txListJson = responseJson['result'];

        endNum -= 1000;

        if (txListJson != null) {
          // data.add(txListJson.map((e) => e).toList());

          txListJson.map((e) => data.add(e)).toList();
        }

        if (endNum <= reNum && fromBlockNumber.toInt() != 0) {
          // return [];

          return data.map((e) => Log.fromJson(e)).toList();
        } else if (endNum <= moNum) {
          return data.map((e) => Log.fromJson(e)).toList();
        }
      }

      return data.map((e) => Log.fromJson(e)).toList();
    } else {
      final parmas = {
        'address': emitContractAddress,
        'fromBlock': fromBlockNumber == null ? '0' : fromBlockNumber.toString(),
        'toBlock': toBlockNumber == null ? 'latest' : toBlockNumber.toString(),
        if (topic_0 != null) 'topic0': topic_0,
        if (topic_1 != null) 'topic1': topic_1,
        if (topic_2 != null) 'topic2': topic_2,
        if (topic_3 != null) 'topic3': topic_3,
        if (topicOpr01 != null) 'topic0_1_opr': topicOpr01,
        if (topicOpr02 != null) 'topic0_2_opr': topicOpr02,
        if (topicOpr03 != null) 'topic0_3_opr': topicOpr03,
        if (topicOpr12 != null) 'topic1_2_opr': topicOpr12,
        if (topicOpr13 != null) 'topic1_3_opr': topicOpr13,
        if (topicOpr23 != null) 'topic2_3_opr': topicOpr23,
      };

      final parmasJoined = parmas.keys
          .toList()
          .map((e) => '$e=${parmas[e].toLowerCase()}')
          .join('&');

      final response = await httpClient.get(
        '$hostURL/api?'
        'module=logs&action=getLogs'
        '&$parmasJoined'
        '&apikey=$apiKey',
      );

      print(
        '$hostURL/api?'
        'module=logs&action=getLogs'
        '&$parmasJoined'
        '&apikey=$apiKey',
      );

      final responseJson = json.decode(response.body);
      if (responseJson['status'].toString() != '1') {
        throw ScanAPIServiceError.ResponseStateError;
      }

      final List txListJson = responseJson['result'];

      return txListJson.map((e) => Log.fromJson(e)).toList();
    }
  }

  Future<List<Log>> getLogsAll({
    @required String emitContractAddress,
    @required BigInt fromBlockNumber,
    BigInt lastBlockHeight,
    BigInt toBlockNumber,
    String topic_0,
    String topic_1,
    String topic_2,
    String topic_3,
    String topicOpr01,
    String topicOpr02,
    String topicOpr03,
    String topicOpr12,
    String topicOpr13,
    String topicOpr23,
  }) async {
    if (this.networkName == "HECO" && fromBlockNumber.toInt() != 0) {
      int endNum = lastBlockHeight.toInt();
      List data = [];

      while (endNum > 0) {
        final parmas = {
          'address': emitContractAddress,

          // 'fromBlock': fromBlockNumber == null
          //     ? (endNum - 1000).toString()
          //     : fromBlockNumber.toString(),
          // 'toBlock': toBlockNumber == null
          //     ? endNum.toString()
          //     : toBlockNumber.toString(),

          'fromBlock': fromBlockNumber == null
              ? (endNum - 1000).toString()
              : (endNum - 1000).toString(),
          'toBlock':
              toBlockNumber == null ? endNum.toString() : endNum.toString(),
          if (topic_0 != null) 'topic0': topic_0,
          if (topic_1 != null) 'topic1': topic_1,
          if (topic_2 != null) 'topic2': topic_2,
          if (topic_3 != null) 'topic3': topic_3,
          if (topicOpr01 != null) 'topic0_1_opr': topicOpr01,
          if (topicOpr02 != null) 'topic0_2_opr': topicOpr02,
          if (topicOpr03 != null) 'topic0_3_opr': topicOpr03,
          if (topicOpr12 != null) 'topic1_2_opr': topicOpr12,
          if (topicOpr13 != null) 'topic1_3_opr': topicOpr13,
          if (topicOpr23 != null) 'topic2_3_opr': topicOpr23,
        };

        final parmasJoined = parmas.keys
            .toList()
            .map((e) => '$e=${parmas[e].toLowerCase()}')
            .join('&');

        final response = await httpClient.get(
          '$hostURL/api?'
          'module=logs&action=getLogs'
          '&$parmasJoined'
          '&apikey=$apiKey',
        );

        print(
          '$hostURL/api?'
          'module=logs&action=getLogs'
          '&$parmasJoined'
          '&apikey=$apiKey',
        );

        final responseJson = json.decode(response.body);

        final List txListJson = responseJson['result'];

        endNum -= 1000;

        if (txListJson != null) {
          // data.add(txListJson.map((e) => e).toList());

          txListJson.map((e) => data.add(e)).toList();
        }

        if (endNum <= fromBlockNumber.toInt() && fromBlockNumber.toInt() != 0) {
          // return [];
          return data.map((e) => Log.fromJson(e)).toList();
        }
      }
    }
  }
}
