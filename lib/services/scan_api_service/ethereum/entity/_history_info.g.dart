// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of '_history_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class HistoryInfoAdapter extends TypeAdapter<HistoryInfo> {
  @override
  final int typeId = 9;

  @override
  HistoryInfo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return HistoryInfo(
      ownerAddress: fields[1] as String,
      fristTransactionBlock: fields[0] as int,
      txs: (fields[2] as List)?.cast<Transaction>(),
    );
  }

  @override
  void write(BinaryWriter writer, HistoryInfo obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.fristTransactionBlock)
      ..writeByte(1)
      ..write(obj.ownerAddress)
      ..writeByte(2)
      ..write(obj.txs);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HistoryInfoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
