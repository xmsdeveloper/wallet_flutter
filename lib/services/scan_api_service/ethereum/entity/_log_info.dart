// @dart=2.9
import 'package:hive/hive.dart';
import 'package:meta/meta.dart';

import '_log.dart';

part '_log_info.g.dart';

@HiveType(typeId: 12)
class LogInfo extends HiveObject {
  /// 第一笔交易记录产生的时间，说明早于本块后，不可能存在交易记录
  @HiveField(0)
  int fristTransactionBlock;

  /// 记录隶属的地址
  @HiveField(1)
  String ownerAddress;

  @HiveField(2)
  String contractAddress;

  @HiveField(3)
  String topic0;

  @HiveField(4)
  List<Log> logs;

  LogInfo({
    @required this.ownerAddress,
    @required this.contractAddress,
    @required this.topic0,
    this.fristTransactionBlock = 0,
    this.logs,
  });
}
