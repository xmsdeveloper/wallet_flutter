// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of '_log_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LogInfoAdapter extends TypeAdapter<LogInfo> {
  @override
  final int typeId = 12;

  @override
  LogInfo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LogInfo(
      ownerAddress: fields[1] as String,
      contractAddress: fields[2] as String,
      topic0: fields[3] as String,
      fristTransactionBlock: fields[0] as int,
      logs: (fields[4] as List)?.cast<Log>(),
    );
  }

  @override
  void write(BinaryWriter writer, LogInfo obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.fristTransactionBlock)
      ..writeByte(1)
      ..write(obj.ownerAddress)
      ..writeByte(2)
      ..write(obj.contractAddress)
      ..writeByte(3)
      ..write(obj.topic0)
      ..writeByte(4)
      ..write(obj.logs);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LogInfoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
