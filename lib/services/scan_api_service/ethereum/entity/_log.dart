// @dart=2.9
import 'package:hive/hive.dart';
import 'package:meta/meta.dart';

part '_log.g.dart';

@HiveType(typeId: 11)
class Log extends HiveObject {
  @HiveField(0)
  final String address;

  @HiveField(1)
  final List<String> topics;

  @HiveField(2)
  final String data;

  @HiveField(3)
  final String blockNumber;

  @HiveField(4)
  final String timeStamp;

  @HiveField(5)
  final String gasPrice;

  @HiveField(6)
  final String gasUsed;

  @HiveField(7)
  final String logIndex;

  @HiveField(8)
  final String transactionHash;

  @HiveField(9)
  final String transactionIndex;

  Log({
    @required this.address,
    @required this.topics,
    @required this.data,
    @required this.blockNumber,
    @required this.timeStamp,
    @required this.gasPrice,
    @required this.gasUsed,
    @required this.logIndex,
    @required this.transactionHash,
    @required this.transactionIndex,
  });

  factory Log.fromJson(Map<String, dynamic> json) => Log(
        address: json['address'],
        topics: json['topics'].cast<String>(),
        data: json['data'],
        blockNumber: json['blockNumber'],
        timeStamp: json['timeStamp'],
        gasPrice: json['gasPrice'],
        gasUsed: json['gasUsed'],
        logIndex: json['logIndex'],
        transactionHash: json['transactionHash'],
        transactionIndex: json['transactionIndex'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['topics'] = this.topics;
    data['data'] = this.data;
    data['blockNumber'] = this.blockNumber;
    data['timeStamp'] = this.timeStamp;
    data['gasPrice'] = this.gasPrice;
    data['gasUsed'] = this.gasUsed;
    data['logIndex'] = this.logIndex;
    data['transactionHash'] = this.transactionHash;
    data['transactionIndex'] = this.transactionIndex;
    return data;
  }
}
