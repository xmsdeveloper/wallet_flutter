// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of '_transaction.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TransactionAdapter extends TypeAdapter<Transaction> {
  @override
  final int typeId = 10;

  @override
  Transaction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Transaction(
      blockNumber: fields[0] as String,
      timeStamp: fields[1] as String,
      hash: fields[2] as String,
      nonce: fields[3] as String,
      blockHash: fields[4] as String,
      transactionIndex: fields[5] as String,
      from: fields[6] as String,
      to: fields[7] as String,
      value: fields[8] as String,
      gas: fields[9] as String,
      gasPrice: fields[10] as String,
      isError: fields[11] as String,
      txreceiptStatus: fields[12] as String,
      input: fields[13] as String,
      contractAddress: fields[14] as String,
      cumulativeGasUsed: fields[15] as String,
      gasUsed: fields[16] as String,
      confirmations: fields[17] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Transaction obj) {
    writer
      ..writeByte(18)
      ..writeByte(0)
      ..write(obj.blockNumber)
      ..writeByte(1)
      ..write(obj.timeStamp)
      ..writeByte(2)
      ..write(obj.hash)
      ..writeByte(3)
      ..write(obj.nonce)
      ..writeByte(4)
      ..write(obj.blockHash)
      ..writeByte(5)
      ..write(obj.transactionIndex)
      ..writeByte(6)
      ..write(obj.from)
      ..writeByte(7)
      ..write(obj.to)
      ..writeByte(8)
      ..write(obj.value)
      ..writeByte(9)
      ..write(obj.gas)
      ..writeByte(10)
      ..write(obj.gasPrice)
      ..writeByte(11)
      ..write(obj.isError)
      ..writeByte(12)
      ..write(obj.txreceiptStatus)
      ..writeByte(13)
      ..write(obj.input)
      ..writeByte(14)
      ..write(obj.contractAddress)
      ..writeByte(15)
      ..write(obj.cumulativeGasUsed)
      ..writeByte(16)
      ..write(obj.gasUsed)
      ..writeByte(17)
      ..write(obj.confirmations);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TransactionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
