// @dart=2.9
import 'package:hive/hive.dart';
import 'package:meta/meta.dart';

import '_transaction.dart';

part '_history_info.g.dart';

@HiveType(typeId: 9)
class HistoryInfo extends HiveObject {
  /// 第一笔交易记录产生的时间，说明早于本块后，不可能存在交易记录
  @HiveField(0)
  int fristTransactionBlock;

  /// 记录隶属的地址
  @HiveField(1)
  String ownerAddress;

  @HiveField(2)
  List<Transaction> txs;

  HistoryInfo({
    @required this.ownerAddress,
    this.fristTransactionBlock = 0,
    this.txs,
  });

  @override
  String toString() => '('
      'fristTransactionBlock: $fristTransactionBlock, '
      'ownerAddress: $ownerAddress, '
      'txs: $txs'
      ')';
}
