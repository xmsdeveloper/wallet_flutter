// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of '_log.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LogAdapter extends TypeAdapter<Log> {
  @override
  final int typeId = 11;

  @override
  Log read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Log(
      address: fields[0] as String,
      topics: (fields[1] as List)?.cast<String>(),
      data: fields[2] as String,
      blockNumber: fields[3] as String,
      timeStamp: fields[4] as String,
      gasPrice: fields[5] as String,
      gasUsed: fields[6] as String,
      logIndex: fields[7] as String,
      transactionHash: fields[8] as String,
      transactionIndex: fields[9] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Log obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.address)
      ..writeByte(1)
      ..write(obj.topics)
      ..writeByte(2)
      ..write(obj.data)
      ..writeByte(3)
      ..write(obj.blockNumber)
      ..writeByte(4)
      ..write(obj.timeStamp)
      ..writeByte(5)
      ..write(obj.gasPrice)
      ..writeByte(6)
      ..write(obj.gasUsed)
      ..writeByte(7)
      ..write(obj.logIndex)
      ..writeByte(8)
      ..write(obj.transactionHash)
      ..writeByte(9)
      ..write(obj.transactionIndex);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LogAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
