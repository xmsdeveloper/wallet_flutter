// @dart=2.9
class GasOracle {
  String lastBlock;
  int safeGasPrice;
  int proposeGasPrice;
  int fastGasPrice;

  GasOracle({
    this.lastBlock,
    this.safeGasPrice,
    this.proposeGasPrice,
    this.fastGasPrice,
  });

  GasOracle.fromJson(Map<String, dynamic> json) {
    lastBlock = json['LastBlock'];
    // safeGasPrice = int.parse(json['SafeGasPrice']);
    // proposeGasPrice = int.parse(json['ProposeGasPrice']);
    // fastGasPrice = int.parse(json['FastGasPrice']);

    safeGasPrice =
        int.parse(double.parse(json['SafeGasPrice']).round().toString());
    proposeGasPrice =
        int.parse(double.parse(json['ProposeGasPrice']).round().toString());
    fastGasPrice =
        int.parse(double.parse(json['FastGasPrice']).round().toString());
  }
}
