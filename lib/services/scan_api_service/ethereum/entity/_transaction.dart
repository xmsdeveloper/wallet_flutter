// @dart=2.9
import 'package:hive/hive.dart';

part '_transaction.g.dart';

@HiveType(typeId: 10)
class Transaction {
  /// 成交区块，成交高度
  @HiveField(0)
  final String blockNumber;

  /// 成交时间
  @HiveField(1)
  final String timeStamp;

  /// 交易Hash值
  @HiveField(2)
  final String hash;

  /// 检索从0开始
  @HiveField(3)
  final String nonce;

  /// 区块Hash值
  @HiveField(4)
  final String blockHash;

  /// 当前交易处于Block中的交易检索
  @HiveField(5)
  final String transactionIndex;

  /// 发送方
  @HiveField(6)
  final String from;

  /// 接收方
  @HiveField(7)
  final String to;

  /// 转账的主币数量
  @HiveField(8)
  final String value;

  /// Gas
  @HiveField(9)
  final String gas;

  /// 燃油费
  @HiveField(10)
  final String gasPrice;

  /// 状态
  @HiveField(11)
  final String isError;

  /// 状态
  @HiveField(12)
  final String txreceiptStatus;

  /// 输入数据
  @HiveField(13)
  final String input;

  /// 合约地址
  @HiveField(14)
  final String contractAddress;

  @HiveField(15)
  final String cumulativeGasUsed;

  /// 实际使用的Gas
  @HiveField(16)
  final String gasUsed;

  @HiveField(17)
  final String confirmations;

  Transaction({
    this.blockNumber,
    this.timeStamp,
    this.hash,
    this.nonce,
    this.blockHash,
    this.transactionIndex,
    this.from,
    this.to,
    this.value,
    this.gas,
    this.gasPrice,
    this.isError,
    this.txreceiptStatus,
    this.input,
    this.contractAddress,
    this.cumulativeGasUsed,
    this.gasUsed,
    this.confirmations,
  });

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        blockNumber: json['blockNumber'],
        timeStamp: json['timeStamp'],
        hash: json['hash'],
        nonce: json['nonce'],
        blockHash: json['blockHash'],
        transactionIndex: json['transactionIndex'],
        from: json['from'],
        to: json['to'],
        value: json['value'],
        gas: json['gas'],
        gasPrice: json['gasPrice'],
        isError: json['isError'],
        txreceiptStatus: json['txreceipt_status'],
        input: json['input'],
        contractAddress: json['contractAddress'],
        cumulativeGasUsed: json['cumulativeGasUsed'],
        gasUsed: json['gasUsed'],
        confirmations: json['confirmations'],
      );

  String get hashBlog => this.hash.replaceRange(9, this.hash.length - 9, '...');

  bool get isCallContract => input.length > 2;

  bool get isCreation => input.length > 2 && to.length <= 2;

  @override
  int get hashCode => 10;

  /// 集合运算中使用，只要hash相等，则视作相同的交易
  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Transaction && other.hash == this.hash;
}
