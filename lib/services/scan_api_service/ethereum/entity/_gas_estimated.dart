// @dart=2.9
import 'package:intl/intl.dart';

/// 估算的结果集
class GasEstimated {
  final int gasPriceWei;
  final BigInt gas;
  final Duration duration;

  const GasEstimated(this.gasPriceWei, this.duration, this.gas);

  String get gasForrmatString => NumberFormat('###,###').format(gas.toInt());

  String get durationString {
    if (duration.inSeconds < 30) {
      return '< 0.5 min';
    } else if (duration.inSeconds >= 30 && duration.inSeconds < 60) {
      return '< 1 min';
    } else if (duration.inSeconds >= 60 && duration.inSeconds < 3600) {
      return '< ${duration.inMinutes} min';
    } else {
      return '> 1 hours';
    }
  }
}
