// @dart=2.9
part of '../local_service.dart';

const KCachedCount = '_KCachedCount';

class CachedCount {
  final DateTime updateTime;
  final String balance;

  const CachedCount(this.updateTime, this.balance);
}

extension NftCount on LocalService {
  String _key(
    TokenBlog blog,
    String account,
  ) =>
      '${KCachedCount}_${blog.address}_$account';

  String _value(DateTime t, String balance) =>
      '${t.millisecondsSinceEpoch ~/ 1000}:$balance';

  Future<void> putCachedCount(
    TokenBlog blog,
    String account,
    String price,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _key(
        blog,
        account,
      ),
      _value(
        DateTime.now(),
        price,
      ),
    );
  }

  Future<void> removeCachedCount(
    TokenBlog blog,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(
      _key(
        blog,
        account,
      ),
    );
  }

  Future<CachedCount> getCachedCount(
    TokenBlog blog,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _key(blog, account);

    if (!box.containsKey(key)) {
      return null;
    } else {
      final value = box.get(key) as String;
      final t = int.parse(value.split(':')[0]);
      final v = value.split(':')[1].toString();
      return CachedCount(DateTime.fromMillisecondsSinceEpoch(t * 1000), v);
    }
  }
}
