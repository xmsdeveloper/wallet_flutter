// @dart=2.9
part of '../local_service.dart';

/// 最近联系人的扩展实现
const KRecentAccountList = '_KRecentAccountList';

extension RecentAccountList on LocalService {
  /// 添加最近转账的地址
  Future<void> insertRecentRecipientAccount(RecipientAccountBlog blog) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<RecipientAccountBlog> list = await selectRecentRecipientAccounts;

    /// 已经存在最近的一次记录,需要调整位置
    list.remove(blog);

    /// 最多记录20个最近地址
    if (list.length >= 20) {
      list.removeAt(0);
    }

    box.put(KRecentAccountList, list..add(blog));
  }

  /// 最近交易过的地址列表
  Future<List<RecipientAccountBlog>> get selectRecentRecipientAccounts =>
      _localDataSandBoxCompleter.future.then((box) => box
              .containsKey(KRecentAccountList)
          ? (box.get(KRecentAccountList) as List).cast<RecipientAccountBlog>()
          : <RecipientAccountBlog>[].cast<RecipientAccountBlog>());

//只保存地址信息
  Future<void> insertRecentRecipientAddress(String blog) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<String> list = await selectRecentRecipientAddress;

    /// 已经存在最近的一次记录,需要调整位置
    list.remove(blog);

    /// 最多记录20个最近地址
    if (list.length >= 20) {
      list.removeAt(0);
    }

    // box.put(KRecentAccountList, list..add(blog));
    box.put(KRecentAccountList, list..insert(0, blog));
  }

  /// 最近交易过的地址列表
  Future<List<String>> get selectRecentRecipientAddress =>
      _localDataSandBoxCompleter.future.then((box) =>
          box.containsKey(KRecentAccountList)
              ? (box.get(KRecentAccountList) as List).cast<String>()
              : <String>[].cast<String>());

  /// 移除地址
  Future<void> deleteRecipientAccountsWhere(
      bool test(RecipientAccountBlog element)) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<RecipientAccountBlog> list = box.get(KRecentAccountList);
    box.put(KRecentAccountList, list..removeWhere(test));
  }
}
