// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KDappSearchHistory = '_KDappSearchHistory';

extension DappSearchHistory on LocalService {
  Future<List<String>> dappHistorys() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KDappSearchHistory)) {
      return [];
    } else {
      return (box.get(KDappSearchHistory) as List).cast<String>();
    }
  }

  Future<void> dappHistoryInsert(String keyword) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappHistorys();
    if (origin.contains(keyword)) {
    } else {
      // origin.add(keyword);
      origin.insert(0, keyword);
    }

    if (origin.length > 20) {
      origin.removeAt(0);
    }

    box.put(KDappSearchHistory, origin);
  }

  Future<void> dappHistoryDelete(String keyword) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappHistorys();

    origin.removeWhere((element) => element == keyword);
    return box.put(KDappSearchHistory, origin);
  }

  Future<void> dappHistoryClean() async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(KDappSearchHistory);
  }
}
