// @dart=2.9
part of '../local_service.dart';

const KCachedPrice = '_KCachedPrice';

class CachedPrice {
  final DateTime updateTime;
  final String balance;

  const CachedPrice(this.updateTime, this.balance);
}

extension TokenPrice on LocalService {
  String _key(
    TokenBlog blog,
    String account,
  ) =>
      '${KCachedPrice}_${blog.address}_$account';

  String _keys(
    String account,
  ) =>
      '${KCachedPrice}_$account';

  String _value(DateTime t, String balance) =>
      '${t.millisecondsSinceEpoch ~/ 1000}:$balance';

  Future<void> putCachedPrice(
    TokenBlog blog,
    String account,
    String price,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _key(
        blog,
        account,
      ),
      _value(
        DateTime.now(),
        price,
      ),
    );
  }

  Future<void> removeCachedPrice(
    TokenBlog blog,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    // return box.put(
    //   _key(
    //     blog,
    //     account,
    //   ),
    //   _value(
    //     DateTime.now(),
    //     price,
    //   ),
    // );
    return box.delete(
      _key(
        blog,
        account,
      ),
    );
  }

  Future<void> putCachedTotalPrice(
    String account,
    String price,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _keys(
        account,
      ),
      _value(
        DateTime.now(),
        price,
      ),
    );
  }

  Future<CachedPrice> getCachedTotalPrice(
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _keys(account);

    if (!box.containsKey(key)) {
      return null;
    } else {
      final value = box.get(key) as String;
      final t = int.parse(value.split(':')[0]);
      final v = value.split(':')[1].toString();
      return CachedPrice(DateTime.fromMillisecondsSinceEpoch(t * 1000), v);
    }
  }

  Future<void> putCachedTotalPrice2(
    String account,
    String price,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _keys(
        account,
      ),
      _value(
        DateTime.now(),
        price,
      ),
    );
  }

  Future<CachedPrice> getCachedTotalPrice2(
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _keys(account);

    if (!box.containsKey(key)) {
      return null;
    } else {
      final value = box.get(key) as String;
      final t = int.parse(value.split(':')[0]);
      final v = value.split(':')[1].toString();
      return CachedPrice(DateTime.fromMillisecondsSinceEpoch(t * 1000), v);
    }
  }

  Future<CachedPrice> getCachedPrice(
    TokenBlog blog,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _key(blog, account);

    if (!box.containsKey(key)) {
      return null;
    } else {
      final value = box.get(key) as String;
      final t = int.parse(value.split(':')[0]);
      final v = value.split(':')[1].toString();
      return CachedPrice(DateTime.fromMillisecondsSinceEpoch(t * 1000), v);
    }
  }
}
