// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KFavoriteTokenList = '_KFavoriteTokenList';

extension FavoriteTokenList on LocalService {
  Future<void> insertFavoriteToken(TokenBlog blog) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<TokenBlog> list = await favoriteTokens;

    /// 已经存在最近的一次记录,需要调整位置
    if (list.contains(blog)) {
      return;
    }

    box.put(KFavoriteTokenList, list..add(blog));
  }

  Future<void> putFavoriteTokens(Iterable<TokenBlog> blogs) async {
    final box = await _localDataSandBoxCompleter.future;
    box.put(KFavoriteTokenList, blogs.toList());
  }

  Future<void> removeFavoriteTokens(Iterable<TokenBlog> blogs) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      KFavoriteTokenList,
      (await favoriteTokens)
        ..removeWhere(
          (element) => blogs.contains(element),
        ),
    );
  }

  Future<void> cleanFavoriteTokens() async {
    final box = await _localDataSandBoxCompleter.future;

    return box.delete(KFavoriteTokenList);
  }

  Future<List<TokenBlog>> get favoriteTokens =>
      _localDataSandBoxCompleter.future.then(
        (box) => box.containsKey(KFavoriteTokenList)
            ? (box.get(KFavoriteTokenList) as List).cast<TokenBlog>()
            : <TokenBlog>[],
      );

  /// 根据指定地址获取代币信息，如果存在
  Future<TokenBlog> findTokenBlogWithAddress(String contractAddress) =>
      _localDataSandBoxCompleter.future.then(
        (box) => box.containsKey(KFavoriteTokenList)
            ? (box.get(KFavoriteTokenList) as List)
                .cast<TokenBlog>()
                .firstWhere((element) =>
                    element.address != null &&
                    element.address.toLowerCase() ==
                        contractAddress.toLowerCase())
            : null,
      );
}
