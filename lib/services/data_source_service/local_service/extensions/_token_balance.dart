// @dart=2.9
part of '../local_service.dart';

const KCachedBalance = '_KCachedBalance';

class CachedBalance {
  final DateTime updateTime;
  final BigInt balance;

  const CachedBalance(this.updateTime, this.balance);
}

extension TokenBalance on LocalService {
  String _key(
    TokenBlog blog,
    String account,
  ) =>
      '${KCachedBalance}_${blog.address}_$account';

  String _value(DateTime t, BigInt balance) =>
      '${t.millisecondsSinceEpoch ~/ 1000}:${balance.toRadixString(16)}';

  Future<void> putCachedBalance(
    TokenBlog blog,
    String account,
    BigInt amount,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _key(
        blog,
        account,
      ),
      _value(
        DateTime.now(),
        amount,
      ),
    );
  }

  Future<CachedBalance> getCachedBalance(
    TokenBlog blog,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _key(blog, account);

    if (!box.containsKey(key)) {
      return null;
    } else {
      final value = box.get(key) as String;
      final t = int.parse(value.split(':')[0]);
      final v = BigInt.parse(value.split(':')[1], radix: 16);
      return CachedBalance(DateTime.fromMillisecondsSinceEpoch(t * 1000), v);
    }
  }

  String _valueNft(DateTime t, BigInt balance) =>
      '${t.millisecondsSinceEpoch ~/ 1000}:${balance}';

  Future<void> putNftCachedBalance(
    TokenBlog blog,
    String account,
    BigInt amount,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _key(
        blog,
        account,
      ),
      _valueNft(
        DateTime.now(),
        amount,
      ),
    );
  }

  Future<CachedBalance> getNftCachedBalance(
    TokenBlog blog,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _key(blog, account);

    if (!box.containsKey(key)) {
      return null;
    } else {
      final value = box.get(key) as String;
      final t = int.parse(value.split(':')[0]);
      final v = BigInt.parse(value.split(':')[1]);
      return CachedBalance(DateTime.fromMillisecondsSinceEpoch(t * 1000), v);
    }
  }
}
