// @dart=2.9
part of '../local_service.dart';

const KCachedMytoken = '_KCachedMytoken';

class CachedMytoken {
  final List<TokenBlog> blog;

  const CachedMytoken(this.blog);
}

extension MyToken on LocalService {
  String _key(
    String mainSymbol,
    String account,
  ) =>
      '${KCachedMytoken}_${mainSymbol}_$account';

  // String _value(DateTime t, BigInt balance) =>
  //     '${t.millisecondsSinceEpoch ~/ 1000}:${balance.toRadixString(16)}';

  List<TokenBlog> _value(List<TokenBlog> blog) => blog;
  Future<void> putCachedBlog(
    String mainSymbol,
    String account,
    // BigInt amount,
    List<TokenBlog> blog,
  ) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      _key(
        mainSymbol,
        account,
      ),
      _value(blog),
    );
  }

  Future<void> insertMyAsset(
      String mainSymbol, String account, TokenBlog blog) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<TokenBlog> list = await getCachedBlog(mainSymbol, account);

    /// 已经存在最近的一次记录,需要调整位置
    if (list.contains(blog)) {
      return;
    }

    box.put(_key(mainSymbol, account), list..add(blog));
  }

  Future<void> cleanMyAsset() async {
    final box = await _localDataSandBoxCompleter.future;

    return box.delete(KCachedMytoken);
  }

  Future<List<TokenBlog>> getCachedBlog(
    String mainSymbol,
    String account,
  ) async {
    final box = await _localDataSandBoxCompleter.future;
    final key = _key(mainSymbol, account);

    if (!box.containsKey(key)) {
      return [];
    } else {
      final value = (box.get(key) as List).cast<TokenBlog>();

      return value;
    }
  }
}
