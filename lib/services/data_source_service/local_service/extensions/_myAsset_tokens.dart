// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KMyAssetTokenList = '_KMyAssetTokenList';

extension MyAssetTokenList on LocalService {
  Future<void> insertMyAssetToken(TokenBlog blog) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<TokenBlog> list = await myAssetTokenss;

    /// 已经存在最近的一次记录,需要调整位置
    if (list.contains(blog)) {
      return;
    }

    box.put(KMyAssetTokenList, list..add(blog));
  }

  Future<void> putMyAssetTokens(Iterable<TokenBlog> blogs) async {
    final box = await _localDataSandBoxCompleter.future;
    box.put(KMyAssetTokenList, blogs.toList());
  }

  Future<void> removeMyAssetTokens(Iterable<TokenBlog> blogs) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      KMyAssetTokenList,
      (await myAssetTokenss)
        ..removeWhere(
          (element) => blogs.contains(element),
        ),
    );
  }

  Future<void> cleanMyAssetTokens() async {
    final box = await _localDataSandBoxCompleter.future;

    return box.delete(KMyAssetTokenList);
  }

  Future<List<TokenBlog>> get myAssetTokenss =>
      _localDataSandBoxCompleter.future.then(
        (box) => box.containsKey(KMyAssetTokenList)
            ? (box.get(KMyAssetTokenList) as List).cast<TokenBlog>()
            : <TokenBlog>[],
      );

  /// 根据指定地址获取代币信息，如果存在
  // Future<TokenBlog> findTokenBlogWithAddress(String contractAddress) =>
  //     _localDataSandBoxCompleter.future.then(
  //       (box) => box.containsKey(KMyAssetTokenList)
  //           ? (box.get(KMyAssetTokenList) as List).cast<TokenBlog>().firstWhere(
  //               (element) =>
  //                   element.address != null &&
  //                   element.address.toLowerCase() ==
  //                       contractAddress.toLowerCase())
  //           : null,
  //     );
}
