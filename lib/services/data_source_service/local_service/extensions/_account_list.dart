// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KAccountList = '_KAccountList';

extension AccountList on LocalService {
  Future<List<NetworkConfig>> accountList() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KAccountList)) {
      return [];
    } else {
      return (box.get(KAccountList) as List).cast<NetworkConfig>();
    }
  }

  Future<void> accountListInsert(NetworkConfig account) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await accountList();
    if (origin.contains(account)) {
    } else {
      // origin.add(keyword);
      origin.add(account);
    }

    // if (origin.length > 20) {
    //   origin.removeAt(0);
    // }

    box.put(KAccountList, origin);
  }

  Future<void> accountListDelete(String identifier) async {
    final box = await _localDataSandBoxCompleter.future;

    final int index = box.values.first.toList().indexWhere((config) =>
        config.identifier.toLowerCase() == identifier.toLowerCase());
    final origin = await accountList();
    // // origin.removeWhere((element) => element == account);
    origin.removeAt(index);
    return box.put(KAccountList, origin);
  }

  Future<void> accountLisClean() async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(KAccountList);
  }
}
