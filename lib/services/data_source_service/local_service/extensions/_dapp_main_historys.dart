// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KDappMainHistory = '_KDappMainHistory';

extension DappMainHistory on LocalService {
  Future<List<DappDetail>> dappMainHistorylist() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KDappMainHistory)) {
      return [];
    } else {
      return (box.get(KDappMainHistory) as List).cast<DappDetail>();
    }
  }

  Future<void> dappMainHistoryInsert(DappDetail keyword) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappMainHistorylist();
    if (origin.map((e) => e.url).toList().contains(keyword.url)) {
    } else {
      // origin.add(keyword);
      origin.insert(0, keyword);
    }

    if (origin.length > 30) {
      origin.removeAt(0);
    }

    box.put(KDappMainHistory, origin);
  }

  Future<void> dappMainHistoryDelete(String url) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappMainHistorylist();

    origin.removeWhere((element) => element.url == url);
    return box.put(KDappMainHistory, origin);
  }

  Future<void> dappMainHistoryClean() async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(KDappMainHistory);
  }
}
