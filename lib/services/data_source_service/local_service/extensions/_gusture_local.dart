// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KGustureLocal = '_KGustureLocal';

extension GustureLocal on LocalService {
  Future<List<int>> gustureLocals() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KGustureLocal)) {
      return [];
    } else {
      return (box.get(KGustureLocal) as List).cast<int>();
    }
  }

  Future<void> guslocalInsert(List<int> gusture) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await gustureLocals();

    if (origin.length > 0) {
      origin.replaceRange(0, origin.length, gusture);
    }

    origin.addAll(gusture);

    // if (origin.length > 20) {
    //   origin.removeAt(0);
    // }

    box.put(KGustureLocal, origin);
  }

  // Future<void> dappHistoryDelete(String keyword) async {
  //   final box = await _localDataSandBoxCompleter.future;
  //   final origin = await dappHistorys();

  //   origin.removeWhere((element) => element == keyword);
  //   return box.put(KGustureLocal, origin);
  // }

  Future<void> guslocalClean() async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(KGustureLocal);
  }
}
