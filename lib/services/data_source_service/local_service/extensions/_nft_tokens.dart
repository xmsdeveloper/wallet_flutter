// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KNftTokenList = '_KNftTokenList';

extension NftTokenList on LocalService {
  Future<void> insertNftToken(TokenBlog blog) async {
    final box = await _localDataSandBoxCompleter.future;
    final List<TokenBlog> list = await nftTokens;

    /// 已经存在最近的一次记录,需要调整位置
    if (list.contains(blog)) {
      return;
    }

    box.put(KNftTokenList, list..add(blog));
  }

  Future<void> putNftTokens(Iterable<TokenBlog> blogs) async {
    final box = await _localDataSandBoxCompleter.future;
    box.put(KNftTokenList, blogs.toList());
  }

  Future<void> removeNftTokens(Iterable<TokenBlog> blogs) async {
    final box = await _localDataSandBoxCompleter.future;

    return box.put(
      KNftTokenList,
      (await nftTokens)
        ..removeWhere(
          (element) => blogs.contains(element),
        ),
    );
  }

  Future<void> cleanNftTokens() async {
    final box = await _localDataSandBoxCompleter.future;

    return box.delete(KNftTokenList);
  }

  Future<List<TokenBlog>> get nftTokens =>
      _localDataSandBoxCompleter.future.then(
        (box) => box.containsKey(KNftTokenList)
            ? (box.get(KNftTokenList) as List).cast<TokenBlog>()
            : <TokenBlog>[],
      );

  // /// 根据指定地址获取代币信息，如果存在
  // Future<TokenBlog> findTokenBlogWithAddress(String contractAddress) =>
  //     _localDataSandBoxCompleter.future.then(
  //       (box) => box.containsKey(KNftTokenList)
  //           ? (box.get(KNftTokenList) as List).cast<TokenBlog>().firstWhere(
  //               (element) =>
  //                   element.address != null &&
  //                   element.address.toLowerCase() ==
  //                       contractAddress.toLowerCase())
  //           : null,
  //     );
}
