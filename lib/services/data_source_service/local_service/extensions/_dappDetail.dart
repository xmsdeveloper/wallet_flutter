// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KDappCollection = '_KDappCollection';

extension DappCollection on LocalService {
  Future<List<DappDetail>> dappCollectionlist() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KDappCollection)) {
      return [];
    } else {
      return (box.get(KDappCollection) as List).cast<DappDetail>();
    }
  }

  Future<List<DappDetail>> dappCollectionTypelist() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KDappCollection)) {
      return [];
    } else {
      return (box.get(KDappCollection) as List)
          .cast<DappDetail>()
          .where((e) => e.type == "all")
          .toList();
    }
  }

  Future<void> dappCollectionInsert(DappDetail keyword) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappCollectionlist();
    if (origin.map((e) => e.url).toList().contains(keyword.url)) {
    } else {
      // origin.add(keyword);

      if (keyword.url == "defualt") {
        origin.insert(0, keyword);
      } else {
        origin.insert(1, keyword);
      }
    }

    if (origin.length > 30) {
      origin.removeAt(origin.length);
    }

    box.put(KDappCollection, origin);
  }

  Future<void> dappCollectionDelete(String url) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappCollectionlist();

    origin.removeWhere((element) => element.url == url);
    return box.put(KDappCollection, origin);
  }

  Future<void> dappCollectionTypeListDelete(String type) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await dappCollectionlist();

    origin.removeWhere((element) => element.type == type);
    return box.put(KDappCollection, origin);
  }

  Future<void> dappCollectionClean() async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(KDappCollection);
  }
}
