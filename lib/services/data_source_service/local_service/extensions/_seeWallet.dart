// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KSeeWallet = '_KSeeWallet';

extension SeeWallet on LocalService {
  Future<List<WalletKey>> seeWalletlist() async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(KSeeWallet)) {
      return [];
    } else {
      return (box.get(KSeeWallet) as List).cast<WalletKey>();
    }
  }

  Future<void> seeWalletInsert(WalletKey keyword) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await seeWalletlist();
    if (origin.map((e) => e.address).toList().contains(keyword.address)) {
    } else {
      // origin.add(keyword);
      origin.insert(0, keyword);
    }

    if (origin.length > 30) {
      origin.removeAt(origin.length);
    }

    box.put(KSeeWallet, origin);
  }

  Future<void> seeWalletDelete(String adress) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await seeWalletlist();

    origin.removeWhere((element) => element.address == adress);
    return box.put(KSeeWallet, origin);
  }

  Future<void> seeWalletClean() async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(KSeeWallet);
  }
}
