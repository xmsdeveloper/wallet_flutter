// @dart=2.9
part of '../local_service.dart';

/// 收藏的代币
const KTokenPriceList = '_KTokenPriceList';

extension TokenPriceList on LocalService {
  String _key(
    String account,
  ) =>
      '${KTokenPriceList}_$account';
  Future<List<double>> tokenPricetList(String account) async {
    final box = await _localDataSandBoxCompleter.future;

    if (!box.containsKey(_key(account))) {
      return [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ];
    } else {
      return box.get(_key(account));
    }
  }

  Future<void> tokenPriceListInsert(
      String address, int index, double tokenPrice) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await tokenPricetList(address);
    origin.removeAt(index);
    origin.insert(index, tokenPrice);
    box.put(_key(address), origin);
  }

  Future<void> tokenPriceAdd(String address) async {
    final box = await _localDataSandBoxCompleter.future;
    final origin = await tokenPricetList(address);
    origin.add(0);
    box.put(_key(address), origin);
  }

  Future<void> tokenPricetListDelete(String address, int index) async {
    final box = await _localDataSandBoxCompleter.future;

    final origin = await tokenPricetList(address);

    origin.removeAt(index);

    origin.add(0);
    return box.put(_key(address), origin);
  }

  Future<void> tokenPricetListClean(String address) async {
    final box = await _localDataSandBoxCompleter.future;
    return box.delete(_key(address));
  }
}
