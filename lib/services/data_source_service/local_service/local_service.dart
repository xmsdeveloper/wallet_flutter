// @dart=2.9

import 'dart:async';
import 'dart:math';
import 'package:hive/hive.dart';
import 'package:wallet_flutter/core/base/key.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/data_source_service/remote_service/dapp_service/service.dart';

import '../../entity/entity.dart';

part 'extensions/_favorite_tokens.dart';
part 'extensions/_recent_recipient.dart';
part 'extensions/_token_balance.dart';
part 'extensions/_dapp_search_history.dart';
part 'extensions/_account_list.dart';
part 'extensions/_dappDetail.dart';
part 'extensions/_dapp_main_historys.dart';
part 'extensions/_seeWallet.dart';
part 'extensions/_token_price.dart';
part 'extensions/_token_priceList.dart';
part 'extensions/_myAsset_tokens.dart';
part 'extensions/_gusture_local.dart';
part 'extensions/_my_tokens.dart';
part 'extensions/_nft_tokens.dart';
part 'extensions/_nft_count.dart';

class LocalService {
  final String storageKey;

  LocalService(this.storageKey) {
    /// 注册适配器
    if (!Hive.isAdapterRegistered(11)) {
      Hive.registerAdapter(RecipientAccountBlogAdapter());
    }
    if (!Hive.isAdapterRegistered(33)) {
      Hive.registerAdapter(DappDetailAdapter());
    }

    Hive.openBox(storageKey).then(_localDataSandBoxCompleter.complete);
  }

  Completer<Box> _localDataSandBoxCompleter = Completer<Box>();
}
