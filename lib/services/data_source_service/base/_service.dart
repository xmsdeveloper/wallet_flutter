// @dart=2.9
part of 'base.dart';

class ServiceHttp {
  final String _hostURL;

  final http.Client _httpClient;

  CacheManager _cacheManager;

  final _notifiers = <String, ValueNotifier>{};

  ServiceHttp({
    @required String hostURL,
    @required http.Client httpClient,
  })  : _hostURL = hostURL,
        _httpClient = httpClient {
    final String key = 'ServiceHttpCacheKey';

    _cacheManager = CacheManager(
      Config(
        key,
        stalePeriod: Duration(days: 3650000),
        maxNrOfCacheObjects: 65535,
      ),
    );
  }

  String _makeUrl(String path, Map<String, dynamic> parmas) {
    /// 拼接URL
    String url = _hostURL;

    try {
      if (path.startsWith('/')) {
        url += path;
      } else {
        url += '/$path';
      }

      parmas.forEach((key, value) {
        url += '&$key=$value';
      });

      url = url.replaceFirst('&', '?');

      url = Uri.encodeFull(url);
    } catch (e) {
      throw ServicesError(ServicesErrorCode.InvalidRequestParmas, e);
    }

    return url;
  }

  /// 用于实现在网络请求过慢时，线返回上一次的数据，在同各国ChangeNotifier更新数据的需求
  ValueNotifier<T> doGetUseChangeNotifier<T extends RemoteResponse>(
    String path,
    T response, {
    Map<String, dynamic> parmas = const {},
    Duration cacheMaxAge = const Duration(minutes: 10),
  }) {
    /// 同一个路径只允许存在一个Notifier
    if (_notifiers.containsKey(path)) {
      return _notifiers[path];
    }

    final newNotifier = ValueNotifier<T>(null);
    _notifiers[path] = newNotifier;

    /// 1.检查缓存数据，若有则先使用
    _cacheManager.getFileFromCache(_makeUrl(path, parmas)).then((cache) {
      if (cache != null) {
        newNotifier.value = response
          .._sourceType = cache.validTill.isBefore(DateTime.now())
              ? RemoteResponseSourceType.VaildCache
              : RemoteResponseSourceType.InvaildCache
          ..fromJson(
            json.decode(
              cache.file.readAsStringSync(),
            ),
          );
      }
    }).then(
      (_) => doGet(
        path,
        response,
        parmas: parmas,
        cacheMaxAge: cacheMaxAge,
      ).then((r) => newNotifier.value = r),
    );

    return newNotifier;
  }

  Future<T> doGet<T extends RemoteResponse>(
    String path,
    T response, {
    Map<String, dynamic> parmas = const {},
    Duration cacheMaxAge = const Duration(minutes: 10),
  }) async {
    /// 拼接URL
    String url = _makeUrl(path, parmas);

    print(url);

    /// 检查网络
    final connectResult = await Connectivity().checkConnectivity();
    final cache = await _cacheManager.getFileFromCache(url);
    if (connectResult == ConnectivityResult.none) {
      /// 无网络状态，一律返回缓存，如果仍然没有则抛异常
      if (cache != null) {
        response.fromJson(json.decode(await cache.file.readAsString()));
        response._sourceType = RemoteResponseSourceType.NoNetwork;
      }

      /// 无网络，无缓存
      else {
        throw ServicesError(ServicesErrorCode.OfflineAndNoCache);
      }
    }

    /// 在线状态
    else {
      /// 获取Header中的ETag字段，判断数据的修改情况
      // final headerResponse = await _httpClient.head(url);
      // final eTag = headerResponse.headers.containsKey('ETag')
      //     ? headerResponse.headers['ETag']
      //     : null;

      if (cache != null && cache.validTill.isBefore(DateTime.now())) {
        response.fromJson(json.decode(await cache.file.readAsString()));
        response._sourceType = RemoteResponseSourceType.VaildCache;
      } else {
        final httpResponse = await _httpClient.get(url);
        if (httpResponse.statusCode != 200) {
          throw ServicesError(
            ServicesErrorCode.ResponseHttpStateError,
            {
              'statusCode': httpResponse.statusCode,
              'body': httpResponse.body,
            },
          );
        }

        /// 成功请求,测试转换对象,如果若不能转换对象，说明返回有问题，则不记录缓存
        try {
          response.fromJson(json.decode(httpResponse.body));
          response._sourceType = RemoteResponseSourceType.Remote;

          /// 若缓存时间设置为zero表示不需要缓存,一律使用网络的结果。比如一些实效性比较高的
          /// 接口，可以定义为Duration.zero
          if (cacheMaxAge > Duration.zero && response.errorCode == 0) {
            _cacheManager.putFile(
              url,
              utf8.encode(
                httpResponse.body,
              ),
              maxAge: cacheMaxAge,
            );
          }
        } catch (e) {
          throw ServicesError(ServicesErrorCode.DecodeResponseError, e);
        }
      }
    }

    return response;
  }

  // Future<T> doPost<T extends RemoteResponse>(String url, T response) async {
  //   final httpResponse = await httpClient.post(url);
  //   if (httpResponse.statusCode != 200) {
  //     throw ServicesError(
  //       ServicesErrorCode.ResponseHttpStateError,
  //       {
  //         'statusCode': httpResponse.statusCode,
  //         'body': httpResponse.body,
  //       },
  //     );
  //   }

  //   try {
  //     return response..fromJson(json.decode(httpResponse.body));
  //   } catch (e) {
  //     throw ServicesError(ServicesErrorCode.DecodeResponseError, e);
  //   }
  // }
}
