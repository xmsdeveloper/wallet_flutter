// @dart=2.9
part of 'base.dart';

enum ServicesErrorCode {
  DecodeResponseError,
  ResponseHttpStateError,
  InvalidRequestParmas,
  OfflineAndNoCache,
}

class ServicesError {
  final ServicesErrorCode code;
  final dynamic reason;

  const ServicesError(this.code, [this.reason]);

  @override
  int get hashCode => code.index;

  @override
  bool operator ==(Object other) =>
      other is ServicesError && other.code == this.code;

  @override
  String toString() => '('
      'code: ${code.toString}, '
      'reason: ${reason.toString}'
      ')';
}
