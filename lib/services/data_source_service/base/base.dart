// @dart=2.9
library qytechnology.package.wallet.services.base;

import 'dart:convert';
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:connectivity/connectivity.dart';
import 'package:wallet_flutter/services/entity/entity.dart';

part '_response.dart';
part '_service.dart';
part '_services_error.dart';
