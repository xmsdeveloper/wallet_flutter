// @dart=2.9
part of 'base.dart';

/// 远程服务器相应结果的数据源
enum RemoteResponseSourceType {
  /// 来自有效的缓存，即对应的接口上一次请求在定义的缓存时间内
  VaildCache,

  /// 来自无效的缓存，即对应的接口上一次请求在定义的缓存时间外,但有可能需要线使用已过期的缓存数据
  InvaildCache,

  /// 无网络情况，用老数据进行了返回,情况上来说，因为没有新数据返回，只能使用缓存，说明对应的返回，
  /// 可能已经过期。
  NoNetwork,

  /// 来自服务器，即真实的请求了服务器获取了结果
  Remote
}

abstract class RemoteResponse implements JsonSerialize {
  int errorCode;
  String reason;
  String time;
  RemoteResponseSourceType _sourceType;

  RemoteResponseSourceType get sourceType => _sourceType;

  void fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    reason = json['reason'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() => {
        'errorCode': errorCode,
        'reason': reason,
        'time': time,
      };
}

typedef ResponseDataCreater<T extends JsonSerialize> = T Function();

class ResponseList<T extends JsonSerialize> extends RemoteResponse {
  List<T> data = [];
  final ResponseDataCreater<T> create;

  ResponseList({
    @required this.create,
  });

  @override
  void fromJson(Map<String, dynamic> json) {
    super.fromJson(json);
    data = [];
    if (!json.containsKey('data') || json.containsKey('data') == null) {
      return;
    }

    json['data'].forEach((e) {
      data.add(create()..fromJson(e));
    });
  }

  @override
  Map<String, dynamic> toJson() {
    final ret = super.toJson();
    ret['data'] = data.map((e) => e.toJson()).toList();
    return ret;
  }
}

class ResponseObject<T extends JsonSerialize> extends RemoteResponse {
  T data;

  ResponseObject({
    @required this.data,
  });

  @override
  void fromJson(Map<String, dynamic> json) {
    super.fromJson(json);

    if (!json.containsKey('data') || json.containsKey('data') == null) {
      return;
    }

    data.fromJson(json['data']);
  }

  @override
  Map<String, dynamic> toJson() {
    final ret = super.toJson();
    ret['data'] = data.toJson();
    return ret;
  }
}
