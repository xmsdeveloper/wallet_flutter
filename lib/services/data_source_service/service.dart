// @dart=2.9
import 'dart:async';
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wallet_core_flutter/utils/utils.dart';

import 'package:wallet_flutter/services/data_source_service/remote_service/remote_service.dart';
import 'package:wallet_flutter/services/data_source_service/local_service/local_service.dart';

export 'base/base.dart';
export 'package:wallet_flutter/services/data_source_service/remote_service/remote_service.dart';
export 'package:wallet_flutter/services/data_source_service/local_service/local_service.dart';

class DataSourceProvider extends ChangeNotifier {
  RemoteServices remoteServices;
  LocalService localService;

  static Future<void> init() async {
    await RemoteServices.init();
  }

  factory DataSourceProvider.of(BuildContext context) =>
      Provider.of<DataSourceProvider>(
        context,
        listen: false,
      );

  DataSourceProvider({
    @required String walletIdentifier,
    @required String networkIdentifier,
  }) {
    remoteServices = RemoteServices(
      _storageIdentifier(walletIdentifier, networkIdentifier, 'Remote'),
    );

    localService = LocalService(
      _storageIdentifier(walletIdentifier, networkIdentifier, 'Local'),
    );
  }

  String _storageIdentifier(String wid, String nid, String suffix) =>
      'DataSourceProviderSandBox_$suffix' +
      bytesToHex(sha256.convert(utf8.encode(wid + nid)).bytes);
}
