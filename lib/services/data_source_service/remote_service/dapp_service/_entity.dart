// @dart=2.9
part of 'service.dart';

class Dapp implements JsonSerialize {
  String name;
  String url;
  String des;
  String logo;

  Dapp({this.name, this.url, this.des, this.logo});

  void fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
    des = json['des'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['url'] = this.url;
    data['des'] = this.des;
    data['logo'] = this.logo;
    return data;
  }
}

class DappBanner implements JsonSerialize {
  String image;
  String url;

  DappBanner({this.image, this.url});

  void fromJson(Map<String, dynamic> json) {
    image = json['image'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['url'] = this.url;
    return data;
  }
}

class DappTypeList implements JsonSerialize {
  String type;
  List<Dapp> items;

  @override
  void fromJson(Map<String, dynamic> json) {
    type = json['type'];

    if (json.containsKey('items')) {
      items = [];

      json['items'].forEach((e) {
        items.add(Dapp()..fromJson(e));
      });
    }
  }

  @override
  Map<String, dynamic> toJson() => {
        'type': type,
        'items': items.map((e) => e.toJson()).toList(),
      };
}

class DappIndexResponse implements JsonSerialize {
  List<DappBanner> banners;
  List<DappTypeList> dapps;

  @override
  void fromJson(Map<String, dynamic> json) {
    if (json.containsKey('banners')) {
      banners = [];
      json['banners'].forEach((e) {
        banners.add(DappBanner()..fromJson(e));
      });
    }

    if (json.containsKey('dapps')) {
      dapps = [];

      json['dapps'].forEach((e) {
        dapps.add(DappTypeList()..fromJson(e));
      });
    }
  }

  @override
  Map<String, dynamic> toJson() => {
        'banners': banners.map((e) => e.toJson()).toList(),
        'dapps': dapps.map((e) => e.toJson()).toList(),
      };
}
