// @dart=2.9
library qytechnology.package.wallet.services.dapp;

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wallet_flutter/services/entity/entity.dart';

import '../../base/base.dart';

part '_entity.dart';

part '_http_impl.dart';
part '_mock_impl.dart';

abstract class DappService {
  ValueNotifier<ResponseObject<DappIndexResponse>> indexListnable();
  ValueNotifier<ResponseObject<DappIndexResponse>> indexListnableen();

  /// 搜索dapp
  Future<ResponseList<Dapp>> searchDappList({@required String keyword});

  /// 获取热门dapp
  Future<ResponseList<Dapp>> hotlist();
}
