// @dart=2.9
part of 'service.dart';

class DappServiceMock extends ServiceHttp implements DappService {
  DappServiceMock({
    @required String hostURL,
    @required http.Client httpClient,
  }) : super(hostURL: hostURL, httpClient: httpClient);

  @override
  ValueNotifier<ResponseObject<DappIndexResponse>> indexListnable() =>
      ValueNotifier<ResponseObject<DappIndexResponse>>(
        ResponseObject<DappIndexResponse>(data: DappIndexResponse())
          ..fromJson(
            json.decode('''
{
    "errorCode": 0,
    "reason": "",
    "data": {
        "banners": [
            {
                "image": "http://hashpay.gitee.io/hashpay/imgs/dapp/banner/01.jpg",
                "url": ""
            },
            {
                "image": "http://hashpay.gitee.io/hashpay/imgs/dapp/banner/02.jpg",
                "url": ""
            },
            {
                "image": "http://hashpay.gitee.io/hashpay/imgs/dapp/banner/03.jpg",
                "url": ""
            }
        ],
        "dapps": [
            {
                "type": "游戏",
                "items": [
                    {
                        "name": "UniSwap",
                        "url": "https://app.uniswap.org/#/swap",
                        "des": "Ethereum 主链Uniswap",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/defieraLogo.png"
                    },
                    {
                        "name": "Lucky Charma",
                        "url": "http://hashpay.gitee.io/hashpay/dapps/luckycharms/index.html",
                        "des": "一款区块链上的幸运抽奖回馈游戏",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/luckywin.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    }
                ]
            },
            {
                "type": "交易所",
                "items": [
                    {
                        "name": "DeFi Era",
                        "url": "http://defiera.gitee.io/#/index",
                        "des": "一个公平、智能的互助系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/defieraLogo.png"
                    },
                    {
                        "name": "Lucky Charma",
                        "url": "http://hashpay.gitee.io/hashpay/dapps/luckycharms/index.html",
                        "des": "一款区块链上的幸运抽奖回馈游戏",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/luckywin.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    }
                ]
            },
            {
                "type": "DeFi",
                "items": [
                    {
                        "name": "DeFi Era",
                        "url": "http://defiera.gitee.io/#/index",
                        "des": "一个公平、智能的互助系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/defieraLogo.png"
                    },
                    {
                        "name": "Lucky Charma",
                        "url": "http://hashpay.gitee.io/hashpay/dapps/luckycharms/index.html",
                        "des": "一款区块链上的幸运抽奖回馈游戏",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/luckywin.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    }
                ]
            }
        ]
    }
}
        '''),
          ),
      );

  @override
  ValueNotifier<ResponseObject<DappIndexResponse>> indexListnableen() =>
      ValueNotifier<ResponseObject<DappIndexResponse>>(
        ResponseObject<DappIndexResponse>(data: DappIndexResponse())
          ..fromJson(
            json.decode('''
{
    "errorCode": 0,
    "reason": "",
    "data": {
        "banners": [
            {
                "image": "http://hashpay.gitee.io/hashpay/imgs/dapp/banner/01.jpg",
                "url": ""
            },
            {
                "image": "http://hashpay.gitee.io/hashpay/imgs/dapp/banner/02.jpg",
                "url": ""
            },
            {
                "image": "http://hashpay.gitee.io/hashpay/imgs/dapp/banner/03.jpg",
                "url": ""
            }
        ],
        "dapps": [
            {
                "type": "游戏",
                "items": [
                    {
                        "name": "UniSwap",
                        "url": "https://app.uniswap.org/#/swap",
                        "des": "Ethereum 主链Uniswap",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/defieraLogo.png"
                    },
                    {
                        "name": "Lucky Charma",
                        "url": "http://hashpay.gitee.io/hashpay/dapps/luckycharms/index.html",
                        "des": "一款区块链上的幸运抽奖回馈游戏",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/luckywin.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    }
                ]
            },
            {
                "type": "交易所",
                "items": [
                    {
                        "name": "DeFi Era",
                        "url": "http://defiera.gitee.io/#/index",
                        "des": "一个公平、智能的互助系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/defieraLogo.png"
                    },
                    {
                        "name": "Lucky Charma",
                        "url": "http://hashpay.gitee.io/hashpay/dapps/luckycharms/index.html",
                        "des": "一款区块链上的幸运抽奖回馈游戏",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/luckywin.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    }
                ]
            },
            {
                "type": "DeFi",
                "items": [
                    {
                        "name": "DeFi Era",
                        "url": "http://defiera.gitee.io/#/index",
                        "des": "一个公平、智能的互助系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/defieraLogo.png"
                    },
                    {
                        "name": "Lucky Charma",
                        "url": "http://hashpay.gitee.io/hashpay/dapps/luckycharms/index.html",
                        "des": "一款区块链上的幸运抽奖回馈游戏",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/luckywin.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    },
                    {
                        "name": "Free Cash",
                        "url": "http://freecash.gitee.io/",
                        "des": "一个去中心化的财务（DeFi）分配系统",
                        "logo": "http://hashpay.gitee.io/hashpay/imgs/dapp/logo/freecash.png"
                    }
                ]
            }
        ]
    }
}
        '''),
          ),
      );

  Future<ResponseList<Dapp>> searchDappList({@required String keyword}) {}

  Future<ResponseList<Dapp>> hotlist({@required String keyword}) {}
}
