// @dart=2.9
part of 'service.dart';

class DappServiceHttp extends ServiceHttp implements DappService {
  DappServiceHttp({
    @required String hostURL,
    @required http.Client httpClient,
  }) : super(hostURL: hostURL, httpClient: httpClient);

  // @override
  // ValueNotifier<ResponseObject<DappIndexResponse>> indexListnable() =>
  //     doGetUseChangeNotifier(
  //       '/dapps/index',
  //       ResponseObject<DappIndexResponse>(data: DappIndexResponse()),
  //     );
  @override
  ValueNotifier<ResponseObject<DappIndexResponse>> indexListnable() =>
      doGetUseChangeNotifier(
        '/index_dapps.json',
        ResponseObject<DappIndexResponse>(data: DappIndexResponse()),
      );
  @override
  ValueNotifier<ResponseObject<DappIndexResponse>> indexListnableen() =>
      doGetUseChangeNotifier(
        '/index_dapps_en.json',
        ResponseObject<DappIndexResponse>(data: DappIndexResponse()),
      );

//搜索dapp
  @override
  Future<ResponseList<Dapp>> searchDappList({String keyword}) => doGet(
        '/dapps/search',
        ResponseList<Dapp>(
          create: () => Dapp(),
        ),
        parmas: {
          'keyword': keyword,
        },
        cacheMaxAge: Duration.zero,
      );

  //热门dapp
  @override
  Future<ResponseList<Dapp>> hotlist() => doGet(
        '/dapps/hotlist',
        ResponseList<Dapp>(
          create: () => Dapp(),
        ),
      );
}
