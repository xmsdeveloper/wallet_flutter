// @dart=2.9
import 'dart:async';

import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:wallet_flutter/core/core.dart';

import 'package:wallet_flutter/services/entity/entity.dart';

import 'token_service/service.dart';
import 'dapp_service/service.dart';

export 'token_service/service.dart';
export 'dapp_service/service.dart';

class _RemoteServicesClient extends http.BaseClient {
  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    /// 国家编码对照表：http://www.loglogo.com/front/countryCode
    /// 语言编码对照表：http://www.lingoes.cn/zh/translator/langcode.htm

    if (ConfigStorageUtils().read<String>(
          ConfigKey.LanguageCode,
        ) ==
        "zh") {
      request.headers['location'] = 'cn';
      request.headers['language'] = 'zh-cn';
    } else if (ConfigStorageUtils().read<String>(
          ConfigKey.LanguageCode,
        ) ==
        "en") {
      request.headers['location'] = 'us';
      request.headers['language'] = 'en';
    } else {
      request.headers['location'] = 'cn';
      request.headers['language'] = 'zh-cn';
    }

    return request.send();
  }
}

class RemoteServices {
  final String storageKey;
  TokenService token;
  DappService dapps;
  _RemoteServicesClient client = _RemoteServicesClient();

  /// 所有RemoteServices产生的数据存储共享一个沙河
  Completer<Box> _walletDataSandBox = Completer();

  RemoteServices(this.storageKey) {
    Hive.openBox(storageKey).then(_walletDataSandBox.complete);
  }

  static Future<void> init() async {
    Hive.registerAdapter(TokenBlogAdapter());
  }

  void setEndpoint(EndPoint endPoint) {
    token = TokenServiceHttp(
      hostURL: endPoint.resourcesServicesURL,
      httpClient: client,
    );

    dapps = DappServiceHttp(
      hostURL: endPoint.resourcesServicesURL,
      httpClient: client,
    );
  }
}
