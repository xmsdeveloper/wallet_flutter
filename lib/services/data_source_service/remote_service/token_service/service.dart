// @dart=2.9
library qytechnology.package.wallet.services.token;

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../base/base.dart';
import '../../../entity/entity.dart';

part '_http_impl.dart';
part '_mock_impl.dart';

abstract class TokenService {
  /// 当前ChainType标准资产列表，也就是默认资产列表
  Future<ResponseList<TokenBlog>> requestStandard();

  /// 搜索代币
  Future<ResponseList<TokenBlog>> requestSearch({@required String keyword});

  /// 热门代币
  Future<ResponseList<TokenBlog>> requestHot();

  /// 获取指定代币的详细信息
  Future<ResponseList<TokenDetail>> requestDetail({@required String address});
}
