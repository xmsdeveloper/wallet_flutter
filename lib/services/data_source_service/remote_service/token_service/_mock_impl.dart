// @dart=2.9
part of 'service.dart';

class TokenServiceMock extends ServiceHttp implements TokenService {
  TokenServiceMock({
    @required String hostURL,
    @required http.Client httpClient,
  }) : super(hostURL: hostURL, httpClient: httpClient);

  Future<ResponseList<TokenBlog>> requestStandard() => Future.value(
        ResponseList<TokenBlog>(create: () => TokenBlog())
          ..fromJson(
            json.decode('''
{
    "errorCode": 0,
    "reason": "",
    "data": [
        {
            "symbol": "DT",
            "address": "",
            "name": "Boundary Network Token",
            "decimals": 18,
            "logo": "https://s2.bqiapp.com/logo/1/ethereum.png?x-oss-process=style/coin_72"
        },
        {
            "symbol": "USDT",
            "address": "0xbb1B314D2154ba31292153b0C1028C5eE2B28553",
            "name": "Tether USD (DRC20)",
            "decimals": 6,
            "logo": "https://s2.bqiapp.com/logo/1/tether.png?x-oss-process=style/coin_72"
        }
    ]
}
'''),
          ),
      );

  @override
  Future<ResponseList<TokenDetail>> requestDetail({String address}) {
    throw UnimplementedError();
  }

  @override
  Future<ResponseList<TokenBlog>> requestHot() {
    throw UnimplementedError();
  }

  @override
  Future<ResponseList<TokenBlog>> requestSearch({String keyword}) {
    throw UnimplementedError();
  }

  Future<ResponseList<TokenDetail>> requestDetails({String address}) {
    throw UnimplementedError();
  }
}
