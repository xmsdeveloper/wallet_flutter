// @dart=2.9
part of 'service.dart';

class TokenServiceHttp extends ServiceHttp implements TokenService {
  TokenServiceHttp({
    @required String hostURL,
    @required http.Client httpClient,
  }) : super(
          hostURL: hostURL,
          httpClient: httpClient,
        );

  // Future<ResponseList<TokenBlog>> requestStandard() => doGet(
  //       '/tokens/standard',
  //       ResponseList<TokenBlog>(
  //         create: () => TokenBlog(),
  //       ),
  //     );
  Future<ResponseList<TokenBlog>> requestStandard() => doGet(
        '/standard_tokens.json',
        ResponseList<TokenBlog>(
          create: () => TokenBlog(),
        ),
      );

  @override
  Future<ResponseList<TokenDetail>> requestDetail({String address}) => doGet(
        '/tokens/detail',
        ResponseList<TokenDetail>(
          create: () => TokenDetail(),
        ),
        parmas: {
          'address': address,
        },
      );

  // @override
  // Future<ResponseList<TokenBlog>> requestHot({String address}) => doGet(
  //       '/tokens/hotlist',
  //       ResponseList<TokenBlog>(
  //         create: () => TokenBlog(),
  //       ),
  //     );
  @override
  Future<ResponseList<TokenBlog>> requestHot({String address}) => doGet(
        '/hot_tokens.json',
        ResponseList<TokenBlog>(
          create: () => TokenBlog(),
        ),
      );

  @override
  Future<ResponseList<TokenBlog>> requestSearch({String keyword}) => doGet(
        '/tokens/search',
        ResponseList<TokenBlog>(
          create: () => TokenBlog(),
        ),
        parmas: {
          'keyword': keyword,
        },
        cacheMaxAge: Duration.zero,
      );
}
