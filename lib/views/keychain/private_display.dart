// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

enum SecurityType {
  PrivateKeyHex,
  KeystoreJson,
  Mnemonics,
}

class PrivateKeyDisplay extends ViewController {
  final WalletKey walletKey;
  final SecurityType securityType;
  final String encryptPassword;

  final Wallet wallet;
  List tyoe = [
    S.current.common_private_key,
    "keystore",
    S.current.wallet_import_type_mnemonic,
  ];
  final tips = [
    {
      'title': S.current.wallet_export_tip0_title,
      'desc': S.current.wallet_export_tip0_desc,
    },
    {
      'title': S.current.wallet_export_tip1_title,
      'desc': S.current.wallet_export_tip1_desc,
    },
    {
      'title': S.current.wallet_export_tip2_title,
      'desc': S.current.wallet_export_tip2_desc,
    }
  ];

  final ValueNotifier<String> securityContentListenable =
      ValueNotifier<String>(null);
  ValueNotifier<bool> isDispaly = ValueNotifier<bool>(false);

  factory PrivateKeyDisplay.rootWallet({
    @required Wallet wallet,
    @required String encryptPassword,
    SecurityType securityType = SecurityType.Mnemonics,
  }) =>
      PrivateKeyDisplay(
        walletKey: null,
        wallet: wallet,
        securityType: securityType,
        encryptPassword: encryptPassword,
      );

  PrivateKeyDisplay({
    this.walletKey,
    this.wallet,
    @required this.encryptPassword,
    this.securityType = SecurityType.PrivateKeyHex,
  }) : super(
          navigationBar: NavigationBar(
            backgroundColor: Colors.white,
            // title: '',
            title: securityType.index > 0
                ? securityType.index > 1
                    ? S.current.wallet_import_type_mnemonic
                    : "keystone"
                : S.current.common_private_key,
            elevation: 0.3,
          ),
        ) {
    if (this.walletKey != null) {
      assert(
        this.securityType != SecurityType.Mnemonics,
        "sub key not support export mnemonics.",
      );
    }
  }

  Future<void> exportSecurityAndDisplay() async {
    switch (securityType) {
      case SecurityType.PrivateKeyHex:
        securityContentListenable.value = bytesToHex(walletKey.privateKeyRaw);
        break;

      case SecurityType.KeystoreJson:
        securityContentListenable.value =
            await walletKey.encryptKeypairToJson(passphrase: encryptPassword);
        break;

      case SecurityType.Mnemonics:
        securityContentListenable.value = wallet.mnemonic;
        break;
    }
    return;
  }

  Widget header(BuildContext context) => Container(
        color: ThemeUtils().getColor(
          'utils.navigation_bar.background',
        ),
        height: ScreenUtil().setHeight(200),
        width: double.infinity,
        child: Column(
          children: [
            Icon(
              Icons.security_sharp,
              size: ScreenUtil().setHeight(100),
              color: ThemeUtils().getColor(
                'utils.navigation_bar.icon',
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(20),
                bottom: ScreenUtil().setHeight(10),
              ),
              child: Text(
                walletKey != null
                    ? walletKey.nickName
                    : wallet != null
                        ? wallet.walletName
                        : '',
                style: ThemeUtils().getTextStyle(
                  'views.keychain.exporter.textstyle.wallet_name',
                ),
              ),
            ),
            if (walletKey != null)
              Text(
                walletKey.address,
                style: ThemeUtils().getTextStyle(
                  'views.keychain.exporter.textstyle.address',
                ),
              )
          ],
        ),
      );

  Widget securityView(BuildContext context) => Column(
        children: [
          Container(
            // height: ScreenUtil().setHeight(73),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color.fromRGBO(252, 239, 230, 1),
            ),
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(24),
              left: ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(20),
            ),
            width: double.infinity,
            child: Row(
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  padding: EdgeInsets.all(ScreenUtil().setSp(16)),
                  height: ScreenUtil().setHeight(73),
                  child: Image.asset(
                    "assets/images/warming.png",
                    height: ScreenUtil().setHeight(22.5),
                    width: ScreenUtil().setWidth(22.5),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: ScreenUtil().setHeight(17),
                    right: ScreenUtil().setWidth(21),
                    bottom: ScreenUtil().setHeight(19),
                  ),
                  width: ScreenUtil().setWidth(252),
                  child: Text(
                    wallet == null
                        ? S.current.wallet_export_warrning_private_key
                        : S.current.wallet_export_warrning,
                    textAlign: TextAlign.left,
                    maxLines: 4,
                    // style: ThemeUtils().getTextStyle(
                    //   'views.keychain.exporter.textstyle.warning',
                    // ),
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(231, 136, 77, 1)),
                  ),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              isDispaly.value = !isDispaly.value;
            },
            child: Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(20),
              ),
              width: ScreenUtil().setWidth(188),
              height: ScreenUtil().setHeight(188),
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //     image: AssetImage(
              //       'assets/imgs/qr_codebg.png',
              //       package: "wallet_flutter",
              //     ),
              //     fit: BoxFit.fill,
              //   ),
              // ),
              child: Stack(
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: QrImage(
                      padding: EdgeInsets.all(8),
                      data: securityContentListenable.value,
                      version: QrVersions.auto,
                      size: ScreenUtil().setSp(184),
                    ),
                  ),
                  ValueListenableBuilder(
                      valueListenable: isDispaly,
                      builder: (context, value, child) {
                        return isDispaly.value == false
                            ? Container(
                                width: ScreenUtil().setWidth(188),
                                height: ScreenUtil().setHeight(188),
                                color: Color.fromRGBO(175, 175, 175, 1),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/key_eyes.png',
                                      width: ScreenUtil().setWidth(39.66),
                                      height: ScreenUtil().setHeight(37.36),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: ScreenUtil().setHeight(21),
                                      ),
                                      child: Center(
                                        child: Text(
                                          S.current.ethereum_home_qrcode_tip1,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(12),
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Center(
                                        child: Text(
                                          S.current.ethereum_home_qrcode_tip2,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(12),
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ))
                            : Container();
                      })
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () async {
              Clipboard.setData(
                ClipboardData(
                  text: securityContentListenable.value,
                ),
              );

              Fluttertoast.showToast(
                msg: S.current.copy_success,
              );
            },
            child: Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(29),
                left: ScreenUtil().setWidth(22),
                right: ScreenUtil().setWidth(22),
              ),
              decoration: BoxDecoration(
                color: Color.fromRGBO(250, 250, 250, 1),
                border: Border.all(
                    width: ScreenUtil().setWidth(1),
                    color: Color.fromRGBO(216, 216, 216, 1)),
                borderRadius: BorderRadius.circular(
                  ScreenUtil().setWidth(5),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(12),
                            left: ScreenUtil().setWidth(11),
                            right: ScreenUtil().setWidth(11),
                            bottom: ScreenUtil().setHeight(10),
                          ),
                          child: Text(
                            tyoe[securityType.index],
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              color: Color.fromRGBO(175, 175, 175, 1),
                            ),
                          ),
                        ),
                        Container(
                          height: ScreenUtil().setHeight(11),
                          width: ScreenUtil().setWidth(12),
                          child: Image.asset(
                            'assets/imgs/wallet_copy_address.png',
                            package: 'wallet_flutter',
                            color: Colors.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(11),
                      bottom: ScreenUtil().setHeight(60),
                    ),
                    child: Text(
                      securityContentListenable.value,
                      maxLines: 10,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),

          // Expanded(child: SizedBox()),
          SizedBox(
            height: ScreenUtil().setHeight(20),
          ),
          Container(
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(20),
              bottom: ScreenUtil().setHeight(30),
            ),
            child: StatusButton.withStyle(
              StatusButtonStyle.Info,
              title: S.current.wallet_export_done,
              onPressedFuture: () async {
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      );

  Widget tipsView(BuildContext context) => Column(
        children: [
          // header(context),
        ]..addAll(
            tips
                .map<Widget>(
                  (e) => Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(20),
                      right: ScreenUtil().setWidth(20),
                      top: ScreenUtil().setHeight(30),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          e['title'],
                          style: ThemeUtils().getTextStyle(
                            'views.keychain.exporter.textstyle.tips_title',
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(15),
                        ),
                        Text(
                          e['desc'],
                          style: ThemeUtils().getTextStyle(
                              'views.keychain.exporter.textstyle.tips_desc'),
                        ),
                      ],
                    ),
                  ),
                )
                .toList()
                  ..add(
                    SizedBox(
                      height: ScreenUtil().setHeight(20),
                    ),
                  )
                  ..add(
                    Container(
                      margin: EdgeInsets.only(
                        left: ScreenUtil().setWidth(20),
                        right: ScreenUtil().setWidth(20),
                        bottom: ScreenUtil().setHeight(30),
                      ),
                      child: StatusButton.withStyle(
                        StatusButtonStyle.Info,
                        title: S.current.wallet_export_show_security,
                        onPressedFuture: exportSecurityAndDisplay,
                      ),
                    ),
                  ),
          ),
      );

  @override
  Widget render(BuildContext context) => CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: securityContentListenable,
              builder: (context, value, child) =>
                  value != null ? securityView(context) : tipsView(context),
            ),
          ),
        ],
      );
}
