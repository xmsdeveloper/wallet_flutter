// @dart=2.9
part of '../importer.dart';

enum ImportType {
  PrivateKey,
  Mnemonic,
  Keystore,
  Adress,
}

class ImportWalletView extends StatefulWidget {
  final List<ImportType> importTypes;
  final DataSourceProvider dataSourceProvider;

  ImportWalletView({
    this.dataSourceProvider,
    this.importTypes = const [
      ImportType.PrivateKey,
      ImportType.Mnemonic,
      ImportType.Keystore,
      ImportType.Adress,
    ],
  });

  @override
  State<StatefulWidget> createState() => _ImportWalletViewState();
}

class _ImportWalletViewState extends State<ImportWalletView>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  List<Widget> subViews = [];
  Map<ImportType, String> importTypeTitle = {
    ImportType.PrivateKey: S.current.wallet_import_type_privatekey,
    ImportType.Mnemonic: S.current.wallet_import_type_mnemonic,
    ImportType.Keystore: S.current.wallet_import_type_Keystore,
    ImportType.Adress: "Adress",
  };

  ValueNotifier<String> qrCodeInfomation = ValueNotifier<String>(null);
  var key;
  void initState() {
    super.initState();
    tabController = TabController(
      initialIndex: 0,
      length: widget.importTypes.length,
      vsync: this,
    );
    subViews = widget.importTypes.map((e) {
      switch (e) {
        case ImportType.PrivateKey:
          return ValueListenableBuilder(
              valueListenable: qrCodeInfomation,
              builder: (context, value, child) {
                return _PrivateKeyImportPage(
                  qrCodeInfomation.value,
                );
              });

        case ImportType.Mnemonic:
          return _MnemonicImportPage();

        case ImportType.Keystore:
          return ValueListenableBuilder(
              valueListenable: qrCodeInfomation,
              builder: (context, value, child) {
                return _KeystoreImportPage(
                  qrCodeInfomation.value,
                );
              });

        case ImportType.Adress:
          return ValueListenableBuilder(
              valueListenable: qrCodeInfomation,
              builder: (context, value, child) {
                return SeeWalletImport(
                  dataSourceProvider: this.widget.dataSourceProvider,
                  keys: qrCodeInfomation.value,
                );
              });
      }
    }).toList();
  }

  PreferredSize segmentBar() => PreferredSize(
        child: Container(
          child: Center(
            child: TabBar(
              onTap: (idx) {
                setState(() {});
              },
              tabs: widget.importTypes
                  .map(
                    (e) => importTypeTitle[e] != "Adress"
                        ? Container(
                            width: ScreenUtil().setWidth(80),
                            alignment: Alignment.center,
                            height: ScreenUtil().setHeight(40),
                            child: Text(
                              importTypeTitle[e],
                            ),
                          )
                        : Container(
                            width: 0,
                          ),
                  )
                  .toList(),
              controller: tabController,
              isScrollable: false,
              indicatorWeight: 1,
              // indicatorColor: ThemeUtils()
              //     .getColor('views.keychain.import.tabbar.indicator_color'),
              indicatorColor: Colors.blue,
              indicatorSize: TabBarIndicatorSize.label,
              labelPadding: EdgeInsets.all(0),
              labelColor: Colors.black,
              // ThemeUtils()
              //     .getColor('views.keychain.import.tabbar.label_color'),
              unselectedLabelColor: Colors.grey[400],
              // ThemeUtils().getColor(
              //     'views.keychain.import.tabbar.unselected_label_color'),
              labelStyle: ThemeUtils()
                  .getTextStyle('views.keychain.import.tabbar.textstyle.label'),
              unselectedLabelStyle: ThemeUtils().getTextStyle(
                  'views.keychain.import.tabbar.textstyle.unselected_label'),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(
          ScreenUtil().setHeight(40),
        ),
      );

  /*等待返回数据是异步操作*/
  void skipToPageD() async {
    // final result = await Navigator.pushNamed(context, pageD);
    final result = await Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        ScanPage(),
      ),
    );

    String p = result as String;
    key = p;

    print(key + ":fanhui");

    qrCodeInfomation.value = key;

    setState(() {});
    // this.addressEditingController.text = p;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        // backgroundColor: ThemeUtils().getColor(
        //   'utils.view_controller.background',
        // ),
        appBar: NavigationBar(
          title: widget.importTypes.contains(ImportType.Adress)
              ? S.current.wallet_watch_name
              : S.current.wallet_import_title,
          actions: [
            widget.importTypes.first.index != 1
                ? InkWell(
                    onTap: () async {
                      skipToPageD();
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                        right: ScreenUtil().setWidth(20),
                      ),
                      height: ScreenUtil().setHeight(20),
                      width: ScreenUtil().setWidth(20),
                      child: Image.asset(
                        'assets/images/saoma.png',
                      ),
                    ),
                  )
                : Container()
          ],
          // bottom: widget.importTypes.contains(ImportType.Mnemonic) ||
          //         widget.importTypes.contains(ImportType.Adress)
          //     ? null
          //     : segmentBar(),
          elevation: 0.5,
        ),
        backgroundColor: Colors.white,
        body: subViews[tabController.index],
      );
}
