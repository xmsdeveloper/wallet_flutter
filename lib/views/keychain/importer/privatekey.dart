// @dart=2.9
part of '../importer.dart';

// class _PrivateKeyImportPage extends StatelessWidget {}

class _PrivateKeyImportPage extends StatefulWidget {
  final String keys;
  const _PrivateKeyImportPage(this.keys);

  @override
  State<_PrivateKeyImportPage> createState() => __PrivateKeyImportPageState();
}

class __PrivateKeyImportPageState extends State<_PrivateKeyImportPage> {
  final TextEditingController privateKeyEditController =
      TextEditingController();
  final TextEditingController walletNameEditController =
      TextEditingController();

  final ValueNotifier<String> privateKeyEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> walletNameEditErrorListenable =
      ValueNotifier<String>(null);

  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");

  @override
  void initState() {
    super.initState();
    setState(() {
      privateKeyEditErrorListenable.value = widget.keys;
    });
  }

  Future<void> doImport(BuildContext context) async {
    print(widget.keys);
    final privateKeyHex = privateKeyEditController.text;

    if (privateKeyHex.length <= 0) {
      errorListenable.value = S.current.wallet_import_input_privatekeys_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    if (!RegExp(r'^(0x)?[0-9a-f]{64}$').hasMatch(privateKeyHex)) {
      errorListenable.value =
          S.current.wallet_import_privatekey_invalid_formatter_private;
      return;
    } else {
      errorListenable.value = "";
    }

    if (walletNameEditController.text.length <= 0) {
      errorListenable.value = S.current.wallet_import_nicke_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    FocusScope.of(context).requestFocus(FocusNode());

    return Future.delayed(
      Duration(milliseconds: 500),
    )
        .then(
      (_) => CoreProvider.of(context).wallet.importKeypairFromPrivateKey(
            type: CoreProvider.of(context).chainCore,
            privateKey: privateKeyEditController.text,
            walletName: walletNameEditController.text,
          ),
    )
        .then(
            // (value) =>
            //     Navigator.of(context, rootNavigator: true).pop<WalletKey>(value),
            (value) async {
      // await ActionSheet.showAlert(
      //   context,
      //   title: S.current.sheet_title,
      //   description: S.current.sheet_descration_tips,
      //   onCancel: () {
      //     Navigator.pop(context);
      //   },
      // );

      Navigator.of(context, rootNavigator: true).pop<WalletKey>(value);
      Navigator.of(context, rootNavigator: true).pop<WalletKey>(value);
    }).catchError((_) {}, test: (error) {
      if (error == WalletStorageError.ErrorWalletIdentifierAlreadyExist) {
        errorListenable.value =
            S.current.wallet_import_privatekey_already_exist;
        return true;
      }
      return false;
    });
  }

  getAdress() {
    privateKeyEditController.text = widget.keys;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                height: 0,
                width: 0,
                child: Text(getAdress().toString()),
              ),
              ValueListenableBuilder(
                valueListenable: privateKeyEditErrorListenable,
                builder: (context, value, child) => TextField(
                  maxLines: 3,
                  controller: privateKeyEditController,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[xX0-9a-fA-F]')),
                  ],
                  autocorrect: true,
                  decoration: InputDecoration(
                    fillColor:
                        // ThemeUtils().getColor(
                        //     'views.keychain.import.text_field_fill_color'),
                        Color.fromRGBO(250, 250, 250, 1),
                    filled: true,
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.red),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    contentPadding: EdgeInsets.all(15),
                    hintText: S.current.wallet_import_type_Keystore_tips,
                    hintStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color.fromRGBO(175, 175, 175, 1),
                    ),
                    errorText: value,
                  ),
                  keyboardType: TextInputType.visiblePassword,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(49)),
                child: ValueListenableBuilder(
                  valueListenable: walletNameEditErrorListenable,
                  builder: (context, value, child) => Input(
                    title: S.current.wallet_import_nicke_name,
                    errorText: value,
                    placeholder: S.current.wallet_import_nicke_name_placeholder,
                    controller: walletNameEditController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(46)),
                child: ValueListenableBuilder(
                  valueListenable: errorListenable,
                  builder: (contet, value, child) {
                    return Text(
                      errorListenable.value,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(203, 0, 0, 1),
                      ),
                    );
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
                child: StatusButton.withStyle(
                  StatusButtonStyle.Info,
                  title: S.current.wallet_import_done,
                  // ignore: missing_return
                  onPressedFuture: () => doImport(context),
                ),
              ),
            ],
          ),
        ),
      );
}
