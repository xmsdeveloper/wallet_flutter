import 'dart:math';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/command/animations/view.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

class ConfirmPage extends StatefulWidget {
  @override
  _ConfirmPageState createState() => _ConfirmPageState();
}

class _ConfirmPageState extends State<ConfirmPage> {
  List words = [];

  List oldarr = CoreProvider().wallet.mnemonic.split(" ");
  List newArr = [];
  List selectArr = [];
  // String selectArrs = "";

  @override
  void initState() {
    super.initState();

    _get(oldarr);
  }

  int getRandomInt(var min, var max) {
    final _random = new Random();
    return (_random.nextInt((max - min).floor()) + min).toInt();
  }

  _get(List oldarr) {
    newArr.addAll(oldarr);
    for (var i = 1; i < newArr.length; i++) {
      var j = getRandomInt(0, i);
      var t = newArr[i];
      newArr[i] = newArr[j];
      newArr[j] = t;
    }

    return newArr;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: NavigationBar(
          title: S.current.create_wallet_confirm_mnemonics_title,
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          return MediaQuery.of(context).size.width <
                  MediaQuery.of(context).size.height * 1.2
              ? Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(
                        top: ScreenUtil().setHeight(39),
                        left: ScreenUtil().setWidth(20),
                        right: ScreenUtil().setWidth(20),
                        bottom: ScreenUtil().setHeight(7),
                      ),
                      child: Text(
                        S.current.create_wallet_confirm_mnemonics_click_prompt,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(17),
                          color: Color.fromRGBO(112, 112, 112, 1),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      width: double.infinity,
                      height: ScreenUtil().setHeight(217),
                      color: Color.fromRGBO(245, 245, 245, 1),
                      child: Stack(
                        children: [
                          Center(
                            child: Container(
                              height: double.infinity,
                              width: double.infinity,
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(16),
                                left: ScreenUtil().setWidth(8),
                                right: ScreenUtil().setWidth(8),
                                bottom: ScreenUtil().setHeight(15),
                              ),
                              child: GridView.builder(
                                physics: const AlwaysScrollableScrollPhysics(
                                  parent: BouncingScrollPhysics(),
                                ),
                                // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                //     maxCrossAxisExtent: ScreenUtil().setWidth(120),
                                //     mainAxisSpacing: ScreenUtil().setHeight(10),
                                //     crossAxisSpacing: ScreenUtil().setWidth(8),
                                //     childAspectRatio: 3),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  mainAxisSpacing: ScreenUtil().setHeight(10),
                                  crossAxisSpacing: ScreenUtil().setWidth(8),
                                  mainAxisExtent: ScreenUtil().setHeight(38),
                                ),
                                itemBuilder: ((context, index) {
                                  return InkWell(
                                      splashColor: Colors.transparent,
                                      highlightColor: Colors.transparent,
                                      onTap: () {},
                                      child: Stack(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Colors.white,
                                            ),
                                            child: Center(),
                                          ),
                                          Container(
                                            alignment: Alignment.topRight,
                                            margin: EdgeInsets.only(
                                              top: ScreenUtil().setHeight(9),
                                              right: ScreenUtil().setWidth(7),
                                            ),
                                            width: ScreenUtil().setWidth(101),
                                            height: ScreenUtil().setHeight(39),
                                            child: Container(
                                              child: Text(
                                                (index + 1).toString(),
                                                style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  color: Color.fromRGBO(
                                                      112, 112, 112, 1),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ));
                                }),
                                itemCount: 12,
                              ),
                            ),
                          ),
                          Container(
                            height: double.infinity,
                            width: double.infinity,
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(16),
                              left: ScreenUtil().setWidth(8),
                              right: ScreenUtil().setWidth(8),
                              bottom: ScreenUtil().setHeight(15),
                            ),
                            child: GridView.builder(
                              physics: const AlwaysScrollableScrollPhysics(
                                parent: BouncingScrollPhysics(),
                              ),
                              // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                              //     maxCrossAxisExtent: ScreenUtil().setWidth(120),
                              //     mainAxisSpacing: ScreenUtil().setHeight(10),
                              //     crossAxisSpacing: ScreenUtil().setWidth(8),
                              //     childAspectRatio: 3),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                mainAxisSpacing: ScreenUtil().setHeight(10),
                                crossAxisSpacing: ScreenUtil().setWidth(8),
                                mainAxisExtent: ScreenUtil().setHeight(38),
                              ),
                              itemBuilder: ((context, index) {
                                return InkWell(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(
                                        () {
                                          this.newArr.add(selectArr[index]);

                                          this.selectArr.removeAt(index);
                                        },
                                      );
                                    },
                                    child: Stack(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(),
                                          child: Center(
                                              child:
                                                  Text(this.selectArr[index])),
                                        ),
                                      ],
                                    ));
                              }),
                              itemCount: selectArr.length,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: ScreenUtil().setHeight(217),
                      margin: EdgeInsets.only(
                        left: ScreenUtil().setWidth(10),
                        right: ScreenUtil().setWidth(10),
                      ),
                      child: GridView.builder(
                        physics: const AlwaysScrollableScrollPhysics(
                          parent: BouncingScrollPhysics(),
                        ),
                        // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        //   maxCrossAxisExtent: ScreenUtil().setWidth(160),
                        //   mainAxisSpacing: ScreenUtil().setHeight(6),
                        //   childAspectRatio: 3,
                        // ),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: ScreenUtil().setHeight(10),
                          crossAxisSpacing: ScreenUtil().setWidth(8),
                          mainAxisExtent: ScreenUtil().setHeight(46),
                        ),
                        itemBuilder: ((context, index) {
                          return InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onTap: () {
                              setState(
                                () {
                                  this.selectArr.add(newArr[index]);
                                  // selectArrs = selectArr.toString().replaceAll('[', '');
                                  // selectArrs = selectArrs.toString().replaceAll(']', '');
                                  // selectArrs = selectArrs.toString().replaceAll(',', '');
                                  // print(selectArrs);

                                  this.newArr.removeAt(index);
                                },
                              );
                            },
                            child: Container(
                                margin: EdgeInsets.only(
                                  // top: ScreenUtil().setHeight(6),
                                  left: ScreenUtil().setWidth(8),
                                  right: ScreenUtil().setWidth(8),
                                ),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: Color.fromRGBO(228, 229, 228, 1),
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                    color: Color.fromRGBO(250, 250, 250, 1)),
                                child: Center(
                                  child: Text(this.newArr[index]),
                                )),
                          );
                        }),
                        itemCount: newArr.length,
                      ),
                    ),
                    InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        if (this.selectArr.length != this.oldarr.length) {
                          Fluttertoast.showToast(
                              msg: S.current
                                  .create_wallet_confirm_mnemonics_confirm_prompt_fail);
                          return;
                        }
                        for (var i = 0; i < this.selectArr.length; i++) {
                          if (selectArr[i] != oldarr[i]) {
                            Fluttertoast.showToast(
                                msg: S.current
                                    .create_wallet_confirm_mnemonics_confirm_prompt_fail);
                            return;
                          }
                        }
                        Fluttertoast.showToast(
                            msg: S.current
                                .create_wallet_confirm_mnemonics_confirm_prompt_success);
                        Navigator.pushAndRemoveUntil(
                          context,
                          ViewAnimations.viewFadeIn(
                            Engine(),
                            routeName: '/index',
                          ),
                          (route) => route == null,
                        );
                      },
                      child: Container(
                        height: ScreenUtil().setHeight(42),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          // color: Colors.blue,
                          color: Color.fromRGBO(62, 149, 252, 1),
                        ),
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(40),
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(20),
                        ),
                        child: Center(
                          child: Text(
                            S.current.create_wallet_confirm_mnemonics_confirm,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              : Row(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.all(20),
                          // width: double.infinity,
                          width: MediaQuery.of(context).size.width / 2 -
                              ScreenUtil().setWidth(60),
                          height: ScreenUtil().setHeight(217),
                          color: Color.fromRGBO(245, 245, 245, 1),
                          child: Stack(
                            children: [
                              Center(
                                child: Container(
                                  height: double.infinity,
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(16),
                                    left: ScreenUtil().setWidth(8),
                                    right: ScreenUtil().setWidth(8),
                                    bottom: ScreenUtil().setHeight(15),
                                  ),
                                  child: GridView.builder(
                                    physics:
                                        const AlwaysScrollableScrollPhysics(
                                      parent: BouncingScrollPhysics(),
                                    ),
                                    // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                    //     maxCrossAxisExtent: ScreenUtil().setWidth(120),
                                    //     mainAxisSpacing: ScreenUtil().setHeight(10),
                                    //     crossAxisSpacing: ScreenUtil().setWidth(8),
                                    //     childAspectRatio: 3),
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3,
                                      mainAxisSpacing:
                                          ScreenUtil().setHeight(10),
                                      crossAxisSpacing:
                                          ScreenUtil().setWidth(8),
                                      mainAxisExtent:
                                          ScreenUtil().setHeight(40),
                                    ),
                                    itemBuilder: ((context, index) {
                                      return InkWell(
                                          splashColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          onTap: () {},
                                          child: Stack(
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  color: Colors.white,
                                                ),
                                                child: Center(),
                                              ),
                                              Container(
                                                alignment: Alignment.topRight,
                                                margin: EdgeInsets.only(
                                                  top:
                                                      ScreenUtil().setHeight(9),
                                                  right:
                                                      ScreenUtil().setWidth(7),
                                                ),
                                                width:
                                                    ScreenUtil().setWidth(101),
                                                height:
                                                    ScreenUtil().setHeight(39),
                                                child: Container(
                                                  child: Text(
                                                    (index + 1).toString(),
                                                    style: TextStyle(
                                                      fontSize:
                                                          ScreenUtil().setSp(9),
                                                      color: Color.fromRGBO(
                                                          112, 112, 112, 1),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ));
                                    }),
                                    itemCount: 12,
                                  ),
                                ),
                              ),
                              Container(
                                height: double.infinity,
                                width: double.infinity,
                                margin: EdgeInsets.only(
                                  top: ScreenUtil().setHeight(16),
                                  left: ScreenUtil().setWidth(8),
                                  right: ScreenUtil().setWidth(8),
                                  bottom: ScreenUtil().setHeight(15),
                                ),
                                child: GridView.builder(
                                  physics: const AlwaysScrollableScrollPhysics(
                                    parent: BouncingScrollPhysics(),
                                  ),
                                  // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                  //     maxCrossAxisExtent: ScreenUtil().setWidth(120),
                                  //     mainAxisSpacing: ScreenUtil().setHeight(10),
                                  //     crossAxisSpacing: ScreenUtil().setWidth(8),
                                  //     childAspectRatio: 3),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    mainAxisSpacing: ScreenUtil().setHeight(10),
                                    crossAxisSpacing: ScreenUtil().setWidth(8),
                                    mainAxisExtent: ScreenUtil().setHeight(38),
                                  ),
                                  itemBuilder: ((context, index) {
                                    return InkWell(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        onTap: () {
                                          setState(
                                            () {
                                              this.newArr.add(selectArr[index]);

                                              this.selectArr.removeAt(index);
                                            },
                                          );
                                        },
                                        child: Stack(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(),
                                              child: Center(
                                                  child: Text(
                                                      this.selectArr[index])),
                                            ),
                                          ],
                                        ));
                                  }),
                                  itemCount: selectArr.length,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(20),
                            right: ScreenUtil().setWidth(20),
                            bottom: ScreenUtil().setHeight(7),
                          ),
                          child: Text(
                            S.current
                                .create_wallet_confirm_mnemonics_click_prompt,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(17),
                              color: Color.fromRGBO(112, 112, 112, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: ScreenUtil().setHeight(217),
                          width: MediaQuery.of(context).size.width / 2 -
                              ScreenUtil().setWidth(30),
                          margin: EdgeInsets.only(
                            // left: ScreenUtil().setWidth(10),
                            right: ScreenUtil().setWidth(10),
                          ),
                          child: GridView.builder(
                            physics: const AlwaysScrollableScrollPhysics(
                              parent: BouncingScrollPhysics(),
                            ),
                            // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                            //   maxCrossAxisExtent: ScreenUtil().setWidth(160),
                            //   mainAxisSpacing: ScreenUtil().setHeight(6),
                            //   childAspectRatio: 3,
                            // ),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              mainAxisSpacing: ScreenUtil().setHeight(10),
                              mainAxisExtent: ScreenUtil().setHeight(45),
                            ),
                            itemBuilder: ((context, index) {
                              return InkWell(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () {
                                  setState(
                                    () {
                                      this.selectArr.add(newArr[index]);
                                      // selectArrs = selectArr.toString().replaceAll('[', '');
                                      // selectArrs = selectArrs.toString().replaceAll(']', '');
                                      // selectArrs = selectArrs.toString().replaceAll(',', '');
                                      // print(selectArrs);

                                      this.newArr.removeAt(index);
                                    },
                                  );
                                },
                                child: Container(
                                    margin: EdgeInsets.only(
                                      // top: ScreenUtil().setHeight(6),
                                      left: ScreenUtil().setWidth(8),
                                      right: ScreenUtil().setWidth(8),
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          width: 1,
                                          color:
                                              Color.fromRGBO(228, 229, 228, 1),
                                        ),
                                        borderRadius: BorderRadius.circular(5),
                                        color:
                                            Color.fromRGBO(250, 250, 250, 1)),
                                    child: Center(
                                      child: Text(this.newArr[index]),
                                    )),
                              );
                            }),
                            itemCount: newArr.length,
                          ),
                        ),
                        InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () {
                            if (this.selectArr.length != this.oldarr.length) {
                              Fluttertoast.showToast(
                                  msg: S.current
                                      .create_wallet_confirm_mnemonics_confirm_prompt_fail);
                              return;
                            }
                            for (var i = 0; i < this.selectArr.length; i++) {
                              if (selectArr[i] != oldarr[i]) {
                                Fluttertoast.showToast(
                                    msg: S.current
                                        .create_wallet_confirm_mnemonics_confirm_prompt_fail);
                                return;
                              }
                            }
                            Fluttertoast.showToast(
                                msg: S.current
                                    .create_wallet_confirm_mnemonics_confirm_prompt_success);
                            Navigator.pushAndRemoveUntil(
                              context,
                              ViewAnimations.viewFadeIn(
                                Engine(),
                                routeName: '/index',
                              ),
                              (route) => route == null,
                            );
                          },
                          child: Container(
                            height: ScreenUtil().setHeight(42),
                            width: MediaQuery.of(context).size.width / 2 -
                                ScreenUtil().setWidth(30),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              // color: Colors.blue,
                              color: Color.fromRGBO(62, 149, 252, 1),
                            ),
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(40),
                              left: ScreenUtil().setWidth(20),
                              right: ScreenUtil().setWidth(20),
                            ),
                            child: Center(
                              child: Text(
                                S.current
                                    .create_wallet_confirm_mnemonics_confirm,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                );
        }));
  }
}
