// @dart=2.9
part of '../importer.dart';

// ignore: must_be_immutable
class _MnemonicImportPage extends StatelessWidget {
  final TextEditingController privateKeyEditController =
      TextEditingController();
  final TextEditingController walletNameEditController =
      TextEditingController();
  final TextEditingController passWordEditController = TextEditingController();
  final TextEditingController cofirmPassWordController =
      TextEditingController();

  final ValueNotifier<String> privateKeyEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> walletNameEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> passWordEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> cofirmPassWordEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");
  String passwordStrength = "";
  final ValueNotifier<String> passwordStrengthListenable =
      ValueNotifier<String>(null);

  /// 密码强度正则,表示匹配任意的拉丁大小写字母，数字再加上下划线和减号
  final weakPwdStrengthRegexp = RegExp(
    r'[\w_-]$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含2个大小写字母
  final middlePwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]{2,}).*$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含1个小写字母,必须包含1个大写字母,必须包含1个特殊字符
  final strongPwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*[!@#$%^&*?\(\)]).*$',
  );

  Future<void> doImport(BuildContext context) async {
    final privateKeyHex = privateKeyEditController.text;

    if (privateKeyHex.length <= 0) {
      errorListenable.value = S.current.wallet_import_input_privatekey_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    if (!RegExp(r'^[a-z+\s?]+$').hasMatch(privateKeyHex)) {
      errorListenable.value =
          S.current.wallet_import_privatekey_invalid_formatter;
      return;
    } else {
      errorListenable.value = "";
    }

    if (walletNameEditController.text.length <= 0) {
      errorListenable.value = S.current.wallet_import_nicke_empty;
      return;
    } else {
      errorListenable.value = "";
    }
    if (passWordEditController.text.length <= 0) {
      errorListenable.value = S.current.wallet_import_password_empty;
      return;
    } else {
      errorListenable.value = "";
    }
    if (cofirmPassWordController.text.length <= 0) {
      errorListenable.value = S.current.create_wallet_pwd_confirm_empty;
      return;
    } else {
      errorListenable.value = "";
    }
    if (passWordEditController.text != cofirmPassWordController.text) {
      errorListenable.value = S.current.create_wallet_pwd_confirm_different;

      return;
    }

    final wordRegExp = RegExp(r'[a-z]+');

    final words = wordRegExp
        .allMatches(privateKeyEditController.text)
        .map((e) => e.group(0))
        .toList();

    // var walletNameEditControlle = TextEditingController();
    // var privateKeyEditController = TextEditingController();
    // var passWordEditController = TextEditingController();
    return CoreProvider.of(context)
        .importRootKeypair(
          walletNameEditController.text,
          words,
          passWordEditController.text,
          true,
        )
        .then((_) => Future.delayed(Duration(milliseconds: 1000)))
        .then(
          (value) => Navigator.pushAndRemoveUntil(
            context,
            ViewAnimations.viewFadeIn(
              Engine(),
              routeName: '/index',
            ),
            (route) => route == null,
          ),
        );
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              ValueListenableBuilder(
                valueListenable: privateKeyEditErrorListenable,
                builder: (context, value, child) => TextField(
                  maxLines: 3,
                  controller: privateKeyEditController,
                  autocorrect: true,
                  decoration: InputDecoration(
                    fillColor:
                        // ThemeUtils().getColor(
                        //     'views.keychain.import.text_field_fill_color'),
                        Color.fromRGBO(250, 250, 250, 1),
                    filled: true,
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(width: 1, color: Colors.red),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        width: 1,
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        width: 1,
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        width: 1,
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    contentPadding: EdgeInsets.all(15),
                    hintText:
                        S.current.wallet_import_input_privatekey_placeholder,
                    hintStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color.fromRGBO(175, 175, 175, 1),
                    ),
                    errorText: value,
                  ),
                  keyboardType: TextInputType.visiblePassword,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(30)),
                child: ValueListenableBuilder(
                  valueListenable: walletNameEditErrorListenable,
                  builder: (context, value, child) => Input(
                    title: S.current.wallet_import_nicke_name,
                    errorText: value,
                    placeholder: S.current.wallet_import_nicke_name_placeholder,
                    controller: walletNameEditController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(32)),
                alignment: Alignment.centerLeft,
                child: Text(
                  S.current.create_wallet_pwd,
                ),
              ),
              Container(
                child: WarmingPage(
                  tipText: S.current.create_wallet_tips,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
                child: ValueListenableBuilder(
                  valueListenable: passWordEditErrorListenable,
                  builder: (context, value, child) => Input(
                    title: "",
                    errorText: value,
                    placeholder: S.current.wallet_import_password_placeholder,
                    controller: passWordEditController,
                    onChanged: (_) {
                      if (weakPwdStrengthRegexp
                              .hasMatch(passWordEditController.text) ==
                          true) {
                        this.passwordStrength =
                            S.current.password_strength_weak;
                        this.passwordStrengthListenable.value =
                            this.passwordStrength;
                      } else {
                        this.passwordStrength = "";
                        this.passwordStrengthListenable.value =
                            this.passwordStrength;
                      }

                      if (middlePwdStrengthRegexp
                              .hasMatch(passWordEditController.text) ==
                          true) {
                        this.passwordStrength =
                            S.current.password_strength_middle;
                        this.passwordStrengthListenable.value =
                            this.passwordStrength;
                      }
                      if (strongPwdStrengthRegexp
                              .hasMatch(passWordEditController.text) ==
                          true) {
                        this.passwordStrength =
                            S.current.password_strength_strength;
                        this.passwordStrengthListenable.value =
                            this.passwordStrength;
                      }
                    },
                  ),
                ),
              ),
              ValueListenableBuilder(
                valueListenable: passwordStrengthListenable,
                builder: (context, value, child) {
                  return this.passwordStrength != ""
                      ? Container(
                          margin:
                              EdgeInsets.only(top: ScreenUtil().setHeight(13)),
                          child: PwdIdentifier(
                            passwordStrength: this.passwordStrength,
                          ),
                        )
                      : Container();
                },
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
                child: ValueListenableBuilder(
                  valueListenable: cofirmPassWordEditErrorListenable,
                  builder: (context, value, child) => Input(
                    title: "",
                    errorText: value,
                    placeholder:
                        S.current.create_wallet_pwd_confirm_placeholder,
                    controller: cofirmPassWordController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(30)),
                child: ValueListenableBuilder(
                  valueListenable: errorListenable,
                  builder: (contet, value, child) {
                    return Text(
                      errorListenable.value,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(203, 0, 0, 1),
                      ),
                    );
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
                child: StatusButton.withStyle(
                  StatusButtonStyle.Info,
                  title: S.current.wallet_import_done,
                  onPressedFuture: () => doImport(context),
                ),
              ),
            ],
          ),
        ),
      );
}
