// @dart=2.9
part of '../importer.dart';

class _KeystoreImportPage extends StatelessWidget {
  final String keys;
  _KeystoreImportPage(this.keys);
  final TextEditingController privateKeyEditController =
      TextEditingController();
  final TextEditingController walletNameEditController =
      TextEditingController();
  final TextEditingController passwordEditController = TextEditingController();

  final ValueNotifier<String> privateKeyEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> walletNameEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> passwordEditErrorListnable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");
  Future<void> doImport(BuildContext context) async {
    // 检查keystore格式
    final keystoreJson = privateKeyEditController.text;

    if (keystoreJson.length <= 0) {
      errorListenable.value = S.current.wallet_import_input_keystore_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    try {
      jsonDecode(keystoreJson);
      errorListenable.value = "";
    } catch (_) {
      errorListenable.value =
          S.current.wallet_import_input_keystore_invalid_formatter;
    }
    if (errorListenable.value != "") {
      return;
    }

    // 检查keystore密码
    if (passwordEditController.text.length <= 0) {
      errorListenable.value = S.current.wallet_import_input_password_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    // 检查钱包名称
    if (walletNameEditController.text.length <= 0) {
      errorListenable.value = S.current.wallet_import_nicke_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    FocusScope.of(context).requestFocus(FocusNode());
    // VerifySheets.shows(
    //   context,
    //   verifyCallback: (context, pwd) {
    //     final verifySuccess =
    //         CoreProvider.of(context).wallet.verifySecurity(pwd);
    //     if (!verifySuccess) {
    //       return S.current.password_verify_faild;
    //     } else {
    //       CoreProvider.of(context)
    //           .wallet
    //           .importKeypairFromJson(
    //             type: CoreProvider.of(context).chainCore,
    //             json: privateKeyEditController.text,
    //             passphrase: passwordEditController.text,
    //             walletName: walletNameEditController.text,
    //           )
    //           .then((value) async {
    //         // Navigator.of(context).pop();
    //         // Navigator.of(context).pop<WalletKey>(value);
    //       })
    //             ..catchError((_) {}, test: (error) {
    //               if (error ==
    //                   WalletStorageError.ErrorWalletIdentifierAlreadyExist) {
    //                 privateKeyEditErrorListenable.value =
    //                     S.current.wallet_import_privatekey_already_exist;
    //                 return true;
    //               } else if (error == WalletStorageError.ErrorDecryptKeystore) {
    //                 passwordEditErrorListnable.value =
    //                     S.current.wallet_import_keystore_password_invaid;
    //                 return true;
    //               }
    //               return false;
    //             });
    //     }
    //     return null;
    //   },
    // );

    return Future.delayed(
      Duration(milliseconds: 500),
    )
        .then(
      (_) => CoreProvider.of(context).wallet.importKeypairFromJson(
            type: CoreProvider.of(context).chainCore,
            json: privateKeyEditController.text,
            passphrase: passwordEditController.text,
            walletName: walletNameEditController.text,
          ),
    )
        .then(
      (value) {
        Navigator.of(context).pop<WalletKey>(value);
        Navigator.of(context).pop<WalletKey>(value);
      },
    ).catchError((_) {}, test: (error) {
      if (error == WalletStorageError.ErrorWalletIdentifierAlreadyExist) {
        errorListenable.value =
            S.current.wallet_import_privatekey_already_exist;
        return true;
      } else if (error == WalletStorageError.ErrorDecryptKeystore) {
        errorListenable.value =
            S.current.wallet_import_keystore_password_invaid;
        return true;
      }
      return false;
    });
  }

  getAdress() {
    privateKeyEditController.text = this.keys;
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                height: 0,
                width: 0,
                child: Text(getAdress().toString()),
              ),
              ValueListenableBuilder(
                valueListenable: privateKeyEditErrorListenable,
                builder: (context, value, child) => TextField(
                  maxLines: 8,
                  scrollPadding: EdgeInsets.zero,
                  controller: privateKeyEditController,
                  autocorrect: true,
                  decoration: InputDecoration(
                    fillColor:
                        // ThemeUtils().getColor(
                        //     'views.keychain.import.text_field_fill_color'),
                        Color.fromRGBO(250, 250, 250, 1),
                    filled: true,
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.red),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    contentPadding: EdgeInsets.all(15),
                    hintText:
                        S.current.wallet_import_input_keystore_placeholder,
                    hintStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color.fromRGBO(175, 175, 175, 1),
                    ),
                    errorText: value,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(30)),
                child: ValueListenableBuilder(
                  valueListenable: passwordEditErrorListnable,
                  builder: (context, value, child) => Input(
                    obscureText: true,
                    title: S.current.wallet_import_input_password,
                    errorText: value,
                    placeholder:
                        S.current.wallet_import_input_password_placeholder,
                    controller: passwordEditController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(30)),
                child: ValueListenableBuilder(
                  valueListenable: walletNameEditErrorListenable,
                  builder: (context, value, child) => Input(
                    title: S.current.wallet_import_nicke_name,
                    errorText: value,
                    placeholder: S.current.wallet_import_nicke_name_placeholder,
                    controller: walletNameEditController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(46)),
                child: ValueListenableBuilder(
                  valueListenable: errorListenable,
                  builder: (contet, value, child) {
                    return Text(
                      errorListenable.value,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(203, 0, 0, 1),
                      ),
                    );
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
                child: StatusButton.withStyle(
                  StatusButtonStyle.Info,
                  title: S.current.wallet_import_done,
                  onPressedFuture: () => doImport(context),
                ),
              ),
            ],
          ),
        ),
      );
}
