// @dart=2.9
part of '../importer.dart';

class SeeWalletImport extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;
  final String keys;

  SeeWalletImport({Key key, this.dataSourceProvider, this.keys})
      : super(key: key);

  @override
  State<SeeWalletImport> createState() => _SeeWalletImportState();
}

class _SeeWalletImportState extends State<SeeWalletImport> {
  final TextEditingController addressEditController = TextEditingController();
  final TextEditingController walletNameEditController =
      TextEditingController();

  final ValueNotifier<String> addressEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> walletNameEditErrorListenable =
      ValueNotifier<String>(null);

  /// 正确地址的正则表达式
  final addressRegexp = RegExp(
    r'[0-9a-f]{40}$',

    /// 忽略大小写
    caseSensitive: false,
  );
  Future<void> doImport(BuildContext context) async {
    final adressKeyHex = addressEditController.text;

    if (adressKeyHex.length <= 0 ||
        !addressRegexp.hasMatch(addressEditController.text) ||
        adressKeyHex.length > 42) {
      addressEditErrorListenable.value = S.current.message_tips_valid_adress;
      // S.current.wallet_import_input_privatekeys_empty;
      return;
    } else {
      addressEditErrorListenable.value = null;
    }

    // if (!RegExp(r'^(0x)?[0-9a-f]{64}$').hasMatch(privateKeyHex)) {
    //   privateKeyEditErrorListenable.value =
    //       S.current.wallet_import_privatekey_invalid_formatter;
    //   return;
    // } else {
    //   privateKeyEditErrorListenable.value = null;
    // }

    if (walletNameEditController.text.length <= 0) {
      walletNameEditErrorListenable.value = S.current.wallet_import_nicke_empty;
      return;
    } else {
      walletNameEditErrorListenable.value = null;
    }

    FocusScope.of(context).requestFocus(FocusNode());
    // this
    //     .dataSourceProvider
    //     .localService
    //     .seeWalletlist()
    //     .then((value) => print(value.first.address));

    // return this.dataSourceProvider.localService.seeWalletInsert(WalletKey(
    //       walletNameEditController.text,
    //       addressEditController.text,
    //       "_avatarPath",
    //       60,
    //       0,
    //       0,
    //       null,
    //       false,
    //       false,
    //     ));
    return Future.delayed(
      Duration(milliseconds: 500),
    )
        .then(
      (_) => SeeWalletStorageUtils().seeWalletInsert(
        WalletKey(
          walletNameEditController.text,
          addressEditController.text,
          "_avatarPath",
          60,
          0,
          0,
          null,
          false,
          false,
        ),
      ),
      // this.dataSourceProvider.localService.seeWalletInsert(
      //       WalletKey(
      //         walletNameEditController.text,
      //         addressEditController.text,
      //         "_avatarPath",
      //         60,
      //         0,
      //         0,
      //         null,
      //         false,
      //         false,
      //       ),
      //     ),
    )
        .then((value) async {
      Navigator.of(context, rootNavigator: true).pop<WalletKey>();
      Navigator.of(context, rootNavigator: true).pop<WalletKey>();
    }).catchError(
      (_) {},
      test: (error) {
        if (error == WalletStorageError.ErrorWalletIdentifierAlreadyExist) {
          addressEditErrorListenable.value =
              S.current.wallet_import_privatekey_already_exist;
          return true;
        }
        return false;
      },
    );
  }

  getAdress() {
    addressEditController.text = widget.keys;
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                height: 0,
                width: 0,
                child: Text(getAdress().toString()),
              ),
              ValueListenableBuilder(
                valueListenable: addressEditErrorListenable,
                builder: (context, value, child) => TextField(
                  maxLines: 3,
                  controller: addressEditController,
                  // inputFormatters: [
                  //   FilteringTextInputFormatter.allow(RegExp('[xX0-9a-fA-F]')),
                  // ],
                  autocorrect: true,
                  decoration: InputDecoration(
                    fillColor:
                        // ThemeUtils().getColor(
                        //     'views.keychain.import.text_field_fill_color'),
                        Color.fromRGBO(250, 250, 250, 1),
                    filled: true,
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.red),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(
                        color:
                            ThemeUtils().getColor('utils.input.border_color'),
                      ),
                    ),
                    contentPadding: EdgeInsets.all(15),
                    // hintText: S.current.wallet_import_type_Keystore_tips,
                    hintText:
                        S.current.wallet_selector_shhet_create_see_input_tips,
                    hintStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color.fromRGBO(175, 175, 175, 1),
                    ),
                    errorText: value,
                  ),
                  keyboardType: TextInputType.visiblePassword,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(7)),
                  child: Text(
                    S.current.wallet_selector_shhet_create_see_tips,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color.fromRGBO(175, 175, 175, 1),
                    ),
                  )),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(74)),
                child: ValueListenableBuilder(
                  valueListenable: walletNameEditErrorListenable,
                  builder: (context, value, child) => Input(
                    title: S.current.wallet_import_nicke_name,
                    errorText: value,
                    placeholder: S.current.wallet_import_nicke_name_placeholder,
                    controller: walletNameEditController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(60)),
                child: StatusButton.withStyle(
                  StatusButtonStyle.Info,
                  title: S.current.wallet_import_done,
                  // ignore: missing_return
                  onPressedFuture: () => doImport(context),
                ),
              ),
            ],
          ),
        ),
      );
}
