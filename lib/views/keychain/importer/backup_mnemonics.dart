import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/keychain/importer/confirm_mnemonics.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/command/theme/icon_utils.dart' as CustomIcons;

class BackupPage extends StatefulWidget {
  @override
  _BackupPageState createState() => _BackupPageState();
}

class _BackupPageState extends State<BackupPage> {
  List oldarr = CoreProvider().wallet.mnemonic.split(" ");

  @override
  void initState() {
    super.initState();

    print(oldarr);
  }

  List tips = [
    {
      "title": S.current.create_wallet_backup_mnemonics_tips1,
    },
    {
      "title": S.current.create_wallet_backup_mnemonics_tips2,
    },
    {
      "title": S.current.create_wallet_backup_mnemonics_tips3,
    },
    {
      "title": S.current.create_wallet_backup_mnemonics_tips4,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.create_wallet_backup_mnemonics_title,
      ),
      body: CustomScrollView(
        physics: const AlwaysScrollableScrollPhysics(
          parent: BouncingScrollPhysics(),
        ),
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(10),
                right: ScreenUtil().setWidth(10),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(30),
                right: ScreenUtil().setWidth(20),
                left: ScreenUtil().setWidth(20),
              ),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(15),
                      bottom: ScreenUtil().setHeight(18),
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      S.current.create_wallet_backup_mnemonics_Wallet_address,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Color.fromRGBO(8, 8, 8, 1),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(24)),
                    child: Row(
                      children: [
                        Container(
                          child: Icon(
                            CustomIcons.manage_your_wallet,
                            size: ScreenUtil().setSp(17.5),
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(left: ScreenUtil().setWidth(7)),
                          child: Text(
                            CoreProvider.of(context).account.address,
                            style: TextStyle(
                              fontSize: 9,
                              color: Color.fromRGBO(112, 112, 112, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(8),
                      right: ScreenUtil().setWidth(18),
                      bottom: ScreenUtil().setHeight(28),
                    ),
                    // height: 1,
                    width: MediaQuery.of(context).size.width,
                    // color: Colors.grey[200],
                    child: Image.asset(
                      'assets/imgs/dotted_line.png',
                      package: "wallet_flutter",
                      fit: BoxFit.fill,
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      S.current.create_wallet_backup_mnemonics_title,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Color.fromRGBO(8, 8, 8, 1),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(10),
                      bottom: ScreenUtil().setHeight(18),
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      S.current.create_wallet_backup_mnemonics_tips5,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: Color.fromRGBO(112, 112, 112, 1),
                      ),
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(180),
                    margin: EdgeInsets.only(
                      bottom: ScreenUtil().setHeight(17),
                      right: ScreenUtil().setWidth(9),
                    ),
                    child: GridView.builder(
                      physics: const AlwaysScrollableScrollPhysics(
                        parent: BouncingScrollPhysics(),
                      ),
                      // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      //   maxCrossAxisExtent: ScreenUtil().setWidth(151),
                      //   childAspectRatio: 3,
                      // ),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        mainAxisExtent: ScreenUtil().setHeight(45),
                      ),
                      itemBuilder: ((context, index) {
                        return InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            // onTap: () {
                            //   setState(
                            //     () {
                            //       this.newArr.add(selectArr[index]);

                            //       // selectArrs = selectArr.toString().replaceAll('[', '');
                            //       // selectArrs = selectArrs.toString().replaceAll(']', '');
                            //       // selectArrs = selectArrs.toString().replaceAll(',', '');
                            //       // print(selectArrs);

                            //       this.selectArr.removeAt(index);
                            //     },
                            //   );
                            // },
                            child: Stack(
                              children: [
                                Container(
                                  height: 60,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        width: 0.5,
                                        color: Color.fromRGBO(228, 229, 228, 1),
                                      ),
                                      color: Colors.white),

                                  // margin: EdgeInsets.all(10),
                                  child: Center(
                                    child: Text(this.oldarr[index]),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(3),
                                    right: ScreenUtil().setWidth(7),
                                  ),
                                  width: ScreenUtil().setWidth(101),
                                  height: ScreenUtil().setHeight(39),
                                  child: Container(
                                    child: Text(
                                      (index + 1).toString(),
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(9),
                                        color: Color.fromRGBO(112, 112, 112, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ));
                      }),
                      itemCount: oldarr.length,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(16),
                left: ScreenUtil().setWidth(27),
                right: ScreenUtil().setWidth(27),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      S.current.create_wallet_backup_mnemonics_tips6,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Color.fromRGBO(217, 83, 79, 1),
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: tips
                        .map(
                          (e) => Container(
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(12),
                            ),
                            child: Text(
                              e["title"],
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(13),
                                color: Color.fromRGBO(217, 83, 79, 1),
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () {
                Navigator.of(context).push(ViewAnimations.viewRightIn(
                  ConfirmPage(),
                ));
              },
              child: Container(
                height: ScreenUtil().setHeight(48),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  // color: Color.fromRGBO(48, 198, 153, 1),
                  color: Color.fromRGBO(62, 149, 252, 1),
                ),
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(52),
                  left: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                ),
                child: Center(
                  child: Text(
                    S.current.create_wallet_backup_mnemonics_confirm,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
