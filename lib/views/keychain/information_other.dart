import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/command/animations/view.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/base/key.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OtherPage extends StatefulWidget {
  final WalletKey walletKey;
  const OtherPage({required this.walletKey, Key? key}) : super(key: key);

  @override
  State<OtherPage> createState() => _OtherPageState();
}

class _OtherPageState extends State<OtherPage> {
  bool isSelect = false;
  void initState() {
    super.initState();
    _getdata();
  }

  @override
  void dispose() {
    super.dispose();
    // animationController.dispose();

    // 离开页面统计（手动采集时才可设置）
  }

  List<WalletKey> newaccount = [];
  _getdata() async {
    newaccount = await SeeWalletStorageUtils().seeWalletlist();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        backgroundColor: Colors.white,
        title: S.current.wallet_delete_title,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child: PreferredSize(
            preferredSize: Size(double.infinity, ScreenUtil().setHeight(128)),
            child: ValueListenableBuilder<WalletKey>(

                /// Infomation页面显示的有可能不是当前选择的私钥，同时用户可能回修改存储的信息
                /// 所以此处监听accountListenable，注意在Builder中不可以使用value字段，因为
                /// value字段一定是当前选择的account。应该使用构造中的walletKey
                valueListenable: CoreProvider.of(context).accountListenable,
                builder: (context, _, child) => GestureDetector(
                      child: Column(
                        children: [
                          Container(
                            height: ScreenUtil().setHeight(118),
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(21),
                                    right: ScreenUtil().setWidth(15),
                                  ),
                                  width: ScreenUtil().setWidth(51),
                                  height: ScreenUtil().setHeight(51),
                                  child: Image.asset(
                                    'assets/images/avator.png',
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                widget.walletKey.nickName ??
                                                    'NoName',
                                                style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(16),
                                                  color: Colors.black,
                                                ),
                                              ),
                                              CoreProvider.of(context)
                                                          .account
                                                          .privateKeyRaw ==
                                                      null
                                                  ? Container(
                                                      margin: EdgeInsets.only(
                                                        left: ScreenUtil()
                                                            .setWidth(10),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                      // height:
                                                      //     ScreenUtil().setHeight(20),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(4),
                                                        border: Border.all(
                                                          width: 1,
                                                          color: Color.fromRGBO(
                                                            175,
                                                            175,
                                                            175,
                                                            1,
                                                          ),
                                                        ),
                                                      ),
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                          left: ScreenUtil()
                                                              .setWidth(5),
                                                          right: ScreenUtil()
                                                              .setWidth(5),
                                                        ),
                                                        child: Center(
                                                          child: Text(
                                                            S.current
                                                                .wallet_watch_name,
                                                            style: TextStyle(
                                                              fontSize:
                                                                  ScreenUtil()
                                                                      .setSp(
                                                                          14),
                                                              color: Color
                                                                  .fromRGBO(
                                                                175,
                                                                175,
                                                                175,
                                                                1,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  : Container()
                                            ],
                                          ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(10)),
                                          InkWell(
                                            splashColor: Colors.transparent,
                                            highlightColor: Colors.transparent,
                                            child: Row(
                                              children: [
                                                Opacity(
                                                  opacity: 0.6,
                                                  child: Text(
                                                    widget.walletKey.address,
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil()
                                                          .setSp(12),
                                                      color: Color.fromRGBO(
                                                        175,
                                                        175,
                                                        175,
                                                        1,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: SizedBox(),
                                ),
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(10),
                            width: MediaQuery.of(context).size.width,
                            color: Color.fromRGBO(246, 246, 246, 1),
                          ),
                        ],
                      ),
                    )),
          )),
          SliverToBoxAdapter(
            child: Container(
              height: ScreenUtil().setHeight(144),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromRGBO(252, 239, 230, 1),
              ),
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(24),
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
              ),
              width: double.infinity,
              child: Row(
                children: [
                  Container(
                    alignment: Alignment.topCenter,
                    padding: EdgeInsets.all(ScreenUtil().setSp(18)),
                    height: ScreenUtil().setHeight(144),
                    child: Image.asset(
                      "assets/images/warming.png",
                      height: ScreenUtil().setHeight(22.5),
                      width: ScreenUtil().setWidth(22.5),
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(144),
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(16),
                      bottom: ScreenUtil().setHeight(16),
                    ),
                    width: ScreenUtil().setWidth(252),
                    child: Text(
                      S.current.wallet_delete_tip,
                      maxLines: 5,
                      style: ThemeUtils().getTextStyle(
                        'views.keychain.exporter.textstyle.warning',
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: ScreenUtil().setHeight(21),
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(31),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelect = !isSelect;
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                        right: ScreenUtil().setWidth(16),
                      ),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(ScreenUtil().setHeight(4)),
                        border: Border.all(
                          width: ScreenUtil().setWidth(1),
                          color: Color.fromRGBO(231, 136, 77, 1),
                        ),
                      ),
                      width: ScreenUtil().setWidth(20),
                      height: ScreenUtil().setHeight(21),
                      child: isSelect == true
                          ? Container(
                              // margin: EdgeInsets.only(
                              //   bottom: ScreenUtil().setHeight(10),
                              // ),
                              // width: ScreenUtil().setWidth(18),
                              // height: ScreenUtil().setHeight(14.4),
                              // child: Image.asset(
                              //   "assets/images/done.png",
                              //   width: ScreenUtil().setWidth(18),
                              //   height: ScreenUtil().setHeight(14.4),
                              // ),
                              child: SvgPicture.asset(
                              "assets/images/done.svg",
                              width: ScreenUtil().setWidth(18),
                              height: ScreenUtil().setHeight(14.4),
                              color: Color.fromRGBO(
                                231,
                                136,
                                77,
                                1,
                              ),
                            ))
                          : Container(),
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(21),
                    child: Center(
                      child: Text(
                        S.current.wallet_delete_checkout,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          color: Color.fromRGBO(231, 136, 77, 1),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(200),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: Colors.grey,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      width: ScreenUtil().setWidth(156),
                      height: ScreenUtil().setHeight(46),
                      child: Center(
                        child: Text(
                          S.current.sheet_cancel,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      if (isSelect == false) {
                        Fluttertoast.showToast(
                          msg: S.current.wallet_delete_message,
                        );
                        return null;
                      }
                      VerifySheets.shows(
                        context,
                        verifyCallback: (context, pwd) {
                          final verifySuccess = CoreProvider.of(context)
                              .wallet
                              .verifySecurity(pwd);
                          if (!verifySuccess) {
                            return S.current.password_verify_faild;
                          } else {
                            // FocusScope.of(context).requestFocus(FocusNode());

                            Navigator.of(context).pop();

                            if (CoreProvider.of(context)
                                    .account
                                    .privateKeyRaw !=
                                null) {
                              /// 删除对应地址
                              CoreProvider.of(context)
                                  .removeAccount(this.widget.walletKey);
                            } else {
                              SeeWalletStorageUtils().deleteSeeWallet(
                                  this.widget.walletKey.address);
                              int index =
                                  newaccount.indexOf(this.widget.walletKey);
                              if (index >= 1) {
                                CoreProvider.of(context).account =
                                    newaccount[index - 1];
                              } else {
                                CoreProvider.of(context).account =
                                    CoreProvider.of(context).wallet.keysOf(
                                        CoreProvider.of(context).chainCore)[0];
                              }
                            }

                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          }
                          return null;
                        },
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      width: ScreenUtil().setWidth(156),
                      height: ScreenUtil().setHeight(46),
                      child: Center(
                        child: Text(
                          S.current.button_delete,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
