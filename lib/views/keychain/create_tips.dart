import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/command/animations/view.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

import 'importer/backup_mnemonics.dart';

class CreatePageTips extends StatefulWidget {
  final String userName;
  final String userPassword;
  const CreatePageTips(
      {required this.userName, required this.userPassword, Key? key})
      : super(key: key);

  @override
  State<CreatePageTips> createState() => _CreatePageTipsState();
}

class _CreatePageTipsState extends State<CreatePageTips> {
  List tips = [
    {
      "tips": S.current.create_wallet_tips_one,
      "isSelect": false,
    },
    {
      "tips": S.current.create_wallet_tips_two,
      "isSelect": false,
    },
    {
      "tips": S.current.create_wallet_tips_three,
      "isSelect": false,
    }
  ];
  Future<void> doGenerate(BuildContext context) async {
    for (var i = 0; i < tips.length; i++) {
      if (tips[i]['isSelect'] == false) {
        Fluttertoast.showToast(
          msg: S.current.create_wallet_tips_message,
        );
        return;
      }
    }
    return CoreProvider.of(context)
        .generateRootKeypair(
          this.widget.userName,
          this.widget.userPassword,
          true,
        )
        .then((_) => Future.delayed(Duration(milliseconds: 1000)))
        .then((value) => Navigator.of(context).push(
              ViewAnimations.viewRightIn(
                // PrivateKeyDisplay.rootWallet(
                //   encryptPassword: confirmController.text,
                //   wallet: CoreProvider.of(context).wallet,
                // ),
                BackupPage(),
              ),
            ));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.create_wallet_title_appbar,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(
                    top: ScreenUtil().setHeight(51),
                    bottom: ScreenUtil().setHeight(78),
                  ),
                  width: ScreenUtil().setWidth(140),
                  height: ScreenUtil().setHeight(84),
                  child: Image.asset("assets/images/create_wallet_tips.png"),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(24),
                    right: ScreenUtil().setWidth(24),
                  ),
                  child: Text(
                    S.current.create_wallet_title,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(top: ScreenUtil().setHeight(24)),
              child: Column(
                children: tips
                    .map((e) => InkWell(
                          onTap: () {
                            setState(() {
                              e['isSelect'] = !e['isSelect'];
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                                bottom: ScreenUtil().setHeight(10)),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  width: 1,
                                  color: e['isSelect'] == false
                                      ? Color.fromRGBO(216, 216, 216, 1)
                                      : Color.fromRGBO(62, 149, 252, 1),
                                )),
                            width: ScreenUtil().setWidth(331),
                            height: ScreenUtil().setHeight(75),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15)),
                                  width: ScreenUtil().setWidth(252),
                                  child: Center(
                                    child: Text(
                                      e["tips"],
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(14),
                                        color: e['isSelect'] == false
                                            ? Color.fromRGBO(175, 175, 175, 1)
                                            : Color.fromRGBO(62, 149, 252, 1),
                                      ),
                                    ),
                                  ),
                                ),
                                e['isSelect'] == false
                                    ? Container(
                                        margin: EdgeInsets.only(
                                            right: ScreenUtil().setWidth(15)),
                                        width: ScreenUtil().setWidth(16),
                                        height: ScreenUtil().setHeight(16),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                            width: 1,
                                            color: Color.fromRGBO(
                                                216, 216, 216, 1),
                                          ),
                                        ),
                                      )
                                    : Container(
                                        margin: EdgeInsets.only(
                                            right: ScreenUtil().setWidth(15)),
                                        width: ScreenUtil().setWidth(16),
                                        height: ScreenUtil().setHeight(16),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color:
                                              Color.fromRGBO(62, 149, 252, 1),
                                        ),
                                        child: Icon(
                                          Icons.done,
                                          size: ScreenUtil().setSp(10),
                                          color: Colors.white,
                                        ),
                                      )
                              ],
                            ),
                          ),
                        ))
                    .toList(),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(60),
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
                bottom: 0,
              ),
              child: StatusButton.withStyle(
                StatusButtonStyle.Info,
                title: S.current.button_next,
                onPressedFuture: () => doGenerate(context),
              ),
            ),
          )
        ],
      ),
    );
  }
}
