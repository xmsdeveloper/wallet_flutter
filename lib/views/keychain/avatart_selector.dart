// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

class AvatarSelector extends StatelessWidget {
  final WalletKey walletKey;

  final ValueNotifier<String> selectedPathListenable;

  final avatarSource = [
    {
      'type_name': '动物类',
      'paths': [
        'assets/imgs/user_icon_anm_0.png',
        'assets/imgs/user_icon_anm_1.png',
        'assets/imgs/user_icon_anm_2.png',
        'assets/imgs/user_icon_anm_3.png',
        'assets/imgs/user_icon_anm_4.png',
      ]
    },
    {
      'type_name': '默认',
      'paths': [
        'assets/imgs/user_icon_0.png',
        'assets/imgs/user_icon_1.png',
        'assets/imgs/user_icon_2.png',
        'assets/imgs/user_icon_3.png',
        'assets/imgs/user_icon_4.png',
        'assets/imgs/user_icon_5.png',
        'assets/imgs/user_icon_6.png',
        'assets/imgs/user_icon_7.png',
        'assets/imgs/user_icon_8.png',
        'assets/imgs/user_icon_9.png',
        'assets/imgs/user_icon_10.png',
        'assets/imgs/user_icon_11.png'
      ]
    }
  ];

  AvatarSelector({
    @required this.walletKey,
  }) : selectedPathListenable = ValueNotifier<String>(walletKey.avatarPath);

  List<Widget> buildSectionGrid(
    BuildContext context,
    Map<String, dynamic> source,
  ) =>
      [
        SliverPadding(
          padding: EdgeInsets.only(
            top: ScreenUtil().setWidth(30),
            left: ScreenUtil().setWidth(20),
            right: ScreenUtil().setWidth(20),
          ),
          sliver: SliverToBoxAdapter(
            child: Text(source['type_name']),
          ),
        ),
        SliverPadding(
          padding: EdgeInsets.all(20),
          sliver: SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 5,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                final srcPath = (source['paths'] as List<String>)[index];
                return GestureDetector(
                  child: Container(
                    padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
                    child: Image.asset(
                      srcPath,
                      fit: BoxFit.cover,
                      package: 'wallet_flutter',
                    ),
                    decoration: BoxDecoration(
                      color: selectedPathListenable.value == srcPath
                          ? ThemeUtils().getColor(
                              'views.keychain.avatar_selector.selected',
                            )
                          : Colors.transparent,
                      borderRadius: BorderRadius.circular(
                        ScreenUtil().setWidth(10),
                      ),
                    ),
                  ),
                  onTap: () {
                    selectedPathListenable.value = srcPath;
                  },
                );
              },
              childCount: (source['paths'] as List<String>).length,
            ),
          ),
        ),
      ];

  List<Widget> buildContentGrid(BuildContext context) {
    final children = <Widget>[];

    avatarSource.forEach(
      (element) => children.addAll(
        buildSectionGrid(context, element),
      ),
    );
    return children;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor:
            ThemeUtils().getColor('utils.view_controller.background'),
        appBar: NavigationBar(
          title: '选择头像',
          bottom: navigatorBarBottom(),
          actions: [
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                walletKey.avatarPath = selectedPathListenable.value;
                Navigator.of(context).pop();
              },
            )
          ],
        ),
        body: ValueListenableBuilder(
          valueListenable: selectedPathListenable,
          builder: (context, value, child) => CustomScrollView(
            physics: BouncingScrollPhysics(),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            slivers: buildContentGrid(context),
          ),
        ),
      );

  PreferredSize navigatorBarBottom() => PreferredSize(
        preferredSize: Size.fromHeight(150),
        child: Container(
          color: ThemeUtils().getColor(
            'utils.navigation_bar.background',
          ),
          height: ScreenUtil().setWidth(150),
          width: double.infinity,
          padding: EdgeInsets.all(ScreenUtil().setWidth(15)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ValueListenableBuilder(
                valueListenable: selectedPathListenable,
                builder: (context, value, child) => Image.asset(
                  value,
                  width: ScreenUtil().setWidth(70),
                  height: ScreenUtil().setWidth(70),
                  package: 'wallet_flutter',
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(20),
                ),
                child: Text(
                  walletKey.nickName,
                  style: ThemeUtils().getTextStyle(
                    'views.keychain.avatar_selector.textstyle.wallet_name',
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
