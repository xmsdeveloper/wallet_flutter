// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screen_util.dart';

import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/keychain/create_tips.dart';
import 'package:wallet_flutter/views/keychain/importer/backup_mnemonics.dart';
import 'package:wallet_flutter/views/keychain/importer/confirm_mnemonics.dart';
import 'package:wallet_flutter/views/keychain/private_display.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/pwdIdentifier.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/views/utils/warming.dart';

class GenerateWalletView extends SliverViewController {
  GenerateWalletView()
      : super(navigationBar: NavigationBar(title: S.current.create_title));

  final controller = _GenerateWalletViewController();

  final errorListenables = [
    ValueNotifier<String>(null),
    ValueNotifier<String>(null),
    ValueNotifier<String>(null),
  ];

  String passwordStrength = "";
  final ValueNotifier<String> passwordStrengthListenable =
      ValueNotifier<String>(null);

  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");

  /// 密码强度正则,表示匹配任意的拉丁大小写字母，数字再加上下划线和减号
  final weakPwdStrengthRegexp = RegExp(
    r'[\w_-]$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含2个大小写字母
  final middlePwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]{2,}).*$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含1个小写字母,必须包含1个大写字母,必须包含1个特殊字符
  final strongPwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*[!@#$%^&*?\(\)]).*$',
  );

  final items = [
    {
      "title": S.current.create_wallet_name,
      "placeholder": S.current.create_wallet_name_placeholder,
      "keyboardType": TextInputType.name,
      "controller": TextEditingController(),
    },
    {
      "title": S.current.create_wallet_pwd,
      "placeholder": S.current.create_wallet_pwd_placeholder,
      "keyboardType": TextInputType.visiblePassword,
      "obscureText": true,
      "controller": TextEditingController(),
    },
    {
      "title": S.current.create_wallet_pwd_confirm,
      "placeholder": S.current.create_wallet_pwd_confirm_placeholder,
      "keyboardType": TextInputType.visiblePassword,
      "obscureText": true,
      "controller": TextEditingController(),
    },
  ];

  Future<void> doGenerate(BuildContext context) async {
    final walletNameController =
        (items[0]['controller'] as TextEditingController);
    final passwordController =
        (items[1]['controller'] as TextEditingController);
    final confirmController = (items[2]['controller'] as TextEditingController);

    if (walletNameController.text.length <= 0) {
      errorListenable.value = S.current.create_wallet_name_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    if (passwordController.text.length <= 0) {
      errorListenable.value = S.current.create_wallet_pwd_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    if (confirmController.text.length <= 0) {
      errorListenable.value = S.current.create_wallet_pwd_confirm_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    if (passwordController.text != confirmController.text) {
      errorListenable.value = S.current.create_wallet_pwd_confirm_different;

      return;
    }

    SystemChannels.textInput.invokeMethod('TextInput.hide');

    return Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        CreatePageTips(
          userName: walletNameController.text,
          userPassword: confirmController.text,
        ),
      ),
    );
  }

  @override
  int sliverViewItemCount() => items.length;

  @override
  Widget sliverViewItemForIndex(BuildContext context, int index) => Container(
        child: ValueListenableBuilder(
            valueListenable: this.errorListenables[index],
            builder: (context, value, child) => index == 0
                ? Column(
                    children: [
                      Input(
                        title: items[index]['title'],
                        placeholder: items[index]['placeholder'],
                        keyboardType: items[index]['keyboardType'],
                        controller: items[index]['controller'],
                        obscureText: items[index]['obscureText'] ?? false,
                        errorText: value,
                      ),
                    ],
                  )
                : index == 1
                    ? Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              items[index]['title'],
                            ),
                          ),
                          Container(
                            child: WarmingPage(
                              tipText: S.current.create_wallet_tips,
                            ),
                          ),
                          Input(
                            title: "",
                            placeholder: items[index]['placeholder'],
                            keyboardType: items[index]['keyboardType'],
                            controller: items[index]['controller'],
                            obscureText: items[index]['obscureText'] ?? false,
                            errorText: value,
                            onChanged: (_) {
                              final passwordController = (items[1]['controller']
                                  as TextEditingController);

                              if (weakPwdStrengthRegexp
                                      .hasMatch(passwordController.text) ==
                                  true) {
                                this.passwordStrength =
                                    S.current.password_strength_weak;
                                this.passwordStrengthListenable.value =
                                    this.passwordStrength;
                              } else {
                                this.passwordStrength = "";
                                this.passwordStrengthListenable.value =
                                    this.passwordStrength;
                              }

                              if (middlePwdStrengthRegexp
                                      .hasMatch(passwordController.text) ==
                                  true) {
                                this.passwordStrength =
                                    S.current.password_strength_middle;
                                this.passwordStrengthListenable.value =
                                    this.passwordStrength;
                              }
                              if (strongPwdStrengthRegexp
                                      .hasMatch(passwordController.text) ==
                                  true) {
                                this.passwordStrength =
                                    S.current.password_strength_strength;
                                this.passwordStrengthListenable.value =
                                    this.passwordStrength;
                              }
                            },
                          ),
                          ValueListenableBuilder(
                            valueListenable: passwordStrengthListenable,
                            builder: (context, value, child) {
                              return this.passwordStrength != ""
                                  ? Container(
                                      margin: EdgeInsets.only(
                                          top: ScreenUtil().setHeight(13)),
                                      child: PwdIdentifier(
                                        passwordStrength: this.passwordStrength,
                                      ),
                                    )
                                  : Container();
                            },
                          )
                        ],
                      )
                    : Column(
                        children: [
                          Input(
                            title: '',
                            placeholder: items[index]['placeholder'],
                            keyboardType: items[index]['keyboardType'],
                            controller: items[index]['controller'],
                            obscureText: items[index]['obscureText'] ?? false,
                            errorText: value,
                          ),
                        ],
                      )),
        padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(32),
          left: ScreenUtil().setWidth(20),
          right: ScreenUtil().setWidth(20),
        ),
      );

  @override
  PreferredSize sliverViewFooter(BuildContext context) => PreferredSize(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: ScreenUtil().setHeight(46)),
              child: ValueListenableBuilder(
                valueListenable: errorListenable,
                builder: (contet, value, child) {
                  return Text(
                    errorListenable.value,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color.fromRGBO(203, 0, 0, 1),
                    ),
                  );
                },
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(25),
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
                bottom: 0,
              ),
              child: StatusButton.withStyle(
                StatusButtonStyle.Info,
                title: S.current.create_title,
                onPressedFuture: () => doGenerate(context),
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(200),
      );
}

class _GenerateWalletViewController {}
