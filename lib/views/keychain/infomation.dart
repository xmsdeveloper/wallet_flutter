// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/views/keychain/avatart_selector.dart';
import 'package:wallet_flutter/views/keychain/information_other.dart';
import 'package:wallet_flutter/views/keychain/private_display.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/command/theme/icon_utils.dart' as CustomIcons;
import 'package:flutter_svg/flutter_svg.dart';

class KeyInfoView extends ViewController {
  final WalletKey walletKey;
  final List datasource = [
    [
      {
        'icon': "assets/images/key.svg",
        'title': S.current.key_info_change_name,
      },
    ],
    [
      {
        'icon': "assets/images/keystore.svg",
        'title': S.current.key_info_export_privatekey,
      },
      {
        'icon': "assets/images/wallet_name.svg",
        'title': S.current.key_info_export_keystore,
      },
    ],
  ];

  KeyInfoView({
    @required this.walletKey,
  }) : super(
          navigationBar: NavigationBar(
            title: S.current.key_info_title,
            elevation: 0.3,
          ),
        );

  void cellOnTap(BuildContext context, int section, int row) async {
    /// 修改名称
    if (section == 0 && row == 0) {
      await ModalInputer.showAlert(
        context,
        initalValue: walletKey.nickName ?? '',
        title: S.current.key_info_change_name,
        placeholder: S.current.key_info_change_name_placeholder,
        shouldEndEditCallback: (text) {
          if (text == null || text.length <= 0) {
            return S.current.key_info_change_name_empty;
          } else {
            return null;
          }
        },
        submited: (text) {
          walletKey.nickName = text;
        },
      );
    }

    /// 导出私钥
    else if (section == 1) {
      VerifySheets.shows(
        context,
        verifyCallback: (context, pwd) {
          final verifySuccess =
              CoreProvider.of(context).wallet.verifySecurity(pwd);
          if (!verifySuccess) {
            // return S.current.password_verify_faild;
            Fluttertoast.showToast(msg: S.current.password_verify_faild);
            return S.current.password_verify_faild;
          } else {
            // FocusScope.of(context).requestFocus(FocusNode());
            Navigator.of(context).pop();
            Navigator.of(context, rootNavigator: true).push(
              ViewAnimations.viewRightIn(
                PrivateKeyDisplay(
                  walletKey: walletKey,
                  encryptPassword: pwd,
                  securityType: row == 0
                      ? SecurityType.PrivateKeyHex
                      : SecurityType.KeystoreJson,
                ),
              ),
            );
            return null;
          }
        },
      );
    }
  }

  Widget buildSection(BuildContext context, int section) => Container(
        child: SliverPadding(
          padding: EdgeInsets.only(top: ScreenUtil().setHeight(2)),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) => buildCell(context, section, index),
              childCount: datasource[section].length,
            ),
          ),
        ),
      );

  Widget buildCell(BuildContext context, int section, int row) =>
      ValueListenableBuilder<WalletKey>(
          valueListenable: CoreProvider.of(context).accountListenable,
          builder: (context, _, child) {
            return Column(
              children: [
                Container(
                  height: ScreenUtil().setHeight(66),
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(10),
                  ),
                  decoration: BoxDecoration(
                    color: ThemeUtils().getColor(
                      'views.chaincore.ethereum.assets.cell_background',
                    ),
                  ),
                  child: ListTile(
                    title: Text(
                      datasource[section][row]['title'],
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Colors.black,
                      ),
                    ),
                    trailing: Container(
                      width: ScreenUtil().setWidth(140),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.arrow_forward_ios_sharp,
                            color: Colors.grey[400],
                            size: ScreenUtil().setHeight(12),
                          ),
                        ],
                      ),
                    ),
                    onTap: () => cellOnTap(context, section, row),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(21),
                    right: ScreenUtil().setWidth(21),
                  ),
                  height: 0.3,
                  color: Color.fromRGBO(216, 216, 216, 1),
                ),
              ],
            );
          });

  @override
  Widget render(BuildContext context) {
    final slivers = <Widget>[
      SliverPersistentHeader(
        delegate: SliverViewControllerHeader(child: sliverViewHeader(context)),
      ),
    ];

    if (CoreProvider.of(context).account.privateKeyRaw != null) {
      for (int section = 0; section < datasource.length; section++) {
        slivers.add(buildSection(context, section));
      }
    }

    /// 如果是根证书的对应类型的第一个地址，不允许删除
    if (!(walletKey.isHDKeypair &&
        walletKey.derivePathAccount == 0 &&
        walletKey.derivePathIndex == 0)) {
      slivers.add(
        SliverPersistentHeader(
          delegate:
              SliverViewControllerHeader(child: sliverViewFooter(context)),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: CustomScrollView(
          physics: BouncingScrollPhysics(),
          slivers: slivers,
        ),
      ),
    );
  }

  PreferredSize sliverViewHeader(BuildContext context) => PreferredSize(
        preferredSize: Size(double.infinity, ScreenUtil().setHeight(128)),
        child: ValueListenableBuilder<WalletKey>(

            /// Infomation页面显示的有可能不是当前选择的私钥，同时用户可能回修改存储的信息
            /// 所以此处监听accountListenable，注意在Builder中不可以使用value字段，因为
            /// value字段一定是当前选择的account。应该使用构造中的walletKey
            valueListenable: CoreProvider.of(context).accountListenable,
            builder: (context, _, child) => GestureDetector(
                  child: Column(
                    children: [
                      Container(
                        height: ScreenUtil().setHeight(118),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                left: ScreenUtil().setWidth(21),
                                right: ScreenUtil().setWidth(15),
                              ),
                              width: ScreenUtil().setWidth(51),
                              height: ScreenUtil().setHeight(51),
                              child: Image.asset(
                                'assets/images/avator.png',
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            walletKey.nickName ?? 'NoName',
                                            style: TextStyle(
                                              fontSize: ScreenUtil().setSp(16),
                                              color: Colors.black,
                                            ),
                                          ),
                                          CoreProvider.of(context)
                                                      .account
                                                      .privateKeyRaw ==
                                                  null
                                              ? Container(
                                                  margin: EdgeInsets.only(
                                                    left: ScreenUtil()
                                                        .setWidth(10),
                                                  ),
                                                  alignment: Alignment.center,
                                                  // height:
                                                  //     ScreenUtil().setHeight(20),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                    border: Border.all(
                                                      width: 1,
                                                      color: Color.fromRGBO(
                                                        175,
                                                        175,
                                                        175,
                                                        1,
                                                      ),
                                                    ),
                                                  ),
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                      left: ScreenUtil()
                                                          .setWidth(5),
                                                      right: ScreenUtil()
                                                          .setWidth(5),
                                                    ),
                                                    child: Center(
                                                      child: Text(
                                                        S.current
                                                            .wallet_watch_name,
                                                        style: TextStyle(
                                                          fontSize: ScreenUtil()
                                                              .setSp(14),
                                                          color: Color.fromRGBO(
                                                            175,
                                                            175,
                                                            175,
                                                            1,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : Container()
                                        ],
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(10)),
                                      InkWell(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        child: Row(
                                          children: [
                                            Opacity(
                                              opacity: 0.6,
                                              child: Text(
                                                walletKey.address,
                                                style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(12),
                                                  color: Color.fromRGBO(
                                                    175,
                                                    175,
                                                    175,
                                                    1,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // Text("188,888,991.00"),
                              ],
                            ),
                            Expanded(
                              child: SizedBox(),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: ScreenUtil().setHeight(10),
                        width: MediaQuery.of(context).size.width,
                        color: Color.fromRGBO(246, 246, 246, 1),
                      ),
                    ],
                  ),
                )),
      );

  PreferredSize sliverViewFooter(BuildContext context) => PreferredSize(
        child: Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setHeight(20),
            right: ScreenUtil().setHeight(20),
            top: ScreenUtil().setHeight(60),
          ),
          child: StatusButton.withStyle(
            StatusButtonStyle.Important,
            title: S.current.key_info_remove,
            onPressedFuture: () {
              Navigator.of(context).push(ViewAnimations.viewRightIn(
                OtherPage(
                  walletKey: this.walletKey,
                ),
              ));

              return Future.value(null);
            },
          ),
        ),
        preferredSize: Size.fromHeight(300),
      );
}
