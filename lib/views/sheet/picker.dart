// @dart=2.9
part of 'sheet.dart';

class BottomPicker extends StatelessWidget {
  final String title;
  final List<Widget> children;

  BottomPicker({
    this.title,
    this.children,
  });

  static show(
    BuildContext context, {
    @required String title,
    @required List<Widget> children,
  }) =>
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => BottomPicker(
          title: title,
          children: children,
        ),
      );

  @override
  Widget build(BuildContext context) => ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: Scaffold(
          appBar: NavigationBar(
            isBottomSheet: true,
            title: this.title,
          ),
          backgroundColor: ThemeUtils().getColor(
            'sheet.background',
          ),
          body: Container(
            width: double.infinity,
            margin: EdgeInsets.only(
              top: 10,
            ),
            child: Column(
              children: children,
            ),
            // color: Colors.red,
          ),
        ),
      );
}
