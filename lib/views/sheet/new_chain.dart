import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/views/utils/finish_animation.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

class NewChainPage extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;
  const NewChainPage({required this.dataSourceProvider});
  @override
  _NewChainPageState createState() => _NewChainPageState();
}

class _NewChainPageState extends State<NewChainPage> {
  //定义表单
  late List nodeformlist;

  final TextEditingController chainTextEditingController =
      TextEditingController(text: '');

  final TextEditingController nameTextEditingController =
      TextEditingController(text: '');
  final TextEditingController netTextEditingController =
      TextEditingController(text: '');
  final TextEditingController maincoinsymbolTextEditingController =
      TextEditingController(text: '');
  final TextEditingController maincoinnameTextEditingController =
      TextEditingController(text: '');
  final TextEditingController decimalsTextEditingController =
      TextEditingController(text: '');
  final TextEditingController logoTextEditingController =
      TextEditingController(text: '');
  final TextEditingController nodeTextEditingController =
      TextEditingController(text: '');
  final TextEditingController scanTextEditingController =
      TextEditingController(text: '');
  final TextEditingController keyTextEditingController =
      TextEditingController(text: '');
  final TextEditingController dataTextEditingController =
      TextEditingController(text: '');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //表单标题，提示，图标信息
    nodeformlist = [
      // {
      //   "title": S.current.deeplink_network_add_identifier,
      //   "hittext": "",
      //   "icon": "*",
      //   TextEditingController: chainTextEditingController,
      // },
      {
        "title": S.current.deeplink_network_add_chain_name,
        "hittext": "",
        "icon": "*",
        TextEditingController: nameTextEditingController,
      },
      {
        "title": S.current.deeplink_network_add_id,
        "hittext": "1280",
        "icon": "",
        TextEditingController: netTextEditingController
      },
      {
        "title": S.current.deeplink_network_add_mainSymbol,
        "hittext": "ETH/HT.....",
        "icon": "*",
        TextEditingController: maincoinsymbolTextEditingController
      },
      {
        "title": S.current.deeplink_network_add_logo_link_address,
        "hittext": "https://www.xxxxxxxxxxxxx.png",
        "icon": "",
        TextEditingController: logoTextEditingController
      },
      {
        "title": S.current.deeplink_network_add_node_server_address,
        "hittext": "https://www.xxxxxxxxxxxxx",
        "icon": "*",
        TextEditingController: nodeTextEditingController
      },
    ];
  }

  Future send() async {
    if (nameTextEditingController.text == "" ||
        nodeTextEditingController.text == '' ||
        maincoinsymbolTextEditingController.text == '') {
      Fluttertoast.showToast(msg: S.current.deeplink_network_add_empty);
      return;
    }

    await NetworkStorageUtils().putNetwork(
      nameTextEditingController.text.trim(),
      NetworkConfig(
          nameTextEditingController.text,
          nameTextEditingController.text,
          nameTextEditingController.text,
          60,
          maincoinsymbolTextEditingController.text,
          18,
          maincoinnameTextEditingController.text,
          logoTextEditingController.text.trim(),
          logoTextEditingController.text.trim(),
          0,
          [
            EndPoint(
              nodeTextEditingController.text.trim(),
              null,
              null,
              null,
            )
          ],
          netTextEditingController.text.trim(),
          {
            "safe": "10e9",
            "propose": "20e9",
            "fast": "30e9",
          },
          "DataProvider > Default",
          [
            "Hashpay.CoreModule.Asset",
            "Hashpay.CoreModule.DappCostom",
            "Hashpay.CoreModule.Setting"
          ],
          "ETH-customize"),
    );
    // await this.widget.dataSourceProvider.localService.accountListInsert(
    //       NetworkConfig(
    //         nameTextEditingController.text.trim(),
    //         nameTextEditingController.text,
    //         nameTextEditingController.text,
    //         60,
    //         maincoinsymbolTextEditingController.text,
    //         18,
    //         maincoinnameTextEditingController.text,
    //         logoTextEditingController.text.trim(),
    //         logoTextEditingController.text.trim(),
    //         0,
    //         [
    //           EndPoint(
    //             nodeTextEditingController.text.trim(),
    //             null,
    //             null,
    //             null,
    //           )
    //         ],
    //         netTextEditingController.text.trim(),
    //         {
    //           "safe": "10e9",
    //           "propose": "20e9",
    //           "fast": "30e9",
    //         },
    //         "DataProvider > Default",
    //         [
    //           "Hashpay.CoreModule.Asset",
    //           "Hashpay.CoreModule.DappCostom",
    //           "Hashpay.CoreModule.Setting"
    //         ],
    //       ),
    //     );

    FinishAnimation.show(context, onCompleted: () {
      Navigator.of(context).pop<String>();
    });
  }

  @override
  void dispose() {
    super.dispose();
    // 离开页面统计（手动采集时才可设置）
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: NavigationBar(
        title: S.current.deeplink_network_add,
      ),
      body: Container(
        child: CustomScrollView(
          slivers: [
            //表单输入栏
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(0.0, 1.0), //阴影xy轴偏移量
                        blurRadius: 1.0, //阴影模糊程度
                        spreadRadius: 1.0 //阴影扩散程度
                        )
                  ],
                  color: Colors.white,
                ),
                padding: EdgeInsets.only(
                  left: ScreenUtil().setHeight(20),
                  right: ScreenUtil().setHeight(20),
                  top: ScreenUtil().setHeight(20),
                  bottom: ScreenUtil().setHeight(20),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: nodeformlist
                      .map(
                        (e) => Column(
                          children: [
                            Container(
                              height: ScreenUtil().setHeight(30),
                              child: Row(
                                // mainAxisAlignment:
                                //     MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    e['title'],
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(16),
                                    ),
                                  ),
                                  Text(
                                    e['icon'],
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                      width: 0.5,
                                      color: Color.fromRGBO(216, 216, 216, 1)),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Container(
                                    // width: ScreenUtil().setWidth(260),

                                    width: MediaQuery.of(context).size.width -
                                        ScreenUtil().setWidth(80),
                                    child: TextFormField(
                                      controller: e[TextEditingController],

                                      // focusNode: inputFocusNode,
                                      // controller:
                                      //     addressEditingController,
                                      autofocus: false,
                                      onChanged: (_) => setState(() {}),
                                      style: ThemeUtils()
                                          .getTextStyle(
                                            'utils.input.textstyle.content',
                                          )
                                          .copyWith(fontSize: 15),
                                      decoration: InputDecoration(
                                        // hintText: S.current
                                        //     .ethereum_home_address_selector_placeholder,
                                        // hintStyle:
                                        //     ThemeUtils().getTextStyle(
                                        //   'utils.input.textstyle.hit',
                                        // ),
                                        hintText: e['hittext'],
                                        hintStyle: ThemeUtils().getTextStyle(
                                          'utils.input.textstyle.hit',
                                        ),
                                        suffixIconConstraints:
                                            BoxConstraints.expand(
                                          width: ScreenUtil().setWidth(40),
                                          height: ScreenUtil().setHeight(40),
                                        ),
                                        border: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        prefixIconConstraints:
                                            BoxConstraints.expand(
                                          width: ScreenUtil().setWidth(40),
                                          height: ScreenUtil().setHeight(40),
                                        ),
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   margin: EdgeInsets.only(
                                  //       left: ScreenUtil().setWidth(20)),
                                  //   width: ScreenUtil().setWidth(20),
                                  //   child: IconButton(
                                  //     padding: EdgeInsets.all(0),
                                  //     // icon: Icon(Icons.crop_free_sharp),
                                  //     icon: e['icon'] != ""
                                  //         ? Image.asset(
                                  //             e['icon'],
                                  //             package: "wallet_flutter",
                                  //           )
                                  //         : Container(),
                                  //     onPressed: () async {},
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                          ],
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
            //确定拦
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setHeight(20),
                  vertical: ScreenUtil().setHeight(40),
                ),
                child: StatusButton.withStyle(StatusButtonStyle.Info,
                    title: S.current.sheet_ok, onPressedFuture: send),
              ),
            )
          ],
        ),
      ),
    );
  }
}
