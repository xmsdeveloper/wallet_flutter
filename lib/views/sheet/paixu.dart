import 'package:flutter/material.dart';

class PaixuPage extends StatefulWidget {
  const PaixuPage({Key? key}) : super(key: key);

  @override
  State<PaixuPage> createState() => _PaixuPageState();
}

class _PaixuPageState extends State<PaixuPage> {
  List<String> items = List.generate(20, (int i) => '$i');
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ReorderableListView(
        children: <Widget>[
          for (String item in items)
            Container(
              key: ValueKey(item),
              height: 100,
              margin: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
              decoration: BoxDecoration(
                  color: Colors
                      .primaries[int.parse(item) % Colors.primaries.length],
                  borderRadius: BorderRadius.circular(10)),
            )
        ],
        onReorder: (int oldIndex, int newIndex) {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          var child = items.removeAt(oldIndex);
          items.insert(newIndex, child);
          setState(() {});
        },
      ),
    );
  }
}
