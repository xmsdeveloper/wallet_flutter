import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/command/cache_image.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/views/sheet/new_chain.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/finish_animation.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/command/theme/icon_utils.dart' as CustomIcons;

class AddChainPage extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;
  const AddChainPage({required this.dataSourceProvider});

  @override
  _AddChainPageState createState() => _AddChainPageState();
}

List<NetworkConfig> chaingroup = [];

class _AddChainPageState extends State<AddChainPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }

  @override
  void dispose() {
    super.dispose();
    // animationController.dispose();
  }

  getdata() async {
    chaingroup.clear();
    await this
        .widget
        .dataSourceProvider
        .localService
        .accountList()
        .then((value) => value.map((e) => chaingroup.add(e)).toList());
    setState(() {});
  }

  Future<String> mockNetworkData(String i) async {
    return i;
  }

  Future<NetworkConfig> mockNetworkDatas(NetworkConfig i) async {
    return i;
  }

  send() async {
    await NetworkStorageUtils().putNetwork(
      "0xac441e74271f92eafb8be0783c17151d342597c042d6635a89db9bd33c7c646ed",
      NetworkConfig(
          "0xac441e74271f92eafb8be0783c17151d342597c042d6635a89db9bd33c7c646ed",
          "DT",
          "DT",
          60,
          "DT Network Chain Coin",
          // int.parse("18"),
          18,
          "DT Network Chain Coin",
          "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
          "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
          0,
          [
            EndPoint(
              "http://alpha.daison.com.cn:3000",
              "http://dt.hashpaywallet.com",
              "http://delta.daison.com.cn",
              null,
            )
          ],
          "3000",
          {
            "safe": "10e9",
            "propose": "20e9",
            "fast": "30e9",
          },
          "DataProvider > Default",
          [
            "Hashpay.CoreModule.Asset",
            "Hashpay.CoreModule.Find",
            "Hashpay.CoreModule.Setting"
          ],
          "ETH"),
    );
    FinishAnimation.show(context, onCompleted: () {
      Navigator.of(context).pop<String>();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: NavigationBar(
        title: S.current.deeplink_network_add,
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: CustomScrollView(
          slivers: [
            //表单输入栏
            SliverToBoxAdapter(
              child: Column(
                children: [
                  Container(
                    color: Color(0xFFF5F5F5),
                    padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(20),
                    ),
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    height: ScreenUtil().setHeight(60),
                    child: Text(
                      '已展示的网络',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  // FutureBuilder<List<String>>(
                  //   future: mockNetworkData(),
                  //   builder: (BuildContext context, AsyncSnapshot snapshot) {
                  //     // 请求已结束
                  //     if (snapshot.connectionState == ConnectionState.done) {
                  //       if (snapshot.hasError) {
                  //         // 请求失败，显示错误
                  //         return Text("Error: ${snapshot.error}");
                  //       } else {
                  //         // 请求成功，显示数据
                  //         // return Text("Contents: ${snapshot.data}");
                  //         return Column(
                  //           children: snapshot.data.map<Widget>((e)=>

                  //           ).tolist(),
                  //         );
                  //       }
                  //     } else {
                  //       // 请求未结束，显示loading
                  //       return CircularProgressIndicator();
                  //     }
                  //   },
                  // ),
                  Column(
                    children: NetworkStorageUtils().chainNames.map<Widget>((e) {
                      return Container(
                        child: Container(
                          // key: ValueKey(i),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  width: 0.5,
                                  color: Color(0xFFEEEEEE),
                                ),
                              ),
                              color: Colors.white,
                            ),
                            padding: EdgeInsets.only(
                              // top: ScreenUtil().setHeight(26),
                              left: ScreenUtil().setWidth(18),
                              right: ScreenUtil().setWidth(20),
                            ),
                            height: ScreenUtil().setHeight(69),
                            child: Row(
                              children: [
                                // CoreProvider.of(context).networkConfig.mainSymbol == "ETH"
                                //     ?
                                Container(
                                  alignment: Alignment.center,
                                  // padding: EdgeInsets.all(ScreenUtil().setHeight(5)),
                                  width: ScreenUtil().setWidth(40),
                                  height: ScreenUtil().setHeight(40),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 0.5,
                                      color: Colors.grey,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                  child: ClipPath.shape(
                                      shape: CircleBorder(),
                                      // child: FutureBuilder<String>(
                                      //     future: mockNetworkData(e),
                                      //     builder: (BuildContext context,
                                      //             AsyncSnapshot snapshot) =>
                                      //         Container(
                                      //           child: CachedImage.assetNetwork(
                                      //             // url:
                                      //             //     "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
                                      //             // url: e.logoURL,
                                      //             url: NetworkStorageUtils()
                                      //                 .networkConfigOf(
                                      //                     snapshot.data)
                                      //                 .logoURL,

                                      //             placeholder: Image.asset(
                                      //               'assets/imgs/default_token.png',
                                      //               package: 'wallet_flutter',
                                      //               fit: BoxFit.fill,
                                      //             ),
                                      //           ),
                                      //         )
                                      child: CachedImage.assetNetwork(
                                        // url:
                                        //     "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
                                        // url: e.logoURL,
                                        url: NetworkStorageUtils()
                                            .networkConfigOf(e)
                                            .logoURL,
                                        placeholder: Image.asset(
                                          'assets/imgs/default_token.png',
                                          package: 'wallet_flutter',
                                          fit: BoxFit.fill,
                                        ),
                                      )
                                      // clipper: _MyClipper(),
                                      // child: CachedImage.assetNetwork(
                                      //   // url:
                                      //   //     "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
                                      //   // url: e.logoURL,
                                      //   url: NetworkStorageUtils()
                                      //       .networkConfigOf(e)
                                      //       .logoURL,
                                      //   placeholder: Image.asset(
                                      //     'assets/imgs/default_token.png',
                                      //     package: 'wallet_flutter',
                                      //     fit: BoxFit.fill,
                                      //   ),
                                      //   width: ScreenUtil().setWidth(30),
                                      //   height: ScreenUtil().setWidth(30),
                                      //   imageCacheHeight: 100,
                                      //   imageCacheWidth: 100,
                                      // ),
                                      // child: Image(
                                      //   image: NetworkImage(
                                      //     NetworkStorageUtils()
                                      //         .networkConfigOf(e)
                                      //         .logoURL,
                                      //   ),
                                      // ),
                                      ),
                                ),

                                SizedBox(
                                  width: ScreenUtil().setWidth(15),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      // 'DT',
                                      // e.chainName,

                                      // style: ThemeUtils().getTextStyle(
                                      //   'views.chaincore.ethereum.assets.textstyle.cell_title',
                                      // ),
                                      NetworkStorageUtils()
                                          .networkConfigOf(e)
                                          .chainName,
                                    ),
                                    SizedBox(height: ScreenUtil().setWidth(2)),
                                    // Text(
                                    //   "DT Network Chain Coin",
                                    //   style: ThemeUtils().getTextStyle(
                                    //     'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                                    //   ),
                                    // ),
                                  ],
                                ),
                                Expanded(child: SizedBox()),
                                InkWell(
                                  onTap: () async {
                                    if (NetworkStorageUtils()
                                            .chainNames
                                            .length >
                                        -1) {
                                      await this
                                          .widget
                                          .dataSourceProvider
                                          .localService
                                          .accountListInsert(
                                              NetworkStorageUtils()
                                                  .networkConfigOf(e));

                                      //从网络列表中删除该链
                                      NetworkStorageUtils().deleteNetwork(e);

                                      getdata();
                                    } else {
                                      Fluttertoast.showToast(
                                        msg: "至少展示一个",
                                      );
                                    }
                                  },
                                  child: Container(
                                    // child: Icon(
                                    //   Icons.arrow_forward_ios,
                                    //   size: ScreenUtil().setHeight(15),
                                    // ),
                                    child: Icon(
                                      // CustomIcons.manage_your_wallet,
                                      IconData(
                                        0xe633,
                                        fontFamily: "myIcon",
                                        fontPackage: "wallet_flutter",
                                      ),

                                      color: Colors.grey,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }).toList(),
                  ),

                  Container(
                    color: Color(0xFFF5F5F5),
                    padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(20),
                    ),
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    height: ScreenUtil().setHeight(60),
                    child: Text(
                      '未展示的网络',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Column(
                    children: chaingroup
                        .map(
                          (e) => Container(
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  width: 0.5,
                                  color: Color(0xFFEEEEEE),
                                ),
                              ),
                              color: Colors.white,
                            ),
                            padding: EdgeInsets.only(
                              // top: ScreenUtil().setHeight(26),
                              left: ScreenUtil().setWidth(18),
                              right: ScreenUtil().setWidth(20),
                            ),
                            height: ScreenUtil().setHeight(69),
                            child: Row(
                              children: [
                                // CoreProvider.of(context).networkConfig.mainSymbol == "ETH"
                                //     ?
                                Container(
                                  alignment: Alignment.center,
                                  // padding: EdgeInsets.all(ScreenUtil().setHeight(5)),
                                  width: ScreenUtil().setWidth(40),
                                  height: ScreenUtil().setHeight(40),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 0.5,
                                      color: Colors.grey,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                  child: ClipPath.shape(
                                    shape: CircleBorder(),
                                    // clipper: _MyClipper(),
                                    // child: FutureBuilder<NetworkConfig>(
                                    //   future: mockNetworkDatas(e),
                                    //   builder: (BuildContext context,
                                    //           AsyncSnapshot snapshot) =>
                                    //       Container(
                                    //     child: CachedImage.assetNetwork(
                                    //       // url:
                                    //       //     "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
                                    //       // url: e.logoURL,
                                    //       url: snapshot.data.logoURL,

                                    //       placeholder: Image.asset(
                                    //         'assets/imgs/default_token.png',
                                    //         package: 'wallet_flutter',
                                    //         fit: BoxFit.fill,
                                    //       ),
                                    //     ),
                                    //   ),
                                    // ),
                                    child: CachedImage.assetNetwork(
                                      // url:
                                      //     "https://s3.ap-northeast-2.amazonaws.com/halo.land/logo%26banner/dt.png",
                                      url: e.logoURL,
                                      placeholder: Image.asset(
                                        'assets/imgs/default_token.png',
                                        package: 'wallet_flutter',
                                        fit: BoxFit.fill,
                                      ),
                                      width: ScreenUtil().setWidth(30),
                                      height: ScreenUtil().setWidth(30),
                                      imageCacheHeight: 100,
                                      imageCacheWidth: 100,
                                    ),
                                  ),
                                ),

                                SizedBox(width: ScreenUtil().setWidth(15)),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      // 'DT',
                                      e.chainName,
                                      // style: ThemeUtils().getTextStyle(
                                      //   'views.chaincore.ethereum.assets.textstyle.cell_title',
                                      // ),
                                    ),
                                    SizedBox(height: ScreenUtil().setWidth(2)),
                                    // Text(
                                    //   "DT Network Chain Coin",
                                    //   style: ThemeUtils().getTextStyle(
                                    //     'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                                    //   ),
                                    // ),
                                  ],
                                ),
                                Expanded(child: SizedBox()),
                                InkWell(
                                  onTap: () async {
                                    //删除列表网络该记录
                                    await this
                                        .widget
                                        .dataSourceProvider
                                        .localService
                                        .accountListDelete(e.identifier);
                                    //从网络列表中增加该链
                                    NetworkStorageUtils()
                                        .putNetwork(e.identifier, e);

                                    getdata();
                                  },
                                  child: Container(
                                    // child: Icon(
                                    //   Icons.arrow_forward_ios,
                                    //   size: ScreenUtil().setHeight(15),
                                    // ),
                                    child: Icon(
                                      // CustomIcons.manage_your_wallet,
                                      IconData(
                                        0xe644,
                                        fontFamily: "myIcon",
                                        fontPackage: "wallet_flutter",
                                      ),

                                      color: Colors.grey,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(24),
                      right: ScreenUtil().setWidth(20),
                    ),
                    height: 1,
                    color: Color.fromRGBO(241, 241, 241, 1),
                  ),
                  InkWell(
                    onTap: () async {
                      await Navigator.of(context).push(
                        ViewAnimations.pushFadeIn(
                          NewChainPage(
                            dataSourceProvider: this.widget.dataSourceProvider,
                          ),
                        ),
                      );

                      getdata();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white38,
                      ),
                      margin: EdgeInsets.only(
                        left: ScreenUtil().setHeight(15),
                        right: ScreenUtil().setHeight(15),
                        top: ScreenUtil().setHeight(66),
                      ),
                      padding: EdgeInsets.only(
                        // top: ScreenUtil().setHeight(26),
                        left: ScreenUtil().setWidth(20),
                        right: ScreenUtil().setWidth(28),
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: ScreenUtil().setHeight(69),
                      child: Center(
                        child: Text(S.current.deeplink_network_add_custom),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
