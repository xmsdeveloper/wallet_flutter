import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/sheet/wallet_select/batch_import.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

class BatchPage extends StatefulWidget {
  final Web3ClientProvider web3Client;

  const BatchPage({Key? key, required this.web3Client}) : super(key: key);
  @override
  _BatchPageState createState() => _BatchPageState();
}

class _BatchPageState extends State<BatchPage> {
  List sampleData = [];
  String selectNums = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sampleData.add(new RadioModel(
      true,
      '10',
    ));
    sampleData.add(new RadioModel(
      false,
      '50',
    ));
    sampleData.add(new RadioModel(
      false,
      '100',
    ));
    _getdw();
  }

  _getdw() async {
    final key = await HDWalletStateMachine.getPrivateKeyForBIP44(
      chainCore: CoreProvider.of(context).networkConfig.chainCore,
      accountIndex: 0,
      changeIndex: 0,
      addressIndex: 1,
    );

    print(bytesToHex(hexToBytes(key)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.wallet_batch_import_title,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
                top: ScreenUtil().setHeight(17),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //阴影xy轴偏移量
                      blurRadius: 1.0, //阴影模糊程度
                      spreadRadius: 0 //阴影扩散程度
                      )
                ],
              ),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(15),
                      bottom: ScreenUtil().setHeight(14),
                      top: ScreenUtil().setHeight(12),
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      S.current.wallet_batch_import_search,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Color.fromRGBO(51, 51, 51, 1),
                      ),
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(77),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Color.fromRGBO(234, 239, 248, 1),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(11),
                          ),
                          width: ScreenUtil().setWidth(80),
                          height: ScreenUtil().setHeight(32),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(238, 238, 238, 1),
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                              width: 1,
                              color: Color.fromRGBO(228, 228, 228, 1),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              '1',
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(22),
                          height: ScreenUtil().setHeight(8),
                          child: Image.asset(
                            'assets/imgs/to.png',
                            package: "wallet_flutter",
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: ScreenUtil().setWidth(11),
                          ),
                          width: ScreenUtil().setWidth(80),
                          height: ScreenUtil().setHeight(32),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(238, 238, 238, 1),
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                              width: 1,
                              color: Color.fromRGBO(228, 228, 228, 1),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              '100',
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(42),
                    // decoration: BoxDecoration(
                    //   border: Border.all(
                    //     width: 1,
                    //     color: Color.fromRGBO(234, 239, 248, 1),
                    //   ),
                    // ),
                    child: Row(
                      children: [
                        // Container(
                        //   margin: EdgeInsets.only(
                        //     left: ScreenUtil().setWidth(10),
                        //     right: ScreenUtil().setWidth(9),
                        //   ),
                        //   decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(5),
                        //     border: Border.all(
                        //         width: 1,
                        //         color: Color.fromRGBO(42, 85, 247, 1)),
                        //   ),
                        //   width: ScreenUtil().setWidth(25),
                        //   height: ScreenUtil().setHeight(25),
                        //   child: Center(
                        //     child: Icon(
                        //       Icons.check,
                        //       size: ScreenUtil().setHeight(22),
                        //       color: Color.fromRGBO(42, 85, 247, 1),
                        //     ),
                        //   ),
                        // ),
                        Container(
                          margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(15),
                          ),
                          child: Text(
                            S.current.wallet_batch_import_private_key_nums,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              color: Color.fromRGBO(8, 8, 8, 1),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(77),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Color.fromRGBO(234, 239, 248, 1),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: List.generate(
                        sampleData.length,
                        (index) {
                          return InkWell(
                            child: _buttonstyle(sampleData[index]),
                            onTap: () {
                              setState(() {
                                sampleData.forEach(
                                    (element) => element.isSelected = false);
                                this.selectNums = sampleData[index].buttonText;

                                sampleData[index].isSelected = true;
                              });
                            },
                          );
                        },
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(ViewAnimations.viewRightIn(
                        // ignore: unnecessary_null_comparison
                        BatchImportPage(
                          selectNums:
                              this.selectNums == "" ? "10" : this.selectNums,
                          web3Client: this.widget.web3Client,
                        ),
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(45),
                          bottom: ScreenUtil().setHeight(45),
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(20)),
                      height: ScreenUtil().setHeight(48),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color.fromRGBO(83, 106, 231, 1),
                      ),
                      child: Center(
                        child: Text(
                          S.current.wallet_batch_import_title,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buttonstyle(sampleData) {
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(10),
        right: ScreenUtil().setWidth(10),
      ),
      child: Stack(
        children: [
          Container(
            width: ScreenUtil().setWidth(80),
            height: ScreenUtil().setHeight(32),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: sampleData.isSelected
                  ? Color.fromRGBO(42, 85, 247, 1)
                  : Colors.white,
              border: Border.all(
                width: 1,
                color: Color.fromRGBO(42, 85, 247, 1),
                // color: sampleData.isSelected ? Colors.blue : Colors.grey,
              ),
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(80),
            height: ScreenUtil().setHeight(32),
            child: Center(
                child: Text(
              "${sampleData.buttonText}",
              style: TextStyle(
                color: sampleData.isSelected
                    ? Colors.white
                    : Color.fromRGBO(42, 85, 247, 1),
              ),
            )),
          ),
        ],
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;

  RadioModel(
    this.isSelected,
    this.buttonText,
  );
}
