// @dart=2.9
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:loading_indicator_view/loading_indicator_view.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/custom_radio.dart';
import 'package:wallet_flutter/views/utils/finish_animation.dart';
import 'package:wallet_flutter/views/utils/loading_view.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

class CreateAccount extends StatefulWidget {
  const CreateAccount({Key key});

  @override
  State<CreateAccount> createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  final ValueNotifier<DateTime> refreshListenable =
      ValueNotifier<DateTime>(DateTime.now());
  TextEditingController textEditingController;

  /// 当前选中的网络
  NetworkConfig selectedNetworkConfig;

  ValueNotifier<String> count = ValueNotifier<String>("0");

  int counts = 0;

  String isSelect = S.current.create_wallet_batch_one;
  int creatNume = 1;

  List tips = [
    {
      "tips": S.current.create_wallet_batch_one,
      "num": 1,
      "isSelect": false,
    },
    {
      "tips": S.current.create_wallet_batch_ten,
      "num": 10,
      "isSelect": false,
    },
    {
      "tips": S.current.create_wallet_batch_fifty,
      "num": 50,
      "isSelect": false,
    },
    {
      "tips": S.current.create_wallet_batch_hundred,
      "num": 100,
      "isSelect": false,
    }
  ];

  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();

    selectedNetworkConfig = CoreProvider().networkConfig;
    Future.delayed(Duration(milliseconds: 2000), () {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16),
                      ),
                    ),
                    height: ScreenUtil().setHeight(56),
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Text(
                        S.current.create_wallet_batch_title,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          color: Color.fromRGBO(51, 51, 51, 1),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )),
                Container(
                  height: 0.5,
                  width: MediaQuery.of(context).size.width,
                  color: Color.fromRGBO(216, 216, 216, 1),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(29)),
                  child: Column(
                    children: tips
                        .map((e) => Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    bottom: e["num"] == 1
                                        ? ScreenUtil().setHeight(22)
                                        : ScreenUtil().setHeight(16),
                                  ),
                                  child: MyRadioOption(
                                    value: e['tips'],
                                    groupValue: isSelect,
                                    lable: '',
                                    text: e["tips"],
                                    onChange: (value) {
                                      setState(
                                        () {
                                          isSelect = value;
                                          this.creatNume = e['num'];
                                        },
                                      );
                                    },
                                  ),
                                ),
                                e["num"] == 1
                                    ? Container(
                                        margin: EdgeInsets.only(
                                          left: ScreenUtil().setWidth(23),
                                          right: ScreenUtil().setWidth(23),
                                          bottom: ScreenUtil().setHeight(26),
                                        ),
                                        height: ScreenUtil().setHeight(36),
                                        child: TextFormField(
                                          enabled: true,
                                          controller: textEditingController,
                                          obscureText: false,
                                          autofocus: false,
                                          onChanged: (value) {},
                                          decoration: InputDecoration(
                                            contentPadding:
                                                const EdgeInsets.symmetric(
                                              vertical: 0.0,
                                              horizontal: 11,
                                            ),
                                            fillColor: Color.fromRGBO(
                                                246, 246, 246, 1),
                                            focusColor: ThemeUtils().getColor(
                                              'utils.input.fill_color',
                                            ),
                                            filled: true,
                                            border: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(ScreenUtil()
                                                      .setWidth(4))),
                                              borderSide: BorderSide(
                                                color: Colors.transparent,
                                              ),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(ScreenUtil()
                                                      .setWidth(4))),
                                              borderSide: BorderSide(
                                                color: Colors.transparent,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(
                                                  ScreenUtil().setWidth(4),
                                                ),
                                              ),
                                              borderSide: BorderSide(
                                                color: Colors.transparent,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(
                                                  ScreenUtil().setWidth(4),
                                                ),
                                              ),
                                              borderSide: BorderSide(
                                                color: Colors.transparent,
                                              ),
                                            ),
                                            hintText: S.current
                                                .create_wallet_batch_address_name,
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                              color: Color.fromRGBO(
                                                  175, 175, 175, 1),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Container(),
                              ],
                            ))
                        .toList(),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: ScreenUtil().setHeight(8),
                    right: ScreenUtil().setWidth(20),
                    left: ScreenUtil().setWidth(20),
                    bottom: ScreenUtil().setHeight(10),
                  ),
                  child: Text(
                    S.current.wallet_selector_new_desc,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(12),
                      color: Color.fromRGBO(175, 175, 175, 1),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(ScreenUtil().setWidth(20)),
                  child: StatusButton.withStyle(
                    StatusButtonStyle.Info,
                    title: S.current.sheet_ok,
                    onPressedFuture: () async {
                      return await Future.delayed(
                        Duration(milliseconds: 0),
                        () {
                          CircularLoading(context,
                                  S.current.create_wallet_batch_loading)
                              .show();

                          for (var i = 0; i < creatNume; i++) {
                            Future.delayed(
                              Duration(milliseconds: 1000),
                              () {
                                // print(i);
                                CoreProvider.of(context)
                                    .wallet
                                    .newKeypair(
                                      type: selectedNetworkConfig.chainCore,
                                      num: i,
                                      accountName: textEditingController.text,
                                    )
                                    .then(
                                  (key) async {
                                    // counts = i;
                                    if (creatNume == i + 1) {
                                      Navigator.of(context).pop<WalletKey>(key);
                                      // CircularLoading(context, "已完成").show();
                                      CircularLoading(
                                              context,
                                              S.current
                                                  .create_wallet_batch_loading)
                                          .dismiss();
                                      FinishAnimation.show(context,
                                          onCompleted: null);
                                    }
                                  },
                                );
                              },
                            );
                          }
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
            Positioned(
              top: 0,
              right: ScreenUtil().setWidth(15),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  // courl = [];
                },
                child: Container(
                  height: ScreenUtil().setHeight(56),
                  width: ScreenUtil().setWidth(40),
                  child: Icon(
                    Icons.close,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
