// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/token_utils.dart'
    as Utils;

class WaAcountDetail extends StatefulWidget {
  final WalletKey account;
  final int index;
  final Web3ClientProvider web3clientProvider;
  final List<WalletKey> accountGroups;
  final DataSourceProvider dataSourceProvider;
  final TokenBlog mainCoin;
  final Color homeColor;

  WaAcountDetail(
    this.account,
    this.index,
    this.web3clientProvider,
    this.accountGroups,
    this.dataSourceProvider,
    this.mainCoin,
    this.homeColor,
  );

  @override
  State<WaAcountDetail> createState() => _WaAcountDetailState();
}

class _WaAcountDetailState extends State<WaAcountDetail> {
  /// 当前选中的网络
  NetworkConfig selectedNetworkConfig;
  //当前选中的账号
  WalletKey selectAccount;
  //监听余额变化
  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(null);
  @override
  void initState() {
    super.initState();
    selectedNetworkConfig = CoreProvider().networkConfig;
    selectAccount = CoreProvider().account;
  }

//修改账户名称
  void _uppateAccountName() async {
    await ModalInputer.showAlert(
      context,
      initalValue: widget.account.nickName ?? '',
      title: S.current.key_info_change_name,
      placeholder: S.current.key_info_change_name_placeholder,
      shouldEndEditCallback: (text) {
        if (text == null || text.length <= 0) {
          return S.current.key_info_change_name_empty;
        } else {
          return null;
        }
      },
      submited: (text) {
        widget.account.nickName = text;
      },
    );
  }

  void _copyAccountAdress() async {
    await Clipboard.setData(
      ClipboardData(
        text: widget.account.address,
      ),
    );
    Fluttertoast.showToast(
      msg: S.current.copy_success,
    );
  }

  void _switchAccount() {
    if (selectAccount == widget.account) {
      // Navigator.of(context).pop();
    } else {
      /// 切换地址
      if (selectAccount != widget.account) {
        CoreProvider.of(context).account = widget.account;
      }
    }
    Navigator.of(context, rootNavigator: true).pop();
    setState(() {});
  }

  //获取余额
  Container balanceUpdate() {
    return Container(
      width: 0,
      height: 0,
      child: Text(widget.dataSourceProvider.localService
          .getCachedBalance(widget.mainCoin, widget.account.address)
          .then(
        (cacheBalance) {
          {
            if (cacheBalance != null) {
              this.balanceListenable.value = Amount(
                value: cacheBalance.balance,
                decimals: widget.mainCoin.decimals,
              );
            }
            if (cacheBalance == null ||
                DateTime.now().difference(cacheBalance.updateTime) >
                    Duration(seconds: 30)) {
              Utils.futureGetBalance(widget.web3clientProvider, widget.mainCoin,
                      widget.account.address)
                  .then((balance) {
                this.balanceListenable.value = balance;
                widget.dataSourceProvider.localService.putCachedBalance(
                    widget.mainCoin, widget.account.address, balance.value);
              });
            }
          }
        },
      ).toString()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(100),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8), color: widget.homeColor),
      margin: EdgeInsets.only(
        bottom: ScreenUtil().setHeight(10),
        left: ScreenUtil().setHeight(20),
        right: ScreenUtil().setHeight(20),
      ),
      child: accountDetail(),
    );
  }

  InkWell accountDetail() {
    return InkWell(
      onTap: () {
        _switchAccount();
      },
      child: Container(
        height: ScreenUtil().setHeight(88),
        child: Stack(
          children: [
            Column(
              children: [
                //昵称
                Container(
                  height: ScreenUtil().setHeight(30),
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(14),
                    top: ScreenUtil().setHeight(5),
                  ),
                  child: accountName(),
                ),
                //账号地址
                Container(
                  height: ScreenUtil().setHeight(30),
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(14),
                  ),
                  child: accountAdress(),
                ),
                //账号类型
                Container(
                  height: ScreenUtil().setHeight(30),
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(14),
                  ),
                  child: addressType(),
                ),
              ],
            ),
            //选中指示器
            Positioned(
              top: ScreenUtil().setHeight(10),
              right: ScreenUtil().setWidth(10),
              child: Container(
                width: ScreenUtil().setWidth(40),
                child: selectedIcon(),
              ),
            ),
            //获取余额写在布局里面，写到方法里面，有问题
            balanceUpdate(),
            //余额
            Positioned(
              bottom: ScreenUtil().setHeight(10),
              right: ScreenUtil().setWidth(16),
              child: Container(
                child: displayBalance(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ValueListenableBuilder accountName() {
    return ValueListenableBuilder(
      valueListenable: CoreProvider.of(context).accountListenable,
      builder: (context, value, child) {
        return Row(
          children: [
            InkWell(
              onTap: () async {
                _uppateAccountName();
              },
              child: Container(
                child: Text(
                  widget.account.nickName,
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(14), color: Colors.white),
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                _uppateAccountName();
              },
              child: Container(
                margin: EdgeInsets.only(left: ScreenUtil().setWidth(16)),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/edit.png',
                      width: ScreenUtil().setWidth(12),
                      height: ScreenUtil().setWidth(12),
                      color: Color.fromRGBO(216, 216, 216, 1),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Row accountAdress() {
    return Row(
      children: [
        InkWell(
          onTap: () async {
            _copyAccountAdress();
          },
          child: Text(
            widget.account.addressBlog,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(12),
              color: Color.fromRGBO(255, 255, 255, 0.5),
            ),
          ),
        ),
        InkWell(
          onTap: () async {
            _copyAccountAdress();
          },
          child: Container(
            // padding: EdgeInsets.all(3),
            margin: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
            height: ScreenUtil().setHeight(11),
            width: ScreenUtil().setWidth(12),
            child: Image.asset('assets/images/copy.png',
                color: Color.fromRGBO(216, 216, 216, 1)),
          ),
        ),
      ],
    );
  }

  Row addressType() {
    return widget.account.derivePathIndex == 0 && widget.index == 0
        ? Row(
            children: [
              Container(
                child: Text(
                  S.current.wallet_selector_initial_address,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(12),
                    color: Color.fromRGBO(255, 255, 255, 0.5),
                  ),
                ),
              ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  Container(
                    child: Text(
                      widget.index == 0
                          ? S.current.wallet_selector_serial_number +
                              widget.accountGroups
                                  .indexOf(widget.account)
                                  .toString()
                          : S.current.wallet_selector_serial_number +
                              (widget.accountGroups.indexOf(widget.account) + 1)
                                  .toString(),
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: Color.fromRGBO(255, 255, 255, 0.5),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
  }

  //选中图标
  Row selectedIcon() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        selectAccount.address == widget.account.address
            ? Container(
                height: ScreenUtil().setHeight(16),
                width: ScreenUtil().setWidth(16),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: widget.homeColor,
                  border: Border.all(width: 3, color: Colors.white),
                ),
              )
            : Container(height: 0, width: 0),
      ],
    );
  }

  //显示余额
  ValueListenableBuilder displayBalance() {
    return ValueListenableBuilder(
      valueListenable: balanceListenable,
      builder: (context, balance, child) => balance == null
          ? Container()
          : Row(
              children: [
                Text(
                  balance.toStringAsFixed(),
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(14),
                    color: Colors.white,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(5),
                  ),
                  child: Text(
                    selectedNetworkConfig.coinBlog.symbol,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
    );
  }
}
