import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/command/theme/icon_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/token_utils.dart'
    as Utils;
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

class BatchImportPage extends StatefulWidget {
  final String selectNums;

  final Web3ClientProvider web3Client;

  const BatchImportPage({
    Key? key,
    required this.selectNums,
    required this.web3Client,
  }) : super(key: key);
  @override
  _BatchImportPageState createState() => _BatchImportPageState();
}

class _BatchImportPageState extends State<BatchImportPage> {
  List<String> priatekey = []; //从链上获取的私钥集合
  List selectPriateKEY = []; //选择的私钥
  List balancelist = []; //地址余额集合
  List valueb = []; //复选框状态

  String balance = "";

  @override
  void initState() {
    super.initState();
    _getPriatekey();
  }

  //从链上获取私钥
  _getPriatekey() async {
    for (var i = 1; i < int.parse(widget.selectNums) + 1; i++) {
      final key = await HDWalletStateMachine.getPrivateKeyForBIP44(
        chainCore: CoreProvider.of(context).networkConfig.chainCore,
        accountIndex: 0,
        changeIndex: 0,
        addressIndex: i,
      );

      final address = await HDWalletStateMachine.getAddressForPrivateKey(
          coinType: CoreProvider.of(context).networkConfig.chainCore,
          privateKey: key);

      // Utils.futureGetBalance(
      //   widget.web3Client,
      //   CoreProvider.of(context).networkConfig.coinBlog,
      //   address,
      // ).then((value) => this.balance = Amount(
      //       value: value.value,
      //       decimals: CoreProvider.of(context).networkConfig.coinBlog.decimals,
      //     ).toStringAsFixed());

      // print(this.balance);

      await Utils.futureGetBalance(
        widget.web3Client,
        CoreProvider.of(context).networkConfig.coinBlog,
        address,
      ).then((value) => this.balance = value.toStringAsFixed());

      setState(() {
        this.priatekey.add(bytesToHex(hexToBytes(key)));

        this.balancelist.add(this.balance);

        if (key.length > 0) {
          bool key = false;
          this.valueb.add(key);
        }
      });
    }
  }

//导入私钥
  Future<void> doImport(BuildContext context) async {
    if (this.selectPriateKEY.length <= 0) {
      Fluttertoast.showToast(
        msg: S.current.wallet_import_input_privatekeys_empty,
      );
      return;
    }

    Future.delayed(
      Duration(milliseconds: 1000),
    ).then((_) {
      for (var i = 0; i < this.selectPriateKEY.length; i++) {
        CoreProvider.of(context).wallet.importKeypairFromPrivateKey(
              type: CoreProvider.of(context).chainCore,
              privateKey: this.selectPriateKEY[i],
              walletName: "batch_imported",
            )..then((value) {
                if (this.selectPriateKEY.length == i + 1) {
                  Navigator.of(context, rootNavigator: true)
                      .pop<WalletKey>(value);
                  Navigator.of(context, rootNavigator: true)
                      .pop<WalletKey>(value);

                  Fluttertoast.showToast(
                      msg: S.current.wallet_batch_import_succeeded);
                }
              }).catchError((_) {}, test: (error) {
                if (error ==
                    WalletStorageError.ErrorWalletIdentifierAlreadyExist) {
                  Fluttertoast.showToast(
                      msg: S.current.wallet_import_privatekey_already_exist);
                  // return true;
                }
                return false;
              });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: NavigationBar(
          title: S.current.wallet_batch_import_title,
        ),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
                top: ScreenUtil().setHeight(17),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0), //阴影xy轴偏移量
                        blurRadius: 1.0, //阴影模糊程度
                        spreadRadius: 0 //阴影扩散程度
                        )
                  ]),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(15),
                      bottom: ScreenUtil().setHeight(14),
                      top: ScreenUtil().setHeight(12),
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      S.current.wallet_batch_import_sub_account_private_key +
                          '（${widget.selectNums}）',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Color.fromRGBO(51, 51, 51, 1),
                      ),
                    ),
                  ),
                  Container(
                    height: 0.5,
                    color: Colors.grey[400],
                  ),
                  Container(
                    height: ScreenUtil().setHeight(36),
                    child: Row(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: ScreenUtil().setWidth(60),
                          child: Text(
                            S.current.wallet_batch_import_edit,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(12),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding:
                              EdgeInsets.only(right: ScreenUtil().setWidth(10)),
                          width: ScreenUtil().setWidth(40),
                          child: Text(
                            S.current.wallet_batch_import_serial_number,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(12),
                            ),
                          ),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(135),
                          alignment: Alignment.center,
                          child: Text(S.current.wallet_batch_import_private_key,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(12),
                              )),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(100),
                          alignment: Alignment.center,
                          child:
                              Text(S.current.wallet_batch_import_main_balance,
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                  )),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(460),
                    child: ListView.builder(
                      physics: const AlwaysScrollableScrollPhysics(
                        parent: BouncingScrollPhysics(),
                      ),
                      // ignore: non_constant_identifier_names
                      itemBuilder: (BuildContext, index) {
                        return Container(
                          color: index % 2 == 0
                              ? Color.fromRGBO(245, 245, 245, 1)
                              : Colors.white,
                          height: ScreenUtil().setHeight(34),
                          child: Row(
                            children: [
                              Container(
                                width: ScreenUtil().setWidth(60),
                                child: Checkbox(
                                  value: valueb[index],
                                  onChanged: (val) {
                                    setState(() {
                                      this.valueb[index] = !this.valueb[index];
                                      if (valueb[index] == true) {
                                        this
                                            .selectPriateKEY
                                            .add(this.priatekey[index]);
                                      } else {
                                        this
                                            .selectPriateKEY
                                            .remove(this.priatekey[index]);
                                      }
                                    });
                                  },
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.only(
                                    right: ScreenUtil().setWidth(10)),
                                width: ScreenUtil().setWidth(40),
                                child: Text((index + 1).toString(),
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(11),
                                    )),
                              ),
                              Container(
                                alignment: Alignment.center,
                                width: ScreenUtil().setWidth(135),
                                child: Text(this.priatekey[index],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(11),
                                    )),
                              ),
                              Container(
                                alignment: Alignment.center,
                                width: ScreenUtil().setWidth(100),
                                child: Text(this.balancelist[index].toString(),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(12),
                                    )),
                              ),
                            ],
                          ),
                        );
                      },
                      itemCount: this.priatekey.length,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      // for (var i = 0; i < this.selectPriateKEY.length; i++) {
                      //   CoreProvider.of(context)
                      //       .wallet
                      //       .importKeypairFromPrivateKey(
                      //           type: CoreProvider.of(context)
                      //               .networkConfig
                      //               .chainCore,
                      //           privateKey: this.selectPriateKEY[i]);
                      // }
                      doImport(context);
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(34),
                          bottom: ScreenUtil().setHeight(24),
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(20)),
                      height: ScreenUtil().setHeight(48),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color.fromRGBO(83, 106, 231, 1),
                      ),
                      child: Center(
                        child: Text(
                          S.current.wallet_selector_import,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
