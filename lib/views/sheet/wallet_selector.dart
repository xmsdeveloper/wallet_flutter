// @dart=2.9
part of 'sheet.dart';

class WalletSelector extends StatefulWidget {
  final Web3ClientProvider web3clientProvider;
  final DataSourceProvider dataSourceProvider;
  final TokenBlog mainCoin;
  final Color homeColor;
  const WalletSelector({
    Key key,
    this.web3clientProvider,
    this.dataSourceProvider,
    this.mainCoin,
    this.homeColor,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _WalletSelectorState();

  static show({
    @required BuildContext context,
    Web3ClientProvider web3clientProvider,
    DataSourceProvider dataSourceProvider,
    TokenBlog mainCoin,
    final List<WalletKey> walletKeys,
    Color homeColor,
  }) =>
      showCustomModalBottomSheet(
        duration: Duration(milliseconds: 100),
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => WalletSelector(
          dataSourceProvider: dataSourceProvider,
          web3clientProvider: web3clientProvider,
          mainCoin: mainCoin,
          homeColor: homeColor,
        ),
        expand: false,
        containerWidget: (context, animation, child) => Container(
          child: child,
          height: MediaQuery.of(context).size.height * 0.85,
        ),
      );
}

class _AccountGroup {
  /// 分类名称
  final String groupName;

  /// 具体账户列表
  final List<WalletKey> accounts;

  /// 展开状态
  bool expanded;

  _AccountGroup({
    @required this.groupName,
    @required this.accounts,
    this.expanded = true,
  });
}

class _WalletSelectorState extends State<WalletSelector>
    with TickerProviderStateMixin {
  List<_AccountGroup> accountGroups = [];

  ScrollController scrollController;

  /// 当前选中的网络
  NetworkConfig selectedNetworkConfig;

  // 内部到滚动逻辑需要参照行高定义
  double get cellHeight => ScreenUtil().setHeight(80);

  int isSelect = 0;
  List<WalletKey> walletAccount = [];
//显示的类型
  List typeList = [
    {
      "icon": "assets/images/create.png",
      "title": S.current.wallet_selector_shhet_types_1,
      "isSelect": 0
    },
    {
      "icon": "assets/images/Import.png",
      "title": S.current.wallet_selector_shhet_types_2,
      "isSelect": 1
    },
    {
      "icon": "assets/images/observation.png",
      "title": S.current.wallet_selector_shhet_types_3,
      "isSelect": 2
    }
  ];
//可创建导入的类型
  List createTypeList = [
    {
      "index": 0,
      "type": S.current.wallet_selector_shhet_create_1,
      "route": "assets/images/add1.svg"
    },
    {
      "index": 1,
      "type": S.current.wallet_selector_shhet_create_2,
      "route": "assets/images/import_wallet.svg"
    }
  ];
  void _getSeeWallet() async {
    walletAccount.clear();
    SeeWalletStorageUtils()
        .seeWalletlist()
        .then((value) => value.map((e) => walletAccount.add(e)).toList());
    setState(() {});
  }

  /// 载入数据源，并且分类
  /// 嵌套的Widget导致了一个bug，目前未知原因
  void _setAndSortAccountDataSource(List<WalletKey> keys) => accountGroups = [
        _AccountGroup(
          groupName: S.current.wallet_selector_header_hd,
          accounts: keys.where((key) => key.isHDKeypair).toList(),
          expanded: true,
        ),
        _AccountGroup(
          groupName: S.current.wallet_selector_header_import,
          accounts: keys.where((key) => !key.isHDKeypair).toList() ?? [],
          expanded: true,
        ),
        _AccountGroup(
          groupName: "see",
          accounts: walletAccount,
          expanded: true,
        )
      ];

  /// 将指定的地址，滚动到用户可以看到到区域
  void _scrollAccountInViewPoint(WalletKey account, [bool animationed = true]) {
    if (account == null) {
      return;
    }

    /// 地址所在到GroupIndex，后续需要计算具体滚动到坐标
    final groupIndex = accountGroups.indexWhere(
      (group) => group.accounts.contains(
        account,
      ),
    );

    if (groupIndex < 0) {
      return;
    }

    /// 由于进行了分类显示，需要准确计算滚动到到目标高度
    final selectIndex = accountGroups[groupIndex].accounts.indexOf(
          account,
        );

    /// 根据当前列表到展开状态加算offset
    double offset = 0;

    /// 计算地址所在Group之前到所有Group的高度
    for (int i = 0; i < groupIndex; i++) {
      /// GroupHeader
      offset += 50;

      /// 若是展开状态，增加16像素的Header偏移
      /// expandedHeaderPadding: _kPanelHeaderExpandedDefaultPadding
      if (accountGroups[i].expanded) {
        offset += accountGroups[i].accounts.length * cellHeight;

        /// 第一个列表是没有头部Padding的
        if (i > 0) {
          /// 必须固定像素值，不需要转换和计算
          offset += 16;
        }
      }
    }

    /// 加算当前Group的高度
    offset += selectIndex * cellHeight;

    /// 防止出现cell显示了一半的情况(取整操作)
    offset = offset -
        scrollController.position.viewportDimension /
            2 ~/
            cellHeight *
            cellHeight;

    if (offset < 0) {
      offset = 0;
    }

    if (animationed) {
      double offsets = offset + 10.0;

      scrollController.animateTo(
        // offset > scrollController.position.maxScrollExtent
        //     ? scrollController.position.maxScrollExtent
        //     : offsets,
        offsets,
        duration: Duration(milliseconds: 350),
        curve: Curves.linear,
      );
    } else {
      scrollController.jumpTo(
        offset > scrollController.position.maxScrollExtent
            ? scrollController.position.maxScrollExtent
            : offset,
      );
    }
  }

  void navigatorImportOtionPage() {
    showCustomModalBottomSheet(
      context: context,
      builder: (context) {
        return ImportOptionPage(
          dataSourceProvider: widget.dataSourceProvider,
          importTypes: [
            ImportType.PrivateKey,
            ImportType.Keystore,
          ],
        );
      },
      containerWidget: (context, animation, child) => Container(
        child: child,
        height: MediaQuery.of(context).size.height * 0.35,
      ),
    ).then(
      (key) {
        _getSeeWallet();
        if (key != null)
          _setAndSortAccountDataSource(
              CoreProvider().wallet.keysOf(selectedNetworkConfig.chainCore));

        _scrollAccountInViewPoint(key);
        setState(() {});
      },
    );
  }

  @override
  void initState() {
    super.initState();
    // 进入页面统计（手动采集时才可设置）
    selectedNetworkConfig = CoreProvider().networkConfig;
    scrollController = ScrollController();

    /// 在build完成后调用滚动到指定区域
    SchedulerBinding.instance.addPostFrameCallback(
      (_) {
        _scrollAccountInViewPoint(CoreProvider().account, false);
      },
    );
    _getSeeWallet();

    /// 对地址进行分类
    _setAndSortAccountDataSource(
        CoreProvider().wallet.keysOf(selectedNetworkConfig.chainCore));

    //判断左边列表，选中状态
    Future.delayed(Duration(milliseconds: 0), () {
      switch (CoreProvider.of(context).account.isHDKeypair) {
        case false:
          setState(() {
            isSelect = 1;
          });
          if (CoreProvider.of(context).account.privateKeyRaw == null) {
            setState(() {
              isSelect = 2;
            });
          }
          break;
        case true:
          setState(() {
            isSelect = 0;
          });
          break;
        default:
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) => Material(
        color: Colors.transparent,
        child: SafeArea(
          child: Navigator(
            onGenerateRoute: (_) => MaterialPageRoute(
              // fullscreenDialog: true,
              builder: (navigatorContext) => ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                child: Scaffold(
                  appBar: NavigationBar(
                    elevation: 0.5,
                    backgroundColor: Colors.white,
                    title: selectedNetworkConfig.chainName,
                    actions: [
                      IconButton(
                        padding: EdgeInsets.all(8),
                        icon: Icon(
                          Icons.close,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  backgroundColor: Color.fromRGBO(246, 246, 246, 1),
                  body: Stack(
                    children: walletSelect(),
                  ),
                ),
              ),
            ),
          ),
        ),
      );

  List walletSelect() {
    return <Widget>[
      Row(
        children: [
          //账号分类选择
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(
              bottom: ScreenUtil().setHeight(60),
            ),
            alignment: Alignment.topCenter,
            // width: ScreenUtil().setWidth(72),
            width: ScreenUtil().setWidth(73),
            child: leftAccountType(),
          ),

          // / 地址选择
          Container(
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(15),
              bottom: ScreenUtil().setHeight(60),
            ),
            // color: Color.fromRGBO(250, 250, 250, 1),
            width:
                MediaQuery.of(context).size.width - ScreenUtil().setWidth(73),
            height: double.infinity,
            child: rightAccountList(),
          ),
        ],
      ),
      //底部创建和导入按钮
      Positioned(
        bottom: 0,
        child: Container(
          height: ScreenUtil().setHeight(63),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              top: BorderSide(
                width: 0.5,
                color: Colors.grey[200],
              ),
            ),
          ),
          child: bottomWalletOPption(),
        ),
      ),
    ];
  }

  SingleChildScrollView leftAccountType() {
    return SingleChildScrollView(
        child: Column(
      children: [
        Container(
          color: Color.fromRGBO(246, 246, 246, 1),
          child: Column(
            children: typeList
                .map<Widget>(
                  (identifier) => GestureDetector(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 0,
                        vertical: ScreenUtil().setHeight(35),
                      ),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: this.isSelect == identifier['isSelect']
                            ? Color.fromRGBO(246, 246, 246, 1)
                            : Colors.white,
                        borderRadius:
                            this.isSelect == 0 && identifier['isSelect'] == 1
                                ? BorderRadius.only(
                                    topRight: Radius.circular(15),
                                  )
                                : this.isSelect == 1
                                    ? identifier['isSelect'] == 0
                                        ? BorderRadius.only(
                                            bottomRight: Radius.circular(15),
                                          )
                                        : BorderRadius.only(
                                            topRight: Radius.circular(15),
                                          )
                                    : this.isSelect == 2 &&
                                            identifier['isSelect'] == 1
                                        ? BorderRadius.only(
                                            bottomRight: Radius.circular(15),
                                          )
                                        : null,
                      ),
                      child: Container(
                          width: ScreenUtil().setWidth(73),
                          child: Row(
                            children: [
                              this.isSelect == identifier['isSelect']
                                  ? Container(
                                      margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(9),
                                        right: ScreenUtil().setWidth(14),
                                      ),
                                      height: ScreenUtil().setHeight(40),
                                      width: ScreenUtil().setWidth(2),
                                      color: Colors.blue,
                                    )
                                  : Container(
                                      margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(9),
                                        right: ScreenUtil().setWidth(14),
                                      ),
                                      height: ScreenUtil().setHeight(40),
                                      width: ScreenUtil().setWidth(2),
                                    ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: ScreenUtil().setHeight(5),
                                    ),
                                    alignment: Alignment.center,
                                    // padding: EdgeInsets.all(
                                    //   ScreenUtil().setHeight(8),
                                    // ),
                                    width: ScreenUtil().setWidth(28.47),
                                    height: ScreenUtil().setHeight(22.19),
                                    child: AnimatedCrossFade(
                                      firstChild: Image.asset(
                                        identifier['icon'],
                                        color: Colors.blue,
                                      ),
                                      secondChild:
                                          Image.asset(identifier['icon']),
                                      crossFadeState: this.isSelect ==
                                              identifier['isSelect']
                                          ? CrossFadeState.showFirst
                                          : CrossFadeState.showSecond,
                                      duration: const Duration(
                                        milliseconds: 500,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      identifier['title'],
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(12),
                                        color: this.isSelect ==
                                                identifier['isSelect']
                                            ? Colors.blue
                                            : Color.fromRGBO(175, 175, 175, 1),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          )),
                    ),
                    onTap: () {
                      this.isSelect = identifier['isSelect'];
                      setState(() {});
                    },
                  ),
                )
                .toList(),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(246, 246, 246, 1),
          ),
          width: ScreenUtil().setWidth(73),
          height: ScreenUtil().setWidth(100),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: this.isSelect == 2
                    ? Radius.circular(15)
                    : Radius.circular(0),
              ),
            ),
            width: ScreenUtil().setWidth(73),
            height: ScreenUtil().setWidth(100),
          ),
        )
      ],
    ));
  }

  SingleChildScrollView rightAccountList() {
    return SingleChildScrollView(
      controller: scrollController,
      physics: const AlwaysScrollableScrollPhysics(
        parent: BouncingScrollPhysics(),
      ),
      child: accountGroups[isSelect].accounts.length > 0
          ? Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              Container(
                margin: EdgeInsets.only(
                  bottom: ScreenUtil().setHeight(10),
                  left: ScreenUtil().setHeight(20),
                  right: ScreenUtil().setHeight(20),
                ),
                alignment: Alignment.topLeft,
                child: Text(
                  S.current.wallet_selector_shhet_title,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(14),
                    // fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.6,
                child: ListView.builder(
                    itemCount: accountGroups[isSelect].accounts.length,
                    itemBuilder: (context, index) {
                      if (isSelect == 0) {
                        return AcountDetail(
                          accountGroups[isSelect].accounts[index],
                          isSelect,
                          widget.web3clientProvider,
                          accountGroups[isSelect].accounts,
                          widget.dataSourceProvider,
                          widget.mainCoin,
                          widget.homeColor,
                        );
                      } else if (isSelect == 1) {
                        return ImAcountDetail(
                          accountGroups[isSelect].accounts[index],
                          isSelect,
                          widget.web3clientProvider,
                          accountGroups[isSelect].accounts,
                          widget.dataSourceProvider,
                          widget.mainCoin,
                          widget.homeColor,
                        );
                      } else {
                        return WaAcountDetail(
                          accountGroups[isSelect].accounts[index],
                          isSelect,
                          widget.web3clientProvider,
                          accountGroups[isSelect].accounts,
                          widget.dataSourceProvider,
                          widget.mainCoin,
                          widget.homeColor,
                        );
                      }
                    }),
              )
            ])
          //如果该列表没有账号，显示提示框，点击可进入导入列表选项
          : InkWell(
              onTap: () async {
                await navigatorImportOtionPage();
              },
              child: Container(
                margin: EdgeInsets.only(
                  bottom: ScreenUtil().setHeight(10),
                  left: ScreenUtil().setHeight(20),
                  right: ScreenUtil().setHeight(20),
                ),
                height: ScreenUtil().setHeight(100),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(
                    width: 0.5,
                    color: Colors.grey[300],
                  ),
                ),
                child: Center(
                  child: Text(
                    S.current.wallet_selector_shhet_no_address,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Colors.grey[500]),
                  ),
                ),
              ),
            ),
    );
  }

  Row bottomWalletOPption() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: createTypeList
              .map(
                (e) => InkWell(
                  onTap: () async {
                    switch (e["index"]) {
                      case 0:
                        isSelect = 0;
                        Navigator.of(context).pop();
                        showCustomModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return CreateAccount();
                          },
                          containerWidget: (context, animation, child) =>
                              Container(
                            child: child,
                            height: MediaQuery.of(context).size.height * 0.57,
                          ),
                        );

                        break;
                      case 1:
                        await navigatorImportOtionPage();
                        break;
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SvgPicture.asset(
                          e['route'],
                          height: ScreenUtil().setHeight(18),
                          width: ScreenUtil().setWidth(18),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(6),
                            right: ScreenUtil().setWidth(45),
                          ),
                          child: Text(
                            // S.current.deeplink_network_add,
                            e['type'],
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                            ),
                          ),
                        ),
                        e['index'] != 2
                            ? Container(
                                width: ScreenUtil().setWidth(1),
                                height: ScreenUtil().setHeight(22),
                                color: Color.fromRGBO(216, 216, 216, 1),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ],
    );
  }
}
