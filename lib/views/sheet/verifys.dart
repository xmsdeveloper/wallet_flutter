// @dart=2.9
part of 'sheet.dart';

typedef VerifyResultCallbacks = String Function(
    BuildContext context, String pwd);

class VerifySheets extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _VerifySheetsState();

  final VerifyResultCallbacks verifyCallback;

  final Widget leading;

  final bool internalMode;

  static Future<T> shows<T>(
    BuildContext context, {
    @required VerifyResultCallbacks verifyCallback,
    Widget leading,
  }) =>
      showModalBottomSheet<T>(
        context: context,
        enableDrag: false,
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: Colors.transparent,
        builder: (context) => VerifySheets(
          verifyCallback: verifyCallback,
          leading: leading,
        ),
      );

  VerifySheets({
    Key key,
    @required this.verifyCallback,
    this.leading,
    this.internalMode = false,
  }) : super(key: key);
}

class _VerifySheetsState extends State<VerifySheets>
    with WidgetsBindingObserver {
  bool inVerifing = true;
  bool ob = false;
  TextEditingController textEditingController;
  StatusButtonController commitButtonController;
  ValueNotifier<String> textEditingErrorListenable =
      ValueNotifier<String>(null);

  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");

  @override
  void initState() {
    super.initState();
    // this.ob = ConfigStorageUtils()
    // .readDisplayPasswordIndexOfKey(ConfigKey.DisplayPassword);
    textEditingController = TextEditingController();
    commitButtonController = StatusButtonController(
      title: S.current.sheet_verify_done,
      busyTitle: S.current.sheet_verify_verifing,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Container(
                height: ScreenUtil().setHeight(201),
                width: ScreenUtil().setWidth(299),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                margin: EdgeInsets.only(
                  left: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                ),
                // color: ThemeUtils().getColor('sheet.background'),

                // padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(40)),
                // padding: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
                child: Stack(
                  children: [
                    Column(
                      children: [
                        if (!widget.internalMode)
                          NavigationBar(
                            elevation: 0,
                            backgroundColor: Colors.transparent,
                            isBottomSheet: false,
                            title: S.current.sheet_verify_title,
                            leading: inVerifing ? Container() : widget.leading,
                          ),
                        Container(
                          height: ScreenUtil().setHeight(46),
                          margin: EdgeInsets.only(
                            // top: ScreenUtil().setHeight(20),
                            left: ScreenUtil().setWidth(20),
                            right: ScreenUtil().setWidth(20),
                          ),
                          child: ValueListenableBuilder(
                            valueListenable: textEditingErrorListenable,
                            builder: (context, value, child) => TextFormField(
                              controller: textEditingController,
                              obscureText: ConfigStorageUtils()
                                  .readDisplayPasswordIndexOfKey(
                                ConfigKey.DisplayPassword,
                              ),
                              autofocus: false,
                              onChanged: (value) {
                                if (textEditingErrorListenable.value != null) {
                                  textEditingErrorListenable.value = null;
                                }
                              },
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 0.0, horizontal: 11),
                                // prefixIcon: Icon(Icons.security),
                                suffixIcon:
                                    //  IconButton(
                                    //   color: ThemeUtils()
                                    //       .getColor('utils.input.border_color'),
                                    //   icon: Icon(Icons.clear),
                                    //   onPressed: () {
                                    //     textEditingController.text = '';
                                    //     textEditingErrorListenable.value = null;
                                    //   },
                                    // ),
                                    InkWell(
                                        onTap: () {
                                          setState(
                                            () {
                                              this.ob = !this.ob;
                                              ConfigStorageUtils()
                                                  .writeDisplayPasswordIndexOfKey(
                                                ConfigKey.DisplayPassword,
                                                this.ob,
                                              );
                                            },
                                          );
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: ScreenUtil().setWidth(13),
                                          ),
                                          child: Image.asset(
                                            ConfigStorageUtils()
                                                        .readDisplayPasswordIndexOfKey(
                                                            ConfigKey
                                                                .DisplayPassword) ==
                                                    true
                                                ? "assets/images/eye_off.png"
                                                : "assets/images/eye_on.png",
                                            width: ScreenUtil().setWidth(21),
                                            height: ScreenUtil().setHeight(13),
                                          ),
                                        )),
                                // fillColor: ThemeUtils().getColor(
                                //   'utils.input.fill_color',
                                // ),
                                suffixIconConstraints: BoxConstraints(),
                                fillColor: Color.fromRGBO(250, 250, 250, 1),
                                focusColor: ThemeUtils().getColor(
                                  'utils.input.fill_color',
                                ),
                                filled: true,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          ScreenUtil().setWidth(5))),
                                  borderSide: BorderSide(
                                    color: ThemeUtils().getColor(
                                      'utils.input.border_color',
                                    ),
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          ScreenUtil().setWidth(5))),
                                  borderSide: BorderSide(
                                    color: ThemeUtils().getColor(
                                      'utils.input.border_color',
                                    ),
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          ScreenUtil().setWidth(5))),
                                  borderSide: BorderSide(
                                    color: ThemeUtils().getColor(
                                      'utils.input.border_color',
                                    ),
                                  ),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          ScreenUtil().setWidth(5))),
                                  borderSide: BorderSide(color: Colors.red),
                                ),
                                hintText:
                                    S.current.sheet_verify_input_placeholder,
                                hintStyle: TextStyle(
                                    fontSize: ScreenUtil().setSp(14),
                                    color: Color.fromRGBO(175, 175, 175, 1)),
                                // errorText: textEditingErrorListenable.value,
                              ),
                            ),
                          ),
                        ),
                        ValueListenableBuilder(
                          valueListenable: errorListenable,
                          builder: (context, value, child) {
                            return Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(16),
                                // bottom: ScreenUtil().setHeight(10),
                              ),
                              child: Text(
                                errorListenable.value,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(14),
                                  color: Color.fromRGBO(203, 0, 0, 1),
                                ),
                              ),
                            );
                          },
                        ),
                        AnimatedPadding(
                          padding: MediaQuery.of(context).viewInsets,
                          duration: Duration(milliseconds: 250),
                        ),
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      child: //操作栏
                          Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  top: BorderSide(
                                    width: 0.3,
                                    color: Colors.grey[300],
                                  ),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                          right: BorderSide(
                                            width: 0.3,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ),
                                      height: ScreenUtil().setHeight(46),
                                      alignment: Alignment.center,
                                      width: ScreenUtil().setWidth(299) / 2,
                                      child: Text(
                                        S.current.sheet_cancel,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(16),
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      SystemChannels.textInput
                                          .invokeMethod('TextInput.hide');

                                      await Future.delayed(
                                          Duration(milliseconds: 1000));

                                      if (textEditingController.text == "") {
                                        errorListenable.value = S.current
                                            .wallet_import_password_empty;
                                        return;
                                      }

                                      textEditingErrorListenable.value =
                                          widget.verifyCallback(
                                        context,
                                        textEditingController.text,
                                      );

                                      if (textEditingErrorListenable.value !=
                                          null) {
                                        textEditingController.text = '';
                                        errorListenable.value =
                                            S.current.password_verify_faild;
                                      }

                                      return Future.value(null);
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(46),
                                      width: ScreenUtil().setWidth(299) / 2,
                                      alignment: Alignment.center,
                                      child: Text(
                                        S.current.sheet_ok,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(16),
                                          color: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                    ),
                  ],
                ),
              ),
              Padding(padding: MediaQuery.of(context).viewInsets),
            ],
          ),
        ],
      );
}
