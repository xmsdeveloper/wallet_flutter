// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/animations/view.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/base/key.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/views/keychain/importer.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

class ImportOptionPage extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;
  final List<ImportType> importTypes;
  ImportOptionPage({this.importTypes, this.dataSourceProvider});

  @override
  State<ImportOptionPage> createState() => _ImportOptionPageState();
}

class _ImportOptionPageState extends State<ImportOptionPage> {
  List datasource = [
    {
      "index": 0,
      'icon': "assets/images/key.png",
      'title': S.current.common_private_key,
    },
    {
      "index": 1,
      'icon': "assets/images/keystore.png",
      'title': S.current.wallet_import_type_Keystore,
    },
    {
      "index": 2,
      'icon': "assets/images/seeWallet_2.png",
      'title': S.current.wallet_watch_name,
    },
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: NavigationBar(
      //   title: S.current.wallet_selector_shhet_create_2,
      //   elevation: 0.3,
      // ),
      backgroundColor: Colors.transparent,
      body: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  Container(
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16),
                        ),
                      ),
                      height: ScreenUtil().setHeight(56),
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text(
                          S.current.wallet_selector_shhet_create_2,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            color: Color.fromRGBO(51, 51, 51, 1),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )),
                  Container(
                    height: 0.5,
                    width: MediaQuery.of(context).size.width,
                    color: Color.fromRGBO(216, 216, 216, 1),
                  ),
                  Column(
                      children: datasource
                          .map((e) => Column(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      switch (e["index"]) {
                                        case 0:
                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .push<WalletKey>(
                                            ViewAnimations.viewRightIn(
                                              ImportWalletView(
                                                importTypes: [
                                                  ImportType.PrivateKey,
                                                ],
                                              ),
                                            ),
                                          );

                                          break;
                                        case 1:
                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .push<WalletKey>(
                                            ViewAnimations.viewRightIn(
                                              ImportWalletView(
                                                importTypes: [
                                                  ImportType.Keystore,
                                                ],
                                              ),
                                            ),
                                          );

                                          break;
                                        case 2:
                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .push<WalletKey>(
                                            ViewAnimations.viewRightIn(
                                              ImportWalletView(
                                                dataSourceProvider: this
                                                    .widget
                                                    .dataSourceProvider,
                                                importTypes: [
                                                  ImportType.Adress,
                                                ],
                                              ),
                                            ),
                                          );
                                          break;
                                        default:
                                      }
                                    },
                                    child: Container(
                                        height: ScreenUtil().setHeight(66),
                                        margin: EdgeInsets.only(
                                          left: ScreenUtil().setWidth(10),
                                        ),
                                        // decoration: BoxDecoration(
                                        //   color: ThemeUtils().getColor(
                                        //     'views.chaincore.ethereum.assets.cell_background',
                                        //   ),
                                        // ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                    left: ScreenUtil()
                                                        .setWidth(14),
                                                    right: ScreenUtil()
                                                        .setWidth(21),
                                                  ),
                                                  width: e['index'] == 2
                                                      ? ScreenUtil()
                                                          .setWidth(20.46)
                                                      : ScreenUtil()
                                                          .setWidth(18.46),
                                                  height: e['index'] == 2
                                                      ? ScreenUtil()
                                                          .setHeight(23.54)
                                                      : ScreenUtil()
                                                          .setHeight(21.54),
                                                  child: Image.asset(e['icon']),
                                                ),
                                                Text(
                                                  e['title'],
                                                  style: TextStyle(
                                                    fontSize:
                                                        ScreenUtil().setSp(16),
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                right:
                                                    ScreenUtil().setWidth(22),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Icon(
                                                    Icons
                                                        .arrow_forward_ios_sharp,
                                                    color: Colors.grey[400],
                                                    size: ScreenUtil()
                                                        .setHeight(12),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        )),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(21),
                                      right: ScreenUtil().setWidth(21),
                                    ),
                                    height: 0.3,
                                    color: Color.fromRGBO(216, 216, 216, 1),
                                  ),
                                ],
                              ))
                          .toList()),
                ],
              ),
              Positioned(
                top: 0,
                right: ScreenUtil().setWidth(15),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                    // courl = [];
                  },
                  child: Container(
                    height: ScreenUtil().setHeight(56),
                    width: ScreenUtil().setWidth(40),
                    child: Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
