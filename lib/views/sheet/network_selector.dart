// @dart=2.9
part of 'sheet.dart';

class NetWorkSelector extends StatefulWidget {
  final Web3ClientProvider web3clientProvider;
  final DataSourceProvider dataSourceProvider;
  final bool isdapp;
  final bool isDispalyFull;
  const NetWorkSelector(
      {Key key,
      this.web3clientProvider,
      this.dataSourceProvider,
      this.isdapp,
      this.isDispalyFull})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _NetWorkSelectorState();

  static show({
    @required BuildContext context,
    Web3ClientProvider web3clientProvider,
    DataSourceProvider dataSourceProvider,
    bool isdapp,
  }) =>
      showCustomModalBottomSheet(
        duration: Duration(milliseconds: 100),
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => NetWorkSelector(
            dataSourceProvider: dataSourceProvider,
            web3clientProvider: web3clientProvider,
            isdapp: isdapp),
        expand: false,
        containerWidget: (context, animation, child) => Container(
          child: child,
          height: MediaQuery.of(context).size.height * 0.95,
        ),
      );
}

class _NetWorkGroup {
  /// 分类名称
  final String groupName;

  /// 具体网络列表
  final List<NetworkConfig> networks;

  _NetWorkGroup({
    @required this.groupName,
    @required this.networks,
  });
}

class _NetWorkSelectorState extends State<NetWorkSelector>
    with TickerProviderStateMixin {
  List<_AccountGroup> accountGroups = [];

  ScrollController scrollController;

  /// 当前选中的网络
  NetworkConfig selectedNetworkConfig;

  // 内部到滚动逻辑需要参照行高定义
  double get cellHeight => ScreenUtil().setHeight(80);

  AnimationController animationController;

  AnimationController controller;

  @override
  void initState() {
    super.initState();

    // 进入页面统计（手动采集时才可设置）

    selectedNetworkConfig = CoreProvider().networkConfig;

    scrollController = ScrollController();

    // / 对地址进行分类
    _setAndSortAccountDataSource();
  }

  List<_NetWorkGroup> networkGroups = [];

  void _setAndSortAccountDataSource() => networkGroups = [
        _NetWorkGroup(
          groupName: S.current.wallet_network_select_type_1,
          networks: NetworkStorageUtils()
              .networkConfiglist()
              .where(
                (element) => element.source == "ETH",
              )
              .toList(),
        ),
        _NetWorkGroup(
          groupName: S.current.wallet_network_select_type_4,
          networks: NetworkStorageUtils()
              .networkConfiglist()
              .where(
                (element) => element.source == "ETH-second",
              )
              .toList(),
        ),
        _NetWorkGroup(
          groupName: S.current.wallet_network_select_type_2,
          networks: NetworkStorageUtils()
              .networkConfiglist()
              .where(
                (element) => element.source == "ETH-compatible",
              )
              .toList(),
        ),
        _NetWorkGroup(
          groupName: S.current.wallet_network_select_type_3,
          networks: NetworkStorageUtils()
              .networkConfiglist()
              .where(
                (element) => element.source == "ETH-customize",
              )
              .toList(),
        ),
      ];

  Future<String> mockNetworkData(String i) async {
    return i;
  }

  @override
  void dispose() {
    super.dispose();
    // 离开页面统计（手动采集时才可设置）
  }

  List<WalletKey> newaccount;

  @override
  Widget build(BuildContext ctx) => Material(
        color: this.widget.isDispalyFull != true ? Colors.transparent : null,
        child: SafeArea(
          child: Navigator(
            onGenerateRoute: (_) => MaterialPageRoute(
              // fullscreenDialog: true,
              builder: (navigatorContext) => ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                child: Scaffold(
                  appBar: NavigationBar(
                    elevation: 0.5,
                    backgroundColor: Colors.white,
                    title: S.current.wallet_network_select_title,
                    actions: [
                      IconButton(
                        padding: EdgeInsets.all(8),
                        icon: Icon(
                          // const IconData(
                          //   0xe623,
                          //   fontFamily: "myIcon",
                          //   fontPackage: "wallet_flutter",
                          // ),
                          Icons.close,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  backgroundColor: ThemeUtils().getColor(
                    'sheet.background',
                  ),
                  body: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Column(
                          children: networkGroups
                              .map(
                                (e) => Column(
                                  children: [
                                    e.networks.length > 0
                                        ? Container(
                                            margin: EdgeInsets.only(
                                              top: ScreenUtil().setHeight(27),
                                              left: ScreenUtil().setWidth(12),
                                              bottom:
                                                  ScreenUtil().setHeight(11),
                                            ),
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            child: Text(
                                              e.groupName,
                                              style: TextStyle(
                                                color: Color.fromRGBO(
                                                    112, 112, 112, 1),
                                                fontSize:
                                                    ScreenUtil().setSp(14),
                                              ),
                                            ),
                                          )
                                        : Container(),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(12),
                                        right: ScreenUtil().setWidth(12),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white,
                                      ),
                                      child: Column(
                                        children: e.networks
                                            .map(
                                              (network) => Column(
                                                children: [
                                                  InkWell(
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                        left: ScreenUtil()
                                                            .setWidth(11),
                                                        right: ScreenUtil()
                                                            .setWidth(11),
                                                      ),
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                        horizontal: 0,
                                                        vertical: ScreenUtil()
                                                            .setHeight(15),
                                                      ),
                                                      width: double.infinity,
                                                      decoration: BoxDecoration(
                                                          border: Border(
                                                              bottom:
                                                                  BorderSide(
                                                        width: 0.2,
                                                        color: Color.fromRGBO(
                                                            216, 216, 216, 1),
                                                      ))
                                                          // borderRadius:
                                                          //     BorderRadius.circular(
                                                          //         10),
                                                          // color: Colors.white,
                                                          ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            child: Row(
                                                              children: [
                                                                Container(
                                                                  margin:
                                                                      EdgeInsets
                                                                          .only(
                                                                    left: ScreenUtil()
                                                                        .setWidth(
                                                                            2),
                                                                    right: ScreenUtil()
                                                                        .setWidth(
                                                                            16),
                                                                  ),
                                                                  width: ScreenUtil()
                                                                      .setWidth(
                                                                          30),
                                                                  height: ScreenUtil()
                                                                      .setHeight(
                                                                          30),
                                                                  child: network
                                                                              .logoURL !=
                                                                          ""
                                                                      ? Image
                                                                          .asset(
                                                                          network
                                                                              .logoURL,
                                                                        )
                                                                      : Image
                                                                          .asset(
                                                                          'assets/imgs/default_token.png',
                                                                          package:
                                                                              'wallet_flutter',
                                                                        ),
                                                                ),
                                                                Container(
                                                                  child: Text(
                                                                    network
                                                                        .chainName,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          selectedNetworkConfig
                                                                      .identifier ==
                                                                  network
                                                                      .identifier
                                                              ? Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerRight,
                                                                  child: Image
                                                                      .asset(
                                                                    'assets/images/on.png',
                                                                    width: ScreenUtil()
                                                                        .setWidth(
                                                                            16),
                                                                    height: ScreenUtil()
                                                                        .setHeight(
                                                                            16),
                                                                  ),
                                                                )
                                                              : Container(
                                                                  width: 0,
                                                                  height: 0,
                                                                )
                                                        ],
                                                      ),
                                                    ),
                                                    onTap: () {
                                                      setState(
                                                        () {
                                                          /// 切换网络并且切换地址数据
                                                          selectedNetworkConfig =
                                                              network;
                                                          // / 切换网络
                                                          if (selectedNetworkConfig !=
                                                              CoreProvider.of(
                                                                      context)
                                                                  .networkConfig) {
                                                            CoreProvider.of(
                                                                        context)
                                                                    .networkConfig =
                                                                selectedNetworkConfig;
                                                          }
                                                          Navigator.of(context)
                                                              .pop();
                                                          setState(() {});
                                                        },
                                                      );
                                                    },
                                                  ),
                                                ],
                                              ),
                                            )
                                            .toList(),
                                      ),
                                    )
                                  ],
                                ),
                              )
                              .toList(),
                        ),
                        InkWell(
                          onTap: () async {
                            await Navigator.of(context)
                                .push(ViewAnimations.viewRightIn(
                              NewChainPage(
                                  dataSourceProvider:
                                      this.widget.dataSourceProvider),
                            ));
                            _setAndSortAccountDataSource();
                          },
                          child: Container(
                            height: ScreenUtil().setHeight(62),
                            // decoration: BoxDecoration(
                            //     borderRadius: BorderRadius.circular(10),
                            //     border: Border.all(
                            //       width: 0.5,
                            //       color: Colors.blue,
                            //     )),
                            margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(12),
                              right: ScreenUtil().setWidth(12),
                              top: ScreenUtil().setHeight(24),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    S.current.wallet_network_select_add,
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(16),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}
