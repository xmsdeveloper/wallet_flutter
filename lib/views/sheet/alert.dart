// @dart=2.9
part of 'sheet.dart';

class ActionSheet extends StatelessWidget {
  final Widget icon;
  final Image image;
  final String title;
  final String description;
  final List<Widget> actions;

  final TextAlign textAlign;

  ActionSheet({
    this.icon,
    this.image,
    this.title,
    this.description,
    this.actions = const <Widget>[],
    this.textAlign = TextAlign.center,
  });

  factory ActionSheet.confirm({
    @required String title,
    @required String description,
    @required VoidCallback onConfirm,
    @required VoidCallback onCancel,
    Image image,
    IconData icon,
    String confirmTitle,
    String cancelTitle,
    TextAlign textAlign = TextAlign.center,
  }) =>
      ActionSheet(
        // icon: Icon(
        //   icon ?? Icons.info_rounded,
        //   size: ScreenUtil().setHeight(50),
        //   color: ThemeUtils().getColor('sheet.icon'),
        // ),
        image: Image.asset(
          'assets/images/clean.png',
          width: ScreenUtil().setWidth(192),
          height: ScreenUtil().setHeight(192),
        ),
        title: title,
        description: description,
        textAlign: textAlign,
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: ScreenUtil().setWidth(155),
                child: StatusButton.withStyle(
                  StatusButtonStyle.Warning,
                  title: cancelTitle ?? S.current.sheet_cancel,
                  onPressedFuture: () {
                    onCancel();
                    return Future.value();
                  },
                ),
              ),
              Container(
                width: ScreenUtil().setWidth(155),
                child: StatusButton.withStyle(
                  StatusButtonStyle.Info,
                  title: confirmTitle ?? S.current.sheet_ok,
                  onPressedFuture: () {
                    onConfirm();
                    return Future.value();
                  },
                ),
              ),
            ],
          )
        ],
      );

  static showConfirm(
    BuildContext context, {
    @required String title,
    @required String description,
    @required VoidCallback onConfirm,
    @required VoidCallback onCancel,
    IconData icon,
    String confirmTitle,
    String cancelTitle,
    TextAlign textAlign = TextAlign.center,
  }) =>
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => ActionSheet.confirm(
          title: title,
          description: description,
          onConfirm: onConfirm,
          onCancel: onCancel,
          icon: icon,
          confirmTitle: confirmTitle,
          cancelTitle: cancelTitle,
          textAlign: textAlign,
        ),
      );

  static showAlert(
    BuildContext context, {
    IconData icon,
    String title,
    String description,
    String cancelTitle,
    VoidCallback onCancel,
  }) =>
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => ActionSheet(
          // icon: Icon(
          //   icon ?? Icons.info_rounded,
          //   size: ScreenUtil().setHeight(50),
          //   color: ThemeUtils().getColor('sheet.icon'),
          // ),
          image: Image.asset(
            'assets/images/clean.png',
            width: ScreenUtil().setWidth(192),
            height: ScreenUtil().setHeight(192),
          ),

          title: title,
          description: description,
          actions: [
            Row(
              children: [
                StatusButton.withStyle(
                  StatusButtonStyle.Info,
                  title: cancelTitle ?? S.current.sheet_ok,
                  onPressedFuture: () {
                    onCancel != null ?? onCancel();
                    Navigator.of(context).pop();
                    return Future.value();
                  },
                ),
              ],
            )
          ],
        ),
      );

  Widget build(BuildContext context) => ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: Scaffold(
            appBar: NavigationBar(
              isBottomSheet: true,
              title: title,
            ),
            backgroundColor: ThemeUtils().getColor(
              'sheet.background',
            ),
            body: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // Container(
                      //   margin: EdgeInsets.symmetric(
                      //     vertical: ScreenUtil().setHeight(34),
                      //   ),
                      //   child: icon,
                      // ),
                      Container(
                        margin: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(34),
                        ),
                        child: image,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          // bottom: ScreenUtil().setHeight(60),
                          left: ScreenUtil().setHeight(24),
                          right: ScreenUtil().setHeight(24),
                        ),
                        child: Text(
                          description,
                          textAlign: textAlign,
                          maxLines: 3,
                          style: ThemeUtils()
                              .getTextStyle('sheet.textstyle.description'),
                        ),
                      ),
                      // Expanded(child: SizedBox()),
                      SizedBox(
                        height: ScreenUtil().setHeight(47),
                      ),
                    ]..addAll(
                        actions
                            .map(
                              (element) => Container(
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setHeight(24),
                                  right: ScreenUtil().setHeight(24),
                                  bottom: ScreenUtil().setHeight(15),
                                ),
                                child: element,
                              ),
                            )
                            .toList()
                              ..add(
                                Container(
                                  height: ScreenUtil().setHeight(30),
                                ),
                              ),
                      ),
                  ),
                ),
              ],
            )),
      );
}
