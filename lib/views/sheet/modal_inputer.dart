// @dart=2.9
part of 'sheet.dart';

// typedef VerifyResultCallback = Future<void> Function(
//     BuildContext context, String pwd);

typedef ModalInputerShouldEndEdit = String Function(String text);
typedef ModalInputerSubmited = void Function(String text);

class ModalInputer extends StatefulWidget {
  final String title;
  final String initalValue;
  final String placeholder;
  final TextInputFormatter inputFormatter;
  final TextInputType keyboardType;
  final bool obscureText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final ModalInputerShouldEndEdit shouldEndEditCallback;
  final ModalInputerSubmited submited;

  // final VerifyResultCallback verifyCallback;

  ModalInputer({
    @required this.title,
    this.initalValue = "",
    this.placeholder = "",
    this.inputFormatter,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.prefixIcon,
    this.suffixIcon,
    this.shouldEndEditCallback,
    this.submited,
  });

  @override
  State<StatefulWidget> createState() => _ModalInputerState();

  static show(
    BuildContext context, {
    @required String title,
    String initalValue = "",
    String placeholder,
    TextInputFormatter inputFormatter,
    TextInputType keyboardType = TextInputType.name,
    bool obscureText = false,
    Widget prefixIcon,
    Widget suffixIcon,
    ModalInputerShouldEndEdit shouldEndEditCallback,
    ModalInputerSubmited submited,
  }) =>
      showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        enableDrag: false,
        isDismissible: false,
        backgroundColor: Colors.transparent,
        builder: (context) => SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: ModalInputer(
              title: title,
              initalValue: initalValue,
              placeholder: placeholder,
              inputFormatter: inputFormatter,
              keyboardType: keyboardType,
              obscureText: obscureText,
              prefixIcon: prefixIcon,
              suffixIcon: suffixIcon,
              shouldEndEditCallback: shouldEndEditCallback,
              submited: submited,
            ),
          ),
        ),
      );

  static showAlert(
    BuildContext context, {
    @required String title,
    String initalValue = "",
    String placeholder,
    TextInputFormatter inputFormatter,
    TextInputType keyboardType = TextInputType.name,
    bool obscureText = false,
    Widget prefixIcon,
    Widget suffixIcon,
    ModalInputerShouldEndEdit shouldEndEditCallback,
    ModalInputerSubmited submited,
  }) =>
      showDialog(
        context: context,
        builder: (context) => SingleChildScrollView(
          child: Container(
            // padding: EdgeInsets.only(
            //   bottom: MediaQuery.of(context).viewInsets.bottom,
            // ),
            child: ModalInputer(
              title: title,
              initalValue: initalValue,
              placeholder: placeholder,
              inputFormatter: inputFormatter,
              keyboardType: keyboardType,
              obscureText: obscureText,
              prefixIcon: prefixIcon,
              suffixIcon: suffixIcon,
              shouldEndEditCallback: shouldEndEditCallback,
              submited: submited,
            ),
          ),
        ),
      );
  // showModalBottomSheet(
  //   isScrollControlled: true,
  //   context: context,
  //   enableDrag: false,
  //   isDismissible: false,
  //   backgroundColor: Colors.transparent,
  //   builder: (context) => SingleChildScrollView(
  //     child: Container(
  //       padding: EdgeInsets.only(
  //         bottom: MediaQuery.of(context).viewInsets.bottom,
  //       ),
  //       child: ModalInputer(
  //         title: title,
  //         initalValue: initalValue,
  //         placeholder: placeholder,
  //         inputFormatter: inputFormatter,
  //         keyboardType: keyboardType,
  //         obscureText: obscureText,
  //         prefixIcon: prefixIcon,
  //         suffixIcon: suffixIcon,
  //         shouldEndEditCallback: shouldEndEditCallback,
  //         submited: submited,
  //       ),
  //     ),
  //   ),
  // );
}

class _ModalInputerState extends State<ModalInputer> {
  bool ob = false;
  bool inVerifing = true;
  TextEditingController textEditingController;
  StatusButtonController commitButtonController;
  FocusNode inputFocusNode = FocusNode();
  ValueNotifier<String> checkErrorMessageListenable =
      ValueNotifier<String>(null);

  @override
  void initState() {
    textEditingController = TextEditingController(text: widget.initalValue);
    commitButtonController = StatusButtonController(
      title: S.current.sheet_verify_done,
      busyTitle: S.current.sheet_verify_verifing,
    );

    super.initState();
  }

  Widget build(BuildContext context) => Material(
        color: Colors.transparent,
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(265),
              left: ScreenUtil().setWidth(39),
              right: ScreenUtil().setWidth(39),
            ),
            // height: MediaQuery.of(context).size.height,
            height: ScreenUtil().setHeight(181),
            width: ScreenUtil().setWidth(299),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    top: ScreenUtil().setHeight(25),
                  ),
                  child: Center(
                    child: Text(
                      S.current.sheet_wallet_name,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(20),
                    right: ScreenUtil().setWidth(20),
                  ),
                  child: ValueListenableBuilder(
                    valueListenable: checkErrorMessageListenable,
                    builder: (context, value, child) => Container(
                      height: ScreenUtil().setHeight(46),
                      child: TextField(
                        focusNode: inputFocusNode,
                        maxLines: 1,
                        textInputAction: TextInputAction.done,
                        keyboardType: widget.keyboardType,
                        controller: textEditingController,
                        obscureText: this.ob,
                        autofocus: true,
                        onChanged: (_) {
                          if (checkErrorMessageListenable.value != null) {
                            checkErrorMessageListenable.value = null;
                          }
                        },
                        onEditingComplete: () {
                          if (widget.shouldEndEditCallback != null) {
                            checkErrorMessageListenable.value =
                                widget.shouldEndEditCallback(
                                    textEditingController.text);
                            if (checkErrorMessageListenable.value == null) {
                              Navigator.of(context).pop();
                              if (widget.submited != null) {
                                widget.submited(textEditingController.text);
                              }
                            } else {
                              FocusScope.of(context)
                                  .requestFocus(inputFocusNode);
                            }
                          } else {
                            Navigator.of(context).pop();
                            if (widget.submited != null) {
                              widget.submited(textEditingController.text);
                            }
                          }
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 0.0, horizontal: 11),
                          // fillColor: ThemeUtils().getColor(
                          //   'utils.input.fill_color',
                          // ),
                          fillColor: Color.fromRGBO(250, 250, 250, 1),
                          filled: true,
                          // prefixIcon: widget.prefixIcon ??
                          //     Icon(
                          //       Icons.edit,
                          //       color: ThemeUtils().getColor(
                          //         'utils.input.border_color',
                          //       ),
                          //     ),
                          // suffixIcon: InkWell(
                          //   onTap: () {
                          //     ob = !ob;
                          //   },
                          //   child: widget.suffixIcon ??
                          //       Image.asset(
                          //         this.ob == false
                          //             ? "assets/images/eye_off.png"
                          //             : "assets/images/eye_on.png",
                          //         width: ScreenUtil().setWidth(20.55),
                          //         height: ScreenUtil().setHeight(8),
                          //       ),
                          // ),
                          // IconButton(
                          //   color: ThemeUtils().getColor(
                          //     'utils.input.border_color',
                          //   ),
                          //   icon: Icon(Icons.clear),
                          //   onPressed: () {
                          //     setState(() {
                          //       textEditingController.text = '';
                          //       checkErrorMessageListenable.value =
                          //           null;
                          //     });
                          //   },
                          // ),
                          hintText: widget.placeholder,
                          hintStyle: TextStyle(
                              fontSize: ScreenUtil().setSp(14),
                              color: Color.fromRGBO(175, 175, 175, 1)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(4))),
                            borderSide: BorderSide(
                              // color: Colors.transparent,
                              color: Color.fromRGBO(216, 216, 216, 1),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(4))),
                            borderSide:
                                // BorderSide(color: Colors.transparent),
                                BorderSide(
                              color: Color.fromRGBO(216, 216, 216, 1),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(4))),
                            borderSide:
                                // BorderSide(color: Colors.transparent),
                                BorderSide(
                              color: Color.fromRGBO(216, 216, 216, 1),
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                ScreenUtil().setWidth(4),
                              ),
                            ),
                            borderSide:
                                // BorderSide(color: Colors.transparent),
                                BorderSide(
                              color: Color.fromRGBO(216, 216, 216, 1),
                            ),
                          ),
                          // errorText: checkErrorMessageListenable.value,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        width: 0.3,
                        color: Colors.grey[300],
                      ),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              right: BorderSide(
                                width: 0.3,
                                color: Colors.grey[300],
                              ),
                            ),
                          ),
                          height: ScreenUtil().setHeight(46),
                          alignment: Alignment.center,
                          width: (MediaQuery.of(context).size.width -
                                  ScreenUtil().setWidth(78)) /
                              2,
                          child: Text(
                            S.current.sheet_cancel,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          if (widget.shouldEndEditCallback != null) {
                            checkErrorMessageListenable.value =
                                widget.shouldEndEditCallback(
                                    textEditingController.text);
                            if (checkErrorMessageListenable.value == null) {
                              Navigator.of(context).pop();
                              if (widget.submited != null) {
                                widget.submited(textEditingController.text);
                              }
                            } else {
                              FocusScope.of(context)
                                  .requestFocus(inputFocusNode);
                            }
                          } else {
                            Navigator.of(context).pop();
                            if (widget.submited != null) {
                              widget.submited(textEditingController.text);
                            }
                          }
                        },
                        child: Container(
                          height: ScreenUtil().setHeight(46),
                          width: (MediaQuery.of(context).size.width -
                                  ScreenUtil().setWidth(78)) /
                              2,
                          alignment: Alignment.center,
                          child: Text(
                            S.current.sheet_ok,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      );
}
