// @dart=2.9

part of 'sheet.dart';

// ignore: must_be_immutable
class _MessageSign extends _InternalSheet with ChangeNotifier {
  _MessageSign({
    @required Map<String, dynamic> requestData,
    @required Wallet wallet,
    @required WalletKey account,
    @required Dapp dapp,
    @required Web3ClientProvider web3clientProvider,
    @required NetworkConfig networkConfig,
    @required DataSourceProvider dataSourceProvider,
    @required ScanAPIService scanAPIService,
  }) : super(
          requestData: requestData,
          title: '申请签名',
          wallet: wallet,
          account: account,
          dapp: dapp,
          web3clientProvider: web3clientProvider,
          networkConfig: networkConfig,
          dataSourceProvider: dataSourceProvider,
          scanAPIService: scanAPIService,
        );

  @override
  bool shouldShowAdvanceSwitch() => false;

  @override
  Future<bool> initDate() async {
    print(requestData);
  }

  void signMessage(BuildContext context, String message) async {
    // int chainId = await web3clientProvider.getChainID();

    message = utf8.decode(hexToBytes(message));
    final certificate = CertificateEthereum(
      privateKey: bytesToHex(
        account.privateKeyRaw,
      ),
    );

    final signature = await certificate.signPersonalMessage(
      message: message,
    );

    print('Message:$message Sign:$signature');

    Navigator.of(context).pop<String>(signature);
  }

  @override
  Widget body(
    BuildContext context,
    BigInt estimatedGas,
    dynamic error,
  ) {
    return Container(
      height: MediaQuery.of(context).size.height / 1.4,
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(20),
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
              ),
              child: ValueListenableBuilder<_TransactionPayment>(
                valueListenable: txPaymentInfoListenable,
                builder: (context, _, child) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.drive_file_rename_outline,
                        size: ScreenUtil().setHeight(40),
                      ),
                      margin: EdgeInsets.all(ScreenUtil().setHeight(20)),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        bottom: ScreenUtil().setHeight(20),
                      ),
                      child: Text(
                        '"${dapp.name ?? dapp.url}"正在申请签名',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 2,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        bottom: ScreenUtil().setHeight(40),
                      ),
                      padding: EdgeInsets.all(40),
                      color: Color(0xFFF0F0F0),
                      child: Text(
                        requestData['object']['data'],
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.normal,
                        ),
                        maxLines: 2,
                      ),
                    ),
                  ]..add(
                      Container(
                        child: StatusButton.withStyle(
                          StatusButtonStyle.Info,
                          title: '签名',
                          onPressedFuture: () async {
                            signMessage(context, requestData['object']['data']);
                          },
                        ),
                      ),
                    ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
