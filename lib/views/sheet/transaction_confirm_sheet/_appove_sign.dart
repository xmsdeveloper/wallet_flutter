// @dart=2.9

part of 'sheet.dart';

// ignore: must_be_immutable
class _TokenAppoveSign extends _InternalSheet with ChangeNotifier {
  TokenBlog appoveToken;

  Amount appoveAmount;

  String appoveAddress;

  final callAppove = ContractFunction(
    'approve',
    [
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, UintType()),
    ],
    outputs: [
      FunctionParameter(null, BoolType()),
    ],
    type: ContractFunctionType.function,
    mutability: StateMutability.nonPayable,
  );

  _TokenAppoveSign({
    @required Map<String, dynamic> requestData,
    @required Wallet wallet,
    @required WalletKey account,
    @required Dapp dapp,
    @required Web3ClientProvider web3clientProvider,
    @required NetworkConfig networkConfig,
    @required DataSourceProvider dataSourceProvider,
    @required ScanAPIService scanAPIService,
  }) : super(
          requestData: requestData,
          title: '授权',
          wallet: wallet,
          account: account,
          dapp: dapp,
          web3clientProvider: web3clientProvider,
          networkConfig: networkConfig,
          dataSourceProvider: dataSourceProvider,
          scanAPIService: scanAPIService,
        );

  void configDefaultPaymentInfo() {
    txPaymentInfoListenable.value = _TransactionPayment(
      (requestData['object'] as Map).containsKey('value')
          ? Amount(
              value: hexToInt(requestData['object']['value']),
              decimals: 18,
            )
          : Amount(value: BigInt.zero, decimals: 18),
      super._gas,
      Amount(
        value: gasPriceConfig.gasConfig.propose,
        decimals: 9,
      ),
    );
  }

  @override
  bool shouldShowAdvanceSwitch() => false;

  @override
  Future<bool> initDate() async {
    final tokenContractAddrerss = requestData['object']['to'];
    final data = requestData['object']['data'] as String;

    /// 先尝试从本地的favoriteTokens中取得需要授权的代币信息,若本地没有收藏
    /// 则尝试从合约中获取这些数据
    try {
      appoveToken = await dataSourceProvider.localService
          .findTokenBlogWithAddress(tokenContractAddrerss);
    } catch (e) {
      print(e);
    }

    if (appoveToken == null) {
      appoveToken = await futureGetTokenBlog(
        web3clientProvider,
        tokenContractAddrerss,
        account.address,
      );
    }

    List parmas = callAppove.decodeCall(
      data.substring(data.startsWith('0x') ? 10 : 8),
    );

    appoveAddress = parmas.first as String;

    appoveAmount = Amount(
      value: parmas.last as BigInt,
      decimals: appoveToken.decimals,
    );

    return true;
  }

  @override
  Widget body(
    BuildContext context,
    BigInt estimatedGas,
    dynamic error,
  ) {
    if (txPaymentInfoListenable.value == null) configDefaultPaymentInfo();

    return Container(
      height: MediaQuery.of(context).size.height / 1.4,
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child: Container(
            padding: EdgeInsets.only(
              bottom: ScreenUtil().setHeight(20),
              left: ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(20),
            ),
            child: ValueListenableBuilder<_TransactionPayment>(
              valueListenable: txPaymentInfoListenable,
              builder: (context, _, child) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.wifi_protected_setup_rounded,
                      size: ScreenUtil().setHeight(40),
                    ),
                    margin: EdgeInsets.all(
                      ScreenUtil().setHeight(20),
                    ),
                  ),

                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(
                      bottom: ScreenUtil().setHeight(20),
                    ),
                    child: Text(
                      '您是否信任"${dapp.name ?? dapp.url}"?',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 2,
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(
                      bottom: ScreenUtil().setHeight(40),
                    ),
                    child: Text(
                      '授权即表示您允许在指定的额度内"${dapp.name ?? dapp.url}"使用您的${appoveToken.symbol}，并且为您自动进行交易。',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                      ),
                      maxLines: 2,
                    ),
                  ),

                  _InputFieldCell(
                    '申请的额度',
                    appoveAmount.toStringAsFixed(),
                    symbol: appoveToken.symbol,
                    textInputType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(
                        RegExp(r'^[1-9]+[0-9]*'),
                      ),
                    ],
                    onChanged: (value) {
                      appoveAmount = Amount.fromString(
                        value,
                        decimals: appoveToken.decimals,
                        valueDecimals: appoveToken.decimals,
                      );

                      /// 修改原始交易数据
                      requestData['object']['data'] = callAppove.encodeCall([
                        appoveAddress,
                        appoveAmount.value,
                      ]);
                    },
                  ),

                  /// 如果手动设置的手续费在取值范围内，则正常显示滑动条，否则需要更换样式
                  if (txPaymentInfoListenable.value.gasPrice.value >=
                          gasPriceConfig.gasConfig.safe &&
                      txPaymentInfoListenable.value.gasPrice.value <=
                          gasPriceConfig.gasConfig.fast)
                    FeeConfigBoard(
                      defaultValue: (BigInt min, BigInt max, BigInt curr) {
                        return (curr - min).toDouble() / (max - min).toDouble();
                      }(
                          gasPriceConfig.gasConfig.safe,
                          gasPriceConfig.gasConfig.fast,
                          txPaymentInfoListenable.value.gasPrice.value),
                      feeTokenBlog: networkConfig.feeTokenBlog,
                      totalStringCallBack: (v) => txPaymentInfoListenable.value
                          .toFeeString(networkConfig.feeTokenBlog),
                      lableCallBack: (v) {
                        final gasPriceWei = gasPriceConfig.gasConfig.safe +
                            (gasPriceConfig.gasConfig.fast -
                                    gasPriceConfig.gasConfig.safe) *
                                BigInt.from((v * 100).toInt()) ~/
                                BigInt.from(100);

                        final gasPriceAmount = Amount(
                          value: gasPriceWei,
                          decimals: 9,
                        );

                        return '${gasPriceAmount.toStringAsFixed()} Gwei';
                      },
                      onChanged: (v) {
                        final gasPriceWei = gasPriceConfig.gasConfig.safe +
                            (gasPriceConfig.gasConfig.fast -
                                    gasPriceConfig.gasConfig.safe) *
                                BigInt.from((v * 100).toInt()) ~/
                                BigInt.from(100);

                        txPaymentInfoListenable.value.gasPrice = Amount(
                          value: gasPriceWei,
                          decimals: 9,
                        );
                        txPaymentInfoListenable.notifyListeners();
                      },
                    )
                  else
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        _FieldCell(
                          '自定义手续费',
                          txPaymentInfoListenable.value.gasPrice
                                  .toStringAsFixed(
                                fractionDigits: 2,
                                format: false,
                              ) +
                              ' Gwei',
                        ),
                        // Expanded(child: SizedBox()),
                        SizedBox(
                          height: 20,
                        ),
                        TextButton(
                            child: Text(
                              '使用推荐',
                              style: ThemeUtils().getTextStyle(
                                'views.chaincore.ethereum.dapp.textstyle.default_fee',
                              ),
                            ),
                            onPressed: () {
                              configDefaultPaymentInfo();
                            }),
                      ],
                    )
                ]
                  ..add(SizedBox(height: 30))
                  ..add(
                    Container(
                      child: StatusButton.withStyle(
                        StatusButtonStyle.Info,
                        title: '下一步',
                        onPressedFuture: () async {
                          super.confirmTransaction(context);
                        },
                      ),
                    ),
                  ),
              ),
            ),
          ))
        ],
      ),
    );
  }
}
