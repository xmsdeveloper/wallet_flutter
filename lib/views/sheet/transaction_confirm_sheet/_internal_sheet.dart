// @dart=2.9
part of 'sheet.dart';

// ignore: must_be_immutable
abstract class _InternalSheet extends StatelessWidget {
  @protected
  BigInt _gas;

  @protected
  dynamic _payloadError;

  @protected
  EstimatedGasPriceResult _gasPriceConfig;
  EstimatedGasPriceResult get gasPriceConfig => _gasPriceConfig;

  /// 用户最终使用手续费存储
  final ValueNotifier<_TransactionPayment> txPaymentInfoListenable =
      ValueNotifier<_TransactionPayment>(null);

  @protected
  final ValueNotifier<bool> _advancedSwitchListenable =
      ValueNotifier<bool>(false);
  bool get isAdvancedMode => _advancedSwitchListenable.value;

  final ValueNotifier<CrossFadeState> _crossFadeStateListenable =
      ValueNotifier<CrossFadeState>(CrossFadeState.showFirst);

  final String title;
  final Map<String, dynamic> requestData;
  final Wallet wallet;
  final WalletKey account;
  final Dapp dapp;

  final NetworkConfig networkConfig;
  final DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final ScanAPIService scanAPIService;

  final StreamController<EventInfo> eventStream;

  //是否需要输入密码
  bool isPassWorld = true;

  /// 数据是否完成填充
  Completer<void> _payloaded = Completer();

  _InternalSheet({
    @required this.requestData,
    @required this.title,
    @required this.wallet,
    @required this.account,
    @required this.dapp,
    @required this.web3clientProvider,
    @required this.networkConfig,
    @required this.dataSourceProvider,
    @required this.scanAPIService,
    this.eventStream,
  }) {
    // {"name":"signPersonalMessage","object":{"data":"0x61616161"},"id":1620318618635}
    if (requestData['name'] == 'signPersonalMessage') {
      initDate().then(_payloaded.complete);
      return;
    }

    /// 原始交易进入后，有一些必须的收据暂未获取到,需要填充剩余数据，而自页面必须依赖这些数据
    /// 为了降低子页面的实现复杂度，在本类中，如果暂时没有拿到全部数据，会统一在页面中显示一个
    /// loading状态，等到payloading完成后，在开始渲染子页面，此时，所有数据已填充完毕。
    Future.wait([
      /// 估算Gas
      web3clientProvider
          .estimateGas(
        from: account.address,
        to: requestData['object']['to'],
        data: (requestData['object'] as Map).containsKey('data')
            ? hexToBytes(requestData['object']['data'])
            : null,
        value: (requestData['object'] as Map).containsKey('value')
            ? hexToInt(requestData['object']['value'])
            : null,
      )
          .catchError((error) {
        _payloadError = error;
        return BigInt.from(3000000);
      }),

      /// 估算GasPrice
      estimatedGasPrice(
        networkConfig,
        dataSourceProvider,
        web3clientProvider,
        scanAPIService,
      ),
    ]).then((value) {
      this._gas = value[0] as BigInt;
      this._gasPriceConfig = value[1] as EstimatedGasPriceResult;

      emit(EventEmit.OnPayloaded);
      return initDate().then(_payloaded.complete);
    }).catchError((e) {
      emit(EventEmit.OnError, e);
    });
  }

  /// 派生类需要控制顶部高级设置开关时重写，不重写则使用默认规则，第一个页面显示，后续不显示
  bool shouldShowAdvanceSwitch() => null;

  /// 可选重写,当派生类除了通用填充的数据意外还需要填充其他数据时，可以重写此方法
  Future<bool> initDate() async => Future.value(true);

  /// 主内容
  Widget body(BuildContext context, BigInt estimatedGas, dynamic error);

  BuildContext contextnew;
  var hash;

  /// 成功发送交易后的回调，默认为页面消失
  void transactionSented(BuildContext context, String hash) {
    // Future.delayed(Duration(milliseconds: 2000), () {
    //   Navigator.of(context).pop<String>(hash);
    // });
    Navigator.of(context).pop<String>(hash);
  }

  bool isAuth;
  void confirmTransaction(context) async {
    // if (ConfigStorageUtils().readIsPassWordIndexOfKey(ConfigKey.IsPassWord) ==
    //     true) {
    //   _crossFadeStateListenable.value = CrossFadeState.showSecond;
    // } else {
    //   sendTransactionAndPop(context);
    // }
    // _crossFadeStateListenable.value = CrossFadeState.showSecond;

    // isAuth = ConfigStorageUtils().readAuthOfKey(ConfigKey.IsAuth);

    // if (isAuth == false) {
    //   _crossFadeStateListenable.value = CrossFadeState.showSecond;
    // } else {
    //   final isAuthenticated = await localAuthApi.authenticate();

    //   Future.delayed(Duration(milliseconds: 1000), () {
    //     sendTransactionAndPop(context);
    //   });
    //   // if (isAuthenticated == true) {
    //   //   Navigator.of(context).pop<String>();
    //   //   // sendTransactionAndPop(context);
    //   // }
    _crossFadeStateListenable.value = CrossFadeState.showSecond;
  }

  void emit(EventEmit event, [dynamic object]) {
    if (eventStream != null && !eventStream.isClosed) {
      eventStream.sink.add(EventInfo(event: event, object: object));
    }
  }

  /// 输入了正确的密码后,为交易签名，并且返回最终结果
  void sendTransactionAndPop(BuildContext context) async {
    final nonce = await web3clientProvider.getTransactionCount(account.address);
    final networkId = await web3clientProvider.getChainID();

    final certificate = CertificateEthereum(
      privateKey: bytesToHex(
        account.privateKeyRaw,
      ),
    );

    Map txObject = requestData['object'] as Map;

    final transactionRaw = await certificate.signTransaction(
      chainId: HexString.fromNumber(networkId),
      to: HexString.fromHex(txObject['to']),
      nonce: HexString.fromNumber(nonce),
      gasPrice:
          HexString.fromBigInt(txPaymentInfoListenable.value.gasPrice.value),
      gasLimit: HexString.fromBigInt(txPaymentInfoListenable.value.gas),
      data: txObject.containsKey('data')
          ? HexString.fromHex(txObject['data'])
          : null,
      value: txObject.containsKey('value')
          ? HexString.fromHexNumber(txObject['value'])
          : null,
    );

    final txHash = await web3clientProvider.sendRawTransaction(transactionRaw);
    emit(EventEmit.OnHash, txHash);
    transactionSented(context, txHash);
    hash = txHash;
    // ConfigStorageUtils().writeIsPassWordIndexOfKey(ConfigKey.IsPassWord, false);

    return;
  }

  @override
  Widget build(BuildContext context) => ValueListenableBuilder<bool>(
        valueListenable: _advancedSwitchListenable,
        builder: (context, _, child) => Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              height: ScreenUtil().setHeight(110),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                child: ValueListenableBuilder<CrossFadeState>(
                  valueListenable: _crossFadeStateListenable,
                  builder: (context, state, child) => NavigationBar(
                    isBottomSheet: true,
                    title: title,
                    leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        size: 20,
                      ),
                      onPressed: () {
                        state == CrossFadeState.showSecond
                            ? _crossFadeStateListenable.value =
                                CrossFadeState.showFirst
                            : Navigator.of(context, rootNavigator: true)
                                .pop<String>(hash);
                      },
                    ),
                    actions: (shouldShowAdvanceSwitch() ??
                            state == CrossFadeState.showFirst)
                        ? [
                            Switch(
                              value: _advancedSwitchListenable.value,
                              onChanged: (isOn) {
                                _advancedSwitchListenable.value = isOn;
                              },
                            ),
                          ]
                        : null,
                    bottom: PreferredSize(
                      preferredSize:
                          Size.fromHeight(ScreenUtil().setHeight(40)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          /// FromAccount
                          Container(
                            margin: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(10),
                              left: ScreenUtil().setWidth(20),
                            ),
                            width: (ScreenUtil().screenWidth - 70) / 2,
                            height: ScreenUtil().setHeight(40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  account.avatarPath,
                                  package: 'wallet_flutter',
                                  width: ScreenUtil().setHeight(28),
                                  height: ScreenUtil().setHeight(28),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(10)),
                                Text(
                                  account.nickName,
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.dapp.textstyle.header_address_or_name',
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                bottom: ScreenUtil().setHeight(10)),
                            width: ScreenUtil().setHeight(30),
                            height: ScreenUtil().setHeight(30),
                            child: Icon(
                              Icons.send_rounded,
                              size: 20,
                              color: ThemeUtils().getColor(
                                'views.chaincore.ethereum.dapp.sheet_header_icon_color',
                              ),
                            ),
                          ),

                          /// ToAccount
                          Container(
                            margin: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(10),
                              right: ScreenUtil().setWidth(20),
                            ),
                            width: (ScreenUtil().screenWidth - 70) / 2,
                            height: ScreenUtil().setHeight(40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Expanded(
                                  child: Text(
                                    dapp.name,
                                    textAlign: TextAlign.right,
                                    maxLines: 1,
                                    style: ThemeUtils().getTextStyle(
                                      'views.chaincore.ethereum.dapp.textstyle.header_address_or_name',
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                if (dapp.logo != null && dapp.logo.length >= 0)
                                  SizedBox(width: ScreenUtil().setWidth(10)),
                                if (dapp.logo != null && dapp.logo.length >= 0)
                                  CachedImage.assetNetwork(
                                    url: dapp.logo,
                                    placeholder: Image.asset(
                                      'assets/imgs/default_dapp.png',
                                      package: 'wallet_flutter',
                                    ),
                                    width: ScreenUtil().setHeight(28),
                                    height: ScreenUtil().setHeight(28),
                                  ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            FutureBuilder(
              future: _payloaded.future,
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return Container();
                } else {
                  return Container(
                    color: Colors.white,
                    child: ValueListenableBuilder<CrossFadeState>(
                      valueListenable: _crossFadeStateListenable,
                      builder: (context, state, child) => AnimatedCrossFade(
                        firstChild: body(
                          context,
                          _gas,
                          _payloadError,
                        ),
                        secondChild: VerifySheet(
                          internalMode: true,
                          verifyCallback: (context, pwd) {
                            if (!wallet.verifySecurity(pwd)) {
                              return S.current.password_verify_faild;
                            } else {
                              sendTransactionAndPop(context);
                              return null;
                            }
                          },
                        ),
                        crossFadeState: state,
                        duration: Duration(milliseconds: 250),
                      ),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      );
}
