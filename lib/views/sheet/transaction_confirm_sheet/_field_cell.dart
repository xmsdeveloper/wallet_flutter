// @dart=2.9
part of 'sheet.dart';

class _FieldCell extends StatelessWidget {
  final String title;
  final String subTitleDesc;
  final String subTitle;
  final bool isAddress;
  final String detail;

  const _FieldCell(
    this.title,
    this.subTitle, {
    this.subTitleDesc,
    this.isAddress = false,
    this.detail,
  });

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setHeight(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.dapp.textstyle.field_title',
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            if (subTitleDesc != null && subTitleDesc.length > 0 && !isAddress)
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    subTitle,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.dapp.textstyle.field_content',
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Text(
                    subTitleDesc,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.dapp.textstyle.field_detail',
                    ),
                  )
                ],
              )
            else
              Text(
                subTitle,
                style: ThemeUtils().getTextStyle(
                  isAddress
                      ? 'views.chaincore.ethereum.dapp.textstyle.field_content_long'
                      : 'views.chaincore.ethereum.dapp.textstyle.field_content',
                ),
              ),
            if (detail != null && detail.length > 0)
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
            if (detail != null && detail.length > 0)
              Text(
                detail,
                style: ThemeUtils().getTextStyle(
                  'views.chaincore.ethereum.dapp.textstyle.field_detail',
                ),
              ),
          ],
        ),
      );
}

class _InputFieldCell extends StatelessWidget {
  final String title;
  final String initValue;
  final ValueChanged<String> onChanged;
  final TextInputType textInputType;
  final List<TextInputFormatter> inputFormatters;
  final String symbol;

  const _InputFieldCell(
    this.title,
    this.initValue, {
    @required this.onChanged,
    this.symbol,
    this.textInputType = TextInputType.number,
    this.inputFormatters = const [],
  });

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setHeight(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.dapp.textstyle.field_title',
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            TextFormField(
              initialValue: initValue,
              onChanged: onChanged,
              keyboardType: textInputType,
              inputFormatters: inputFormatters,
              style: ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.dapp.textstyle.field_content',
              ),
              decoration: symbol != null
                  ? InputDecoration(
                      suffixText: symbol,
                      suffixStyle: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.dapp.textstyle.field_symbol',
                      ),
                    )
                  : null,
            )
          ],
        ),
      );
}
