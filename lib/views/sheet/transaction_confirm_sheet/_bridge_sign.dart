// @dart=2.9

part of 'sheet.dart';

// ignore: must_be_immutable
class _BridgeSign extends _InternalSheet with ChangeNotifier {
  Amount sentAmount;
  Amount fee;
  TokenBlog bridgeToken;
  ValueNotifier<Widget> progressBarLisentable;
  ValueNotifier<bool> showProgressLisentable = ValueNotifier(false);
  NetworkConfig targetNetworkConfig;

  String targetNetworkReceiptor;

  _BridgeSign({
    @required Map<String, dynamic> requestData,
    @required Wallet wallet,
    @required WalletKey account,
    @required Dapp dapp,
    @required Web3ClientProvider web3clientProvider,
    @required NetworkConfig networkConfig,
    @required DataSourceProvider dataSourceProvider,
    @required ScanAPIService scanAPIService,
    @required StreamController<EventInfo> eventStream,
  }) : super(
          requestData: requestData,
          title: '跨链交易',
          wallet: wallet,
          account: account,
          dapp: dapp,
          web3clientProvider: web3clientProvider,
          networkConfig: networkConfig,
          dataSourceProvider: dataSourceProvider,
          scanAPIService: scanAPIService,
          eventStream: eventStream,
        );

  @override
  Future<bool> initDate() async {
    sentAmount = requestData['amount'];
    fee = requestData['fee'];
    bridgeToken = requestData['token'];
    progressBarLisentable = requestData['statebuilder'];
    targetNetworkConfig = requestData['target_network'];
    targetNetworkReceiptor = requestData['receiptor'];
    return Future.value(true);
  }

  @override
  void transactionSented(BuildContext context, String hash) {
    if (eventStream == null) {
      super.transactionSented(context, hash);
    } else {
      showProgressLisentable.value = true;
      _crossFadeStateListenable.value = CrossFadeState.showFirst;
    }
  }

  @override
  bool shouldShowAdvanceSwitch() => !showProgressLisentable.value;

  void configDefaultPaymentInfo() {
    txPaymentInfoListenable.value = _TransactionPayment(
      (requestData['object'] as Map).containsKey('value')
          ? Amount(
              value: hexToInt(requestData['object']['value']),
              decimals: 18,
            )
          : Amount(value: BigInt.zero, decimals: 18),
      super._gas,
      Amount(
        value: gasPriceConfig.gasConfig.propose,
        decimals: 9,
      ),
    );
  }

  Widget _content(bool advancedMode) {
    return Container(
      padding: EdgeInsets.only(
        bottom: ScreenUtil().setHeight(20),
        left: ScreenUtil().setWidth(20),
        right: ScreenUtil().setWidth(20),
      ),
      child: ValueListenableBuilder<_TransactionPayment>(
        valueListenable: txPaymentInfoListenable,
        builder: (context, _, child) => ValueListenableBuilder(
          valueListenable: showProgressLisentable,
          builder: (context, showProgress, child) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 15,
              ),
              _BridgeChainCell(
                title: 'From',
                icon: CachedImage.assetNetwork(
                  url: networkConfig.logoURL,
                ),
                name: networkConfig.chainName,
                address: account.address,
              ),
              Stack(
                children: [
                  Center(
                    child: Text(
                      '${sentAmount.toStringAsFixed(fractionDigits: 6)} ${bridgeToken.symbol}',
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.dapp.textstyle.bridge_amount',
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 12,
                      ),
                      Icon(
                        Icons.south,
                        size: 20,
                      )
                    ],
                  ),
                ],
              ),
              _BridgeChainCell(
                  title: 'To',
                  icon: CachedImage.assetNetwork(
                    url: targetNetworkConfig.logoURL,
                  ),
                  name: targetNetworkConfig.chainName,
                  address: targetNetworkReceiptor),

              if (!showProgress)
                _FieldCell(
                  '手续费',
                  // '${fee.toStringAsFixed()} ${bridgeToken.symbol}',
                  '限时全免',
                ),
              if (!showProgress)
                Divider(
                  height: 2,
                  color: Color(0xFFE4E4E4),
                ),

              /// Gas
              if (advancedMode && !showProgress)
                _InputFieldCell(
                  'Gas',
                  txPaymentInfoListenable.value.gas.toString(),
                  symbol: 'Gas',
                  textInputType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                      RegExp(r'^[1-9]+[0-9]*'),
                    ),
                  ],
                  onChanged: (value) {
                    txPaymentInfoListenable.value.gas = BigInt.parse(value);
                    txPaymentInfoListenable.notifyListeners();
                  },
                ),

              /// GasPrice
              if (advancedMode && !showProgress)
                _InputFieldCell(
                  'GasPrice',

                  /// 显示时需要使用GWei单位
                  txPaymentInfoListenable.value.gasPrice.toStringAsFixed(
                    fractionDigits: 2,
                    format: false,
                  ),
                  symbol: 'Gwei',
                  textInputType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                      RegExp(r'^[0-9]+(\.[0-9]{0,2})?'),
                    ),
                  ],
                  onChanged: (value) {
                    var newGasPrice = Amount.fromString(
                      value,
                      decimals: 9,
                      valueDecimals: 9,
                    );

                    /// 不允许手动设置低于最对安全值的GasPrice
                    if (newGasPrice.value < gasPriceConfig.gasConfig.safe) {
                      newGasPrice = Amount(
                        value: gasPriceConfig.gasConfig.safe,
                        decimals: 9,
                      );
                    }

                    txPaymentInfoListenable.value.gasPrice = newGasPrice;
                    txPaymentInfoListenable.notifyListeners();
                  },
                ),

              /// GasConfigSlider
              if (!advancedMode && !showProgress)

                /// 如果手动设置的手续费在取值范围内，则正常显示滑动条，否则需要更换样式
                if (txPaymentInfoListenable.value.gasPrice.value >=
                        gasPriceConfig.gasConfig.safe &&
                    txPaymentInfoListenable.value.gasPrice.value <=
                        gasPriceConfig.gasConfig.fast)
                  FeeConfigBoard(
                    defaultValue: (BigInt min, BigInt max, BigInt curr) {
                      return (curr - min).toDouble() / (max - min).toDouble();
                    }(
                      gasPriceConfig.gasConfig.safe,
                      gasPriceConfig.gasConfig.fast,
                      txPaymentInfoListenable.value.gasPrice.value,
                    ),
                    feeTokenBlog: networkConfig.feeTokenBlog,
                    totalStringCallBack: (v) =>
                        txPaymentInfoListenable.value.toFeeString(
                      networkConfig.feeTokenBlog,
                    ),
                    lableCallBack: (v) {
                      final gasPriceWei = gasPriceConfig.gasConfig.safe +
                          (gasPriceConfig.gasConfig.fast -
                                  gasPriceConfig.gasConfig.safe) *
                              BigInt.from((v * 100).toInt()) ~/
                              BigInt.from(100);

                      final gasPriceAmount = Amount(
                        value: gasPriceWei,
                        decimals: 9,
                      );

                      return '${gasPriceAmount.toStringAsFixed()} Gwei';
                    },
                    onChanged: (v) {
                      final gasPriceWei = gasPriceConfig.gasConfig.safe +
                          (gasPriceConfig.gasConfig.fast -
                                  gasPriceConfig.gasConfig.safe) *
                              BigInt.from((v * 100).toInt()) ~/
                              BigInt.from(100);

                      txPaymentInfoListenable.value.gasPrice = Amount(
                        value: gasPriceWei,
                        decimals: 9,
                      );
                      txPaymentInfoListenable.notifyListeners();
                    },
                  )
                else
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      _FieldCell(
                        '自定义手续费',
                        txPaymentInfoListenable.value.gasPrice.toStringAsFixed(
                              fractionDigits: 2,
                              format: false,
                            ) +
                            ' Gwei',
                      ),
                      Expanded(child: SizedBox()),
                      TextButton(
                          child: Text(
                            '使用推荐',
                            style: ThemeUtils().getTextStyle(
                              'views.chaincore.ethereum.dapp.textstyle.default_fee',
                            ),
                          ),
                          onPressed: () {
                            configDefaultPaymentInfo();
                          }),
                    ],
                  ),
            ]
              ..add(
                SizedBox(
                  height:
                      showProgress && progressBarLisentable != null ? 10 : 30,
                ),
              )
              ..add(
                showProgress && progressBarLisentable != null
                    ? ValueListenableBuilder(
                        valueListenable: progressBarLisentable,
                        builder: (context, progressBar, child) =>
                            progressBar != null ? progressBar : Container(),
                      )
                    : Container(
                        child: StatusButton.withStyle(
                          StatusButtonStyle.Info,
                          title: '下一步',
                          onPressedFuture: () async {
                            super.confirmTransaction(context);
                          },
                        ),
                      ),
              ),
          ),
        ),
      ),
    );
  }

  @override
  Widget body(
    BuildContext context,
    BigInt estimatedGas,
    dynamic error,
  ) {
    if (txPaymentInfoListenable.value == null) configDefaultPaymentInfo();

    return AnimatedCrossFade(
      firstChild: _content(true),
      secondChild: _content(false),
      crossFadeState:
          isAdvancedMode ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      duration: Duration(milliseconds: 250),
    );
  }
}

class _BridgeChainCell extends StatelessWidget {
  final Widget icon;
  final String title;
  final String name;
  final String address;

  _BridgeChainCell({
    @required this.title,
    @required this.icon,
    @required this.name,
    @required this.address,
  });

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 50,
              decoration: BoxDecoration(
                color: Color(0xFFF5F5F5),
                borderRadius: BorderRadius.circular(
                  ScreenUtil().setWidth(5),
                ),
              ),
              child: Row(
                children: [
                  Container(
                    height: 20,
                    margin: EdgeInsets.only(
                      left: 10,
                      right: 10,
                    ),
                    child: icon,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.dapp.textstyle.bridge_chain_name',
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        address,
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.dapp.textstyle.bridge_chain_address',
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      );
}
