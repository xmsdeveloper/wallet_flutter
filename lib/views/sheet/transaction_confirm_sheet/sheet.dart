// @dart=2.9

import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screen_util.dart';

import 'package:wallet_core_flutter/utils/utils.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/gas_config_board.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/gas_price_estimated_utils.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/token_utils.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

part '_transaction_sign.dart';
part '_internal_sheet.dart';
part '_field_cell.dart';
part '_appove_sign.dart';
part '_bridge_sign.dart';
part '_message_sign.dart';

enum EventEmit {
  // 数据填充完成
  OnPayloaded,
  OnHash,
  OnReceipt,
  OnError,
}

class EventInfo {
  final EventEmit event;
  final dynamic object;

  const EventInfo({
    @required this.event,
    @required this.object,
  });
}

class _TransactionPayment {
  final Amount value;

  BigInt gas;

  /// gasPrice
  Amount gasPrice;

  _TransactionPayment(
    this.value,
    this.gas,
    this.gasPrice,
  );

  /// 转化为手续费计算过程的Striing
  String toFeeString(FeeTokenBlog tokenBlog) =>
      Amount.fromString((gas * gasPrice.value).toString(),
              decimals: tokenBlog.decimals)
          .toStringAsFixed(fractionDigits: 6) +
      ' ${tokenBlog.symbol}';

  /// 交易的整体费用
  String toTotalString(FeeTokenBlog tokenBlog) =>
      Amount.fromString((value.value + gas * gasPrice.value).toString(),
              decimals: tokenBlog.decimals)
          .toStringAsFixed(fractionDigits: 6) +
      ' ${tokenBlog.symbol}';
}

enum _TransactionType {
  /// 发送链上主币
  Sent,

  /// 调用合约
  Invoke,

  /// 调用合约,并且需要支付主币
  InvokePayable,

  /// 授权代币
  AppoveToken,

  /// 代币转账
  SentToken,

  /// 跨链交易
  BridgeTransfer,
}

class TransactionConfirmSheetController extends StatefulWidget {
  final Map<String, dynamic> requestData;

  /// 当前用户的主私钥
  final Wallet wallet;

  /// 当前选择的账户
  final WalletKey account;

  /// 当前打开的Dapp信息
  final Dapp dapp;

  /// Web3
  final Web3ClientProvider web3clientProvide;

  /// 手续费估算需要使用的服务
  final ScanAPIService scanAPIService;

  /// 源数据服务器中支持一个估算手续费的方法需要使用
  final DataSourceProvider dataSourceProvider;

  /// 网络配置
  final NetworkConfig networkConfig;

  /// 事件监听
  final StreamController<EventInfo> eventStreamController;

  TransactionConfirmSheetController({
    @required this.requestData,
    @required this.wallet,
    @required this.account,
    @required this.dapp,
    @required this.dataSourceProvider,
    @required this.web3clientProvide,
    @required this.scanAPIService,
    @required this.networkConfig,
    this.eventStreamController,
  });

  /// 判断交易类型
  _TransactionType get transactionType {
    final String data = (requestData['object']['data'] as String).toLowerCase();
    final String value = requestData['object']['value'];
    _TransactionType type = _TransactionType.Sent;

    /// 判断是否是普通转账
    if (data == null) {
      return type;
    }

    /// 以下均为合约调用
    // "39509351": "increaseAllowance(address,uint256)",
    // "a457c2d7": "decreaseAllowance(address,uint256)",
    // "095ea7b3": "approve(address,uint256)",
    // "a9059cbb": "transfer(address,uint256)",
    // "23b872dd": "transferFrom(address,address,uint256)"
    // "346a9074": "burn(bytes32,address,uint256)",

    // approve(address,uint256)
    if (data.startsWith('0x095ea7b3') || data.startsWith('095ea7b3')) {
      type = _TransactionType.AppoveToken;
    }

    // transfer(address,uint256)
    else if (data.startsWith('0xa9059cbb') || data.startsWith('a9059cbb')) {
      type = _TransactionType.SentToken;
    }

    // transferToBridge(address) ||
    // transferToBridge(address,address,uint256) ||
    // burn(bytes32,address,uint256)
    // else if (data.startsWith('0x8a78bc16') ||
    //     data.startsWith('8a78bc16') ||
    //     data.startsWith('0x346a9074') ||
    //     data.startsWith('346a9074') ||
    //     data.startsWith('0xc51d673a') ||
    //     data.startsWith('c51d673a')) {
    //   type = _TransactionType.BridgeTransfer;
    // }

    // payable
    else if (value != null && hexToInt(value) > BigInt.zero) {
      type = _TransactionType.InvokePayable;
    } else {
      type = _TransactionType.Invoke;
    }

    return type;
  }

  static Future<String> show({
    @required BuildContext context,
    @required Map<String, dynamic> requestData,
    @required Wallet wallet,
    @required WalletKey account,
    @required Dapp dapp,
    @required NetworkConfig networkConfig,
    @required DataSourceProvider dataSourceProvider,
    @required Web3ClientProvider web3clientProvider,
    @required ScanAPIService scanAPIService,
    StreamController<EventInfo> eventStreamController,
  }) =>
      showModalBottomSheet(
        context: context,
        enableDrag: false,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (context) => TransactionConfirmSheetController(
          requestData: requestData,
          wallet: wallet,
          account: account,
          dapp: dapp,
          web3clientProvide: web3clientProvider,
          scanAPIService: scanAPIService,
          dataSourceProvider: dataSourceProvider,
          networkConfig: networkConfig,
          eventStreamController: eventStreamController,
        ),
      );

  @override
  State<StatefulWidget> createState() =>
      _TransactionConfirmSheetControllerState();
}

class _TransactionConfirmSheetControllerState
    extends State<TransactionConfirmSheetController> {
  Widget child;

  @override
  void initState() {
    super.initState();
    child = _parseTxAndBuildSheet(context);
  }

  /// 判断当前数据源应该首先展示哪种样式的页面和对应的功能
  Widget _parseTxAndBuildSheet(BuildContext context) {
    switch (widget.requestData['name'] as String) {

      /// 对交易进行签名
      case 'signTransaction':
        switch (widget.transactionType) {
          case _TransactionType.AppoveToken:
            return _TokenAppoveSign(
              requestData: widget.requestData,
              wallet: widget.wallet,
              account: widget.account,
              dapp: widget.dapp,
              web3clientProvider: widget.web3clientProvide,
              scanAPIService: widget.scanAPIService,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
            );
          case _TransactionType.BridgeTransfer:
            return _BridgeSign(
              requestData: widget.requestData,
              wallet: widget.wallet,
              account: widget.account,
              dapp: widget.dapp,
              web3clientProvider: widget.web3clientProvide,
              scanAPIService: widget.scanAPIService,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
              eventStream: widget.eventStreamController,
            );
          default:
            return _TransactionSign(
              requestData: widget.requestData,
              wallet: widget.wallet,
              account: widget.account,
              dapp: widget.dapp,
              web3clientProvider: widget.web3clientProvide,
              scanAPIService: widget.scanAPIService,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
            );
        }
        break;

      /// 以下三种均为签名类型，可以使用相同的页面
      case 'signPersonalMessage':
        return _MessageSign(
          requestData: widget.requestData,
          wallet: widget.wallet,
          account: widget.account,
          dapp: widget.dapp,
          web3clientProvider: widget.web3clientProvide,
          scanAPIService: widget.scanAPIService,
          dataSourceProvider: widget.dataSourceProvider,
          networkConfig: widget.networkConfig,
        );

      case 'signMessage':
      case 'signTypedMessage':
        throw UnimplementedError();
    }

    throw UnimplementedError();
  }

  @override
  Widget build(BuildContext context) => child;
}
