// @dart=2.9

part of 'sheet.dart';

// ignore: must_be_immutable
class _TransactionSign extends _InternalSheet with ChangeNotifier {
  _TransactionSign({
    @required Map<String, dynamic> requestData,
    @required Wallet wallet,
    @required WalletKey account,
    @required Dapp dapp,
    @required Web3ClientProvider web3clientProvider,
    @required NetworkConfig networkConfig,
    @required DataSourceProvider dataSourceProvider,
    @required ScanAPIService scanAPIService,
  }) : super(
          requestData: requestData,
          title: '交易签名',
          wallet: wallet,
          account: account,
          dapp: dapp,
          web3clientProvider: web3clientProvider,
          networkConfig: networkConfig,
          dataSourceProvider: dataSourceProvider,
          scanAPIService: scanAPIService,
        );

  void configDefaultPaymentInfo() {
    txPaymentInfoListenable.value = _TransactionPayment(
      (requestData['object'] as Map).containsKey('value')
          ? Amount(
              value: hexToInt(requestData['object']['value']),
              decimals: 18,
            )
          : Amount(value: BigInt.zero, decimals: 18),
      super._gas,
      Amount(
        value: gasPriceConfig.gasConfig.propose,
        decimals: 9,
      ),
    );
  }

  Widget _content(bool advancedMode) {
    return Container(
      padding: EdgeInsets.only(
        bottom: ScreenUtil().setHeight(20),
        left: ScreenUtil().setWidth(20),
        right: ScreenUtil().setWidth(20),
      ),
      child: ValueListenableBuilder<_TransactionPayment>(
        valueListenable: txPaymentInfoListenable,
        builder: (context, _, child) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// 如果Tx.Value字段不为空则显示 Value+手续费的显示样式，否则直接显示
              if (txPaymentInfoListenable.value.value.value > BigInt.zero)
                _FieldCell(
                  '总金额',
                  txPaymentInfoListenable.value.value.toStringAsFixed(
                    fractionDigits: 6,
                  ),
                  subTitleDesc: txPaymentInfoListenable.value.toFeeString(
                    networkConfig.feeTokenBlog,
                  ),
                )
              else
                _FieldCell(
                  '手续费',
                  txPaymentInfoListenable.value
                      .toFeeString(networkConfig.feeTokenBlog),
                ),

              /// Gas
              if (advancedMode)
                _InputFieldCell(
                  'Gas',
                  txPaymentInfoListenable.value.gas.toString(),
                  symbol: 'Gas',
                  textInputType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                      RegExp(r'^[1-9]+[0-9]*'),
                    ),
                  ],
                  onChanged: (value) {
                    txPaymentInfoListenable.value.gas = BigInt.parse(value);
                    txPaymentInfoListenable.notifyListeners();
                  },
                ),

              /// GasPrice
              if (advancedMode)
                _InputFieldCell(
                  'GasPrice',

                  /// 显示时需要使用GWei单位
                  txPaymentInfoListenable.value.gasPrice.toStringAsFixed(
                    fractionDigits: 2,
                    format: false,
                  ),
                  symbol: 'Gwei',
                  textInputType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                      RegExp(r'^[0-9]+(\.[0-9]{0,2})?'),
                    ),
                  ],
                  onChanged: (value) {
                    var newGasPrice = Amount.fromString(
                      value,
                      decimals: 9,
                      valueDecimals: 9,
                    );

                    /// 不允许手动设置低于最对安全值的GasPrice
                    if (newGasPrice.value < gasPriceConfig.gasConfig.safe) {
                      newGasPrice = Amount(
                        value: gasPriceConfig.gasConfig.safe,
                        decimals: 9,
                      );
                    }

                    txPaymentInfoListenable.value.gasPrice = newGasPrice;
                    txPaymentInfoListenable.notifyListeners();
                  },
                ),

              /// From
              if (advancedMode)
                _FieldCell(
                  '发送地址',
                  requestData['object']['from'],
                  isAddress: true,
                ),

              /// To
              if (advancedMode)
                _FieldCell(
                  '接收地址',
                  requestData['object']['to'],
                  isAddress: true,
                ),

              /// Data
              if (advancedMode &&
                  (requestData['object'] as Map).containsKey('data'))
                _FieldCell(
                  'Data',
                  requestData['object']['data'],
                  isAddress: true,
                ),

              /// GasConfigSlider
              if (!advancedMode)

                /// 如果手动设置的手续费在取值范围内，则正常显示滑动条，否则需要更换样式
                if (txPaymentInfoListenable.value.gasPrice.value >=
                        gasPriceConfig.gasConfig.safe &&
                    txPaymentInfoListenable.value.gasPrice.value <=
                        gasPriceConfig.gasConfig.fast)
                  FeeConfigBoard(
                    defaultValue: (BigInt min, BigInt max, BigInt curr) {
                      return (curr - min).toDouble() / (max - min).toDouble();
                    }(
                        gasPriceConfig.gasConfig.safe,
                        gasPriceConfig.gasConfig.fast,
                        txPaymentInfoListenable.value.gasPrice.value),
                    feeTokenBlog: networkConfig.feeTokenBlog,
                    totalStringCallBack: (v) => txPaymentInfoListenable.value
                        .toFeeString(networkConfig.feeTokenBlog),
                    lableCallBack: (v) {
                      final gasPriceWei = gasPriceConfig.gasConfig.safe +
                          (gasPriceConfig.gasConfig.fast -
                                  gasPriceConfig.gasConfig.safe) *
                              BigInt.from((v * 100).toInt()) ~/
                              BigInt.from(100);

                      final gasPriceAmount = Amount(
                        value: gasPriceWei,
                        decimals: 9,
                      );

                      return '${gasPriceAmount.toStringAsFixed()} Gwei';
                    },
                    onChanged: (v) {
                      final gasPriceWei = gasPriceConfig.gasConfig.safe +
                          (gasPriceConfig.gasConfig.fast -
                                  gasPriceConfig.gasConfig.safe) *
                              BigInt.from((v * 100).toInt()) ~/
                              BigInt.from(100);

                      txPaymentInfoListenable.value.gasPrice = Amount(
                        value: gasPriceWei,
                        decimals: 9,
                      );
                      txPaymentInfoListenable.notifyListeners();
                    },
                  )
                else
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      _FieldCell(
                        '自定义手续费',
                        txPaymentInfoListenable.value.gasPrice.toStringAsFixed(
                              fractionDigits: 2,
                              format: false,
                            ) +
                            ' Gwei',
                      ),
                      // Expanded(child: SizedBox()),
                      SizedBox(
                        height: 30,
                      ),
                      TextButton(
                          child: Text(
                            '使用推荐',
                            style: ThemeUtils().getTextStyle(
                              'views.chaincore.ethereum.dapp.textstyle.default_fee',
                            ),
                          ),
                          onPressed: () {
                            configDefaultPaymentInfo();
                          }),
                    ],
                  )
            ]
              // ..add(SizedBox(height: 10))
              ..add(
                Container(
                  child: StatusButton.withStyle(
                    StatusButtonStyle.Info,
                    title: '下一步',
                    onPressedFuture: () async {
                      super.confirmTransaction(context);
                    },
                  ),
                ),
              )),
      ),
    );
  }

  @override
  Widget body(
    BuildContext context,
    BigInt estimatedGas,
    dynamic error,
  ) {
    if (txPaymentInfoListenable.value == null) configDefaultPaymentInfo();

    return OrientationBuilder(builder: (context, orientation) {
      return MediaQuery.of(context).size.width >
              MediaQuery.of(context).size.height * 1.2
          ? Container(
              height: MediaQuery.of(context).size.height / 1.4,
              child: ListView(
                children: [
                  AnimatedCrossFade(
                    firstChild: _content(true),
                    secondChild: _content(false),
                    crossFadeState: isAdvancedMode
                        ? CrossFadeState.showFirst
                        : CrossFadeState.showSecond,
                    duration: Duration(milliseconds: 250),
                  ),
                ],
              ),
            )
          : AnimatedCrossFade(
              firstChild: _content(true),
              secondChild: _content(false),
              crossFadeState: isAdvancedMode
                  ? CrossFadeState.showFirst
                  : CrossFadeState.showSecond,
              duration: Duration(milliseconds: 250),
            );
    });
  }
}
