// @dart=2.9
part of 'sheet.dart';

typedef VerifyResultCallback = String Function(
    BuildContext context, String pwd);

class VerifySheet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _VerifySheetState();

  final VerifyResultCallback verifyCallback;

  final Widget leading;

  final bool internalMode;

  static Future<T> show<T>(
    BuildContext context, {
    @required VerifyResultCallback verifyCallback,
    Widget leading,
  }) =>
      showModalBottomSheet<T>(
        context: context,
        enableDrag: false,
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: Colors.transparent,
        builder: (context) => VerifySheet(
          verifyCallback: verifyCallback,
          leading: leading,
        ),
      );

  VerifySheet({
    Key key,
    @required this.verifyCallback,
    this.leading,
    this.internalMode = false,
  }) : super(key: key);
}

class _VerifySheetState extends State<VerifySheet> with WidgetsBindingObserver {
  bool inVerifing = true;
  bool ob = false;
  TextEditingController textEditingController;
  StatusButtonController commitButtonController;
  ValueNotifier<String> textEditingErrorListenable =
      ValueNotifier<String>(null);

  @override
  void initState() {
    super.initState();
    // this.ob = ConfigStorageUtils()
    // .readDisplayPasswordIndexOfKey(ConfigKey.DisplayPassword);
    textEditingController = TextEditingController();
    commitButtonController = StatusButtonController(
      title: S.current.sheet_verify_done,
      busyTitle: S.current.sheet_verify_verifing,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            child: Container(
              color: ThemeUtils().getColor('sheet.background'),
              padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(40)),
              child: Column(
                children: [
                  if (!widget.internalMode)
                    NavigationBar(
                      isBottomSheet: true,
                      title: S.current.sheet_verify_title,
                      leading: inVerifing ? Container() : widget.leading,
                    ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(30),
                    ),
                    child: Icon(
                      const IconData(
                        0xe686,
                        fontFamily: 'myIcon',
                        fontPackage: "wallet_flutter",
                      ),
                      size: ScreenUtil().setHeight(50),
                    ),
                    // Icon(
                    //   Icons.lock,
                    //   size: ScreenUtil().setHeight(50),
                    //   color: ThemeUtils().getColor('sheet.icon'),
                    // ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(20),
                      left: ScreenUtil().setWidth(20),
                      right: ScreenUtil().setWidth(20),
                    ),
                    child: ValueListenableBuilder(
                      valueListenable: textEditingErrorListenable,
                      builder: (context, value, child) => TextFormField(
                        controller: textEditingController,
                        obscureText: true,
                        autofocus: false,
                        onChanged: (value) {
                          if (textEditingErrorListenable.value != null) {
                            textEditingErrorListenable.value = null;
                          }
                        },
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.security),
                          suffixIcon: IconButton(
                            color: ThemeUtils()
                                .getColor('utils.input.border_color'),
                            icon: Icon(Icons.clear),
                            onPressed: () {
                              textEditingController.text = '';
                              textEditingErrorListenable.value = null;
                            },
                          ),
                          fillColor: ThemeUtils().getColor(
                            'utils.input.fill_color',
                          ),
                          focusColor: ThemeUtils().getColor(
                            'utils.input.fill_color',
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(5))),
                            borderSide: BorderSide(
                                color: ThemeUtils().getColor(
                              'utils.input.border_color',
                            )),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(5))),
                            borderSide: BorderSide(
                              color: ThemeUtils().getColor(
                                'utils.input.border_color',
                              ),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(5))),
                            borderSide: BorderSide(
                              color: ThemeUtils().getColor(
                                'utils.input.border_color',
                              ),
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(ScreenUtil().setWidth(5))),
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          hintText: S.current.sheet_verify_input_placeholder,
                          errorText: textEditingErrorListenable.value,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: ScreenUtil().setWidth(160),
                          height: ScreenUtil().setHeight(48),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.blue,
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(20),
                          ),
                          child: Center(
                            child: Text(
                              S.current.sheet_cancel,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(18),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: ScreenUtil().setHeight(48),
                        width: ScreenUtil().setWidth(160),
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(20),
                          left: ScreenUtil().setWidth(20),
                        ),
                        child: StatusButton.withController(
                          commitButtonController,
                          onPressedFuture: () async {
                            SystemChannels.textInput
                                .invokeMethod('TextInput.hide');

                            await Future.delayed(Duration(milliseconds: 1000));

                            textEditingErrorListenable.value =
                                widget.verifyCallback(
                              context,
                              textEditingController.text,
                            );

                            if (textEditingErrorListenable.value != null) {
                              textEditingController.text = '';
                            }

                            return Future.value(null);
                          },
                        ),
                      ),
                    ],
                  ),
                  AnimatedPadding(
                    padding: MediaQuery.of(context).viewInsets,
                    duration: Duration(milliseconds: 250),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
}
