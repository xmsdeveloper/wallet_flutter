// @dart=2.9
import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/views/begin/welcome.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/setting/graphics/graphics_verify.dart';

class SplashView extends StatelessWidget {
  Widget _body(BuildContext context) => Engine.splashBuilder != null
      ? Engine.splashBuilder(context)
      : Scaffold(
          backgroundColor: Colors.white,
          body: Center(
            child: Image(
              width: ScreenUtil().setWidth(100),
              height: ScreenUtil().setWidth(125),
              image: const AssetImage(
                'assets/imgs/app_icon.png',
                package: "wallet_flutter",
              ),
            ),
          ),
        );

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width,
        maxHeight: MediaQuery.of(context).size.height,
      ),
      designSize: Size(375, 812),
      allowFontScaling: false,
    );

    return FutureBuilder(
      future: ThemeUtils.init(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            Future.delayed(
              Duration(milliseconds: 500),
              () async {
                if (CoreProvider.of(context).wallet != null) {
                  Navigator.pushAndRemoveUntil(
                    context,
                    ViewAnimations.viewFadeIn(
                      Engine(),
                      routeName: '/index',
                    ),
                    (route) => route == null,
                  );
                } else {
                  Navigator.pushReplacement(
                    context,
                    ViewAnimations.viewFadeIn(
                      WelcomeView(),
                    ),
                  );
                }
              },
            );
            return _body(context);

          default:
            return _body(context);
        }
      },
    );
  }
}
