// @dart=2.9
import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/keychain/generate.dart';
import 'package:wallet_flutter/views/keychain/importer.dart';

class WelcomeView extends StatelessWidget {
  final List<Map<String, dynamic>> list = [
    {
      'icon': Icons.account_balance_wallet,
      'actionName': S.current.welcome_create,
      'des': S.current.welcome_create_desc,
    },
    {
      'icon': Icons.login,
      'actionName': S.current.welcome_import,
      'des': S.current.welcome_import_desc,
    },
  ];

  // final List bootpagedatas = Engine.welcomeBuilder;
  final List bootpagedata = [
    {
      "image": "assets/imgs/guidance_chart_first.png",
      "title": S.current.ethereum_welcome_firsttitle,
      "subtitle": S.current.ethereum_welcome_firstsubtitle,
    },
    {
      "image": "assets/imgs/guidance_chart_second.png",
      "title": S.current.ethereum_welcome_secondtitle,
      "subtitle": S.current.ethereum_welcome_secondsubtitle,
    },
    {
      "image": "assets/imgs/guidance_chart_third.png",
      "title": S.current.ethereum_welcome_thirdtitle,
      "subtitle": S.current.ethereum_welcome_thirdsubtitle,
    }
  ];

  Widget _actionWidgetWithIdx(BuildContext context, int idx) => GestureDetector(
        onTap: () {
          if (idx == 0) {
            Navigator.of(context)
                .push(ViewAnimations.viewFadeIn(GenerateWalletView()));
          } else if (idx == 1) {
            Navigator.push(
              context,
              ViewAnimations.viewFadeIn(
                ImportWalletView(
                  importTypes: [
                    ImportType.Mnemonic,
                  ],
                ),
              ),
            );
          }
        },
        child: Container(
          width: ScreenUtil().setWidth(158),
          height: ScreenUtil().setHeight(53),
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(10),
            right: ScreenUtil().setWidth(10),
          ),
          padding: EdgeInsets.only(
            top: ScreenUtil().setWidth(10),
            bottom: ScreenUtil().setWidth(10),
          ),
          decoration: idx == 0
              ? BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Colors.blue,
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(
                    ScreenUtil().setWidth(50),
                  ),
                )
              : BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color.fromRGBO(119, 45, 230, 1),
                      Color.fromRGBO(101, 170, 245, 1),
                      // Colors.pinkAccent,
                      // Colors.white,
                    ],
                  ),
                  borderRadius: BorderRadius.circular(
                    ScreenUtil().setWidth(50),
                  ),
                ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(5)),
                child: Icon(
                  list[idx]['icon'] as IconData,
                  size: ScreenUtil().setWidth(20),
                  color: idx == 0 ? Colors.blue : Colors.white,
                ),
              ),
              Text(
                list[idx]['actionName'],
                style: TextStyle(
                  color: idx == 0 ? Colors.blue : Colors.white,
                ),
              ),
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Engine.welcomeBuilder == null
        ? Scaffold(
            // backgroundColor: Colors.white,

            backgroundColor: ThemeUtils().getColor(
              'views.begin.welcome.row_background',
            ),

            body: Container(
              child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: new Container(
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(150),
                              bottom: ScreenUtil().setHeight(48),
                            ),
                            child: Swiper(
                              itemCount: 3,
                              itemBuilder: (context, index) {
                                return Column(
                                  children: [
                                    Container(
                                      width: ScreenUtil().setWidth(335),
                                      height: ScreenUtil().setHeight(257),
                                      child: Image.asset(
                                        this.bootpagedata[index]['image'],
                                        package: 'wallet_flutter',
                                        // fit: BoxFit.fill,
                                      ),
                                    ),
                                    SizedBox(height: ScreenUtil().setWidth(43)),
                                    Container(
                                      child: Text(
                                        this.bootpagedata[index]['title'],
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: ScreenUtil().setSp(20),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: ScreenUtil().setWidth(160),
                                      child: Text(
                                        this.bootpagedata[index]['subtitle'],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                              pagination: new SwiperPagination(
                                builder: SwiperCustomPagination(
                                  builder: (context, config) {
                                    return CustomP(config.activeIndex);
                                  },
                                ),
                              ),
                            )),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: ScreenUtil().setHeight(26),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: list
                              .map(
                                (e) => Container(
                                  child: Column(
                                    children: <Widget>[
                                      _actionWidgetWithIdx(
                                          context, list.indexOf(e)),
                                    ],
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        : Engine.welcomeBuilder(context);
  }
}

//指示器
// ignore: must_be_immutable
class CustomP extends StatelessWidget {
  var _currentIndex;
  CustomP(this._currentIndex);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(80),
      child: GridView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 5 / 1,
          crossAxisSpacing: ScreenUtil().setWidth(4),
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            decoration: BoxDecoration(
              color: _currentIndex == index
                  ? Theme.of(context).primaryColor
                  : Color.fromRGBO(105, 140, 241, 0.3),

              // borderRadius: BorderRadius.circular(4),
              borderRadius: BorderRadius.circular(180),
            ),
          );
        },
        itemCount: 3,
      ),
    );
  }
}
