// @dart=2.9
library qytechnology.package.wallet.views.chaincore.ethereum;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:provider/provider.dart';
import 'package:wallet_flutter/command/command.dart';

import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

export 'package:wallet_flutter/views/chaincore/ethereum/asset.dart';
export 'package:wallet_flutter/views/chaincore/ethereum/dapps.dart';
export 'package:wallet_flutter/views/chaincore/ethereum/setting.dart';
export 'package:wallet_flutter/views/chaincore/bridge/bridge.dart';
import 'package:vibrate/vibrate.dart';

class IndexTabView extends StatefulWidget {
  final Key key;

  IndexTabView({this.key});

  @override
  State<StatefulWidget> createState() => _IndexTabViewState();
}

typedef ChildrenBuilder = Widget Function(
    BuildContext context, List<Widget> children);

class _IndexTabViewState extends State<IndexTabView> {
  PageController pageController = PageController();
  ValueNotifier<int> currentIndexListenable = ValueNotifier<int>(0);

  @override
  void initState() {
    super.initState();
    // ModuleRegistry().createModules(
    //   context,
    //   CoreProvider.of(context).networkConfig.modules,
    // );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget listenablesBuilder({
    @required BuildContext context,
    @required ModuleGroupBuilder builder,
  }) =>
      ValueListenableBuilder<Wallet>(
        valueListenable: CoreProvider.of(context).walletListenable,
        builder: (context, wallet, child) => MultiProvider(
          providers: [
            ListenableProvider<DataSourceProvider>(
              create: (context) => DataSourceProvider(
                networkIdentifier:
                    CoreProvider.of(context).networkConfig.identifier,
                walletIdentifier: wallet.identifier,
              ),
            ),
          ],
          builder: (context, child) => ValueListenableBuilder<EndPoint>(
            valueListenable: CoreProvider.of(context).endPointListenable,
            builder: (context, endpoint, child) {
              DataSourceProvider.of(context).remoteServices.setEndpoint(
                    endpoint,
                  );

              /// 配置DataSourceProvider实例中的endpoint

              return MultiProvider(
                providers: [
                  ListenableProvider<Web3ClientProvider>(
                    create: (context) => Web3ClientProvider.fromHttp(
                      url: endpoint.nodeURL,
                    ),
                  ),
                ],
                builder: (context, child) => builder(
                  context,
                  Engine.createModules(
                    context,
                    CoreProvider.of(context).networkConfig.modules,
                  ),
                ),
              );
            },
          ),
        ),
      );

  @override
  Widget build(BuildContext context) => listenablesBuilder(
        context: context,
        builder: (context, modules) => ValueListenableBuilder(
          valueListenable: currentIndexListenable,
          builder: (context, index, child) => Scaffold(
            bottomNavigationBar: BottomNavigationBar(
              selectedFontSize: ScreenUtil().setSp(12),
              unselectedFontSize: ScreenUtil().setSp(12),
              backgroundColor:
                  ThemeUtils().getColor('utils.bottom_bar.background'),
              fixedColor:
                  //     ThemeUtils().getColor('utils.bottom_bar.selected_item_color'),
                  // Color.fromRGBO(54, 144, 196, 1),
                  Colors.blue,
              items: modules.map((e) => e.barItem).toList(),
              currentIndex: currentIndexListenable.value,
              type: BottomNavigationBarType.fixed,
              onTap: (index) {
                // Vibrate.feedback(FeedbackType.success);
                Vibrate.feedback(FeedbackType.impact);
                if (currentIndexListenable.value != index) {
                  currentIndexListenable.value = index;
                }
              },
            ),
            body: IndexedStack(
              children: modules.map((e) => e.body).toList(),
              index: currentIndexListenable.value,
            ),
          ),
        ),
      );
}
