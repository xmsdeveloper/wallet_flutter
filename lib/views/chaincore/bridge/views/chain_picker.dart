// @dart=2.9

part of '../bridge.dart';

// 目标链选择底部弹出框
void _showTargetChainsPickerView(
  BuildContext context,
  List<NetworkConfig> targetChainsPair,
  ValueNotifier<NetworkConfig> listenable,
) =>
    BottomPicker.show(
      context,
      title: '请选择网络',
      children: targetChainsPair
          .map(
            (config) => InkWell(
              onTap: () {
                listenable.value = config;
                Navigator.of(context).pop();
              },
              child: Container(
                padding: EdgeInsets.fromLTRB(
                  ScreenUtil().setWidth(20), // L
                  ScreenUtil().setWidth(10), // T
                  ScreenUtil().setWidth(20), // R
                  ScreenUtil().setWidth(10), // B
                ),
                child: Row(
                  children: [
                    Container(
                      width: ScreenUtil().setWidth(40),
                      height: ScreenUtil().setWidth(40),
                      decoration: BoxDecoration(
                        color: ThemeUtils().getColor(
                          'utils.view_controller.background',
                        ),
                        borderRadius: BorderRadius.circular(
                          ScreenUtil().setWidth(20),
                        ),
                        // border: Border.all(
                        //   color: ThemeUtils().getColor(
                        //     'views.bridge.combox_drop_icon',
                        //   ),
                        //   width: 1.0,
                        // ),
                      ),
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: CachedImage.assetNetwork(
                          url: config.logoURL,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(10),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          config.chainName,
                          style: ThemeUtils().getTextStyle(
                            'views.bridge.textstyle.picker_item_title',
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(3),
                        ),
                        Text(
                          config.markName,
                          style: ThemeUtils().getTextStyle(
                            'views.bridge.textstyle.picker_item_desc',
                          ),
                        ),
                      ],
                    ),
                    Expanded(child: SizedBox()),
                    if (config == listenable.value)
                      Icon(
                        Icons.check,
                        color: ThemeUtils().getColor(
                          'views.bridge.picker_check',
                        ),
                      ),
                  ],
                ),
              ),
            ),
          )
          .toList(),
    );
