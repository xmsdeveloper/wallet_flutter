// @dart=2.9
part of '../bridge.dart';

enum ProgressStage {
  Begin,
  WaitingLock,
  WaitingProposal,
  WaitingSignature,
  WaitingMinted,
  Success,
}

Map<ProgressStage, String> _progressStageStrings() => {
      ProgressStage.Begin: '开始交易',
      ProgressStage.WaitingLock: '锁定资产',
      ProgressStage.WaitingProposal: '申请凭证',
      ProgressStage.WaitingSignature: '验证交易',
      ProgressStage.WaitingMinted: '正在铸币',
      ProgressStage.Success: '交易完成',
    };

class _StageIcon extends StatelessWidget {
  final ProgressStage stage;
  final bool isBuzy;
  final bool isDone;
  final double size;

  const _StageIcon({
    @required this.stage,
    this.isBuzy = false,
    this.isDone = false,
    this.size = 15,
  });

  @override
  Widget build(BuildContext context) => isBuzy
      ? BallSpinFadeLoaderIndicator(
          minBallRadius: ScreenUtil().setHeight(1),
          maxBallRadius: ScreenUtil().setHeight(2),
          radius: ScreenUtil().setHeight(10),
          ballColor: ThemeUtils().getColor(
            'views.chaincore.ethereum.assets_detail.tab_bar_indicator',
          ),
        )
      : Container(
          width: 15,
          height: 15,
          decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.all(Radius.circular(15 / 2)),
          ),
        );
}

class _ProgressBar extends StatefulWidget {
  final List<ProgressStage> stages;
  final EdgeInsetsGeometry padding;
  final ValueNotifier<ProgressStage> stageLisentable;

  _ProgressBar({
    @required this.stages,
    @required this.stageLisentable,
    this.padding,
  });

  @override
  State<StatefulWidget> createState() => _ProgressBarState();
}

class _ProgressBarState extends State<_ProgressBar>
    with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );

    animation = CurvedAnimation(
      parent: animationController,
      curve: ElasticOutCurve(),
    );

    ///动画开始
    animationController.repeat(reverse: true);
  }

  @override
  dispose() {
    //路由销毁时需要释放动画资源
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: widget.stageLisentable,
        builder: (context, currStage, child) {
          final List<Widget> children = [];
          print(currStage.toString());
          widget.stages.forEach((stage) {
            children.add(
              _StageIcon(
                stage: stage,
                isBuzy: stage == currStage,
                isDone: stage.index < currStage.index,
              ),
            );

            if (widget.stages.last != stage) {
              children.add(
                Expanded(
                  child: Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                ),
              );
            }
          });

          return Container(
            // color: Colors.red,
            padding: widget.padding,
            child: Column(
              children: [
                FadeTransition(
                  opacity: animationController,
                  child: Icon(
                    Icons.account_balance_wallet,
                    size: ScreenUtil().setHeight(50),
                  ),
                ),
                Text(
                  _progressStageStrings()[currStage],
                ),
                SizedBox(height: ScreenUtil().setHeight(20)),
                Row(children: children),
              ],
            ),
          );
        },
      );
}
