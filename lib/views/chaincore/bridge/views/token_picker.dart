// @dart=2.9
part of '../bridge.dart';

// 跨链资产选择底部弹出框
void showBridgeTokensPickerView(
  BuildContext context,
  List<TokenBlog> bridgeTokens,
  ValueNotifier<TokenBlog> listenable,
) =>
    BottomPicker.show(
      context,
      title: '请选择桥上资产',
      children: bridgeTokens
          .map(
            (token) => InkWell(
              onTap: () {
                listenable.value = token;
                Navigator.of(context).pop();
              },
              child: Container(
                padding: EdgeInsets.fromLTRB(
                  ScreenUtil().setWidth(20), // L
                  ScreenUtil().setWidth(10), // T
                  ScreenUtil().setWidth(20), // R
                  ScreenUtil().setWidth(10), // B
                ),
                child: Row(
                  children: [
                    Container(
                      width: ScreenUtil().setWidth(40),
                      height: ScreenUtil().setWidth(40),
                      decoration: BoxDecoration(
                        color: ThemeUtils().getColor(
                          'utils.view_controller.background',
                        ),
                        borderRadius: BorderRadius.circular(
                          ScreenUtil().setWidth(20),
                        ),
                        // border: Border.all(
                        //   color: ThemeUtils().getColor(
                        //     'views.bridge.combox_drop_icon',
                        //   ),
                        //   width: 1.0,
                        // ),
                      ),
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: CachedImage.assetNetwork(
                          url: token.logo,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(10),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          token.symbol,
                          style: ThemeUtils().getTextStyle(
                            'views.bridge.textstyle.picker_item_title',
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(3),
                        ),
                        Text(
                          token.name,
                          style: ThemeUtils().getTextStyle(
                            'views.bridge.textstyle.picker_item_desc',
                          ),
                        ),
                      ],
                    ),
                    Expanded(child: SizedBox()),
                    if (token == listenable.value)
                      Icon(
                        Icons.check,
                        color: ThemeUtils().getColor(
                          'views.bridge.picker_check',
                        ),
                      ),
                  ],
                ),
              ),
            ),
          )
          .toList(),
    );
