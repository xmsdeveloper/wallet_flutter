// @dart=2.9
part of '../bridge.dart';

class _ComBox extends StatelessWidget {
  final String title;
  final Widget icon;
  final String text;
  final IconData suffixIconData;
  final VoidCallback onPressed;
  final double height;

  _ComBox({
    @required this.title,
    @required this.icon,
    @required this.text,
    this.height,
    this.suffixIconData,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            this.title,
            style: ThemeUtils().getTextStyle(
              'views.bridge.textstyle.combox_title',
            ),
            textAlign: TextAlign.start,
          ),
          SizedBox(
            height: ScreenUtil().setHeight(8),
          ),
          Container(
            height: this.height ?? ScreenUtil().setHeight(50),
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateColor.resolveWith(
                  (states) => ThemeUtils().getColor(
                    'views.bridge.combox_background',
                  ),
                ),
                padding: MaterialStateProperty.resolveWith(
                  (states) => EdgeInsets.all(
                    ScreenUtil().setWidth(5),
                  ),
                ),
                elevation: MaterialStateProperty.resolveWith(
                  (states) => 1,
                ),
                shape: MaterialStateProperty.resolveWith(
                  (states) => RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      ScreenUtil().setWidth(10),
                    ),
                  ),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: ScreenUtil().setWidth(20),
                    width: ScreenUtil().setWidth(20),
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(10),
                      right: ScreenUtil().setWidth(10),
                    ),
                    child: this.icon,
                  ),
                  Text(
                    this.text,
                    style: ThemeUtils().getTextStyle(
                      'views.bridge.textstyle.combox_text',
                    ),
                  ),
                  if (this.onPressed != null) Expanded(child: SizedBox()),
                  if (this.onPressed != null)
                    Icon(
                      this.suffixIconData ?? Icons.arrow_drop_down,
                      color: ThemeUtils().getColor(
                        'views.bridge.combox_drop_icon',
                      ),
                    ),
                ],
              ),
              onPressed: this.onPressed,
            ),
          ),
        ],
      );
}
