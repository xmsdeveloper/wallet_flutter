// @dart=2.9
part of '../bridge.dart';

enum TransferToBridgeState {
  // 目标资产锁定成功
  OnLockedSuccess,
  // 锁定资产交易提案成功
  OnProposalSuccess,
  // 等待提案超时
  OnProposalTimeoutError,
  // 锁定资产交易验证成功
  OnVerifySuccess,
  OnVerifyTimeoutError,
  // 跨入交易已完成
  OnMintSuccess,
  OnMintTimeoutError,
}

const BTX_FieldCode_None = 0x0;
const BTX_FieldCode_TxHash = 0x01;
const BTX_FieldCode_From = 0x02;
const BTX_FieldCode_To = 0x04;
const BTX_FieldCode_Value = 0x08;
const BTX_FieldCode_Status = 0x10;
const BTX_FieldCode_BlockNumber = 0x20;
const BTX_FieldCode_Input = 0x40;
const BTX_FieldCode_All = 0xFF;

BigInt btxFieldsEncode(int code) => BigInt.from(code);
