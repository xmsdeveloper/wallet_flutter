// @dart=2.9
part of '../bridge.dart';

const String DefaultCallFrom = '0x0000000000000000000000000000000000000000';

class BridgeProxy {
  final getExistenceTransactionABI = ContractFunction(
    'getExistenceTransaction',
    [
      // chainIdentifier
      FunctionParameter(null, FixedBytes(32)),
      // chainTxHash
      FunctionParameter(null, FixedBytes(32)),
      // fieldCode
      FunctionParameter(null, UintType(length: 8)),
    ],
    outputs: [
      // txFieldDatas
      FunctionParameter(null, DynamicBytes()),
      // proposalState
      FunctionParameter(null, UintType()),
      // reliability
      FunctionParameter(null, UintType()),
    ],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  final String bridgeContractAddress;
  final Web3ClientProvider web3;

  BridgeProxy({
    @required this.web3,
    this.bridgeContractAddress = '0x000000000000000000000000000000000000000E',
  });

  // 获取外链交易可信度
  Future<int> transactionReliability({
    @required String chainIdentifier,
    @required String txHash,
  }) async {
    final data = getExistenceTransactionABI.encodeCall([
      hexToBytes(chainIdentifier),
      hexToBytes(txHash),
      btxFieldsEncode(BTX_FieldCode_TxHash),
    ]);

    try {
      final response = await web3.call(
        from: DefaultCallFrom,
        to: bridgeContractAddress,
        data: data,
        atBlock: BlockNum.pending(),
      );

      final values = getExistenceTransactionABI.decodeReturnValues(response);

      if (bytesToHex(values[0], include0x: true).toLowerCase() ==
          txHash.toLowerCase()) {
        return (values[2] as BigInt).toInt();
      }
      return 0;
    } catch (e) {
      return 0;
    }
  }

  // 获取外链交易是否提案
  Future<bool> isExistence({
    @required String chainIdentifier,
    @required String txHash,
  }) async {
    final data = getExistenceTransactionABI.encodeCall([
      hexToBytes(chainIdentifier),
      hexToBytes(txHash),
      btxFieldsEncode(BTX_FieldCode_TxHash),
    ]);

    try {
      final response = await web3.call(
        from: DefaultCallFrom,
        to: bridgeContractAddress,
        data: data,
        atBlock: BlockNum.pending(),
      );

      final values = getExistenceTransactionABI.decodeReturnValues(response);

      if (bytesToHex(values[0], include0x: true).toLowerCase() ==
          txHash.toLowerCase()) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }
}
