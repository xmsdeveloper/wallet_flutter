// @dart=2.9
part of '../bridge.dart';

class _BridgePair {
  // 桥对应的外链标识
  String chainIdentifier;
  // 外链的代币合约地址
  String outSideChainTokenAddress;
  // 外链的跨链桥合约地址
  String outSideBridgeContracts;
  // 内链对应的锚定币地址
  String inSideAnchorTokenAddress;

  String toString() =>
      '(chainIdentifier:$chainIdentifier, outSideChainTokenAddress:$outSideChainTokenAddress, outSideBridgeContracts:$outSideBridgeContracts, inSideAnchorTokenAddress:$inSideAnchorTokenAddress)';

  // 获取当前网络的配置
  NetworkConfig get config =>
      NetworkStorageUtils().networkConfigOf(chainIdentifier);
}

class AnchorTokensContract {
  // 跨链桥服务运行的主链对应的NetworkConfig标识
  final String hostNetworkIdentifier;
  final String contractAddress;
  final String callFromAddress = '0x0000000000000000000000000000000000000000';

  NetworkConfig hostNetWorkConfig;

  Web3ClientProvider _web3;
  DataSourceProvider _dataSourceProvider;
  BridgeProxy _bridgeProxy;

  Completer<List<_BridgePair>> _pairRequestCompleter = new Completer();

  // 获取跨链桥上配置的基本参数
  Future<List<_BridgePair>> _bridgePairs() async {
    final fun = ContractFunction(
      'anchorTokenList',
      [],
      outputs: [
        FunctionParameter(null, DynamicLengthArray(type: FixedBytes(32))),
        FunctionParameter(null, DynamicLengthArray(type: AddressType())),
        FunctionParameter(null, DynamicLengthArray(type: AddressType())),
        FunctionParameter(null, DynamicLengthArray(type: AddressType())),
      ],
      type: ContractFunctionType.function,
      mutability: StateMutability.nonPayable,
    );

    final response = await _web3.call(
      from: callFromAddress,
      to: contractAddress,
      data: fun.encodeCall(),
    );

    final values = fun.decodeReturnValues(response);

    final List<Uint8List> chainIdentifiers = values[0];
    final List<String> outSideChainTokenAddress = values[1];
    final List<String> outSideBridgeContracts = values[2];
    final List<String> inSideAnchorTokenAddress = values[3];

    List<_BridgePair> ret = [];

    for (int i = 0; i < chainIdentifiers.length; i++) {
      ret.add(
        _BridgePair()
          ..chainIdentifier = bytesToHex(
            chainIdentifiers[i],
            include0x: true,
          )
          ..outSideChainTokenAddress = '0x' + outSideChainTokenAddress[i]
          ..outSideBridgeContracts = '0x' + outSideBridgeContracts[i]
          ..inSideAnchorTokenAddress = '0x' + inSideAnchorTokenAddress[i],
      );
    }

    return ret;
  }

  AnchorTokensContract({
    this.hostNetworkIdentifier =
        '0x0000000000000000000000000000000000000000000000000000000000000000',
    this.contractAddress = '0x0000000000000000000000000000000000000010',
  }) {
    hostNetWorkConfig =
        NetworkStorageUtils().networkConfigOf(this.hostNetworkIdentifier);

    _web3 = Web3ClientProvider.fromHttp(
      url: hostNetWorkConfig.currenEndPoint.nodeURL,
    );

    _dataSourceProvider = DataSourceProvider(
      networkIdentifier: this.hostNetworkIdentifier,
      walletIdentifier: CoreProvider().wallet.identifier,
    );

    _dataSourceProvider.remoteServices.setEndpoint(
      hostNetWorkConfig.currenEndPoint,
    );

    _bridgeProxy = BridgeProxy(web3: _web3);

    _bridgePairs().then((value) => _pairRequestCompleter.complete(value));
  }

  Future<bool> isMintedOf({
    @required String chainIdentifier,
    @required String txHash,
  }) async {
    final fun = ContractFunction(
      'isMintedOf',
      [
        FunctionParameter(null, FixedBytes(32)),
        FunctionParameter(null, FixedBytes(32)),
      ],
      outputs: [
        FunctionParameter(null, BoolType()),
      ],
      type: ContractFunctionType.function,
      mutability: StateMutability.view,
    );

    final response = await _web3.call(
      from: callFromAddress,
      to: contractAddress,
      data: fun.encodeCall(
        [
          hexToBytes(chainIdentifier),
          hexToBytes(txHash),
        ],
      ),
    );

    return fun.decodeReturnValues(response)[0] as bool;
  }

  // 获取支持跨链的资产列表,
  // 可以跨链的资产一定在钱包对应的发行的主链中有对应的锚定资产，
  // 所以只需要在本链的DataSourceServer中获取对应的信息即可。
  // 跨链桥的操作一定会选中某一个条链，所有选择目标方向，
  // inChainIdentifier则是用于筛选目标链
  Future<List<TokenBlog>> bridgeTokens(
    String fromChainIdentifier,
    DataSourceProvider fromChainDataSourceProvider,
  ) async {
    final bridgePairs = await _pairRequestCompleter.future;

    final result = <TokenBlog>[];

    for (int i = 0; i < bridgePairs.length; i++) {
      try {
        // 防止重复
        if (result.indexWhere(
              (blog) =>
                  blog.address.toLowerCase() ==
                  (fromChainIdentifier.toLowerCase() !=
                          hostNetworkIdentifier.toLowerCase()
                      ? bridgePairs[i].outSideChainTokenAddress.toLowerCase()
                      : bridgePairs[i].inSideAnchorTokenAddress.toLowerCase()),
            ) >=
            0) {
          continue;
        }

        final details = await fromChainDataSourceProvider.remoteServices.token
            .requestDetail(
          address: fromChainIdentifier != hostNetworkIdentifier
              ? bridgePairs[i].outSideChainTokenAddress
              : bridgePairs[i].inSideAnchorTokenAddress,
        );

        result.add(details.data.first.blog());
      } catch (e) {
        // 未查询到对应的代币
        print(e);
      }
    }

    return result;
  }

  // 获取对应代币在对应链上的锚定合约
  Future<String> bridgeLockerContract(
    TokenBlog tokenBlog,
    String fromChainIdentifier,
  ) async {
    final bridgePairs = await _pairRequestCompleter.future;

    return bridgePairs
        .firstWhere(
          (pair) =>
              pair.outSideChainTokenAddress.toLowerCase() ==
                  tokenBlog.address.toLowerCase() &&
              pair.chainIdentifier.toLowerCase() ==
                  fromChainIdentifier.toLowerCase(),
        )
        .outSideBridgeContracts;
  }

  // 根据当前选择的跨链资产和From链方向获取，可以选择的目标链配置
  Future<List<NetworkConfig>> bridgeTargetNetworks(
    TokenBlog tokenBlog,
    String fromChainIdentifier,
  ) async {
    final bridgePairs = await _pairRequestCompleter.future;

    // 当前选择的是BridgeChain，那么tokenBlog本就指向了一个锚定币
    if (fromChainIdentifier.toLowerCase() ==
        hostNetworkIdentifier.toLowerCase()) {
      return bridgePairs
          .where(
            (pair) =>
                pair.inSideAnchorTokenAddress.toLowerCase() ==
                    tokenBlog.address.toLowerCase() &&

                // 本地必须有对应链的配置,本地若没有配置，则不支持交易
                NetworkStorageUtils().chainNames.contains(pair.chainIdentifier),
          )
          .toList()
          .map((e) => NetworkStorageUtils().networkConfigOf(e.chainIdentifier))
          .toList();
    } else {
      // 对应可以选择的链的判断应该是桥上资产的虚拟链接的数量
      // 1.获取当前选中资产对应的BridgeChain中的锚定币地址
      final hrc22Token = bridgePairs
          .firstWhere(
            (pair) =>
                pair.chainIdentifier.toLowerCase() ==
                    fromChainIdentifier.toLowerCase() &&
                pair.outSideChainTokenAddress.toLowerCase() ==
                    tokenBlog.address.toLowerCase(),
          )
          .inSideAnchorTokenAddress;

      // 2.锚定币指向同一个地址则说明在支撑的列表内
      return bridgePairs
          .where(
            (pair) =>
                // 剔除当前选中的来源链
                pair.chainIdentifier != fromChainIdentifier &&

                // 对应的BridgeChain的锚定币相同
                pair.inSideAnchorTokenAddress.toLowerCase() ==
                    hrc22Token.toLowerCase() &&

                // 本地必须有对应链的配置,本地若没有配置，则不支持交易
                NetworkStorageUtils().chainNames.contains(pair.chainIdentifier),
          )
          .toList()
          .map((e) => NetworkStorageUtils().networkConfigOf(e.chainIdentifier))
          .toList()
            // 加入BridgeChain配置
            ..insert(
                0,
                NetworkStorageUtils().networkConfigOf(
                  hostNetworkIdentifier,
                ));
    }
  }

  // 组建跨出交易
  Future<Map<String, dynamic>> buildBurnFromBridge({
    @required hrc22TokenContract,
    @required String toChainIdentifier,
    @required String receiptorAddress,
    @required Amount amount,
    @required String fromAddress,
    @required TokenBlog bridgeToken,
    @required NetworkConfig targetChainConfig,
  }) async {
    final tx = {
      'from': fromAddress,
      'to': hrc22TokenContract,
    };

    ContractFunction fun = ContractFunction(
      'burn',
      [
        FunctionParameter('toChainIdentifier', FixedBytes(32)),
        FunctionParameter('recipient', AddressType()),
        FunctionParameter('amount', UintType()),
      ],
      outputs: [
        FunctionParameter('success', BoolType()),
      ],
      mutability: StateMutability.nonPayable,
    );

    tx['data'] = bytesToHex(
      fun.encodeCall([
        hexToBytes(toChainIdentifier),
        receiptorAddress,
        amount.value,
      ]),
      include0x: true,
    );

    // return {
    //   'name': 'signTransaction',
    //   'object': tx,
    // };

    return {
      'name': 'signTransaction',
      'object': tx,
      'amount': amount,
      'fee': Amount(
        value: amount.value ~/ BigInt.from(1000),
        decimals: amount.decimals,
      ),
      'token': bridgeToken,
      // 'statebuilder': progressBarListenable,
      'target_network': targetChainConfig,
      'receiptor': receiptorAddress,
    };
  }

  // 组建跨入交易
  Future<Map<String, dynamic>> buildTransferToBridge({
    @required String fromChainIdentifier,
    @required String fromChainTokenContract,
    @required String receiptorAddress,
    @required Amount amount,
    @required String fromAddress,
    @required TokenBlog bridgeToken,
    @required ValueNotifier<Widget> progressBarListenable,
    @required NetworkConfig targetChainConfig,
  }) async {
    final bridgePairs = await _pairRequestCompleter.future;

    final pair = bridgePairs.firstWhere(
      (p) =>
          p.chainIdentifier.toLowerCase() ==
              fromChainIdentifier.toLowerCase() &&
          p.outSideChainTokenAddress.toLowerCase() ==
              fromChainTokenContract.toLowerCase(),
    );

    assert(pair != null, 'BridgePairNountFound');

    final tx = {
      'from': fromAddress,
      'to': pair.outSideBridgeContracts,
    };

    // 主币跨链
    if (pair.outSideChainTokenAddress == null ||
        pair.outSideChainTokenAddress.length <= 0 ||
        pair.outSideChainTokenAddress.toLowerCase() ==
            callFromAddress.toLowerCase()) {
      ContractFunction fun = ContractFunction(
        'transferToBridge',
        [FunctionParameter('receipt', AddressType())],
        mutability: StateMutability.payable,
      );

      tx['data'] = bytesToHex(
        fun.encodeCall([
          receiptorAddress,
        ]),
        include0x: true,
      );

      tx['value'] = intToHex(amount.value);
    }
    // 代币跨链
    else {
      ContractFunction fun = ContractFunction(
        'transferToBridge',
        [
          FunctionParameter('tokenContract', AddressType()),
          FunctionParameter('receipt', AddressType()),
          FunctionParameter('amount', UintType()),
        ],
      );

      tx['data'] = bytesToHex(
        fun.encodeCall([
          pair.outSideChainTokenAddress,
          receiptorAddress,
          amount.value,
        ]),
        include0x: true,
      );
    }

    return {
      'name': 'signTransaction',
      'object': tx,
      'amount': amount,
      'fee': Amount(
        value: amount.value ~/ BigInt.from(1000),
        decimals: amount.decimals,
      ),
      'token': bridgeToken,
      'statebuilder': progressBarListenable,
      'target_network': targetChainConfig,
      'receiptor': receiptorAddress,
    };
  }

  // 监听跨入交易的实时状态
  Stream<TransferToBridgeState> watchTranferToBridgeState(
    String chainIdentifier,
    String txHash,
  ) {
    // ignore: close_sinks
    final StreamController<TransferToBridgeState> controller =
        StreamController();

    // 最大监听次数
    final retryCountMaxLimit = 20;
    // 每次监听的时间间隔
    final retryInterval = Duration(seconds: 2);

    // 交易成功需要等待一定的时间，才可以捕获到Locked事件，所以第一个延迟的时间较长
    Future.delayed(Duration(seconds: 5)).then((_) async {
      ///
      controller.sink.add(TransferToBridgeState.OnLockedSuccess);

      ////////////////////////////////////////////////////////////////////////
      /// 等待提案
      ////////////////////////////////////////////////////////////////////////
      print('等待提案');
      bool exist = false;
      for (int i = 0; i < retryCountMaxLimit; i++) {
        exist = await _bridgeProxy.isExistence(
          chainIdentifier: chainIdentifier,
          txHash: txHash,
        );
        print('第${i + 1}次尝试获取: $exist');

        // 不存在则继续等待,最大等待时间为5s+20s
        if (!exist) {
          await Future.delayed(retryInterval);
          continue;
        }

        // 已经在BridgeChain中获取到了对应的外链交易的提案
        controller.sink.add(TransferToBridgeState.OnProposalSuccess);

        // 进入下一个环节
        break;
      }
      if (!exist) {
        controller.sink.addError(TransferToBridgeState.OnProposalTimeoutError);
        controller.close();
        return;
      }

      ////////////////////////////////////////////////////////////////////////
      /// 监听提案的外链交易的可信度
      ////////////////////////////////////////////////////////////////////////
      print('监听提案的外链交易的可信度');
      int reliability = 0;
      for (int i = 0; i < retryCountMaxLimit; i++) {
        reliability = await _bridgeProxy.transactionReliability(
          chainIdentifier: chainIdentifier,
          txHash: txHash,
        );

        print('第${i + 1}次尝试获取: $reliability');
        // 不存在则继续等待,最大等待时间为5s+20s
        if (reliability <= 50) {
          await Future.delayed(retryInterval);
          continue;
        }

        // 已经在BridgeChain中获取到了对应的外链交易的提案
        controller.sink.add(TransferToBridgeState.OnVerifySuccess);

        // 进入下一个环节
        break;
      }
      if (reliability < 50) {
        controller.sink.addError(TransferToBridgeState.OnVerifyTimeoutError);
        controller.close();
        return;
      }

      ////////////////////////////////////////////////////////////////////////
      /// 等待铸币完成
      ////////////////////////////////////////////////////////////////////////
      print('等待铸币完成');
      bool isMinted = false;
      for (int i = 0; i < retryCountMaxLimit; i++) {
        isMinted = await isMintedOf(
          chainIdentifier: chainIdentifier,
          txHash: txHash,
        );

        print('第${i + 1}次尝试获取: $isMinted');
        // 不存在则继续等待,最大等待时间为5s+20s
        if (!isMinted) {
          await Future.delayed(retryInterval);
          continue;
        }

        // 已经在BridgeChain中获取到了对应的外链交易的提案
        controller.sink.add(TransferToBridgeState.OnMintSuccess);

        // 进入下一个环节
        break;
      }

      if (!isMinted) {
        controller.sink.addError(TransferToBridgeState.OnMintTimeoutError);
        controller.close();
        return;
      } else {
        controller.close();
      }
    });

    return controller.stream;
  }
}
