// @dart=2.9
library qytechnology.package.wallet.views.chaincore.bridge;

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:loading_indicator_view/loading_indicator_view.dart';
import 'package:wallet_core_flutter/providers/ethereum.dart';
import 'package:wallet_core_flutter/utils/utils.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/entity/entity.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/chaincore/ethereum.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/token_utils.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/sheet/transaction_confirm_sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

part 'anchor_tokens/anchor_tokens.dart';
part 'anchor_tokens/enums.dart';
part 'anchor_tokens/bridge_proxy.dart';
part 'views/combox.dart';
part 'views/chain_picker.dart';
part 'views/token_picker.dart';
part 'views/progress_bar.dart';

class BridgeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BridgeViewState();
  static BottomNavigationBarItem barItem() => BottomNavigationBarItem(
        label: S.current.ethereum_tab_bridge,
        backgroundColor: ThemeUtils().getColor('utils.bottom_bar.background'),
        icon: Image.asset(
          'assets/imgs/bridge.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: ThemeUtils().getColor(
            'utils.bottom_bar.normal_item_color',
          ),
        ),
        activeIcon: Image.asset(
          'assets/imgs/bridge.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: Colors.green[200],
          // ThemeUtils().getColor(
          //   'utils.bottom_bar.selected_item_color',
          // ),
        ),
      );
}

class _BridgeViewState extends State<BridgeView> {
  // 需要使用通用的交易页面，桥当作一个Dapp
  final Dapp bridgeDapp = Dapp(
    name: '时光桥',
    logo:
        'https://radar-me.obs.ap-southeast-1.myhuaweicloud.com/dapplogo/dapp_bridge.png',
  );

  // 桥上跨链资产管理合约
  AnchorTokensContract anchorTokensProxy;

  // 页面所需的数据是否已经加载完成
  final Completer<bool> payloadCompleter = Completer();

  // 当前选中的锚定币
  ValueNotifier<TokenBlog> currentAnchorTokenListenable = ValueNotifier(null);

  // 当前选中的目标链
  ValueNotifier<NetworkConfig> currentTargetNetworkListenable =
      ValueNotifier(null);

  // 当前选择的钱包地址
  ValueNotifier<RecipientAccountBlog> currentReciptorListenable =
      ValueNotifier(null);

  // 可选择的锚定币列表
  List<TokenBlog> bridgeTokens;

  // 可选择的目标链列表
  List<NetworkConfig> targetChains;

  // 数量
  TextEditingController amountEditController = TextEditingController();

  // 状态条控制
  ValueNotifier<ProgressStage> stateLisentable =
      ValueNotifier(ProgressStage.Begin);

  // 当前选中的BridgeToken对链上桥合约的授权数量
  ValueNotifier<Amount> allowanceOfBridgeListenable = ValueNotifier(null);

  // 操作按钮的状态控制 - Appove
  StatusButtonController appoveButtonController;

  // 操作按钮的状态控制 - Exchange
  StatusButtonController exchangeButtonController;

  Completer<Amount> balanceCompleter;

  @override
  void initState() {
    super.initState();

    anchorTokensProxy = AnchorTokensContract();

    appoveButtonController = StatusButtonController(
      title: '授权',
      busyTitle: '授权中...',
      status: StatusButtonState.Normal,
      style: StatusButtonStyle.Info,
      enable: false,
    );

    exchangeButtonController = StatusButtonController(
      title: '开始交易',
      busyTitle: '交易中',
      status: StatusButtonState.Normal,
      style: StatusButtonStyle.Info,
      enable: false,
    );

    ///////////////////////////////////////////////////////////////////////////
    // 获取当前用户选择的链中的所有锚定币资产
    ///////////////////////////////////////////////////////////////////////////
    anchorTokensProxy
        .bridgeTokens(
      CoreProvider.of(context).networkConfig.identifier,
      DataSourceProvider.of(context),
    )
        .then((tokens) {
      bridgeTokens = tokens;

      // 设置默认选择项
      currentAnchorTokenListenable.value = bridgeTokens.first;
    });

    ///////////////////////////////////////////////////////////////////////////
    // 切换跨链资产监听
    ///////////////////////////////////////////////////////////////////////////
    currentAnchorTokenListenable.addListener(() async {
      // 加载目标链列表
      targetChains = await anchorTokensProxy.bridgeTargetNetworks(
        currentAnchorTokenListenable.value,
        CoreProvider.of(context).networkConfig.identifier,
      );

      // 默认显示第一个跨链交易对
      currentTargetNetworkListenable.value = targetChains.first;

      // 用户切换资产以后需要重新获取额度，先重置为null，让页面显示忙碌状态
      // 因为后续的逻辑是异步过程，所以可以很好的处理页面锁定逻辑
      allowanceOfBridgeListenable.value = null;
      if (currentAnchorTokenListenable.value.isContract && isBridgeIn) {
        await refreshAllowanceOfBridgeToken();
      }

      await _currentBridgeTokenBalanceFuture();

      if (!payloadCompleter.isCompleted) {
        payloadCompleter.complete(true);
      }
    });

    ///////////////////////////////////////////////////////////////////////////
    // 切换目标链监听
    ///////////////////////////////////////////////////////////////////////////
    currentTargetNetworkListenable.addListener(() {
      // 目标链切换时,地址要求用户重新输入
      currentReciptorListenable.value = null;
    });

    ///////////////////////////////////////////////////////////////////////////
    // 当前选中的钱包地址切换
    ///////////////////////////////////////////////////////////////////////////
    CoreProvider.of(context).accountListenable.addListener(() {
      _currentBridgeTokenBalanceFuture();
    });
  }

  Future<void> _currentBridgeTokenBalanceFuture() async {
    balanceCompleter = Completer();
    if (currentAnchorTokenListenable.value.isContract) {
      await futureGetBalance(
        Web3ClientProvider.of(context),
        currentAnchorTokenListenable.value,
        CoreProvider.of(context).account.address,
      ).then(balanceCompleter.complete);
    } else {
      await Web3ClientProvider.of(context)
          .getBalance(
            CoreProvider.of(context).account.address,
          )
          .then(
            (balance) => balanceCompleter.complete(
              Amount(
                value: balance,
                decimals: 18,
              ),
            ),
          );
    }
  }

  bool get isBridgeIn =>
      CoreProvider.of(context).networkConfig.identifier !=
      anchorTokensProxy.hostNetworkIdentifier;

  // 获取授权额度
  Future<void> refreshAllowanceOfBridgeToken() async {
    // 检测的不是一个代币合约，直接返回一个超大的值，但一般来说应该避免这个逻辑，
    // 如果是主币交易不应该有检测授权额度但操作
    TokenBlog tokenBlog = currentAnchorTokenListenable.value;
    if (!tokenBlog.isContract) {
      return;
    }

    Web3ClientProvider web3 = Web3ClientProvider.of(context);
    String owner = CoreProvider.of(context).account.address;
    final spender = await anchorTokensProxy.bridgeLockerContract(
      currentAnchorTokenListenable.value,
      CoreProvider.of(context).networkConfig.identifier,
    );

    final fun = ContractFunction(
      'allowance',
      [
        // owner
        FunctionParameter(null, AddressType()),
        // spender
        FunctionParameter(null, AddressType()),
      ],
      outputs: [
        FunctionParameter(null, UintType()),
      ],
      type: ContractFunctionType.function,
      mutability: StateMutability.view,
    );

    final response = await web3.call(
      from: owner,
      to: tokenBlog.address,
      data: fun.encodeCall(
        [
          owner,
          spender,
        ],
      ),
    );

    final allowance = fun.decodeReturnValues(response)[0] as BigInt;

    allowanceOfBridgeListenable.value = Amount(
      value: allowance,
      decimals: tokenBlog.decimals,
    );

    return;
  }

  Widget bridgeChainSelector(BuildContext context) => Row(
        children: [
          Expanded(
            child: _ComBox(
              title: '从',
              icon: CachedImage.assetNetwork(
                url: CoreProvider.of(context).networkConfig.logoURL,
              ),
              text: CoreProvider.of(context).networkConfig.chainName,
              height: ScreenUtil().setHeight(40),
            ),
          ),
          Column(
            children: [
              SizedBox(
                height: ScreenUtil().setHeight(28),
              ),
              SizedBox(
                width: ScreenUtil().setHeight(30),
                child: Icon(
                  Icons.arrow_forward,
                  size: 20,
                ),
              )
            ],
          ),
          Expanded(
            child: ValueListenableBuilder<NetworkConfig>(
              valueListenable: currentTargetNetworkListenable,
              builder: (context, targetNetwork, child) => _ComBox(
                title: '至',
                icon: CachedImage.assetNetwork(
                  url: targetNetwork.logoURL,
                ),
                text: targetNetwork.chainName,
                height: ScreenUtil().setHeight(40),
                onPressed: () => _showTargetChainsPickerView(
                  context,
                  targetChains,
                  currentTargetNetworkListenable,
                ),
              ),
            ),
          ),
        ],
      );

  Widget bridgeReciptorSelector(BuildContext context) =>
      ValueListenableBuilder<RecipientAccountBlog>(
        valueListenable: currentReciptorListenable,
        builder: (context, receiptor, child) => _ComBox(
          title: '目标链接收地址',
          icon: CachedImage.assetNetwork(
            url: currentTargetNetworkListenable.value.logoURL,
          ),
          text: receiptor == null ? '请填写接收地址' : receiptor.addressBlog,
          suffixIconData: Icons.chevron_right,
          onPressed: () async {
            /// 显示地址搜索选择页面
            final RecipientAccountBlog recipient =
                await Navigator.of(context).push<RecipientAccountBlog>(
              ViewAnimations.viewRightIn(
                AddressSelectorCustom(
                  localKeys: CoreProvider.of(context).wallet.keysOf(
                        CoreProvider.of(context).networkConfig.chainCore,
                      ),
                  recentAccounts: await DataSourceProvider.of(context)
                      .localService
                      .selectRecentRecipientAccounts,
                ),
              ),
            );

            if (recipient != null &&
                currentReciptorListenable.value != recipient) {
              currentReciptorListenable.value = recipient;
            }
          },
        ),
      );

  Widget bridgeAmountInput(BuildContext context) =>
      ValueListenableBuilder<TokenBlog>(
        valueListenable: currentAnchorTokenListenable,
        builder: (context, bridgeToken, child) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  '数量',
                  style: ThemeUtils().getTextStyle(
                    'views.bridge.textstyle.cell_title',
                  ),
                  textAlign: TextAlign.start,
                ),
                Expanded(child: SizedBox()),
                FutureBuilder<Amount>(
                  future: balanceCompleter.future,
                  builder: (context, snapshot) => Row(
                    children: [
                      Text(
                        snapshot.connectionState != ConnectionState.done
                            ? ''
                            : '可用余额: ' +
                                snapshot.data.toStringAsFixed() +
                                ' ' +
                                bridgeToken.symbol,
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.treansfer.textstyle.bottom_balance',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(8),
            ),
            TextFormField(
              style: ThemeUtils().getTextStyle(
                'views.bridge.textstyle.address_content',
              ),
              controller: amountEditController,
              inputFormatters: [
                FilteringTextInputFormatter.allow(
                  RegExp(r'^[0-9]+(\.[0-9]*)?'),
                )
              ],
              keyboardType: TextInputType.numberWithOptions(
                signed: true,
                decimal: true,
              ),
              decoration: InputDecoration(
                hintText: '请输入数量',
                contentPadding: EdgeInsets.fromLTRB(
                  ScreenUtil().setWidth(10),
                  ScreenUtil().setWidth(0),
                  ScreenUtil().setWidth(20),
                  ScreenUtil().setWidth(0),
                ),
                hintStyle: ThemeUtils().getTextStyle(
                  'views.bridge.textstyle.amount_placeholder',
                ),
                fillColor: ThemeUtils().getColor(
                  'views.bridge.input_cell_background',
                ),
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(ScreenUtil().setWidth(10)),
                  ),
                  borderSide: BorderSide(
                    color: ThemeUtils().getColor(
                      'views.bridge.input_cell_background',
                    ),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(ScreenUtil().setWidth(10)),
                  ),
                  borderSide: BorderSide(
                    color: ThemeUtils().getColor(
                      'views.bridge.input_cell_background',
                    ),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(ScreenUtil().setWidth(10)),
                  ),
                  borderSide: BorderSide(
                    color: ThemeUtils().getColor(
                      'views.bridge.input_cell_background',
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(8),
            ),
            ValueListenableBuilder<TextEditingValue>(
              valueListenable: amountEditController,
              builder: (context, editingValue, child) => Text(
                '手续费：限时全免',
                // editingValue == null || editingValue.text.length <= 0
                //     ? '手续费: 限时全免'
                //     : '手续费: ' +
                //         Amount.fromString(
                //           editingValue.text,
                //           decimals: bridgeToken.decimals + 3,
                //           valueDecimals: bridgeToken.decimals,
                //         ).toStringAsFixed() +
                //         ' ' +
                //         bridgeToken.symbol,
                style: ThemeUtils().getTextStyle(
                  'views.chaincore.ethereum.treansfer.textstyle.bottom_balance',
                ),
              ),
            ),
          ],
        ),
      );

  Widget bridgeOperations(BuildContext context) =>
      ValueListenableBuilder<TokenBlog>(
        valueListenable: currentAnchorTokenListenable,
        builder: (context, bridgeToken, _) => ValueListenableBuilder(
          valueListenable: currentTargetNetworkListenable,
          builder: (context, targetNetwork, _) => Container(
            height: ScreenUtil().setHeight(50),
            child: isBridgeIn
                ?
                // 当前选择的跨链资产是链上铸币，则没有授权按钮
                bridgeToken.isContract
                    ? ValueListenableBuilder<Amount>(
                        valueListenable: allowanceOfBridgeListenable,
                        builder: (context, allowanceQuota, _) =>
                            ValueListenableBuilder<TextEditingValue>(
                          valueListenable: amountEditController,
                          builder: (context, amountEditing, _) {
                            if (allowanceQuota == null) {
                              return StatusButton.withStyle(
                                StatusButtonStyle.Info,
                                state: StatusButtonState.Busy,
                                title: 'Loading...',
                                onPressedFuture: () async {},
                              );
                            } else {
                              final inputedAmount = Amount.fromString(
                                amountEditing.text,
                                decimals: bridgeToken.decimals,
                                valueDecimals: bridgeToken.decimals,
                              );

                              // 暂未输入数值
                              if (inputedAmount.value <= BigInt.zero) {
                                return StatusButton.withController(
                                  appoveButtonController..enable = false,
                                  onPressedFuture: () async {},
                                );
                              }

                              // 代币交易，并且额度充足
                              if (allowanceQuota.value >= inputedAmount.value) {
                                return StatusButton.withStyle(
                                  StatusButtonStyle.Info,
                                  state: StatusButtonState.Normal,
                                  title: '开始交易',
                                  onPressedFuture: () async =>
                                      doBridgeTransfer(),
                                );
                              } else {
                                return StatusButton.withController(
                                  appoveButtonController..enable = true,
                                  onPressedFuture: () async {
                                    if (await doApproveBridgeTransaction(
                                      inputedAmount,
                                    )) {
                                      await refreshAllowanceOfBridgeToken();
                                    }
                                  },
                                );
                              }
                            }
                          },
                        ),
                      )
                    : StatusButton.withStyle(
                        StatusButtonStyle.Info,
                        state: StatusButtonState.Normal,
                        title: '开始交易',
                        onPressedFuture: () async => doBridgeTransfer(),
                      )
                : StatusButton.withStyle(
                    StatusButtonStyle.Info,
                    state: StatusButtonState.Normal,
                    title: '开始交易',
                    onPressedFuture: () async => doBridgeTransfer(),
                  ),
          ),
        ),
      );

  // 授权交易
  Future<bool> doApproveBridgeTransaction(Amount amount) async {
    // await SystemChannels.textInput.invokeMethod('TextInput.hide');
    FocusScope.of(context).requestFocus(FocusNode());

    if (currentReciptorListenable.value == null) {
      Fluttertoast.showToast(msg: '请填写目标链接收地址');
      return false;
    }

    // 交易一律使用的是当前选择的配置信息
    TokenBlog tokenBlog = currentAnchorTokenListenable.value;
    // 检测的不是一个代币合约，直接返回一个超大的值，但一般来说应该避免这个逻辑，
    // 如果是主币交易不应该有检测授权额度但操作
    if (!tokenBlog.isContract) {
      return Future.value(true);
    }

    Web3ClientProvider web3 = Web3ClientProvider.of(context);
    String spender = await anchorTokensProxy.bridgeLockerContract(
      tokenBlog,
      CoreProvider.of(context).networkConfig.identifier,
    );

    final tx = {
      'from': CoreProvider.of(context).account.address,
      'to': tokenBlog.address,
    };

    final fun = ContractFunction(
      'approve',
      [
        // owner
        FunctionParameter(null, AddressType()),
        // spender
        FunctionParameter(null, UintType()),
      ],
      outputs: [
        FunctionParameter(null, BoolType()),
      ],
      type: ContractFunctionType.function,
      mutability: StateMutability.view,
    );

    tx['data'] = bytesToHex(
      fun.encodeCall(
        [
          spender,
          amount.value,
        ],
      ),
      include0x: true,
    );

    final bridgeTransactionHash = await TransactionConfirmSheetController.show(
      context: context,
      requestData: {
        'name': 'signTransaction',
        'object': tx,
      },
      wallet: CoreProvider.of(context).wallet,
      account: CoreProvider.of(context).account,
      dapp: bridgeDapp,
      networkConfig: CoreProvider.of(context).networkConfig,
      dataSourceProvider: DataSourceProvider.of(context),
      web3clientProvider: Web3ClientProvider.of(context),
      scanAPIService: ScanAPIService(
        httpClient: http.Client(),
        hostURL: CoreProvider.of(context).networkConfig.currenEndPoint.scanURL,
        apiKey:
            CoreProvider.of(context).networkConfig.currenEndPoint.scanAPIKey,
      ),
    );

    if (bridgeTransactionHash == null) {
      return false;
    }

    // 监听交易，确保Appove完成,最大获取次数20，每次间隔2s
    // 首次获取直接延迟7s，大多公链的出块时间是3-5s，降低对接口的请求次数
    await Future.delayed(Duration(seconds: 7));
    for (int i = 0; i < 20; i++) {
      try {
        // 无异常成功获取，说明交易提交并且已经执行，可以重新检测授权额度
        await web3.getTransactionReceipt(
          bridgeTransactionHash,
        );
        return true;
      } catch (e) {
        await Future.delayed(Duration(seconds: 2));
      }
    }

    // 所有次数耗尽仍为成功
    return false;
  }

  // 执行锁定跨入交易,
  /////////////////////////////////////////////////
  // ！！噩梦级难度，请小心谨慎！！
  /////////////////////////////////////////////////
  void doBridgeTransfer() async {
    // await SystemChannels.textInput.invokeMethod('TextInput.hide');
    FocusScope.of(context).requestFocus(FocusNode());

    if (currentReciptorListenable.value == null) {
      Fluttertoast.showToast(msg: '请填写目标链接收地址');
      return;
    }

    final NetworkConfig contextNetworkConfig =
        CoreProvider.of(context).networkConfig;

    var requestMessage;

    TokenBlog bridgeToken = currentAnchorTokenListenable.value;

    StreamController<EventInfo> observers;

    // 来源为本链，则一律为跨出的燃烧交易
    if (contextNetworkConfig.identifier ==
        anchorTokensProxy.hostNetworkIdentifier) {
      requestMessage = await anchorTokensProxy.buildBurnFromBridge(
        toChainIdentifier: currentTargetNetworkListenable.value.identifier,
        hrc22TokenContract: bridgeToken.address,
        receiptorAddress: currentReciptorListenable.value.address,
        amount: Amount.fromString(
          amountEditController.text.length <= 0
              ? '0'
              : amountEditController.text,
          decimals: bridgeToken.decimals,
          valueDecimals: bridgeToken.decimals,
        ),
        fromAddress: CoreProvider.of(context).account.address,
        bridgeToken: bridgeToken,
        targetChainConfig: currentTargetNetworkListenable.value,
      );
    } else {
      requestMessage = await anchorTokensProxy.buildTransferToBridge(
        fromChainIdentifier: CoreProvider.of(context).networkConfig.identifier,
        fromChainTokenContract: bridgeToken.address,
        receiptorAddress: currentReciptorListenable.value.address,
        amount: Amount.fromString(
          amountEditController.text.length <= 0
              ? '0'
              : amountEditController.text,
          decimals: bridgeToken.decimals,
          valueDecimals: bridgeToken.decimals,
        ),
        fromAddress: CoreProvider.of(context).account.address,
        bridgeToken: bridgeToken,
        targetChainConfig: currentTargetNetworkListenable.value,
        progressBarListenable: ValueNotifier(
          _ProgressBar(
            stageLisentable: stateLisentable,
            padding: EdgeInsets.symmetric(horizontal: 40),
            stages: [
              ProgressStage.WaitingLock,
              ProgressStage.WaitingProposal,
              ProgressStage.WaitingSignature,
              ProgressStage.WaitingMinted,
            ],
          ),
        ),
      );

      // 设置状态为正在进行Lock交易
      stateLisentable.value = ProgressStage.WaitingLock;

      // 交易坚挺和状态反馈
      // ignore: close_sinks
      observers = StreamController();
      // 监听首个OnHash事件获取第一笔交易的Hash值
      observers.stream
          .firstWhere((info) => info.event == EventEmit.OnHash)
          .then(
        (info) async {
          Stream<TransferToBridgeState> stateStream =
              anchorTokensProxy.watchTranferToBridgeState(
            contextNetworkConfig.identifier,
            info.object as String,
          );

          stateStream.listen(
            (event) {
              switch (event) {
                case TransferToBridgeState.OnLockedSuccess:
                  stateLisentable.value = ProgressStage.WaitingProposal;
                  break;

                case TransferToBridgeState.OnProposalSuccess:
                  stateLisentable.value = ProgressStage.WaitingSignature;
                  break;

                case TransferToBridgeState.OnVerifySuccess:
                  stateLisentable.value = ProgressStage.WaitingMinted;
                  break;

                case TransferToBridgeState.OnMintSuccess:
                  stateLisentable.value = ProgressStage.Success;
                  break;

                // 其他状态都是错误信息，由onError处理
                default:
                  break;
              }
            },
            onError: (error) {
              if (error is TransferToBridgeState) {
              } else {}
            },
            onDone: () {
              Navigator.of(context).pop<String>(info.object as String);
            },
          );
        },
      ).catchError((streamErr) {
        // 用户取消交易时会遇到一个noElement的异常，此处捕获，但是忽律这个错误
      });
    }

    final bridgeTransactionHash = await TransactionConfirmSheetController.show(
      context: context,
      requestData: requestMessage,
      wallet: CoreProvider.of(context).wallet,
      account: CoreProvider.of(context).account,
      dapp: bridgeDapp,
      networkConfig: CoreProvider.of(context).networkConfig,
      dataSourceProvider: DataSourceProvider.of(context),
      web3clientProvider: Web3ClientProvider.of(context),
      scanAPIService: ScanAPIService(
        httpClient: http.Client(),
        hostURL: CoreProvider.of(context).networkConfig.currenEndPoint.scanURL,
        apiKey:
            CoreProvider.of(context).networkConfig.currenEndPoint.scanAPIKey,
      ),
      eventStreamController: observers,
    );

    if (observers != null && !observers.isClosed) {
      observers.close();
    }

    if (bridgeTransactionHash != null) {
      amountEditController.clear();
      currentReciptorListenable.value = null;
      Fluttertoast.showToast(msg: '交易成功');
    }
    print(bridgeTransactionHash);
  }

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: CoreProvider.of(context).accountListenable,
        builder: (context, account, child) => FutureBuilder<bool>(
          future: payloadCompleter.future,
          builder: (context, snapshot) => snapshot.connectionState !=
                  ConnectionState.done
              ? Container()
              : Container(
                  color: ThemeUtils().getColor('views.bridge.background'),
                  child: CustomScrollView(
                    physics: const AlwaysScrollableScrollPhysics(
                      parent: BouncingScrollPhysics(),
                    ),
                    slivers: [
                      SliverNavigationBar(
                        title: "时光桥",
                      ),
                      SliverPadding(
                        padding: EdgeInsets.fromLTRB(
                          ScreenUtil().setWidth(15),
                          ScreenUtil().setHeight(20),
                          ScreenUtil().setWidth(15),
                          ScreenUtil().setHeight(20),
                        ),
                        sliver: SliverToBoxAdapter(
                          child: Container(
                            decoration: BoxDecoration(
                              color: ThemeUtils()
                                  .getColor('views.bridge.foreground'),
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(1, 1),
                                  color: Colors.black26,
                                  blurRadius: 2,
                                ),
                              ],
                            ),
                            padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
                            child: Column(
                              children: [
                                //////////////////////////////////////////////////
                                // BridgeToken
                                //////////////////////////////////////////////////
                                ValueListenableBuilder(
                                  valueListenable: currentAnchorTokenListenable,
                                  builder: (context, anchorToken, child) =>
                                      _ComBox(
                                    title: '桥上资产',
                                    icon: CachedImage.assetNetwork(
                                      url: anchorToken.logo,
                                    ),
                                    text: anchorToken.symbol,
                                    onPressed: () => showBridgeTokensPickerView(
                                      context,
                                      bridgeTokens,
                                      currentAnchorTokenListenable,
                                    ),
                                  ),
                                ),

                                //////////////////////////////////////////////////
                                // ChainSelector
                                //////////////////////////////////////////////////
                                SizedBox(height: ScreenUtil().setHeight(15)),
                                bridgeChainSelector(context),

                                //////////////////////////////////////////////////
                                // Reciptor
                                //////////////////////////////////////////////////
                                SizedBox(height: ScreenUtil().setHeight(15)),
                                bridgeReciptorSelector(context),

                                //////////////////////////////////////////////////
                                // Amount
                                //////////////////////////////////////////////////
                                SizedBox(height: ScreenUtil().setHeight(15)),
                                bridgeAmountInput(context),

                                //////////////////////////////////////////////////
                                // Actions
                                //////////////////////////////////////////////////
                                SizedBox(height: ScreenUtil().setHeight(15)),
                                bridgeOperations(context),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      );
}
