// @dart=2.9
part of '../setting.dart';

typedef BeforLanguageChanged = Future<void> Function(Locale local);

class LanguagePage extends StatefulWidget {
  final BeforLanguageChanged beforLanguageChanged;

  LanguagePage({this.beforLanguageChanged});

  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  Completer<int> cacheLoadingCompleter = Completer();
  List<_LanguageLocal> _datas() => [
        _LanguageLocal(
          S.current.ethereum_setting_language_zh_title,
          Locale.fromSubtags(languageCode: 'zh'),
          "assets/imgs/language_cn.png",
        ),
        _LanguageLocal(
          S.current.ethereum_setting_language_en_title,
          Locale.fromSubtags(languageCode: 'en'),
          "assets/imgs/language_en.png",
        ),
      ];

  int selectedIndex = 1;

  /// 计算缓存大小
  void loadingCache() async {
    int cacheSize = 0;
    Directory tempDir = await getTemporaryDirectory();
    tempDir.listSync(recursive: true, followLinks: false).forEach((file) {
      cacheSize += file.statSync().size;
    });
    cacheLoadingCompleter.complete(cacheSize);
  }

  /// 清除全部缓存
  void cleanCache() async {
    Directory tempDir = await getTemporaryDirectory();

    tempDir.listSync(recursive: false, followLinks: false).forEach((file) {
      file.deleteSync(recursive: true);
    });

    cacheLoadingCompleter = Completer();
    loadingCache();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    if (CurrentLocaleLisetnable().value == null) {
      selectedIndex = 0;
    } else {
      selectedIndex = _datas().indexWhere(
        (element) =>
            element.locale.languageCode ==
            CurrentLocaleLisetnable().value.languageCode,
      );
    }

    if (selectedIndex < 0) {
      selectedIndex = 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_setting_language_title,
        elevation: 0.3,
      ),
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                    child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        top: ScreenUtil().setHeight(0),
                      ),
                      color: ThemeUtils().getColor(
                        'views.chaincore.ethereum.settings.netinfomation.cell_background',
                      ),
                      // color: ThemeUtils().getColor(
                      //     'views.chaincore.ethereum.assets.cell_background'),
                      child: ListTile(
                        leading: Container(
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(5),
                          ),
                          width: ScreenUtil().setWidth(18),
                          height: ScreenUtil().setHeight(18),
                          child: Image.asset(
                            _datas()[index].icon,
                            package: 'wallet_flutter',
                          ),
                        ),
                        minLeadingWidth: ScreenUtil().setWidth(10),
                        contentPadding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(25),
                        ),
                        title: Text(
                          _datas()[index].text,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.settings.language.textstyle.cell_title',
                          ),
                        ),
                        trailing: Icon(
                          selectedIndex == index
                              ? Icons.radio_button_checked_rounded
                              : Icons.radio_button_off_rounded,
                          color: ThemeUtils().getColor(
                            'views.chaincore.ethereum.settings.header_background',
                          ),
                        ),
                        onTap: () async {
                          selectedIndex = index;

                          CurrentLocaleLisetnable().value =
                              _datas()[index].locale;

                          await ConfigStorageUtils().write<String>(
                            ConfigKey.LanguageCode,
                            _datas()[index].locale.languageCode,
                          );
                          cleanCache();

                          Navigator.pop(context);

                          setState(() {});
                        },
                      ),
                    ),
                    Container(
                      height: 1,
                      color: ThemeUtils().getColor(
                        'page.background',
                      ),
                      margin: EdgeInsets.only(left: ScreenUtil().setWidth(36)),
                    ),
                  ],
                ));
              },
              childCount: _datas().length,
            ),
          ),
        ],
      ),
    );
  }
}

class _LanguageLocal extends Object {
  String icon;
  String text;
  Locale locale;

  _LanguageLocal(this.text, this.locale, this.icon);
}
