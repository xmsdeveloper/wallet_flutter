// @dart=2.9
part of '../setting.dart';

class NetworkPage extends StatefulWidget {
  @override
  _NetworkPageState createState() => _NetworkPageState();
}

class _NetworkPageState extends State<NetworkPage> {
  List<NetworkConfig> networks;

  void initState() {
    super.initState();
    networks = NetworkStorageUtils()
        .chainNames
        .map((e) => NetworkStorageUtils().networkConfigOf(e))
        .toList();
  }

  Widget buildSection(BuildContext context, int section) => SliverPadding(
        padding: EdgeInsets.only(top: ScreenUtil().setHeight(1)),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) => buildCell(context, section, index),
            childCount: 1,
          ),
        ),
      );

  Widget buildCell(BuildContext context, int section, int row) => Container(
        height: ScreenUtil().setHeight(62),
        color: ThemeUtils().getColor(
          'views.chaincore.ethereum.settings.netinfomation.cell_background',
        ),
        child: ListTile(
          minLeadingWidth: ScreenUtil().setWidth(10),
          contentPadding: EdgeInsets.only(
              left: ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(20)),
          leading: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(360),
            ),
            width: ScreenUtil().setWidth(18),
            height: ScreenUtil().setHeight(18),
            margin: EdgeInsets.only(top: ScreenUtil().setHeight(4)),
            child: Container(
              child: Image.network(
                networks[section].coinLogoURL,
              ),
            ),
          ),
          title: Text(
            networks[section].markName,
            style: ThemeUtils().getTextStyle(
              'views.keychain.infomation.textstyle.cell_title',
            ),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios_sharp,
            color:
                ThemeUtils().getColor('views.keychain.infomation.cell_arrow'),
            size: ScreenUtil().setHeight(15),
          ),
          onTap: () => Navigator.of(context).push(
            ViewAnimations.viewRightIn(
              NetworkinfomationPage(
                networkConfig: networks[section],
              ),
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    final slivers = <Widget>[];

    for (int section = 0; section < networks.length; section++) {
      slivers.add(buildSection(context, section));
    }

    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_setting_network_title,
      ),
      backgroundColor: ThemeUtils().getColor(
        'views.chaincore.ethereum.settings.netinfomation.background',
      ),
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: slivers,
      ),
    );
  }
}
