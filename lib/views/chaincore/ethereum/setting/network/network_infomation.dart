// @dart=2.9
part of '../../setting.dart';

class NetworkinfomationPage extends StatefulWidget {
  final NetworkConfig networkConfig;

  // final NetworkConfig networkConfigs;
  NetworkinfomationPage({
    this.networkConfig,
  });
  @override
  _NetworkinfomationPageState createState() => _NetworkinfomationPageState();
}

class _NetworkinfomationPageState extends State<NetworkinfomationPage> {
  bool isOpen = false;
  int groupValue = 1;
  List textsource;

  void initState() {
    super.initState();

    textsource = [
      {
        "title": S.current.ethereum_setting_network_network_terms,
        "subtext": '${this.widget.networkConfig.markName}',
      },
      {
        "title": S.current.ethereum_setting_network_chain_iD,
        "subtext": "${this.widget.networkConfig.networkID}",
      },
      {
        "title": S.current.ethereum_setting_network_symbol,
        "subtext": "${this.widget.networkConfig.mainSymbol}",
      },
      // {
      //   "title": S.current.ethereum_setting_blockchain_browser,
      //   "subtext": "${this.widget.networkConfig.currenEndPoint.scanURL}",
      // },
      {
        "title": S.current.ethereum_setting_d_s_server,
        "subtext":
            "${this.widget.networkConfig.currenEndPoint.resourcesServicesURL}",
      },
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_setting_network_information_title,
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: ScreenUtil().setHeight(20),
                  right: ScreenUtil().setWidth(20),
                  top: ScreenUtil().setHeight(20),
                ),
                height: ScreenUtil().setHeight(60),
                // width: ScreenUtil().setWidth(335),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    width: ScreenUtil().setWidth(1),
                    color: ThemeUtils().getColor(
                      'views.chaincore.ethereum.settings.netinfomation.sizebox_color',
                    ),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(10),
                    right: ScreenUtil().setWidth(10),
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      S.current.ethereum_setting_network_information_tips,
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.settings.netinfomation.textstyle.tips_title',
                      ),
                    ),
                  ),
                ),
              ),
              Column(
                children: this
                    .textsource
                    .map((e) => Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(20),
                                bottom: ScreenUtil().setHeight(20),
                              ),
                              width: ScreenUtil().setWidth(350),
                              child: Text(
                                e['title'],
                                style: ThemeUtils().getTextStyle(
                                  'views.chaincore.ethereum.settings.netinfomation.textstyle.cell_subtitle',
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: ScreenUtil().setHeight(20)),
                              width: ScreenUtil().setWidth(350),
                              child: Text(
                                e['subtext'],
                                style: ThemeUtils().getTextStyle(
                                  'views.chaincore.ethereum.settings.netinfomation.textstyle.cell_title',
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(1),
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(10),
                                  right: ScreenUtil().setWidth(10),
                                ),
                                color: ThemeUtils().getColor(
                                  'views.chaincore.ethereum.settings.netinfomation.sizebox_color',
                                ),
                              ),
                            ),
                          ],
                        ))
                    .toList(),
              ),
              _buildPanel()
            ],
          )
        ],
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelList(
      expandedHeaderPadding: EdgeInsets.only(bottom: 0),
      elevation: 0,
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          isOpen = !isOpen;
        });
      },
      children: [
        ExpansionPanel(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: Container(

                  // height: ScreenUtil().setHeight(30),
                  child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  S.current.ethereum_setting_network_node,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.settings.netinfomation.textstyle.cell_subtitle',
                  ),
                ),
              )),
            );
          },
          body: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                width: ScreenUtil().setWidth(1),
                color: ThemeUtils().getColor(
                  'views.chaincore.ethereum.settings.netinfomation.node_border',
                ),
              )),
              child: Column(
                children: [
                  Column(
                    children: widget.networkConfig.endpoints
                        .map((endPoint) => ListTile(
                              title: Column(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                            right: ScreenUtil().setWidth(10)),
                                        child: Text(
                                          S.current
                                              .ethereum_setting_network_information_node,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(12),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          endPoint.nodeURL,
                                          maxLines: 1,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(12),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                            right: ScreenUtil().setWidth(10)),
                                        child: Text(
                                          S.current
                                              .ethereum_setting_network_information_net,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(12),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            right: ScreenUtil().setWidth(5)),
                                        height: ScreenUtil().setHeight(6),
                                        width: ScreenUtil().setWidth(6),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.green),
                                      ),
                                      Text(
                                        "80 ms",
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              trailing: Radio(
                                  value: true,
                                  groupValue: endPoint ==
                                      widget.networkConfig.currenEndPoint,
                                  onChanged: (index) {}),
                              onTap: () {
                                setState(() {});
                              },
                            ))
                        .toList(),
                  ),
                ],
              )),
          isExpanded: isOpen,
        ),
      ],
    );
  }
}
