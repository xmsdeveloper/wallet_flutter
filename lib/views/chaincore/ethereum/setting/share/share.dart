// @dart=2.9
part of '../../setting.dart';

class SharePage extends StatefulWidget {
  @override
  _SharePageState createState() => _SharePageState();
}

class _SharePageState extends State<SharePage> {
  List<_LanguageLocal> _datas = [];

  int selectedIndex = 1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: NavigationBar(
          title: "邀请分享",
        ),
        backgroundColor: Colors.blue,
        body: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(27),
                bottom: ScreenUtil().setHeight(20),
              ),
              width: ScreenUtil().setWidth(82),
              height: ScreenUtil().setHeight(82),
              child: Image.asset(
                'assets/imgs/logoabout.png',
                package: 'wallet_flutter',
              ),
            ),
            Container(
              child: Center(
                child: Text(
                  '邀请好友一起使用Radar Wallet',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(
              height: ScreenUtil().setHeight(542),
              width: ScreenUtil().setWidth(329),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    'assets/imgs/share_bg.png',
                    package: 'wallet_flutter',
                  ),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(56),
                      top: ScreenUtil().setHeight(36),
                      bottom: ScreenUtil().setHeight(10),
                    ),
                    child: Text(
                      '方式一',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(56),
                      bottom: ScreenUtil().setHeight(10),
                    ),
                    child: Text(
                      '通过二维码获取下载页面安装Radar Wallet',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(
                      ScreenUtil().setWidth(3),
                    ),
                    width: ScreenUtil().setWidth(120),
                    height: ScreenUtil().setHeight(120),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/imgs/qrcode_bg.png',
                          package: 'wallet_flutter',
                        ),
                      ),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(left: ScreenUtil().setWidth(3)),
                      child: QrImage(
                        data: "http://web.radarlab.me/radar_token/#/download",
                        version: QrVersions.auto,
                        size: ScreenUtil().setSp(300),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setWidth(30),
                    ),
                    width: ScreenUtil().setWidth(245),
                    height: 1,
                    color: Colors.grey,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(56),
                      top: ScreenUtil().setWidth(30),
                      bottom: ScreenUtil().setHeight(10),
                    ),
                    child: Text(
                      '方式二',
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(56),
                      bottom: ScreenUtil().setHeight(10),
                    ),
                    child: Text(
                      '发送下载页链接给好友',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(275),
                    height: ScreenUtil().setHeight(63),
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setHeight(20),
                      right: ScreenUtil().setWidth(20),
                      top: ScreenUtil().setHeight(20),
                    ),

                    // width: ScreenUtil().setWidth(335),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        width: ScreenUtil().setWidth(1),
                        color: ThemeUtils().getColor(
                          'views.chaincore.ethereum.settings.netinfomation.sizebox_color',
                        ),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: ScreenUtil().setWidth(10),
                        right: ScreenUtil().setWidth(10),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "我正在使用Radar Wallet管理自己的数字资产，邀请你一起使用!(推荐使用浏览器打开)http://web.radarlab.me/radar_token/#/download",
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.settings.netinfomation.textstyle.tips_title',
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      await Clipboard.setData(
                        ClipboardData(
                          text: "http://web.radarlab.me/radar_token/#/download",
                        ),
                      );

                      Fluttertoast.showToast(
                        msg: S.current.copy_success,
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.all(20),
                      width: ScreenUtil().setWidth(260),
                      height: ScreenUtil().setHeight(40),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Center(
                        child: Text(
                          '分享给好友',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
