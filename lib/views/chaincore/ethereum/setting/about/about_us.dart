// @dart=2.9
part of '../../setting.dart';

class AboutPage extends StatefulWidget {
  final String netVersion;
  final String localversion;
  final String downUrl;
  final String content;
  final Web3ClientProvider web3clientProvider;
  final DataSourceProvider dataSourceProvider;

  AboutPage(
      {this.netVersion,
      this.localversion,
      this.downUrl,
      this.content,
      this.web3clientProvider,
      this.dataSourceProvider});
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  List<NetworkConfig> networks;

  void initState() {
    super.initState();
    networks = NetworkStorageUtils()
        .chainNames
        .map((e) => NetworkStorageUtils().networkConfigOf(e))
        .toList();

    datasource = [
      {
        "identifier": "system",
        "title": S.current.ethereum_setting_about_us_version_update,
        "subtext": this.widget.netVersion,
      },
      {
        "identifier": "clause",
        "title": "服务和隐私条款",
        "subtext": "",
      },
    ];
  }

  List datasource;

  Widget buildSection(
    BuildContext context,
  ) =>
      SliverPadding(
        padding: EdgeInsets.only(top: ScreenUtil().setHeight(1)),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) => buildCell(context),
            childCount: 1,
          ),
        ),
      );

  Widget buildCell(
    BuildContext context,
  ) =>
      InkWell(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(20),
              ),
              width: ScreenUtil().setWidth(80),
              height: ScreenUtil().setHeight(80),
              child: Image.asset(
                'assets/imgs/gct.png',
                package: 'wallet_flutter',
              ),
            ),
            Container(
              margin: EdgeInsets.all(
                ScreenUtil().setHeight(10),
              ),
              // child: Text(
              //   'GctWallet',
              //   style: TextStyle(
              //     fontSize: ScreenUtil().setSp(18),
              //   ),
              // ),
            ),
            Container(
              child: Text(
                S.current.ethereum_setting_about_us_current_version +
                    ':${this.widget.localversion}',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(14),
                ),
              ),
              margin: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(60),
              ),
            ),
            Column(
                children: datasource
                    .map(
                      (e) => InkWell(
                        onTap: () {
                          if (e['identifier'] == "system") {
                            if (this.widget.localversion ==
                                this.widget.netVersion) {
                              Fluttertoast.showToast(
                                msg: S.current.ethereum_setting_updata_tips,
                                gravity: ToastGravity.CENTER,
                              );
                            } else {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AppVersionPage(
                                    url: this.widget.downUrl,
                                    content: this.widget.content,
                                  );
                                },
                              );
                            }
                          } else if (e['identifier'] == "clause") {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return FreezePage();
                              },
                            );
                            // openDapp(
                            //   context,
                            //   Dapp(
                            //     url:
                            //         "http://web.radarlab.me/radar_machine/#/notice",
                            //     logo: "",
                            //   ),
                            // );
                            //   Fluttertoast.showToast(
                            //       msg: S.current
                            //           .message_box_prompt_password_not_yet_open);
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: ThemeUtils().getColor(
                              'views.chaincore.ethereum.settings.netinfomation.cell_background',
                            ),
                            border: Border(
                              bottom: BorderSide(
                                width: 1,
                                color: ThemeUtils().getColor(
                                  'views.chaincore.ethereum.settings.netinfomation.background',
                                ),
                              ),
                            ),
                          ),
                          height: ScreenUtil().setHeight(62),
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(20)),
                                width: ScreenUtil().setWidth(150),
                                child: Text(
                                  e['title'],
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerRight,
                                width: ScreenUtil().setWidth(200),
                                padding: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(10),
                                ),
                                child: Text(e['subtext']),
                              ),
                              e['subtext'] != "" &&
                                      this.widget.localversion !=
                                          this.widget.netVersion
                                  ? Container(
                                      height: 10,
                                      width: 10,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(180),
                                        color: Colors.red,
                                      ),
                                    )
                                  : Container(),
                              Container(
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  size: ScreenUtil().setHeight(15),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                    .toList())
          ],
        ),
      );

  void openDapp(BuildContext context, Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: this.widget.web3clientProvider,
          dataSourceProvider: this.widget.dataSourceProvider,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_setting_about_us_title,
      ),
      backgroundColor: ThemeUtils().getColor(
        'views.chaincore.ethereum.settings.netinfomation.background',
      ),
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) => buildCell(context),
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }
}
