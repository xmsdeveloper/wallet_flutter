// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/generated/l10n.dart';

class FreezePage extends StatefulWidget {
  _FreezePageState createState() => _FreezePageState();
}

class _FreezePageState extends State<FreezePage> {
  int speedProgress;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();

    // bindBackgroundIsolate((id, status, progress) {});
  }

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.all(ScreenUtil().setHeight(20)),
      child: new Material(
        //创建透明层
        type: MaterialType.transparency, //透明类型(dialog的半透明效果)
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: ScreenUtil().setWidth(335),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: ThemeUtils().getColor(
                  'views.chaincore.ethereum.updata.background_color',
                ),
              ),
              child: Column(
                children: [
                  // Container(
                  //   width: ScreenUtil().setWidth(335),
                  //   height: ScreenUtil().setHeight(146),
                  //   child: Image.asset(
                  //     'assets/imgs/updata.png',
                  //     fit: BoxFit.fill,
                  //     package: 'wallet_flutter',
                  //   ),
                  // ),
                  // Container(
                  //   alignment: Alignment.centerLeft,
                  //   margin: EdgeInsets.only(
                  //     top: ScreenUtil().setHeight(20),
                  //     left: ScreenUtil().setWidth(29),
                  //     right: ScreenUtil().setWidth(29),
                  //   ),
                  //   child: Text(
                  //     S.current.ethereum_setting_updata_title,
                  //     style: ThemeUtils().getTextStyle(
                  //       'views.chaincore.ethereum.updata.textstyle.title',
                  //     ),
                  //   ),
                  // ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(28),
                      bottom: ScreenUtil().setHeight(33),
                    ),
                    width: ScreenUtil().setWidth(62.01),
                    height: ScreenUtil().setHeight(61.94),
                    child: Image.asset(
                      'assets/imgs/prohibit.png',
                      package: "wallet_flutter",
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(
                        left: ScreenUtil().setWidth(40),
                        right: ScreenUtil().setWidth(40),
                        bottom: ScreenUtil().setHeight(48)),
                    child: Text(
                      '资产已被锁定！您的账户涉嫌盗取资产，请联系客服，提供相应材料处理！',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Color.fromRGBO(112, 112, 112, 1),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
