// @dart=2.9
part of '../setting.dart';

class AppVersionPage extends StatefulWidget {
  final String url;
  final String content;
  final String netVersion;
  AppVersionPage(
      {Key key, @required this.url, @required this.content, this.netVersion})
      : super(key: key);

  _AppVersionPageState createState() => _AppVersionPageState();
}

class _AppVersionPageState extends State<AppVersionPage> {
  int speedProgress;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.all(ScreenUtil().setHeight(20)),
      child: new Material(
        //创建透明层
        type: MaterialType.transparency, //透明类型(dialog的半透明效果)
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                    colors: [Colors.transparent, Colors.white],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [
                      0.1,
                      0.2,
                    ]),
              ),
              width: ScreenUtil().setWidth(287.49),
              child: Column(
                children: [
                  Container(
                    width: ScreenUtil().setWidth(287.49),
                    height: ScreenUtil().setHeight(141),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/images/updata.png',
                          // package: 'wallet_flutter',
                        ),
                        fit: BoxFit.fill,
                      ),
                      // borderRadius: BorderRadius.circular(20),
                      // color: ThemeUtils().getColor(
                      //   'views.chaincore.ethereum.updata.background_color',
                      // ),
                    ),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(40),
                            left: ScreenUtil().setWidth(22),
                          ),
                          child: Text(
                            // this.widget.netVersion,
                            S.current.ethereum_setting_updata_find_title,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(21),
                              color: Colors.white,
                            ),
                            // style: ThemeUtils().getTextStyle(
                            //   'views.chaincore.ethereum.updata.textstyle.title',
                            // ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(6),
                            left: ScreenUtil().setWidth(22),
                          ),
                          child: Text(
                            // this.widget.netVersion,
                            this.widget.netVersion,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(20),
                              color: Colors.white,
                            ),
                            // style: ThemeUtils().getTextStyle(
                            //   'views.chaincore.ethereum.updata.textstyle.title',
                            // ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(28),
                          left: ScreenUtil().setWidth(22),
                        ),
                        child: Text(
                          this.widget.netVersion,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.updata.textstyle.title',
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(28),
                        ),
                        child: Text(
                          S.current.ethereum_setting_updata_title,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.updata.textstyle.title',
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(15),
                      bottom: ScreenUtil().setHeight(45),
                      left: ScreenUtil().setWidth(22),
                      right: ScreenUtil().setWidth(22),
                    ),
                    child: Text(
                      this.widget.content ?? "",
                      // "1、整体界面优优化",
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.updata.textstyle.content',
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () async {
                          Navigator.pop(context);
                          ConfigStorageUtils().writeUpdatePromptIndexOfKey(
                              ConfigKey.UpdatePrompt, false);
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            // left: ScreenUtil().setWidth(20),
                            right: ScreenUtil().setWidth(16),
                            // top: ScreenUtil().setHeight(110),
                            bottom: ScreenUtil().setHeight(34),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              width: 1,
                              color: Color.fromRGBO(216, 216, 216, 1),
                            ),
                          ),
                          height: ScreenUtil().setHeight(40),
                          width: ScreenUtil().setWidth(120),
                          child: Center(
                            child: Text(
                              S.current.ethereum_setting_update_alert_cancel,
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          // _download();
                          await FlutterWebBrowser.openWebPage(
                            url: this.widget.url,
                            customTabsOptions: CustomTabsOptions(
                              colorScheme: CustomTabsColorScheme.dark,
                              toolbarColor: Colors.deepPurple,
                              secondaryToolbarColor: Colors.green,
                              navigationBarColor: Colors.amber,
                              addDefaultShareMenuItem: true,
                              instantAppsEnabled: true,
                              showTitle: true,
                              urlBarHidingEnabled: true,
                            ),
                            safariVCOptions: SafariViewControllerOptions(
                              barCollapsingEnabled: true,
                              preferredBarTintColor: Colors.green,
                              preferredControlTintColor: Colors.amber,
                              dismissButtonStyle:
                                  SafariViewControllerDismissButtonStyle.close,
                              modalPresentationCapturesStatusBarAppearance:
                                  true,
                            ),
                          );
                          Navigator.pop(context);
                          // await _download();

                          // Navigator.pop(context);
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            // top: ScreenUtil().setHeight(110),
                            bottom: ScreenUtil().setHeight(34),
                          ),
                          decoration: BoxDecoration(
                            color: ThemeUtils().getColor(
                              'views.chaincore.ethereum.settings.header_background',
                            ),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          height: ScreenUtil().setHeight(40),
                          width: ScreenUtil().setWidth(120),
                          child: Center(
                            child: Text(
                              S.current.ethereum_setting_update_alert_confirm,
                              style: ThemeUtils().getTextStyle(
                                'views.chaincore.ethereum.updata.textstyle.buttom_text',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
