// @dart=2.9
part of '../setting.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final TextEditingController walletNameEditController =
      TextEditingController();
  final TextEditingController passWordEditController = TextEditingController();

  final TextEditingController confirmpassWordEditController =
      TextEditingController();
  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");
  final ValueNotifier<String> walletNameEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> passWordEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> confirmpassWordEditErrorListenable =
      ValueNotifier<String>(null);

  String passwordStrength = "";
  bool err = false;
  final ValueNotifier<String> passwordStrengthListenable =
      ValueNotifier<String>(null);

  /// 密码强度正则,表示匹配任意的拉丁大小写字母，数字再加上下划线和减号
  final weakPwdStrengthRegexp = RegExp(
    r'[\w_-]$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含2个大小写字母
  final middlePwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]{2,}).*$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含1个小写字母,必须包含1个大写字母,必须包含1个特殊字符
  final strongPwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*[!@#$%^&*?\(\)]).*$',
  );
  Future<void> doChangge(BuildContext context) async {
    final verifySuccess = CoreProvider.of(context)
        .wallet
        .verifySecurity(walletNameEditController.text);

    if (walletNameEditController.text.length <= 0) {
      errorListenable.value =
          S.current.ethereum_setting_change_password_old_tips;

      return;
    } else if (passWordEditController.text.length <= 0) {
      errorListenable.value = "";
      errorListenable.value =
          S.current.ethereum_setting_change_password_new_tips;
      return;
    } else if (confirmpassWordEditController.text.length <= 0) {
      errorListenable.value = null;
      errorListenable.value =
          S.current.ethereum_setting_change_password_confim_tips;
      return;
    } else if (confirmpassWordEditController.text !=
        passWordEditController.text) {
      errorListenable.value =
          S.current.ethereum_setting_change_password_not_consistent;
      errorListenable.value =
          S.current.ethereum_setting_change_password_not_consistent;
      return;
    } else if (!verifySuccess) {
      errorListenable.value = null;
      errorListenable.value = null;
      errorListenable.value =
          S.current.ethereum_setting_change_password_incorrect;
    } else {
      walletNameEditErrorListenable.value = null;
      CoreProvider.of(context).wallet.securitySHA256 =
          sha256.convert(utf8.encode(passWordEditController.text)).bytes;
      CoreProvider.of(context).wallet.save();
      await Future.delayed(Duration(milliseconds: 1000));
      Fluttertoast.showToast(
          msg: S.current.message_box_prompt_password_modified_successfully);

      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: NavigationBar(
          elevation: 0.3,
          title: S.current.ethereum_setting_change_password_title,
        ),
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  child: WarmingPage(
                    tipText: S.current.create_wallet_tips,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: ValueListenableBuilder(
                    valueListenable: walletNameEditErrorListenable,
                    builder: (context, value, child) => Input(
                      title: "",
                      errorText: value,
                      placeholder: S.current
                          .ethereum_setting_change_password_input_old_tips,
                      controller: walletNameEditController,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: ValueListenableBuilder(
                    valueListenable: passWordEditErrorListenable,
                    builder: (context, value, child) => Input(
                      title: "",
                      errorText: value,
                      placeholder: S.current
                          .ethereum_setting_change_password_input_new_tips,
                      controller: passWordEditController,
                      onChanged: (_) {
                        if (weakPwdStrengthRegexp
                                .hasMatch(passWordEditController.text) ==
                            true) {
                          this.passwordStrength =
                              S.current.password_strength_weak;
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        } else {
                          this.passwordStrength = "";
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        }

                        if (middlePwdStrengthRegexp
                                .hasMatch(passWordEditController.text) ==
                            true) {
                          this.passwordStrength =
                              S.current.password_strength_middle;
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        }
                        if (strongPwdStrengthRegexp
                                .hasMatch(passWordEditController.text) ==
                            true) {
                          this.passwordStrength =
                              S.current.password_strength_strength;
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        }
                      },
                    ),
                  ),
                ),
                ValueListenableBuilder(
                  valueListenable: passwordStrengthListenable,
                  builder: (context, value, child) {
                    return this.passwordStrength != ""
                        ? Container(
                            margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(13)),
                            child: PwdIdentifier(
                              passwordStrength: this.passwordStrength,
                            ),
                          )
                        : Container();
                  },
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: ValueListenableBuilder(
                    valueListenable: confirmpassWordEditErrorListenable,
                    builder: (context, value, child) => Input(
                      title: "",
                      errorText: value,
                      placeholder: S.current
                          .ethereum_setting_change_password_input_confim_tips,
                      controller: confirmpassWordEditController,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          ViewAnimations.viewRightIn(
                            ForgetPassword(),
                          ),
                        );
                      },
                      child: Container(
                        // margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(16),
                        ),
                        // alignment: Alignment.centerRight,
                        child: Text(
                          S.current
                              .ethereum_setting_change_password_forget_the_password,
                          style: TextStyle(
                            fontSize: ScreenUtil().setHeight(14),
                            color: Color.fromRGBO(51, 51, 51, 1),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(46)),
                  child: ValueListenableBuilder(
                    valueListenable: errorListenable,
                    builder: (contet, value, child) {
                      return Text(
                        errorListenable.value,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          color: Color.fromRGBO(203, 0, 0, 1),
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
                  child: StatusButton.withStyle(
                    StatusButtonStyle.Info,
                    title: S.current.ethereum_dapp_collection_sheet_title_2,
                    // ignore: missing_return
                    onPressedFuture: () async {
                      await doChangge(context);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
