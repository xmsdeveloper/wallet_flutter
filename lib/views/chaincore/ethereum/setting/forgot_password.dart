// @dart=2.9
part of '../setting.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  @override
  final TextEditingController privateKeyEditController =
      TextEditingController();
  final TextEditingController passWordEditController = TextEditingController();
  final TextEditingController confirmpassWordEditController =
      TextEditingController();

  final ValueNotifier<String> errorListenable = ValueNotifier<String>("");
  final ValueNotifier<String> privateKeyEditErrorListenable =
      ValueNotifier<String>(null);

  final ValueNotifier<String> passWordEditErrorListenable =
      ValueNotifier<String>(null);
  final ValueNotifier<String> confirmpassWordEditErrorListenable =
      ValueNotifier<String>(null);

  String passwordStrength = "";
  final ValueNotifier<String> passwordStrengthListenable =
      ValueNotifier<String>(null);

  /// 密码强度正则,表示匹配任意的拉丁大小写字母，数字再加上下划线和减号
  final weakPwdStrengthRegexp = RegExp(
    r'[\w_-]$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含2个大小写字母
  final middlePwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]{2,}).*$',
  );

  /// 密码强度正则,表示最短6位，必须包含1个数字,必须包含1个小写字母,必须包含1个大写字母,必须包含1个特殊字符
  final strongPwdStrengthRegexp = RegExp(
    r'.*(?=.{6,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*[!@#$%^&*?\(\)]).*$',
  );

  Future<void> doChangge(BuildContext context) async {
    final privateKeyHex = privateKeyEditController.text;
    final mnemonicWords = CoreProvider.of(context).wallet.mnemonic;

    if (privateKeyHex.length <= 0) {
      errorListenable.value = S.current.wallet_import_input_privatekey_empty;
      return;
    } else {
      errorListenable.value = "";
    }

    if (!RegExp(r'^[a-z+\s?]+$').hasMatch(privateKeyHex)) {
      errorListenable.value =
          S.current.wallet_import_privatekey_invalid_formatter;
      return;
    } else if (passWordEditController.text.length <= 0) {
      errorListenable.value =
          S.current.ethereum_setting_change_password_new_tips;
      return;
    } else if (confirmpassWordEditController.text.length <= 0) {
      errorListenable.value = "";
      errorListenable.value =
          S.current.ethereum_setting_change_password_confim_tips;
      return;
    } else if (confirmpassWordEditController.text !=
        passWordEditController.text) {
      errorListenable.value =
          S.current.ethereum_setting_change_password_not_consistent;

      return;
      // ignore: unrelated_type_equality_checks
    } else if (mnemonicWords.trim() != privateKeyEditController.text.trim()) {
      errorListenable.value =
          S.current.ethereum_setting_change_password_incorrect_mnemonics;
    } else {
      errorListenable.value = "";
      CoreProvider.of(context).wallet.securitySHA256 =
          sha256.convert(utf8.encode(passWordEditController.text)).bytes;
      CoreProvider.of(context).wallet.save();
      // Navigator.of(context).push(
      //   ViewAnimations.viewRightIn(
      //     SettingView(),
      //   ),
      // );
      await Future.delayed(Duration(milliseconds: 1000));
      Fluttertoast.showToast(
          msg: S.current.message_box_prompt_password_reset_successful);

      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: NavigationBar(
          title: S.current.ethereum_setting_change_password_forget_the_password,
          elevation: 0.3,
        ),
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                ValueListenableBuilder(
                  valueListenable: privateKeyEditErrorListenable,
                  builder: (context, value, child) => TextField(
                    maxLines: 5,
                    controller: privateKeyEditController,
                    autocorrect: true,
                    decoration: InputDecoration(
                      isCollapsed: true,
                      // fillColor: ThemeUtils().getColor(
                      //     'views.keychain.import.text_field_fill_color'),
                      fillColor:
                          // ThemeUtils().getColor(
                          //     'views.keychain.import.text_field_fill_color'),
                          Color.fromRGBO(250, 250, 250, 1),
                      filled: true,
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(color: Colors.red),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          color:
                              ThemeUtils().getColor('utils.input.border_color'),
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          color:
                              ThemeUtils().getColor('utils.input.border_color'),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          color:
                              ThemeUtils().getColor('utils.input.border_color'),
                        ),
                      ),
                      contentPadding: EdgeInsets.all(
                        ScreenUtil().setWidth(10),
                      ),
                      hintText:
                          S.current.wallet_import_input_privatekey_placeholder,
                      hintStyle: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(175, 175, 175, 1),
                      ),
                      errorText: value,
                    ),
                    keyboardType: TextInputType.visiblePassword,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: ValueListenableBuilder(
                    valueListenable: passWordEditErrorListenable,
                    builder: (context, value, child) => Input(
                      title: "",
                      errorText: value,
                      placeholder: S.current
                          .ethereum_setting_change_password_input_new_tips,
                      controller: passWordEditController,
                      onChanged: (_) {
                        if (weakPwdStrengthRegexp
                                .hasMatch(passWordEditController.text) ==
                            true) {
                          this.passwordStrength =
                              S.current.password_strength_weak;
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        } else {
                          this.passwordStrength = "";
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        }

                        if (middlePwdStrengthRegexp
                                .hasMatch(passWordEditController.text) ==
                            true) {
                          this.passwordStrength =
                              S.current.password_strength_middle;
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        }
                        if (strongPwdStrengthRegexp
                                .hasMatch(passWordEditController.text) ==
                            true) {
                          this.passwordStrength =
                              S.current.password_strength_strength;
                          this.passwordStrengthListenable.value =
                              this.passwordStrength;
                        }
                      },
                    ),
                  ),
                ),
                ValueListenableBuilder(
                  valueListenable: passwordStrengthListenable,
                  builder: (context, value, child) {
                    return this.passwordStrength != ""
                        ? Container(
                            margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(13)),
                            child: PwdIdentifier(
                              passwordStrength: this.passwordStrength,
                            ),
                          )
                        : Container();
                  },
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: ValueListenableBuilder(
                    valueListenable: confirmpassWordEditErrorListenable,
                    builder: (context, value, child) => Input(
                      title: "",
                      errorText: value,
                      placeholder: S.current
                          .ethereum_setting_change_password_input_confim_tips,
                      controller: confirmpassWordEditController,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
                  child: ValueListenableBuilder(
                    valueListenable: errorListenable,
                    builder: (contet, value, child) {
                      return Text(
                        errorListenable.value,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          color: Color.fromRGBO(203, 0, 0, 1),
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
                  child: StatusButton.withStyle(
                    StatusButtonStyle.Info,
                    title: S.current.wallet_import_done,
                    // ignore: missing_return
                    onPressedFuture: () async {
                      await doChangge(context);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
