// @dart=2.9
import 'package:flutter/material.dart';
import 'package:wallet_flutter/command/animations/view.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/theme/icon_utils.dart' as CustomIcons;

import 'graphics_setting.dart';

class GraphicsPage extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;
  const GraphicsPage({this.dataSourceProvider});

  @override
  State<GraphicsPage> createState() => _GraphicsPageState();
}

class _GraphicsPageState extends State<GraphicsPage> {
  final ValueNotifier<bool> isAuth = ValueNotifier<bool>(false);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isAuth.value =
        ConfigStorageUtils().readGestureIndexOfKey(ConfigKey.Gesture);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_setting_gusture,
        elevation: 0.3,
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: ThemeUtils().getColor(
                'content.color',
              ),
            ),
            height: ScreenUtil().setHeight(54),
            child: ListTile(
              contentPadding: EdgeInsets.only(
                // top: ScreenUtil().setHeight(6),
                left: ScreenUtil().setWidth(15),
                right: ScreenUtil().setWidth(20),
                // bottom: ScreenUtil().setHeight(20),
              ),
              minLeadingWidth: ScreenUtil().setWidth(10),
              leading: Container(
                height: ScreenUtil().setHeight(54),
                margin: EdgeInsets.only(),
                child: Icon(
                  CustomIcons.gestureIcon,
                  size: ScreenUtil().setHeight(18.09),
                ),
              ),
              title: Container(
                alignment: Alignment.centerLeft,
                height: ScreenUtil().setHeight(54),
                child: Row(
                  children: [
                    Text(
                      S.current.ethereum_setting_gusture,
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.settings.textstyle.cell_subtitle',
                      ),
                    ),
                  ],
                ),
              ),
              trailing: Container(
                alignment: Alignment.centerRight,
                width: ScreenUtil().setWidth(120),
                child: InkWell(
                  onTap: () async {
                    if (isAuth.value == false) {
                      await Navigator.of(context)
                          .push(
                        ViewAnimations.viewRightIn(
                          SettingPage(
                            dataSourceProvider: widget.dataSourceProvider,
                          ),
                        ),
                      )
                          .then((value) {
                        if (value != null)
                          setState(() {
                            isAuth.value = true;
                          });
                      });
                      ;
                    } else {
                      isAuth.value = false;
                      ConfigStorageUtils()
                          .writeAuthOfKey(ConfigKey.Gesture, false);

                      widget.dataSourceProvider.localService.guslocalClean();
                    }
                    // Navigator.of(context).push(
                    //   ViewAnimations.viewRightIn(
                    //     SettingPage(
                    //       dataSourceProvider: widget.dataSourceProvider,
                    //     ),
                    //   ),
                    // );
                  },
                  child: ValueListenableBuilder(
                    valueListenable: isAuth,
                    builder: (context, value, child) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              right: ScreenUtil().setWidth(8),
                            ),
                            child: Text(
                              isAuth.value == false
                                  ? S.current.ethereum_setting_gusture_off
                                  : S.current.ethereum_setting_gusture_on,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(16),
                                color: Color.fromRGBO(175, 175, 175, 1),
                              ),
                            ),
                          ),
                          Container(
                            child: Image.asset(
                              isAuth.value == false
                                  ? 'assets/images/switch_off.png'
                                  : 'assets/images/switch_on.png',
                              width: ScreenUtil().setWidth(38),
                              height: ScreenUtil().setHeight(19),
                            ),
                          )
                        ],
                      );
                    },
                  ),
                ),
              ),
              // onTap: () {
              //   Navigator.of(context).push(
              //     ViewAnimations.viewRightIn(
              //       FinferprintRemindPage(),
              //     ),
              //   );
              // },
            ),
          )
        ],
      ),
    );
  }
}
