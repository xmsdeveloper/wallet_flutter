// @dart=2.9
import 'package:flutter/material.dart';
import 'package:gesture_recognition/gesture_view.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VerifyPage extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;

  const VerifyPage({Key key, this.dataSourceProvider}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _VerifyState();
  }
}

class _VerifyState extends State<VerifyPage> {
  List<int> curResult = [];
  // List<int> correctResult = [0, 1, 2, 5, 8, 7, 6];
  List<dynamic> correctResult;
  int status = 0; // 0: NONE,1: SUCCESS,2: ERROR
  List<int> onlyShowItems;
  GlobalKey<GestureState> gestureStateKey = GlobalKey();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print(widget.dataSourceProvider.localService.dappHistorys());
    widget.dataSourceProvider.localService.gustureLocals().then((value) {
      setState(() {
        var dedu = new Set(); //用set进行去重
        dedu.addAll(value);

        this.correctResult = dedu.toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.3,
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: Text(
          S.current.ethereum_setting_gusture_verification_title,
          style: TextStyle(
            color: Colors.black,
            fontSize: ScreenUtil().setSp(16),
          ),
        ),
      ),

      // appBar: NavigationBar(
      //   title: "图形认证",
      //   elevation: 0.3,
      // ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: ScreenUtil().setHeight(140),
              width: MediaQuery.of(context).size.width,
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //       image: AssetImage('assets/images/gesture_bg.png'),
              //       fit: BoxFit.fill),
              // ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/gusture_bg.png'),
                    fit: BoxFit.fill),
              ),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(23),
                      bottom: ScreenUtil().setHeight(12),
                    ),
                    height: ScreenUtil().setHeight(62),
                    width: ScreenUtil().setWidth(62),
                    child: Image.asset(
                        // CoreProvider.of(context).account.avatarPath,
                        // package: "wallet_flutter",
                        'assets/images/gesture_logo.png'),
                  ),
                  Container(
                      child: Text(
                    CoreProvider.of(context).wallet.walletName,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                      fontWeight: FontWeight.bold,
                    ),
                  ))
                ],
              ),
            ),
            Center(
              child: GestureView(
                key: this.gestureStateKey,
                // size: MediaQuery.of(context).size.width * 0.8,
                ringWidth: 1,
                ringRadius: ScreenUtil().setHeight(40),
                circleRadius: ScreenUtil().setHeight(15),
                immediatelyClear: true,
                lineWidth: 1,
                showUnSelectRing: false,
                size: MediaQuery.of(context).size.width,
                onPanUp: (List<int> items) {
                  analysisGesture(items);
                  if (status == 1) {
                    Navigator.pop(context);
                  }
                },
                onPanDown: () {
                  gestureStateKey.currentState.selectColor = Colors.blue;
                  setState(() {
                    status = 0;
                  });
                },
              ),
            ),
            Container(
              child: Center(
                child: status == 0
                    ? Text(
                        // "Current: ${curResult.toString()}",

                        S.current.ethereum_setting_gusture_verification_tips,

                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(18),
                            color: Colors.black),
                      )
                    : status == 1
                        ? Text("")
                        : Text(
                            S.current
                                .ethereum_setting_gusture_verification_tips_failed,
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(18),
                                color: Colors.red),
                          ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  analysisGesture(List<int> items) {
    bool isCorrect = true;
    if (items.length == correctResult.length) {
      for (int i = 0; i < items.length; i++) {
        if (items[i] != correctResult[i]) {
          isCorrect = false;
          break;
        }
      }
      gestureStateKey.currentState.selectColor = Colors.blue;
    } else {
      isCorrect = false;
      gestureStateKey.currentState.selectColor = Colors.red;
    }
    setState(() {
      status = isCorrect ? 1 : 2;
      curResult = items;
    });
  }
}
