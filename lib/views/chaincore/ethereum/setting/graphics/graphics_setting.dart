// @dart=2.9
import 'package:flutter/material.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:gesture_recognition/gesture_view.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SettingPage extends StatefulWidget {
  final DataSourceProvider dataSourceProvider;

  const SettingPage({Key key, this.dataSourceProvider}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _SettingState();
  }
}

class _SettingState extends State<SettingPage> {
  List<int> result = [];
  List<int> confirm = [];
  int i = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   leading: IconButton(
      //       icon: Icon(Icons.arrow_back),
      //       onPressed: () => Navigator.of(context).pop()),
      //   title: Text("Setting Gesture"),
      // ),
      appBar: NavigationBar(
        title: S.current.ethereum_setting_gusture_setting_title,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: ScreenUtil().setHeight(140),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/gusture_bg.png'),
                    fit: BoxFit.fill),
              ),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(23),
                      bottom: ScreenUtil().setHeight(12),
                    ),
                    height: ScreenUtil().setHeight(62),
                    width: ScreenUtil().setWidth(62),
                    child: Image.asset('assets/images/gesture_logo.png'
                        // CoreProvider.of(context).account.avatarPath,
                        // package: "wallet_flutter",
                        ),
                  ),
                  Container(
                      child: Text(
                    CoreProvider.of(context).wallet.walletName,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                      fontWeight: FontWeight.bold,
                    ),
                  ))
                ],
              ),
            ),
            Center(
              child: GestureView(
                ringWidth: 1,
                ringRadius: ScreenUtil().setHeight(40),
                circleRadius: ScreenUtil().setHeight(15),
                immediatelyClear: true,
                lineWidth: 1,
                showUnSelectRing: false,
                size: MediaQuery.of(context).size.width,
                onPanUp: (List<int> items) {
                  if (i == 1) {
                    setState(() {
                      result = items;

                      i++;
                      // widget.dataSourceProvider.localService
                      //     .guslocalInsert(result);
                    });
                  } else {
                    setState(
                      () {
                        confirm = items;
                        if (result.length != confirm.length) {
                          Fluttertoast.showToast(
                            msg: S.current
                                .ethereum_setting_gusture_setting_confirm_failed,
                          );
                          return;
                        }
                        for (var i = 0; i < confirm.length; i++) {
                          if (confirm[i] != result[i]) {
                            Fluttertoast.showToast(
                              msg: S.current
                                  .ethereum_setting_gusture_setting_confirm_failed,
                            );
                            return;
                          }
                        }
                        widget.dataSourceProvider.localService
                            .guslocalInsert(confirm);

                        ConfigStorageUtils()
                            .writeGestureIndexOfKey(ConfigKey.Gesture, true);
                        setState(() {});

                        Fluttertoast.showToast(
                          msg: S.current
                              .ethereum_setting_gusture_setting_confirm_success,
                        );
                        Navigator.of(context).pop(true);
                      },
                    );
                  }
                },
              ),
            ),
            Container(
              child: Center(
                child: Text(
                  // "result: ${result.toString()}",
                  this.result.length > 0
                      ? S.current.ethereum_setting_gusture_setting_confirm
                      : S.current.ethereum_setting_gusture_setting_tips,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
