// @dart=2.9
part of '../setting.dart';

class SettingView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SettingViewState();

  static BottomNavigationBarItem barItem() => BottomNavigationBarItem(
        label: S.current.ethereum_setting_baritem_title,
        backgroundColor: ThemeUtils().getColor('utils.bottom_bar.background'),
        icon: Image.asset(
          'assets/imgs/tabs2.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: ThemeUtils().getColor('utils.bottom_bar.normal_item_color'),
        ),
        activeIcon: Image.asset(
          'assets/imgs/tabs2.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: Color.fromRGBO(48, 198, 153, 1),
        ),
      );
}

class _SettingCellDataSource {
  final String identifier;

  final Widget trailing;
  final String title;
  final VoidCallback onTap;
  final IconData icon;

  _SettingCellDataSource({
    @required this.identifier,
    @required this.title,
    @required this.icon,
    this.trailing,
    this.onTap,
  });
}

class _SettingViewState extends State<SettingView> {
  Completer<int> cacheLoadingCompleter = Completer();
  //定义本地版本号
  String localversion;

  //定义网路版本好
  String netVersion;
  String downUrl;
  //更新消息
  String content;

  DateTime redkey = ConfigStorageUtils().readOfKey("notices");

  /// 计算缓存大小
  void loadingCache() async {
    int cacheSize = 0;
    Directory tempDir = await getTemporaryDirectory();
    tempDir.listSync(recursive: true, followLinks: false).forEach((file) {
      cacheSize += file.statSync().size;
    });
    cacheLoadingCompleter.complete(cacheSize);
  }

  /// 清除全部缓存
  void cleanCache() async {
    Directory tempDir = await getTemporaryDirectory();

    tempDir.listSync(recursive: false, followLinks: false).forEach((file) {
      file.deleteSync(recursive: true);
    });

    cacheLoadingCompleter = Completer();
    loadingCache();
    setState(() {});
  }

  _checkVersionInformation() async {
    // 获取数据源版本信息
    var url = Engine.url;
    assert(Engine.url != null);

    // var url = "http://hashpay.gitee.io/hashpay/json/version.json"; //测试下载地址
    var httpClient = new HttpClient();
    // ignore: close_sinks
    var request = await httpClient.getUrl(Uri.parse(url));
    var response = await request.close();
    if (response.statusCode == HttpStatus.ok) {
      var json = await response.transform(utf8.decoder).join();
      var data = jsonDecode(json);

      setState(() {
        this.netVersion = data['version'];

        this.downUrl = data['url'];
        this.content = data['content'];
      });
    }

    //  获取安装的版本号
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    setState(() {
      this.localversion = packageInfo.version;
    });
    if (this.localversion == this.netVersion) {
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return AppVersionPage(
            url: this.downUrl,
            content: this.content,
          );
        },
      );
    }
  }

  List<List<_SettingCellDataSource>> datasource() => [
        [
          _SettingCellDataSource(
            identifier: 'manage_your_wallet',
            icon: CustomIcons.manage_your_wallet,
            title: S.current.ethereum_setting_manage,
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: ScreenUtil().setHeight(15),
            ),
          ),
          _SettingCellDataSource(
            identifier: 'backup_wallet',
            icon: CustomIcons.backup_wallet,
            title: S.current.ethereum_setting_backup_mnemonics,
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: ScreenUtil().setHeight(15),
            ),
          ),
        ],
        [
          _SettingCellDataSource(
            identifier: 'change_password',
            icon: CustomIcons.change_password,
            title: S.current.ethereum_setting_change_password,
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: ScreenUtil().setHeight(15),
            ),
          ),
          _SettingCellDataSource(
            identifier: "network",
            icon: CustomIcons.network,
            title: S.current.ethereum_setting_network_settings,
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: ScreenUtil().setHeight(15),
            ),
          ),
          // _SettingCellDataSource(
          //   identifier: 'language',
          //   icon: CustomIcons.language,
          //   title: S.current.ethereum_setting_language_settings,
          //   trailing: Icon(
          //     Icons.arrow_forward_ios,
          //     size: ScreenUtil().setHeight(15),
          //   ),
          // ),
        ],
        // [
        //   _SettingCellDataSource(
        //     identifier: 'share',
        //     icon: Icons.share,
        //     title: "分享",
        //     trailing: Icon(
        //       Icons.arrow_forward_ios,
        //       size: ScreenUtil().setHeight(15),
        //     ),
        //   ),
        //   _SettingCellDataSource(
        //     identifier: 'Notice',
        //     icon: Icons.notifications,
        //     title: "公告",
        //     trailing: Icon(
        //       Icons.arrow_forward_ios,
        //       size: ScreenUtil().setHeight(15),
        //     ),
        //   ),
        // ],
        [
          _SettingCellDataSource(
            identifier: "clean",
            icon: CustomIcons.clean,

            title: S.current.ethereum_setting_clean_up_cache,
            // trailing: Text('${cach}Mb'),
            trailing: FutureBuilder<int>(
              future: cacheLoadingCompleter.future,
              builder: (context, snapshot) =>
                  snapshot.connectionState != ConnectionState.done
                      ? Text('正在计算...')
                      : Text(
                          '${(snapshot.data / 1024 / 1024).toStringAsFixed(2)} MB',
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.settings.textstyle.cell_title',
                          ),
                        ),
            ),
          ),
          _SettingCellDataSource(
            identifier: 'about',
            icon: CustomIcons.about,
            title: S.current.ethereum_setting_about_us,
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: ScreenUtil().setHeight(15),
            ),
          ),
          // _SettingCellDataSource(
          //   identifier: 'system',
          //   icon: CustomIcons.system,
          //   title: S.current.ethereum_setting_version_number,
          //   trailing: Text(
          //     this.localversion != null ? this.localversion : "1.0.8",
          //     style: ThemeUtils().getTextStyle(
          //       'views.chaincore.ethereum.settings.textstyle.cell_title',
          //     ),
          //   ),
          // ),
        ],
      ];

  @override
  void initState() {
    super.initState();

    CoreProvider.of(context).wallet;

    loadingCache();

    // showDialog(
    //   context: context,
    //   builder: (context) {
    //     return FreezePage();
    //   },
    // );

    //初始化调用获取版本号接口
    // _getPackageInfo();

    // _getVersionInformation();

    _checkVersionInformation();
  }

  Widget buildCell(BuildContext context, _SettingCellDataSource data, wallet) =>
      Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: ThemeUtils().getColor(
                'content.color',
              ),
            ),
            height: ScreenUtil().setHeight(54),
            child: ListTile(
              contentPadding: EdgeInsets.only(
                top: ScreenUtil().setHeight(6),
                left: ScreenUtil().setWidth(15),
                right: ScreenUtil().setWidth(20),
                // bottom: ScreenUtil().setHeight(20),
              ),

              minLeadingWidth: ScreenUtil().setWidth(10),
              leading: Container(
                  margin: EdgeInsets.only(),
                  child: Icon(
                    data.icon,
                    size: ScreenUtil().setHeight(25),
                    // color: ThemeUtils().getColor(
                    //   'views.chaincore.ethereum.settings.header_background',
                    // ),
                  )
                  // data.leading,
                  ),
              title: Row(
                children: [
                  Text(
                    data.title,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.settings.textstyle.cell_subtitle',
                    ),
                  ),
                ],
              ),
              trailing: data.trailing,
              // onTap: () => cellOnTap(context, section, row),
              onTap: () {
                if (data.identifier == 'language') {
                  Navigator.of(context).push(
                    ViewAnimations.viewRightIn(
                      LanguagePage(),
                    ),
                  );
                } else if (data.identifier == 'network') {
                  Navigator.of(context).push(
                    ViewAnimations.viewRightIn(
                      NetworkPage(),
                    ),
                  );
                } else if (data.identifier == 'clean') {
                  ActionSheet.showConfirm(
                    context,
                    title: S.current.sheet_title,
                    description: S.current.sheet_descration,
                    onConfirm: () async {
                      cleanCache();
                      Navigator.pop(context);
                      setState(() {});
                    },
                    onCancel: () {
                      Navigator.pop(context);
                    },
                  );
                } else if (data.identifier == 'manage_your_wallet') {
                  WalletSelector.show(context: context);
                } else if (data.identifier == 'backup_wallet') {
                  VerifySheet.show(
                    context,
                    verifyCallback: (context, pwd) {
                      final verifySuccess =
                          CoreProvider.of(context).wallet.verifySecurity(pwd);
                      if (!verifySuccess) {
                        return S.current.password_verify_faild;
                      } else {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.of(context).pop();
                        Navigator.of(context, rootNavigator: true).push(
                          ViewAnimations.viewRightIn(
                            PrivateKeyDisplay.rootWallet(
                              encryptPassword: pwd,
                              wallet: CoreProvider.of(context).wallet,
                            ),
                          ),
                        );
                        return null;
                      }
                    },
                  );
                } else if (data.identifier == 'system') {
                  // Navigator.of(context).push(
                  //   ViewAnimations.viewRightIn(
                  //     AppVersionPage(),
                  //   ),
                  // );
                  //回调触发弹框

                  //Future类型,then或者await获取
                  if (this.localversion == this.netVersion) {
                    Fluttertoast.showToast(
                      msg: S.current.ethereum_setting_updata_tips,
                      gravity: ToastGravity.CENTER,
                    );
                  } else {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AppVersionPage(
                          url: this.downUrl,
                          content: this.content,
                        );
                      },
                    );
                  }
                } else if (data.identifier == 'change_password') {
                  Navigator.of(context).push(
                    ViewAnimations.viewRightIn(ChangePassword()),
                  );
                } else if (data.identifier == 'about') {
                  Navigator.of(context).push(
                    ViewAnimations.viewRightIn(
                      AboutPage(
                        localversion: this.localversion,
                        netVersion: this.netVersion,
                        downUrl: this.downUrl,
                        content: this.content,
                        web3clientProvider: Web3ClientProvider.of(context),
                        dataSourceProvider: DataSourceProvider.of(context),
                      ),
                    ),
                  );
                } else if (data.identifier == 'share') {
                  // Navigator.of(context).push(
                  //   ViewAnimations.viewRightIn(
                  //     SharePage(),
                  //   ),
                  // );
                  Fluttertoast.showToast(msg: "暂未开放");
                } else if (data.identifier == 'Notice') {
                  // ConfigStorageUtils()
                  //     .writeOfKey("notices", defaultValue: DateTime.now());
                  // setState(() {});

                  // openDapp(
                  //   context,
                  //   Dapp(
                  //     url: "http://web.radarlab.me/radar_machine/#/notice",
                  //     logo: "",
                  //   ),
                  // );
                  Fluttertoast.showToast(msg: "暂未开放");
                }
              },
            ),
          ),
          Container(
            height: 1,
            color: ThemeUtils().getColor(
              'page.background',
            ),
            margin: EdgeInsets.only(left: ScreenUtil().setWidth(36)),
          ),
        ],
      );
  @override
  Widget build(BuildContext context) => ValueListenableBuilder<Wallet>(
        valueListenable: CoreProvider.of(context).walletListenable,
        builder: (context, wallet, child) => Scaffold(
          appBar: NavigationBar(
            title: S.current.ethereum_setting_title,
          ),
          backgroundColor: ThemeUtils().getColor('page.background'),
          // backgroundColor: ThemeUtils().getColor(
          //   'utils.view_controller.background',
          // ),
          body: CustomScrollView(
            physics: BouncingScrollPhysics(),
            slivers: [
              // SliverPersistentHeader(
              //   delegate:
              //       SliverViewControllerHeader(child: sliverViewHeader(wallet)),
              // ),
            ]..addAll(
                datasource()
                    .map(
                      (section) => SliverPadding(
                        padding: EdgeInsets.only(
                          bottom: ScreenUtil().setHeight(15),
                        ),
                        sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) =>
                                buildCell(context, section[index], wallet),
                            childCount: section.length,
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
          ),
        ),
      );

  PreferredSize sliverViewHeader(wallet) => PreferredSize(
        preferredSize: Size(double.infinity, ScreenUtil().setHeight(210)),
        child: Container(
          color: ThemeUtils()
              .getColor('views.chaincore.ethereum.assets.header_background'),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: ScreenUtil().setHeight(40)),
              Image.asset(
                'assets/imgs/default_wallet.png',
                package: 'wallet_flutter',
                fit: BoxFit.fill,
                width: ScreenUtil().setWidth(56),
                height: ScreenUtil().setWidth(56),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(20),
                  // bottom: ScreenUtil().setHeight(60),
                ),
                child: Text(
                  // 'Wallet Name',
                  // wallet.walletName ?? 'noName',
                  CoreProvider.of(context).wallet.walletName,

                  style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.assets.textstyle.wallet_name'),
                ),
              ),
            ],
          ),
        ),
      );

  void openDapp(BuildContext context, Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: Web3ClientProvider.of(context),
          dataSourceProvider: DataSourceProvider.of(context),
        ),
      ),
    );
  }
}
