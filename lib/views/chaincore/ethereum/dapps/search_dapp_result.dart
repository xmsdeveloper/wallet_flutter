// @dart=2.9
part of '../dapps.dart';

// ignore: must_be_immutable
class _SearchResult extends StatefulWidget {
  final Ethereum.DataSourceProvider dataSourceProvider;
  Ethereum.ResponseList<Ethereum.Dapp> searchDappList;
  final Web3ClientProvider web3clientProvider;

  _SearchResult({
    @required this.dataSourceProvider,
    @required this.searchDappList,
    @required this.web3clientProvider,
  });
  @override
  State<StatefulWidget> createState() => _SearchResultState();
}

class _SearchResultState extends State<_SearchResult> {
  @override
  Widget build(BuildContext context) {
    return this.widget.searchDappList != null
        ? this.widget.searchDappList.errorCode == 0
            ? Column(
                children: this
                    .widget
                    .searchDappList
                    .data
                    .map((e) => InkWell(
                          onTap: () {
                            openDapp(
                              context,
                              Ethereum.Dapp(
                                name: e.name,
                                url: e.url,
                                logo: e.logo,
                              ),
                            );
                            this
                                .widget
                                .dataSourceProvider
                                .localService
                                .dappHistoryInsert(e.url);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: ThemeUtils().getColor(
                                  'views.chaincore.ethereum.assets.cell_background'),
                            ),
                            margin: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(2),
                            ),
                            height: ScreenUtil().setHeight(75),
                            child: Row(
                              children: [
                                Container(
                                  width: ScreenUtil().setWidth(40),
                                  height: ScreenUtil().setHeight(40),
                                  margin: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(20),
                                    right: ScreenUtil().setWidth(10),
                                  ),
                                  child: Image.network(e.logo),
                                  //     CachedImage.assetNetwork(
                                  //   url: e.logo,
                                  //   placeholder: Image.asset(
                                  //     'assets/imgs/default_dapp.png',
                                  //     package: 'wallet_flutter',
                                  //   ),
                                  //   width: ScreenUtil().setWidth(40),
                                  //   height: ScreenUtil().setWidth(40),
                                  //   imageCacheHeight: 100,
                                  //   imageCacheWidth: 100,
                                  // ),
                                ),
                                Container(
                                  width: ScreenUtil().setWidth(200),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        e.name,
                                        // e['mum'],
                                        style: ThemeUtils().getTextStyle(
                                          'views.chaincore.ethereum.dapp.textstyle.dapp_name',
                                        ),
                                      ),
                                      Text(
                                        e.des,
                                        maxLines: 1,
                                        style: ThemeUtils().getTextStyle(
                                          'views.chaincore.ethereum.dapp.textstyle.dapp_desc',
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ))
                    .toList(),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(200),
                      bottom: ScreenUtil().setHeight(15),
                    ),
                    width: ScreenUtil().setWidth(46),
                    height: ScreenUtil().setHeight(46),
                    child: Image.asset(
                      'assets/imgs/personal_protocol.png',
                      package: 'wallet_flutter',
                      color: ThemeUtils().getColor(
                        'views.chaincore.ethereum.dapp_searchPage.unmatch_icon',
                      ),
                    ),
                  ),
                  Container(
                    child: Center(
                      child: Text(S.current.ethereum_dapp_search_tips,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.dapp_searchPage.textstyle.search_tips',
                          )),
                    ),
                  ),
                ],
              )
        : Container();
  }

  void openDapp(BuildContext context, Ethereum.Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          dataSourceProvider: widget.dataSourceProvider,
          web3clientProvider: widget.web3clientProvider,
        ),
      ),
    );
  }
}
