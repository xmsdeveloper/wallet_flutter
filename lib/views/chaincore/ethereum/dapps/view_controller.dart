// @dart=2.9
part of '../dapps.dart';

class DappsView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DappsViewState();

  static BottomNavigationBarItem barItem() => BottomNavigationBarItem(
        label: S.current.ethereum_dapp_baritem_title,
        backgroundColor: ThemeUtils().getColor('utils.bottom_bar.background'),
        icon: Image.asset(
          'assets/imgs/tabs1.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: ThemeUtils().getColor('utils.bottom_bar.normal_item_color'),
        ),
        activeIcon: Image.asset(
          'assets/imgs/tabs1.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: Color.fromRGBO(48, 198, 153, 1),
        ),
      );
}

class _DappsViewState extends State<DappsView> {
  final ScrollController gridController = ScrollController();
  double scrollViewStartOffset;
  @override
  void initState() {
    super.initState();
    // final account = ConfigStorageUtils().readaccount(ConfigKey.Selecteaccount);

    // CoreProvider.of(context).account = account;

    CoreProvider.of(context).accountListenable.addListener(() async {
      bool isblack = await Web3ClientProvider.of(context)
          .isBlackAddress(CoreProvider.of(context).account.address);
      if (isblack) {
        showDialog(
          context: context,
          builder: (context) {
            return FreezePage();
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        // appBar: NavigationBar(
        //   title: S.current.ethereum_dapp_baritem_title,
        // ),
        body: ValueListenableBuilder<
            Ethereum.ResponseObject<Ethereum.DappIndexResponse>>(
          valueListenable: Ethereum.DataSourceProvider.of(context)
              .remoteServices
              .dapps
              .indexListnable(),
          builder: (context, response, child) => response == null
              ? Container()
              : CustomScrollView(
                  physics: const AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics(),
                  ),
                  slivers: [
                    /// Banner
                    SliverPersistentHeader(
                      delegate: SliverViewControllerHeader(
                          child: sliverViewHeader(response)),
                    ),
                  ]..addAll(
                      /// DappTypeList
                      response.data.dapps
                          .map(
                            (typeList) => SliverPadding(
                              padding: EdgeInsets.only(
                                top: ScreenUtil().setHeight(20),
                              ),
                              sliver: SliverToBoxAdapter(
                                child: typeList.items.length > 0
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                              horizontal:
                                                  ScreenUtil().setWidth(20),
                                              vertical:
                                                  ScreenUtil().setHeight(10),
                                            ),
                                            child: Text(
                                              typeList.type,
                                              style: ThemeUtils().getTextStyle(
                                                'views.chaincore.ethereum.dapp.textstyle.dapp_type',
                                              ),
                                            ),
                                          ),
                                          GridPageView(
                                            dataSourceDelegate:
                                                GridPageViewDataSourceDelegate(
                                              columCount:
                                                  typeList.items.length < 4
                                                      ? typeList.items.length
                                                      : 4,
                                              pageWidth:
                                                  ScreenUtil().screenWidth *
                                                      0.85,
                                              pageHeight: ScreenUtil()
                                                  .setHeight(
                                                      (typeList.items.length < 4
                                                              ? typeList
                                                                  .items.length
                                                              : 4) *
                                                          75),
                                              itemBuilder: (context, index) =>
                                                  ListTile(
                                                dense: true,
                                                hoverColor: Colors.red,
                                                minLeadingWidth:
                                                    ScreenUtil().setWidth(7),
                                                leading: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child:
                                                      CachedImage.assetNetwork(
                                                    placeholder: Image.asset(
                                                      'assets/imgs/default_dapp.png',
                                                      package: 'wallet_flutter',
                                                    ),
                                                    url: typeList
                                                        .items[index].logo,
                                                    width: ScreenUtil()
                                                        .setHeight(40),
                                                    height: ScreenUtil()
                                                        .setHeight(40),
                                                    imageCacheWidth: 100,
                                                    imageCacheHeight: 100,
                                                  ),
                                                ),

                                                // Container(
                                                //     height: ScreenUtil()
                                                //         .setHeight(40),
                                                //     width: ScreenUtil()
                                                //         .setWidth(40),
                                                //     decoration:
                                                //         BoxDecoration(
                                                //       color: Colors.white,
                                                //       borderRadius:
                                                //           BorderRadius
                                                //               .all(
                                                //         Radius.circular(
                                                //             5.0),
                                                //       ),
                                                //     ),
                                                //     child: Row(
                                                //       mainAxisAlignment:
                                                //           MainAxisAlignment
                                                //               .center,
                                                //       children: [
                                                //         CachedImage
                                                //             .assetNetwork(
                                                //           placeholder:
                                                //               Image.asset(
                                                //             'assets/imgs/default_dapp.png',
                                                //             package:
                                                //                 'wallet_flutter',
                                                //           ),
                                                //           url: typeList
                                                //               .items[
                                                //                   index]
                                                //               .logo,
                                                //           width: ScreenUtil()
                                                //               .setHeight(
                                                //                   40),
                                                //           height:
                                                //               ScreenUtil()
                                                //                   .setHeight(
                                                //                       40),
                                                //           imageCacheWidth:
                                                //               100,
                                                //           imageCacheHeight:
                                                //               100,
                                                //         ),
                                                //       ],
                                                //     ),),
                                                title: Text(
                                                  typeList.items[index].name,
                                                  style:
                                                      ThemeUtils().getTextStyle(
                                                    'views.chaincore.ethereum.dapp.textstyle.dapp_name',
                                                  ),
                                                ),
                                                subtitle: Text(
                                                  typeList.items[index].des ??
                                                      "",
                                                  maxLines: 2,
                                                  style:
                                                      ThemeUtils().getTextStyle(
                                                    'views.chaincore.ethereum.dapp.textstyle.dapp_desc',
                                                  ),
                                                ),
                                                onTap: () {
                                                  if (typeList
                                                          .items[index].url !=
                                                      "") {
                                                    openDapp(
                                                      context,
                                                      typeList.items[index],
                                                    );
                                                  } else {
                                                    Fluttertoast.showToast(
                                                        msg: "暂未开放");
                                                  }
                                                },
                                              ),
                                              itemCount: typeList.items.length,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(33),
                                              right: ScreenUtil().setWidth(32),
                                            ),
                                            height: 1,
                                            color: Color.fromRGBO(
                                                241, 241, 241, 1),
                                          ),
                                        ],
                                      )
                                    : Container(),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                ),
        ),
      );

  void openDapp(BuildContext context, Ethereum.Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: Web3ClientProvider.of(context),
          dataSourceProvider: Ethereum.DataSourceProvider.of(context),
        ),
      ),
    );
  }

  PreferredSize sliverViewHeader(response) => PreferredSize(
        preferredSize: Size(double.infinity, ScreenUtil().setHeight(240)),
        child: Container(
          height: ScreenUtil().setHeight(222),
          child: Stack(
            children: [
              Container(
                height: ScreenUtil().setHeight(200),
                child: Swiper(
                  itemHeight: ScreenUtil().setHeight(200),
                  itemBuilder: (context, index) => Image.network(
                    response.data.banners[index].image,
                    // 'https://t7.baidu.com/it/u=3902551096,3717324701&fm=193&f=GIF',
                    fit: BoxFit.fill,
                  ),
                  itemCount: response.data.banners.length,
                ),
              ),

              /// Search TextFormField
              Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(178),
                  left: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                ),
                height: ScreenUtil().setHeight(44),
                child: Material(
                  elevation: 3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(
                      ScreenUtil().setWidth(22),
                    ),
                  ),
                  color: Colors.transparent,
                  child: TextFormField(
                    onTap: () {
                      Navigator.of(context).push(
                        ViewAnimations.viewRightIn(
                          SearchPage(
                            web3clientProvider: Web3ClientProvider.of(context),
                            dataSourceProvider:
                                Ethereum.DataSourceProvider.of(context),
                          ),
                        ),
                      );
                    },
                    readOnly: true,
                    style: ThemeUtils()
                        .getTextStyle(
                          'utils.input.textstyle.content',
                        )
                        .copyWith(fontSize: 15),
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      contentPadding: EdgeInsets.all(0),
                      hintText:
                          S.current.ethereum_home_address_selector_placeholder,
                      hintStyle: ThemeUtils().getTextStyle(
                        'utils.input.textstyle.hit',
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(
                            ScreenUtil().setWidth(22),
                          ),
                        ),
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(
                            ScreenUtil().setWidth(22),
                          ),
                        ),
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(
                            ScreenUtil().setWidth(22),
                          ),
                        ),
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      prefixIconConstraints: BoxConstraints.expand(
                        width: ScreenUtil().setWidth(50),
                        height: ScreenUtil().setHeight(40),
                      ),
                      prefixIcon: Icon(
                        Icons.search_rounded,
                        // color: ThemeUtils().getColor(
                        //   'views.chaincore.ethereum.address_selector.section_header_search',
                        // ),
                        color: Colors.grey,
                      ),
                      suffixIconConstraints: BoxConstraints.expand(
                        width: ScreenUtil().setWidth(50),
                        height: ScreenUtil().setHeight(40),
                      ),
                      // suffixIcon: Icon(
                      //   Icons.crop_free_rounded,
                      //   color: ThemeUtils().getColor(
                      //     'views.chaincore.ethereum.address_selector.section_header_search',
                      //   ),
                      // ),
                    ),
                    onFieldSubmitted: (url) {
                      if (url != " ") {
                        openDapp(
                          context,
                          Ethereum.Dapp(
                            url: url,
                          ),
                        );
                      } else {
                        Fluttertoast.showToast(msg: "暂未开放");
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
