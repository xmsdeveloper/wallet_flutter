// @dart=2.9
part of '../dapps.dart';

class DappContentView extends StatefulWidget {
  final Ethereum.Dapp dapp;
  final Wallet wallet;
  final WalletKey account;
  final NetworkConfig networkConfig;
  final Web3ClientProvider web3clientProvider;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final bool isTips;
  //是否是网址
  final exp = new RegExp(r'^((https|http|)?:\/\/)[^\s]+');

  DappContentView({
    @required this.dapp,
    @required this.wallet,
    @required this.account,
    @required this.networkConfig,
    @required this.web3clientProvider,
    @required this.dataSourceProvider,
    this.isTips,
  }) {
    assert(this.dapp != null);
    assert(this.wallet != null);
    assert(this.account != null);
    assert(this.networkConfig != null);
    // assert(web3clientProvider != null);
    // assert(dataSourceProvider != null);
  }

  @override
  State<StatefulWidget> createState() => _DappContentViewState();
}

class _DappContentViewState extends State<DappContentView>
    implements Ethereum.Delegate {
  Completer<Ethereum.WebViewController> webViewControllerCompeter = Completer();

  void bottomSheetShow(BuildContext context) {
    final actions = [
      // {
      //   'icon': Icons.wallet_giftcard,
      //   'title': "钱包",
      //   'onTap': () {
      //     Future.delayed(Duration(milliseconds: 500), () {
      //       WalletSelector.show(context: context, isdapp: true);
      //     });
      //     setState(() {});
      //   }
      // },
      {
        'icon': Icons.refresh_rounded,
        'title': S.current.ethereum_dapp_menu_refresh,
        'onTap': () {
          webViewControllerCompeter.future.then((value) {
            value.reload();
            setState(() {});
          });
        }
      },
      {
        'icon': Icons.copy_rounded,
        'title': S.current.ethereum_dapp_menu_copy,
        'onTap': () {
          webViewControllerCompeter.future.then(
            (value) async {
              await Clipboard.setData(
                ClipboardData(
                  text: await value.currentUrl(),
                ),
              );

              Fluttertoast.showToast(
                msg: S.current.copy_success,
              );
            },
          );
        }
      },
      {
        'icon': Icons.share_outlined,
        'title': S.current.ethereum_dapp_menu_share,
        'onTap': () {
          // Share.share(this.widget.dapp.url);
          FlutterShare.share(
              title: 'share',
              linkUrl: this.widget.dapp.url,
              chooserTitle: 'share');
        }
      },
      {
        'icon': Icons.open_in_browser_outlined,
        'title': S.current.ethereum_dapp_menu_browser_open,
        'onTap': () async {
          await FlutterWebBrowser.openWebPage(
            url: this.widget.dapp.url,
            customTabsOptions: CustomTabsOptions(
              colorScheme: CustomTabsColorScheme.dark,
              toolbarColor: Colors.deepPurple,
              secondaryToolbarColor: Colors.green,
              navigationBarColor: Colors.amber,
              addDefaultShareMenuItem: true,
              instantAppsEnabled: true,
              showTitle: true,
              urlBarHidingEnabled: true,
            ),
            safariVCOptions: SafariViewControllerOptions(
              barCollapsingEnabled: true,
              preferredBarTintColor: Colors.green,
              preferredControlTintColor: Colors.amber,
              dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
              modalPresentationCapturesStatusBarAppearance: true,
            ),
          );
        }
      },
      {
        'icon': Icons.collections_bookmark_rounded,
        'title': S.current.ethereum_dapp_collection_mycollection,
        'onTap': () {
          webViewControllerCompeter.future.then(
            (value) async {
              // await Clipboard.setData(
              //   ClipboardData(
              //     text: await value.currentUrl(),
              //   ),
              // );
              if (this.widget.dapp.logo != null) {
                await this
                    .widget
                    .dataSourceProvider
                    .localService
                    .dappCollectionInsert(DappDetail(
                      des: this.widget.dapp.des,
                      url: this.widget.dapp.url,
                      name: this.widget.dapp.name,
                      logo: this.widget.dapp.logo,
                      isSelect: false,
                      type: "all",
                    ));
                setState(() {});
                // await this
                //     .widget
                //     .dataSourceProvider
                //     .localService
                //     .dappCollectionDelete(this.widget.dapp.url);
              }

              Fluttertoast.showToast(
                msg: S.current.ethereum_dapp_collection_collection_success,
              );
            },
          );
        }
      },
      // {
      //   'icon': Icons.paste,
      //   'title': S.current.ethereum_dapp_menu_browser_tp,
      //   'onTap': () {
      //     Fluttertoast.showToast(
      //       msg: S.current.message_box_prompt_password_not_yet_open,
      //     );
      //   }
      // },
      // {
      //   'icon': Icons.report_gmailerrorred_rounded,
      //   'title': S.current.ethereum_dapp_menu_browser_report,
      //   'onTap': () {
      //     Fluttertoast.showToast(
      //       msg: S.current.message_box_prompt_password_not_yet_open,
      //     );
      //   }
      // },
    ];

    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            color: ThemeUtils().getColor(
              'views.chaincore.ethereum.dapp.bottom_action_background',
            ),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(
                    ScreenUtil().setWidth(20),
                    ScreenUtil().setWidth(5),
                    0,
                    ScreenUtil().setWidth(5),
                  ),
                  child: Row(
                    children: [
                      RegExp(r'^((https|http|)?:\/\/)[^\s]+')
                                  .hasMatch(widget.dapp.logo) ||
                              widget.dapp.logo == ""
                          ? CachedImage(
                              url: widget.dapp.logo,
                              placeholder: Image.asset(
                                'assets/imgs/default_dapp.png',
                                package: 'wallet_flutter',
                              ),
                              width: ScreenUtil().setWidth(30),
                              height: ScreenUtil().setWidth(30),
                            )
                          : Container(
                              width: ScreenUtil().setWidth(30),
                              height: ScreenUtil().setWidth(30),
                              child: Image.asset(widget.dapp.logo),
                            ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        widget.dapp.name ?? widget.dapp.url,
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.dapp.textstyle.bottom_action_header',
                        ),
                      ),
                      Expanded(child: SizedBox()),
                      IconButton(
                        padding: EdgeInsets.zero,
                        iconSize: 20,
                        icon: Icon(
                          Icons.close,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                ),
                Divider(
                  height: 2,
                  color: ThemeUtils().getColor(
                    'views.chaincore.ethereum.dapp.bottom_action_divider',
                  ),
                ),
                GridView.builder(
                  padding: EdgeInsets.fromLTRB(
                    0,
                    ScreenUtil().setWidth(20),
                    0,
                    ScreenUtil().setWidth(20),
                  ),
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    // crossAxisSpacing: 10,
                    // mainAxisSpacing: 10,
                  ),
                  itemBuilder: (context, index) => Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black38,
                              blurRadius: 1,
                              offset: Offset(1, 1),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.circular(8),
                        ),
                        child: IconButton(
                          icon: Icon(
                            actions[index]['icon'],
                            size: ScreenUtil().setWidth(24),
                            color: ThemeUtils().getColor(
                              'views.chaincore.ethereum.dapp.bottom_action_icon_color',
                            ),
                          ),
                          onPressed: () {
                            (actions[index]['onTap'] as Function)();
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(6),
                      ),
                      Text(
                        actions[index]['title'],
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.dapp.textstyle.bottom_action_title',
                        ),
                      ),
                    ],
                  ),
                  itemCount: actions.length,
                  shrinkWrap: true,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String result;
  String appbarshow = "true";
  bool isOpen = true;

  void show() {
    showDialog(
      // 弹框外去除背景色
      barrierColor: Colors.transparent,
      context: context,
      barrierDismissible: false,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) {
          return WillPopScope(
              // 禁止 back 按键
              onWillPop: () async => false,
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: ScreenUtil().setHeight(60),
                      color: Colors.transparent,
                    ),
                  ),
                  AlertDialog(
                    // 移除阴影
                    elevation: 0.0,
                    // 弹框去除背景色
                    backgroundColor: Colors.transparent,
                    content: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Colors.transparent,
                      ),
                      // height: ScreenUtil().setHeight(209),

                      // width: ScreenUtil().setWidth(220),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // SizedBox(
                          //   width: 32,
                          //   height: 32,
                          //   child: LineSpinFadeLoaderIndicator(
                          //     ballColor: Colors.blue,
                          //   ),
                          // ),
                          // Image.asset(
                          //   'assets/images/loading.gif',
                          //   width: ScreenUtil().setWidth(53),
                          //   height: ScreenUtil().setHeight(53),
                          // ),
                          widget.dapp.logo != ""
                              ? Container(
                                  margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(216),
                                  ),
                                  child: Image.network(
                                    widget.dapp.logo,
                                    width: ScreenUtil().setWidth(50),
                                    height: ScreenUtil().setHeight(50),
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(216),
                                  ),
                                  child: Image.asset(
                                    "assets/images/dapp_default.png",
                                    width: ScreenUtil().setWidth(50),
                                    height: ScreenUtil().setHeight(50),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                          Container(
                            height: ScreenUtil().setHeight(20),
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(6),
                            ),
                            child: Text(
                              widget.dapp.name ?? "",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                                color: Color.fromRGBO(175, 175, 175, 1),
                              ),
                            ),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(20),
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(11),
                            ),
                            child: Text(
                              S.current.ethereum_dapp_search_loading,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(16),
                                color: Color.fromRGBO(0, 0, 0, 1),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ));
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    // this.widget.dataSourceProvider.localService.dappDetailInsert(DappDetail());
    // 隐藏底部按钮栏
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);

// 隐藏状态栏
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    // SystemChrome.setPreferredOrientations(
    //     [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

    if (this.widget.dapp.url.contains("horizontal=true")) {
      // 强制横屏
      SystemChrome.setPreferredOrientations(
          [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    }
    // widget.isTips == null &&
    //         ConfigStorageUtils()
    //                 .readIsDappTipsIndexOfKey(ConfigKey.isDappTips) ==
    //             true
    //     ?
    // this.isOpen == true
    //     ? Future.delayed(Duration(milliseconds: 500), () {
    //         showModalBottomSheet(
    //             context: context,
    //             backgroundColor: Colors.transparent,
    //             builder: (context) => ListView(
    //                   children: [
    //                     Column(
    //                       mainAxisAlignment: MainAxisAlignment.end,
    //                       children: [
    //                         Container(
    //                           alignment: Alignment.bottomCenter,
    //                           height: ScreenUtil().setHeight(500),
    //                           width: MediaQuery.of(context).size.width,
    //                           // color: ThemeUtils().getColor(
    //                           //   'views.chaincore.ethereum.dapp.bottom_action_background',
    //                           // ),

    //                           decoration: BoxDecoration(
    //                               color: Colors.grey[100],
    //                               borderRadius: BorderRadius.only(
    //                                 topLeft: Radius.circular(15),
    //                                 topRight: Radius.circular(15),
    //                               )),
    //                           child: Column(
    //                             children: [
    //                               Container(
    //                                 margin: EdgeInsets.fromLTRB(
    //                                   ScreenUtil().setWidth(20),
    //                                   ScreenUtil().setWidth(15),
    //                                   0,
    //                                   ScreenUtil().setWidth(15),
    //                                 ),
    //                                 child: Center(
    //                                   child: Text(S.current
    //                                       .ethereum_dapp_access_instructions),
    //                                 ),
    //                               ),
    //                               Container(
    //                                 decoration: BoxDecoration(
    //                                   color: Colors.white,
    //                                   borderRadius: BorderRadius.circular(15),
    //                                 ),
    //                                 height: ScreenUtil().setHeight(260),
    //                                 width: MediaQuery.of(context).size.width -
    //                                     ScreenUtil().setWidth(30),
    //                                 child: Column(
    //                                   mainAxisAlignment:
    //                                       MainAxisAlignment.spaceEvenly,
    //                                   children: [
    //                                     RegExp(r'^((https|http|)?:\/\/)[^\s]+')
    //                                                 .hasMatch(
    //                                                     widget.dapp.logo) ||
    //                                             widget.dapp.logo == ""
    //                                         ? CachedImage(
    //                                             url: widget.dapp.logo,
    //                                             placeholder: Image.asset(
    //                                               'assets/imgs/default_dapp.png',
    //                                               package: 'wallet_flutter',
    //                                             ),
    //                                             width:
    //                                                 ScreenUtil().setWidth(30),
    //                                             height:
    //                                                 ScreenUtil().setWidth(30),
    //                                           )
    //                                         : Container(
    //                                             width:
    //                                                 ScreenUtil().setWidth(30),
    //                                             height:
    //                                                 ScreenUtil().setWidth(30),
    //                                             child: Image.asset(
    //                                                 widget.dapp.logo),
    //                                           ),
    //                                     Container(
    //                                       child: Text(
    //                                         S.current.ethereum_dapp_title,
    //                                         style: TextStyle(
    //                                           fontSize: ScreenUtil().setSp(16),
    //                                         ),
    //                                       ),
    //                                     ),
    //                                     Container(
    //                                       margin: EdgeInsets.only(
    //                                         left: ScreenUtil().setWidth(30),
    //                                         right: ScreenUtil().setWidth(30),
    //                                       ),
    //                                       child: Text(
    //                                         S.current.ethereum_dapp_describe,
    //                                         style: TextStyle(
    //                                             fontSize:
    //                                                 ScreenUtil().setSp(14),
    //                                             color: Colors.black54),
    //                                       ),
    //                                     ),
    //                                   ],
    //                                 ),
    //                               ),
    //                               // Container(
    //                               //   alignment: Alignment.topLeft,
    //                               //   margin: EdgeInsets.all(ScreenUtil().setHeight(30)),
    //                               //   child: Text(
    //                               //     '下次不再提示',
    //                               //     style: TextStyle(
    //                               //       color: Colors.grey,
    //                               //     ),
    //                               //   ),
    //                               // ),
    //                               Row(
    //                                 mainAxisAlignment:
    //                                     MainAxisAlignment.spaceAround,
    //                                 children: [
    //                                   InkWell(
    //                                     onTap: () {
    //                                       Navigator.pop(context);
    //                                       Navigator.pop(context);
    //                                     },
    //                                     child: Container(
    //                                       margin: EdgeInsets.only(
    //                                         top: ScreenUtil().setHeight(30),
    //                                         bottom: ScreenUtil().setHeight(30),
    //                                       ),
    //                                       height: ScreenUtil().setHeight(46),
    //                                       width: MediaQuery.of(context)
    //                                                   .size
    //                                                   .width /
    //                                               2 -
    //                                           ScreenUtil().setWidth(30),
    //                                       decoration: BoxDecoration(
    //                                         color: Colors.blue[100],
    //                                         borderRadius:
    //                                             BorderRadius.circular(8),
    //                                       ),
    //                                       child: Center(
    //                                         child: Text(
    //                                           S.current.ethereum_dapp_back,
    //                                           style: TextStyle(
    //                                             color: Colors.blue,
    //                                             fontSize:
    //                                                 ScreenUtil().setSp(18),
    //                                           ),
    //                                         ),
    //                                       ),
    //                                     ),
    //                                   ),
    //                                   InkWell(
    //                                     onTap: () {
    //                                       ConfigStorageUtils()
    //                                           .writeIsDappTipsIndexOfKey(
    //                                               ConfigKey.isDappTips, false);
    //                                       Navigator.pop(context);
    //                                     },
    //                                     child: Container(
    //                                       margin: EdgeInsets.only(
    //                                         top: ScreenUtil().setHeight(30),
    //                                         bottom: ScreenUtil().setHeight(30),
    //                                       ),
    //                                       height: ScreenUtil().setHeight(46),
    //                                       width: MediaQuery.of(context)
    //                                                   .size
    //                                                   .width /
    //                                               2 -
    //                                           ScreenUtil().setWidth(30),
    //                                       decoration: BoxDecoration(
    //                                         color: Colors.blue,
    //                                         borderRadius:
    //                                             BorderRadius.circular(8),
    //                                       ),
    //                                       child: Center(
    //                                         child: Text(
    //                                           S.current.sheet_verify_verifing,
    //                                           style: TextStyle(
    //                                             color: Colors.white,
    //                                             fontSize:
    //                                                 ScreenUtil().setSp(18),
    //                                           ),
    //                                         ),
    //                                       ),
    //                                     ),
    //                                   ),
    //                                 ],
    //                               ),
    //                             ],
    //                           ),
    //                         ),
    //                       ],
    //                     ),
    //                   ],
    //                 ));
    //       })
    //     : Container();

    Future.delayed(Duration(milliseconds: 100), () {
      show();
    });
    // WidgetsFlutterBinding.ensureInitialized();

    // if (widget.dapp.url == "https://www.cosmic.online/#/") {
    //   // 强制横屏
    //   SystemChrome.setPreferredOrientations(
    //       [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    // }
  }

  @override
  void dispose() {
// 显示底部/顶部状态栏

    super.dispose();
    Future.delayed(Duration(milliseconds: 1000), display);
  }

  void display() {
    // // 强制竖屏
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    SystemChrome.setPreferredOrientations([]);
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.bottom, SystemUiOverlay.top]);
  }

  DateTime _lastTime; //上次点击时间

  Future<bool> _onWillPop() async {
    if (_lastTime == null ||
        DateTime.now().difference(_lastTime) > Duration(seconds: 1)) {
      //两次点击间隔超过1s重新计时
      _lastTime = DateTime.now();
      // Toast.show("再点一次退出应用", context);
      Fluttertoast.showToast(msg: S.current.flutter_totoast_againback);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        child: Scaffold(
            backgroundColor: ThemeUtils().getColor(
              'utils.view_controller.background',
            ),
            appBar:
                // MediaQuery.of(context).size.height >
                //         MediaQuery.of(context).size.width
                //     ?
                this.appbarshow == "true"
                    ? NavigationBar(
                        title: widget.dapp.name ?? widget.dapp.url,
                        leading: IconButton(
                          icon: Icon(Icons.arrow_back_ios, size: 20),
                          onPressed: () async {
                            final controller =
                                await webViewControllerCompeter.future;

                            if (await controller.canGoBack()) {
                              controller.goBack();
                            } else {
                              Navigator.of(context).pop();
                            }
                          },
                        ),
                        actions: [
                          Container(
                            // margin: EdgeInsets.only(right: ScreenUtil().setWidth(20)),
                            // color: Colors.red,
                            child: Row(
                              children: [
                                IconButton(
                                  padding: EdgeInsets.zero,
                                  iconSize: 25,
                                  icon: Icon(
                                    Icons.more_horiz_rounded,
                                  ),
                                  onPressed: () {
                                    bottomSheetShow(context);
                                  },
                                ),
                                IconButton(
                                  padding: EdgeInsets.zero,
                                  iconSize: 20,
                                  icon: Icon(
                                    Icons.close,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    : null,
            body: SafeArea(
                child: Ethereum.BrowserBuilder(
              address: CoreProvider.of(context).account.address,
              rpcUrl: widget.networkConfig.currenEndPoint.nodeURL,
              chainID: widget.networkConfig.networkID,
              initialUrl: widget.dapp.url,
              delegate: this,
              onWebViewCreated: (controller) {
                webViewControllerCompeter.complete(controller);
              },
              onPageStarted: (url) => {print('PageStarted: $url')},
              onPageFinished: (url) async {
                final controller = await webViewControllerCompeter.future;

                if (widget.dapp.name == null || widget.dapp.name.length <= 0) {
                  final controller = await webViewControllerCompeter.future;
                  final title =
                      await controller.evaluateJavascript('document.title');

                  setState(() {
                    /// 去掉双引号
                    widget.dapp.name = title.substring(1, title.length - 1);
                  });
                }

                if (this.isOpen == true) {
                  Navigator.of(context).pop();
                }

                this.isOpen = false;
              },
              javascriptChannels: [
                //调用扫一扫
                Ethereum.JavascriptChannel(
                  name: 'scanQRCode',
                  onMessageReceived:
                      (Ethereum.JavascriptMessage message) async {
                    final result = await Navigator.of(context).push(
                      ViewAnimations.viewRightIn(
                        ScanPage(),
                      ),
                    );

                    this.result =
                        result.toString().replaceAll(new RegExp(r"\s+"), "");

                    print("${this.result}");

                    webViewControllerCompeter.future.then((controller) =>
                        controller.evaluateJavascript(
                            'qrcodeRelst(\"${this.result.replaceAll(new RegExp(r"\s+"), "")}\")'));

                    this.result = result;
                  },
                ),

                // //调用扫一扫
                // Ethereum.JavascriptChannel(
                //   name: 'scanQRCodes',
                //   onMessageReceived: (Ethereum.JavascriptMessage message) async {
                //     final result = await Navigator.of(context).push(
                //       ViewAnimations.viewRightIn(
                //         ScanPage(),
                //       ),
                //     );

                //     this.result =
                //         result.toString().replaceAll(new RegExp(r"\s+"), "");

                //     print("${this.result}");

                //     webViewControllerCompeter.future.then((controller) =>
                //         controller.evaluateJavascript(
                //             'qrcodeRelsts(\"${this.result.replaceAll(new RegExp(r"\s+"), "")}\")'));

                //     this.result = result;
                //   },
                // ),
                //删除红点写入记录
                Ethereum.JavascriptChannel(
                  name: 'deleNotices',
                  onMessageReceived:
                      (Ethereum.JavascriptMessage message) async {
                    ConfigStorageUtils().deleteOfKey("notices");
                  },
                ),
                Ethereum.JavascriptChannel(
                  name: 'setHeaderViewShow',
                  onMessageReceived:
                      (Ethereum.JavascriptMessage message) async {
                    // print(message);
                    // ConfigStorageUtils().deleteOfKey("notices");
                    appbarshow = message.message;

// 隐藏状态栏和底部按钮栏
                    SystemChrome.setEnabledSystemUIOverlays([]);
                    setState(() {});

                    // print(message.message);
                  },
                ),
              ],
            ))

            // floatingActionButton: FloatingActionButton(
            //   child: Text('返回'),
            //   onPressed: () async {
            //     final controller = await webViewControllerCompeter.future;
            //     controller.evaluateJavascript('fromFlutter("$result")');
            //     // Navigator.pop(context);
            //   },
            // ),
            ),
        onWillPop: _onWillPop,
      );
  @override
  Future<Object> ecRecoverDelegate(String message) {
    throw UnimplementedError();
  }

  @override
  Future<Object> requestAccountsDelegate(String message) =>
      Future.value([widget.account.address]);

  @override
  Future<Object> signMessageDelegate(String message) {
    print(message);
    return Future.value('asdfasdfasdfasdfasdfasdfasdf');
  }

  @override
  Future<Object> signPersonalMessageDelegate(String message) {
    return TransactionConfirmSheetController.show(
      context: context,
      requestData: json.decode(message),
      wallet: widget.wallet,
      account: widget.account,
      dapp: widget.dapp,
      web3clientProvider: widget.web3clientProvider,
      scanAPIService: Ethereum.ScanAPIService(
        httpClient: http.Client(),
        hostURL: widget.networkConfig.currenEndPoint.scanURL,
        apiKey: widget.networkConfig.currenEndPoint.scanAPIKey,
      ),
      dataSourceProvider: widget.dataSourceProvider,
      networkConfig: widget.networkConfig,
    );
  }

  @override
  Future<Object> signTransactionDelegate(String message) {
    return TransactionConfirmSheetController.show(
      context: context,
      requestData: json.decode(message),
      wallet: widget.wallet,
      account: widget.account,
      dapp: widget.dapp,
      web3clientProvider: widget.web3clientProvider,
      scanAPIService: Ethereum.ScanAPIService(
        httpClient: http.Client(),
        hostURL: widget.networkConfig.currenEndPoint.scanURL,
        apiKey: widget.networkConfig.currenEndPoint.scanAPIKey,
      ),
      dataSourceProvider: widget.dataSourceProvider,
      networkConfig: widget.networkConfig,
    );
  }

  @override
  Future<Object> signTypedMessageDelegate(String message) {
    throw UnimplementedError();
  }
}
