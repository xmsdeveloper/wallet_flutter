// @dart=2.9
part of '../dapps.dart';

class SearchPage extends StatefulWidget {
  final Web3ClientProvider web3clientProvider;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final List<Dapp> dapplisttype;

  SearchPage({
    @required this.dataSourceProvider,
    @required this.web3clientProvider,
    this.dapplisttype,
  });
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String _latestSearchKeyword = "";

  Timer _latestSearchTimer;
  final TextEditingController searchDappListController =
      TextEditingController();
  final FocusNode inputFocusNode = FocusNode();
  //是否是网址
  final exp = new RegExp(r'^((https|http|)?:\/\/)[^\s]+');
  final Completer<Ethereum.ResponseList<Ethereum.Dapp>> dappHotList =
      Completer();
  Ethereum.ResponseList<Ethereum.Dapp> searchDappList;
  // Ethereum.ResponseList<Ethereum.Dapp> dappHotList;
  List<String> dappHistorylist;

  List<Ethereum.Dapp> dapplisttype;

  String newValue;
  // final url = Uri.parse('cli.im/file');

  // void getStatusCode() async {
  //   http.Response response = await http.get(url);
  //   print(response.statusCode);
  //   if (response.statusCode == 200) {
  //     print('exists');
  //   } else {
  //     print('not exists');
  //   }
  // }

  @override
  void initState() {
    super.initState();
    inputFocusNode.addListener(() => setState(() {}));

    // print(this.widget.dapplisttype.first.name);

    widget.dataSourceProvider.localService
        .dappHistorys()
        .then((value) => dappHistorylist = value);

    // widget.dataSourceProvider.localService.dappHistorys().then(
    //   (value) {
    //     dappHistorylist = value;

    //     return widget.dataSourceProvider.remoteServices.dapps.hotlist();
    //   },
    // ).then(dappHotList.complete);

    // 间隔500毫秒检测输入的Keyword并且重新搜索结果
    // _latestSearchTimer = Timer.periodic(Duration(milliseconds: 500), (timer) {
    //   if (_latestSearchKeyword == this.searchDappListController.text ||
    //       this.searchDappListController.text.length <= 0) {
    //     return;
    //   }
    //   final List<Future<List<Ethereum.Dapp>>> futureGroup = [];
    //   final searchKeyWord = this.searchDappListController.text;

    //   futureGroup.add(this
    //       .widget
    //       .dataSourceProvider
    //       .remoteServices
    //       .dapps
    //       .searchDappList(keyword: searchKeyWord)
    //       // ignore: missing_return
    //       .then((value) {
    //     searchDappList = value;
    //     _latestSearchKeyword = searchKeyWord;
    //     setState(() {});
    //   }));
    // });
  }

  @override
  void dispose() {
    super.dispose();
    // _latestSearchTimer.cancel();
    // 离开页面统计（手动采集时才可设置）
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.3,

        backgroundColor: Colors.grey.shade100,
        // ThemeUtils().getColor(
        //   'utils.input.fill_color',
        // ),
        //取消自带返回事件
        automaticallyImplyLeading: false,
        actions: [
          Container(
            margin: EdgeInsets.only(
              right: ScreenUtil().setHeight(20),
            ),
            child: Center(
              child: InkWell(
                child: Text(
                  S.current.backbuttom_name,
                  style: TextStyle(
                    color: ThemeUtils().getColor(
                      'utils.back_button.color',
                    ),
                  ),
                ),
                onTap: () => Navigator.pop(context),
              ),
            ),
          )
        ],
        title: Container(
          // width: ScreenUtil().setWidth(335),
          height: ScreenUtil().setHeight(40),
          decoration: BoxDecoration(
            color: ThemeUtils().getColor(
              'utils.search_box.background',
            ),
            borderRadius: BorderRadius.circular(30),
          ),
          child: TextFormField(
            textInputAction: TextInputAction.search,
            focusNode: inputFocusNode,
            controller: searchDappListController,
            autofocus: false,
            onChanged: (_) {
              // setState(() {});
            },
            onFieldSubmitted: (_) async {
              if (exp.hasMatch(searchDappListController.text)) {
                http.Response response =
                    await http.get(this.searchDappListController.text);

                if (response.statusCode == 200) {
                  openDapp(
                    context,
                    Ethereum.Dapp(
                      url: this.searchDappListController.text,
                      logo: "",
                    ),
                  );
                  this
                      .widget
                      .dataSourceProvider
                      .localService
                      .dappHistoryInsert(this.searchDappListController.text);
                } else {
                  Fluttertoast.showToast(msg: "网址不存在，请检查网址是否输入正确");
                }
              } else {
                try {
                  http.Response response = await http
                      .get("https://" + this.searchDappListController.text)
                      .timeout(Duration(
                        milliseconds: 2000,
                      ));
                  print(response.statusCode);
                  if (response.statusCode == 200) {
                    openDapp(
                      context,
                      Ethereum.Dapp(
                        url: "https://" + this.searchDappListController.text,
                        logo: "",
                      ),
                    );
                    this
                        .widget
                        .dataSourceProvider
                        .localService
                        .dappHistoryInsert(
                            "https://" + this.searchDappListController.text);
                  } else {
                    Fluttertoast.showToast(msg: "网址不存在，请检查网址是否输入正确");
                  }
                } catch (e) {
                  Fluttertoast.showToast(msg: "网址不存在，请检查网址是否输入正确");
                }
              }
            },
            style: ThemeUtils()
                .getTextStyle(
                  'utils.input.textstyle.content',
                )
                .copyWith(fontSize: 15),
            decoration: InputDecoration(
              fillColor: Colors.white,
              // fillColor: ThemeUtils().getColor(
              //   'utils.search_box.background',
              // ),
              filled: true,
              contentPadding: EdgeInsets.all(0),
              hintText: S.current.ethereum_home_address_selector_placeholder,
              hintStyle: ThemeUtils().getTextStyle(
                'utils.input.textstyle.hit',
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    ScreenUtil().setWidth(22),
                  ),
                ),
                borderSide: BorderSide(color: Colors.grey[200]),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    ScreenUtil().setWidth(22),
                  ),
                ),
                borderSide: BorderSide(color: Colors.transparent),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    ScreenUtil().setWidth(22),
                  ),
                ),
                borderSide: BorderSide(color: Colors.transparent),
              ),
              prefixIconConstraints: BoxConstraints.expand(
                width: ScreenUtil().setWidth(50),
                height: ScreenUtil().setHeight(40),
              ),
              prefixIcon: Icon(
                Icons.search_rounded,
                color: Colors.grey,
                // color: ThemeUtils().getColor(
                //   'views.chaincore.ethereum.address_selector.section_header_search',
                // ),
              ),
              suffixIconConstraints: BoxConstraints.expand(
                width: ScreenUtil().setWidth(50),
                height: ScreenUtil().setHeight(40),
              ),
              // suffixIcon: IconButton(
              //   padding: EdgeInsets.zero,
              //   icon: Icon(
              //     Icons.close,
              //     size: 15,
              //     color: ThemeUtils().getColor(
              //       'views.chaincore.ethereum.address_selector.section_header_search',
              //     ),
              //   ),
              //   onPressed: () {
              //     setState(() {
              //       searchDappListController.clear();
              //     });
              //   },
              // ),
            ),
          ),
        ),
      ),
      backgroundColor: Colors.grey.shade100,
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return searchHistory();
                // AnimatedCrossFade(
                //   firstChild: searchHistory(),

                //   // _SearcHistory(
                //   //   dappHistorylist: this.dappHistorylist,
                //   //   dappHotList: this.dappHotList,
                //   //   dataSourceProvider: this.widget.dataSourceProvider,
                //   //   searchDappListController: this.searchDappListController.text,
                //   // ),
                //   secondChild: Column(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       Container(
                //         margin: EdgeInsets.all(ScreenUtil().setWidth(20)),
                //         child: Text(
                //           S.current.ethereum_dapp_search_result,
                //         ),
                //       ),
                //       // _SearchResult(
                //       //   searchDappList: this.searchDappList,
                //       //   web3clientProvider: this.widget.web3clientProvider,
                //       //   dataSourceProvider: this.widget.dataSourceProvider,
                //       // ),
                //     ],
                //   ),
                //   crossFadeState: this.searchDappListController.text == ""
                //       ? CrossFadeState.showFirst
                //       : CrossFadeState.showSecond,
                //   duration: const Duration(seconds: 0),
                // );
              },
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget searchHistory() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //热门搜索
        Container(
          margin: EdgeInsets.all(ScreenUtil().setHeight(20)),
          alignment: Alignment.centerLeft,
          child: Text(
            S.current.ethereum_dapp_hot_search,
            style: ThemeUtils().getTextStyle(
              'views.chaincore.ethereum.dapp_searchPage.textstyle.section_title',
            ),
          ),
        ),

        CoreProvider().networkConfig.endpoints.first.resourcesServicesURL !=
                null
            ? Container(
                height: ScreenUtil().setHeight(120),
                child: GridView.builder(
                    itemCount: widget.dapplisttype.length <= 6
                        ? widget.dapplisttype.length
                        : 6,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: ScreenUtil().setHeight(10),
                      mainAxisExtent: ScreenUtil().setHeight(45),
                    ),
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                          bottom: ScreenUtil().setWidth(10),
                        ),
                        width: ScreenUtil().setWidth(100),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                right: ScreenUtil().setWidth(10),
                              ),
                              // decoration: BoxDecoration(
                              //   borderRadius:
                              //       BorderRadius.circular(10),
                              // ),
                              width: ScreenUtil().setWidth(30),
                              height: ScreenUtil().setHeight(30),
                              child: InkWell(
                                onTap: () async {
                                  if (this.widget.dapplisttype[index].url !=
                                      "") {
                                    openDapp(
                                      context,
                                      Ethereum.Dapp(
                                        url:
                                            this.widget.dapplisttype[index].url,
                                        name: this
                                            .widget
                                            .dapplisttype[index]
                                            .name,
                                        logo: this
                                            .widget
                                            .dapplisttype[index]
                                            .logo,
                                      ),
                                    );
                                  } else {
                                    Fluttertoast.showToast(
                                        msg: S.current
                                            .message_box_prompt_password_not_yet_open);
                                  }
                                  // http.Response response = await http
                                  //     .get(this.widget.dapplisttype[index].url);
                                  // if (response.statusCode == 200) {
                                  //   openDapp(
                                  //     context,
                                  //     Ethereum.Dapp(
                                  //       url:
                                  //           this.widget.dapplisttype[index].url,
                                  //       name: this
                                  //           .widget
                                  //           .dapplisttype[index]
                                  //           .name,
                                  //       logo: this
                                  //           .widget
                                  //           .dapplisttype[index]
                                  //           .logo,
                                  //     ),
                                  //   );
                                  // } else {
                                  //   Fluttertoast.showToast(
                                  //     msg: S.current
                                  //         .message_box_prompt_password_not_yet_open,
                                  //   );
                                  // }
                                  if (dappHistorylist.contains(
                                      this.widget.dapplisttype[index].url)) {
                                  } else {
                                    this
                                        .widget
                                        .dataSourceProvider
                                        .localService
                                        .dappHistoryInsert(this
                                            .widget
                                            .dapplisttype[index]
                                            .url);
                                    setState(() {});
                                  }
                                },
                                child: ClipRRect(
                                  // borderRadius:
                                  //     BorderRadius.circular(
                                  //   10,
                                  // ),
                                  child: CachedImage.assetNetwork(
                                    url: this.widget.dapplisttype[index].logo,
                                    placeholder: Image.asset(
                                      'assets/imgs/dapp_default.png',
                                      package: 'wallet_flutter',
                                    ),
                                    width: ScreenUtil().setWidth(30),
                                    height: ScreenUtil().setWidth(30),
                                    imageCacheHeight: 100,
                                    imageCacheWidth: 100,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () async {
                                  // if (e.url != "") {
                                  //   openDapp(
                                  //     context,
                                  //     Ethereum.Dapp(
                                  //       url: e.url,
                                  //       name: e.name,
                                  //       logo: e.logo,
                                  //     ),
                                  //   );
                                  // } else {
                                  //   Fluttertoast.showToast(
                                  //       msg: "暂未开放");
                                  // }
                                  openDapp(
                                    context,
                                    Ethereum.Dapp(
                                      url: this.widget.dapplisttype[index].url,
                                      name:
                                          this.widget.dapplisttype[index].name,
                                      logo:
                                          this.widget.dapplisttype[index].logo,
                                    ),
                                  );

                                  if (dappHistorylist.contains(this
                                          .widget
                                          .dapplisttype[index]
                                          .url) ||
                                      this.widget.dapplisttype[index].url ==
                                          "") {
                                  } else {
                                    this
                                        .widget
                                        .dataSourceProvider
                                        .localService
                                        .dappHistoryInsert(this
                                            .widget
                                            .dapplisttype[index]
                                            .url);
                                    setState(
                                      () {
                                        //请求一次接口，刷新数据。
                                        // searchDappListController.text =
                                        //     "";
                                      },
                                    );
                                  }
                                },
                                child: Text(
                                  this.widget.dapplisttype[index].name,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.dapp_searchPage.textstyle.dapp_name',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
              )
            : Container(),

        //搜索历史
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(20),
                top: ScreenUtil().setWidth(25),
                bottom: ScreenUtil().setHeight(19),
              ),
              width: ScreenUtil().setWidth(180),
              alignment: Alignment.centerLeft,
              child: Text(
                S.current.ethereum_dapp_search_history,
                style: ThemeUtils().getTextStyle(
                  'views.chaincore.ethereum.dapp_searchPage.textstyle.section_title',
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setWidth(25),
                bottom: ScreenUtil().setHeight(15),
              ),
              alignment: Alignment.centerRight,
              width: ScreenUtil().setWidth(145),
              child: InkWell(
                // child: Icon(
                //   const IconData(
                //     0xe609,
                //     fontFamily: 'myIcon',
                //   ),
                //   size: ScreenUtil().setHeight(20),
                // ),
                child: Icon(
                  CustomIcons.delet,
                  size: ScreenUtil().setSp(16),
                  color: Color.fromRGBO(0, 0, 0, 1),
                ),
                onTap: () {
                  this
                      .widget
                      .dataSourceProvider
                      .localService
                      .dappHistoryClean();
                  setState(() {});
                },
              ),
            ),
          ],
        ),
        FutureBuilder(
          future: widget.dataSourceProvider.localService.dappHistorys(),
          builder: (context, snapshot) => snapshot.connectionState !=
                  ConnectionState.done
              ? Container()
              : Container(
                  height: ScreenUtil().setHeight(41) * snapshot.data.length,
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 1.6,
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(20),
                                  // bottom: ScreenUtil().setWidth(9),
                                ),
                                child: GestureDetector(
                                  onLongPressStart: (detail) {
                                    //菜单弹出栏,复制删除
                                    showMenu(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      context: context,
                                      position: RelativeRect.fromLTRB(
                                        detail.globalPosition.dx, //取点击位置坐弹出x坐标

                                        detail.globalPosition.dy -
                                            ScreenUtil().setHeight(20),
                                        detail.globalPosition.dx,
                                        detail.globalPosition.dy,
                                      ), //弹出框位置
                                      items: <PopupMenuEntry>[
                                        PopupMenuItem(
                                          child: Center(
                                            child: InkWell(
                                              onTap: () async {
                                                await Clipboard.setData(
                                                  ClipboardData(
                                                    text: snapshot.data[index],
                                                  ),
                                                );

                                                Fluttertoast.showToast(
                                                  msg: S.current.copy_success,
                                                );

                                                Navigator.pop(context);
                                              },
                                              child: Container(
                                                margin: EdgeInsets.all(
                                                    ScreenUtil().setHeight(22)),
                                                child: Text(
                                                  S.current
                                                      .ethereum_dapp_search_menu_bar_copy,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        PopupMenuItem(
                                          child: Center(
                                            child: InkWell(
                                              onTap: () async {
                                                await widget.dataSourceProvider
                                                    .localService
                                                    .dappHistoryDelete(
                                                        snapshot.data[index]);
                                                Navigator.pop(context);
                                                setState(() {});
                                              },
                                              child: Container(
                                                margin: EdgeInsets.all(
                                                  ScreenUtil().setHeight(22),
                                                ),
                                                child: Text(
                                                  S.current
                                                      .ethereum_dapp_search_menu_bar_delete,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                  onTap: () {
                                    this.searchDappListController.text =
                                        snapshot.data[index];
                                    openDapp(
                                      context,
                                      Ethereum.Dapp(
                                        url: snapshot.data[index],
                                        logo: "",
                                      ),
                                    );
                                  },
                                  child: Text(snapshot.data[index],
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(14),
                                        color: Color.fromRGBO(0, 0, 0, 1),
                                      )
                                      // style: ThemeUtils().getTextStyle(
                                      //   'views.chaincore.ethereum.dapp_searchPage.textstyle.history_name',
                                      // ),
                                      ),
                                ),
                              ),
                              InkWell(
                                onTap: () async {
                                  await widget.dataSourceProvider.localService
                                      .dappHistoryDelete(snapshot.data[index]);

                                  setState(() {});
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                    right: ScreenUtil().setWidth(30),
                                    bottom: ScreenUtil().setWidth(14),
                                  ),
                                  height: ScreenUtil().setHeight(20),
                                  child: Icon(
                                    Icons.close,
                                    size: ScreenUtil().setSp(18),
                                  ),
                                ),
                              )
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          )
                        ],
                      );
                    },
                  ),
                ),
        ),
      ],
    );
  }

  void openDapp(BuildContext context, Ethereum.Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: widget.web3clientProvider,
          dataSourceProvider: widget.dataSourceProvider,
        ),
      ),
    );
  }
}
