import 'dart:convert';
import 'dart:io';

import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/entity/entity.dart';

Future<List<TokenBlog>> futureGetTransferList(
  CoreProvider sourceCoreProvider,
  String rec,
) async {
  List<TokenBlog> tansferList = [];
  var httpClient = new HttpClient();
  // ignore: close_sinks
  var url = sourceCoreProvider.endPoint.scanURL +
      "/api?module=account&action=tokentx&address=${sourceCoreProvider.account.address}&page=1&offset=100&startblock=0&endblock=999999999&sort=asc&apikey=" +
      sourceCoreProvider.endPoint.scanAPIKey;
  var request = await httpClient.getUrl(Uri.parse(url));
  var response = await request.close();
  if (response.statusCode == HttpStatus.ok) {
    var json = await response.transform(utf8.decoder).join();
    var data = jsonDecode(json);
    List d = [];
    d = data['result'];

    d.map((e) {
      if (!tansferList.contains(TokenBlog(
            name: e['tokenName'],
            symbol: e['tokenSymbol'],
            address: e['contractAddress'],
            decimals: int.parse(e['tokenDecimal']),
          )) &&
          e['tokenSymbol'].toString().length <= 8 &&
          RegExp(rec).hasMatch(e['tokenName']) == false &&
          !e['tokenName'].toString().contains("."))
        tansferList.add(TokenBlog(
          name: e['tokenName'],
          symbol: e['tokenSymbol'],
          address: e['contractAddress'],
          decimals: int.parse(e['tokenDecimal']),
        ));
    }).toList();
  }
  return tansferList;
}
