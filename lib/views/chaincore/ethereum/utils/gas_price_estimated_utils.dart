// @dart=2.9

import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

/// 手续费配置需要大量的顺序逻辑，并且多个地方需要使用此处直接提供一个Future，来获取最终的结果或者错误
class EstimatedGasPriceResult {
  final FeeEstimatedMode fromMode;
  final GasConfig gasConfig;

  const EstimatedGasPriceResult(this.fromMode, this.gasConfig);
}

Future<EstimatedGasPriceResult> estimatedGasPrice(
  NetworkConfig networkConfig,
  DataSourceProvider dataSourceProvider,
  Web3ClientProvider web3clientProvider,
  ScanAPIService scanAPIService,
) async {
  for (FeeEstimatedMode mode in networkConfig.feeEstimatedMode) {
    switch (mode) {
      case FeeEstimatedMode.Unsupport:
        throw FeeEstimatedMode.Unsupport;

      case FeeEstimatedMode.Scan:
        try {
          final gasOracle =
              await scanAPIService.gasTrackerGasOracle().catchError(print);

          if (gasOracle != null) {
            /// 区块浏览器的接口返回的单位是Gwei，而GasConfig使用的是最小单位，需要转换
            return EstimatedGasPriceResult(
              mode,
              // GasConfig(
              //   safe: BigInt.from(gasOracle.safeGasPrice) *
              //       BigInt.from(10).pow(9),
              //   propose: BigInt.from(gasOracle.proposeGasPrice) *
              //       BigInt.from(10).pow(9),
              //   fast: BigInt.from(gasOracle.fastGasPrice) *
              //       BigInt.from(10).pow(9),
              // ),
              GasConfig(
                safe: BigInt.from(gasOracle.safeGasPrice) *
                    BigInt.from(10).pow(9),
                propose: BigInt.from(gasOracle.proposeGasPrice) *
                        BigInt.from(10).pow(9) +
                    BigInt.from(1),
                fast: BigInt.from(gasOracle.fastGasPrice) *
                        BigInt.from(10).pow(9) +
                    BigInt.from(2),
              ),
            );
          }
        } catch (e) {
          /// 如果没有实现该接口，或者出现错误,继续使用下一个顺序
          print('FeeEstimatedMode.Scan estimating Failed');
          continue;
        }

        break;

      case FeeEstimatedMode.DataProvider:

        /// 使用节点估算手续费
        final gasPrice = await web3clientProvider.getGasPrice();

        // return EstimatedGasPriceResult(
        //   mode,
        //   GasConfig(
        //     safe: gasPrice,
        //     propose: gasPrice,
        //     fast: gasPrice,
        //   ),
        // );
        // break;
        return EstimatedGasPriceResult(
          mode,
          GasConfig(
            safe: gasPrice,
            propose: gasPrice + BigInt.from(1000),
            fast: gasPrice + BigInt.from(2000),
          ),
        );
        break;

      case FeeEstimatedMode.Default:
        try {
          print(networkConfig.defaultGasOracle.toString());
          return EstimatedGasPriceResult(mode, networkConfig.defaultGasOracle);
        } catch (e) {
          continue;
        }
        break;
      case FeeEstimatedMode.Noder:

        /// 使用节点估算手续费
        final gasPrice = await web3clientProvider.getGasPrice();

        // return EstimatedGasPriceResult(
        //   mode,
        //   GasConfig(
        //     safe: gasPrice,
        //     propose: gasPrice,
        //     fast: gasPrice,
        //   ),
        // );
        // break;
        return EstimatedGasPriceResult(
          mode,
          GasConfig(
            safe: gasPrice,
            propose: gasPrice + BigInt.from(1000),
            fast: gasPrice + BigInt.from(2000),
          ),
        );
    }
  }

  ///TODO 无法获取手续费配置
  return null;
}
