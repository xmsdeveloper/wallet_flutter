// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';

typedef LabelStringCallBack = String Function(double sliderValue);

class FeeConfigBoard extends StatelessWidget {
  final ValueNotifier<double> sliderValueListenable = ValueNotifier<double>(50);

  final FeeTokenBlog feeTokenBlog;
  final LabelStringCallBack lableCallBack;
  final LabelStringCallBack totalStringCallBack;
  final ValueChanged<double> onChanged;

  final TextStyle titleStyle;

  FeeConfigBoard({
    @required this.feeTokenBlog,
    @required this.lableCallBack,
    @required this.totalStringCallBack,
    @required this.onChanged,
    this.titleStyle,
    double defaultValue = 0.5,
  }) {
    sliderValueListenable.value = defaultValue;
  }

  @override
  Widget build(BuildContext context) => ValueListenableBuilder<double>(
        valueListenable: sliderValueListenable,
        builder: (context, sliderValue, child) => Container(
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              /// Title
              Row(
                children: [
                  Text(
                    S.current.ethereum_utils_fee_config_board_title,
                    style: titleStyle ??
                        ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.dapp.textstyle.field_title',
                        ),
                  ),
                  Expanded(child: SizedBox()),
                  Text(
                    totalStringCallBack(
                      sliderValueListenable.value,
                    ),
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.dapp.textstyle.field_content_fee',
                    ),
                  )
                ],
              ),

              /// Slider
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: ScreenUtil().setHeight(15),
                    width: ScreenUtil().setWidth(24),
                    child: Image.asset(
                      'assets/imgs/slow.png',
                      color: Colors.grey,
                      package: "wallet_flutter",
                    ),
                  ),
                  Container(
                    // width: ScreenUtil().setWidth(240),
                    width: MediaQuery.of(context).size.width -
                        ScreenUtil().setWidth(160),
                    child: SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                        overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                      ),
                      child: Container(
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(32),
                          bottom: ScreenUtil().setHeight(32),
                          left: ScreenUtil().setWidth(7),
                          right: ScreenUtil().setWidth(7),
                        ),
                        child: Slider(
                          min: 0,
                          max: 1,
                          value: sliderValueListenable.value,
                          // divisions: 20,

                          // activeColor: Color.fromRGBO(72, 118, 255, 1),
                          // activeColor: Color.fromRGBO(48, 198, 153, 1),
                          activeColor: Colors.blue,
                          inactiveColor: Color.fromRGBO(241, 241, 241, 1),
                          label: lableCallBack(sliderValueListenable.value),
                          onChanged: (v) {
                            sliderValueListenable.value = v;
                            onChanged(v);
                          },
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(15),
                    width: ScreenUtil().setWidth(24),
                    child: Image.asset(
                      'assets/imgs/speed.png',
                      color: Colors.grey,
                      package: "wallet_flutter",
                    ),
                  ),
                ],
              ),

              /// BottomTips
              Row(
                children: [
                  Expanded(
                    child: SizedBox(
                      child: GestureDetector(
                        child: Text(
                          S.current.ethereum_utils_fee_config_board_slow,
                          textAlign: TextAlign.left,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.dapp.textstyle.field_title',
                          ),
                        ),
                        onTap: () {
                          sliderValueListenable.value = 0;
                          onChanged(0);
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      child: GestureDetector(
                        child: Text(
                          S.current.ethereum_utils_fee_config_board_propose,
                          textAlign: TextAlign.center,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.dapp.textstyle.field_title',
                          ),
                        ),
                        onTap: () {
                          sliderValueListenable.value = 0.5;
                          onChanged(0.5);
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      child: GestureDetector(
                        child: Text(
                          S.current.ethereum_utils_fee_config_board_fast,
                          textAlign: TextAlign.right,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.dapp.textstyle.field_title',
                          ),
                        ),
                        onTap: () {
                          sliderValueListenable.value = 1;
                          onChanged(1);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
}
