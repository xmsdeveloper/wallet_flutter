// @dart=2.9
import 'dart:convert';
import 'dart:typed_data';

import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/entity/entity.dart';

import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

/// 函数可以获得对应的Token的数量
Future<Amount> futureGetErc721Balance(
  Web3ClientProvider web3Client,
  TokenBlog tokenBlog,
  String address,
) {
  final balanceOf = ContractFunction(
    'balanceOf',
    [FunctionParameter(null, AddressType())],
    outputs: [FunctionParameter(null, UintType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  return web3Client
      .call(
        from: address,
        to: tokenBlog.address,
        data: balanceOf.encodeCall([address]),
      )
      .then(balanceOf.decodeReturnValues)
      .then((outputs) => Amount(
            value: outputs.first,
            decimals: tokenBlog.decimals,
          ))
      .timeout(
        Duration(seconds: 10),
        onTimeout: () => Amount(
          value: BigInt.zero,
          decimals: tokenBlog.decimals,
        ),
      );
}

Future<TokenBlog> futureGetErc721TokenBlog(
  Web3ClientProvider web3Client,
  String contractAddress,
  String fromAddress,
) async {
  final callName = ContractFunction(
    'name',
    [],
    outputs: [FunctionParameter(null, StringType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  final callSymbol = ContractFunction(
    'symbol',
    [],
    outputs: [FunctionParameter(null, StringType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );
  //通过id查询到的=余额为ERC1155,否则为erc721
  final balanceOf = ContractFunction(
    'balanceOf',
    [
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, UintType())
    ],
    outputs: [FunctionParameter(null, UintType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );
  String protocol;
  try {
    final balanceof = await web3Client
        .call(
          from: fromAddress,
          to: contractAddress,
          data: balanceOf.encodeCall([fromAddress, BigInt.zero]),
        )
        .then(balanceOf.decodeReturnValues)
        .then((outputs) => Amount(
              value: outputs.first,
              decimals: 0,
            ))
        .timeout(
          Duration(seconds: 10),
          onTimeout: () => Amount(
            value: BigInt.zero,
            decimals: 0,
          ),
        );
    if (balanceof.value >= BigInt.zero) {
      protocol = "ERC1155";
    }
  } catch (e) {
    protocol = "ERC721";
  }
  print(234);
  print(
    await web3Client
        .call(
          from: fromAddress,
          to: contractAddress,
          data: callName.encodeCall(),
        )
        .then(callName.decodeReturnValues)
        .then((outputs) => outputs.first as String)
        .timeout(
          Duration(seconds: 5),
          onTimeout: () => 'Unknown',
        ),
  );
  try {
    return TokenBlog(
      address: contractAddress,
      name: await web3Client
          .call(
            from: fromAddress,
            to: contractAddress,
            data: callName.encodeCall(),
          )
          .then(callName.decodeReturnValues)
          .then((outputs) => outputs.first as String)
          .timeout(
            Duration(seconds: 5),
            onTimeout: () => 'Unknown',
          ),
      symbol: await web3Client
          .call(
            from: fromAddress,
            to: contractAddress,
            data: callName.encodeCall(),
          )
          .then(callName.decodeReturnValues)
          .then((outputs) => outputs.first as String)
          .timeout(
            Duration(seconds: 5),
            onTimeout: () => 'Unknown',
          ),
      decimals: 0,
      protocol: protocol,
    );
  } catch (e) {
    print(e);
    return null;
  }
}

Future<BigInt> futureGetNftTokenId(
  Web3ClientProvider web3Client,
  String contractAddress,
  String fromAddress,
  BigInt index,
) async {
  final calltokenOfOwnerByIndex = ContractFunction(
    'tokenOfOwnerByIndex',
    [
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, UintType()),
    ],
    outputs: [FunctionParameter(null, UintType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  return await web3Client
      .call(
        from: fromAddress,
        to: contractAddress,
        data: calltokenOfOwnerByIndex.encodeCall([fromAddress, index]),
      )
      .then(calltokenOfOwnerByIndex.decodeReturnValues)
      .then((outputs) => outputs.first as BigInt)
      .timeout(
        Duration(seconds: 5),
        onTimeout: () => null,
      );
}

Future<String> futureGetNftTokenUrl(
  Web3ClientProvider web3Client,
  String contractAddress,
  String fromAddress,
  BigInt tokenId,
) async {
  final calltokenURI = ContractFunction(
    'tokenURI',
    [
      FunctionParameter(null, UintType()),
    ],
    outputs: [FunctionParameter(null, StringType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  return await web3Client
      .call(
        from: fromAddress,
        to: contractAddress,
        data: calltokenURI.encodeCall([tokenId]),
      )
      .then(calltokenURI.decodeReturnValues)
      .then((outputs) => outputs.first as String)
      .timeout(
        Duration(seconds: 5),
        onTimeout: () => null,
      );
}

Future<Amount> futureErc1155Balance(
  Web3ClientProvider web3Client,
  TokenBlog tokenBlog,
  String address,
  BigInt tokenID,
) {
  final balanceOf = ContractFunction(
    'balanceOf',
    [
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, UintType())
    ],
    outputs: [FunctionParameter(null, UintType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  return web3Client
      .call(
        from: address,
        to: tokenBlog.address,
        data: balanceOf.encodeCall([address, tokenID]),
      )
      .then(balanceOf.decodeReturnValues)
      .then((outputs) => Amount(
            value: outputs.first,
            decimals: tokenBlog.decimals,
          ))
      .timeout(
        Duration(seconds: 10),
        onTimeout: () => Amount(
          value: BigInt.zero,
          decimals: tokenBlog.decimals,
        ),
      );
}

Future<String> futureErc721TokenTransfer(
  Web3ClientProvider web3Client,
  TokenBlog tokenBlog,
  WalletKey fromAccount,
  String toAddress,
  BigInt tokenId,
  BigInt gas,
  BigInt gasPriceInWei,
) async {
  assert(toAddress != null && toAddress.length > 0, 'Invaid To Address');

  final nonce = await web3Client.getTransactionCount(fromAccount.address);
  final chainID = await web3Client.getChainID();
  final certificate = CertificateEthereum(
    privateKey: bytesToHex(
      fromAccount.privateKeyRaw,
    ),
  );

  String transactionRaw;

  final transferFunction = ContractFunction(
    'transferFrom',
    [
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, UintType()),
    ],
    outputs: [
      FunctionParameter(null, BoolType()),
    ],
    type: ContractFunctionType.function,
    mutability: StateMutability.nonPayable,
  );

  transactionRaw = await certificate.signTransaction(
    chainId: HexString.fromNumber(chainID),
    to: HexString.fromHex(tokenBlog.address),
    nonce: HexString.fromNumber(nonce),
    gasPrice: HexString.fromBigInt(gasPriceInWei),
    gasLimit: HexString.fromBigInt(gas),
    data: HexString(
        transferFunction.encodeCall([fromAccount.address, toAddress, tokenId])),
  );

  return web3Client.sendRawTransaction(transactionRaw);
}

Future<String> futureErc1155TokenTransfer(
  Web3ClientProvider web3Client,
  TokenBlog tokenBlog,
  WalletKey fromAccount,
  String toAddress,
  BigInt tokenId,
  BigInt amount,
  BigInt gas,
  BigInt gasPriceInWei,
  String tx,
) async {
  assert(toAddress != null && toAddress.length > 0, 'Invaid To Address');

  final nonce = await web3Client.getTransactionCount(fromAccount.address);
  final chainID = await web3Client.getChainID();
  final certificate = CertificateEthereum(
    privateKey: bytesToHex(
      fromAccount.privateKeyRaw,
    ),
  );

  String transactionRaw;

  final transferFunction = ContractFunction(
    'safeTransferFrom',
    [
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, AddressType()),
      FunctionParameter(null, UintType()),
      FunctionParameter(null, UintType()),
      FunctionParameter(null, DynamicBytes()),
    ],
    outputs: [
      FunctionParameter(null, BoolType()),
    ],
    type: ContractFunctionType.function,
    mutability: StateMutability.nonPayable,
  );

  transactionRaw = await certificate.signTransaction(
    chainId: HexString.fromNumber(chainID),
    to: HexString.fromHex(tokenBlog.address),
    nonce: HexString.fromNumber(nonce),
    gasPrice: HexString.fromBigInt(gasPriceInWei),
    gasLimit: HexString.fromBigInt(gas),
    data: HexString(transferFunction.encodeCall([
      fromAccount.address,
      toAddress,
      tokenId,
      amount,
      uint8ListFromList(utf8.encode("data"))
    ])),
  );

  return web3Client.sendRawTransaction(transactionRaw);
}
