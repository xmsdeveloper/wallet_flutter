// @dart=2.9
import 'dart:typed_data';

import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/services/entity/entity.dart';

import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

/// 函数可以获得对应的Token的数量
Future<Amount> futureGetBalance(
  Web3ClientProvider web3Client,
  TokenBlog tokenBlog,
  String address,
) {
  /// 链上主币
  if (!tokenBlog.isContract) {
    return web3Client
        .getBalance(address)
        .then(
          (balance) => Amount(
            value: balance,
            decimals: tokenBlog.decimals,
          ),
        )
        .timeout(
          Duration(seconds: 5),
          onTimeout: () => Amount(
            value: BigInt.zero,
            decimals: tokenBlog.decimals,
          ),
        );
  }

  /// 代币
  else {
    final balanceOf = ContractFunction(
      'balanceOf',
      [FunctionParameter(null, AddressType())],
      outputs: [FunctionParameter(null, UintType())],
      type: ContractFunctionType.function,
      mutability: StateMutability.view,
    );

    return web3Client
        .call(
          from: address,
          to: tokenBlog.address,
          data: balanceOf.encodeCall([address]),
        )
        .then(balanceOf.decodeReturnValues)
        .then((outputs) => Amount(
              value: outputs.first,
              decimals: tokenBlog.decimals,
            ))
        .timeout(
          Duration(seconds: 10),
          onTimeout: () => Amount(
            value: BigInt.zero,
            decimals: tokenBlog.decimals,
          ),
        );
  }
}

Future<TokenBlog> futureGetTokenBlog(
  Web3ClientProvider web3Client,
  String contractAddress,
  String fromAddress,
) async {
  final callName = ContractFunction(
    'name',
    [],
    outputs: [FunctionParameter(null, StringType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  final callSymbol = ContractFunction(
    'symbol',
    [],
    outputs: [FunctionParameter(null, StringType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  final callDecimals = ContractFunction(
    'decimals',
    [],
    outputs: [FunctionParameter(null, UintType())],
    type: ContractFunctionType.function,
    mutability: StateMutability.view,
  );

  // print("ww" + contractAddress + fromAddress);
  // print(await web3Client.call(
  //   from: "0x57f1887a8BF19b14fC0dF6Fd9B2acc9Af147eA85",
  //   to: "0x57f1887a8BF19b14fC0dF6Fd9B2acc9Af147eA85",
  //   data: Uint8List.fromList([0, 0, 0, 0]),
  // ));

  try {
    return TokenBlog(
      address: contractAddress,
      name: await web3Client
          .call(
            from: fromAddress,
            to: contractAddress,
            data: callName.encodeCall(),
          )
          .then(callName.decodeReturnValues)
          .then((outputs) => outputs.first as String)
          .timeout(
            Duration(seconds: 5),
            onTimeout: () => 'Unknown',
          ),
      symbol: await web3Client
          .call(
            from: fromAddress,
            to: contractAddress,
            data: callSymbol.encodeCall(),
          )
          .then(callSymbol.decodeReturnValues)
          .then((outputs) => outputs.first as String)
          .timeout(
            Duration(seconds: 5),
            onTimeout: () => 'Unknown',
          ),
      decimals: await web3Client
          .call(
            from: fromAddress,
            to: contractAddress,
            data: callDecimals.encodeCall(),
          )
          .then(callDecimals.decodeReturnValues)
          .then((outputs) => (outputs.first as BigInt).toInt())
          .timeout(
            Duration(seconds: 5),
            onTimeout: () => 0,
          ),
    );
  } catch (e) {
    print(e);
    return null;
  }
}

Future<String> futureTokenTransfer(
  Web3ClientProvider web3Client,
  TokenBlog tokenBlog,
  WalletKey fromAccount,
  String toAddress,
  Amount amount,
  BigInt gas,
  BigInt gasPriceInWei,
) async {
  assert(toAddress != null && toAddress.length > 0, 'Invaid To Address');

  final nonce = await web3Client.getTransactionCount(fromAccount.address);
  final chainID = await web3Client.getChainID();
  final certificate = CertificateEthereum(
    privateKey: bytesToHex(
      fromAccount.privateKeyRaw,
    ),
  );

  String transactionRaw;

  if (tokenBlog.isContract) {
    final transferFunction = ContractFunction(
      'transfer',
      [
        FunctionParameter(null, AddressType()),
        FunctionParameter(null, UintType()),
      ],
      outputs: [
        FunctionParameter(null, BoolType()),
      ],
      type: ContractFunctionType.function,
      mutability: StateMutability.nonPayable,
    );

    transactionRaw = await certificate.signTransaction(
      chainId: HexString.fromNumber(chainID),
      to: HexString.fromHex(tokenBlog.address),
      nonce: HexString.fromNumber(nonce),
      gasPrice: HexString.fromBigInt(gasPriceInWei),
      gasLimit: HexString.fromBigInt(gas),
      data: HexString(transferFunction.encodeCall([toAddress, amount.value])),
    );
  } else {
    transactionRaw = await certificate.signTransaction(
      chainId: HexString.fromNumber(chainID),
      to: HexString.fromHex(toAddress),
      nonce: HexString.fromNumber(nonce),
      gasPrice: HexString.fromBigInt(gasPriceInWei),
      gasLimit: HexString.fromBigInt(gas),
      value: HexString.fromBigInt(amount.value),
    );
  }

  return web3Client.sendRawTransaction(transactionRaw);
}
