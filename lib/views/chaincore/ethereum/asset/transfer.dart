// @dart=2.9
part of '../asset.dart';

class Transfer extends StatefulWidget {
  final Ethereum.TokenBlog tokenBlog;
  final WalletKey fromAccount;
  // final Ethereum.RecipientAccountBlog toAccount;
  final Ethereum.RecipientAccountBlog addressEditingController;
  final Web3ClientProvider web3Client;
  final Ethereum.ScanAPIService scanAPIService;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;

  Transfer({
    @required this.tokenBlog,
    @required this.fromAccount,
    // @required this.toAccount,
    @required this.web3Client,
    @required this.scanAPIService,
    @required this.dataSourceProvider,
    @required this.networkConfig,
    this.addressEditingController,
  });

  @override
  State<StatefulWidget> createState() => _TransferState();
}

class _TransferState extends State<Transfer> {
  final ScrollController scrollController = ScrollController();

  final TextEditingController sentAmountTextEditingController =
      TextEditingController(text: '');

  ValueNotifier<bool> payloadListenable = ValueNotifier<bool>(false);
  final TextEditingController addressEditingController =
      TextEditingController();
  final FocusNode inputFocusNode = FocusNode();

  bool inSentAllModule = false;

  /// 当前余额
  Amount balance;

  Amount inputValue;

  Utils.EstimatedGasPriceResult gasPriceResult;

  BigInt gas;

  Amount gasPrice;

  BigInt granularity = BigInt.one;

  List<String> list;

  bool isAuth = false;
  Timer estimateGasInterval;
  bool estimating = false;

  Amount coinBalance;

  String coin;

  /// 正确地址的正则表达式
  final addressRegexp = RegExp(
    r'[0-9a-f]{40}$',

    /// 忽略大小写
    caseSensitive: false,
  );
  @override
  void initState() {
    super.initState();
    if (widget.addressEditingController != null) {
      this.addressEditingController.text =
          widget.addressEditingController.address;
    }

    isAuth = ConfigStorageUtils().readAuthOfKey(ConfigKey.IsAuth);

    // estimateGasInterval = Timer.periodic(Duration(seconds: 1), (timer) async {
    //   if (!estimating) {
    //     estimating = true;
    //     try {
    //       await payload();
    //     } finally {
    //       estimating = false;
    //     }
    //   }
    // });
    payload();
    _getbalance();
    coin = CoreProvider.of(context).networkConfig.coinBlog.symbol;
  }

  //获取主币余额
  _getbalance() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.

    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.

    } else if (connectivityResult == ConnectivityResult.none) {
      return Fluttertoast.showToast(
        msg: S.current.network_error,
      );
    }
    await Utils.futureGetBalance(
      widget.web3Client,
      CoreProvider.of(context).networkConfig.coinBlog,
      CoreProvider.of(context).account.address,
    ).then((value) => this.coinBalance = value);
  }

  Future<void> payload() async {
    /// 获取当前地址，当前币种的余额信息
    await Utils.futureGetBalance(
      widget.web3Client,
      widget.tokenBlog,
      widget.fromAccount.address,
    ).then(
      (balance) => this.balance = balance,
    );

    /// 估算GasPrice
    await Utils.estimatedGasPrice(
      widget.networkConfig,
      widget.dataSourceProvider,
      widget.web3Client,
      widget.scanAPIService,
    ).then((value) => gasPriceResult = value);

    gasPrice = Amount(
      value: gasPriceResult.gasConfig.propose,
      decimals: widget.networkConfig.feeTokenBlog.decimals,
    );

    // 尝试获取granularity接口,若出现异常则对应的合约没有实现接口
    // 不做处理,但不能影响后面的流程，该功能可以用于限制最低转账额度
    if (widget.tokenBlog.isContract) {
      try {
        final granularityFunction = ContractFunction(
          'granularity',
          [],
          outputs: [
            FunctionParameter(null, UintType()),
          ],
          type: ContractFunctionType.function,
          mutability: StateMutability.view,
        );

        final granularityResponse = await widget.web3Client.call(
          from: widget.fromAccount.address,
          to: widget.tokenBlog.address,
          data: granularityFunction.encodeCall(),
        );

        granularity = await granularityFunction
            .decodeReturnValues(granularityResponse)
            .first as BigInt;
      } catch (e) {
        granularity = BigInt.one;
      }
    }

    /// 估算Gas
    Uint8List transactionData;

    /// 合约交易
    if (widget.tokenBlog.isContract) {
      final transferFunction = ContractFunction(
        'transfer',
        [
          FunctionParameter(null, AddressType()),
          FunctionParameter(null, UintType()),
        ],
        outputs: [
          FunctionParameter(null, BoolType()),
        ],
        type: ContractFunctionType.function,
        mutability: StateMutability.nonPayable,
      );

      transactionData = transferFunction.encodeCall([
        // widget.toAccount.address,
        widget.addressEditingController != null
            ? widget.addressEditingController.address
            : addressEditingController.text,
        inputValue != null
            ? inputValue.value
            : Amount(
                // value: granularity,
                value: BigInt.one,
                decimals: widget.tokenBlog.decimals,
              ).value,
      ]);
    }

    try {
      /// gas 在转主币到合约时可能由于合约实现的逻辑导致报错
      if (widget.tokenBlog.isContract) {
        gas = await widget.web3Client.estimateGas(
          from: widget.fromAccount.address,
          to: widget.tokenBlog.isContract
              ? widget.tokenBlog.address
              // : widget.toAccount.address,
              : widget.addressEditingController != null
                  ? widget.addressEditingController.address
                  : addressEditingController.text,
          data: transactionData,
        );
      } else {
        /// 主币精度一定是18位
        gas = await widget.web3Client.estimateGas(
          from: widget.fromAccount.address,
          to: widget.tokenBlog.isContract
              ? widget.tokenBlog.address
              // : widget.toAccount.address,
              : widget.addressEditingController != null
                  ? widget.addressEditingController.address
                  : addressEditingController.text,
          value: Amount.fromString(
            sentAmountTextEditingController.text,
            decimals: 18,
            valueDecimals: 18,
          ).value,
          data: null,
        );
      }
    } catch (e) {
      gas = BigInt.from(21000);
    }

    /// 防止一些主链的bug，估算和实际发送不匹配，这里直接上调10%
    if (widget.tokenBlog.isContract) {
      gas = gas * BigInt.from(12) ~/ BigInt.from(10);
    }

    payloadListenable.value = true;
  }

  void handSentAllMode() {
    if (inSentAllModule) {
      /// 代币的SentAll表示全部的剩余量
      if (widget.tokenBlog.isContract) {
        inputValue = balance;
      }

      /// 计算主币最大转出量
      else {
        inputValue = Amount(
          value: balance.value - (gas * gasPrice.value),
          decimals: balance.decimals,
        );
      }

      sentAmountTextEditingController.text = inputValue.toStringAsFixed();
    }
  }

  Future sentTransaction() async {
    if (addressEditingController.text == null ||
        addressEditingController.text.length <= 0) {
      return Fluttertoast.showToast(
        msg: S.current.ethereum_home_transfer_plaease_input_adress,
      );
    }
    var dw = addressEditingController.text;
    if (addressRegexp.hasMatch(dw) == false ||
        addressEditingController.text.length > 42) {
      return Fluttertoast.showToast(
          msg: S
              .current.ethereum_home_transfer_plaease_check_address_is_entered);
    }

    /// 金额没有输入，不提示，唯一的输入框都不填值，提示个毛，只有测试干这事
    if (sentAmountTextEditingController.text == null ||
        sentAmountTextEditingController.text.length <= 0) {
      return Fluttertoast.showToast(
          msg: S.current.ethereum_home_transfer_plaease_input_number);
    }

    try {
      if (feer().value > this.coinBalance.value) {
        Fluttertoast.showToast(
          msg: S.current
                  .ethereum_home_transfer_insufficient_handling_charges_one +
              '${this.coin} ' +
              S.current
                  .ethereum_home_transfer_insufficient_handling_charges_two,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 8,
          webShowClose: true,
        );
        return;
      }
    } catch (e) {}

    try {
      if (double.parse(sentAmountTextEditingController.text) >
          double.parse(balance.toStringAsFixed().replaceAll(",", ''))) {
        return Fluttertoast.showToast(
            msg: S.current.ethereum_home_transfer_lack_of_balance);
      }
    } catch (e) {
      return Fluttertoast.showToast(
          msg: S.current.ethereum_home_transfer_plaease_input_number);
    }

    if (granularity != BigInt.zero) if (inputValue.value % granularity !=
        BigInt.zero) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        msg: S.current.ethereum_home_transfer_granularity.replaceFirst(
          '%d',
          Amount(
            value: granularity,
            decimals: widget.tokenBlog.decimals,
          ).toStringAsFixed(
            fractionDigits: 0,
          ),
        ),
      );
      return;
    }

    if (isAuth == false) {
      final correctPwd = await VerifySheets.shows<String>(
        context,
        verifyCallback: (context, pwd) {
          final verifySuccess =
              CoreProvider.of(context).wallet.verifySecurity(pwd);
          if (!verifySuccess) {
            return S.current.password_verify_faild;
          } else {
            /// 验证成功后，先收起密码验证输入框
            Navigator.of(context).pop<String>(pwd);
            return null;
          }
        },
      );

      if (correctPwd != null) {
        Utils.futureTokenTransfer(
          widget.web3Client,
          widget.tokenBlog,
          widget.fromAccount,
          // widget.toAccount.address,
          widget.addressEditingController != null
              ? widget.addressEditingController.address
              : addressEditingController.text,
          inputValue,
          gas,
          gasPrice.value,
        ).then((txHash) => FinishAnimation.show(context, onCompleted: () {
              Navigator.of(context).pop<String>(txHash);
            }));

        await widget.dataSourceProvider.localService
            .insertRecentRecipientAddress(this.addressEditingController.text);
        setState(() {});
      }
    } else {
      final isAuthenticated = await localAuthApi.authenticate();
      if (isAuthenticated != false) {
        Utils.futureTokenTransfer(
          widget.web3Client,
          widget.tokenBlog,
          widget.fromAccount,
          // widget.toAccount.address,
          widget.addressEditingController != null
              ? widget.addressEditingController.address
              : addressEditingController.text,
          inputValue,
          gas,
          gasPrice.value,
        ).then((txHash) => FinishAnimation.show(context, onCompleted: () {
              Navigator.of(context).pop<String>(txHash);
            }));

        await widget.dataSourceProvider.localService
            .insertRecentRecipientAddress(this.addressEditingController.text);
        setState(() {});
      } else {
        Fluttertoast.showToast(
          msg: S.current.ethereum_trade_fingerprint_not,
          gravity: ToastGravity.CENTER_LEFT,
        );
      }
    }
  }

  Amount feer() => Amount.fromString(
        (gas * gasPrice.value).toString(),
        decimals: widget.networkConfig.feeTokenBlog.decimals,
      );

  /// 转化为手续费计算过程的Striing
  String feeString() =>
      Amount.fromString(
        (gas * gasPrice.value).toString(),
        decimals: widget.networkConfig.feeTokenBlog.decimals,
      ).toStringAsFixed(fractionDigits: 8) +
      ' ${widget.networkConfig.feeTokenBlog.symbol}';

  /*等待返回数据是异步操作*/
  void skipToPageD() async {
    // final result = await Navigator.pushNamed(context, pageD);
    final result = await Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        ScanPage(),
      ),
    );

    String p = result as String;

    this.addressEditingController.text = p;
    payload();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        // backgroundColor: ThemeUtils().getColor(
        //   'utils.view_controller.background',
        // ),
        backgroundColor: Color.fromRGBO(250, 250, 250, 1),
        appBar: NavigationBar(
          title: 'Transfer ' + widget.tokenBlog.symbol,
          // bottom: navigationBarBottom(context),

          actions: [
            Container(
              margin: EdgeInsets.only(right: ScreenUtil().setWidth(20)),
              width: ScreenUtil().setWidth(20),
              child: IconButton(
                padding: EdgeInsets.all(0),
                // icon: Icon(Icons.crop_free_sharp),
                icon: Icon(
                  const IconData(
                    0xe60a,
                    fontFamily: "myIcon",
                    fontPackage: "wallet_flutter",
                  ),
                ),
                onPressed: () {
                  skipToPageD();
                },
              ),
            ),
          ],
        ),
        body: OrientationBuilder(
          builder: (context, orientation) {
            return SingleChildScrollView(
              controller: scrollController,
              physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  sentAmountInputer(context),

                  /// 监听手续费估算结果，由于可能会增加轮询获取功能，没有选用futureBuilder
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(20),
                      right: ScreenUtil().setWidth(20),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    child: ValueListenableBuilder<bool>(
                      valueListenable: payloadListenable,
                      builder: (context, success, child) => !success
                          ? Container()
                          : Container(
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      // color: Colors.black12,
                                      color: Color.fromRGBO(0, 0, 0, 0.1),
                                      offset: Offset(0.0, 0.5), //阴影xy轴偏移量
                                      blurRadius: 1.0, //阴影模糊程度
                                      spreadRadius: 0.0 //阴影扩散程度
                                      )
                                ],
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                              ),
                              padding: EdgeInsets.fromLTRB(
                                ScreenUtil().setWidth(20),
                                0,
                                ScreenUtil().setWidth(20),
                                0,
                              ),
                              // margin: EdgeInsets.fromLTRB(
                              //   0,
                              //   0,
                              //   0,
                              //   0,
                              // ),
                              child: Utils.FeeConfigBoard(
                                titleStyle: ThemeUtils().getTextStyle(
                                  'utils.input.textstyle.title',
                                ),
                                defaultValue:
                                    (BigInt min, BigInt max, BigInt curr) {
                                  return (curr - min).toDouble() /
                                      (max - min).toDouble();
                                }(
                                  gasPriceResult.gasConfig.safe,
                                  gasPriceResult.gasConfig.fast,
                                  gasPrice.value,
                                ),
                                feeTokenBlog: widget.networkConfig.feeTokenBlog,
                                totalStringCallBack: (v) => feeString(),
                                lableCallBack: (v) {
                                  final gasPriceWei = gasPriceResult
                                          .gasConfig.safe +
                                      (gasPriceResult.gasConfig.fast -
                                              gasPriceResult.gasConfig.safe) *
                                          BigInt.from((v * 100).toInt()) ~/
                                          BigInt.from(100);

                                  final gasPriceAmount = Amount(
                                    value: gasPriceWei,
                                    decimals: 9,
                                  );

                                  return '${gasPriceAmount.toStringAsFixed()} Gwei';
                                },
                                onChanged: (v) {
                                  final gasPriceWei = gasPriceResult
                                          .gasConfig.safe +
                                      (gasPriceResult.gasConfig.fast -
                                              gasPriceResult.gasConfig.safe) *
                                          BigInt.from((v * 100).toInt()) ~/
                                          BigInt.from(100);

                                  this.gasPrice = Amount(
                                    value: gasPriceWei,
                                    decimals: 9,
                                  );

                                  handSentAllMode();
                                },
                              ),
                            ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setHeight(20),
                      vertical: ScreenUtil().setHeight(40),
                    ),
                    child: StatusButton.withStyle(
                      StatusButtonStyle.Info,
                      title: S.current.ethereum_home_transfer_do_sent,
                      onPressedFuture: sentTransaction,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );

  PreferredSize navigationBarBottom(BuildContext context) => PreferredSize(
        preferredSize: Size.fromHeight(ScreenUtil().setHeight(40)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            /// FromAccount
            Container(
              margin: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(10),
                left: ScreenUtil().setWidth(20),
              ),
              width: (ScreenUtil().screenWidth - 70) / 2,
              height: ScreenUtil().setHeight(40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    widget.fromAccount.avatarPath,
                    package: 'wallet_flutter',
                    width: ScreenUtil().setHeight(28),
                    height: ScreenUtil().setHeight(28),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Text(
                    widget.fromAccount.nickName,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.treansfer.textstyle.name_or_address',
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(10)),
              width: ScreenUtil().setHeight(30),
              height: ScreenUtil().setHeight(30),
              child: Icon(
                Icons.send_rounded,
                size: 20,
                color: ThemeUtils().getColor(
                  'utils.navigation_bar.icon',
                ),
              ),
            ),

            /// ToAccount
            Container(
              margin: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(10),
                right: ScreenUtil().setWidth(20),
              ),
              width: (ScreenUtil().screenWidth - 70) / 2,
              height: ScreenUtil().setHeight(40),
            ),
          ],
        ),
      );

  Widget sentAmountInputer(BuildContext context) => Container(
        margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                // color: Colors.black12,
                color: Color.fromRGBO(0, 0, 0, 0.1),
                offset: Offset(0.0, 0.5), //阴影xy轴偏移量
                blurRadius: 1.0, //阴影模糊程度
                spreadRadius: 0.0 //阴影扩散程度
                )
          ],
          color: Colors.white,
        ),
        padding: EdgeInsets.only(
          left: ScreenUtil().setHeight(20),
          right: ScreenUtil().setHeight(20),
          top: ScreenUtil().setHeight(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: ScreenUtil().setHeight(30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    S.current.ethereum_trade_toadress,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                    ),
                  ),
                ],
              ),
            ),
            Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.grey[200],
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      // width: ScreenUtil().setWidth(260),
                      width: MediaQuery.of(context).size.width -
                          ScreenUtil().setWidth(120),

                      child: TextFormField(
                        focusNode: inputFocusNode,
                        controller: addressEditingController,
                        autofocus: false,
                        onChanged: (_) => setState(() {
                          payload();
                        }),
                        style: ThemeUtils()
                            .getTextStyle(
                              'utils.input.textstyle.content',
                            )
                            .copyWith(fontSize: 15),
                        decoration: InputDecoration(
                          hintText: S.current
                              .ethereum_home_address_selector_placeholder,
                          hintStyle: ThemeUtils().getTextStyle(
                            'utils.input.textstyle.hit',
                          ),
                          suffixIconConstraints: BoxConstraints.expand(
                            width: ScreenUtil().setWidth(40),
                            height: ScreenUtil().setHeight(40),
                          ),
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          prefixIconConstraints: BoxConstraints.expand(
                            width: ScreenUtil().setWidth(40),
                            height: ScreenUtil().setHeight(40),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                      width: ScreenUtil().setWidth(20),
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        // icon: Icon(Icons.crop_free_sharp),
                        icon: Image.asset(
                          'assets/imgs/address_book.png',
                          package: "wallet_flutter",
                        ),
                        onPressed: () async {
                          final address = await Navigator.of(context).push(
                            ViewAnimations.viewRightIn(
                              AddressSelector(
                                recentAccounts: [],
                                localKeys: [],
                                dataSourceProvider: widget.dataSourceProvider,
                              ),
                            ),
                          );

                          setState(() {
                            this.addressEditingController.text = address;
                          });
                          payload();
                        },
                      ),
                    ),
                  ],
                )),
            SizedBox(height: ScreenUtil().setHeight(10)),

            Container(
              height: ScreenUtil().setHeight(30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    S.current.ethereum_trade_amount,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1,
                    color: Colors.grey[200],
                  ),
                ),
              ),
              child: TextFormField(
                // focusNode: inputFocusNode,
                controller: sentAmountTextEditingController,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  // FilteringTextInputFormatter.digitsOnly, //数字，只能是整数
                  FilteringTextInputFormatter.allow(RegExp("[0-9.]")), //数字包括小数
                  // FilteringTextInputFormatter.allow(
                  //     RegExp("[a-zA-Z]")), //只允许输入字母
                ],
                autofocus: false,
                onChanged: (value) {
                  inputValue = Amount.fromString(
                    value,
                    decimals: widget.tokenBlog.decimals,
                    valueDecimals: widget.tokenBlog.decimals,
                  );

                  if (inSentAllModule) {
                    inSentAllModule = false;
                  }
                  setState(() {
                    payload();
                  });
                },
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(19),
                    color: Color.fromRGBO(72, 118, 255, 1)),
                decoration: InputDecoration(
                  hintText: '0.00',
                  hintStyle: ThemeUtils().getTextStyle(
                    'utils.input.textstyle.hit',
                  ),
                  suffixIconConstraints: BoxConstraints.expand(
                    width: ScreenUtil().setWidth(40),
                    height: ScreenUtil().setHeight(40),
                  ),
                  suffixText: widget.tokenBlog.symbol,
                  suffixStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(19),
                  ),
                  border: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  prefixIconConstraints: BoxConstraints.expand(
                    width: ScreenUtil().setWidth(40),
                    height: ScreenUtil().setHeight(40),
                  ),
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(10)),

            /// Balance
            ValueListenableBuilder<bool>(
              valueListenable: payloadListenable,
              builder: (context, success, child) => Row(
                children: [
                  Text(
                    !success
                        ? S.current.ethereum_home_transfer_loading
                        : S.current.ethereum_home_transfer_balance_tip +
                            balance.toStringAsFixed() +
                            ' ' +
                            widget.tokenBlog.symbol,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.treansfer.textstyle.bottom_balance',
                    ),
                  ),
                  Expanded(child: SizedBox()),
                  GestureDetector(
                    child: Text(
                      S.current.ethereum_home_transfer_sent_all,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(13),
                          color: Color.fromRGBO(72, 118, 255, 1)),
                    ),
                    onTap: () {
                      inSentAllModule = true;
                      handSentAllMode();
                    },
                  )
                ],
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(16)),
          ],
        ),
      );

  @override
  void dispose() {
    scrollController.dispose();
    addressEditingController.dispose();
    super.dispose();
  }
}
