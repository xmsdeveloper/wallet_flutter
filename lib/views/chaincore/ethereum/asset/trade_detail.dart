// @dart=2.9
part of '../asset.dart';

class TradeDetailPage extends StatefulWidget {
  final String ownerAccount;
  final bool txSuccess;
  final Widget suffix;
  final int timeStampSec;
  final String tokenSymbol;
  final String shaCode;
  final String gas;
  final String to;
  final String from;
  final String blockchin;
  final Widget suffixDetail;

  TradeDetailPage({
    this.tokenSymbol,
    this.ownerAccount,
    this.txSuccess,
    this.suffix,
    this.timeStampSec,
    this.shaCode,
    this.gas,
    this.to,
    this.from,
    this.blockchin,
    this.suffixDetail,
  });
  @override
  _TradeDetailPageState createState() => _TradeDetailPageState();
}

class _TradeDetailPageState extends State<TradeDetailPage> {
  // //转出判断
  // bool get isTransferOut =>
  //     this.widget.tx.from.toLowerCase() ==
  //     this.widget.ownerAccount.toLowerCase();

  // //转入判断
  // bool get isTransferIn =>
  //     this.widget.tx.to.toLowerCase() == this.widget.ownerAccount.toLowerCase();

  // bool get isTransferSelf =>
  //     this.widget.tx.from.toLowerCase() == this.widget.tx.to.toLowerCase();

  List datasource;

  @override
  void initState() {
    super.initState();
    print(widget.tokenSymbol);
    datasource = [
      {
        "title": S.current.ethereum_trade_detail_transaction_time,
        "subtext": DateFormat('yyyy-MM-dd HH:mm').format(
          DateTime.fromMillisecondsSinceEpoch(
            this.widget.timeStampSec * 1000,
          ),
        ),
      },
      {
        "title": S.current.ethereum_trade_detail_transaction_type,
        "subtext": S.current.ethereum_trade_detail_transfer_accounts
      },
      {
        "title": S.current.ethereum_trade_detail_receiving_address,
        "subtext": this.widget.to,
      },
      {
        "title": S.current.ethereum_trade_detail_sending_address,
        "subtext": this.widget.from,
      },
      {
        "title": S.current.ethereum_trade_detail_miners_fee,
        "subtext": this.widget.gas,
      },
      {
        "title": S.current.ethereum_trade_detail_transaction_number,
        "subtext": this.widget.shaCode,
      },
      {
        "title": S.current.ethereum_trade_detail_block,
        "subtext": this.widget.blockchin,
      },
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_trade_detail_title,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(ScreenUtil().setHeight(40)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(10)),
                child: this.widget.suffixDetail,
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(15),
                  bottom: ScreenUtil().setHeight(10),
                ),
                height: ScreenUtil().setHeight(50),
                width: ScreenUtil().setWidth(50),
                child: Image.asset(
                  'assets/imgs/sts_successed.png',
                  package: 'wallet_flutter',
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  bottom: ScreenUtil().setHeight(20),
                ),
                child: Text(
                  this.widget.txSuccess
                      ? S.current.ethereum_home_detail_tx_state_succses
                      : S.current.ethereum_home_detail_tx_state_failed,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.trade_detail.textstyle.state_title',
                  ),
                ),
              ),
              Column(
                children: this
                    .datasource
                    .map(
                      (e) => Container(
                        width: ScreenUtil().setWidth(335),
                        height: ScreenUtil().setHeight(69),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              width: ScreenUtil().setWidth(1),
                              color: Colors.grey[300],
                            ),
                          ),
                        ),
                        margin: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(20),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              child: Text(
                                e['title'],
                                style: ThemeUtils().getTextStyle(
                                  'views.chaincore.ethereum.trade_detail.textstyle.title',
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Clipboard.setData(
                                  ClipboardData(
                                    text: e['subtext'],
                                  ),
                                );
                                Fluttertoast.showToast(
                                  msg: S.current.copy_success,
                                );
                              },
                              child: Container(
                                child: Text(
                                  e['subtext'],
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.trade_detail.textstyle.content',
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                    .toList(),
              ),
              CoreProvider.of(context).networkConfig.mainSymbol != "ETH"
                  ?
              Container(
                child: ListTile(
                  onTap: () {
                    openDapp(
                        context,
                        Dapp(
                          url: CoreProvider.of(context)
                                  .networkConfig
                                  .endpoints[0]
                                  .scanURL
                                  .replaceAll("api.", "")
                                  .replaceAll('api-', '') +
                              '/tx/' +
                              widget.shaCode,
                          logo: "",
                        ));
                  },
                  contentPadding: EdgeInsets.only(
                    top: ScreenUtil().setHeight(15),
                    left: ScreenUtil().setWidth(60),
                    right: ScreenUtil().setWidth(60),
                  ),
                  minLeadingWidth: ScreenUtil().setWidth(10),
                  leading: Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(5),
                    ),
                    child: Icon(
                      Icons.add_photo_alternate,
                      size: ScreenUtil().setHeight(18),
                      color: Colors.blue,
                    ),
                  ),
                  title: Text(
                    S.current.ethereum_trade_detail_jump_title,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.trade_detail.textstyle.jump_title',
                    ),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: ScreenUtil().setHeight(13),
                    color: ThemeUtils().getColor(
                      'views.chaincore.ethereum.settings.header_background',
                    ),
                  ),
                ),
              )
              : Container(),
            ],
          )
        ],
      ),
    );
  }

  void openDapp(BuildContext context, Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: null,
          dataSourceProvider: null,
          isTips: false,
        ),
      ),
    );
  }
}
