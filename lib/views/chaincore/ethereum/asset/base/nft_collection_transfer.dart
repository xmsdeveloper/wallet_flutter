// @dart=2.9
part of '../../asset.dart';

// ignore: must_be_immutable
class NftCollectionTransfer extends StatefulWidget {
  final String imgUrl;

  final String tokenId;
  final String name;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final Web3ClientProvider web3clientProvider;
  Ethereum.ScanAPIService scanAPIService;
  final TokenBlog nftTokenblog;
  final WalletKey account;
  final String amount;

  NftCollectionTransfer(
      {Key key,
      this.imgUrl,
      this.tokenId,
      this.name,
      this.dataSourceProvider,
      this.networkConfig,
      this.web3clientProvider,
      this.scanAPIService,
      this.nftTokenblog,
      this.account,
      this.amount})
      : super(key: key);

  @override
  State<NftCollectionTransfer> createState() => _NftCollectionTransferState();
}

class _NftCollectionTransferState extends State<NftCollectionTransfer> {
  final ScrollController scrollController = ScrollController();
  final TextEditingController sentAmountTextEditingController =
      TextEditingController(text: '');

  ValueNotifier<bool> payloadListenable = ValueNotifier<bool>(false);
  final TextEditingController addressEditingController =
      TextEditingController();
  final FocusNode inputFocusNode = FocusNode();

  Amount inputValue;

  Utils.EstimatedGasPriceResult gasPriceResult;

  BigInt gas;

  Amount gasPrice;

  BigInt granularity = BigInt.one;

  BigInt erc1155InputValue = BigInt.one;

  /// 正确地址的正则表达式
  final addressRegexp = RegExp(
    r'[0-9a-f]{40}$',

    /// 忽略大小写
    caseSensitive: false,
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    payload();
    payload();
    inputValue = Amount(
      // value: granularity,
      value: BigInt.one,
      decimals: 18,
    );
  }

  Future<void> payload() async {
    /// 估算GasPrice
    await Utils.estimatedGasPrice(
      widget.networkConfig,
      widget.dataSourceProvider,
      widget.web3clientProvider,
      widget.scanAPIService,
    ).then((value) => gasPriceResult = value);

    gasPrice = Amount(
      value: gasPriceResult.gasConfig.propose,
      decimals: 18,
    );

    // 尝试获取granularity接口,若出现异常则对应的合约没有实现接口
    // 不做处理,但不能影响后面的流程，该功能可以用于限制最低转账额度
    try {
      final granularityFunction = ContractFunction(
        'granularity',
        [],
        outputs: [
          FunctionParameter(null, UintType()),
        ],
        type: ContractFunctionType.function,
        mutability: StateMutability.view,
      );

      final granularityResponse = await widget.web3clientProvider.call(
        from: widget.account.address,
        to: widget.nftTokenblog.address,
        data: granularityFunction.encodeCall(),
      );

      granularity = await granularityFunction
          .decodeReturnValues(granularityResponse)
          .first as BigInt;
    } catch (e) {
      granularity = BigInt.one;
    }

    /// 估算Gas
    Uint8List transactionData;

    /// 合约交易
    final transferFunction = ContractFunction(
      'transferFrom',
      [
        FunctionParameter(null, AddressType()),
        FunctionParameter(null, AddressType()),
        FunctionParameter(null, UintType()),
      ],
      outputs: [
        FunctionParameter(null, BoolType()),
      ],
      type: ContractFunctionType.function,
      mutability: StateMutability.nonPayable,
    );

    transactionData = transferFunction.encodeCall([
      // widget.toAccount.address,
      widget.account.address,
      addressEditingController.text,
      BigInt.from(int.parse(widget.tokenId)),
    ]);

    try {
      /// gas 在转主币到合约时可能由于合约实现的逻辑导致报错
      gas = await widget.web3clientProvider.estimateGas(
        from: widget.account.address,
        // to: addressEditingController.text,
        to: addressEditingController.text,
        data: transactionData,
      );
    } catch (e) {
      gas = BigInt.from(21000);
    }

    /// 防止一些主链的bug，估算和实际发送不匹配，这里直接上调10%
    gas = gas * BigInt.from(60) ~/ BigInt.from(10);

    payloadListenable.value = true;
  }

  Future sentTransaction() async {
    if (addressEditingController.text == null ||
        addressEditingController.text.length <= 0) {
      return Fluttertoast.showToast(
        msg: S.current.ethereum_home_transfer_plaease_input_adress,
      );
    }
    var dw = addressEditingController.text;
    if (addressRegexp.hasMatch(dw) == false ||
        addressEditingController.text.length > 42) {
      return Fluttertoast.showToast(
          msg: S
              .current.ethereum_home_transfer_plaease_check_address_is_entered);
    }
    final correctPwd = await VerifySheets.shows<String>(
      context,
      verifyCallback: (context, pwd) {
        final verifySuccess =
            CoreProvider.of(context).wallet.verifySecurity(pwd);
        if (!verifySuccess) {
          return S.current.password_verify_faild;
        } else {
          /// 验证成功后，先收起密码验证输入框
          Navigator.of(context).pop<String>(pwd);
          return null;
        }
      },
    );

    if (correctPwd != null) {
      Utils.futureErc721TokenTransfer(
        widget.web3clientProvider,
        widget.nftTokenblog,
        widget.account,
        // widget.toAccount.address,
        addressEditingController.text,
        BigInt.from(int.parse(widget.tokenId)),
        gas,
        gasPrice.value,
      ).then((txHash) => FinishAnimation.show(context, onCompleted: () {
            Navigator.of(context).pop<String>(txHash);
          }));

      await widget.dataSourceProvider.localService
          .insertRecentRecipientAddress(this.addressEditingController.text);
      setState(() {});
    }
  }

  Future sent1155Transaction() async {
    if (addressEditingController.text == null ||
        addressEditingController.text.length <= 0) {
      return Fluttertoast.showToast(
        msg: S.current.ethereum_home_transfer_plaease_input_adress,
      );
    }
    var dw = addressEditingController.text;
    if (addressRegexp.hasMatch(dw) == false ||
        addressEditingController.text.length > 42) {
      return Fluttertoast.showToast(
          msg: S
              .current.ethereum_home_transfer_plaease_check_address_is_entered);
    }
    final correctPwd = await VerifySheets.shows<String>(
      context,
      verifyCallback: (context, pwd) {
        final verifySuccess =
            CoreProvider.of(context).wallet.verifySecurity(pwd);
        if (!verifySuccess) {
          return S.current.password_verify_faild;
        } else {
          /// 验证成功后，先收起密码验证输入框
          Navigator.of(context).pop<String>(pwd);
          return null;
        }
      },
    );

    if (correctPwd != null) {
      Utils.futureErc1155TokenTransfer(
        widget.web3clientProvider,
        widget.nftTokenblog,
        widget.account,
        // widget.toAccount.address,
        addressEditingController.text,
        BigInt.from(int.parse(widget.tokenId)),
        erc1155InputValue ?? BigInt.one,
        gas,
        gasPrice.value,
        "2323",
      ).then((txHash) => FinishAnimation.show(context, onCompleted: () {
            print(txHash);
            Navigator.of(context).pop<String>(txHash);
          }));

      await widget.dataSourceProvider.localService
          .insertRecentRecipientAddress(this.addressEditingController.text);
      setState(() {});
    }
  }

  /// 转化为手续费计算过程的Striing
  String feeString() =>
      Amount.fromString(
        (gas * gasPrice.value).toString(),
        decimals: 18,
      ).toStringAsFixed(fractionDigits: 8) +
      ' ${widget.networkConfig.feeTokenBlog.symbol}';

  @override
  Widget build(BuildContext context) => Scaffold(
        // backgroundColor: ThemeUtils().getColor(
        //   'utils.view_controller.background',
        // ),
        backgroundColor: Color.fromRGBO(250, 250, 250, 1),
        appBar: NavigationBar(
          title: 'Transfer',
          // bottom: navigationBarBottom(context),
        ),
        body: OrientationBuilder(
          builder: (context, orientation) {
            return SingleChildScrollView(
              controller: scrollController,
              physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  sentAmountInputer(context),

                  /// 监听手续费估算结果，由于可能会增加轮询获取功能，没有选用futureBuilder
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(20),
                      right: ScreenUtil().setWidth(20),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    child: ValueListenableBuilder<bool>(
                      valueListenable: payloadListenable,
                      builder: (context, success, child) => !success
                          ? Container()
                          : Container(
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      // color: Colors.black12,
                                      color: Color.fromRGBO(0, 0, 0, 0.1),
                                      offset: Offset(0.0, 0.5), //阴影xy轴偏移量
                                      blurRadius: 1.0, //阴影模糊程度
                                      spreadRadius: 0.0 //阴影扩散程度
                                      )
                                ],
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                              ),
                              padding: EdgeInsets.fromLTRB(
                                ScreenUtil().setWidth(20),
                                0,
                                ScreenUtil().setWidth(20),
                                0,
                              ),
                              // margin: EdgeInsets.fromLTRB(
                              //   0,
                              //   0,
                              //   0,
                              //   0,
                              // ),
                              child: Utils.FeeConfigBoard(
                                titleStyle: ThemeUtils().getTextStyle(
                                  'utils.input.textstyle.title',
                                ),
                                defaultValue:
                                    (BigInt min, BigInt max, BigInt curr) {
                                  return (curr - min).toDouble() /
                                      (max - min).toDouble();
                                }(
                                  gasPriceResult.gasConfig.safe,
                                  gasPriceResult.gasConfig.fast,
                                  gasPrice.value,
                                ),
                                feeTokenBlog: widget.networkConfig.feeTokenBlog,
                                totalStringCallBack: (v) => feeString(),
                                lableCallBack: (v) {
                                  final gasPriceWei = gasPriceResult
                                          .gasConfig.safe +
                                      (gasPriceResult.gasConfig.fast -
                                              gasPriceResult.gasConfig.safe) *
                                          BigInt.from((v * 100).toInt()) ~/
                                          BigInt.from(100);

                                  final gasPriceAmount = Amount(
                                    value: gasPriceWei,
                                    decimals: 9,
                                  );

                                  return '${gasPriceAmount.toStringAsFixed()} Gwei';
                                },
                                onChanged: (v) {
                                  final gasPriceWei = gasPriceResult
                                          .gasConfig.safe +
                                      (gasPriceResult.gasConfig.fast -
                                              gasPriceResult.gasConfig.safe) *
                                          BigInt.from((v * 100).toInt()) ~/
                                          BigInt.from(100);

                                  this.gasPrice = Amount(
                                    value: gasPriceWei,
                                    decimals: 9,
                                  );
                                },
                              ),
                            ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setHeight(20),
                      vertical: ScreenUtil().setHeight(40),
                    ),
                    child: StatusButton.withStyle(
                      StatusButtonStyle.Info,
                      title: S.current.sheet_title,
                      onPressedFuture:
                          widget.nftTokenblog.protocol.toLowerCase() == "erc721"
                              ? sentTransaction
                              : sent1155Transaction,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );

  Widget sentAmountInputer(BuildContext context) => Container(
        margin: EdgeInsets.all(ScreenUtil().setWidth(22)),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                // color: Colors.black12,
                color: Color.fromRGBO(0, 0, 0, 0.1),
                offset: Offset(0.0, 0.5), //阴影xy轴偏移量
                blurRadius: 1.0, //阴影模糊程度
                spreadRadius: 0.0 //阴影扩散程度
                )
          ],
          color: Colors.white,
        ),
        padding: EdgeInsets.only(
          left: ScreenUtil().setHeight(20),
          right: ScreenUtil().setHeight(20),
          top: ScreenUtil().setHeight(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: ScreenUtil().setHeight(30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    S.current.ethereum_trade_toadress,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                    ),
                  ),
                ],
              ),
            ),
            Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.grey[200],
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      // width: ScreenUtil().setWidth(260),
                      width: MediaQuery.of(context).size.width -
                          ScreenUtil().setWidth(120),

                      child: TextFormField(
                        focusNode: inputFocusNode,
                        controller: addressEditingController,
                        autofocus: false,
                        onChanged: (_) => setState(() {}),
                        style: ThemeUtils()
                            .getTextStyle(
                              'utils.input.textstyle.content',
                            )
                            .copyWith(fontSize: 15),
                        decoration: InputDecoration(
                          hintText: S.current
                              .ethereum_home_address_selector_placeholder,
                          hintStyle: ThemeUtils().getTextStyle(
                            'utils.input.textstyle.hit',
                          ),
                          suffixIconConstraints: BoxConstraints.expand(
                            width: ScreenUtil().setWidth(40),
                            height: ScreenUtil().setHeight(40),
                          ),
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          prefixIconConstraints: BoxConstraints.expand(
                            width: ScreenUtil().setWidth(40),
                            height: ScreenUtil().setHeight(40),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                      width: ScreenUtil().setWidth(20),
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        // icon: Icon(Icons.crop_free_sharp),
                        icon: Image.asset(
                          'assets/imgs/address_book.png',
                          package: "wallet_flutter",
                        ),
                        onPressed: () async {
                          final address = await Navigator.of(context).push(
                            ViewAnimations.viewRightIn(
                              AddressSelector(
                                recentAccounts: [],
                                localKeys: [],
                                dataSourceProvider: widget.dataSourceProvider,
                              ),
                            ),
                          );

                          setState(() {
                            this.addressEditingController.text = address;
                          });
                        },
                      ),
                    ),
                  ],
                )),
            SizedBox(height: ScreenUtil().setHeight(10)),
            Container(
              height: ScreenUtil().setHeight(30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    S.current.ethereum_nft_asset_management_send_nft,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(10),
              ),
              child: Container(
                  margin: EdgeInsets.only(),
                  decoration: BoxDecoration(
                    // color: Colors.grey[100],

                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      nftInfo(),
                      widget.nftTokenblog.protocol.toLowerCase() == "erc1155"
                          ? Container(
                              // width: MediaQuery.of(context).size.width / 2 -
                              //     ScreenUtil().setWidth(42),

                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  NumberControllerWidget(
                                    width: ScreenUtil().setWidth(40),
                                    height: ScreenUtil().setHeight(30),
                                    iconWidth: ScreenUtil().setSp(40),
                                    numText: '1',
                                    maxNum: widget.amount,
                                    addValueChanged: (num) {},
                                    removeValueChanged: (num) {},
                                    updateValueChanged: (num) {
                                      erc1155InputValue = BigInt.from(num);
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(10),
                                    ),
                                    child: Text(
                                      S.current
                                              .ethereum_asset_management_home_asset_balance +
                                          "${widget.amount}",
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(14),
                                        color: Colors.grey,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container()
                    ],
                  )),
            ),
            SizedBox(height: ScreenUtil().setHeight(10)),
            SizedBox(height: ScreenUtil().setHeight(10)),
            SizedBox(height: ScreenUtil().setHeight(16)),
          ],
        ),
      );

  @override
  void dispose() {
    scrollController.dispose();
    addressEditingController.dispose();
    super.dispose();
  }

  Widget nftInfo() {
    return Container(
      width: MediaQuery.of(context).size.width / 2 - ScreenUtil().setWidth(42),
      child: Row(
        children: [
          Container(
              width: ScreenUtil().setWidth(56),
              height: ScreenUtil().setWidth(56),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: CachedImage.assetNetwork(
                  fit: BoxFit.fill,
                  url: widget.imgUrl,
                  // url:
                  //     e['normalized_metadata']
                  //         ["image"],
                  placeholder: Image.asset(
                    'assets/imgs/nft_default3.png',
                    package: 'wallet_flutter',
                    fit: BoxFit.fill,
                  ),

                  imageCacheHeight: 100,
                  imageCacheWidth: 100,
                ),
              )),
          Container(
            margin: EdgeInsets.only(
                // left: ScreenUtil().setWidth(10),
                ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    // top: ScreenUtil().setHeight(8),
                    left: ScreenUtil().setWidth(5),
                    bottom: ScreenUtil().setHeight(5),
                  ),
                  child: Text(
                    widget.name ?? "",
                    maxLines: 1,
                    style: TextStyle(),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(5),
                  ),
                  child: Text(
                    "#" + widget.tokenId,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
