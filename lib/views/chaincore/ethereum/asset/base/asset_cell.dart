// @dart=2.9
part of '../../asset.dart';

class AssetCell extends StatefulWidget {
  final int index;
  final int i;
  final Ethereum.TokenBlog tokenBlog;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final WalletKey account;
  final ValueNotifier<DateTime> refreshListenable;
  final NetworkConfig networkConfig;
  final bool isBlack;
  final Color color;

  AssetCell(
    this.index,
    this.i,
    this.tokenBlog,
    this.account,
    this.dataSourceProvider,
    this.networkConfig, {
    this.refreshListenable,
    this.isBlack,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _AssetCellState();
}

class _AssetCellState extends State<AssetCell> {
  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(null);
  final ValueNotifier<String> totalListenable = ValueNotifier<String>("~");
  BigInt height;
  String dollar = "~";
  List data = [];
  bool isCach = false;

  double usdtPrice;
  getLastBlockHight() async {
    height = await Web3ClientProvider.fromHttp(
            url: CoreProvider.of(context).endPoint.nodeURL)
        .getBlockNumber();
  }

  //获取usdt价值多少usd（美元）
  getUPrice() async {
    var header = {'Connection': 'keep-alive'};
    var url = "https://data.gateapi.io/api2/1/ticker/USDT_usd";
    var usdrespose = await http.get(url, headers: header);
    var usdresponseBody = json.decode(usdrespose.body);
    usdtPrice = double.parse(usdresponseBody['last']);
  }

//计算代币价格之和
  getTotalPrice() async {
    await widget.dataSourceProvider.localService.putCachedTotalPrice2(
        widget.account.address,
        (double.parse(this.totalListenable.value != "~"
                    ? this.totalListenable.value
                    : "0") +
                double.parse(await widget.dataSourceProvider.localService
                    .getCachedTotalPrice(widget.account.address)
                    .then((value) => value.balance)))
            .toStringAsFixed(2));

    if (widget.i == 0) {
      await widget.dataSourceProvider.localService.putCachedTotalPrice(
          widget.account.address,
          await widget.dataSourceProvider.localService
              .getCachedTotalPrice2(widget.account.address)
              .then((value) => value.balance));
      Future.delayed(Duration(milliseconds: 0), () {
        widget.dataSourceProvider.localService
            .getCachedTotalPrice(widget.account.address)
            .then((value) {
          eventBus.fire(new AssetEvent(double.parse(value.balance)));
        });
      });
    }
  }

//获取代币价值多少个usdt    有些代币不能直接通过接口得到最新价格，所以统一再次进行转换得到想要的价格
  _getTokenPrice() async {
    // ConfigStorageUtils().writeTotalAmountIndexOfKey(ConfigKey.TotalAmount, 0);
    // await widget.dataSourceProvider.localService.putCachedTotalPrice2(
    //   widget.account.address,
    //   "0",
    // );
    await getUPrice();

    //计算usdt兑换成美元的价格表
    if (widget.tokenBlog.symbol != "USDT" && this.isCach == false) {
      var header = {'Connection': 'keep-alive'};
      var url =
          "https://data.gateapi.io/api2/1/ticker/${widget.tokenBlog.symbol ?? CoreProvider.of(context).networkConfig.mainSymbol}_usdt";

      var usdrespose = await http.get(url, headers: header);
      var usdresponseBody = json.decode(usdrespose.body);
      if (usdresponseBody['last'] != null) {
        try {
          this.totalListenable.value = (double.parse(usdresponseBody['last']) *
                  double.parse(balanceListenable.value
                      .toStringAsFixed()
                      .replaceAll(",", '')) *
                  this.usdtPrice)
              .toStringAsFixed(4);
        } catch (e) {
          this.totalListenable.value = (double.parse(usdresponseBody['last']) *
                  double.parse(balanceListenable.value
                      .toStringAsFixed()
                      .replaceAll(",", '')) *
                  this.usdtPrice)
              .toStringAsFixed(4);
        }
      } else {
        this.totalListenable.value = "~";
      }
    } else {
      try {
        this.totalListenable.value = (double.parse(balanceListenable.value
                    .toStringAsFixed()
                    .replaceAll(",", '')) *
                this.usdtPrice)
            .toStringAsFixed(4);
      } catch (e) {
        this.totalListenable.value = (double.parse(balanceListenable.value
                    .toStringAsFixed()
                    .replaceAll(",", '')) *
                this.usdtPrice)
            .toStringAsFixed(4);
      }
    }
    //单个代币金额总价缓存
    await widget.dataSourceProvider.localService.putCachedPrice(
        widget.tokenBlog, widget.account.address, this.totalListenable.value);

    await widget.dataSourceProvider.localService.tokenPriceListInsert(
        widget.account.address,
        widget.i,
        double.parse(this.totalListenable.value != "~"
            ? this.totalListenable.value
            : "0"));

    // getTotalPrice();

    _getCachTokenPrice();
  }

//自定义网络显示金额
  _getCustTokenTotalAamount() async {
    ConfigStorageUtils().writeTotalAmountIndexOfKey(ConfigKey.TotalAmount, 0);
    await getUPrice();

    //计算usdt兑换成美元的价格表
    if (CoreProvider.of(context).networkConfig.mainSymbol != "USDT") {
      var header = {'Connection': 'keep-alive'};
      var url =
          "https://data.gateapi.io/api2/1/ticker/${CoreProvider.of(context).networkConfig.mainSymbol}_usdt";

      var usdrespose = await http.get(url, headers: header);
      var usdresponseBody = json.decode(usdrespose.body);
      if (usdresponseBody['last'] != null) {
        this.totalListenable.value = (double.parse(usdresponseBody['last']) *
                double.parse(balanceListenable.value
                    .toStringAsFixed()
                    .replaceAll(",", '')) *
                this.usdtPrice)
            .toStringAsFixed(4);
      } else {
        this.totalListenable.value = "~";
      }
    } else {
      this.totalListenable.value = (double.parse(balanceListenable.value
                  .toStringAsFixed()
                  .replaceAll(",", '')) *
              this.usdtPrice)
          .toStringAsFixed(4);
    }

    await ConfigStorageUtils().writeTotalAmountIndexOfKey(
        ConfigKey.TotalAmount,
        double.parse(this.totalListenable.value != "~"
                ? this.totalListenable.value
                : "0") +
            ConfigStorageUtils()
                .readTotalAmountIndexOfKey(ConfigKey.TotalAmount));

    await widget.dataSourceProvider.localService.putCachedTotalPrice(
      widget.account.address,
      ConfigStorageUtils()
          .readTotalAmountIndexOfKey(ConfigKey.TotalAmount)
          .toStringAsFixed(2),
    );
    Future.delayed(Duration(milliseconds: 1500), () {
      widget.dataSourceProvider.localService
          .getCachedTotalPrice(widget.account.address)
          .then((value) {
        eventBus.fire(new AssetEvent(double.parse(value.balance)));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getLastBlockHight();
    _getCachTokenPrice();
    // widget.dataSourceProvider.localService.tokenPricetListClean();
  }

  _getCachTokenPrice() async {
    List<double> data = await widget.dataSourceProvider.localService
        .tokenPricetList(widget.account.address)
        .then((value) => value);

    await widget.dataSourceProvider.localService.putCachedTotalPrice(
        widget.account.address,
        ((data.getRange(0, widget.index).sum).toStringAsFixed(2)));

    if (widget.i == 0 || widget.i + 1 == widget.index) {
      Future.delayed(Duration(milliseconds: 0), () {
        eventBus.fire(new AssetEvent(data.getRange(0, widget.index).sum));
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    // totalListenable.dispose();
  }

//获取代币金额（数量*行情价格）
  void fetchPrice() {
    widget.tokenBlog != null
        ? widget.dataSourceProvider.localService
            .getCachedPrice(widget.tokenBlog, widget.account.address)
            .then((cachePrice) {
            if (cachePrice != null) {
              // _getCachTokenPrice();
              this.totalListenable.value = cachePrice.balance;
            }
            if (cachePrice == null ||
                DateTime.now().difference(cachePrice.updateTime) >
                    Duration(seconds: 10)) {
              _getTokenPrice();
            }
          })
        : _getCustTokenTotalAamount();
  }

  void fetchBalance() {
    widget.tokenBlog != null
        ? widget.dataSourceProvider.localService
            .getCachedBalance(widget.tokenBlog, widget.account.address)
            .then((cacheBalance) async {
            if (cacheBalance != null) {
              this.balanceListenable.value = Amount(
                value: cacheBalance.balance,
                decimals: widget.tokenBlog.decimals,
              );
            }

            if (cacheBalance == null ||
                DateTime.now().difference(cacheBalance.updateTime) >
                    Duration(seconds: 10)) {
              await Utils.futureGetBalance(
                Web3ClientProvider.of(context),
                widget.tokenBlog,
                widget.account.address,
              ).then((balance) {
                this.balanceListenable.value = balance;
                widget.dataSourceProvider.localService.putCachedBalance(
                  widget.tokenBlog,
                  widget.account.address,
                  balance.value,
                );
              });
              _getTokenPrice();
              fetchPrice();
            }
          })
        : Web3ClientProvider.of(context)
            .getBalance(widget.account.address)
            .then(
              (value) => this.balanceListenable.value = Amount(
                  value: value,
                  decimals: CoreProvider().networkConfig.mainDecimals),
            );
  }

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () {
          /// push的context和当前代码块的context不同，当前代码块使用的是IndexTabView的Context，
          /// 所有的Provider都创建在了IndexTabView的Context中，push后的context是和IndexTabView
          /// 兄弟关系，所以没有部分context，考虑到后续的页面也不会随着地址和网络切换更新，所以改用
          /// 直接传递Provider的形式实现。(CoreProvider是创建在顶层，全局可用)
          widget.isBlack == true
              ? showDialog(
                  context: context,
                  builder: (context) {
                    return FreezePage();
                  },
                )
              : Navigator.of(context, rootNavigator: true).push(
                  ViewAnimations.viewRightIn(
                    widget.tokenBlog != null
                        ? widget.tokenBlog.isContract
                            ? TokenDetailView(
                                tokenBlog: widget.tokenBlog,
                                account: widget.account,
                                web3Client: Web3ClientProvider.of(context),
                                endPoint: CoreProvider.of(context).endPoint,
                                dataSourceProvider:
                                    Ethereum.DataSourceProvider.of(context),
                                networkConfig: widget.networkConfig,
                                lastBlockHeight: this.height,
                                color: widget.color,
                              )
                            : MainCoinDetailView(
                                tokenBlog: widget.tokenBlog,
                                account: widget.account,
                                web3Client: Web3ClientProvider.of(context),
                                endPoint: CoreProvider.of(context).endPoint,
                                dataSourceProvider:
                                    Ethereum.DataSourceProvider.of(context),
                                networkConfig: widget.networkConfig,
                                color: widget.color,
                              )
                        : CustomCoinDetailView(
                            tokenBlog: widget.tokenBlog,
                            account: widget.account,
                            web3Client: Web3ClientProvider.of(context),
                            endPoint: CoreProvider.of(context).endPoint,
                            dataSourceProvider:
                                Ethereum.DataSourceProvider.of(context),
                            networkConfig: widget.networkConfig,
                            color: widget.color,
                          ),
                  ),
                );
        },
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(),
              padding: EdgeInsets.only(
                // top: ScreenUtil().setHeight(26),
                left: ScreenUtil().setWidth(22),
                right: ScreenUtil().setWidth(22),
              ),
              height: ScreenUtil().setHeight(68),
              child: Row(
                children: [
                  Container(
                    alignment: Alignment.center,
                    width: ScreenUtil().setWidth(40),
                    height: ScreenUtil().setHeight(40),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.grey[200],
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: ClipPath.shape(
                      shape: CircleBorder(),
                      child: CachedImage.assetNetwork(
                        url: widget.tokenBlog != null
                            ? widget.tokenBlog.logo ??
                                CoreProvider.of(context)
                                        .endPoint
                                        .resourcesServicesURL +
                                    '/' +
                                    widget.tokenBlog.address +
                                    "/logo.png"
                            : CoreProvider().networkConfig.logoURL,
                        placeholder: Image.asset(
                          'assets/imgs/default_token.png',
                          package: 'wallet_flutter',
                          fit: BoxFit.fill,
                        ),
                        width: ScreenUtil().setWidth(30),
                        height: ScreenUtil().setWidth(30),
                        imageCacheHeight: 100,
                        imageCacheWidth: 100,
                      ),
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(15)),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.tokenBlog != null
                            ? widget.tokenBlog.symbol
                            : CoreProvider().networkConfig.mainSymbol,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(19),
                          color: Color.fromRGBO(58, 59, 61, 1),
                        ),
                      ),
                    ],
                  ),
                  Expanded(child: SizedBox()),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      ValueListenableBuilder(
                        valueListenable: widget.refreshListenable,
                        builder: (context, _, child) {
                          fetchBalance();

                          return Column(
                            children: [
                              ValueListenableBuilder(
                                valueListenable: balanceListenable,
                                builder: (context, balance, child) => balance ==
                                        null
                                    ? Container()
                                    : Text(
                                        balanceListenable.value.decimals != 0
                                            // ? balance.toStringAsFixed()
                                            ? balanceListenable.value
                                                .toStringAsFixed(
                                                fractionDigits:
                                                    balanceListenable.value
                                                                .decimals >
                                                            8
                                                        ? 4
                                                        : balanceListenable
                                                            .value.decimals,
                                              )
                                            : "0",
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(19),
                                          color: Color.fromRGBO(58, 59, 61, 1),
                                        ),
                                      ),
                              ),
                            ],
                          );
                        },
                      ),
                      ValueListenableBuilder(
                          valueListenable: widget.refreshListenable,
                          builder: (context, _, _child) {
                            fetchPrice();
                            return ValueListenableBuilder(
                              valueListenable: totalListenable,
                              builder: (context, value, child) {
                                return Column(
                                  children: [
                                    Container(
                                      child: Text(
                                        '\$ ' + totalListenable.value,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                          color:
                                              Color.fromRGBO(175, 175, 175, 1),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            );
                          })
                    ],
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(24),
                right: ScreenUtil().setWidth(20),
              ),
              height: 1,
              color: Color.fromRGBO(241, 241, 241, 1),
            )
          ],
        ),
      );
}
