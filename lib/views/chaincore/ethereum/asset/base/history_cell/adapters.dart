// @dart=2.9
part of '../../../asset.dart';

enum FilterType {
  All,
  In,
  Out,
  Other,
}

// ignore: non_constant_identifier_names
final Map<FilterType, String> FilterTypeNames = {
  FilterType.All: S.current.ethereum_home_detail_txfilter_all,
  FilterType.In: S.current.ethereum_home_detail_txfilter_in,
  FilterType.Out: S.current.ethereum_home_detail_txfilter_out,
  FilterType.Other: S.current.ethereum_home_detail_txfilter_call,
};

abstract class _CellAdapterBuilder {
  /// 是否可以使用该适配器生成Widget
  bool matching(Object data);

  /// 筛选条件
  bool matchingFilter(Object data, FilterType type, String ownerAddress);

  Widget build(
    BuildContext context,
    Object txOrLog,
    String ownerAccount,
    Ethereum.TokenBlog tokenBlog,
    int index,
  );
}

class _HistoryCellBuilderAdapter {
  final List<_CellAdapterBuilder> adapters;

  _HistoryCellBuilderAdapter({@required this.adapters});

  List<T> filterByType<T>(
    List<T> source,
    FilterType filterType,
    String ownerAddress,
  ) {
    final ret = <T>[];

    source.forEach(
      (e) {
        /// 是否有匹配到适配器
        final adapterIndex = adapters.indexWhere(
          (adapter) => adapter.matching(e),
        );

        /// 调用适配器的筛选逻辑
        if (adapterIndex >= 0 &&
            adapters[adapterIndex].matchingFilter(
              e,
              filterType,
              ownerAddress,
            )) ret.add(e);
      },
    );

    return ret.cast<T>();
  }

  Widget build({
    @required BuildContext context,
    @required String currentAddress,
    @required Object txOrLog,
    @required Ethereum.TokenBlog tokenBlog,
    @required int index,
  }) {
    final adapterIndex = adapters.indexWhere(
      (adapter) => adapter.matching(txOrLog),
    );

    if (adapterIndex >= 0) {
      return adapters[adapterIndex].build(
        context,
        txOrLog,
        currentAddress,
        tokenBlog,
        index,
      );
    } else {
      return Container(color: Colors.red);
    }
  }
}
