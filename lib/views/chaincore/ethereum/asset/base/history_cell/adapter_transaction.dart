// @dart=2.9
part of '../../../asset.dart';

class _BaseTransactionAdapterBuilder extends _CellAdapterBuilder {
  @override
  bool matching(Object data) => data is Ethereum.Transaction;

  /// 筛选条件
  @override
  bool matchingFilter(Object data, FilterType type, String ownerAddress) {
    if (data is! Ethereum.Transaction) {
      return false;
    }

    final Ethereum.Transaction tx = data as Ethereum.Transaction;

    /// 转出
    final isTransferOut = tx.from.toLowerCase() == ownerAddress.toLowerCase() &&
        tx.to.toLowerCase() != ownerAddress.toLowerCase() &&
        tx.input == '0x';

    /// 转入
    final isTransferIn = tx.to.toLowerCase() == ownerAddress.toLowerCase() &&
        tx.from.toLowerCase() != ownerAddress.toLowerCase() &&
        tx.value != '0';

    switch (type) {
      case FilterType.All:
        return true;

      case FilterType.In:
        return isTransferIn;

      case FilterType.Out:
        return isTransferOut;

      case FilterType.Other:
        return !isTransferOut && !isTransferIn;

      default:
        return false;
    }
  }

  @override
  Widget build(
    BuildContext context,
    Object txOrLog,
    String ownerAccount,
    Ethereum.TokenBlog tokenBlog,
    int index,
  ) {
    final Ethereum.Transaction tx = txOrLog as Ethereum.Transaction;

    /// 转出
    final isTransferOut =
        tx.from.toLowerCase() == ownerAccount.toLowerCase() && tx.input == '0x';

    /// 转入
    final isTransferIn =
        tx.to.toLowerCase() == ownerAccount.toLowerCase() && tx.input == '0x';

    /// 自己转给自己
    final isTransferSelf = isTransferOut && isTransferIn;

    return _HistoryBaseCell(
      prefixIcon: tx.isCallContract
          ? tx.isCreation
              ? Icons.ballot
              // : Icons.swap_horizontal_circle_outlined
              : CustomIcons.contract
          : isTransferSelf
              ? Icons.receipt
              : isTransferOut
                  // ? Icons.reply_rounded
                  ? CustomIcons.out
                  // : Icons.send_rounded,
                  : CustomIcons.into,

      prefixIconColor: tx.isCallContract
          ? tx.isCreation
              ? ThemeUtils().getColor(
                  'views.chaincore.ethereum.assets_detail.cell_override',
                )
              : ThemeUtils().getColor(
                  'views.chaincore.ethereum.assets_detail.cell_transfer',
                )
          : isTransferSelf
              ? ThemeUtils().getColor(
                  'views.chaincore.ethereum.assets_detail.cell_override',
                )
              : isTransferOut
                  // ? ThemeUtils().getColor(
                  //     'views.chaincore.ethereum.assets_detail.cell_transfer',
                  //   )
                  ? Color.fromRGBO(217, 83, 79, 1)
                  : ThemeUtils().getColor(
                      'views.chaincore.ethereum.assets_detail.cell_collection',
                    ),

      tailIcon: Icons.arrow_forward_ios,

      suffix: tx.isCallContract
          ? tx.isCreation

              /// 部署合约
              ? Text(
                  S.current.ethereum_home_detail_tx_state_creation,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
                  ),
                )

              /// 调用合约
              : Text(
                  S.current.ethereum_home_detail_tx_state_call,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
                  ),
                )

          /// 普通转账
          : isTransferSelf
              ? Text(
                  S.current.ethereum_home_detail_tx_state_override,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
                  ),
                )
              : Text(
                  (isTransferOut ? '- ' : '+ ') +
                      Amount.fromString(
                        tx.value,
                        decimals: tokenBlog.decimals,
                        valueDecimals: 0,
                      ).toStringAsFixed(fractionDigits: 8) +
                      ' ${tokenBlog.symbol.toUpperCase()}',
                  style: isTransferOut
                      // ? ThemeUtils().getTextStyle(
                      //     'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
                      //   )
                      ? TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          color: Color.fromRGBO(217, 83, 79, 1))
                      : ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_in',
                        ),
                ),
      suffixDetail: tx.isCallContract
          ? tx.isCreation

              /// 部署合约
              ? Text(
                  S.current.ethereum_home_detail_tx_state_creation,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.trade_detail.textstyle.number_title',
                  ),
                )

              /// 调用合约
              : Text(
                  S.current.ethereum_home_detail_tx_state_call,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.trade_detail.textstyle.number_title',
                  ),
                )

          /// 普通转账
          : isTransferSelf
              ? Text(
                  S.current.ethereum_home_detail_tx_state_override,
                  style: ThemeUtils().getTextStyle(
                    'views.chaincore.ethereum.trade_detail.textstyle.number_title',
                  ),
                )
              : Text(
                  (isTransferOut ? '- ' : '+ ') +
                      Amount.fromString(
                        tx.value,
                        decimals: tokenBlog.decimals,
                        valueDecimals: 0,
                      ).toStringAsFixed(fractionDigits: 8) +
                      ' ${tokenBlog.symbol.toUpperCase()}',
                  style: isTransferOut
                      ? ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.trade_detail.textstyle.number_title',
                        )
                      : ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.trade_detail.textstyle.number_title',
                        ),
                ),

      shaCode: tx.hash,
      timeStampSec: int.parse(tx.timeStamp),

      txSuccess: tx.txreceiptStatus == '1',
      background: [Color(0xFFF8F8F8), Colors.white][index % 2],

      tokenSymbol: tokenBlog.symbol,
      ownerAccount: ownerAccount,
      from: tx.from,
      to: tx.to,
      gas: Amount(
              value: BigInt.parse(tx.gasUsed) * BigInt.parse(tx.gasPrice),
              decimals: 18)
          .toStringAsFixed(fractionDigits: 8),
      blockchin: tx.blockNumber,

      // ownerAddress: ownerAddress,
      onTap: () {},
    );
  }
}

// class _HistoryBaseCell extends StatelessWidget {
//   final Ethereum.Transaction tx;
//   final int index;
//   final int balanceDecimals;
//   final String fromAddress;

//   _HistoryBaseCell({
//     Key key,
//     @required this.tx,
//     @required this.index,
//     @required this.balanceDecimals,
//     @required this.fromAddress,
//   }) : super(key: key);

//   bool get isTransferOut => tx.from.toLowerCase() == fromAddress.toLowerCase();

//   bool get isCallContract => tx.input.length > 2;

//   bool get isCreation => tx.input.length > 2 && tx.to.length <= 2;

//   @override
//   Widget build(BuildContext context) => Container(
//         color: [Color(0xFFF8F8F8), Colors.white][index % 2],
//         padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
//         height: ScreenUtil().setHeight(80),
//         child: Row(
//           children: [
//             Icon(
//               /// 是否合约交互
//               isCallContract
//                   ? isCreation
//                       ? Icons.ballot
//                       : Icons.swap_horizontal_circle_outlined
//                   : isTransferOut
//                       ? Icons.reply_rounded
//                       : Icons.send_rounded,
//               size: 18,
//               color: isTransferOut
//                   ? ThemeUtils().getColor(
//                       'views.chaincore.ethereum.assets_detail.cell_transfer',
//                     )
//                   : ThemeUtils().getColor(
//                       'views.chaincore.ethereum.assets_detail.cell_collection',
//                     ),
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Text(
//                   tx.hashBlog,
//                   style: ThemeUtils().getTextStyle(
//                     'views.chaincore.ethereum.assets_detail.textstyle.cell_hash',
//                   ),
//                 ),
//                 Expanded(child: SizedBox()),
//                 Text(
//                   DateFormat('yyyy-MM-dd HH:mm').format(
//                     DateTime.fromMillisecondsSinceEpoch(
//                       int.parse(tx.timeStamp) * 1000,
//                     ),
//                   ),
//                   style: ThemeUtils().getTextStyle(
//                     'views.chaincore.ethereum.assets_detail.textstyle.cell_date',
//                   ),
//                 ),
//               ],
//             ),
//             Expanded(child: SizedBox()),
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.end,
//               children: [
//                 /// 无数据默认值为 ‘0x'
//                 isCallContract
//                     ? isCreation

//                         /// 部署合约
//                         ? Text(
//                             S.current.ethereum_home_detail_tx_state_creation,
//                             style: ThemeUtils().getTextStyle(
//                               'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
//                             ),
//                           )

//                         /// 调用合约
//                         : Text(
//                             S.current.ethereum_home_detail_tx_state_call,
//                             style: ThemeUtils().getTextStyle(
//                               'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
//                             ),
//                           )

//                     /// 普通转账
//                     : Text(
//                         (isTransferOut ? '- ' : '+ ') +
//                             Amount.fromString(
//                               tx.value,
//                               decimals: 18,
//                               valueDecimals: 0,
//                             ).toStringAsFixed(fractionDigits: 8),
//                         style: isTransferOut
//                             ? ThemeUtils().getTextStyle(
//                                 'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
//                               )
//                             : ThemeUtils().getTextStyle(
//                                 'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_in',
//                               ),
//                       ),
//                 Expanded(child: SizedBox()),
//                 Text(
//                   tx.txreceiptStatus == '1'
//                       ? S.current.ethereum_home_detail_tx_state_succses
//                       : S.current.ethereum_home_detail_tx_state_failed,
//                   style: ThemeUtils().getTextStyle(
//                     'views.chaincore.ethereum.assets_detail.textstyle.cell_state',
//                   ),
//                 ),
//               ],
//             )
//           ],
//         ),
//       );
// }

class _HistoryBaseCell extends StatelessWidget {
  final IconData prefixIcon;
  final IconData tailIcon;
  final Color prefixIconColor;
  final Widget suffix;
  final Widget suffixDetail;
  final String shaCode;
  final int timeStampSec;
  final bool txSuccess;
  final Color background;
  final VoidCallback onTap;
  final Ethereum.Transaction tx;
  final String ownerAccount;
  final String to;
  final String from;
  final String tokenSymbol;
  final Ethereum.Log data;
  final String gas;
  final String blockchin;

  _HistoryBaseCell({
    @required this.prefixIcon,
    @required this.tailIcon,
    @required this.prefixIconColor,
    @required this.suffix,
    @required this.shaCode,
    @required this.timeStampSec,
    @required this.txSuccess,
    @required this.background,
    @required this.onTap,
    this.from,
    this.to,
    this.tx,
    this.ownerAccount,
    this.tokenSymbol,
    this.data,
    this.gas,
    this.blockchin,
    this.suffixDetail,
  });

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () {
          Navigator.of(context).push(ViewAnimations.viewRightIn(
            TradeDetailPage(
              tokenSymbol: this.tokenSymbol,
              ownerAccount: this.ownerAccount,
              to: this.to,
              from: this.from,
              txSuccess: this.txSuccess,
              suffix: this.suffix,
              timeStampSec: this.timeStampSec,
              shaCode: this.shaCode,
              gas: this.gas,
              blockchin: blockchin,
              suffixDetail: suffixDetail,
            ),
          ));
        },
        child: this.ownerAccount.toLowerCase() == this.from.toLowerCase() ||
                this.ownerAccount.toLowerCase() == this.to.toLowerCase()
            ? Container(
                color: background,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                height: ScreenUtil().setHeight(80),
                child: Row(
                  children: [
                    Icon(
                      prefixIcon,
                      size: 18,
                      color: prefixIconColor,
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(18),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          this.to.length > 0
                              ? this
                                  .to
                                  .replaceRange(9, this.to.length - 9, '...')
                              : this.ownerAccount.replaceRange(
                                  9, this.ownerAccount.length - 9, '...'),
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.assets_detail.textstyle.cell_hash',
                          ),
                        ),

                        // Text(
                        //   this.shaCode.replaceRange(9, this.shaCode.length - 9, '...'),
                        //   style: ThemeUtils().getTextStyle(
                        //     'views.chaincore.ethereum.assets_detail.textstyle.cell_hash',
                        //   ),
                        // ),
                        Expanded(child: SizedBox()),
                        Text(
                          DateFormat('yyyy-MM-dd HH:mm').format(
                            DateTime.fromMillisecondsSinceEpoch(
                              timeStampSec * 1000,
                            ),
                          ),
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.assets_detail.textstyle.cell_date',
                          ),
                        ),
                      ],
                    ),
                    Expanded(child: SizedBox()),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        suffix,
                        Expanded(child: SizedBox()),
                        txSuccess
                            ? Text(
                                S.current.ethereum_home_detail_tx_state_succses,
                                style: ThemeUtils().getTextStyle(
                                  'views.chaincore.ethereum.assets_detail.textstyle.cell_state_success',
                                ),
                              )
                            : Text(
                                S.current.ethereum_home_detail_tx_state_failed,
                                style: ThemeUtils().getTextStyle(
                                  'views.chaincore.ethereum.assets_detail.textstyle.cell_state_error',
                                ),
                              ),
                        // Text(this.tx.to),
                      ],
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(18),
                    ),
                    Icon(
                      tailIcon,
                      size: 18,
                      color: Colors.grey,
                    ),
                  ],
                ),
              )
            : Container(
                height: 0,
                width: 0,
              ),
      );
}
