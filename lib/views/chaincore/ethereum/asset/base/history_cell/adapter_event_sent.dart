// @dart=2.9
part of '../../../asset.dart';

/// ERC777
/// event Sent(
///   address indexed operator,
///   address indexed from,
///   address indexed to,
///   uint256 amount,
///   bytes data,
///   bytes operatorData
/// );
class _EventSentAdapterBuilder extends _CellAdapterBuilder {
  static ContractEvent event = ContractEvent(false, 'Sent', [
    EventComponent(FunctionParameter<String>('operator', AddressType()), true),
    EventComponent(FunctionParameter<String>('from', AddressType()), true),
    EventComponent(FunctionParameter<String>('to', AddressType()), true),
    EventComponent(FunctionParameter<BigInt>('amount', UintType()), false),
    EventComponent(FunctionParameter<Uint8List>('data', DynamicBytes()), false),
    EventComponent(FunctionParameter<Uint8List>('oper', DynamicBytes()), false),
  ]);

  @override
  bool matching(Object data) =>
      data is Ethereum.Log &&
      bytesToHex(event.signature, include0x: true).toLowerCase() ==
          data.topics.first.toLowerCase();

  /// 筛选条件
  @override
  bool matchingFilter(Object data, FilterType type, String ownerAddress) {
    if (data is! Ethereum.Log) {
      return false;
    }

    final Ethereum.Log log = data as Ethereum.Log;

    switch (type) {
      case FilterType.All:
        return true;

      case FilterType.Out:
        return log.topics[1].toLowerCase() ==
                paddingToEventTopic(ownerAddress).toLowerCase() ||
            log.topics[2].toLowerCase() ==
                paddingToEventTopic(ownerAddress).toLowerCase();

      case FilterType.In:
        return log.topics[3].toLowerCase() ==
            paddingToEventTopic(ownerAddress).toLowerCase();

      case FilterType.Other:
        return false;

      default:
        return false;
    }
  }

  @override
  Widget build(
    BuildContext context,
    Object txOrLog,
    String ownerAccount,
    Ethereum.TokenBlog tokenBlog,
    int index,
  ) {
    final Ethereum.Log log = txOrLog as Ethereum.Log;

    final decodeResult = event.decodeResults(log.topics, log.data);
    final opertor = '0x' + (decodeResult[0] as String);
    final from = '0x' + (decodeResult[1] as String);
    final to = '0x' + (decodeResult[2] as String);
    final amount = decodeResult[3] as BigInt;

    final isTransferOut = from.toLowerCase() == ownerAccount.toLowerCase() ||
        opertor == ownerAccount.toLowerCase();

    return _HistoryBaseCell(
      prefixIcon: isTransferOut
          // ? Icons.reply_rounded
          ? CustomIcons.out
          // : Icons.send_rounded,
          : CustomIcons.into,
      // isTransferOut ? Icons.reply_rounded : Icons.send_rounded,
      prefixIconColor: isTransferOut
          // ? ThemeUtils().getColor(
          //     'views.chaincore.ethereum.assets_detail.cell_transfer',
          //   )
          ? Color.fromRGBO(217, 83, 79, 1)
          : ThemeUtils().getColor(
              'views.chaincore.ethereum.assets_detail.cell_collection',
            ),
      // isTransferOut
      //     ? ThemeUtils().getColor(
      //         'views.chaincore.ethereum.assets_detail.cell_transfer',
      //       )
      //     : ThemeUtils().getColor(
      //         'views.chaincore.ethereum.assets_detail.cell_collection',
      //       ),
      tailIcon: Icons.arrow_forward_ios,
      suffix: Text(
        (isTransferOut ? '- ' : '+ ') +
            Amount(
              value: amount,
              decimals: tokenBlog.decimals,
            ).toStringAsFixed(
              fractionDigits: tokenBlog.decimals > 8 ? 8 : tokenBlog.decimals,
            ) +
            ' ${tokenBlog.symbol.toUpperCase()}',
        style: isTransferOut
            // ? ThemeUtils().getTextStyle(
            //     'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
            //   )
            ? TextStyle(
                fontSize: ScreenUtil().setSp(14),
                color: Color.fromRGBO(217, 83, 79, 1))
            : ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_in',
              ),
        // isTransferOut
        //     ? ThemeUtils().getTextStyle(
        //         'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
        //       )
        //     : ThemeUtils().getTextStyle(
        //         'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_in',
        //       ),
      ),
      suffixDetail: Text(
        (isTransferOut ? '- ' : '+ ') +
            Amount(
              value: amount,
              decimals: tokenBlog.decimals,
            ).toStringAsFixed(
              fractionDigits: tokenBlog.decimals > 8 ? 8 : tokenBlog.decimals,
            ) +
            ' ${tokenBlog.symbol.toUpperCase()}',
        style: isTransferOut
            ? ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_out',
              )
            : ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.assets_detail.textstyle.cell_balance_in',
              ),
      ),
      shaCode: log.transactionHash,
      timeStampSec: int.parse(log.timeStamp),
      txSuccess: true,
      background: [Color(0xFFF8F8F8), Colors.white][index % 2],
      ownerAccount: ownerAccount,
      gas: Amount(
              value: BigInt.parse(log.gasUsed) * BigInt.parse(log.gasPrice),
              decimals: 18)
          .toStringAsFixed(fractionDigits: 8),
      to: to,
      from: from,
      blockchin: log.blockNumber,
      onTap: () {},
    );
  }
}
