// @dart=2.9
part of '../../asset.dart';

class NftTokenDetail extends StatefulWidget {
  final String description;
  final String imgUrl;
  final String tokenId;
  final String name;
  final TokenBlog nftTokenblog;
  final WalletKey account;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final Web3ClientProvider web3clientProvider;
  final String amount;

  const NftTokenDetail(
      {this.description,
      this.imgUrl,
      this.tokenId,
      this.dataSourceProvider,
      this.networkConfig,
      this.web3clientProvider,
      this.nftTokenblog,
      this.account,
      this.name,
      this.amount});
  @override
  State<NftTokenDetail> createState() => _NftTokenDetailState();
}

class _NftTokenDetailState extends State<NftTokenDetail> {
  Map usdresponsDescription;
  Ethereum.ScanAPIService scanAPIService;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    scanAPIService = Ethereum.ScanAPIService(
      httpClient: http.Client(),
      hostURL: CoreProvider.of(context).endPoint.scanURL,
      apiKey: CoreProvider.of(context).endPoint.scanAPIKey,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_nft_asset_management_nft_detail,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height -
                  ScreenUtil().setHeight(200),
              child: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: Container(
                      color: Colors.white,
                      width: MediaQuery.of(context).size.width,
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Container(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: AspectRatio(
                                aspectRatio: 1.2,
                                child: ClipRRect(
                                  child: CachedImage.assetNetwork(
                                    url: widget.imgUrl,
                                    fit: BoxFit.fill,
                                    placeholder: Image.asset(
                                      'assets/imgs/nft_default3.png',
                                      package: 'wallet_flutter',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(15),
                                left: ScreenUtil().setWidth(15),
                                bottom: ScreenUtil().setHeight(15),
                              ),
                              child: Text(
                                widget.name ?? "",
                                maxLines: 1,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: ScreenUtil().setWidth(15),
                              ),
                              child: Text(
                                "#" + widget.tokenId,
                                maxLines: 1,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(14),
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ),
                    ),
                  ),
                  //NFT资产介绍
                  widget.description != null
                      ? SliverToBoxAdapter(
                          child: Container(
                            color: Colors.white,
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(10),
                              bottom: ScreenUtil().setHeight(10),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(15),
                                    left: ScreenUtil().setWidth(15),
                                  ),
                                  child: Text('介绍'),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(15),
                                    left: ScreenUtil().setWidth(15),
                                    bottom: ScreenUtil().setHeight(10),
                                  ),
                                  child: Text(
                                    widget.description,
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(14),
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : SliverToBoxAdapter(),
                  //交易记录
                  SliverToBoxAdapter(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(15),
                                left: ScreenUtil().setWidth(15),
                              ),
                              child: Text(S.current
                                  .ethereum_nft_asset_management_transaction_record)),
                        ],
                      ),
                    ),
                  ),
                  // //交易记录
                  // SliverToBoxAdapter(
                  //   child:
                  // ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: InkWell(
                onTap: () {
                  Navigator.of(context, rootNavigator: true).push(
                    ViewAnimations.viewRightIn(
                      NftCollectionTransfer(
                        name: widget.name,
                        imgUrl: widget.imgUrl,
                        tokenId: widget.tokenId,
                        dataSourceProvider: widget.dataSourceProvider,
                        networkConfig: widget.networkConfig,
                        web3clientProvider: widget.web3clientProvider,
                        scanAPIService: this.scanAPIService,
                        nftTokenblog: widget.nftTokenblog,
                        account: widget.account,
                        amount: widget.amount,
                      ),
                    ),
                  );
                },
                child: Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  height: ScreenUtil().setHeight(88),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(15),
                      left: ScreenUtil().setWidth(15),
                      bottom: ScreenUtil().setHeight(15),
                      right: ScreenUtil().setWidth(15),
                    ),
                    child: Center(
                      child: Text(
                        S.current.ethereum_home_transfer_do_sent,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
