// @dart=2.9
part of '../../asset.dart';

class NftDetail extends StatefulWidget {
  final Ethereum.TokenBlog nftDetail;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final Web3ClientProvider web3clientProvider;
  final WalletKey account;
  final int balanceIndex;
  const NftDetail(
      {this.nftDetail,
      this.dataSourceProvider,
      this.networkConfig,
      this.web3clientProvider,
      this.account,
      this.balanceIndex});
  @override
  State<NftDetail> createState() => _NftDetailState();
}

class _NftDetailState extends State<NftDetail> {
  final ValueNotifier<List> nFtTokenInfo = ValueNotifier<List>([]);
  final ValueNotifier<bool> showOption = ValueNotifier<bool>(true);
  //刷新新监听器
  final ValueNotifier<DateTime> refreshListenable =
      ValueNotifier<DateTime>(DateTime.now());
  final ValueNotifier<bool> nodataImageListenable = ValueNotifier<bool>(false);

  String chainName;
  String ercType;

  int count = 0;
  geterc1155balance() async {
    await Utils.futureErc1155Balance(
      widget.web3clientProvider,
      widget.nftDetail,
      widget.account.address,
      BigInt.zero,
    ).then((balance) {});
  }

  getChainNftDetail() async {
    if (count > 0) {
      getNFTTokenList();
    }
    if (widget.balanceIndex <= 0 && count <= 0) {
      nodataImageListenable.value = true;
    }
    //  print("链上获取");
    try {
      for (var i = 0; i < widget.balanceIndex; i++) {
        final tokenID = await Utils.futureGetNftTokenId(
            widget.web3clientProvider,
            widget.nftDetail.address,
            widget.account.address,
            BigInt.from(i));

        final tokenURL = await Utils.futureGetNftTokenUrl(
          widget.web3clientProvider,
          widget.nftDetail.address,
          widget.account.address,
          tokenID,
        );

        var httpClient = new HttpClient();
        // ignore: close_sinks
        var request = await httpClient
            .getUrl(Uri.parse(tokenURL))
            .timeout(Duration(seconds: 4));

        var response = await request.close();

        var json = await response.transform(utf8.decoder).join();
        var data = jsonDecode(json);

        nFtTokenInfo.value.add({
          "name": data['name'] ?? "",
          "tokenId": tokenID.toString(),
          "description": "",
          "imgUrl": data['image'] ?? ""
        });
        nodataImageListenable.value = true;
        setState(() {});
      }
    } catch (e) {
      if (nFtTokenInfo.value.length <= 0) {
        getNFTTokenList();
      }
    }
  }

  getNFTTokenList() async {
    // print("第三方API");
    this.chainName = widget.networkConfig.chainName.toLowerCase();

    if (widget.networkConfig.chainName == "Ethereum") {
      this.chainName = "rest";
    } else if (widget.networkConfig.chainName == "BSC") {
      this.chainName = "bnb";
    }
    var header = {
      'Connection': 'keep-alive',
      'X-API-KEY': 'HYfXOQHehc3cBckQ42pgfNaa'
    };
    var url =
        "https://${this.chainName}api.nftscan.com/api/v2/account/own/${widget.account.address}?erc_type=${ercType}&contract_address=${widget.nftDetail.address}&limit=1000&show_attribute=true";

    var usdrespose = await http.get(url, headers: header);

    var response = json.decode(usdrespose.body);
    print(url);
    try {
      count = response['data']['total'];
      if (widget.nftDetail.protocol.toLowerCase() == "erc1155") {
        widget.dataSourceProvider.localService.putNftCachedBalance(
          widget.nftDetail,
          widget.account.address,
          BigInt.from(count),
        );
      }
    } catch (e) {
      count = 0;
    }
    for (var i = 0; i < count; i++) {
      nFtTokenInfo.value.add({
        "name": response['data']['content'][i]['name'] ?? "",
        "tokenId": response['data']['content'][i]['token_id'],
        "description": null,
        // json.decode(response['data']['content'][i]
        //         ['metadata_json'])['description'] ??
        //     "",
        "imgUrl": response['data']['content'][i]['nftscan_uri'] ??
            response['data']['content'][i]['image_uri'] ??
            "",
        "amount": response['data']['content'][i]['amount']
      });
      nodataImageListenable.value = true;
      setState(() {});
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ercType = widget.nftDetail.protocol.toLowerCase();

    if (widget.nftDetail.protocol.toUpperCase() == "ERC721") {
      getChainNftDetail();
    } else {
      getNFTTokenList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_nft_asset_management_nft_detail,
        elevation: 0.5,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        physics: const AlwaysScrollableScrollPhysics(
          parent: BouncingScrollPhysics(),
        ),
        slivers: [
          CupertinoSliverRefreshControl(
            refreshTriggerPullDistance: 100,
            onRefresh: () async {
              return Future.delayed(Duration(milliseconds: 2000), () async {
                nFtTokenInfo.value.clear();
                nodataImageListenable.value = false;
                await getChainNftDetail();
                setState(() {});
                refreshListenable.value = DateTime.now();
              });
            },
          ),
          SliverToBoxAdapter(
            child: Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(26),
                            top: ScreenUtil().setHeight(25)),
                        child: ClipPath.shape(
                          shape: CircleBorder(),
                          child: CachedImage.assetNetwork(
                            url: widget.nftDetail.logo,
                            placeholder: Image.asset(
                                'assets/imgs/nft_default.png',
                                package: 'wallet_flutter',
                                fit: BoxFit.fill),
                            width: ScreenUtil().setWidth(40),
                            height: ScreenUtil().setWidth(40),
                            imageCacheHeight: 100,
                            imageCacheWidth: 100,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(10),
                              top: ScreenUtil().setHeight(25),
                            ),
                            width: ScreenUtil().setWidth(200),
                            child: Text(
                              widget.nftDetail.symbol,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(16),
                                color: Color.fromRGBO(58, 59, 61, 1),
                              ),
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(8)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(10),
                                  right: ScreenUtil().setWidth(10),
                                ),
                                alignment: Alignment.center,
                                // height:
                                //     ScreenUtil().setHeight(20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  border: Border.all(
                                    width: 1,
                                    color: Color.fromRGBO(230, 230, 230, 1),
                                  ),
                                ),
                                child: Container(
                                  margin: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(5),
                                    right: ScreenUtil().setWidth(5),
                                  ),
                                  child: Center(
                                    child: Text(
                                      widget.nftDetail.protocol.toUpperCase(),
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(10),
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                widget.nftDetail.address.replaceRange(9,
                                    widget.nftDetail.address.length - 9, '...'),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: ThemeUtils().getTextStyle(
                                  'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              InkWell(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  await Clipboard.setData(
                                    ClipboardData(
                                      text: widget.nftDetail.address,
                                    ),
                                  );

                                  Fluttertoast.showToast(
                                    msg: S.current.copy_success,
                                  );
                                },
                                child: Row(
                                  children: [
                                    Opacity(
                                      opacity: 0.8,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: ScreenUtil().setWidth(12)),
                                        height: ScreenUtil().setHeight(11),
                                        width: ScreenUtil().setWidth(12),
                                        child: Image.asset(
                                          'assets/imgs/wallet_copy_address.png',
                                          package: 'wallet_flutter',
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                  // widget.nftDetail.name != "" && widget.nftDetail.name != null
                  //     ? Column(
                  //         crossAxisAlignment: CrossAxisAlignment.start,
                  //         children: [
                  //             Container(
                  //                 margin: EdgeInsets.only(
                  //                     top: ScreenUtil().setHeight(25),
                  //                     left: ScreenUtil().setWidth(22),
                  //                     right: ScreenUtil().setWidth(22),
                  //                     bottom: ScreenUtil().setHeight(25)),
                  //                 height: 0.5,
                  //                 color: Color.fromRGBO(216, 216, 216, 1)),
                  //             //介绍
                  //             Container(
                  //               margin: EdgeInsets.only(
                  //                   left: ScreenUtil().setWidth(22),
                  //                   right: ScreenUtil().setWidth(22)),
                  //               child: ExpandableText(
                  //                   text: widget.nftDetail.name ?? "",
                  //                   maxLines: 3,
                  //                   style: ThemeUtils().getTextStyle(
                  //                       'views.chaincore.ethereum.assets.textstyle.cell_subtitle')),
                  //             )
                  //           ])
                  //     : Container(),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(25),
                      bottom: ScreenUtil().setHeight(16),
                    ),
                    height: ScreenUtil().setHeight(10),
                    color: Color.fromRGBO(246, 246, 246, 1),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: nFtTokenInfo.value.length == 1
                  ? CrossAxisAlignment.start
                  : CrossAxisAlignment.center,
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                        left: ScreenUtil().setWidth(22),
                        right: ScreenUtil().setWidth(22),
                        bottom: ScreenUtil().setHeight(20)),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(S.current.ethereum_home_baritem_title),
                          InkWell(
                            onTap: () {
                              setState(() {
                                showOption.value = !showOption.value;
                              });
                            },
                            child: Container(
                              height: ScreenUtil().setHeight(16),
                              width: ScreenUtil().setWidth(16),
                              child: Image.asset(
                                showOption.value == true
                                    ? "assets/images/image_show_ options.png"
                                    : "assets/images/list_show_ options.png",
                                height: ScreenUtil().setHeight(16),
                                width: ScreenUtil().setWidth(16),
                              ),
                            ),
                          )
                        ])),
                showOption.value == true
                    ? imageShowCollection()
                    : listShowCollection()
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget imageShowCollection() {
    return ValueListenableBuilder(
        valueListenable: nFtTokenInfo,
        builder: (context, value, child) {
          return nFtTokenInfo.value.length > 0
              ? Wrap(
                  children: nFtTokenInfo.value
                      .map(
                        (e) => Wrap(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.of(context, rootNavigator: true).push(
                                  ViewAnimations.viewRightIn(
                                    NftTokenDetail(
                                      name: e['name'],
                                      description: e['description'],
                                      imgUrl: e['imgUrl'],
                                      tokenId: e['tokenId'],
                                      amount: e['amount'],
                                      dataSourceProvider:
                                          widget.dataSourceProvider,
                                      networkConfig: widget.networkConfig,
                                      web3clientProvider:
                                          widget.web3clientProvider,
                                      nftTokenblog: widget.nftDetail,
                                      account: widget.account,
                                    ),
                                  ),
                                );
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width / 2 -
                                    ScreenUtil().setWidth(10),
                                child: AspectRatio(
                                  aspectRatio: 0.74,
                                  child: Container(
                                    margin: EdgeInsets.only(
                                      left: value.length == 1
                                          ? ScreenUtil().setWidth(15)
                                          : ScreenUtil().setWidth(5),
                                      right: ScreenUtil().setWidth(5),
                                      bottom: ScreenUtil().setHeight(10),
                                    ),
                                    decoration: BoxDecoration(
                                      // color: Colors.grey[100],
                                      border: Border.all(
                                        width: 0.5,
                                        color: Color.fromRGBO(230, 230, 230, 1),
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: CachedImage.assetNetwork(
                                              fit: BoxFit.fill,
                                              url: e['imgUrl'],
                                              placeholder: Image.asset(
                                                  'assets/imgs/nft_default.png',
                                                  package: 'wallet_flutter',
                                                  fit: BoxFit.fill),
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2 -
                                                  ScreenUtil().setWidth(20),
                                              height: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2 -
                                                  ScreenUtil().setWidth(20),
                                              imageCacheHeight: 100,
                                              imageCacheWidth: 100),
                                        )),
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: ScreenUtil().setHeight(11),
                                            left: ScreenUtil().setWidth(5),
                                            bottom: ScreenUtil().setHeight(5),
                                          ),
                                          child: Text(
                                            e['name'] ?? "",
                                            maxLines: 1,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                            left: ScreenUtil().setWidth(5),
                                          ),
                                          child: Text(
                                            "#" + e['tokenId'],
                                            maxLines: 1,
                                            style: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                        ercType == "erc1155"
                                            ? Container(
                                                alignment:
                                                    Alignment.bottomRight,
                                                margin: EdgeInsets.only(
                                                  right:
                                                      ScreenUtil().setWidth(10),
                                                ),
                                                child: Text(
                                                  "x" + e['amount'],
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                    fontSize:
                                                        ScreenUtil().setSp(14),
                                                  ),
                                                ),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                      .toList())
              : nodataview();
          // Container(
          //     child:
          //         Image.asset("assets/images/nft_default.png", scale: 3.1));
        });
  }

  Widget listShowCollection() {
    return nFtTokenInfo.value.length > 0
        ? Column(
            children: nFtTokenInfo.value
                .map((e) => InkWell(
                      onTap: () {
                        Navigator.of(context, rootNavigator: true)
                            .push(ViewAnimations.viewRightIn(NftTokenDetail(
                          name: e['name'],
                          description: e['description'],
                          imgUrl: e['imgUrl'],
                          tokenId: e['tokenId'],
                          amount: e['amount'],
                          dataSourceProvider: widget.dataSourceProvider,
                          networkConfig: widget.networkConfig,
                          web3clientProvider: widget.web3clientProvider,
                          nftTokenblog: widget.nftDetail,
                          account: widget.account,
                        )));
                      },
                      child: Column(children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(10)),
                                  child: Row(children: [
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: ScreenUtil().setWidth(26)),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(2),
                                            child: CachedImage.assetNetwork(
                                                url: e['imgUrl'],
                                                placeholder: Image.asset(
                                                    'assets/imgs/nft_default.png',
                                                    package: 'wallet_flutter',
                                                    fit: BoxFit.fill),
                                                width:
                                                    ScreenUtil().setWidth(30),
                                                height:
                                                    ScreenUtil().setWidth(30),
                                                imageCacheHeight: 100,
                                                imageCacheWidth: 100))),
                                    Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  left: ScreenUtil()
                                                      .setWidth(18)),
                                              width: ScreenUtil().setWidth(100),
                                              child: Text(e['name'] ?? "",
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: ScreenUtil()
                                                          .setSp(12),
                                                      color: Color.fromRGBO(
                                                          58, 59, 61, 1)))),
                                          Container(
                                            width: ScreenUtil().setWidth(100),
                                            margin: EdgeInsets.only(
                                              top: ScreenUtil().setHeight(10),
                                              left: ScreenUtil().setWidth(18),
                                            ),
                                            child: Text(
                                              "#" + e['tokenId'],
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: ThemeUtils().getTextStyle(
                                                  'views.chaincore.ethereum.assets.textstyle.cell_subtitle'),
                                            ),
                                          ),
                                        ])
                                  ])),
                              Row(
                                children: [
                                  ercType == "erc1155"
                                      ? Container(
                                          alignment: Alignment.centerRight,
                                          margin: EdgeInsets.only(
                                              right: ScreenUtil().setWidth(10)),
                                          child: Text(
                                            e['amount'],
                                            maxLines: 1,
                                            style: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                          ),
                                        )
                                      : Container(),
                                  Container(
                                      margin: EdgeInsets.only(
                                          right: ScreenUtil().setWidth(29)),
                                      child: Icon(Icons.arrow_forward_ios,
                                          size: ScreenUtil().setSp(14),
                                          color: Colors.grey))
                                ],
                              )
                            ]),
                        Container(
                            margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(25),
                                left: ScreenUtil().setWidth(22),
                                right: ScreenUtil().setWidth(22),
                                bottom: ScreenUtil().setHeight(25)),
                            height: 0.5,
                            color: Colors.grey[200])
                      ]),
                    ))
                .toList())
        : nodataview();
  }

  ValueListenableBuilder nodataview() {
    return ValueListenableBuilder(
        valueListenable: nodataImageListenable,
        builder: (context, value, child) {
          return nodataImageListenable.value == false &&
                  widget.balanceIndex != 0
              ? Container(
                  child: Center(
                    child: LineSpinFadeLoaderIndicator(
                      ballColor: Colors.grey,
                    ),
                  ),
                )
              : NoDataView(
                  imgPath: 'assets/images/no_token.png',
                  tips: S.current.ethereum_asset_management_search_nodata_tips,
                  topHeight: 146,
                );
        });
  }
}
