// @dart=2.9
part of '../../asset.dart';

class NftAssetCell extends StatefulWidget {
  final Ethereum.TokenBlog nftDetail;
  final ValueNotifier<DateTime> refreshListenable;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final WalletKey account;

  const NftAssetCell({
    this.nftDetail,
    this.refreshListenable,
    this.dataSourceProvider,
    this.account,
    this.networkConfig,
  });
  @override
  State<NftAssetCell> createState() => _NftAssetCellState();
}

class _NftAssetCellState extends State<NftAssetCell> {
  int nftCount;
  final ValueNotifier<Amount> totalListenable =
      ValueNotifier<Amount>(Amount(value: BigInt.from(0), decimals: 0));

  void fetchPrice() async {
    widget.dataSourceProvider.localService
        .getNftCachedBalance(widget.nftDetail, widget.account.address)
        .then((cacheCount) {
      // print(cacheCount.balance);
      if (cacheCount != null) {
        // _getCachTokenPrice();

        this.totalListenable.value =
            Amount(value: cacheCount.balance, decimals: 0);
      }
      if (cacheCount == null ||
          DateTime.now().difference(cacheCount.updateTime) >
              Duration(seconds: 15)) {
        // getNFTTokenCount();
        Utils.futureGetErc721Balance(
          Web3ClientProvider.of(context),
          widget.nftDetail,
          widget.account.address,
        ).then((balance) {
          this.totalListenable.value = balance;
          widget.dataSourceProvider.localService.putNftCachedBalance(
            widget.nftDetail,
            widget.account.address,
            balance.value,
          );
        });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getNFTTokenList();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context, rootNavigator: true).push(
          ViewAnimations.viewRightIn(
            NftDetail(
              nftDetail: widget.nftDetail,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
              web3clientProvider: Web3ClientProvider.of(context),
              account: widget.account,
              balanceIndex: totalListenable.value.value.toInt(),
            ),
          ),
        );
      },
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(),
            padding: EdgeInsets.only(
              // top: ScreenUtil().setHeight(26),
              left: ScreenUtil().setWidth(22),
              right: ScreenUtil().setWidth(22),
            ),
            height: ScreenUtil().setHeight(68),
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  width: ScreenUtil().setWidth(40),
                  height: ScreenUtil().setHeight(40),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Colors.grey[200],
                    ),
                    shape: BoxShape.circle,
                  ),
                  child: ClipPath.shape(
                    shape: CircleBorder(),
                    child: CachedImage.assetNetwork(
                      url: widget.nftDetail.logo,
                      placeholder: Image.asset(
                        'assets/imgs/nft_default.png',
                        package: 'wallet_flutter',
                        fit: BoxFit.fill,
                      ),
                      width: ScreenUtil().setWidth(30),
                      height: ScreenUtil().setWidth(30),
                      imageCacheHeight: 100,
                      imageCacheWidth: 100,
                    ),
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(15)),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: ScreenUtil().setWidth(200),
                      child: Text(
                        widget.nftDetail.symbol,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(19),
                          color: Color.fromRGBO(58, 59, 61, 1),
                        ),
                      ),
                    )
                  ],
                ),
                Expanded(child: SizedBox()),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    // Text(
                    //   // widget.usdresponseBody['owns_total'].toString(),
                    //   nftCount.toString(),
                    //   style: TextStyle(
                    //     fontSize: ScreenUtil().setSp(19),
                    //     color: Color.fromRGBO(58, 59, 61, 1),
                    //   ),
                    // ),
                    ValueListenableBuilder(
                        valueListenable: widget.refreshListenable,
                        builder: (context, _, _child) {
                          fetchPrice();
                          return ValueListenableBuilder(
                            valueListenable: totalListenable,
                            builder: (context, value, child) {
                              return Column(
                                children: [
                                  Container(
                                    child: Text(
                                      totalListenable.value.value.toString(),
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(14),
                                        color: Color.fromRGBO(175, 175, 175, 1),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          );
                        })
                  ],
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(24),
              right: ScreenUtil().setWidth(20),
            ),
            height: 1,
            color: Color.fromRGBO(241, 241, 241, 1),
          )
        ],
      ),
    );
  }
}
