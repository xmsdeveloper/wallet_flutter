// @dart=2.9
part of '../../asset.dart';

class HomeAassetPage extends StatefulWidget {
  final Ethereum.DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final WalletKey account;
  List<Ethereum.TokenBlog> sTokens;
  HomeAassetPage({
    this.dataSourceProvider,
    this.web3clientProvider,
    this.sTokens,
    this.account,
  });

  @override
  State<HomeAassetPage> createState() => _HomeAassetPageState();
}

class _HomeAassetPageState extends State<HomeAassetPage> {
  static List<GlobalKey<RemovableItemState>> childItemStates = [];

  final ValueNotifier<List<String>> balanceList =
      ValueNotifier<List<String>>([]);

  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(null);

  ValueNotifier<List<Ethereum.TokenBlog>> homeAssetList =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    homeAssetList.value = widget.sTokens;
    eventBus.on<HomeAssetDelete>().listen((event) {
      widget.dataSourceProvider.localService.favoriteTokens.then(
        (value) {
          // setState(() {
          //   widget.sTokens = value;
          // });
          homeAssetList.value = value;
        },
      );
    });
    // setState(() {});

    for (var i = 0; i < widget.sTokens.length; i++) {
      GlobalKey<RemovableItemState> removeGK = GlobalKey(debugLabel: "$i");
      childItemStates.add(removeGK);

      Utils.futureGetBalance(
        // Web3ClientProvider.of(context),
        widget.web3clientProvider,
        widget.sTokens[i],
        widget.account.address,
      ).then(
        (balance) {
          // this.balanceListenable.value = balance;

          try {
            print(balance.decimals);
            balance.decimals != 0
                ? balanceList.value.add(balance.toStringAsFixed())
                : balanceList.value.add("0");

            widget.dataSourceProvider.localService.putCachedBalance(
              widget.sTokens[i],
              widget.account.address,
              balance.value,
            );
          } catch (e) {}
          setState(() {});
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_asset_management_home_asset_title,
        elevation: 0.3,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: Container(
                height: MediaQuery.of(context).size.height * 0.9,
                width: MediaQuery.of(context).size.width,
                child: ValueListenableBuilder(
                    valueListenable: homeAssetList,
                    builder: (context, value, child) {
                      return ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: value.length,
                          itemBuilder: (context, i) {
                            return Column(
                              children: [
                                RemovableItem(
                                  sTokens: value[i],
                                  dataSourceProvider: widget.dataSourceProvider,
                                  visible: AccountDetai(
                                    sTokens: value[i],
                                    account: widget.account,
                                    dataSourceProvider:
                                        widget.dataSourceProvider,
                                    web3clientProvider:
                                        widget.web3clientProvider,
                                  ),
                                  moveItemKey: childItemStates[i],
                                  onActionDown: () {
                                    //循环遍历列表，关闭非当前item打开项
                                    closeItems(childItemStates, i);
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Container(
                                    width: double.infinity,
                                    height: 0.5,
                                    color: Colors.grey[200],
                                  ),
                                )
                              ],
                            );
                          });
                    })),
          )
        ],
      ),
    );
  }

  /// 循环遍历，利用State.来关闭非当前Item
  static void closeItems(
      List<GlobalKey<RemovableItemState>> childItemStates, int index) {
    childItemStates.forEach((element) {
      if (element != childItemStates[index]) {
        element.currentState?.closeItems();
      }
    });
  }
}
