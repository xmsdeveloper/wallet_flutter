// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/cache_image.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/services/entity/_token_blog.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/utils/token_utils.dart'
    as Utils;
import 'package:wallet_flutter/views/chaincore/ethereum/utils/gas_config_board.dart'
    as Utils;
import 'package:wallet_flutter/views/chaincore/ethereum/utils/gas_price_estimated_utils.dart'
    as Utils;
import 'package:wallet_flutter/views/chaincore/ethereum/utils/gas_price.dart'
    as Utils;

class NewAssetDetail extends StatefulWidget {
  final TokenBlog newAsset;
  final DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final WalletKey account;
  NewAssetDetail(
      {this.newAsset,
      this.dataSourceProvider,
      this.web3clientProvider,
      this.account});

  @override
  State<NewAssetDetail> createState() => _NewAssetDetailState();
}

class _NewAssetDetailState extends State<NewAssetDetail> {
  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(Amount(
    value: BigInt.from(0),
    decimals: 18,
  ));
  void fetchBalance() {
    widget.dataSourceProvider.localService
        .getCachedBalance(widget.newAsset, widget.newAsset.address)
        .then((cacheBalance) {
      if (cacheBalance != null) {
        this.balanceListenable.value = Amount(
          value: cacheBalance.balance,
          decimals: widget.newAsset.decimals,
        );
      }

      if (cacheBalance == null ||
          DateTime.now().difference(cacheBalance.updateTime) >
              Duration(seconds: 30)) {
        Utils.futureGetBalance(
          // Web3ClientProvider.of(context),
          widget.web3clientProvider,
          widget.newAsset,
          widget.account.address,
        ).then((balance) {
          this.balanceListenable.value = balance;

          widget.dataSourceProvider.localService.putCachedBalance(
            widget.newAsset,
            widget.account.address,
            balance.value,
          );
        });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchBalance();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // widget.newAsset.indexWhere((element) => element == e) != 0
        //     ?

        // : Container(),
        Container(
          // height: ScreenUtil().setHeight(78),
          decoration: BoxDecoration(
            // borderRadius: BorderRadius.circular(4),
            color: Colors.white,
            // border: Border(
            //   bottom: BorderSide(
            //     width: 0.2,
            //     color: Color.fromRGBO(216, 216, 216, 1),
            //   ),
            // ),
          ),
          margin: EdgeInsets.only(
            top: ScreenUtil().setHeight(10),
            bottom: ScreenUtil().setHeight(10),
          ),
          child: ListTile(
            minLeadingWidth: ScreenUtil().setWidth(0),
            contentPadding: EdgeInsets.only(
              left: ScreenUtil().setWidth(18),
              right: ScreenUtil().setWidth(0),
            ),
            leading: Container(
              width: ScreenUtil().setWidth(35),
              height: ScreenUtil().setHeight(35),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Colors.grey[200],
                ),
                shape: BoxShape.circle,
              ),
              child: ClipPath.shape(
                shape: CircleBorder(),
                child: CachedImage.assetNetwork(
                  // url: "https://hashpayinfo.github.io/bsc/" +
                  //     e.address +
                  //     "/logo.png",
                  // url:
                  //     "https://hashpayinfo.github.io/bsc/0x55d398326f99059fF775485246999027B3197955/logo.png",
                  url: widget.newAsset.logo,
                  placeholder: Image.asset(
                    'assets/imgs/default_token.png',
                    package: 'wallet_flutter',
                  ),
                  // width: ScreenUtil().setWidth(30),
                  // height: ScreenUtil().setWidth(30),
                  imageCacheHeight: 100,
                  imageCacheWidth: 100,
                ),
              ),
            ),
            title: Container(
              // height: ScreenUtil().setHeight(40),
              // width: ScreenUtil().setWidth(220),

              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.newAsset.symbol,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.assets.textstyle.cell_title',
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(2)),
                  Text(
                    // e.address,

                    widget.newAsset.address.length > 0
                        ? widget.newAsset.address.replaceRange(
                            9, widget.newAsset.address.length - 9, '...')
                        : widget.newAsset.address.replaceRange(
                            9, widget.newAsset.address.length - 9, '...'),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(2)),
                  Row(
                    children: [
                      ValueListenableBuilder(
                        valueListenable: balanceListenable,
                        builder: (context, vaue, child) {
                          return Text(
                            balanceListenable.value.decimals != 0
                                ? S.current
                                        .ethereum_asset_management_home_asset_balance +
                                    balanceListenable.value.toStringAsFixed()
                                : S.current
                                        .ethereum_asset_management_home_asset_balance +
                                    "0",
                            style: ThemeUtils().getTextStyle(
                              'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                            ),
                          );
                        },
                      )
                      // Text(
                      //   balanceList.value.length >= this.widget.sTokens.length
                      //       ? S.current
                      //               .ethereum_asset_management_home_asset_balance +
                      //           balanceList.value[i]
                      //       : S.current
                      //               .ethereum_asset_management_home_asset_balance +
                      //           "0",
                      //   style: ThemeUtils().getTextStyle(
                      //     'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                      //   ),
                      // )
                    ],
                  )
                ],
              ),
            ),
            trailing: Container(
              padding: EdgeInsets.only(
                right: ScreenUtil().setWidth(22),
              ),
              child: InkWell(
                // onTap: () async {
                //   await widget.dataSourceProvider.localService
                //       .insertFavoriteToken(
                //     widget.newAsset,
                //   );
                //   widget.dataSourceProvider.localService.insertMyAsset(
                //       CoreProvider.of(context).networkConfig.mainSymbol,
                //       CoreProvider.of(context).account.address,
                //       widget.newAsset);
                //   // newAsset.remove(e);
                //   // _getlastMyAsset();
                //   setState(() {});
                // },
                child: Container(
                    child: Image.asset(
                  'assets/images/switch_off.png',
                  width: ScreenUtil().setWidth(38),
                  height: ScreenUtil().setHeight(19),
                )),
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(20),
            right: ScreenUtil().setWidth(20),
          ),
          height: 0.2,
          color: Color.fromRGBO(216, 216, 216, 1),
        ),
      ],
    );
  }
}
