// @dart=2.9
part of '../../asset.dart';

class AccountDetai extends StatefulWidget {
  final TokenBlog sTokens;
  final WalletKey account;
  final DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final List<Ethereum.TokenBlog> favoriteTokens;
  final bool isHome;
  const AccountDetai(
      {this.sTokens,
      this.dataSourceProvider,
      this.account,
      this.web3clientProvider,
      this.isHome,
      this.favoriteTokens});

  @override
  State<AccountDetai> createState() => _AccountDetaiState();
}

class _AccountDetaiState extends State<AccountDetai> {
  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(Amount(
    value: BigInt.from(0),
    decimals: 18,
  ));
  void fetchBalance() {
    widget.dataSourceProvider.localService
        .getCachedBalance(widget.sTokens, widget.account.address)
        .then((cacheBalance) {
      if (cacheBalance != null) {
        this.balanceListenable.value = Amount(
          value: cacheBalance.balance,
          decimals: widget.sTokens.decimals,
        );
      }

      if (cacheBalance == null ||
          DateTime.now().difference(cacheBalance.updateTime) >
              Duration(seconds: 30)) {
        Utils.futureGetBalance(
          // Web3ClientProvider.of(context),
          widget.web3clientProvider,
          widget.sTokens,
          widget.account.address,
        ).then((balance) {
          this.balanceListenable.value = balance;

          widget.dataSourceProvider.localService.putCachedBalance(
            widget.sTokens,
            widget.account.address,
            balance.value,
          );
        });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchBalance();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: ScreenUtil().setHeight(60),
      height: ScreenUtil().setHeight(87),
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(4),
        color: Colors.white,
        border: Border(
          top: BorderSide(
            width: 0.2,
            color: Color.fromRGBO(216, 216, 216, 1),
          ),
        ),
      ),

      // margin: EdgeInsets.only(
      //   top: ScreenUtil().setHeight(2),
      // ),
      // height: ScreenUtil().setHeight(73),
      child: ListTile(
        minLeadingWidth: ScreenUtil().setWidth(0),
        contentPadding: EdgeInsets.only(
          left: ScreenUtil().setWidth(18),
          bottom: ScreenUtil().setHeight(8),
          right: ScreenUtil().setWidth(0),
        ),
        leading: Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(6),
                ),
                width: ScreenUtil().setWidth(35),
                height: ScreenUtil().setWidth(35),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.2,
                    color: Colors.grey[200],
                  ),
                  shape: BoxShape.circle,
                ),
                child:
                    // Image.network(
                    //   snapshot.data.data[index].logo,
                    // ),

                    ClipPath.shape(
                  shape: CircleBorder(),
                  child:
                      // CachedImage.assetNetwork(
                      //   // url: e.logo,
                      //   url: widget.sTokens.logo ??
                      //       CoreProvider.of(context).endPoint.resourcesServicesURL +
                      //           '/' +
                      //           widget.sTokens.address +
                      //           "/logo.png" ??
                      //       widget.sTokens.logo,
                      //   // widget.sTokens[i].logo ??
                      //   //     CoreProvider.of(context)
                      //   //             .endPoint
                      //   //             .resourcesServicesURL +
                      //   //         '/' +
                      //   //         widget.sTokens[i].address +
                      //   //         "/logo.png" ??
                      //   //     widget.sTokens[i].logo,
                      //   placeholder: Image.asset(
                      //     'assets/imgs/default_token.png',
                      //     package: 'wallet_flutter',
                      //   ),
                      //   width: ScreenUtil().setWidth(30),
                      //   height: ScreenUtil().setWidth(30),
                      //   imageCacheHeight: 100,
                      //   imageCacheWidth: 100,
                      // ),
                      //     Image.network(
                      //   widget.sTokens.logo ??
                      //       CoreProvider.of(context).endPoint.resourcesServicesURL +
                      //           '/' +
                      //           widget.sTokens.address +
                      //           "/logo.png" ??
                      //       widget.sTokens.logo,
                      // ),
                      Image.network(
                    widget.sTokens.logo ??
                        CoreProvider.of(context).endPoint.resourcesServicesURL +
                            '/' +
                            widget.sTokens.address +
                            "/logo.png" ??
                        widget.sTokens.logo,
                    errorBuilder: (context, error, StackTrace) {
                      return Image.asset(
                        'assets/imgs/default_token.png',
                        package: 'wallet_flutter',
                        width: ScreenUtil().setWidth(30),
                        height: ScreenUtil().setHeight(30),
                        fit: BoxFit.fill,
                      );
                    },
                    frameBuilder: (context, child, frame, wasSynchronusLoaded) {
                      if (wasSynchronusLoaded) {
                        return child;
                      }
                      return AnimatedOpacity(
                        child: child,
                        opacity: frame == null ? 0 : 1,
                        duration: const Duration(seconds: 2),
                        curve: Curves.easeOut,
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        title: Container(
          // margin: EdgeInsets.only(
          //   bottom: ScreenUtil().setHeight(30),
          // ),
          // decoration: BoxDecoration(
          //     border: Border.all(
          //   width: 1,
          // )),
          height: ScreenUtil().setHeight(80),
          // height: ScreenUtil().setHeight(40),
          width: ScreenUtil().setWidth(220),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                // e.symbol,
                widget.sTokens.symbol,
                style: ThemeUtils().getTextStyle(
                  'views.chaincore.ethereum.assets.textstyle.cell_title',
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(2)),
              widget.sTokens.address != ""
                  ? Text(
                      // widget.sTokens.address,
                      widget.sTokens.address.length > 0
                          ? widget.sTokens.address.replaceRange(
                              9, widget.sTokens.address.length - 9, '...')
                          : widget.sTokens.address.replaceRange(
                              9, widget.sTokens.address.length - 9, '...'),

                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                      ),
                    )
                  : Container(),
              SizedBox(height: ScreenUtil().setHeight(2)),
              Row(
                children: [
                  ValueListenableBuilder(
                    valueListenable: balanceListenable,
                    builder: (context, vaue, child) {
                      return Text(
                        // balanceListenable.value.decimals != 0
                        //     ? S.current
                        //             .ethereum_asset_management_home_asset_balance +
                        //         balanceListenable.value.toStringAsFixed()
                        //     : S.current
                        //             .ethereum_asset_management_home_asset_balance +
                        //         "0",
                        balanceListenable.value.decimals != 0
                            // ? balance.toStringAsFixed()
                            ? S.current
                                    .ethereum_asset_management_home_asset_balance +
                                balanceListenable.value.toStringAsFixed(
                                  fractionDigits:
                                      balanceListenable.value.decimals > 8
                                          ? 4
                                          : balanceListenable.value.decimals,
                                )
                            : "0",
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                        ),
                      );
                    },
                  )
                  // Text(
                  //   balanceList.value.length >= this.widget.sTokens.length
                  //       ? S.current
                  //               .ethereum_asset_management_home_asset_balance +
                  //           balanceList.value[i]
                  //       : S.current
                  //               .ethereum_asset_management_home_asset_balance +
                  //           "0",
                  //   style: ThemeUtils().getTextStyle(
                  //     'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                  //   ),
                  // )
                ],
              )
            ],
          ),
        ),
        trailing: widget.sTokens.symbol !=
                CoreProvider().networkConfig.mainSymbol
            ? widget.isHome != false
                ? Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(
                      right: ScreenUtil().setWidth(20),
                    ),
                    width: ScreenUtil().setWidth(95),
                    child:
                        // Switch(
                        //     activeColor: Colors.blue,
                        //     value: widget.sTokens
                        //         .contains(widget.sTokens[i]),
                        //     onChanged: (isFav) {
                        //       if (isFav) {
                        //         widget.dataSourceProvider.localService
                        //             .insertFavoriteToken(
                        //           widget.sTokens[i],
                        //         );
                        //       } else {
                        //         widget.dataSourceProvider.localService
                        //             .removeFavoriteTokens(
                        //           [widget.sTokens[i]],
                        //         );
                        //       }

                        //       widget.dataSourceProvider.localService
                        //           .favoriteTokens
                        //           .then(
                        //         (value) {
                        //           setState(() {
                        //             // widget.sTokens = value;
                        //           });
                        //         },
                        //       );
                        //     }),
                        Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            bottom: ScreenUtil().setHeight(10),
                          ),
                          width: ScreenUtil().setHeight(25),
                          height: ScreenUtil().setHeight(25),
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            shape: BoxShape.circle,
                          ),
                          child: Icon(
                            Icons.horizontal_rule,
                            color: Colors.white,
                            size: ScreenUtil().setHeight(20),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(
                    // alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(15),
                      right: ScreenUtil().setWidth(22),
                    ),

                    child: InkWell(
                      onTap: () async {
                        if (!widget.favoriteTokens.contains(widget.sTokens)) {
                          if (await widget.dataSourceProvider.localService
                                  .favoriteTokens
                                  .then((value) => value.length) <=
                              20) {
                            widget.dataSourceProvider.localService
                                .insertFavoriteToken(
                              widget.sTokens,
                            );
                          } else {
                            Fluttertoast.showToast(
                              msg: S.current.ethereum_home_asset_limit_warming,
                            );
                          }
                        } else {
                          widget.dataSourceProvider.localService
                              .removeFavoriteTokens(
                            [widget.sTokens],
                          );
                          widget.dataSourceProvider.localService
                              .tokenPricetListDelete(
                                  CoreProvider.of(context).account.address,
                                  await widget.dataSourceProvider.localService
                                      .favoriteTokens
                                      .then((value) => value.indexOf(
                                            widget.sTokens,
                                          )));
                        }
                        widget.dataSourceProvider.localService.favoriteTokens
                            .then(
                          (value) {
                            // setState(() {
                            //   widget.favoriteTokens = value;

                            //   fetchTokenDataSource();
                            // });
                            setState(() {});
                          },
                        );
                      },
                      child: Container(
                        child: Image.asset(
                          !widget.favoriteTokens.contains(widget.sTokens)
                              ? 'assets/images/switch_off.png'
                              : 'assets/images/switch_on.png',
                          width: ScreenUtil().setWidth(38),
                          height: ScreenUtil().setHeight(19),
                        ),
                      ),
                    ),
                  )
            : Container(
                child: Text(''),
              ),
      ),
    );
  }
}
