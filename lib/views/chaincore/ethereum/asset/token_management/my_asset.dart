// @dart=2.9
part of '../../asset.dart';

class MyassetPage extends StatefulWidget {
  final Ethereum.DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final WalletKey account;
  final List<Ethereum.TokenBlog> myAssets;
  final List<Ethereum.TokenBlog> favoriteTokens;
  final List<Ethereum.TokenBlog> show;

  const MyassetPage({
    this.dataSourceProvider,
    this.web3clientProvider,
    this.myAssets,
    this.account,
    this.favoriteTokens,
    this.show,
  });

  @override
  State<MyassetPage> createState() => _MyassetPageState();
}

class _MyassetPageState extends State<MyassetPage> {
  List<String> balanceList = [];
  final ValueNotifier<bool> ignoreShow = ValueNotifier<bool>(false);
  final ValueNotifier<List<Ethereum.TokenBlog>> sTokens =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  List<Ethereum.TokenBlog> favoriteTokens = [];

  List<Ethereum.TokenBlog> newAsset = [];

  final ValueNotifier<List<Ethereum.TokenBlog>> lastMyAsset =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  List<TokenBlog> showMyTokens = [];
  _getNewAsset() async {
    //查询已添加过的代币
    final sourceProvider = widget.dataSourceProvider;
    final sourceCoreProvider = CoreProvider.of(context);
    showMyTokens = await sourceProvider.localService.getCachedBlog(
        sourceCoreProvider.networkConfig.mainSymbol,
        sourceCoreProvider.account.address);
    newAsset.clear();
    widget.show.map((e) {
      if (!newAsset.contains(e) && !showMyTokens.contains(e)) {
        newAsset.add(e);
      }
    }).toList();
  }

  _getlastMyAsset() async {
    final sourceProvider = widget.dataSourceProvider;
    final sourceCoreProvider = CoreProvider.of(context);
    showMyTokens = await sourceProvider.localService.getCachedBlog(
        sourceCoreProvider.networkConfig.mainSymbol,
        sourceCoreProvider.account.address);
    // newAsset.clear();
    lastMyAsset.value.clear();
    // await _getNewAsset();
    widget.myAssets.map((e) {
      if (!newAsset.contains(e)) {
        lastMyAsset.value.add(e);
      }
    }).toList();
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (!widget.myAssets.contains(widget.favoriteTokens[0])) {
      widget.myAssets.insert(0, widget.favoriteTokens[0]);
    }
    _getNewAsset();
    _getlastMyAsset();
    for (var i = 0; i < widget.myAssets.length; i++) {
      widget.dataSourceProvider.localService
          .getCachedBalance(widget.myAssets[i], widget.account.address)
          .then(
        (cacheBalance) {
          if (cacheBalance != null) {
            // this.balanceListenable.value = Amount(
            //   value: cacheBalance.balance,
            //   decimals: widget.sTokens[i].decimals,
            // );
            balanceList.add(Amount(
                    value: cacheBalance.balance,
                    decimals: widget.myAssets[i].decimals)
                .toStringAsFixed());

            //  .toStringAsFixed();

            // balanceList.add(cacheBalance.balance.toString());
          }

          if (cacheBalance == null ||
              DateTime.now().difference(cacheBalance.updateTime) >
                  Duration(seconds: 0)) {
            Utils.futureGetBalance(
              // Web3ClientProvider.of(context),
              widget.web3clientProvider,
              widget.myAssets[i],
              widget.account.address,
            ).then(
              (balance) {
                // this.balanceListenable.value = balance;

                balanceList.add(balance.toStringAsFixed());

                widget.dataSourceProvider.localService.putCachedBalance(
                  widget.myAssets[i],
                  widget.account.address,
                  balance.value,
                );
                setState(() {});
              },
            );
          }
          setState(() {});
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_asset_management_home_asset_my_balance,
        elevation: 0.3,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
                valueListenable: ignoreShow,
                builder: (context, value, child) {
                  return value == false && newAsset.length > 0
                      ? Column(
                          children: [
                            // ValueListenableBuilder(
                            //   valueListenable: myAssetList,
                            //   builder: (context, value, child) {
                            //     return
                            //   },

                            Container(
                                margin: EdgeInsets.only(
                                  top: ScreenUtil().setHeight(20),
                                  left: ScreenUtil().setWidth(18),
                                  right: ScreenUtil().setWidth(8),
                                  // bottom: ScreenUtil().setHeight(10),
                                ),
                                // color: Color.fromRGBO(246, 246, 246, 1),
                                // height: ScreenUtil().setHeight(35),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          S.current
                                              .ethereum_asset_management_my_balance_new_asset,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(16),
                                            // color: Color.fromRGBO(
                                            //     112, 112, 112, 1),
                                          ),
                                        )
                                      ],
                                    ),
                                    InkWell(
                                      onTap: () {
                                        ignoreShow.value = true;
                                        eventBus.fire(new NewAssetNum(0));
                                        widget.show
                                            .map((e) => widget
                                                .dataSourceProvider.localService
                                                .insertMyAsset(
                                                    CoreProvider.of(context)
                                                        .networkConfig
                                                        .mainSymbol,
                                                    CoreProvider.of(context)
                                                        .account
                                                        .address,
                                                    e))
                                            .toList();
                                        _getlastMyAsset();
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          right: ScreenUtil().setWidth(17),
                                        ),
                                        child: Text(
                                          S.current
                                              .ethereum_asset_management_my_balance_show_ignore,
                                          style: TextStyle(
                                            fontSize: ScreenUtil().setSp(14),
                                            color: Color.fromRGBO(
                                                112, 112, 112, 1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                            showTokenPage(),
                            Container(
                              height: ScreenUtil().setHeight(10),
                              color: Color.fromRGBO(246, 246, 246, 1),
                            )
                          ],
                        )
                      : Container();
                }),
          ),
          SliverToBoxAdapter(
            child: Column(
              children: [
                // ignoreShow.value == false && newAsset.length > 0
                //     ? Container(
                //         height: ScreenUtil().setHeight(10),
                //         color: Color.fromRGBO(246, 246, 246, 1),
                //       )
                //     : Container(),
                Container(
                  child: Column(
                    children: [
                      ListTile(
                        contentPadding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                        ),
                        title: Text(
                          // group.groupName,
                          S.current
                              .ethereum_asset_management_my_balance_assetList,
                          style: ThemeUtils().getTextStyle(
                            'sheet.wallet_selector.textstyle.accounts_group_title',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
              child: ValueListenableBuilder(
                  valueListenable: lastMyAsset,
                  builder: (context, value, child) {
                    return Container(
                      height: MediaQuery.of(context).size.height * 0.9,
                      width: MediaQuery.of(context).size.width,
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: value.length,
                          itemBuilder: (context, i) {
                            return Column(
                              children: [
                                AccountDetai(
                                  sTokens: value[i],
                                  account: widget.account,
                                  dataSourceProvider: widget.dataSourceProvider,
                                  web3clientProvider: widget.web3clientProvider,
                                  isHome: false,
                                  favoriteTokens: widget.favoriteTokens,
                                ),
                              ],
                            );
                          }),
                    );
                  }))
        ],
      ),
    );
  }

  Widget showTokenPage() => Column(
        children: newAsset
            .map((e) => Container(
                  child: Stack(
                    children: [
                      NewAssetDetail(
                        newAsset: e,
                        dataSourceProvider: widget.dataSourceProvider,
                        web3clientProvider: widget.web3clientProvider,
                        account: widget.account,
                      ),
                      Positioned(
                        right: ScreenUtil().setWidth(22),
                        child: InkWell(
                          onTap: () async {
                            if (await widget.dataSourceProvider.localService
                                    .favoriteTokens
                                    .then((value) => value.length) <=
                                20) {
                              await widget.dataSourceProvider.localService
                                  .insertFavoriteToken(
                                e,
                              );
                            } else {
                              Fluttertoast.showToast(
                                  msg: S.current
                                      .ethereum_home_asset_limit_warming);
                            }

                            widget.dataSourceProvider.localService
                                .insertMyAsset(
                                    CoreProvider.of(context)
                                        .networkConfig
                                        .mainSymbol,
                                    CoreProvider.of(context).account.address,
                                    e);
                            newAsset.remove(e);
                            _getlastMyAsset();
                            setState(() {});
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(38),
                            height: ScreenUtil().setHeight(70),
                            // color: Colors.blue,
                          ),
                        ),
                      )
                    ],
                  ),
                ))
            .toList(),
      );
}
