// @dart=2.9
part of '../asset.dart';

class AssetView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AssetViewState();
  static BottomNavigationBarItem barItem() => BottomNavigationBarItem(
        label: S.current.ethereum_home_baritem_title,
        backgroundColor: ThemeUtils().getColor('utils.bottom_bar.background'),
        icon: Image.asset(
          'assets/imgs/tabs0.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: ThemeUtils().getColor(
            'utils.bottom_bar.normal_item_color',
          ),
        ),
        activeIcon: Image.asset(
          'assets/imgs/tabs0.png',
          package: 'wallet_flutter',
          width: ScreenUtil().setWidth(20),
          color: Colors.green[200],
          // ThemeUtils().getColor(
          //   'utils.bottom_bar.selected_item_color',
          // ),
        ),
      );
}

class _AssetViewState extends State<AssetView>
    with AutomaticKeepAliveClientMixin<AssetView> {
  @override
  bool get wantKeepAlive => true;

  final ValueNotifier<DateTime> refreshListenable =
      ValueNotifier<DateTime>(DateTime.now());
  //比较本地与网络时间  -1本地小于网络    0，1  等于，大于
  int compare;
  int noticesTime;

  Future<List<Ethereum.TokenBlog>> fetchTokenDataSource() async {
    final sourceProvider = Ethereum.DataSourceProvider.of(context);

    /// 本地有数据直接返回
    List<Ethereum.TokenBlog> favTokens =
        await sourceProvider.localService.favoriteTokens;
    if (favTokens.length > 0) {
      return favTokens;
    }

    favTokens =
        await sourceProvider.remoteServices.token.requestStandard().then(
              (rsp) => rsp.data,
            );

    /// 写入本地
    if (favTokens.length > 0) {
      favTokens.forEach(sourceProvider.localService.insertFavoriteToken);
    }

    return favTokens;
  }

  @override
  void initState() {
    super.initState();
    _getNoticesTtime();
    Fluttertoast.showToast(msg: "节点切换成功，请重启软件");
  }

  _getNoticesTtime() async {
    var httpClient = new HttpClient();

    var request = await httpClient.getUrl(
      Uri.parse("http://ho.radarlab.me/notice/getNewDate"),
    );

    var response = await request.close();
    if (response.statusCode == HttpStatus.ok) {
      var json = await response.transform(utf8.decoder).join();
      var data = jsonDecode(json);

      setState(() {
        this.noticesTime = data['data'];

        if (ConfigStorageUtils().readOfKey("hbnotices") != null) {
          this.compare = ConfigStorageUtils()
              .readOfKey("hbnotices")
              .compareTo((this.noticesTime));
        } else {
          this.compare = 0.compareTo((this.noticesTime));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ValueListenableBuilder<WalletKey>(
      valueListenable: CoreProvider.of(context).accountListenable,
      builder: (context, account, child) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: ThemeUtils().getColor(
                'utils.view_controller.background',
              ),
              elevation: 0.3,
              actions: [
                Container(
                  width: ScreenUtil().setWidth(50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          ConfigStorageUtils().writeOfKey("hbnotices",
                              defaultValue: this.noticesTime);
                          setState(() {
                            this.compare = 1;
                          });

                          openDapp(
                            context,
                            Dapp(
                              url: "http://notice.radarlab.me/#/?chain=ho",
                              logo: "",
                              name: "公告",
                            ),
                          );
                          // Fluttertoast.showToast(
                          //   msg: S.current
                          //       .message_box_prompt_password_not_yet_open,
                          // );
                        },
                        child: Icon(
                          Icons.notifications,
                          color: Colors.black,
                        ),
                      ),
                      compare == -1
                          ? Container(
                              margin: EdgeInsets.only(
                                right: ScreenUtil().setWidth(15),
                              ),
                              height: 6,
                              width: 6,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(180),
                                color: Colors.red,
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ],
              title: Row(
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      WalletSelector.show(context: context);
                    },
                    child: Container(
                      height: ScreenUtil().setHeight(30),
                      decoration: BoxDecoration(
                        color: Colors.blue[400],
                        borderRadius: BorderRadius.circular(
                          ScreenUtil().setHeight(30),
                        ),
                      ),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                left: ScreenUtil().setWidth(10),
                                right: ScreenUtil().setWidth(5)),
                            child: Text(
                              CoreProvider.of(context).networkConfig.chainName,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(16),
                              ),
                            ),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(22),
                            width: ScreenUtil().setWidth(22),
                            margin: EdgeInsets.only(
                                right: ScreenUtil().setWidth(5)),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(180),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/imgs/arrow_right.png',
                                  package: 'wallet_flutter',
                                  color: Colors.grey,
                                  height: ScreenUtil().setHeight(14),
                                  width: ScreenUtil().setWidth(14),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            backgroundColor: ThemeUtils().getColor(
              'page.background',
            ),
            body: CustomScrollView(
              physics: const AlwaysScrollableScrollPhysics(
                parent: BouncingScrollPhysics(),
              ),
              slivers: [
                CupertinoSliverRefreshControl(
                  onRefresh: () {
                    refreshListenable.value = DateTime.now();
                    return Future.delayed(Duration(milliseconds: 1000));
                  },
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (content, index) {
                      return Container(
                        child: ValueListenableBuilder<WalletKey>(
                          valueListenable:
                              CoreProvider.of(context).accountListenable,
                          builder: (context, key, child) => GestureDetector(
                            child: Column(
                              children: [
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                      right: ScreenUtil().setWidth(20),
                                      left: ScreenUtil().setWidth(20),
                                      top: ScreenUtil().setHeight(15),
                                      bottom: ScreenUtil().setHeight(8),
                                    ),
                                    padding: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(10),
                                      left: ScreenUtil().setHeight(10),
                                    ),
                                    height: ScreenUtil().setHeight(120),
                                    width: ScreenUtil().setWidth(375),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                        colors: [
                                          Colors.blue,
                                          Colors.green[300],
                                        ],
                                      ),
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Image.asset(
                                                        key.avatarPath,
                                                        package:
                                                            'wallet_flutter',
                                                        width: ScreenUtil()
                                                            .setWidth(29),
                                                        height: ScreenUtil()
                                                            .setWidth(29),
                                                      ),
                                                      SizedBox(
                                                          width: ScreenUtil()
                                                              .setWidth(7)),
                                                      Text(
                                                        key.nickName ??
                                                            'NoName',
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: ThemeUtils()
                                                            .getTextStyle(
                                                                'views.chaincore.ethereum.assets.textstyle.wallet_name'),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                      height: ScreenUtil()
                                                          .setHeight(16)),
                                                  InkWell(
                                                    splashColor:
                                                        Colors.transparent,
                                                    highlightColor:
                                                        Colors.transparent,
                                                    onTap: () async {
                                                      Clipboard.setData(
                                                        ClipboardData(
                                                          text: key.address,
                                                        ),
                                                      );

                                                      Fluttertoast.showToast(
                                                        msg: S.current
                                                            .copy_success,
                                                      );
                                                    },
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                          key.addressBlog,
                                                          style: ThemeUtils()
                                                              .getTextStyle(
                                                                  'views.chaincore.ethereum.assets.textstyle.address'),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              left: ScreenUtil()
                                                                  .setWidth(
                                                                      12)),
                                                          height: ScreenUtil()
                                                              .setHeight(11),
                                                          width: ScreenUtil()
                                                              .setWidth(12),
                                                          child: Image.asset(
                                                            'assets/imgs/wallet_copy_address.png',
                                                            package:
                                                                'wallet_flutter',
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            // Text("188,888,991.00"),
                                          ],
                                        ),
                                        Expanded(
                                          child: SizedBox(),
                                        ),
                                        Column(
                                          children: [
                                            InkWell(
                                              splashColor: Colors.transparent,
                                              highlightColor:
                                                  Colors.transparent,
                                              onTap: () {
                                                Navigator.of(context).push(
                                                  ViewAnimations.viewRightIn(
                                                    KeyInfoView(
                                                      walletKey: key,
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                child: Icon(
                                                  Icons.more_horiz,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(),
                                              margin: EdgeInsets.only(
                                                top: ScreenUtil().setHeight(30),
                                              ),
                                              child: IconButton(
                                                padding: EdgeInsets.zero,
                                                icon: Icon(
                                                  const IconData(
                                                    0xe600,
                                                    fontFamily: "myIcon",
                                                    fontPackage:
                                                        "wallet_flutter",
                                                  ),
                                                  size: ScreenUtil()
                                                      .setHeight(24),
                                                  color: ThemeUtils().getColor(
                                                    'views.chaincore.ethereum.assets.header_icon_color',
                                                  ),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context).push(
                                                    ViewAnimations.viewRightIn(
                                                      QrCodePage(account: key),
                                                    ),
                                                  );
                                                },
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Container(
                                      color: ThemeUtils().getColor(
                                        'page.background',
                                      ),
                                      height: ScreenUtil().setHeight(47),

                                      padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(35),
                                        bottom: ScreenUtil().setHeight(10),
                                      ),
                                      width: ScreenUtil().setWidth(115),
                                      // padding: EdgeInsets.only(
                                      //   top: ScreenUtil().setWidth(18),
                                      //   left: ScreenUtil().setWidth(35),
                                      // ),
                                      alignment: Alignment.bottomLeft,
                                      child: Text(
                                        S.current.ethereum_home_baritem_title,
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.bottomRight,
                                      height: ScreenUtil().setHeight(47),
                                      width: ScreenUtil().setWidth(260),
                                      padding: EdgeInsets.only(
                                        right: ScreenUtil().setWidth(20),
                                        bottom: ScreenUtil().setHeight(10),
                                      ),
                                      child: InkWell(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        onTap: () async {
                                          await Navigator.of(context).push(
                                            ViewAnimations.viewRightIn(
                                              AssetManagementPage(
                                                dataSourceProvider:
                                                    Ethereum.DataSourceProvider
                                                        .of(context),
                                                web3clientProvider:
                                                    Web3ClientProvider.of(
                                                        context),
                                              ),
                                            ),
                                          );
                                          // setState(() {});
                                        },
                                        child: Icon(
                                          const IconData(
                                            0xe623,
                                            fontFamily: "myIcon",
                                            fontPackage: "wallet_flutter",
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  child: FutureBuilder<
                                          List<Ethereum.TokenBlog>>(
                                      future: fetchTokenDataSource(),
                                      builder: (context, snapshot) => snapshot
                                                  .connectionState !=
                                              ConnectionState.done
                                          // ? Container()
                                          ? Container(
                                              child: Center(
                                                child:
                                                    LineSpinFadeLoaderIndicator(
                                                  ballColor: Colors.grey,
                                                ),
                                              ),
                                            )
                                          : Container(
                                              height: snapshot.data.length *
                                                  ScreenUtil().setHeight(80),
                                              child: CustomScrollView(
                                                physics:
                                                    new NeverScrollableScrollPhysics(),
                                                // physics: const AlwaysScrollableScrollPhysics(
                                                //   parent: BouncingScrollPhysics(),
                                                // ),
                                                slivers: [
                                                  CupertinoSliverRefreshControl(
                                                    onRefresh: () {
                                                      refreshListenable.value =
                                                          DateTime.now();
                                                      return Future.delayed(
                                                        Duration(
                                                            milliseconds: 2000),
                                                      );
                                                    },
                                                  ),
                                                  SliverList(
                                                    delegate:
                                                        SliverChildBuilderDelegate(
                                                      (BuildContext context,
                                                              int index) =>
                                                          AssetCell(
                                                        index,
                                                        snapshot.data.length,
                                                        snapshot.data[index],
                                                        account,
                                                        Ethereum.DataSourceProvider
                                                            .of(context),
                                                        CoreProvider.of(context)
                                                            .networkConfig,
                                                        refreshListenable:
                                                            refreshListenable,
                                                      ),
                                                      childCount:
                                                          snapshot.data.length,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    childCount: 1,
                  ),
                )
              ],
            )
            // ListView(
            //   physics: const AlwaysScrollableScrollPhysics(
            //     parent: BouncingScrollPhysics(),
            //   ),
            //   children: [],
            // ),
            );
      },
    );
  }

  void openDapp(BuildContext context, Ethereum.Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: Web3ClientProvider.of(context),
          dataSourceProvider: Ethereum.DataSourceProvider.of(context),
        ),
      ),
    );
  }
}
