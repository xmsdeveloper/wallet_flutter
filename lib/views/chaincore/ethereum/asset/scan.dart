// @dart=2.9
part of '../asset.dart';
// import 'package:images_picker/images_picker.dart';

class ScanPage extends StatefulWidget {
  bool isFor;
  ScanPage({this.isFor});
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  ScanController controller = ScanController();
  String qrcode = 'Unknown';

  //判断闪光灯开启图标
  bool flash = false;

  @override
  void initState() {
    super.initState();
    // 进入页面统计（手动采集时才可设置）
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false, //输入框抵住键盘
        // appBar: AppBar(
        //   title: const Text('Plugin example app'),
        // ),
        body: OrientationBuilder(builder: (context, orientation) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                children: [
                  // Text('Running on: $_platformVersion\n'),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ScanView(
                      controller: controller,

                      // scanLineColor: Colors.green.shade400,
                      onCapture: (data) {
                        setState(
                          () {
                            this.qrcode = data;
                            print(qrcode);
                            if (this.widget.isFor == true) {
                              print(this.widget.isFor);
                              Navigator.pop(context, this.qrcode);
                              Navigator.of(context).push(
                                ViewAnimations.viewRightIn(
                                  AddressSelector(
                                    localKeys: [],
                                    recentAccounts: [],
                                    qrcode: this.qrcode,
                                  ),
                                ),
                              );
                            } else {
                              Navigator.pop(context, this.qrcode);
                            }

                            // Navigator.of(context).push(
                            //   ViewAnimations.viewRightIn(
                            //     AddressSelector(
                            //       localKeys: [],
                            //       recentAccounts: [],
                            //     ),
                            //   ),
                            // );
                          },
                        );
                      },
                    ),
                  ),
                  // Text('scan result is $qrcode'),

                  Positioned(
                    top: MediaQuery.of(context).size.width <
                            MediaQuery.of(context).size.height * 1.2
                        ? ScreenUtil().setHeight(580)
                        : MediaQuery.of(context).size.height -
                            ScreenUtil().setHeight(40),
                    // left: ScreenUtil().setWidth(100),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Text(
                        S.current.ethereum_home_scan_title,
                        textAlign: TextAlign.center,
                        style: ThemeUtils().getTextStyle(
                          'views.chaincore.ethereum.updata.textstyle.buttom_text',
                        ),
                      ),
                    ),
                  ),
                  MediaQuery.of(context).size.width <
                          MediaQuery.of(context).size.height * 1.2
                      ? Positioned(
                          width: MediaQuery.of(context).size.width,
                          top: ScreenUtil().setHeight(620),
                          child: IconButton(
                            color: ThemeUtils().getColor(
                              'views.chaincore.ethereum.assets.header_icon_color',
                            ),
                            onPressed: () {
                              controller.toggleTorchMode();
                              setState(() {
                                this.flash = !flash;
                              });
                            },
                            icon: Icon(
                              this.flash == false
                                  ? Icons.flash_off
                                  : Icons.flash_on,
                              size: ScreenUtil().setHeight(24),
                            ),
                          ),
                        )
                      : Positioned(
                          width: MediaQuery.of(context).size.width,
                          // top: MediaQuery.of(context).size.height / 2,
                          // right: ScreenUtil().setWidth(40),
                          child: IconButton(
                            color: ThemeUtils().getColor(
                              'views.chaincore.ethereum.assets.header_icon_color',
                            ),
                            onPressed: () {
                              controller.toggleTorchMode();
                              setState(() {
                                this.flash = !flash;
                              });
                            },
                            icon: Icon(
                              this.flash == false
                                  ? Icons.flash_off
                                  : Icons.flash_on,
                              size: ScreenUtil().setHeight(24),
                            ),
                          ),
                        ),

                  Positioned(
                    top: ScreenUtil().setHeight(60),
                    left: ScreenUtil().setWidth(30),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        child: Icon(
                          Icons.close,
                          color: ThemeUtils().getColor(
                            'views.chaincore.ethereum.assets.header_icon_color',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        }));
  }
}
