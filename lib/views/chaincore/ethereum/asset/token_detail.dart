// @dart=2.9
part of '../asset.dart';

class TokenDetailView extends StatefulWidget {
  final Ethereum.TokenBlog tokenBlog;

  final WalletKey account;
  final Web3ClientProvider web3Client;
  final EndPoint endPoint;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final BigInt lastBlockHeight;
  final Color color;

  TokenDetailView({
    @required this.tokenBlog,
    @required this.account,
    @required this.web3Client,
    @required this.endPoint,
    @required this.dataSourceProvider,
    @required this.networkConfig,
    @required this.lastBlockHeight,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _TokenDetailViewState();
}

class _TokenDetailViewState extends State<TokenDetailView>
    with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  static const int PageSize = 20;

  /// 交易记录筛选控制
  TabController tabController;
  ValueNotifier<FilterType> fliterListenable =
      ValueNotifier<FilterType>(FilterType.All);

  Ethereum.LogsService logsService;
  Ethereum.ScanAPIService scanAPIService;

  ValueNotifier<List<Ethereum.Log>> dataSourceListenable =
      ValueNotifier<List<Ethereum.Log>>(null);

  List<Ethereum.Log> _flitedDataSource = [];

  int count = 0;

  // ignore: non_constant_identifier_names
  final Map<FilterType, String> FilterTypeNames = {
    FilterType.All: S.current.ethereum_home_detail_txfilter_all,
    FilterType.In: S.current.ethereum_home_detail_txfilter_in,
    FilterType.Out: S.current.ethereum_home_detail_txfilter_out,
    FilterType.Other: S.current.ethereum_home_detail_txfilter_call,
  };
  BigInt height;
  getLastBlockHight() async {
    height = await Web3ClientProvider.fromHttp(
            url: CoreProvider.of(context).endPoint.nodeURL)
        .getBlockNumber();
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      initialIndex: 0,
      length: FilterType.values.length,
      vsync: this,
    );

    scanAPIService = Ethereum.ScanAPIService(
      httpClient: http.Client(),
      hostURL: widget.endPoint.scanURL,
      apiKey: widget.endPoint.scanAPIKey,
      networkName: CoreProvider.of(context).networkConfig.chainName,
    );

    logsService = Ethereum.LogsService(
      emitContractAddress: widget.tokenBlog.address,
      fromAddress: widget.account.address,
      scanAPIService: scanAPIService,
      lastKblockHeight: widget.lastBlockHeight,
      networkName: CoreProvider.of(context).networkConfig.chainName,
    );

    /// 数据逻辑
    /// 1.先使用数据库中的数据显示50条记录
    /// 2.立即开始请求最新的数据，在请求完毕后，service会自动写入数据库。
    /// 3.写入完毕后重新获取记录
    logsService.selectLogs(0, PageSize).then((logs) {
      dataSourceListenable.value = logs;
    });

    if (CoreProvider.of(context).networkConfig.chainName == "HECO") {
      _onRefreshAll();
    }

    /// 上拉追加
    scrollController.addListener(
      () async {
        if (scrollController.position.pixels !=
            scrollController.position.maxScrollExtent) {
          return;
        }

        if (dataSourceListenable.value == null ||
            dataSourceListenable.value.length <= 0) {
          return;
        }

        /// 1.先展示完所有本地数据库的数据,若数据库已没有数据则使用网络载入
        var pageTxs = await logsService.selectLogs(
          dataSourceListenable.value.length,
          PageSize,
        );

        if (pageTxs != null && pageTxs.length > 0) {
          dataSourceListenable.value.addAll(pageTxs);
          setState(() {});
        }

        /// 数据库记录已显示完毕
        if (pageTxs.length < PageSize) {
          if (CoreProvider.of(context).networkConfig.chainName != "HECO") {
            _more();
          }
        }
      },
    );

    /// 在build完成后调用滚动到指定区域,模拟一次下拉刷新
    SchedulerBinding.instance.addPostFrameCallback(
      (_) async {
        scrollController.animateTo(
          -81.0,
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      },
    );
  }

  Future<void> _more() async {
    await getLastBlockHight();
    print(widget.tokenBlog.protocol);
    var newLogs = <Ethereum.Log>[];
    if (widget.tokenBlog.protocol == null ||
        widget.tokenBlog.protocol == '20') {
      newLogs = await logsService.more(
        lastBlockHeight2: this.height,
        topic_1: paddingToEventTopic(widget.account.address),
        topic_2: paddingToEventTopic(widget.account.address),
        topicOpr12: 'or',
      );
    } else if (widget.tokenBlog.protocol == '777') {
      newLogs = await logsService.more(
        lastBlockHeight2: this.height,
        // topic_2: paddingToEventTopic(widget.account.address),
        // topic_3: paddingToEventTopic(widget.account.address),
        // topicOpr23: 'or',
        topic_1: paddingToEventTopic(widget.account.address),
        topic_2: paddingToEventTopic(widget.account.address),
        topicOpr12: 'or',
      );
    }

    if (newLogs != null && newLogs.length > 0) {
      dataSourceListenable.value.addAll(newLogs);
      setState(() {});
    }
  }

  Future<void> _onRefreshAll() async {
    await getLastBlockHight();
    await Future.delayed(Duration(seconds: 1));

    var newLogs = [];
    widget.tokenBlog.protocol = '777';

    if (widget.tokenBlog.protocol == null ||
        widget.tokenBlog.protocol == '20') {
      newLogs = await logsService.refreshAll(
        lastBlockHeight2: this.height,
        topic_1: paddingToEventTopic(widget.account.address),
        topic_2: paddingToEventTopic(widget.account.address),
        topicOpr12: 'or',
      );
    } else if (widget.tokenBlog.protocol == '777') {
      newLogs = await logsService.refreshAll(
        lastBlockHeight2: this.height,
        // topic_2: paddingToEventTopic(widget.account.address),
        // topic_3: paddingToEventTopic(widget.account.address),
        // topicOpr23: 'or',
        topic_1: paddingToEventTopic(widget.account.address),
        topic_2: paddingToEventTopic(widget.account.address),
        topicOpr12: 'or',
      );
    }

    if (newLogs != null && newLogs.length > 0) {
      dataSourceListenable.value = await logsService.selectLogs(
        0,
        PageSize,
      );
    }
  }

  Future<void> _onRefresh() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.

    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.

    } else if (connectivityResult == ConnectivityResult.none) {
      return Fluttertoast.showToast(
        msg: S.current.network_error,
      );
    }
    await getLastBlockHight();
    await Future.delayed(Duration(seconds: 1));

    var newLogs = [];
    widget.tokenBlog.protocol = '777';

    if (widget.tokenBlog.protocol == null ||
        widget.tokenBlog.protocol == '20') {
      newLogs = await logsService.refresh(
        lastBlockHeight2: this.height,
        topic_1: paddingToEventTopic(widget.account.address),
        topic_2: paddingToEventTopic(widget.account.address),
        topicOpr12: 'or',
      );
    } else if (widget.tokenBlog.protocol == '777') {
      newLogs = await logsService.refresh(
        lastBlockHeight2: this.height,
        // topic_2: paddingToEventTopic(widget.account.address),
        // topic_3: paddingToEventTopic(widget.account.address),
        // topicOpr23: 'or',
        topic_1: paddingToEventTopic(widget.account.address),
        topic_2: paddingToEventTopic(widget.account.address),
        topicOpr12: 'or',
      );
    }

    if (newLogs != null && newLogs.length > 0) {
      dataSourceListenable.value = await logsService.selectLogs(
        0,
        PageSize,
      );
    }
  }

  Future checkPendingTransaction(String txHash) async {
    if (txHash == null) {
      return;
    }

    print('checkPendingTransaction:$txHash');
    final receipt = widget.web3Client.getTransactionReceipt(txHash);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Color.fromRGBO(250, 250, 250, 1),
        body: CustomScrollView(
          controller: scrollController,
          physics: const AlwaysScrollableScrollPhysics(
            parent: BouncingScrollPhysics(),
          ),
          slivers: [
            _DetailViewHeader(
              tokenBlog: widget.tokenBlog,
              account: widget.account,
              web3Client: widget.web3Client,
              endPoint: widget.endPoint,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
              color: widget.color,
            ),

            /// SmarRefresher
            CupertinoSliverRefreshControl(
              refreshTriggerPullDistance: 80,
              onRefresh: _onRefresh,
            ),

            SliverToBoxAdapter(
              child: Container(
                decoration: BoxDecoration(),
                margin: EdgeInsets.only(
                  left: ScreenUtil().setWidth(20),
                ),
                child: TabBar(
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorColor: Colors.blue,
                  controller: tabController,
                  indicatorWeight: 2,
                  isScrollable: true,
                  tabs: FilterTypeNames.values
                      .map(
                        (e) => Container(
                          alignment: Alignment.center,
                          height: ScreenUtil().setHeight(40),
                          child: Text(
                            e,
                            style: ThemeUtils().getTextStyle(
                              'views.chaincore.ethereum.assets_detail.textstyle.cell_tab_title',
                            ),
                          ),
                        ),
                      )
                      .toList(),
                  onTap: (index) {
                    fliterListenable.value = FilterType.values[index];
                  },
                ),
              ),
            ),

            ValueListenableBuilder<List<Ethereum.Log>>(
              valueListenable: dataSourceListenable,
              builder: (context, logs, child) =>
                  ValueListenableBuilder<FilterType>(
                valueListenable: fliterListenable,
                builder: (context, ftype, child) {
                  final cellAdapter = _HistoryCellBuilderAdapter(
                    adapters: [
                      _EventTransferAdapterBuilder(),
                      _EventSentAdapterBuilder(),
                    ],
                  );

                  if (logs != null && logs.length > 0) {
                    _flitedDataSource = cellAdapter.filterByType(
                      logs,
                      ftype,
                      widget.account.address,
                    );

                    // print(dataSourceListenable.value.length);
                  } else {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) =>
                            //
                            Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(97),
                              ),
                              width: ScreenUtil().setWidth(57),
                              height: ScreenUtil().setHeight(56),
                              child:
                                  Image.asset("assets/images/asset_nodata.png"),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(9),
                              ),
                              child: Text(
                                S.current.ethereum_trade_transactions,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(12),
                                  color: Color.fromRGBO(216, 216, 216, 1),
                                ),
                              ),
                            ),
                          ],
                        ),
                        childCount: 1,
                      ),
                    );
                  }

                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        return cellAdapter.build(
                          context: context,
                          currentAddress: widget.account.address,
                          txOrLog: _flitedDataSource[index],
                          tokenBlog: widget.tokenBlog,
                          index: index,
                        );
                      },
                      childCount: _flitedDataSource == null
                          ? 0
                          : _flitedDataSource.length,
                    ),
                  );
                },
              ),
            ),
            CoreProvider.of(context).networkConfig.chainName == "HECO"
                ? SliverToBoxAdapter(
                    child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          openDapp(
                            context,
                            Dapp(
                              url: "https://hecoinfo.com/" +
                                  "en-us/"
                                      "address/" +
                                  widget.account.address +
                                  "?tab=HRC-20+Token+Txns",
                              logo: "",
                            ),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(10),
                            bottom: ScreenUtil().setHeight(30),
                          ),
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(10),
                            right: ScreenUtil().setWidth(10),
                            top: ScreenUtil().setHeight(5),
                            bottom: ScreenUtil().setHeight(5),
                          ),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                          ),
                          child: Text(
                            S.current.ethereum_trade_history_more,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(12),
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ))
                : SliverToBoxAdapter(),
          ],
        ),
      );

  void openDapp(BuildContext context, Dapp dapp) {
    Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: null,
          dataSourceProvider: null,
          isTips: false,
        ),
      ),
    );
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }
}
