// @dart=2.9
part of '../asset.dart';

class TokenInfomationPage extends StatefulWidget {
  final Ethereum.TokenBlog tokenBlog;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final String mainAdress;

  TokenInfomationPage({
    @required this.tokenBlog,
    @required this.dataSourceProvider,
    @required this.mainAdress,
  });

  @override
  State<StatefulWidget> createState() => _TokenInfomationPageState();
}

class _TokenInfomationPageState extends State<TokenInfomationPage> {
  List datasource = [];

  Ethereum.ResponseList<Ethereum.TokenDetail> responseDetail;
  Ethereum.TokenDetail detail;
  var data = {};

  @override
  void initState() {
    super.initState();
    datasource = [
      {
        "title": S.current.ethereum_token_information_asset_name,
        "submit": widget.tokenBlog.name,
      },
      {
        "title": S.current.ethereum_token_information_asset_url,
        "submit": "",
      },
      {
        "title": S.current.ethereum_token_information_contract_address,
        "submit": widget.tokenBlog.address.length > 0
            ? widget.tokenBlog.address
                .replaceRange(9, widget.tokenBlog.address.length - 9, '...')
            : ""
      },
      {
        "title": S.current.ethereum_token_information_total_issue,
        "submit": "",
      },
      {
        "title": S.current.ethereum_token_information_accuracy,
        "submit": widget.tokenBlog.decimals.toString(),
      },
    ];
    // widget.dataSourceProvider.remoteServices.token
    //     // .requestDetail(address: widget.tokenBlog.address)
    //     .requestDetail(
    //         address: this.widget.tokenBlog.address == null
    //             ? ""
    //             : this.widget.tokenBlog.address)
    //     .then(
    //   (response) {
    //     setState(() {
    //       responseDetail = response;

    //       detail = responseDetail.data.first;

    //       datasource = [
    //         {
    //           "title": S.current.ethereum_token_information_asset_name,
    //           "submit": detail.name,
    //         },
    //         {
    //           "title": S.current.ethereum_token_information_asset_url,
    //           "submit": detail.website,
    //         },
    //         {
    //           "title": S.current.ethereum_token_information_contract_address,
    //           "submit": detail.address.length > 0
    //               ? detail.address
    //                   .replaceRange(9, detail.address.length - 9, '...')
    //               : ""
    //         },
    //         {
    //           "title": S.current.ethereum_token_information_accuracy,
    //           "submit": detail.decimals.toString(),
    //         },
    //         {
    //           "title": S.current.ethereum_token_information_start_time,
    //           "submit": detail.publishedOn,
    //         },
    //       ];
    //     });
    //   },
    // );

    _getData();
  }

  _getData() async {
    var httpClient = new HttpClient();
    // ignore: close_sinks
    var request = await httpClient.getUrl(Uri.parse(
        CoreProvider.of(context).endPoint.resourcesServicesURL +
            "/" +
            this.widget.tokenBlog.address +
            "/info.json"));

    var response = await request.close();

    if (response.statusCode == 200) {
      var json = await response.transform(utf8.decoder).join();

      data = jsonDecode(json);
      datasource = [
        {
          "title": S.current.ethereum_token_information_asset_name,
          "submit": data['name']
        },
        {
          "title": S.current.ethereum_token_information_asset_url,
          "submit": data['website']
        },
        {
          "title": S.current.ethereum_token_information_contract_address,
          "submit": widget.tokenBlog.address.length > 0
              ? widget.tokenBlog.address
                  .replaceRange(9, widget.tokenBlog.address.length - 9, '...')
              : ""
        },
        {
          "title": S.current.ethereum_token_information_total_issue,
          "submit": ""
        },
        {
          "title": S.current.ethereum_token_information_accuracy,
          "submit": data['decimals'].toString()
        },
      ];

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_trade_head,
        elevation: 0.5,
      ),
      backgroundColor: Colors.white,
      body: () {
        if (datasource.length == 0) {
          return Container();
        }

        return ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(
                    top: ScreenUtil().setHeight(37),
                    bottom: ScreenUtil().setHeight(23),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          bottom: ScreenUtil().setHeight(7),
                        ),
                        child: CachedImage.assetNetwork(
                          // url: this.widget.tokenBlog.logo,
                          url: this.widget.tokenBlog.logo ??
                              CoreProvider.of(context)
                                      .endPoint
                                      .resourcesServicesURL +
                                  '/' +
                                  this.widget.tokenBlog.address +
                                  "/logo.png",

                          placeholder: Image.asset(
                            'assets/imgs/default_token.png',
                            package: 'wallet_flutter',
                          ),
                          width: ScreenUtil().setWidth(76),
                          height: ScreenUtil().setWidth(76),
                          imageCacheHeight: 100,
                          imageCacheWidth: 100,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(6),
                        ),
                        child: Text(
                          this.widget.tokenBlog.symbol,
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.tokenInformation.textstyle.token_name',
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(4),
                        ),
                        child: Text(
                          this.widget.tokenBlog.name,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(12),
                            color: Color.fromRGBO(112, 112, 112, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: ScreenUtil().setHeight(10),
                  color: Color.fromRGBO(246, 246, 246, 1),
                ),
                Column(
                  children: this
                      .datasource
                      .map(
                        (e) => Container(
                          height: ScreenUtil().setHeight(55),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: ScreenUtil().setWidth(0.5),
                                color: ThemeUtils().getColor(
                                  'views.chaincore.ethereum.tokenInformation.slid_color',
                                ),
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(13),
                              right: ScreenUtil().setWidth(13)),
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(11),
                            right: ScreenUtil().setWidth(11),
                            top: ScreenUtil().setHeight(9),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  e['title'],
                                  maxLines: 1,
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.tokenInformation.textstyle.general_title',
                                  ),
                                ),
                              ),
                              Align(
                                // alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    InkWell(
                                      onTap: e['title'] == "合约地址" ||
                                              e['title'] == "Contract address"
                                          ? () {
                                              Clipboard.setData(
                                                ClipboardData(
                                                  text:
                                                      widget.tokenBlog.address,
                                                ),
                                              );

                                              Fluttertoast.showToast(
                                                msg: S.current.copy_success,
                                              );
                                            }
                                          : null,
                                      child: Container(
                                        // width: ScreenUtil().setWidth(306),
                                        child: Text(
                                          e['submit'],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: e['title'] != "URL"
                                              ? ThemeUtils().getTextStyle(
                                                  'views.chaincore.ethereum.tokenInformation.textstyle.content',
                                                )
                                              : ThemeUtils().getTextStyle(
                                                  'views.chaincore.ethereum.tokenInformation.textstyle.url',
                                                ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: ScreenUtil().setHeight(10),
                  color: Color.fromRGBO(246, 246, 246, 1),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(24),
                    top: ScreenUtil().setWidth(19),
                  ),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    S.current.ethereum_token_information_introduce,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(24),
                    right: ScreenUtil().setWidth(24),
                    top: ScreenUtil().setWidth(19),
                  ),
                  alignment: Alignment.centerLeft,
                  child: Text(data['description'] ?? "",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(112, 112, 112, 1),
                      )),
                )
              ],
            ),
          ],
        );
      }(),
    );
  }
}
