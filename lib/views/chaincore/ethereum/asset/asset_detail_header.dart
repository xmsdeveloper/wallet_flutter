// @dart=2.9
part of '../asset.dart';

class _DetailViewHeader extends StatefulWidget {
  final Ethereum.TokenBlog tokenBlog;

  final WalletKey account;
  final Web3ClientProvider web3Client;
  final EndPoint endPoint;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final ValueNotifier<DateTime> refreshListenable;
  final Color color;
  _DetailViewHeader({
    @required this.tokenBlog,
    @required this.account,
    @required this.web3Client,
    @required this.endPoint,
    @required this.dataSourceProvider,
    @required this.networkConfig,
    this.refreshListenable,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _DetailViewHeaderState();
}

class _DetailViewHeaderState extends State<_DetailViewHeader> {
  Ethereum.ScanAPIService scanAPIService;

  bool isBlack;

  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(null);

  @override
  void initState() {
    super.initState();
    scanAPIService = Ethereum.ScanAPIService(
      httpClient: http.Client(),
      hostURL: widget.endPoint.scanURL,
      apiKey: widget.endPoint.scanAPIKey,
    );
    fetchBalance();
  }

  void fetchBalance() {
    widget.tokenBlog != null
        ? widget.dataSourceProvider.localService
            .getCachedBalance(widget.tokenBlog, widget.account.address)
            .then((cacheBalance) {
            if (cacheBalance != null) {
              this.balanceListenable.value = Amount(
                value: cacheBalance.balance,
                decimals: widget.tokenBlog.decimals,
              );
            }

            if (cacheBalance == null ||
                DateTime.now().difference(cacheBalance.updateTime) >
                    Duration(seconds: 30)) {
              Utils.futureGetBalance(
                // Web3ClientProvider.of(context),
                widget.web3Client,
                widget.tokenBlog,
                widget.account.address,
              ).then((balance) {
                this.balanceListenable.value = balance;

                widget.dataSourceProvider.localService.putCachedBalance(
                  widget.tokenBlog,
                  widget.account.address,
                  balance.value,
                );
              });
            }
          })
        : widget.web3Client.getBalance(widget.account.address).then(
              (value) => this.balanceListenable.value = Amount(
                  value: value,
                  decimals: CoreProvider().networkConfig.mainDecimals),
            );
  }

  @override
  Widget build(BuildContext context) => SliverAppBar(
      backgroundColor: Color.fromRGBO(250, 250, 250, 1),
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: [
        CoreProvider.of(context).networkConfig.source != "ETH-customize"
            ? Container(
                margin: EdgeInsets.only(
                  right: ScreenUtil().setWidth(22),
                ),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          ViewAnimations.viewRightIn(
                            TokenInfomationPage(
                              tokenBlog: this.widget.tokenBlog,
                              mainAdress: widget.account.address,
                              dataSourceProvider:
                                  this.widget.dataSourceProvider,
                            ),
                          ),
                        );
                      },
                      child: Container(
                        child: Text(
                          S.current.ethereum_trade_head + "  >",
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            : Container()
      ],
      key: PageStorageKey<String>('TopSliverAppBar'),
      centerTitle: true,
      elevation: 0,
      floating: false,
      // 标题栏是否固定
      pinned: false,
      expandedHeight: ScreenUtil().setHeight(262),
      // expandedHeight: ScreenUtil().setHeight(172),
      iconTheme: IconThemeData(
        color: ThemeUtils().getColor(
          'views.chaincore.ethereum.assets_detail.header_icon',
        ),
      ),
      title: Text(
        widget.tokenBlog != null
            ? widget.tokenBlog.symbol
            : CoreProvider().networkConfig.mainSymbol,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(17),
          color: Colors.white,
        ),
      ),
      flexibleSpace: Stack(
        children: [
          Container(
            height: ScreenUtil().setHeight(292),
            // height: ScreenUtil().setHeight(172),
            decoration: BoxDecoration(color: Color.fromRGBO(62, 149, 252, 1)),
            child: FlexibleSpaceBar(
              // stretchModes: [
              //   StretchMode.fadeTitle,
              // ],
              collapseMode: CollapseMode.pin,
              background: Container(
                // color: Colors.blue,
                child: Column(
                  children: [
                    SizedBox(height: ScreenUtil().setHeight(107)),
                    // SizedBox(height: ScreenUtil().setHeight(10)),

                    /// Balance

                    widget.tokenBlog != null
                        ? FutureBuilder<Amount>(
                            future: Utils.futureGetBalance(
                              widget.web3Client,
                              widget.tokenBlog,
                              widget.account.address,
                            ),
                            builder: (contxt, snapshot) => Container(
                              // height: ScreenUtil().setHeight(30),
                              child: snapshot.connectionState !=
                                      ConnectionState.done
                                  ? BallSpinFadeLoaderIndicator(
                                      minBallRadius: ScreenUtil().setHeight(1),
                                      maxBallRadius: ScreenUtil().setHeight(3),
                                      radius: ScreenUtil().setHeight(13),
                                      ballColor: Colors.black,
                                    )
                                  : Text(
                                      /// 部分代币的精度不足8，需要处理
                                      snapshot.data != null
                                          ? snapshot.data.toStringAsFixed(
                                              fractionDigits:
                                                  snapshot.data.decimals > 8
                                                      ? 8
                                                      : snapshot.data.decimals,
                                            )
                                          : "0",
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(30),
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                            ),
                          )
                        : ValueListenableBuilder(
                            valueListenable: widget.refreshListenable,
                            builder: (context, _, child) {
                              fetchBalance();
                              return ValueListenableBuilder(
                                valueListenable: balanceListenable,
                                builder: (context, balance, child) =>
                                    balance == null ? Container() : Text(
                                        // balanceListenable.value.decimals != 0
                                        //     ? balance.toStringAsFixed()
                                        //     : "0",
                                        // // balance.toStringAsFixed(),
                                        // style: TextStyle(
                                        //   fontSize: ScreenUtil().setSp(30),
                                        //   color: Colors.white,
                                        //   fontWeight: FontWeight.bold,
                                        // ),
                                        ''),
                              );
                            },
                          ),

                    /// Address
                    SizedBox(height: ScreenUtil().setHeight(8)),
                    Text(
                      S.current.ethereum_trade_available_balance,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14),
                        color: Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),

                    /// Actions
                    // SizedBox(height: ScreenUtil().setWidth(10)),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Color.fromRGBO(250, 250, 250, 1),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(250, 250, 250, 1),
                      offset: Offset(0.0, 1.0), //阴影xy轴偏移量
                      blurRadius: 0.0, //阴影模糊程度
                      spreadRadius: 0.0 //阴影扩散程度
                      )
                ],
              ),
              height: ScreenUtil().setHeight(44),
              width: MediaQuery.of(context).size.width,
            ),
          ),
          Positioned(
            bottom: 1,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      // color: Colors.black12,
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0.0, 0.5), //阴影xy轴偏移量
                      blurRadius: 1.0, //阴影模糊程度
                      spreadRadius: 0.0 //阴影扩散程度
                      )
                ],
              ),

              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(10),
                left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(20),
              ),

              // width: ScreenUtil().setWidth(335),
              width:
                  MediaQuery.of(context).size.width - ScreenUtil().setWidth(40),
              height: ScreenUtil().setHeight(88),
              child: Row(
                children: [
                  Container(
                    width: (MediaQuery.of(context).size.width / 2) -
                        ScreenUtil().setWidth(40),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          ScreenUtil().setHeight(44 / 2),
                        ),
                      ),
                      child: InkWell(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/imgs/collage.png',
                              height: ScreenUtil().setHeight(30),
                              package: "wallet_flutter",
                            ),
                            SizedBox(height: ScreenUtil().setHeight(10)),
                            Text(
                              S.current.ethereum_home_detail_collection,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            )
                          ],
                        ),
                        // style: ButtonStyle(
                        //   backgroundColor: MaterialStateColor.resolveWith(
                        //     (states) => Colors.white,
                        //     // ThemeUtils().getColor(
                        //     //   'views.chaincore.ethereum.assets_detail.header_button_2',
                        //     // ),
                        //   ),
                        // ),
                        onTap: () {
                          CoreProvider.of(context).account.privateKeyRaw != null
                              ? Navigator.of(context).push(
                                  ViewAnimations.viewRightIn(
                                    widget.tokenBlog != null
                                        ? QrCodePage(
                                            tokenBlog: widget.tokenBlog,
                                            account: widget.account,
                                            symbol: widget.tokenBlog.symbol,
                                            chainname: CoreProvider.of(context)
                                                .networkConfig
                                                .chainName,
                                            color: widget.color,
                                          )
                                        : QrCodePage(
                                            account: widget.account,
                                            chainname: CoreProvider.of(context)
                                                .networkConfig
                                                .chainName,
                                            symbol: "",
                                            color: widget.color,
                                          ),
                                  ),
                                )
                              : Fluttertoast.showToast(
                                  msg: S.current.wallet_watch_name,
                                );
                        },
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                    height: ScreenUtil().setHeight(50),
                    width: 0.5,
                    color: Color.fromRGBO(226, 226, 226, 1),
                  ),
                  Expanded(child: SizedBox()),
                  Container(
                    // height: ScreenUtil().setHeight(44),
                    width: (MediaQuery.of(context).size.width / 2) -
                        ScreenUtil().setWidth(40),
                    child: ClipRRect(
                        child: InkWell(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                // Icon(
                                //   Icons.send_rounded,
                                //   size:
                                //       ScreenUtil().setHeight(30),
                                //   color: Colors.black,
                                // ),
                                Image.asset(
                                  'assets/imgs/send.png',
                                  height: ScreenUtil().setHeight(30),
                                  package: "wallet_flutter",
                                ),
                                SizedBox(height: ScreenUtil().setHeight(10)),
                                Text(
                                  S.current.ethereum_home_detail_transfer,
                                  style: TextStyle(
                                    color: Colors.black,
                                  ),
                                )
                              ],
                            ),
                            // style: ButtonStyle(
                            //   backgroundColor: MaterialStateColor.resolveWith(
                            //       (states) => Colors.white
                            //       // ThemeUtils().getColor(
                            //       //   'views.chaincore.ethereum.assets_detail.header_button_1',
                            //       // ),
                            //       ),
                            // ),
                            onTap: () =>

                                // /// 显示地址搜索选择页面
                                // Navigator.of(context)
                                //     .push<
                                //         Ethereum
                                //             .RecipientAccountBlog>(
                                //       ViewAnimations.viewRightIn(
                                //         AddressSelector(
                                //           localKeys:
                                //               CoreProvider.of(context)
                                //                   .wallet
                                //                   .keysOf(
                                //                     CoreProvider.of(
                                //                             context)
                                //                         .networkConfig
                                //                         .chainCore,
                                //                   ),
                                //           recentAccounts: await widget
                                //               .dataSourceProvider
                                //               .localService
                                //               .selectRecentRecipientAccounts,
                                //         ),
                                //       ),
                                //     )
                                //     .then(
                                //       /// 正确选择了地址，则进入转账页面
                                //       (recipient) => recipient != null
                                //           ?
                                CoreProvider.of(context)
                                            .account
                                            .privateKeyRaw !=
                                        null
                                    ? Navigator.of(context)
                                        .pushAndRemoveUntil<String>(
                                        ViewAnimations.viewRightIn(
                                          widget.tokenBlog != null
                                              ? Transfer(
                                                  web3Client: widget.web3Client,
                                                  tokenBlog: widget.tokenBlog,
                                                  fromAccount: widget.account,
                                                  scanAPIService:
                                                      scanAPIService,
                                                  // toAccount:
                                                  //     recipient,
                                                  dataSourceProvider:
                                                      widget.dataSourceProvider,
                                                  networkConfig:
                                                      widget.networkConfig,
                                                )
                                              : CustomCoinTransfer(
                                                  web3Client: widget.web3Client,
                                                  tokenBlog: widget.tokenBlog,
                                                  fromAccount: widget.account,
                                                  scanAPIService:
                                                      scanAPIService,
                                                  // toAccount:
                                                  //     recipient,
                                                  dataSourceProvider:
                                                      widget.dataSourceProvider,
                                                  networkConfig:
                                                      widget.networkConfig,
                                                ),
                                        ),
                                        (route) => true,
                                      )
                                    : Fluttertoast.showToast(
                                        msg: S.current.wallet_watch_name,
                                      )
                            // : null,
                            )
                        // .then(checkPendingTransaction),
                        ),
                  ),
                  // ),
                ],
              ),
            ),
          ),

          //项目详情
          // widget.tokenBlog != null
          //     ? Positioned(
          //         top: ScreenUtil().setHeight(79),
          //         right: ScreenUtil().setWidth(22),
          //         child: Row(
          //           children: [
          //             InkWell(
          //               onTap: () {
          //                 Navigator.of(context).push(
          //                   ViewAnimations.viewRightIn(
          //                     TokenInfomationPage(
          //                       tokenBlog: this.widget.tokenBlog,
          //                       mainAdress: widget.account.address,
          //                       dataSourceProvider:
          //                           this.widget.dataSourceProvider,
          //                     ),
          //                   ),
          //                 );
          //               },
          //               child: Container(
          //                 child: Text(
          //                   S.current.ethereum_trade_head,
          //                   style: TextStyle(
          //                     fontSize: ScreenUtil().setSp(12),
          //                     color: Colors.white,
          //                   ),
          //                 ),
          //               ),
          //             )
          //           ],
          //         ))
          //     : Container(),
        ],
      ));

  @override
  void dispose() {
    super.dispose();
  }
}
