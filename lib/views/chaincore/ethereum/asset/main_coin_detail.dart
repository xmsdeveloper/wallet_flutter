// @dart=2.9
part of '../asset.dart';

class MainCoinDetailView extends StatefulWidget {
  final Ethereum.TokenBlog tokenBlog;
  final WalletKey account;
  final Web3ClientProvider web3Client;
  final EndPoint endPoint;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final Color color;

  MainCoinDetailView({
    @required this.tokenBlog,
    @required this.account,
    @required this.web3Client,
    @required this.endPoint,
    @required this.dataSourceProvider,
    @required this.networkConfig,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _MainCoinDetailViewState();
}

class _MainCoinDetailViewState extends State<MainCoinDetailView>
    with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  static const int PageSize = 50;
  bool inRefersh = false;
  int count = 0;

  /// 交易记录筛选控制
  TabController tabController;
  ValueNotifier<FilterType> fliterListenable =
      ValueNotifier<FilterType>(FilterType.All);

  Ethereum.HistoryService historyService;
  Ethereum.ScanAPIService scanAPIService;

  ValueNotifier<List<Ethereum.Transaction>> dataSourceListenable =
      ValueNotifier<List<Ethereum.Transaction>>(null);

  List<Ethereum.Transaction> _flitedDataSource = [];

  // ignore: non_constant_identifier_names
  final Map<FilterType, String> FilterTypeNames = {
    FilterType.All: S.current.ethereum_home_detail_txfilter_all,
    FilterType.In: S.current.ethereum_home_detail_txfilter_in,
    FilterType.Out: S.current.ethereum_home_detail_txfilter_out,
    FilterType.Other: S.current.ethereum_home_detail_txfilter_call,
  };
  @override
  void initState() {
    super.initState();

    tabController = TabController(
      initialIndex: 0,
      length: FilterType.values.length,
      vsync: this,
    );

    scanAPIService = Ethereum.ScanAPIService(
      httpClient: http.Client(),
      hostURL: widget.endPoint.scanURL,
      apiKey: widget.endPoint.scanAPIKey,
    );

    historyService = Ethereum.HistoryService(
      fromAddress: widget.account.address,
      scanAPIService: scanAPIService,
    );

    /// 数据逻辑
    /// 1.先使用数据库中的数据显示50条记录
    /// 2.立即开始请求最新的数据，在请求完毕后，service会自动写入数据库。
    /// 3.写入完毕后重新获取记录
    historyService.selectTransaction(0, PageSize).then((txs) {
      dataSourceListenable.value = txs;
    });

    // / 上拉追加
    scrollController.addListener(
      () async {
        if (scrollController.position.pixels !=
                scrollController.position.maxScrollExtent ||
            inRefersh) {
          return;
        }

        /// 1.先展示完所有本地数据库的数据,若数据库已没有数据则使用网络载入
        var pageTxs = await historyService.selectTransaction(
          dataSourceListenable.value.length,
          PageSize,
        );

        if (pageTxs != null && pageTxs.length > 0) {
          dataSourceListenable.value.addAll(pageTxs);
          setState(() {});
        }

        /// 数据库记录已显示完毕
        if (pageTxs.length < PageSize) {
          pageTxs = await historyService.more();
          if (pageTxs != null && pageTxs.length > 0) {
            dataSourceListenable.value.addAll(pageTxs);
            setState(() {});
          }
        }
      },
    );

    /// 在build完成后调用滚动到指定区域,模拟一次下拉刷新
    SchedulerBinding.instance.addPostFrameCallback(
      (_) async {
        scrollController.animateTo(
          -81.0,
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      },
    );
  }

  Future checkPendingTransaction(String txHash) async {
    if (txHash == null) {
      return;
    }

    print('checkPendingTransaction:$txHash');
    final receipt = widget.web3Client.getTransactionReceipt(txHash);

    // print(receipt.toString());
  }

  // /// 根据选项卡选项筛选数据，并存储在本对象中
  // void _doFilter() {
  //   switch (fliterListenable.value) {

  //     /// ALL
  //     case 0:
  //       _flitedDataSource = dataSourceListenable.value;
  //       return;

  //     /// In
  //     case 1:
  //       _flitedDataSource = dataSourceListenable.value
  //           .where((e) =>
  //               e.from.toLowerCase() != widget.account.address.toLowerCase() &&
  //               e.value.length > 0)
  //           .toList();
  //       return;

  //     /// Out
  //     case 2:
  //       _flitedDataSource = dataSourceListenable.value
  //           .where((e) =>
  //               e.from.toLowerCase() == widget.account.address.toLowerCase() &&
  //               e.value.length > 0 &&
  //               e.input.length <= 2)
  //           .toList();
  //       return;

  //     /// Other
  //     case 3:
  //       _flitedDataSource = dataSourceListenable.value
  //           .where((e) =>
  //               e.from.toLowerCase() == widget.account.address.toLowerCase() &&
  //               e.input.length > 2)
  //           .toList();
  //       return;
  //   }
  // }

  Future<void> _onRefresh() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.

    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.

    } else if (connectivityResult == ConnectivityResult.none) {
      return Fluttertoast.showToast(
        msg: S.current.network_error,
      );
    }

    try {
      inRefersh = true;
      await Future.delayed(Duration(milliseconds: 500));
      await historyService.refresh();

      dataSourceListenable.value = await historyService.selectTransaction(
        0,
        PageSize,
      );
      // }
    } catch (e) {} finally {
      inRefersh = false;
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        //       backgroundColor: ThemeUtils().getColor(
        //         'views.chaincore.ethereum.assets_detail.header_background',
        //       ),
        //       body: SafeArea(
        //         child:
        backgroundColor: Color.fromRGBO(250, 250, 250, 1),
        body: CustomScrollView(
          controller: scrollController,
          physics: const AlwaysScrollableScrollPhysics(
              // parent: BouncingScrollPhysics(),
              parent: BouncingScrollPhysics()),
          slivers: [
            _DetailViewHeader(
              tokenBlog: widget.tokenBlog,
              account: widget.account,
              web3Client: widget.web3Client,
              endPoint: widget.endPoint,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
              color: widget.color,
            ),

            //  SmarRefresher
            CupertinoSliverRefreshControl(
              refreshTriggerPullDistance: 80,
              onRefresh: _onRefresh,
            ),

            // Container(
            //   child:

            //       /// TabBar
            //       SliverSafeArea(
            //     top: false,
            //     bottom: false,
            //     left: false,
            //     right: false,
            //     sliver: SliverAppBar(
            //       automaticallyImplyLeading: false,
            //       elevation: 0,
            //       // 标题栏是否固定
            //       pinned: true,
            //       // backgroundColor: ThemeUtils().getColor(
            //       //   'views.chaincore.ethereum.assets_detail.header_background',
            //       // ),
            //       backgroundColor: Color.fromRGBO(250, 250, 250, 1),

            //     ),
            //   ),
            // ),
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.only(
                  left: ScreenUtil().setWidth(3),
                ),
                child: TabBar(
                  // indicatorColor: ThemeUtils().getColor(
                  //   'views.chaincore.ethereum.assets_detail.tab_bar_indicator',
                  // ),
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorColor: Colors.blue,
                  controller: tabController,
                  indicatorWeight: 2,
                  isScrollable: true,
                  labelColor: Colors.black,
                  unselectedLabelColor: Color.fromRGBO(175, 175, 175, 1),
                  tabs: FilterTypeNames.values
                      .map(
                        (e) => Container(
                          alignment: Alignment.center,
                          height: ScreenUtil().setHeight(40),
                          child: Text(e,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(16),
                              )
                              // style: ThemeUtils().getTextStyle(
                              //   'views.chaincore.ethereum.assets_detail.textstyle.cell_tab_title',
                              // ),
                              ),
                        ),
                      )
                      .toList(),
                  onTap: (index) {
                    fliterListenable.value = FilterType.values[index];
                  },
                ),
              ),
            ),

            ValueListenableBuilder<List<Ethereum.Transaction>>(
              valueListenable: dataSourceListenable,
              builder: (context, txs, child) =>
                  ValueListenableBuilder<FilterType>(
                valueListenable: fliterListenable,
                builder: (context, ftype, child) {
                  final cellAdapter = _HistoryCellBuilderAdapter(adapters: [
                    _BaseTransactionAdapterBuilder(),
                  ]);

                  if (txs != null && txs.length > 0) {
                    _flitedDataSource = cellAdapter.filterByType(
                      txs,
                      ftype,
                      widget.account.address,
                    );
                  } else {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) =>
                            //
                            Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(97),
                              ),
                              width: ScreenUtil().setWidth(57),
                              height: ScreenUtil().setHeight(56),
                              child:
                                  Image.asset("assets/images/asset_nodata.png"),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(9),
                              ),
                              child: Text(
                                S.current.ethereum_trade_transactions,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(12),
                                  color: Color.fromRGBO(216, 216, 216, 1),
                                ),
                              ),
                            ),
                          ],
                        ),
                        childCount: 1,
                      ),
                    );
                  }

                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) => cellAdapter.build(
                        context: context,
                        currentAddress: widget.account.address,
                        txOrLog: _flitedDataSource[index],
                        tokenBlog: widget.tokenBlog,
                        index: index,
                      ),
                      childCount: _flitedDataSource == null
                          ? 0
                          : _flitedDataSource.length,
                    ),
                  );
                },
              ),
            )
          ],
        ),
      );

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }
}
