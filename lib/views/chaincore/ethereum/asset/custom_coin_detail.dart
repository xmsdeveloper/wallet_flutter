// @dart=2.9
part of '../asset.dart';

class CustomCoinDetailView extends StatefulWidget {
  final Ethereum.TokenBlog tokenBlog;
  final WalletKey account;
  final Web3ClientProvider web3Client;
  final EndPoint endPoint;
  final Ethereum.DataSourceProvider dataSourceProvider;
  final NetworkConfig networkConfig;
  final Color color;

  CustomCoinDetailView({
    @required this.tokenBlog,
    @required this.account,
    @required this.web3Client,
    @required this.endPoint,
    @required this.dataSourceProvider,
    @required this.networkConfig,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _CustomCoinDetailView();
}

class _CustomCoinDetailView extends State<CustomCoinDetailView>
    with TickerProviderStateMixin {
  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(null);
  final ValueNotifier<DateTime> refreshListenable =
      ValueNotifier<DateTime>(DateTime.now());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        //       backgroundColor: ThemeUtils().getColor(
        //         'views.chaincore.ethereum.assets_detail.header_background',
        //       ),
        //       body: SafeArea(
        //         child:
        backgroundColor: Color.fromRGBO(250, 250, 250, 1),
        body: CustomScrollView(
          // controller: scrollController,
          physics: const AlwaysScrollableScrollPhysics(
              // parent: BouncingScrollPhysics(),
              parent: BouncingScrollPhysics()),
          slivers: [
            _DetailViewHeader(
              // tokenBlog: widget.tokenBlog,
              tokenBlog: TokenBlog(
                symbol: CoreProvider.of(context).networkConfig.mainSymbol,
                address: "",
                name: CoreProvider.of(context).networkConfig.mainSymbol,
                decimals: 18,
              ),
              account: widget.account,
              web3Client: widget.web3Client,
              endPoint: widget.endPoint,
              dataSourceProvider: widget.dataSourceProvider,
              networkConfig: widget.networkConfig,
              refreshListenable: refreshListenable,
              color: widget.color,
            ),
            // CupertinoSliverRefreshControl(
            //   onRefresh: () {
            //     refreshListenable.value = DateTime.now();
            //     return Future.delayed(Duration(milliseconds: 1000));
            //   },
            // ),
            SliverToBoxAdapter(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(97),
                    ),
                    width: ScreenUtil().setWidth(57),
                    height: ScreenUtil().setHeight(56),
                    child: Image.asset("assets/images/asset_nodata.png"),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(9),
                    ),
                    child: Text(
                      S.current.ethereum_trade_transactions,
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: Color.fromRGBO(216, 216, 216, 1),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  @override
  void dispose() {
    super.dispose();
  }
}
