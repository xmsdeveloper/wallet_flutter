// @dart=2.9
part of '../../asset.dart';

class HomeNftPage extends StatefulWidget {
  final Ethereum.DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final WalletKey account;
  List<Ethereum.TokenBlog> localNftList;
  HomeNftPage({
    this.dataSourceProvider,
    this.web3clientProvider,
    this.localNftList,
    this.account,
  });

  @override
  State<HomeNftPage> createState() => _HomeNftPageState();
}

class _HomeNftPageState extends State<HomeNftPage> {
  static List<GlobalKey<RemoveNftItemState>> childItemStates = [];

  final ValueNotifier<List<String>> balanceList =
      ValueNotifier<List<String>>([]);

  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(null);

  ValueNotifier<List<Ethereum.TokenBlog>> homeNftList =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    homeNftList.value = widget.localNftList;
    eventBus.on<HomeNftDelete>().listen((event) {
      widget.dataSourceProvider.localService.nftTokens.then(
        (value) {
          // setState(() {
          //   widget.sTokens = value;
          // });
          homeNftList.value = value;
        },
      );
    });
    // setState(() {});

    for (var i = 0; i < widget.localNftList.length; i++) {
      GlobalKey<RemoveNftItemState> removeGK = GlobalKey(debugLabel: "$i");
      childItemStates.add(removeGK);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_nft_asset_management_home_asset_title,
        elevation: 0.3,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: Container(
                height: MediaQuery.of(context).size.height * 0.9,
                width: MediaQuery.of(context).size.width,
                child: ValueListenableBuilder(
                    valueListenable: homeNftList,
                    builder: (context, value, child) {
                      return ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: value.length,
                          itemBuilder: (context, i) {
                            return Column(
                              children: [
                                RemoveNftItem(
                                  sTokens: value[i],
                                  dataSourceProvider: widget.dataSourceProvider,
                                  visible: NftDetai(
                                    sTokens: value[i],
                                    account: widget.account,
                                    dataSourceProvider:
                                        widget.dataSourceProvider,
                                    web3clientProvider:
                                        widget.web3clientProvider,
                                  ),
                                  moveItemKey: childItemStates[i],
                                  onActionDown: () {
                                    //循环遍历列表，关闭非当前item打开项
                                    closeItems(childItemStates, i);
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Container(
                                    width: double.infinity,
                                    height: 0.5,
                                    color: Colors.grey[200],
                                  ),
                                )
                              ],
                            );
                          });
                    })),
          )
        ],
      ),
    );
  }

  /// 循环遍历，利用State.来关闭非当前Item
  static void closeItems(
      List<GlobalKey<RemoveNftItemState>> childItemStates, int index) {
    childItemStates.forEach((element) {
      if (element != childItemStates[index]) {
        element.currentState?.closeItems();
      }
    });
  }
}
