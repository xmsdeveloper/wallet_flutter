// @dart=2.9
part of '../../asset.dart';

class MyNftPage extends StatefulWidget {
  final Ethereum.DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final WalletKey account;
  final List<Ethereum.TokenBlog> myAssets;
  final List<Ethereum.TokenBlog> localNftList;
  final List<Ethereum.TokenBlog> show;
  final NetworkConfig networkConfig;

  const MyNftPage({
    this.dataSourceProvider,
    this.web3clientProvider,
    this.myAssets,
    this.account,
    this.localNftList,
    this.show,
    this.networkConfig,
  });

  @override
  State<MyNftPage> createState() => _MyNftPageState();
}

class _MyNftPageState extends State<MyNftPage> {
  List<String> balanceList = [];
  final ValueNotifier<bool> ignoreShow = ValueNotifier<bool>(false);
  final ValueNotifier<List<Ethereum.TokenBlog>> sTokens =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  List<Ethereum.TokenBlog> favoriteTokens = [];
  List<Ethereum.TokenBlog> newAsset = [];
  final ValueNotifier<bool> nodataImageListenable = ValueNotifier<bool>(false);
  final ValueNotifier<List<Ethereum.TokenBlog>> allMyAsset =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  List<TokenBlog> showMyTokens = [];
  String ping;
  _getAllMyNftList() async {
    this.ping = widget.networkConfig.chainName.toLowerCase();
    if (widget.networkConfig.chainName == "Ethereum") {
      this.ping = "rest";
    } else if (widget.networkConfig.chainName == "BSC") {
      this.ping = "bnb";
    }
    var header = {
      'Connection': 'keep-alive',
      'X-API-KEY': 'HYfXOQHehc3cBckQ42pgfNaa'
    };
    var url =
        "https://${this.ping}api.nftscan.com/api/v2/account/own/all/${widget.account.address}";
    var usdrespose = await http.get(url, headers: header);
    print(url);
    var response = json.decode(usdrespose.body);
    List nfTokenList = response['data'];

    if (nfTokenList.length > 0) {
      setState(() {
        nfTokenList.forEach((nft) {
          allMyAsset.value.add(
            TokenBlog(
              logo: nft["logo_url"],
              name: nft["description"],
              symbol: nft["contract_name"],
              address: nft["contract_address"],
              protocol: nft["assets"][0]['erc_type'],
              decimals: 0,
            ),
          );
        });
      });
    } else {
      nodataImageListenable.value = true;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getAllMyNftList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavigationBar(
        title: S.current.ethereum_nft_asset_management_home_asset_my_balance,
        elevation: 0.3,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      ListTile(
                        contentPadding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                        ),
                        title: Text(
                          S.current
                              .ethereum_asset_management_my_balance_assetList,
                          style: ThemeUtils().getTextStyle(
                            'sheet.wallet_selector.textstyle.accounts_group_title',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          this.allMyAsset.value.length > 0
              ? SliverToBoxAdapter(
                  child: ValueListenableBuilder(
                      valueListenable: allMyAsset,
                      builder: (context, value, child) {
                        return Container(
                          height: MediaQuery.of(context).size.height * 0.7,
                          width: MediaQuery.of(context).size.width,
                          child: ListView.builder(
                              physics: BouncingScrollPhysics(),
                              itemCount: value.length,
                              itemBuilder: (context, i) {
                                return Column(
                                  children: [
                                    NftDetai(
                                      sTokens: value[i],
                                      account: widget.account,
                                      dataSourceProvider:
                                          widget.dataSourceProvider,
                                      web3clientProvider:
                                          widget.web3clientProvider,
                                      isHome: false,
                                      localNftList: widget.localNftList,
                                    ),
                                  ],
                                );
                              }),
                        );
                      }))
              : ValueListenableBuilder(
                  valueListenable: nodataImageListenable,
                  builder: (context, value, child) {
                    return SliverToBoxAdapter(
                      child: nodataImageListenable.value == false
                          ? Container(
                              child: Center(
                                child: LineSpinFadeLoaderIndicator(
                                  ballColor: Colors.grey,
                                ),
                              ),
                            )
                          : NoDataView(
                              imgPath: 'assets/images/no_token.png',
                              tips: S.current
                                  .ethereum_asset_management_search_nodata_tips,
                              topHeight: 146,
                            ),
                    );
                  })
        ],
      ),
    );
  }

  Widget showTokenPage() => Column(
        children: newAsset
            .map((e) => Container(
                  child: Stack(
                    children: [
                      NewAssetDetail(
                        newAsset: e,
                        dataSourceProvider: widget.dataSourceProvider,
                        web3clientProvider: widget.web3clientProvider,
                        account: widget.account,
                      ),
                      Positioned(
                        right: ScreenUtil().setWidth(22),
                        child: InkWell(
                          onTap: () async {
                            if (await widget.dataSourceProvider.localService
                                    .favoriteTokens
                                    .then((value) => value.length) <=
                                20) {
                              await widget.dataSourceProvider.localService
                                  .insertFavoriteToken(
                                e,
                              );
                            } else {
                              Fluttertoast.showToast(
                                  msg: S.current
                                      .ethereum_home_asset_limit_warming);
                            }

                            widget.dataSourceProvider.localService
                                .insertMyAsset(
                                    CoreProvider.of(context)
                                        .networkConfig
                                        .mainSymbol,
                                    CoreProvider.of(context).account.address,
                                    e);
                            newAsset.remove(e);

                            setState(() {});
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(38),
                            height: ScreenUtil().setHeight(70),
                            // color: Colors.blue,
                          ),
                        ),
                      )
                    ],
                  ),
                ))
            .toList(),
      );
}
