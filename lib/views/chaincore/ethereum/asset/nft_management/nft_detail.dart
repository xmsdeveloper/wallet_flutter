// @dart=2.9
part of '../../asset.dart';

class NftDetai extends StatefulWidget {
  final TokenBlog sTokens;
  final WalletKey account;
  final DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;
  final List<Ethereum.TokenBlog> localNftList;
  final bool isHome;
  const NftDetai(
      {this.sTokens,
      this.dataSourceProvider,
      this.account,
      this.web3clientProvider,
      this.isHome,
      this.localNftList});

  @override
  State<NftDetai> createState() => _NftDetaiState();
}

class _NftDetaiState extends State<NftDetai> {
  final ValueNotifier<Amount> balanceListenable = ValueNotifier<Amount>(Amount(
    value: BigInt.from(0),
    decimals: 18,
  ));
  List<Ethereum.TokenBlog> localNewNftList = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.dataSourceProvider.localService.nftTokens.then((value) {
      setState(() {
        localNewNftList = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: ScreenUtil().setHeight(60),
      height: ScreenUtil().setHeight(87),
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(4),
        color: Colors.white,
        border: Border(
          top: BorderSide(
            width: 0.2,
            color: Color.fromRGBO(216, 216, 216, 1),
          ),
        ),
      ),

      // margin: EdgeInsets.only(
      //   top: ScreenUtil().setHeight(2),
      // ),
      // height: ScreenUtil().setHeight(73),
      child: ListTile(
        minLeadingWidth: ScreenUtil().setWidth(0),
        contentPadding: EdgeInsets.only(
          left: ScreenUtil().setWidth(18),
          bottom: ScreenUtil().setHeight(8),
          right: ScreenUtil().setWidth(0),
        ),
        leading: Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(12),
                ),
                width: ScreenUtil().setWidth(35),
                height: ScreenUtil().setWidth(35),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.2,
                    color: Colors.grey[200],
                  ),
                  shape: BoxShape.circle,
                ),
                child: ClipPath.shape(
                  shape: CircleBorder(),
                  child: Image.network(
                    widget.sTokens.logo ??
                        CoreProvider.of(context).endPoint.resourcesServicesURL +
                            '/' +
                            widget.sTokens.address +
                            "/logo.png" ??
                        widget.sTokens.logo,
                    errorBuilder: (context, error, StackTrace) {
                      return Image.asset(
                        'assets/imgs/default_token.png',
                        package: 'wallet_flutter',
                        width: ScreenUtil().setWidth(30),
                        height: ScreenUtil().setHeight(30),
                        fit: BoxFit.fill,
                      );
                    },
                    frameBuilder: (context, child, frame, wasSynchronusLoaded) {
                      if (wasSynchronusLoaded) {
                        return child;
                      }
                      return AnimatedOpacity(
                        child: child,
                        opacity: frame == null ? 0 : 1,
                        duration: const Duration(seconds: 2),
                        curve: Curves.easeOut,
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        title: Container(
          height: ScreenUtil().setHeight(80),
          // height: ScreenUtil().setHeight(40),
          width: ScreenUtil().setWidth(220),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                // e.symbol,
                widget.sTokens.symbol ?? "",
                style: ThemeUtils().getTextStyle(
                  'views.chaincore.ethereum.assets.textstyle.cell_title',
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(2)),
              widget.sTokens.address != ""
                  ? Text(
                      // widget.sTokens.address,
                      widget.sTokens.address.length > 0
                          ? widget.sTokens.address.replaceRange(
                              9, widget.sTokens.address.length - 9, '...')
                          : widget.sTokens.address.replaceRange(
                              9, widget.sTokens.address.length - 9, '...'),

                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
        trailing: widget.sTokens.symbol !=
                CoreProvider().networkConfig.mainSymbol
            ? widget.isHome != false
                ? Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(
                      right: ScreenUtil().setWidth(20),
                    ),
                    width: ScreenUtil().setWidth(95),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              // bottom: ScreenUtil().setHeight(10),
                              ),
                          width: ScreenUtil().setHeight(25),
                          height: ScreenUtil().setHeight(25),
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            shape: BoxShape.circle,
                          ),
                          child: Icon(
                            Icons.horizontal_rule,
                            color: Colors.white,
                            size: ScreenUtil().setHeight(20),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(
                    // alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(15),
                      right: ScreenUtil().setWidth(22),
                    ),

                    child: InkWell(
                      onTap: () async {
                        if (!localNewNftList.contains(widget.sTokens)) {
                          widget.dataSourceProvider.localService.insertNftToken(
                            widget.sTokens,
                          );
                        } else {
                          widget.dataSourceProvider.localService
                              .removeNftTokens(
                            [widget.sTokens],
                          );
                        }
                        widget.dataSourceProvider.localService.nftTokens.then(
                          (value) {
                            setState(() {
                              localNewNftList = value;
                            });
                          },
                        );
                      },
                      child: Container(
                        child: Image.asset(
                          !localNewNftList.contains(widget.sTokens)
                              ? 'assets/images/switch_off.png'
                              : 'assets/images/switch_on.png',
                          width: ScreenUtil().setWidth(38),
                          height: ScreenUtil().setHeight(19),
                        ),
                      ),
                    ),
                  )
            : Container(
                child: Text(''),
              ),
      ),
    );
  }
}
