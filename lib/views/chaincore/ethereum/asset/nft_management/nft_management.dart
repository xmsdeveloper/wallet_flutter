// @dart=2.9
part of '../../asset.dart';

// ignore: must_be_immutable
class NftManagementPage extends StatefulWidget {
  final Ethereum.DataSourceProvider dataSourceProvider;
  final Web3ClientProvider web3clientProvider;

  NftManagementPage({
    @required this.dataSourceProvider,
    @required this.web3clientProvider,
  });
  @override
  _NftManagementPageState createState() => _NftManagementPageState();
}

class _NftManagementPageState extends State<NftManagementPage> {
  String _latestSearchKeyword = "";

  Timer _latestSearchTimer;

  List<Ethereum.TokenBlog> nftTokens = [];

  /// 合约地址正则
  final contractAddressRegexp = RegExp(
    r'[0-9a-f]{40}',

    /// 忽略大小写
    caseSensitive: false,
  );

  final Completer<ResponseList<Ethereum.TokenBlog>> hotListResponse =
      Completer();

  final TextEditingController searchTokenListController =
      TextEditingController();
  final FocusNode inputFocusNode = FocusNode();

  List<Ethereum.TokenBlog> searchlist = [];

  ScrollController scrollController;
  bool isOpen = true;

  bool isOpenLocal = true;

  bool igNore = false;

  final List<Future<List<Ethereum.TokenBlog>>> futureGroup = [];
  // List<Ethereum.TokenBlog> sTokens = [];
  List data;
  final ValueNotifier<List<Ethereum.TokenBlog>> localNftList =
      ValueNotifier<List<Ethereum.TokenBlog>>([]);
  final ValueNotifier<bool> ignoreShow = ValueNotifier<bool>(false);

  final ValueNotifier<int> showNum = ValueNotifier<int>(0);
  Future<List<Ethereum.TokenBlog>> fetchTokenDataSource() async {
    /// 本地有数据直接返回
    localNftList.value = await widget.dataSourceProvider.localService.nftTokens;

    return localNftList.value;
  }

  final ValueNotifier<bool> errorListenable = ValueNotifier<bool>(true);
  List<TokenBlog> show = [];
  TokenBlog chainTokenBlog;
  @override
  void initState() {
    super.initState();

    fetchTokenDataSource();

    //获取代币本地数据和网络数据
    widget.dataSourceProvider.localService.nftTokens.then((tokens) {
      nftTokens = tokens;
      return widget.dataSourceProvider.remoteServices.token.requestHot();
    }).then(hotListResponse.complete);

    // 间隔500毫秒检测输入的Keyword并且重新搜索结果
    _latestSearchTimer = Timer.periodic(Duration(milliseconds: 1000), (_) {
      // searchlist.clear();

      if (this.searchTokenListController.text == '') {
        searchlist.clear();
      }
      //防止接口一直被调用
      if (_latestSearchKeyword == this.searchTokenListController.text ||
          this.searchTokenListController.text.length <= 0) {
        return;
      }
      //刷新状态，重新搜索时，状态变为搜索中的状态
      errorListenable.value = true;

      final List<Future<List<Ethereum.TokenBlog>>> futureGroup = [];
      this.searchlist = [];

      final searchKeyWord = this.searchTokenListController.text;

      // 判断Keywor是否是一个完整的地址，
      if (contractAddressRegexp.hasMatch(searchKeyWord)) {
        _getNft(searchKeyWord);
      } else {
        errorListenable.value = false;
        _latestSearchKeyword = searchKeyWord;
      }
    });
  }

  _getNft(String searchKeyWord) async {
    await Utils.futureGetErc721TokenBlog(
      this.widget.web3clientProvider,
      searchKeyWord,
      CoreProvider.of(context).account.address,
    ).then(
      (chainTokenBlog) => searchlist = [chainTokenBlog],
    );

    setState(() {});
    errorListenable.value = true;
    _latestSearchKeyword = searchKeyWord;
  }

  @override
  void dispose() {
    super.dispose();
    _latestSearchTimer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0,
        actions: [
          Container(
            margin: EdgeInsets.only(
              right: ScreenUtil().setHeight(20),
            ),
            child: Center(
              child: InkWell(
                child: Text(
                  S.current.backbuttom_name,
                  style: TextStyle(
                    color: Color.fromRGBO(51, 51, 51, 1),
                  ),
                ),
                onTap: () => Navigator.pop(context),
              ),
            ),
          )
        ],
        title: Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(),
            height: ScreenUtil().setHeight(36),
            decoration: BoxDecoration(
              color: Color.fromRGBO(241, 241, 241, 1),
              borderRadius: BorderRadius.circular(4),
            ),
            padding:
                EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
            child: TextFormField(
              textInputAction: TextInputAction.search,
              focusNode: inputFocusNode,
              controller: searchTokenListController,
              onChanged: (_) {
                if (searchTokenListController.text == "") {
                  _latestSearchKeyword = "";
                }
                setState(() {});
              },
              style: ThemeUtils()
                  .getTextStyle(
                    'utils.input.textstyle.content',
                  )
                  .copyWith(fontSize: 15),
              decoration: InputDecoration(
                  hintText: S.current.ethereum_nft_asset_management_hit_text,
                  hintStyle:
                      // ThemeUtils().getTextStyle(
                      //   'utils.input.textstyle.hit',
                      // ),
                      TextStyle(
                    fontSize: ScreenUtil().setSp(14),
                    color: Color.fromRGBO(112, 112, 112, 1),
                  ),
                  contentPadding:
                      EdgeInsets.only(bottom: ScreenUtil().setHeight(0)),
                  // border: InputBorder.none,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(
                        ScreenUtil().setWidth(22),
                      ),
                    ),
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  // enabledBorder: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(
                        ScreenUtil().setWidth(22),
                      ),
                    ),
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  // focusedBorder: InputBorder.none,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(
                        ScreenUtil().setWidth(22),
                      ),
                    ),
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  prefixIconConstraints: BoxConstraints.expand(
                    width: ScreenUtil().setWidth(50),
                    height: ScreenUtil().setHeight(60),
                  ),
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    // color: ThemeUtils().getColor(
                    //   'views.chaincore.ethereum.address_selector.section_header_search',
                    // ),
                    color: Color.fromRGBO(133, 133, 133, 1),
                  ),
                  suffixIcon: inputFocusNode.hasFocus
                      ? InkWell(
                          child: Container(
                            margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(30),
                            ),
                            child: Icon(
                              Icons.close,
                              size: 15,
                              color: ThemeUtils().getColor(
                                'views.chaincore.ethereum.address_selector.section_header_search',
                              ),
                            ),
                          ),
                          onTap: () {
                            searchTokenListController.clear();
                            searchTokenListController.text = "";
                            _latestSearchKeyword = "";
                            setState(() {});
                          },
                        )
                      : null),
            )),
      ),
      backgroundColor: Colors.white,
      body: this.searchTokenListController.text == ""
          ? SingleChildScrollView(
              controller: scrollController,
              physics: const AlwaysScrollableScrollPhysics(
                parent: BouncingScrollPhysics(),
              ),
              child: Column(
                children: [
                  ValueListenableBuilder(
                      valueListenable: localNftList,
                      builder: (context, value, child) {
                        return Container(
                          child: ListTile(
                              contentPadding: EdgeInsets.only(
                                left: ScreenUtil().setWidth(20),
                                right: ScreenUtil().setWidth(20),
                              ),
                              title: Text(
                                S.current
                                    .ethereum_nft_asset_management_home_asset_title,
                                style: ThemeUtils().getTextStyle(
                                  'sheet.wallet_selector.textstyle.accounts_group_title',
                                ),
                              ),
                              trailing: Container(
                                width: ScreenUtil().setWidth(140),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.arrow_forward_ios_sharp,
                                      color: Colors.grey[400],
                                      size: ScreenUtil().setHeight(12),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () async {
                                await Navigator.of(context).push(
                                  ViewAnimations.viewRightIn(
                                    HomeNftPage(
                                      dataSourceProvider:
                                          widget.dataSourceProvider,
                                      web3clientProvider:
                                          widget.web3clientProvider,
                                      account: CoreProvider.of(context).account,
                                      localNftList: this.localNftList.value,
                                    ),
                                  ),
                                );
                                setState(() {
                                  widget.dataSourceProvider.localService
                                      .favoriteTokens
                                      .then(
                                    (value) {
                                      setState(() {
                                        nftTokens = value;
                                      });
                                    },
                                  );
                                });
                              }),
                        );
                      }),
                  Container(
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(20),
                      right: ScreenUtil().setWidth(20),
                    ),
                    height: 0.2,
                    color: Color.fromRGBO(216, 216, 216, 1),
                  ),
                  //我的资产
                  InkWell(
                    onTap: () async {
                      await Navigator.of(context).push(
                          ViewAnimations.viewRightIn(MyNftPage(
                              dataSourceProvider: widget.dataSourceProvider,
                              web3clientProvider: widget.web3clientProvider,
                              account: CoreProvider.of(context).account,
                              localNftList: this.localNftList.value,
                              show: show,
                              networkConfig:
                                  CoreProvider.of(context).networkConfig)));
                      setState(() {
                        fetchTokenDataSource();
                      });
                    },
                    child: Container(
                      child: ListTile(
                        contentPadding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(20),
                        ),
                        title: Text(
                          // group.groupName,
                          S.current
                              .ethereum_nft_asset_management_home_asset_my_balance,
                          style: ThemeUtils().getTextStyle(
                            'sheet.wallet_selector.textstyle.accounts_group_title',
                          ),
                        ),
                        trailing: Container(
                          width: ScreenUtil().setWidth(140),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              showNum.value > 0
                                  ? Container(
                                      margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(16),
                                        right: ScreenUtil().setWidth(11),
                                      ),
                                      height: ScreenUtil().setHeight(16),
                                      width: ScreenUtil().setWidth(16),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red,
                                      ),
                                      child: Center(
                                          child: ValueListenableBuilder(
                                              valueListenable: showNum,
                                              builder: (context, value, child) {
                                                return Text(
                                                  showNum.value.toString(),
                                                  style: TextStyle(
                                                    fontSize:
                                                        ScreenUtil().setSp(12),
                                                    color: Colors.white,
                                                  ),
                                                );
                                              })),
                                    )
                                  : Container(),
                              Icon(
                                Icons.arrow_forward_ios_sharp,
                                color: Colors.grey[400],
                                size: ScreenUtil().setHeight(12),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: ScreenUtil().setHeight(10),
                    color: Color.fromRGBO(246, 246, 246, 1),
                  ),
                ],
              ),
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.all(ScreenUtil().setHeight(20)),
                  child: Text(
                    S.current.ethereum_asset_management_searchtype_second,
                  ),
                ),
                Expanded(child: searchResultPage()),
              ],
            ),
    );
  }

  Widget hotTokenPage() => FutureBuilder<ResponseList<Ethereum.TokenBlog>>(
        future: hotListResponse.future,
        builder: (context, snapshot) => snapshot.connectionState !=
                ConnectionState.done
            ? Container(
                child: Center(
                  child: LineSpinFadeLoaderIndicator(
                    ballColor: Colors.grey,
                  ),
                ),
              )
            : Column(
                children: snapshot.data.data
                    .map(
                      (e) => Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              // borderRadius: BorderRadius.circular(4),
                              color: Colors.white,
                              border: Border(
                                bottom: BorderSide(
                                  width: 0.2,
                                  color: Color.fromRGBO(216, 216, 216, 1),
                                ),
                              ),
                            ),
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(1),
                            ),
                            child: ListTile(
                              minLeadingWidth: ScreenUtil().setWidth(0),
                              contentPadding: EdgeInsets.only(
                                left: ScreenUtil().setWidth(18),
                                right: ScreenUtil().setWidth(0),
                              ),
                              leading: Container(
                                width: ScreenUtil().setWidth(35),
                                height: ScreenUtil().setWidth(35),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: Colors.grey[200],
                                  ),
                                  shape: BoxShape.circle,
                                ),
                                child: ClipPath.shape(
                                  shape: CircleBorder(),
                                  child: CachedImage.assetNetwork(
                                    url: e.logo,
                                    placeholder: Image.asset(
                                      'assets/imgs/default_token.png',
                                      package: 'wallet_flutter',
                                    ),
                                    imageCacheHeight: 100,
                                    imageCacheWidth: 100,
                                  ),
                                ),
                              ),
                              title: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      e.symbol,
                                      style: ThemeUtils().getTextStyle(
                                        'views.chaincore.ethereum.assets.textstyle.cell_title',
                                      ),
                                    ),
                                    SizedBox(height: ScreenUtil().setHeight(2)),
                                    Text(
                                      // e.address,

                                      e.address.length > 0
                                          ? e.address.replaceRange(
                                              9, e.address.length - 9, '...')
                                          : e.address.replaceRange(
                                              9, e.address.length - 9, '...'),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: ThemeUtils().getTextStyle(
                                        'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              trailing: Container(
                                // alignment: Alignment.centerRight,
                                padding: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(22),
                                ),

                                child: InkWell(
                                  onTap: () async {
                                    if (!nftTokens.contains(
                                      TokenBlog(
                                        symbol: e.symbol,
                                        address: e.address.toLowerCase(),
                                        name: e.name,
                                        decimals: e.decimals,
                                        logo: e.logo,
                                        protocol: e.protocol,
                                      ),
                                    )) {
                                      if (await widget.dataSourceProvider
                                              .localService.favoriteTokens
                                              .then((value) => value.length) <=
                                          20) {
                                        widget.dataSourceProvider.localService
                                            .insertFavoriteToken(
                                          TokenBlog(
                                            symbol: e.symbol,
                                            address: e.address.toLowerCase(),
                                            name: e.name,
                                            decimals: e.decimals,
                                            logo: e.logo,
                                            protocol: e.protocol,
                                          ),
                                        );
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: S.current
                                                .ethereum_home_asset_limit_warming);
                                      }
                                    } else {
                                      widget.dataSourceProvider.localService
                                          .removeFavoriteTokens(
                                        [
                                          TokenBlog(
                                            symbol: e.symbol,
                                            address: e.address.toLowerCase(),
                                            name: e.name,
                                            decimals: e.decimals,
                                            logo: e.logo,
                                            protocol: e.protocol,
                                          ),
                                        ],
                                      );
                                      widget.dataSourceProvider.localService
                                          .tokenPricetListDelete(
                                              CoreProvider.of(context)
                                                  .account
                                                  .address,
                                              await widget.dataSourceProvider
                                                  .localService.favoriteTokens
                                                  .then((value) =>
                                                      value.indexOf(
                                                        TokenBlog(
                                                          symbol: e.symbol,
                                                          address: e.address
                                                              .toLowerCase(),
                                                          name: e.name,
                                                          decimals: e.decimals,
                                                          logo: e.logo,
                                                          protocol: e.protocol,
                                                        ),
                                                      )));
                                      //删除代币金额缓存
                                      widget.dataSourceProvider.localService
                                          .removeCachedPrice(
                                              TokenBlog(
                                                symbol: e.symbol,
                                                address:
                                                    e.address.toLowerCase(),
                                                name: e.name,
                                                decimals: e.decimals,
                                                logo: e.logo,
                                                protocol: e.protocol,
                                              ),
                                              CoreProvider.of(context)
                                                  .account
                                                  .address);
                                    }

                                    widget.dataSourceProvider.localService
                                        .favoriteTokens
                                        .then(
                                      (value) {
                                        setState(() {
                                          nftTokens = value;

                                          fetchTokenDataSource();
                                        });
                                      },
                                    );
                                  },
                                  child: Container(
                                    child: Image.asset(
                                      !nftTokens.contains(
                                        TokenBlog(
                                          symbol: e.symbol,
                                          address: e.address.toLowerCase(),
                                          name: e.name,
                                          decimals: e.decimals,
                                          logo: e.logo,
                                          protocol: e.protocol,
                                        ),
                                      )
                                          ? 'assets/images/switch_off.png'
                                          : 'assets/images/switch_on.png',
                                      width: ScreenUtil().setWidth(38),
                                      height: ScreenUtil().setHeight(19),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                    .toList(),
              ),
      );

  Widget searchResultPage() {
    return this.searchlist.length > 0
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: searchlist.length >= 10 ? 10 : searchlist.length,
            itemBuilder: (contex, index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.white,
                ),
                margin: EdgeInsets.only(
                  bottom: ScreenUtil().setHeight(1),
                ),
                height: ScreenUtil().setHeight(73),
                child: Row(
                  children: [
                    Container(
                        alignment: Alignment.center,
                        width: ScreenUtil().setWidth(35),
                        height: ScreenUtil().setHeight(35),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Colors.grey[200],
                          ),
                          shape: BoxShape.circle,
                        ),
                        margin: EdgeInsets.only(
                          left: ScreenUtil().setWidth(20),
                          right: ScreenUtil().setWidth(10),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ClipPath.shape(
                                shape: CircleBorder(),
                                child: AspectRatio(
                                  aspectRatio: 1 / 1,
                                  child: Image.network(
                                      CoreProvider.of(context)
                                              .endPoint
                                              .resourcesServicesURL +
                                          '/' +
                                          searchlist[index].address +
                                          "/logo.png",
                                      errorBuilder:
                                          (context, error, StackTrace) {
                                    return Image.asset(
                                      'assets/imgs/nft_default.png',
                                      package: 'wallet_flutter',
                                      fit: BoxFit.fill,
                                    );
                                  }, frameBuilder: (context, child, frame,
                                          wasSynchronusLoaded) {
                                    if (wasSynchronusLoaded) {
                                      return child;
                                    }
                                    return AnimatedOpacity(
                                      child: child,
                                      opacity: frame == null ? 0 : 1,
                                      duration: const Duration(seconds: 2),
                                      curve: Curves.easeOut,
                                    );
                                  }),
                                )),
                          ],
                        )),
                    Container(
                      width: ScreenUtil().setWidth(200),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            searchlist[index].symbol,
                            // e['mum'],
                            style: ThemeUtils().getTextStyle(
                              'views.chaincore.ethereum.assets.textstyle.cell_title',
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(2)),
                          Text(
                            // searchlist[index].address,
                            searchlist[index].address.replaceRange(
                                12,
                                searchlist[index].address.length - 12,
                                '......'),

                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: ThemeUtils().getTextStyle(
                              'views.chaincore.ethereum.assets.textstyle.cell_subtitle',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.only(
                        right: ScreenUtil().setWidth(10),
                      ),
                      width: ScreenUtil().setWidth(95),
                      child: Switch(
                        activeColor: Colors.blue,
                        value: nftTokens.contains(searchlist[index]),
                        onChanged: (isFav) async {
                          if (isFav) {
                            widget.dataSourceProvider.localService
                                .insertNftToken(searchlist[index]);
                          } else {
                            widget.dataSourceProvider.localService
                                .removeNftTokens(
                              [searchlist[index]],
                            );
                          }

                          widget.dataSourceProvider.localService.nftTokens.then(
                            (value) {
                              nftTokens = value;
                              setState(() {});
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              );
            },
          )
        : ValueListenableBuilder(
            valueListenable: errorListenable,
            builder: (context, value, child) {
              return Container(
                alignment: Alignment.center,
                child: errorListenable.value == false
                    ? Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(146),
                            ),
                            width: ScreenUtil().setWidth(103.16),
                            height: ScreenUtil().setHeight(106.36),
                            child: Image.asset(
                              'assets/images/no_token.png',
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(30),
                            ),
                            child: Text(
                              S.current
                                  .ethereum_asset_management_search_nodata_tips,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                                color: Color.fromRGBO(175, 175, 175, 1),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              openDapp(
                                context,
                                Ethereum.Dapp(
                                  url:
                                      "https://github.com/hashpayinfo/hashpayinfo.github.io/",
                                  logo: "",
                                ),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(10),
                              ),
                              child: Text(
                                S.current
                                    .ethereum_asset_management_search_nodata_submit,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(14),
                                  color: Color.fromRGBO(62, 149, 252, 1),
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Text(
                        S.current
                            .ethereum_asset_management_search_nodata_loading,
                      ),
              );
            });
  }

  void openDapp(BuildContext context, Ethereum.Dapp dapp) async {
    await Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        DappContentView(
          dapp: dapp,
          account: CoreProvider.of(context).account,
          wallet: CoreProvider.of(context).wallet,
          networkConfig: CoreProvider.of(context).networkConfig,
          web3clientProvider: null,
          dataSourceProvider: null,
        ),
      ),
    );
    setState(() {});
  }
}
