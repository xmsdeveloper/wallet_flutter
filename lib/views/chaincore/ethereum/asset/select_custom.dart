// @dart=2.9
part of '../asset.dart';

class AddressSelectorCustom extends StatefulWidget {
  final List<Ethereum.RecipientAccountBlog> recentAccounts;
  final List<Ethereum.RecipientAccountBlog> localAccounts;
  final String qrcode;

  AddressSelectorCustom({
    @required List<WalletKey> localKeys,
    @required List<Ethereum.RecipientAccountBlog> recentAccounts,
    this.qrcode,
  })  : localAccounts = localKeys
            .map(
              (key) => Ethereum.RecipientAccountBlog(
                key.address,
                key.nickName,
                key.avatarPath,
              ),
            )
            .toList(),

        /// 翻转顺序
        this.recentAccounts =
            recentAccounts == null ? [] : recentAccounts.reversed.toList();

  @override
  State<StatefulWidget> createState() => _AddressSelectorCustomState();
}

typedef RecipientAccountBlogCallBack = void Function(
    Ethereum.RecipientAccountBlog blog);

class _AddressSelectorCustomState extends State<AddressSelectorCustom> {
  /// 类型和名称对应的语言文案转换
  final Map<_AddressSourceType, String> addressSourceTypeToName = {
    _AddressSourceType.RecentUsed:
        S.current.ethereum_home_address_selector_recent,
    _AddressSourceType.Local: S.current.ethereum_home_address_selector_local,
  };

  final TextEditingController addressEditingController =
      TextEditingController();
  final FocusNode inputFocusNode = FocusNode();

  /// 正确地址的正则表达式
  final addressRegexp = RegExp(
    r'[0-9a-f]{40}',

    /// 忽略大小写
    caseSensitive: false,
  );

  /// 所有可以搜索的数据
  Map<_AddressSourceType, List<Ethereum.RecipientAccountBlog>>
      addressDataSource = {};

  /// 展开状态记录
  Map<_AddressSourceType, bool> isExpandedStateMapping = {
    _AddressSourceType.RecentUsed: true,
    _AddressSourceType.Local: true,
  };

  @override
  void initState() {
    super.initState();

    this.addressEditingController.text = this.widget.qrcode;

    /// 0：最近交易地址
    addressDataSource[_AddressSourceType.RecentUsed] =
        widget.recentAccounts ?? [];

    /// 1: 自身Wallet地址
    addressDataSource[_AddressSourceType.Local] = widget.localAccounts;

    inputFocusNode.addListener(() => setState(() {}));
  }

  /// 是否所有的条件都无法匹配任何正确的结果,可以根据此判断就行空页显示
  bool isUnmatchAllAddress() {
    /// 如果输入框没有内容，则一定可以显示出Local的本机地址，和所有最近使用的地址
    if (addressEditingController.text.length <= 0) {
      return false;
    }

    /// 若有内容则尝试是否可以从addressDataSource中获取到任意一个正确的地址
    if (filterSourceByType(_AddressSourceType.Local).length > 0 ||
        filterSourceByType(_AddressSourceType.RecentUsed).length > 0) {
      return false;
    }

    /// 当前输入的数据是否可以提取任意一个正确的地址
    return !addressRegexp.hasMatch(addressEditingController.text);
  }

  /// 筛选后的数据集合
  List<Ethereum.RecipientAccountBlog> filterSourceByType(
          _AddressSourceType type) =>

      /// 暂未输入地址
      /// 任意区间匹配，用户可能直接输入头几位，或者末尾几位
      addressEditingController.text == null ||
              addressEditingController.text.length <= 0
          ? addressDataSource[type]
          : addressDataSource[type]
              .where(
                (blog) =>

                    /// 地址包含
                    blog.address.toLowerCase().contains(
                          addressEditingController.text.toLowerCase(),
                        ) ||

                    /// 名称包含
                    (blog.name != null &&
                        blog.name.toLowerCase().contains(
                              addressEditingController.text.toLowerCase(),
                            )),
              )
              .toList();

  Widget addressCellTile(
    Ethereum.RecipientAccountBlog blog, {
    @required _RecipientAccountBlogCallBack onTap,
  }) =>
      Container(
        color: ThemeUtils().getColor(
          'views.chaincore.ethereum.address_selector.cell_background',
        ),
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10)),
        child: ListTile(
          onTap: () => onTap(blog),
          // trailing: Icon(
          //   Icons.arrow_forward_ios,
          //   size: 12,
          // ),
          tileColor: ThemeUtils().getColor(
            'views.chaincore.ethereum.address_selector.cell_background',
          ),
          title: Row(
            children: [
              Image.asset(
                blog.avatarPath,
                package: 'wallet_flutter',
                width: 30,
                height: 30,
              ),
              SizedBox(width: ScreenUtil().setWidth(15)),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    blog.name ??
                        S.current.ethereum_home_address_selector_new_address,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.address_selector.textstyle.cell_account_name',
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(2)),
                  Text(
                    blog.addressBlog,
                    overflow: TextOverflow.ellipsis,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.address_selector.textstyle.cell_account_address',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );

  /*等待返回数据是异步操作*/
  void skipToPageD() async {
    // final result = await Navigator.pushNamed(context, pageD);
    final result = await Navigator.of(context).push(
      ViewAnimations.viewRightIn(
        ScanPage(),
      ),
    );

    String p = result as String;

    this.addressEditingController.text = p;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ThemeUtils().getColor(
          'utils.view_controller.background',
        ),
        appBar: NavigationBar(
          title: S.current.ethereum_home_address_selector_title,
          actions: [
            IconButton(
              // icon: Icon(Icons.crop_free_sharp),
              icon: Icon(
                const IconData(
                  0xe60a,
                  fontFamily: "myIcon",
                  fontPackage: "wallet_flutter",
                ),
              ),
              onPressed: () {
                skipToPageD();
              },
            ),
          ],
          bottom: PreferredSize(
            child: Container(
              padding:
                  EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
              color: ThemeUtils().getColor(
                'utils.view_controller.background',
              ),
              child: TextFormField(
                focusNode: inputFocusNode,
                controller: addressEditingController,
                autofocus: true,
                onChanged: (_) => setState(() {}),
                style: ThemeUtils()
                    .getTextStyle(
                      'utils.input.textstyle.content',
                    )
                    .copyWith(fontSize: 15),
                decoration: InputDecoration(
                    hintText:
                        S.current.ethereum_home_address_selector_placeholder,
                    hintStyle: ThemeUtils().getTextStyle(
                      'utils.input.textstyle.hit',
                    ),
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    prefixIconConstraints: BoxConstraints.expand(
                      width: ScreenUtil().setWidth(40),
                      height: ScreenUtil().setHeight(40),
                    ),
                    prefixIcon: Icon(
                      Icons.search_rounded,
                      color: ThemeUtils().getColor(
                        'views.chaincore.ethereum.address_selector.section_header_search',
                      ),
                    ),
                    suffixIconConstraints: BoxConstraints.expand(
                      width: ScreenUtil().setWidth(40),
                      height: ScreenUtil().setHeight(40),
                    ),
                    suffixIcon: inputFocusNode.hasFocus
                        ? IconButton(
                            padding: EdgeInsets.zero,
                            icon: Icon(
                              Icons.close,
                              size: 15,
                              color: ThemeUtils().getColor(
                                'views.chaincore.ethereum.address_selector.section_header_search',
                              ),
                            ),
                            onPressed: () {
                              setState(() {
                                addressEditingController.clear();
                              });
                            },
                          )
                        : null),
              ),
            ),
            preferredSize: Size.fromHeight(ScreenUtil().setHeight(50)),
          ),
        ),
        body: isUnmatchAllAddress()
            ? SizedBox.expand(
                child: Column(
                  children: [
                    SizedBox(height: ScreenUtil().setHeight(160)),
                    Icon(
                      Icons.search_off_rounded,
                      size: 80,
                      color: ThemeUtils().getColor(
                        'views.chaincore.ethereum.address_selector.unmatch_icon',
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20)),
                    Text(
                      S.current.ethereum_home_address_selector_unmatch,
                      style: ThemeUtils().getTextStyle(
                        'views.chaincore.ethereum.address_selector.textstyle.unmatch_any_addrses',
                      ),
                    ),
                  ],
                ),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    if (addressEditingController.text.length > 0 &&
                        addressRegexp.hasMatch(addressEditingController.text))
                      ListTile(
                        tileColor: ThemeUtils().getColor(
                          'views.chaincore.ethereum.address_selector.section_header_background',
                        ),
                        title: Text(
                          'Input',
                          style: ThemeUtils().getTextStyle(
                            'views.chaincore.ethereum.address_selector.textstyle.section_title',
                          ),
                        ),
                      ),
                    if (addressEditingController.text.length > 0 &&
                        addressRegexp.hasMatch(addressEditingController.text))
                      addressCellTile(
                        Ethereum.RecipientAccountBlog(
                            '0x' +
                                addressRegexp
                                    .firstMatch(
                                      addressEditingController.text,
                                    )
                                    .group(0),
                            null,
                            'assets/imgs/default_account.png'),
                        onTap: (blog) => Navigator.of(context).pop(blog),
                      ),
                    ExpansionPanelList(
                      elevation: 0,
                      expandedHeaderPadding: EdgeInsets.zero,
                      expansionCallback: (panelIndex, isExpanded) {
                        /// 计算对应的状态下标
                        /// 当前页面中是否显示了最近使用的地址
                        setState(() {
                          if (filterSourceByType(_AddressSourceType.RecentUsed)
                                  .length >
                              0) {
                            isExpandedStateMapping[panelIndex == 0
                                ? _AddressSourceType.RecentUsed
                                : _AddressSourceType.Local] = !isExpanded;
                          } else {
                            isExpandedStateMapping[_AddressSourceType.Local] =
                                !isExpanded;
                          }
                        });
                      },
                      children: _AddressSourceType.values

                          /// 没有数据源或者筛选以后没有数据源，不就行widget的build
                          .where((type) => filterSourceByType(type).length > 0)
                          .map(
                            (type) => ExpansionPanel(
                              backgroundColor: ThemeUtils().getColor(
                                'views.chaincore.ethereum.address_selector.section_header_background',
                              ),
                              canTapOnHeader: true,
                              isExpanded: isExpandedStateMapping[type],
                              headerBuilder: (context, isExpanded) => ListTile(
                                title: Text(
                                  addressSourceTypeToName[type],
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.address_selector.textstyle.section_title',
                                  ),
                                ),
                              ),
                              body: Column(
                                children: filterSourceByType(type)
                                    .map(
                                      (blog) => addressCellTile(
                                        blog,
                                        onTap: (_) =>
                                            Navigator.of(context).pop(blog),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ],
                ),
              ),
      );

  @override
  void dispose() {
    super.dispose();
  }
}

enum AddressSourceType {
  RecentUsed,
  Local,
}
