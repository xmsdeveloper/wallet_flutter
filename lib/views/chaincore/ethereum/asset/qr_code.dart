// @dart=2.9
part of '../asset.dart';

class QrCodePage extends StatefulWidget {
  final WalletKey account;
  final Ethereum.TokenBlog tokenBlog;
  final String symbol;
  final String chainname;
  final Color color;
  QrCodePage(
      {@required this.account,
      this.tokenBlog,
      this.symbol,
      this.chainname,
      this.color});
  @override
  _QrCodePageState createState() => _QrCodePageState();
}

class _QrCodePageState extends State<QrCodePage> {
  GlobalKey globalKey = GlobalKey();
  Uint8List newPngBytes;

  @override
  void initState() {
    super.initState();
    // 进入页面统计（手动采集时才可设置）
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// 保存图片
  static Future<void> saveImage(GlobalKey globalKey) async {
    RenderRepaintBoundary boundary =
        globalKey.currentContext.findRenderObject();
    var image = await boundary.toImage(pixelRatio: 6.0);
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();

    var status = await Permission.storage.status;
    if (status.isUndetermined) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.storage,
      ].request();
      saveImage(globalKey);
    }
    if (status.isGranted) {
      print("Android已授权");
      final result = await ImageGallerySaver.saveImage(pngBytes, quality: 60);
      if (result != null) {
        print('ok');
        // toast("保存成功", wring: false);
      } else {
        print('error');
        // toast("保存失败");
      }
    }
    if (status.isDenied) {
      print("Android拒绝");
    }

    // if (Platform.isIOS) {
    //   var status = await Permission.photos.status;
    //   if (status.isUndetermined) {
    //     Map<Permission, PermissionStatus> statuses = await [
    //       Permission.photos,
    //     ].request();
    //     saveImage(globalKey);
    //   }
    //   if (status.isGranted) {
    //     final result = await ImageGallerySaver.saveImage(pngBytes,
    //         quality: 60, name: "hello");
    //     if (result) {
    //       print('ok');
    //       // toast("保存成功", wring: false);
    //     } else {
    //       print('error');
    //       // toast("保存失败");
    //     }
    //   }
    //   if (status.isDenied) {
    //     print("IOS拒绝");
    //   }
    // } else if (Platform.isAndroid) {
    //   var status = await Permission.storage.status;
    //   if (status.isUndetermined) {
    //     Map<Permission, PermissionStatus> statuses = await [
    //       Permission.storage,
    //     ].request();
    //     saveImage(globalKey);
    //   }
    //   if (status.isGranted) {
    //     print("Android已授权");
    //     final result = await ImageGallerySaver.saveImage(pngBytes, quality: 60);
    //     if (result != null) {
    //       print('ok');
    //       // toast("保存成功", wring: false);
    //     } else {
    //       print('error');
    //       // toast("保存失败");
    //     }
    //   }
    //   if (status.isDenied) {
    //     print("Android拒绝");
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
          elevation: 0,
          backgroundColor: widget.color,
          centerTitle: true,
          title: Text(
            S.current.ethereum_home_qrcode_title,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(16),
            ),
          )),
      // NavigationBar(
      //   title: S.current.ethereum_home_qrcode_title,
      // ),
      // backgroundColor: ThemeUtils().getColor(
      //   'views.chaincore.ethereum.assets.header_background',
      // ),

      // backgroundColor: Color.fromRGBO(62, 149, 252, 1),
      backgroundColor: widget.color,
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                RepaintBoundary(
                  key: globalKey,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          left: ScreenUtil().setWidth(22),
                          right: ScreenUtil().setWidth(22),
                          top: ScreenUtil().setHeight(30),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          // color: ThemeUtils().getColor(
                          //   'views.chaincore.ethereum.qrcode.qr_background',
                          // ),
                          // borderRadius: BorderRadius.circular(15),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15),
                          ),
                          // boxShadow: [
                          //   BoxShadow(
                          //       color: Colors.grey[200],
                          //       offset: Offset(0.0, 5.0), //阴影xy轴偏移量
                          //       blurRadius: 1.0, //阴影模糊程度
                          //       spreadRadius: 1.0 //阴影扩散程度
                          //       )
                          // ],
                        ),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                left: ScreenUtil().setWidth(15),
                                right: ScreenUtil().setWidth(15),
                                top: ScreenUtil().setHeight(16),
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color.fromRGBO(252, 239, 230, 1),
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(18),
                                      right: ScreenUtil().setWidth(12),
                                      top: ScreenUtil().setHeight(17),
                                      bottom: ScreenUtil().setHeight(18),
                                    ),
                                    child: Image.asset(
                                      'assets/images/warming.png',
                                      width: ScreenUtil().setWidth(23),
                                      height: ScreenUtil().setHeight(23),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,

                                    // margin: EdgeInsets.only(
                                    //   left: ScreenUtil().setWidth(20),
                                    //   right: ScreenUtil().setWidth(20),
                                    //   top: ScreenUtil().setHeight(25),
                                    //   bottom: ScreenUtil().setHeight(20),
                                    // ),
                                    width: ScreenUtil().setWidth(240),

                                    child: Text(
                                      // CoreProvider.of(context)
                                      //             .networkConfig
                                      //             .chainName ==
                                      //         "Ethereum"
                                      //     // ? CoreProvider.of(context).networkConfig.chainName ==
                                      //     //         "BSC"
                                      //     // ? S.current.ethereum_home_qrcode_transfertips +
                                      //     //     " BEP20" +
                                      //     //     " ${widget.symbol} " +
                                      //     //     S.current
                                      //     //         .ethereum_home_qrcode_transfertips2
                                      //     ? S.current.ethereum_home_qrcode_transfertips +
                                      //         " ERC20" +
                                      //         " ${widget.symbol} " +
                                      //         S.current
                                      //             .ethereum_home_qrcode_transfertips2
                                      //     : CoreProvider.of(context)
                                      //                 .networkConfig
                                      //                 .chainName ==
                                      //             "HALO"
                                      //         ? S.current.ethereum_home_qrcode_transfertips +
                                      //             " ${widget.symbol} " +
                                      //             S.current
                                      //                 .ethereum_home_qrcode_transfertips2
                                      //         : CoreProvider.of(context)
                                      //                     .networkConfig
                                      //                     .chainName ==
                                      //                 "DT Network"
                                      //             ? S.current
                                      //                     .ethereum_home_qrcode_transfertips +
                                      //                 " ${widget.symbol} " +
                                      //                 S.current
                                      //                     .ethereum_home_qrcode_transfertips2
                                      //             : CoreProvider.of(context)
                                      //                         .networkConfig
                                      //                         .chainName ==
                                      //                     "HECO"
                                      //                 ? S.current
                                      //                         .ethereum_home_qrcode_transfertips +
                                      //                     " HRC20" +
                                      //                     " ${widget.symbol} " +
                                      //                     S.current
                                      //                         .ethereum_home_qrcode_transfertips2
                                      //                 : S.current
                                      //                         .ethereum_home_qrcode_transfertips +
                                      //                     " ${widget.symbol} " +
                                      //                     S.current
                                      //                         .ethereum_home_qrcode_transfertips2,
                                      S.current
                                              .ethereum_home_qrcode_transfertips +
                                          "${widget.symbol}" +
                                          S.current
                                              .ethereum_home_qrcode_transfertips2,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(14),
                                        color: Color.fromRGBO(231, 136, 77, 1),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(21),
                                bottom: ScreenUtil().setHeight(39),
                              ),
                              child: Align(
                                child: Text(
                                  widget.account.nickName,
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.qrcode.textstyle.nickname',
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              child: Text(S.current.ethereum_home_qrcode_tips,
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(13),
                                    color: Color.fromRGBO(112, 112, 112, 1),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(20),
                              ),
                              width: ScreenUtil().setWidth(205.26),
                              height: ScreenUtil().setHeight(206.69),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                    'assets/imgs/qr_codebg.png',
                                    package: "wallet_flutter",
                                  ),
                                  fit: BoxFit.fill,
                                ),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: QrImage(
                                  data: widget.account.address,
                                  version: QrVersions.auto,
                                  size: ScreenUtil().setSp(190),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: ScreenUtil().setHeight(20)),
                              child: Text(
                                S.current.ethereum_home_qrcode_wallet,
                                // style: ThemeUtils().getTextStyle(
                                //   'views.chaincore.ethereum.qrcode.textstyle.wallet_address_title',
                                // ),
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  color: Color.fromRGBO(155, 155, 155, 1),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                await Clipboard.setData(
                                  ClipboardData(
                                    text: widget.account.address,
                                  ),
                                );

                                Fluttertoast.showToast(
                                  msg: S.current.copy_success,
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.all(20),
                                child: Text(
                                  widget.account.address,
                                  textAlign: TextAlign.center,
                                  style: ThemeUtils().getTextStyle(
                                    'views.chaincore.ethereum.qrcode.textstyle.wallet_address_text',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(15),
                            bottomRight: Radius.circular(15),
                          ),
                          color: Colors.grey[200],
                        ),
                        margin: EdgeInsets.only(
                          left: ScreenUtil().setWidth(22),
                          right: ScreenUtil().setWidth(22),
                        ),
                        // width: ScreenUtil().setWidth(335),
                        height: ScreenUtil().setHeight(66),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                              onTap: () async {
                                await saveImage(globalKey);
                                Fluttertoast.showToast(
                                  msg: S.current
                                      .message_box_prompt_saved_to_phone_album,
                                );
                              },
                              child: Container(
                                child: Row(
                                  children: [
                                    Container(
                                      width: ScreenUtil().setWidth(22),
                                      height: ScreenUtil().setHeight(22),
                                      child: Image.asset(
                                        'assets/imgs/share.png',
                                        package: "wallet_flutter",
                                      ),
                                    ),
                                    SizedBox(
                                      width: ScreenUtil().setWidth(16),
                                    ),
                                    Container(
                                      child: Text(
                                        S.current.ethereum_home_qrcode_share,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: ScreenUtil().setHeight(54),
                              width: ScreenUtil().setWidth(2),
                              color: Color.fromRGBO(226, 226, 226, 1),
                            ),
                            InkWell(
                              onTap: () async {
                                await Clipboard.setData(
                                  ClipboardData(
                                    text: widget.account.address,
                                  ),
                                );

                                Fluttertoast.showToast(
                                  msg: S.current.copy_success,
                                );
                              },
                              child: Container(
                                child: Row(
                                  children: [
                                    Container(
                                      width: ScreenUtil().setWidth(22),
                                      height: ScreenUtil().setHeight(22),
                                      child: Image.asset(
                                        'assets/imgs/copy.png',
                                        package: "wallet_flutter",
                                      ),
                                    ),
                                    SizedBox(
                                      width: ScreenUtil().setWidth(16),
                                    ),
                                    Container(
                                      child: Text(
                                        S.current
                                            .ethereum_home_qrcode_copy_address,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Opacity(
                  opacity: 0.5,
                  child: Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(42),
                    ),
                    child: Center(
                      child: Text(
                        'Hashpay',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ));
}
