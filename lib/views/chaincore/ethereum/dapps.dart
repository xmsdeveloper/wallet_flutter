// @dart=2.9
library qytechnology.package.wallet.views.chaincore.ethereum.dapps;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;
import 'package:flutter_screenutil/screen_util.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_share/flutter_share.dart';

import 'package:dapp_browser_implements/ethereum/ethereum.dart' as Ethereum;
import 'package:wallet_flutter/command/theme/icon_utils.dart' as CustomIcons;
import 'package:dapp_browser/dapp_browser.dart' as Ethereum;
import 'package:wallet_flutter/services/ethereum.dart' as Ethereum;
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/views/chaincore/ethereum.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/setting/freeze_prompt.dart';

import 'package:wallet_flutter/views/sheet/transaction_confirm_sheet/sheet.dart';

import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

part 'dapps/view_controller.dart';
part 'dapps/dapp_web_view.dart';

part 'dapps/search_dapp.dart';

part 'dapps/search_dapp_result.dart';
