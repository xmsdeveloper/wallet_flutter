// @dart=2.9
library qytechnology.package.wallet.views.chaincore.ethereum.setting;

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import 'package:flutter_screenutil/screen_util.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:path_provider/path_provider.dart';
import 'package:package_info/package_info.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';

import 'package:wallet_flutter/command/theme/icon_utils.dart' as CustomIcons;

import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/command/language.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/services/ethereum.dart';
import 'package:wallet_flutter/views/chaincore/ethereum.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/dapps.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/setting/freeze_prompt.dart';

import 'package:wallet_flutter/views/keychain/private_display.dart';
import 'package:wallet_flutter/views/sheet/sheet.dart';
import 'package:wallet_flutter/views/utils/pwdIdentifier.dart';
import 'package:wallet_flutter/views/utils/utils.dart';
import 'package:wallet_flutter/views/utils/warming.dart';
import 'package:wallet_flutter/web3_provider/ethereum/web3_provider.dart';

part 'setting/view_controller.dart';

part 'setting/language.dart';

part 'setting/network.dart';

part 'setting/network/network_infomation.dart';

part 'setting/updata.dart';

part 'setting/change_password.dart';

part 'setting/forgot_password.dart';
part 'setting/about/about_us.dart';
part 'setting/share/share.dart';
