import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';

class MyRadioOption<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final String lable;
  final String text;
  final ValueChanged<T> onChange;
  const MyRadioOption({
    required this.value,
    required this.groupValue,
    required this.lable,
    required this.text,
    required this.onChange,
  });

  Widget buildLable() {
    final bool isSelected = value == groupValue;
    return isSelected == false
        ? Container(
            width: ScreenUtil().setWidth(16),
            height: ScreenUtil().setHeight(16),
            decoration: ShapeDecoration(
              shape: CircleBorder(
                side: BorderSide(
                  color: Color.fromRGBO(
                    216,
                    216,
                    216,
                    1,
                  ),
                ),
              ),
            ),
          )
        : Container(
            width: ScreenUtil().setWidth(16),
            height: ScreenUtil().setHeight(16),
            child: Image.asset(
              "assets/images/on.png",
            ),
          );
  }

  Widget buildtext() {
    return Text(
      text,
      style: TextStyle(
        fontSize: ScreenUtil().setSp(14),
        color: Color.fromRGBO(0, 0, 0, 1),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(23),
        // bottom: ScreenUtil().setHeight(22),
      ),
      child: InkWell(
        onTap: () => onChange(value),
        splashColor: Colors.cyan.withOpacity(0.5),
        child: Padding(
          padding: EdgeInsets.all(
            ScreenUtil().setWidth(5),
          ),
          child: Row(
            children: [
              buildLable(),
              SizedBox(width: ScreenUtil().setWidth(8)),
              buildtext(),
            ],
          ),
        ),
      ),
    );
  }
}
