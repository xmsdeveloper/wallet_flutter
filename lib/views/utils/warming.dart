import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';

class WarmingPage extends StatelessWidget {
  final String tipText;
  const WarmingPage({required this.tipText, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        // left: ScreenUtil().setHeight(24),
        // right: ScreenUtil().setWidth(24),
        top: ScreenUtil().setHeight(20),
      ),
      // height: ScreenUtil().setHeight(60),
      // width: ScreenUtil().setWidth(335),
      decoration: BoxDecoration(
        color: Color.fromRGBO(252, 239, 230, 1),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.topLeft,
            // height: ScreenUtil().setHeight(69),
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(17),
              top: ScreenUtil().setHeight(17),
            ),
            child: Column(
              children: [
                Image.asset(
                  'assets/images/warming.png',
                  width: ScreenUtil().setWidth(22.5),
                  height: ScreenUtil().setHeight(22.5),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(
              ScreenUtil().setHeight(17),
            ),
            width: ScreenUtil().setWidth(252),
            // height: ScreenUtil().setHeight(69),
            child: Align(
              // alignment: Alignment.center,
              child: Text(
                tipText,
                // style: ThemeUtils().getTextStyle(
                //   'views.chaincore.ethereum.settings.netinfomation.textstyle.tips_title',
                // ),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(14),
                  color: Color.fromRGBO(231, 136, 77, 1),
                ),
                maxLines: 4,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
