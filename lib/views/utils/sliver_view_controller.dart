// @dart=2.9
part of 'utils.dart';

class SliverViewControllerHeader extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  SliverViewControllerHeader({
    this.child,
  });

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) =>
      child;

  @override
  double get maxExtent => child.preferredSize.height;

  @override
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
}

abstract class SliverViewController extends ViewController {
  final NavigationBar navigationBar;

  SliverViewController({
    this.navigationBar,
  }) : super(
          navigationBar: navigationBar,
        );

  @override
  Widget render(BuildContext context) => CustomScrollView(
        physics: BouncingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        slivers: [
          SliverPersistentHeader(
            delegate:
                SliverViewControllerHeader(child: sliverViewHeader(context)),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              this.sliverViewItemForIndex,
              childCount: this.sliverViewItemCount(),
            ),
          ),
          SliverPersistentHeader(
            delegate:
                SliverViewControllerHeader(child: sliverViewFooter(context)),
          ),
        ],
      );

  PreferredSize sliverViewHeader(BuildContext context) => PreferredSize(
        child: Container(),
        preferredSize: Size.fromHeight(0),
      );

  PreferredSize sliverViewFooter(BuildContext context) => PreferredSize(
        child: Container(),
        preferredSize: Size.fromHeight(0),
      );

  // double sliverViewItemHeightForIndex(int index);
  int sliverViewItemCount();
  Widget sliverViewItemForIndex(BuildContext context, int index);
}
