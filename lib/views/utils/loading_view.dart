import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:loading_indicator_view/loading_indicator_view.dart';
import 'package:lottie/lottie.dart';
import 'package:wallet_flutter/generated/l10n.dart';

/// 圆形 Loading 框
class CircularLoading {
  // 调用者上下文
  final BuildContext _callerContext;
  String tips;

  CircularLoading(this._callerContext, this.tips);

  /// 显示 Loading 框
  ///
  /// [buildContext] 上下文
  void show() {
    debugPrint("Show loading dialog");
    // String tips = "";
    showDialog(
      // 弹框外去除背景色
      barrierColor: Colors.transparent,
      context: _callerContext,
      barrierDismissible: false,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) {
          return WillPopScope(
            // 禁止 back 按键
            onWillPop: () async => true,
            child: AlertDialog(
              // 移除阴影
              elevation: 0.0,
              // 弹框去除背景色
              backgroundColor: Colors.transparent,
              content: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                ),
                height: ScreenUtil().setHeight(209),
                // width: ScreenUtil().setWidth(220),
                width: MediaQuery.of(context).size.width -
                    ScreenUtil().setWidth(40),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // SizedBox(
                    //   width: 32,
                    //   height: 32,
                    //   child: LineSpinFadeLoaderIndicator(
                    //     ballColor: Colors.blue,
                    //   ),
                    // ),
                    // Image.asset(
                    //   'assets/images/loading.gif',
                    //   width: ScreenUtil().setWidth(53),
                    //   height: ScreenUtil().setHeight(53),
                    // ),
                    tips == ""
                        ? Container(
                            height: ScreenUtil().setHeight(160),
                            width: ScreenUtil().setWidth(160),
                            child: Lottie.asset(
                              'assets/clean.json',
                            ),
                          )
                        : Image.asset(
                            'assets/images/loading.gif',
                            width: ScreenUtil().setWidth(53),
                            height: ScreenUtil().setHeight(53),
                          ),
                    Container(
                      margin: EdgeInsets.only(
                        top: ScreenUtil().setHeight(29),
                      ),
                      child: Text(
                        this.tips,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          color: Color.fromRGBO(0, 0, 0, 1),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  /// 隐藏 Loading 框
  /// 该方法使用不当可能导致副作用，如路由返回丢失
  void dismiss() {
    // Navigator.of(_callerContext).pop();
    Navigator.of(_callerContext, rootNavigator: true).pop();
  }
}
