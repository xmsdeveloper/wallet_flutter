// @dart=2.9
part of 'utils.dart';

class Input extends StatefulWidget {
  final String title;
  final String placeholder;
  final TextInputType keyboardType;
  // final String initialValue;
  final bool obscureText;
  final TextEditingController controller;
  final String errorText;
  final TextStyle textStyle;
  final TextStyle hintStyle;
  final EdgeInsets contentPadding;
  final bool autofocus;
  final bool clearIconEnable;
  final Widget suffixIcon;
  final String suffixText;
  final TextStyle suffixStyle;
  final List<TextInputFormatter> inputFormatters;
  final ValueChanged<String> onChanged;

  Input({
    @required this.title,
    this.errorText,
    this.placeholder,
    this.keyboardType,
    // this.initialValue,
    this.obscureText = false,
    this.controller,
    this.textStyle,
    this.hintStyle,
    this.contentPadding,
    this.autofocus = false,
    this.clearIconEnable = true,
    this.suffixIcon,
    this.suffixText,
    this.suffixStyle,
    this.inputFormatters,
    this.onChanged,
  });

  @override
  State<StatefulWidget> createState() => _InputState();
}

class _InputState extends State<Input> {
  FocusNode inputFocusNode = FocusNode();

  TextEditingController textController;
  void initState() {
    inputFocusNode.addListener(() {
      setState(() {});
    });
    super.initState();

    textController = widget.controller ?? TextEditingController();
  }

  void dispose() {
    super.dispose();
    inputFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              widget.title,
              style: ThemeUtils().getTextStyle('utils.input.textstyle.title'),
              textAlign: TextAlign.left,
            ),
          ),
          TextFormField(
            controller: textController,
            obscureText: widget.obscureText,
            autofocus: widget.autofocus,
            keyboardType: widget.keyboardType,
            style: widget.textStyle ??
                ThemeUtils()
                    .getTextStyle('utils.input.textstyle.content')
                    .copyWith(
                      textBaseline: TextBaseline.alphabetic,
                    ),
            focusNode: inputFocusNode,
            inputFormatters: widget.inputFormatters,
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              errorText: widget.errorText,
              hintText: widget.placeholder,
              contentPadding: widget.contentPadding,
              hintStyle: widget.hintStyle ??
                  ThemeUtils().getTextStyle('utils.input.textstyle.hit'),
              border: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: ThemeUtils().getColor('utils.input.border_color'),
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: ThemeUtils().getColor('utils.input.border_color'),
                ),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color:
                      ThemeUtils().getColor('utils.input.border_focus_color'),
                ),
              ),
              suffixText: widget.suffixText,
              suffixStyle: widget.suffixStyle,
              suffixIconConstraints: BoxConstraints.expand(
                width: ScreenUtil().setWidth(30),
                height: ScreenUtil().setHeight(30),
              ),
              suffixIcon: widget.suffixIcon ??
                  (widget.clearIconEnable && inputFocusNode.hasFocus
                      ? IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(
                            Icons.close,
                            size: 15,
                            color: ThemeUtils()
                                .getColor('utils.input.border_focus_color'),
                          ),
                          onPressed: () {
                            textController.text = '';
                          },
                        )
                      : null),
            ),
          )
        ],
      );
}
