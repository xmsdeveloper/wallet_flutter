// @dart=2.9
part of 'utils.dart';

class TabViewController extends StatefulWidget {
  final List<Widget> viewControllers;

  final List<BottomNavigationBarItem> barItems;

  final List<NavigationBar> navigationBars;

  TabViewController({
    @required this.viewControllers,
    @required this.barItems,
    @required this.navigationBars,
  });

  @override
  State<StatefulWidget> createState() => _TabViewControllerState();
}

class _TabViewControllerState extends State<TabViewController> {
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    currentIndex = 0;
  }

  void _changePage(int index) {
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
      });
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor:
            ThemeUtils().getColor('utils.view_controller.background'),
        appBar: widget.navigationBars[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: widget.barItems,
          currentIndex: currentIndex,
          type: BottomNavigationBarType.shifting,
          onTap: (index) {
            _changePage(index);
          },
        ),
        body: widget.viewControllers[currentIndex],
      );
}
