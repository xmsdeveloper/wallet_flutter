// @dart=2.9
part of 'utils.dart';

enum StatusButtonState {
  Normal,
  Busy,
}

enum StatusButtonStyle {
  Info,
  Warning,
  Important,
}

class StatusButtonController with ChangeNotifier {
  String _title;
  String _busyTitle;
  StatusButtonState _status;
  StatusButtonStyle _style;
  bool _enable = true;

  StatusButtonController({
    String title,
    String busyTitle = 'Waiting...',
    StatusButtonState status = StatusButtonState.Normal,
    StatusButtonStyle style = StatusButtonStyle.Info,
    bool enable = true,
  }) {
    _title = title;
    _status = status;
    _style = style;
    _busyTitle = busyTitle;
    _enable = enable;
  }

  bool get enable => _enable;
  set enable(bool value) {
    _enable = value;
    notifyListeners();
  }

  String get title => _title;
  set title(String value) {
    _title = value;
    notifyListeners();
  }

  StatusButtonState get status => _status;
  set status(StatusButtonState value) {
    _status = value;
    notifyListeners();
  }

  String get busyTitle => _busyTitle;
  set busyTitle(value) {
    _busyTitle = value;
    notifyListeners();
  }

  StatusButtonStyle get style => _style;
  set style(StatusButtonStyle value) {
    _style = value;
    notifyListeners();
  }
}

typedef FutureCallback = Future<void> Function();

class StatusButton extends StatefulWidget {
  final StatusButtonController controller;
  final FutureCallback onPressFuture;
  final bool onFutureDoneSetState;

  StatusButton({
    @required this.controller,
    @required this.onPressFuture,
    this.onFutureDoneSetState = true,
  });

  @override
  State<StatefulWidget> createState() => _StatusButtonState();

  factory StatusButton.withController(
    StatusButtonController controller, {
    @required FutureCallback onPressedFuture,
  }) =>
      StatusButton(
        onPressFuture: onPressedFuture,
        controller: controller,
      );

  factory StatusButton.withStyle(
    StatusButtonStyle style, {
    bool enable = true,
    StatusButtonState state = StatusButtonState.Normal,
    @required String title,
    @required FutureCallback onPressedFuture,
    String busyTitle,
  }) =>
      StatusButton(
        onPressFuture: onPressedFuture,
        controller: StatusButtonController(
          busyTitle: busyTitle ?? title,
          title: title,
          status: state,
          style: style,
          enable: enable,
        ),
      );
}

class _StatusButtonState extends State<StatusButton> {
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    VoidCallback onPressed = () {
      if (widget.onPressFuture != null) {
        setState(() {
          widget.controller.status = StatusButtonState.Busy;
        });

        widget.onPressFuture().then(
          (value) {
            widget.onFutureDoneSetState
                ? setState(
                    () {
                      widget.controller.status = StatusButtonState.Normal;
                    },
                  )
                : widget.controller.status = StatusButtonState.Normal;
          },
        ).catchError((_) {}, test: (e) {
          setState(() {
            widget.controller.status = StatusButtonState.Normal;
          });
          return false;
        });
      }
    };

    final children = <Widget>[];

    // 按钮背景
    String backroundKeypath;
    TextStyle textStyle;
    switch (widget.controller._style) {
      case StatusButtonStyle.Info:
        backroundKeypath = 'utils.button.info.background';

        textStyle = ThemeUtils().getTextStyle('utils.button.info.textstyle');

        break;

      case StatusButtonStyle.Warning:
        backroundKeypath = 'utils.button.warning.background';
        textStyle = ThemeUtils().getTextStyle('utils.button.warning.textstyle');
        break;

      case StatusButtonStyle.Important:
        backroundKeypath = 'utils.button.important.background';
        textStyle =
            ThemeUtils().getTextStyle('utils.button.important.textstyle');
        break;

      default:
        backroundKeypath = 'utils.button.info.background';
        textStyle = ThemeUtils().getTextStyle('utils.button.info.textstyle');
    }

    // 处理Busy状态
    if (widget.controller.status == StatusButtonState.Busy) {
      children.add(
        Container(
          padding: EdgeInsets.only(right: ScreenUtil().setWidth(8)),
          child: BallSpinFadeLoaderIndicator(
            minBallRadius: ScreenUtil().setHeight(1),
            maxBallRadius: ScreenUtil().setHeight(2),
            radius: ScreenUtil().setHeight(8),
          ),
        ),
      );
    }
    children.add(Text(
      widget.controller.status == StatusButtonState.Normal
          ? widget.controller.title
          : widget.controller.busyTitle,
      style: textStyle,
    ));

    return
        // ElevatedButton(
        //   onPressed: widget.controller.enable
        //       ? widget.controller.status == StatusButtonState.Normal
        //           ? onPressed
        //           : null
        //       : null,
        //   style: ButtonStyle(
        //     backgroundColor: MaterialStateColor.resolveWith(
        //       (states) => widget.controller.enable
        //           ? widget.controller.status == StatusButtonState.Normal
        //               ? ThemeUtils().getColor(
        //                   backroundKeypath,
        //                 )
        //               // ? Color.fromRGBO(62, 149, 252, 1)
        //               // Colors.blueAccent
        //               // Color.fromRGBO(48, 198, 153, 1)
        //               : ThemeUtils().getColor(
        //                   'utils.button.disable.background',
        //                 )
        //           : ThemeUtils().getColor(
        //               'utils.button.disable.background',
        //             ),
        //     ),
        //   ),
        //   child: SizedBox.fromSize(
        //     child: Row(
        //       children: children,
        //       mainAxisAlignment: MainAxisAlignment.center,
        //     ),
        //     size: Size.fromHeight(ScreenUtil().setHeight(46)),
        //   ),
        // );
        InkWell(
            onTap: widget.controller.enable
                ? widget.controller.status == StatusButtonState.Normal
                    ? onPressed
                    : null
                : null,
            child: Container(
              decoration: BoxDecoration(
                color: MaterialStateColor.resolveWith(
                  (states) => widget.controller.enable
                      ? widget.controller.status == StatusButtonState.Normal
                          ? ThemeUtils().getColor(
                              backroundKeypath,
                            )
                          // ? Color.fromRGBO(62, 149, 252, 1)
                          // Colors.blueAccent
                          // Color.fromRGBO(48, 198, 153, 1)
                          : ThemeUtils().getColor(
                              'utils.button.disable.background',
                            )
                      : ThemeUtils().getColor(
                          'utils.button.disable.background',
                        ),
                ),
                borderRadius: BorderRadius.circular(8),
              ),
              child: SizedBox.fromSize(
                child: Row(
                  children: children,
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
                size: Size.fromHeight(ScreenUtil().setHeight(46)),
              ),
            ));
  }
}
