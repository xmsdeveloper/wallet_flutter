// @dart=2.9
part of 'utils.dart';

class localAuthApi {
  static final _auth = LocalAuthentication();

  static Future<bool> hasBiometrics() async {
    try {
      return await _auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      return false;
    }
  }

  // static Future<List<BiometricType>> getAvailableBiometrics() async {
  //   try {
  //     return await _auth.getAvailableBiometrics();
  //   } catch (e) {
  //     return e;
  //   }
  // }

  static Future<bool> authenticate() async {
    final isAvailable = await hasBiometrics();
    // print(isAvailable);

    if (!isAvailable) {
      Fluttertoast.showToast(
        msg: S.current.ethereum_trade_fingerprint_notEnrolled,
        gravity: ToastGravity.CENTER_LEFT,
      );
      return false;
    }
    ;
    final andStrings = AndroidAuthMessages(
      signInTitle: S.current.ethereum_trade_fingerprint_title,
      // goToSettingsDescription: "2",
      biometricHint: "",
      cancelButton: S.current.ethereum_trade_fingerprint_cancel,
    );
    try {
      return await _auth.authenticate(
        biometricOnly: true,
        localizedReason: S.current.ethereum_trade_fingerprint_tip,
        sensitiveTransaction: false,
        useErrorDialogs: true,
        stickyAuth: true,
        androidAuthStrings: andStrings,
      );
    } on PlatformException catch (e) {
      print(e.message);
      // if (e.details == null) {
      //   Fluttertoast.showToast(
      //     msg: "请在手机系统设置指纹信息",
      //     gravity: ToastGravity.CENTER_LEFT,
      //   );
      // }
      switch (e.code) {
        case "NotEnrolled":
          Fluttertoast.showToast(
            msg: S.current.ethereum_trade_fingerprint_notEnrolled,
            gravity: ToastGravity.CENTER_LEFT,
          );
          break;
        case "LockedOut":
          Fluttertoast.showToast(
            msg: S.current.ethereum_trade_fingerprint_local,
            gravity: ToastGravity.CENTER_LEFT,
          );
          break;
        case "PermanentlyLockedOut":
          Fluttertoast.showToast(
            msg: S.current.ethereum_trade_fingerprint_permanentlyLockedOut,
            gravity: ToastGravity.CENTER_LEFT,
          );
          break;
        default:
      }

      return false;
    }
  }
}

// class AuthPage extends StatefulWidget {
//   AuthPage({Key key}) : super(key: key);

//   @override
//   _AuthPageState createState() => _AuthPageState();
// }

// class _AuthPageState extends State<AuthPage> {
//   LocalAuthentication localAuth = LocalAuthentication();
//   @override
//   void dispose() {
//     super.dispose();
//     localAuth.stopAuthentication();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("指纹识别认证演示"),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             TextButton(
//               child: Text("检查是否支持生物识别"),
//               onPressed: () async {
//                 try {
//                   bool canCheckBiometrics = await localAuth.canCheckBiometrics;
//                   print(canCheckBiometrics);
//                 } catch (e) {
//                   print(e);
//                 }
//               },
//             ),
//             SizedBox(height: 30),
//             TextButton(
//               child: Text("获取生物识别技术列表"),
//               onPressed: () async {
//                 try {
//                   List<BiometricType> availableBiometrics =
//                       await localAuth.getAvailableBiometrics();
//                   print(availableBiometrics);
//                 } catch (e) {
//                   print(e);
//                 }
//               },
//             ),
//             SizedBox(height: 30),
//             TextButton(
//               child: Text("指纹/生物识别"),
//               onPressed: () async {
//                 const andStrings = const AndroidAuthMessages(
//                   cancelButton: '取消',
//                   goToSettingsButton: '去设置',
//                   // fingerprintNotRecognized: '指纹识别失败',
//                   goToSettingsDescription: '请设置指纹.',
//                   // fingerprintHint: '指纹',
//                   // fingerprintSuccess: '指纹识别成功',
//                   signInTitle: '指纹验证',
//                   // fingerprintRequiredTitle: '请先录入指纹!',
//                 );

//                 try {
//                   bool didAuthenticate =
//                       // ignore: deprecated_member_use
//                       await localAuth.authenticateWithBiometrics(
//                     localizedReason: '扫描指纹进行身份识别',
//                     useErrorDialogs: true,
//                     stickyAuth: true,
//                     androidAuthStrings: andStrings,
//                   );
//                   print(didAuthenticate);
//                 } catch (e) {
//                   print(e);
//                 }
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
