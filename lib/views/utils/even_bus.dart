import 'package:event_bus/event_bus.dart';

//Bus初始化a
EventBus eventBus = EventBus();

//首页代币
class AssetEvent {
  double str = 0.00;
  AssetEvent(double str) {
    this.str = str;
  }
}

//删除首页代币
class HomeAssetDelete {
  String str = "";
  HomeAssetDelete(String str) {
    this.str = str;
  }
}

//删除首页nft代币
class HomeNftDelete {
  String str = "";
  HomeNftDelete(String str) {
    this.str = str;
  }
}

//新增资产显示数量
class NewAssetNum {
  int str = 0;
  NewAssetNum(int str) {
    this.str = str;
  }
}
