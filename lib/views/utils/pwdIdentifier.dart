// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/core/base/amount.dart';

class PwdIdentifier extends StatefulWidget {
  final String passwordStrength;

  PwdIdentifier({this.passwordStrength});

  @override
  State<PwdIdentifier> createState() => _PwdIdentifierState();
}

class _PwdIdentifierState extends State<PwdIdentifier> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: ScreenUtil().setWidth(52),
          height: ScreenUtil().setHeight(12),
          decoration: BoxDecoration(
            color: widget.passwordStrength != ""
                ? Color.fromRGBO(253, 240, 206, 1)
                : Color.fromRGBO(230, 230, 230, 1),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(
                ScreenUtil().setHeight(7),
              ),
              bottomLeft: Radius.circular(
                ScreenUtil().setHeight(7),
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(2),
            right: ScreenUtil().setWidth(2),
          ),
          width: ScreenUtil().setWidth(52),
          height: ScreenUtil().setHeight(12),
          decoration: BoxDecoration(
            color: widget.passwordStrength == "middle" ||
                    widget.passwordStrength == "strength" ||
                    widget.passwordStrength == "中" ||
                    widget.passwordStrength == "强"
                ? Color.fromRGBO(151, 218, 123, 1)
                : Color.fromRGBO(230, 230, 230, 1),
          ),
        ),
        Container(
          width: ScreenUtil().setWidth(52),
          height: ScreenUtil().setHeight(12),
          decoration: BoxDecoration(
            color: widget.passwordStrength == "strength" ||
                    widget.passwordStrength == "强"
                ? Color.fromRGBO(80, 196, 25, 1)
                : Color.fromRGBO(230, 230, 230, 1),
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(
                ScreenUtil().setHeight(7),
              ),
              bottomRight: Radius.circular(
                ScreenUtil().setHeight(7),
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(30),
          ),
          child: Text(
            this.widget.passwordStrength,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(14),
            ),
          ),
        ),
      ],
    );
  }
}
