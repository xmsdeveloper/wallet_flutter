// @dart=2.9
part of 'utils.dart';

class GridPageViewDataSourceDelegate {
  /// 行数
  final int columCount;

  /// 页宽
  final double pageWidth;

  /// 页高
  final double pageHeight;

  /// 分页移动量
  final double pageChangeOffset;

  final IndexedWidgetBuilder itemBuilder;
  final int itemCount;

  GridPageViewDataSourceDelegate({
    @required this.columCount,
    @required this.pageWidth,
    @required this.pageHeight,
    @required this.itemBuilder,
    @required this.itemCount,
    double pageChangeOffset = 40,
  }) : this.pageChangeOffset = pageChangeOffset.abs();
}

// ignore: must_be_immutable
class GridPageView extends StatelessWidget {
  double scrollViewStartOffset = 0;
  final ScrollController gridController = ScrollController();

  final GridPageViewDataSourceDelegate dataSourceDelegate;

  GridPageView({
    @required this.dataSourceDelegate,
  });

  @override
  Widget build(BuildContext context) => Listener(
        onPointerDown: (_) {
          scrollViewStartOffset = gridController.offset;
        },
        onPointerUp: (_) {
          double pageWidth = dataSourceDelegate.pageWidth;
          double targetOffset;
          double moveVec = scrollViewStartOffset - gridController.offset;
          double changeOffsetVec = dataSourceDelegate.pageChangeOffset;

          if (moveVec == 0) {
            return;
          }

          /// 向左滑动
          else if (moveVec < 0) {
            /// 向左滑动超过50像素,小于一页
            if (moveVec <= -changeOffsetVec && moveVec > -pageWidth) {
              targetOffset = (gridController.offset + pageWidth).toInt() ~/
                  pageWidth.toInt() *
                  pageWidth;
            }

            /// 向左边滑动不超过50像素，还原当前页
            else if (moveVec > -changeOffsetVec) {
              targetOffset = gridController.offset.toInt() ~/
                  pageWidth.toInt() *
                  pageWidth;
            }
          }

          /// 向右滑动
          else {
            /// 向右滑动超过50像素,小于一页
            if (moveVec >= changeOffsetVec && moveVec < pageWidth) {
              targetOffset = gridController.offset.toInt() ~/
                  pageWidth.toInt() *
                  pageWidth;
            }

            /// 向右滑动不超过50像素，显示当前页
            else if (moveVec < changeOffsetVec) {
              targetOffset = (gridController.offset + pageWidth).toInt() ~/
                  pageWidth.toInt() *
                  pageWidth;
            }
          }

          if (targetOffset <= 0) {
            targetOffset = 0;
          }

          if (targetOffset > gridController.position.maxScrollExtent) {
            targetOffset = gridController.position.maxScrollExtent;
          }

          gridController.animateTo(
            targetOffset,
            duration: Duration(milliseconds: 200),
            curve: Curves.easeOut,
          );
        },
        child: Container(
          height: dataSourceDelegate.pageHeight,
          child: GridView.builder(
            padding: EdgeInsets.only(
              right: MediaQuery.of(context).size.width -
                  dataSourceDelegate.pageWidth,
            ),
            physics: const AlwaysScrollableScrollPhysics(
              parent: BouncingScrollPhysics(),
            ),
            controller: gridController,
            scrollDirection: Axis.horizontal,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: dataSourceDelegate.columCount,
              crossAxisSpacing: 0,
              mainAxisSpacing: 0,
              mainAxisExtent: dataSourceDelegate.pageWidth,
            ),
            itemBuilder: dataSourceDelegate.itemBuilder,
            itemCount: dataSourceDelegate.itemCount,
          ),
        ),
      );
}
