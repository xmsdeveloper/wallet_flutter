// @dart=2.9
part of 'utils.dart';

class NavigationBar extends AppBar {
  NavigationBar(
      {bool automaticallyImplyLeading = true,
      Widget leading,
      double titleSpacing,
      String title,
      List<Widget> actions,
      PreferredSizeWidget bottom,
      double leadingWidth,
      bool isBottomSheet = false,
      Color backgroundColor,
      double elevation = 0.0})
      : super(
          automaticallyImplyLeading: automaticallyImplyLeading,
          iconTheme: IconThemeData(
            color: Colors.black,
            // ThemeUtils().getColor(
            //   isBottomSheet ? 'sheet.icon' : 'utils.navigation_bar.icon',
            // ),
          ),
          leading: leading,
          leadingWidth: leadingWidth,
          elevation: isBottomSheet ? 0.1 : elevation,
          centerTitle: true,
          titleSpacing: titleSpacing ?? 0,
          title: Text(
            title,
            style: ThemeUtils().getTextStyle(
              isBottomSheet
                  ? 'sheet.textstyle.navigation_title'
                  // ? 'sheet.textstyle.navigation_title'
                  // : 'utils.navigation_bar.textstyle.title',
                  : 'sheet.textstyle.navigation_title',
            ),
          ),
          backgroundColor: backgroundColor ??
              ThemeUtils().getColor(isBottomSheet
                  ? 'sheet.background'
                  // : 'utils.navigation_bar.background',
                  : 'views.chaincore.ethereum.assets.cell_background'),
          actions: actions,
          bottom: bottom,
          excludeHeaderSemantics: true,
        );
}

class SliverNavigationBar extends SliverAppBar {
  SliverNavigationBar({
    bool automaticallyImplyLeading = true,
    Widget leading,
    double titleSpacing,
    @required String title,
    List<Widget> actions,
    PreferredSizeWidget bottom,
    double leadingWidth,
    bool isBottomSheet = false,
  }) : super(
          automaticallyImplyLeading: automaticallyImplyLeading,
          iconTheme: IconThemeData(
            color: ThemeUtils().getColor(
              isBottomSheet ? 'sheet.icon' : 'utils.navigation_bar.icon',
            ),
          ),
          leading: leading,
          leadingWidth: leadingWidth,
          elevation: isBottomSheet ? 2 : 0,
          centerTitle: true,
          pinned: true,
          floating: true,
          titleSpacing: titleSpacing ?? 0,
          title: Text(
            title,
            style: ThemeUtils().getTextStyle(
              isBottomSheet
                  ? 'sheet.textstyle.navigation_title'
                  : 'utils.navigation_bar.textstyle.title',
            ),
          ),
          backgroundColor: ThemeUtils().getColor(
            isBottomSheet
                ? 'sheet.background'
                : 'utils.navigation_bar.background',
          ),
          actions: actions,
          bottom: bottom,
          excludeHeaderSemantics: true,
        );
}
