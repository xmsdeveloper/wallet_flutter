// @dart=2.9
part of 'utils.dart';

// ignore: camel_case_types
class NoDataView extends StatefulWidget {
  final String imgPath;
  final String tips;
  final int topHeight;

  const NoDataView({Key key, this.imgPath, this.tips, this.topHeight})
      : super(key: key);

  @override
  State<NoDataView> createState() => _NoDataViewState();
}

class _NoDataViewState extends State<NoDataView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(
            top: ScreenUtil().setHeight(widget.topHeight),
          ),
          width: ScreenUtil().setWidth(103.16),
          height: ScreenUtil().setHeight(106.36),
          child: Image.asset(
            widget.imgPath,
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: ScreenUtil().setHeight(30),
          ),
          child: Text(
            widget.tips,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(14),
              color: Color.fromRGBO(175, 175, 175, 1),
            ),
          ),
        ),
      ],
    );
  }
}
