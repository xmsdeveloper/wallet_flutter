// @dart=2.9
part of 'utils.dart';

abstract class ViewController extends StatelessWidget {
  final NavigationBar navigationBar;
  final Widget bottomBar;
  final List<Widget> footerBtns;

  ViewController({
    this.navigationBar,
    this.bottomBar,
    this.footerBtns,
  });

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor:
            ThemeUtils().getColor('utils.view_controller.background'),
        appBar: navigationBar,
        persistentFooterButtons: footerBtns,
        bottomNavigationBar:
            null == bottomBar ? null : SafeArea(child: bottomBar),
        body: render(context),
      );

  Widget render(BuildContext context);
}
