// @dart=2.9
// import 'package:flutter/material.dart';

// class NumChangeWidget extends StatefulWidget {
//   final double height;
//   int num;
//   final ValueChanged<int> onValueChanged;

//   NumChangeWidget(
//       {Key key, this.height = 36.0, this.num = 0, this.onValueChanged})
//       : super(key: key);

//   @override
//   _NumChangeWidgetState createState() {
//     return _NumChangeWidgetState();
//   }
// }

// class _NumChangeWidgetState extends State<NumChangeWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: widget.height,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.all(Radius.circular(2.0)),
//         border: Border.all(color: Colors.grey, width: 0.5),
//       ),
//       child: Row(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           GestureDetector(
//             onTap: _minusNum,
//             child: Container(
//               width: 32.0,
//               alignment: Alignment.center,
//               child: Icon(
//                 Icons.remove,
//               ), // 设计图
//             ),
//           ),
//           Container(
//             width: 0.5,
//             color: Colors.grey,
//           ),
//           Container(
//             width: 62.0,
//             alignment: Alignment.center,
//             child: Text(
//               widget.num.toString(),
//               maxLines: 1,
//               style: TextStyle(fontSize: 20.0, color: Colors.black),
//             ),
//           ),
//           Container(
//             width: 0.5,
//             color: Colors.grey,
//           ),
//           GestureDetector(
//             onTap: _addNum,
//             child: Container(
//               width: 32.0,
//               alignment: Alignment.center,
//               child: Icon(Icons.add), // 设计图
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   void _minusNum() {
//     if (widget.num == 0) {
//       return;
//     }

//     setState(() {
//       widget.num -= 1;

//       if (widget.onValueChanged != null) {
//         widget.onValueChanged(widget.num);
//       }
//     });
//   }

//   void _addNum() {
//     setState(() {
//       widget.num += 1;

//       if (widget.onValueChanged != null) {
//         widget.onValueChanged(widget.num);
//       }
//     });
//   }
// }
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/generated/l10n.dart';

//faith 2020年04月20日11:05:57
class NumberControllerWidget extends StatefulWidget {
  //高度
  final double height;
  //输入框的宽度 总体宽度为自适应
  final double width;
  //按钮的宽度
  final double iconWidth;
  //默认输入框显示的数量
  final String numText;
  //最大1的数量
  final String maxNum;
  //点击加号回调 数量
  final ValueChanged addValueChanged;
  //点击减号回调 数量
  final ValueChanged removeValueChanged;
  //点击减号任意一个回调 数量
  final ValueChanged updateValueChanged;

  NumberControllerWidget({
    this.height = 30,
    this.width = 40,
    this.iconWidth = 40,
    this.numText = '1',
    this.addValueChanged,
    this.removeValueChanged,
    this.updateValueChanged,
    this.maxNum,
  });
  @override
  _NumberControllerWidgetState createState() => _NumberControllerWidgetState();
}

class _NumberControllerWidgetState extends State<NumberControllerWidget> {
  var textController = new TextEditingController();
  final FocusNode inputFocusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.textController.text = widget.numText;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          height: widget.height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              border: Border.all(width: 1, color: Colors.black12)),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              //减号
              coustomIconButton(
                icon: Icons.remove,
                isAdd: false,
              ),
              //输入框
              Container(
                width: widget.width,
                decoration: BoxDecoration(
                    border: Border(
                        left: BorderSide(width: 1, color: Colors.black12),
                        right: BorderSide(width: 1, color: Colors.black12))),
                child: TextFormField(
                  focusNode: inputFocusNode,
                  controller: textController,
                  autofocus: false,
                  onChanged: (_) => setState(() {
                    if (widget.updateValueChanged != null)
                      widget.updateValueChanged(textController.text);
                  }),
                  style: ThemeUtils()
                      .getTextStyle(
                        'utils.input.textstyle.content',
                      )
                      .copyWith(fontSize: 15),
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  enableInteractiveSelection: true,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.only(left: 0, top: 2, bottom: 2, right: 0),
                    border: const OutlineInputBorder(
                      gapPadding: 0,
                      borderSide: BorderSide(
                        width: 0,
                        style: BorderStyle.none,
                      ),
                    ),
                  ),
                ),
                //  TextField(
                //   controller: textController,
                //   keyboardType: TextInputType.number,
                //   textAlign: TextAlign.center,
                //   style: TextStyle(fontSize: 12),
                //   enableInteractiveSelection: false,
                //   decoration: InputDecoration(
                //     contentPadding:
                //         EdgeInsets.only(left: 0, top: 2, bottom: 2, right: 0),
                //     border: const OutlineInputBorder(
                //       gapPadding: 0,
                //       borderSide: BorderSide(
                //         width: 0,
                //         style: BorderStyle.none,
                //       ),
                //     ),
                //   ),
                // ),
              ),
              //加号
              coustomIconButton(icon: Icons.add, isAdd: true),
            ],
          ),
        )
      ],
    );
  }

  Widget coustomIconButton({IconData icon, bool isAdd}) {
    return Container(
      width: widget.iconWidth,
      child: IconButton(
        padding: EdgeInsets.all(0),
        icon: Icon(
          icon,
          size: ScreenUtil().setSp(16),
        ),
        onPressed: () {
          var num = int.parse(textController.text);
          if (!isAdd && num == 1) return;
          if (isAdd && num >= int.parse(widget.maxNum)) return;
          if (isAdd) {
            num++;
            if (widget.addValueChanged != null) widget.addValueChanged(num);
          } else {
            num--;
            if (widget.removeValueChanged != null)
              widget.removeValueChanged(num);
          }
          textController.text = '$num';
          if (widget.updateValueChanged != null) widget.updateValueChanged(num);
        },
      ),
    );
  }
}
