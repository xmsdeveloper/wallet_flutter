// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:provider/provider.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/command/language.dart';
import 'package:wallet_flutter/engine/engine.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/views/begin/splash.dart';
import 'package:wallet_flutter/services/ethereum.dart';

/// 导入网络
import 'package:wallet_flutter/command/deeplink/network_import.dart';

typedef EngineStarter = Function({
  List<LocalizationsDelegate> localizationDelegates,
});

void engineStart({
  List<LocalizationsDelegate> localizationDelegates = const [],
}) async {
  // WidgetsFlutterBinding.ensureInitialized();
  // await FlutterDownloader.initialize(
  //     //表示是否在控制台显示调试信息
  //     // debug: true,
  //     );

  await Hive.initFlutter();

  /// 私钥钱包等核心数据
  await CoreProvider.init();

  /// 用户缓存数据
  await DataSourceProvider.init();

  // / 交易记录
  // await HistoryService.init();

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ),
  );

  final MainRouterParser mainRouterParser = MainRouterParser();
  final MainRouterDelegate mainRouterDelegate = MainRouterDelegate();

  //////////////////////////////////////////////////
  // 语言切换设置
  //////////////////////////////////////////////////
  final languageCode = ConfigStorageUtils().read<String>(
    ConfigKey.LanguageCode,
  );

  if (languageCode == null) {
    switch (Engine.language) {
      case "en":
        CurrentLocaleLisetnable().value =
            Locale.fromSubtags(languageCode: "en");
        ConfigStorageUtils().write(ConfigKey.LanguageCode, "en");
        break;
      case "zh":
        CurrentLocaleLisetnable().value =
            Locale.fromSubtags(languageCode: "zh");
        ConfigStorageUtils().write(ConfigKey.LanguageCode, "zh");
        break;
      default:
        Locale.fromSubtags(languageCode: "zh");
        ConfigStorageUtils().write(ConfigKey.LanguageCode, "zh");
    }
    // CurrentLocaleLisetnable().value = Locale.fromSubtags(languageCode: "zh");
  } else {
    CurrentLocaleLisetnable().value =
        Locale.fromSubtags(languageCode: languageCode);
  }

  // CurrentLocaleLisetnable().addListener(() async {
  //   await S.load(CurrentLocaleLisetnable().value);
  //   ConfigStorageUtils().write<String>(
  //     ConfigKey.LanguageCode,
  //     CurrentLocaleLisetnable().value.languageCode,
  //   );
  // });
  //////////////////////////////////////////////////
  //////////////////////////////////////////////////

  runApp(
    MultiProvider(
      providers: [
        ListenableProvider<CoreProvider>.value(value: CoreProvider())
      ],
      builder: (context, child) => MaterialApp.router(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          S.delegate
        ]..addAll(localizationDelegates),
        locale: CurrentLocaleLisetnable().value,
        supportedLocales: S.delegate.supportedLocales,
        theme: ThemeData(
          primaryColor: Colors.blue,
          primarySwatch: Colors.blue,
          backgroundColor: Colors.white,
        ),
        routeInformationParser: mainRouterParser,
        routerDelegate: mainRouterDelegate,
      ),
    ),
  );
}

class MainRouterDelegate extends RouterDelegate<RouteSettings>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<RouteSettings> {
  final GlobalKey<NavigatorState> _navigatorStateKey =
      GlobalKey<NavigatorState>();

  RouteSettings _currentRouteSettings;

  @override
  GlobalKey<NavigatorState> get navigatorKey => _navigatorStateKey;

  @override
  RouteSettings get currentConfiguration => _currentRouteSettings;

  @override
  Widget build(BuildContext context) => Navigator(
        key: _navigatorStateKey,
        onPopPage: (route, result) => route.didPop(result) ? true : false,
        onGenerateInitialRoutes: (navigator, initialRoute) => [
          ViewAnimations.viewFadeIn(
            SplashView(),
          ),
        ],
      );

  @override
  Future<void> setNewRoutePath(RouteSettings settings) async {
    switch (settings.name) {

      /// lg: limo://limowallet.io/network/import?sourceURL=http://xxx.xxx.xxx/networkname.json
      case '/network/import':
        {
          showModalBottomSheet(
            backgroundColor: Colors.transparent,
            isScrollControlled: true,
            enableDrag: false,
            context: _navigatorStateKey.currentContext,
            builder: (context) => NetworkImport(settings),
          );
        }
        break;
    }
    return;
  }
}

class MainRouterParser extends RouteInformationParser<RouteSettings> {
  @override
  Future<RouteSettings> parseRouteInformation(
    RouteInformation routeInformation,
  ) async {
    List<String> splited = routeInformation.location.split('?');

    if (splited.length < 2) {
      return RouteSettings(name: routeInformation.location);
    } else {
      final arguments = {};

      splited[1].split('&').forEach((element) {
        final parmasPair = element.split('=');

        arguments[parmasPair.first] =
            parmasPair.length < 2 ? '' : parmasPair.last;
      });

      print(arguments.toString());

      return RouteSettings(
        name: splited[0],
        arguments: arguments,
      );
    }
  }

  @override
  RouteInformation restoreRouteInformation(RouteSettings configuration) {
    String location = configuration.name;

    if (configuration.arguments != null && configuration.arguments is Map) {
      final Map<String, String> arguments = configuration.arguments;

      location = location +
          '?' +
          arguments.keys.map((k) => '$k=${arguments[k]}').join('&');
    }

    return RouteInformation(location: location);
  }
}
