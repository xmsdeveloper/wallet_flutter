// @dart=2.9
library qytechnology.package.wallet.views.chaincore.engine;

export 'package:wallet_flutter/engine/engine.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/command/language.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/engine/_booter.dart';
import 'package:wallet_flutter/services/data_source_service/service.dart';
import 'package:wallet_flutter/views/chaincore/bridge/bridge.dart';

////////////////////////////////////////////////////////////////////////////////
/// EthereumCore
////////////////////////////////////////////////////////////////////////////////
import 'package:wallet_flutter/views/chaincore/ethereum.dart' as EthereumCore;
import 'package:flutter_screen_orientation/flutter_screen_orientation.dart';
import 'package:wallet_flutter/views/chaincore/ethereum/setting/graphics/graphics_verify.dart';

typedef ModuleBuilder = Module Function(
  BuildContext context,
  Map arguments,
);

typedef ModuleGroupBuilder = Widget Function(
  BuildContext context,
  List<Module> modules,
);

class Module {
  final BottomNavigationBarItem barItem;
  final Widget body;

  const Module({
    this.barItem,
    @required this.body,
  });
}

class Engine extends StatefulWidget {
  static Engine _instance;
  static final Map<String, ModuleBuilder> moduleMapping = {};
  static EngineStarter start = engineStart;
  static WidgetBuilder splashBuilder;
  static WidgetBuilder welcomeBuilder;
  static String url;
  static String install;
  static String language;
  static String picture;

  static String pictures;

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_EngineState>().restartApp();
  }

  Engine._() {
    // 通用资产模块
    moduleMapping['Ethereum.CoreModule.Asset'] =
        (BuildContext context, Map arguments) => Module(
              barItem: EthereumCore.AssetView.barItem(),
              body: EthereumCore.AssetView(),
            );

    // 通用Dapp模块
    moduleMapping['Ethereum.CoreModule.Dapp'] =
        (BuildContext context, Map arguments) => Module(
              barItem: EthereumCore.DappsView.barItem(),
              body: EthereumCore.DappsView(),
            );

    // 通用设置模块
    moduleMapping['Ethereum.CoreModule.Setting'] =
        (BuildContext context, Map arguments) => Module(
              barItem: EthereumCore.SettingView.barItem(),
              body: EthereumCore.SettingView(),
            );

    // 中继跨链桥模块
    moduleMapping['Ethereum.CoreModule.Bridge'] =
        (BuildContext context, Map arguments) => Module(
              barItem: EthereumCore.BridgeView.barItem(),
              body: BridgeView(),
            );
  }

  factory Engine() {
    if (_instance == null) {
      _instance = Engine._();
    }
    return _instance;
  }

  static void register(String moduleName, ModuleBuilder builder) {
    assert(
        !moduleMapping.containsKey(moduleName), '$moduleName Already Registed');
    moduleMapping[moduleName] = builder;
  }

  static List<Module> createModules(
          BuildContext context, List<String> modules) =>
      modules
          .map((moduleName) => moduleMapping[moduleName](context, null))
          .toList();

  @override
  State<StatefulWidget> createState() => _EngineState();
}

class _EngineState extends State<Engine> with WidgetsBindingObserver {
  GlobalKey _currentChainCoreKey;

  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  List<dynamic> correctResult;
  DataSourceProvider dataSourceProvider;
  _gusture() async {
    await DataSourceProvider(walletIdentifier: '', networkIdentifier: '')
        .localService
        .gustureLocals()
        .then((value) {
      correctResult = value;
    });
    dataSourceProvider =
        DataSourceProvider(walletIdentifier: '', networkIdentifier: '');

    if (correctResult.length > 0) {
      showDialog(
        context: context,
        builder: (context) {
          return WillPopScope(
              child: VerifyPage(
                dataSourceProvider: this.dataSourceProvider,
              ),
              onWillPop: () async => false);
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _gusture();

    ConfigStorageUtils()
        .writeUpdatePromptIndexOfKey(ConfigKey.UpdatePrompt, true);
    // ModuleRegistry().createModules(
    //   context,
    //   CoreProvider.of(context).networkConfig.modules,
    // );
    // print(context.findAncestorStateOfType<_EngineState>());

    Future.delayed(Duration(milliseconds: 1000), () {
      FlutterScreenOrientation.instance().init();
      FlutterScreenOrientation.instance().listenerOrientation((e) {
        if (e == FlutterScreenOrientation.portraitUp) {
          SystemChrome.setPreferredOrientations(
              [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
        } else if (e == FlutterScreenOrientation.portraitDown) {
        } else if (e == FlutterScreenOrientation.landscapeLeft) {
          // 强制横屏
          SystemChrome.setPreferredOrientations([
            DeviceOrientation.landscapeLeft,
            DeviceOrientation.landscapeRight
          ]);
        } else if (e == FlutterScreenOrientation.landscapeRight) {
          // 强制横屏
          SystemChrome.setPreferredOrientations([
            DeviceOrientation.landscapeLeft,
            DeviceOrientation.landscapeRight
          ]);
        }
      });
    });
  }

  /// 移出组件中注册的观察者
  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  /// 当应用生命周期发生变化时 , 会回调该方法
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);

    print("当前的应用生命周期状态 : ${state}");

    if (state == AppLifecycleState.paused) {
      print("应用进入后台 paused");
    } else if (state == AppLifecycleState.resumed) {
      _gusture();
      print("应用进入前台 resumed");
    } else if (state == AppLifecycleState.inactive) {
      // 应用进入非活动状态 , 如来了个电话 , 电话应用进入前台
      // 本应用进入该状态
      print("应用进入非活动状态 inactive");
    } else if (state == AppLifecycleState.detached) {
      // 应用程序仍然在 Flutter 引擎上运行 , 但是与宿主 View 组件分离
      print("应用进入 detached 状态 detached");
    }
  }

  @override
  Widget build(BuildContext context) => KeyedSubtree(
      key: key,
      child: ValueListenableBuilder(
        valueListenable: CurrentLocaleLisetnable(),
        builder: (context, config, child) => ValueListenableBuilder(
          valueListenable: CoreProvider.of(context).networkConfigListenable,
          builder: (context, config, child) => ValueListenableBuilder(
            valueListenable: CoreProvider.of(context).endPointListenable,
            builder: (context, endpoint, child) {
              _currentChainCoreKey = GlobalKey();
              switch (config.chainCore) {
                case ChainCore.ethreum:
                  return EthereumCore.IndexTabView(
                    key: _currentChainCoreKey,
                  );
                  break;
                case ChainCore.tron:
                  return EthereumCore.IndexTabView(
                    key: _currentChainCoreKey,
                  );

                default:
                  throw UnsupportedError(
                    'Unsupported ${config.chainCore.toString()}',
                  );
              }
            },
          ),
        ),
      ));
}
