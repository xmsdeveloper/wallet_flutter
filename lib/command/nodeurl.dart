//@dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:wallet_flutter/core/core.dart';

class NodeurlLisetnable extends ValueNotifier<EndPoint> {
  static NodeurlLisetnable _instance;

  NodeurlLisetnable._() : super(null);

  factory NodeurlLisetnable() {
    if (_instance == null) {
      _instance = NodeurlLisetnable._();
    }
    return _instance;
  }
}
