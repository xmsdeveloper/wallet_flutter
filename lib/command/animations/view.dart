// @dart=2.9
import 'package:flutter/material.dart';

class ViewAnimations {
  static PageRouteBuilder<T> pushFadeIn<T>(
    Widget widget, {
    String routeName,
  }) =>
      PageRouteBuilder(
          settings: RouteSettings(name: routeName),
          pageBuilder: (context, animation, secondaryAnimation) => widget,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            print('animation:$animation');
            print('secondaryAnimation:$secondaryAnimation');

            return SlideTransition(
              position: secondaryAnimation.drive(
                Tween(begin: Offset.zero, end: Offset(1.0, 0.0)).chain(
                  CurveTween(
                    curve: Curves.ease,
                  ),
                ),
              ),
              child: SlideTransition(
                position: animation.drive(
                  Tween(begin: Offset(1.0, 0.0), end: Offset.zero).chain(
                    CurveTween(
                      curve: Curves.ease,
                    ),
                  ),
                ),
                child: child,
              ),
            );
          });

  static PageRouteBuilder<T> viewFadeIn<T>(Widget widget, {String routeName}) =>
      PageRouteBuilder(
        settings: RouteSettings(name: routeName),
        pageBuilder: (context, animation, secondaryAnimation) => widget,
        transitionsBuilder: (context, animation, secondaryAnimation, child) =>
            FadeTransition(
          opacity: animation.drive(
            Tween(begin: 0.3, end: 1.0).chain(CurveTween(
              curve: Curves.easeIn,
            )),
          ),
          child: child,
        ),
      );

  static PageRouteBuilder<T> viewRightIn<T>(Widget widget) =>
      _createTransitionRoute(
        Tween(begin: Offset(1.0, 0.0), end: Offset.zero),
        widget,
      );

  static PageRouteBuilder<T> viewLeftIn<T>(Widget widget) =>
      _createTransitionRoute(
        Tween(begin: Offset(-1.0, 0.0), end: Offset.zero),
        widget,
      );

  static PageRouteBuilder<T> viewTopIn<T>(Widget widget) =>
      _createTransitionRoute(
        Tween(begin: Offset(0.0, -1.0), end: Offset.zero),
        widget,
      );

  static PageRouteBuilder<T> viewBottomIn<T>(Widget widget) =>
      _createTransitionRoute(
        Tween(begin: Offset(0.0, 1.0), end: Offset.zero),
        widget,
      );

  static PageRouteBuilder<T> _createTransitionRoute<T>(
          Tween<Offset> tween, Widget widget) =>
      PageRouteBuilder<T>(
        pageBuilder: (context, animation, secondaryAnimation) => widget,
        transitionsBuilder: (context, animation, secondaryAnimation, child) =>
            SlideTransition(
          position: animation.drive(
            tween.chain(
              CurveTween(
                curve: Curves.ease,
              ),
            ),
          ),
          child: child,
        ),
      );
}
