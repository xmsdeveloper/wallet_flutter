// @dart=2.9
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screen_util.dart';

class ThemeUtils {
  static ThemeUtils _instance;

  Map<String, dynamic> _colorsMapping;

  ThemeUtils._();

  factory ThemeUtils() {
    assert(
      _instance != null,
      '\nEnsure to initialize ColorUtils before accessing it.\nPlease execute the init method : ColorUtils.init()',
    );
    return _instance;
  }

  static Future<void> init({String themeName = 'default'}) async {
    _instance ??= ThemeUtils._();

    String content =
        await rootBundle.loadString('configuration/themes/$themeName.json');

    _instance._colorsMapping = json.decode(content);
  }

  TextStyle getTextStyle(String keypath, {double alpha = 1.0}) {
    dynamic node = _element(_colorsMapping, keypath);

    if (node is Map && node.keys.toSet().containsAll(['size', 'color'])) {
      final weight = node.containsKey('weight')
          ? node['weight'].toString().toLowerCase()
          : null;

      FontWeight fontWeight = FontWeight.normal;
      switch (weight) {
        case 'thin':
        case '100':
        case 'w100':
          fontWeight = FontWeight.w100;
          break;

        case 'extra-light':
        case '200':
        case 'w200':
          fontWeight = FontWeight.w200;
          break;

        case 'light':
        case '300':
        case 'w300':
          fontWeight = FontWeight.w300;
          break;

        case 'normal':
        case 'regular':
        case 'plain':
        case '400':
        case 'w400':
          fontWeight = FontWeight.w400;
          break;

        case 'medium':
        case '500':
        case 'w500':
          fontWeight = FontWeight.w500;
          break;

        case 'semi-bold':
        case '600':
        case 'w600':
          fontWeight = FontWeight.w600;
          break;

        case 'bold':
        case '700':
        case 'w700':
          fontWeight = FontWeight.w700;
          break;

        case 'extra-bold':
        case '800':
        case 'w800':
          fontWeight = FontWeight.w800;
          break;

        case 'black':
        case '900':
        case 'w900':
          fontWeight = FontWeight.w900;
          break;
      }

      final style = TextStyle(
        color: _elementToColor(node, 'color', alpha: alpha),
        fontSize: ScreenUtil().setSp(
          double.parse(
            _element(node, 'size').toString(),
          ),
        ),
        fontWeight: fontWeight,
      );

      return style;
    }

    throw '"$keypath" is not the text style describe.';
  }

  dynamic _element(dynamic root, String keypath) {
    dynamic node = root;
    keypath.split('.').forEach((element) {
      if (node is Map && (node).containsKey(element)) {
        node = node[element];
      }
    });

    return node;
  }

  Color _elementToColor(dynamic root, String keypath, {double alpha = 1.0}) {
    dynamic node = _element(root, keypath);
    if (node is! String) {
      throw '"$keypath" is not a complete color mark, or the corresponding mark cannot be found.';
    } else {
      final hexStringColor = (node as String).toUpperCase().replaceAll("#", "");
      return Color(int.parse(
          '0x${(0xff * alpha).toInt().toRadixString(16)}$hexStringColor'));
    }
  }

  Color getColor(String keypath, {double alpha = 1.0}) =>
      _elementToColor(_colorsMapping, keypath, alpha: alpha);
}
