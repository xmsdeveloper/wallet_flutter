import 'package:flutter/material.dart';
//字体图标

const IconData walletname = IconData(
  0xe6a4,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData touxiang = IconData(
  0xe680,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData siyao = IconData(
  0xe674,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData keystore = IconData(
  0xe674,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData manage_your_wallet = IconData(
  0xe620,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData backup_wallet = IconData(
  0xe619,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData change_password = IconData(
  0xe616,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData network = IconData(
  0xe618,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData language = IconData(
  0xe615,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData clean = IconData(
  0xe622,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);

const IconData system = IconData(
  0xe621,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData about = IconData(
  0xe614,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData notice = IconData(
  0xe625,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData into = IconData(
  0xe624,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData fail = IconData(
  0xe626,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData contract = IconData(
  0xe627,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData out = IconData(
  0xe628,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData add = IconData(
  0xe638,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData import = IconData(
  0xe63a,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData see = IconData(
  0xe63b,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData menu = IconData(
  0xe63e,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData wallet = IconData(
  0xe63f,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData finde = IconData(
  0xe640,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData edit = IconData(
  0xe641,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData file = IconData(
  0xe642,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData seeKey = IconData(
  0xe635,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData seeKeyStores = IconData(
  0xe636,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData seeWalletName = IconData(
  0xe637,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData delet = IconData(
  0xe609,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData fingerprintIcon = IconData(
  0xe658,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
const IconData gestureIcon = IconData(
  0xe65a,
  fontFamily: "myIcon",
  fontPackage: "wallet_flutter",
);
