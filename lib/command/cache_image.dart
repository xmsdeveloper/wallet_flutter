// @dart=2.9
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class CachedImage extends StatefulWidget {
  final String url;
  final Image placeholder;
  final double width;
  final double height;
  final int imageCacheHeight;
  final int imageCacheWidth;
  final Color color;
  final BoxFit fit;

  CachedImage({
    @required this.url,
    @required this.placeholder,
    this.width,
    this.height,
    this.imageCacheHeight,
    this.imageCacheWidth,
    this.color,
    this.fit,
  });

  factory CachedImage.assetNetwork({
    Image placeholder,
    String url,
    double width,
    double height,
    int imageCacheHeight,
    int imageCacheWidth,
    Color color,
    BoxFit fit,
  }) =>
      CachedImage(
        placeholder: placeholder,
        url: url,
        width: width,
        height: height,
        imageCacheHeight: imageCacheHeight,
        imageCacheWidth: imageCacheWidth,
        color: color,
        fit: fit,
      );

  static void remov(String url) {
    DefaultCacheManager().removeFile(url);
  }

  @override
  State<StatefulWidget> createState() => _CachedImageState();
}

class _CachedImageState extends State<CachedImage> {
  final Completer<Image> _getCacheCompleter = Completer();

  void remove() {}
  @override
  void initState() {
    super.initState();
    //若url不正确，比如不是正确的地址和null
    try {
      /// 直接检查缓存
      DefaultCacheManager().getFileFromCache(widget.url).then(
        (cacheFile) {
          if (cacheFile == null) {
            /// 无缓存, 下载后缓存
            final cacheStream = DefaultCacheManager().getImageFile(
              widget.url,
              maxHeight: widget.imageCacheHeight,
              maxWidth: widget.imageCacheWidth,
            );
            cacheStream.first.then(
              (cache) async {
                final fileInfo = await DefaultCacheManager().getFileFromCache(
                  cache.originalUrl,
                );

                _getCacheCompleter.complete(
                  Image.file(
                    fileInfo.file,
                    color: widget.color,
                    fit: widget.fit,
                  ),
                );
              },
            );
          }

          /// 有缓存直接返回,不判断过期，图片仅在手动清楚缓存后重新下载。
          else {
            _getCacheCompleter.complete(
              Image.file(
                cacheFile.file,
                color: widget.color,
                // fit: BoxFit.fill,
                fit: widget.fit,
              ),
            );
          }
        },
      );
    } catch (e) {
      _getCacheCompleter.complete(null);
    }
  }

  @override
  Widget build(BuildContext context) => FutureBuilder<Image>(
        future: _getCacheCompleter.future,
        builder: (context, snapshot) => SizedBox(
          width: widget.width,
          height: widget.height,
          child: snapshot.connectionState != ConnectionState.done
              ? widget.placeholder
              : (snapshot.data ?? widget.placeholder),
        ),
      );
}
