//@dart=2.9
import 'package:flutter/cupertino.dart';

class CurrentLocaleLisetnable extends ValueNotifier<Locale> {
  static CurrentLocaleLisetnable _instance;

  CurrentLocaleLisetnable._() : super(null);

  factory CurrentLocaleLisetnable() {
    if (_instance == null) {
      _instance = CurrentLocaleLisetnable._();
    }
    return _instance;
  }
}
