//@dart=2.9

import 'dart:async';
import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:wallet_flutter/command/command.dart';
import 'package:wallet_flutter/command/theme/theme_utils.dart';
import 'package:wallet_flutter/core/core.dart';
import 'package:wallet_flutter/generated/l10n.dart';
import 'package:wallet_flutter/views/utils/utils.dart';

class NetworkImport extends StatefulWidget {
  final RouteSettings settings;

  NetworkImport(this.settings);

  @override
  State<StatefulWidget> createState() => _NetworkImprotState();
}

class _NetworkImprotState extends State<NetworkImport> {
  Completer<NetworkConfig> getConfigFromHttpServices = Completer();

  bool agreeDisclaimers = false;

  @override
  void initState() {
    super.initState();

    /// 读取配置文件
    http.get((widget.settings.arguments as Map)['url']).then((rsp) {
      getConfigFromHttpServices.complete(
        NetworkConfig.fromMap(
          json.decode(rsp.body),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /// NavigationBar
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
              child: NavigationBar(
                title: S.current.deeplink_network_import_title,
                isBottomSheet: true,
              ),
            ),

            /// Header

            /// Content
            FutureBuilder<NetworkConfig>(
              future: this.getConfigFromHttpServices.future,
              builder: (context, snapshot) => snapshot.connectionState !=
                      ConnectionState.done
                  ? Container()
                  : Container(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
                      width: double.infinity,
                      color: ThemeUtils().getColor(
                        'sheet.background',
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: CachedImage(
                              url: snapshot.data.logoURL,
                              placeholder: Image.asset(
                                'imgs/eth.png',
                                package: 'wallet_flutter',
                              ),
                              width: ScreenUtil().setWidth(40),
                              height: ScreenUtil().setWidth(40),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setWidth(20),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              S.current.deeplink_network_import_tips,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setWidth(20),
                          ),
                          _FieldCell(
                            S.current.deeplink_network_import_network_name,
                            snapshot.data.markName,
                          ),
                          _FieldCell(
                            S.current.deeplink_network_import_network_symbol,
                            snapshot.data.mainSymbol,
                          ),
                          _FieldCell(
                            S.current.deeplink_network_import_network_decimals,
                            snapshot.data.mainDecimals.toString(),
                          ),
                          _FieldCell(
                            S.current.deeplink_network_import_network_scan,
                            snapshot.data.endpoints.first.scanURL,
                          ),
                          SizedBox(
                            height: ScreenUtil().setWidth(20),
                          ),
                          GestureDetector(
                            child: Row(
                              children: [
                                Icon(
                                  agreeDisclaimers
                                      ? Icons.check_box
                                      : Icons.check_box_outline_blank_rounded,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  S.current
                                      .deeplink_network_import_network_disclaimers_tips,
                                ),
                              ],
                            ),
                            onTap: () {
                              setState(() {
                                agreeDisclaimers = !agreeDisclaimers;
                              });
                            },
                          ),
                          SizedBox(
                            height: ScreenUtil().setWidth(10),
                          ),
                          StatusButton.withStyle(
                            StatusButtonStyle.Warning,
                            title: S.current
                                .deeplink_network_import_network_disclaimers_agree,
                            enable: agreeDisclaimers,
                            onPressedFuture: () async {
                              NetworkStorageUtils()
                                  .putNetwork(
                                      snapshot.data.identifier, snapshot.data)
                                  .catchError((error) {
                                if (error ==
                                    NetworkUtilsError.ConfigAlreadyExist) {
                                  Fluttertoast.showToast(
                                    msg: S.current
                                        .deeplink_network_import_network_already_exist,
                                  );
                                }
                              }).then(
                                (value) => Navigator.of(context).pop(),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
            ),
          ],
        ),
      );
}

class _FieldCell extends StatelessWidget {
  final String title;
  final String subTitleDesc;
  final String subTitle;
  final bool isAddress;
  final String detail;

  const _FieldCell(
    this.title,
    this.subTitle, {
    this.subTitleDesc,
    this.isAddress = false,
    this.detail,
  });

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setHeight(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: ThemeUtils().getTextStyle(
                'views.chaincore.ethereum.dapp.textstyle.field_title',
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            if (subTitleDesc != null && subTitleDesc.length > 0 && !isAddress)
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    subTitle,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.dapp.textstyle.field_content',
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Text(
                    subTitleDesc,
                    style: ThemeUtils().getTextStyle(
                      'views.chaincore.ethereum.dapp.textstyle.field_detail',
                    ),
                  )
                ],
              )
            else
              Text(
                subTitle,
                style: ThemeUtils().getTextStyle(
                  isAddress
                      ? 'views.chaincore.ethereum.dapp.textstyle.field_content_long'
                      : 'views.chaincore.ethereum.dapp.textstyle.field_content',
                ),
              ),
            if (detail != null && detail.length > 0)
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
            if (detail != null && detail.length > 0)
              Text(
                detail,
                style: ThemeUtils().getTextStyle(
                  'views.chaincore.ethereum.dapp.textstyle.field_detail',
                ),
              ),
          ],
        ),
      );
}
