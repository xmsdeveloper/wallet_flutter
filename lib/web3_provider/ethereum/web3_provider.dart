// @dart=2.9
library qytechnology.package.wallet.web3_provider.ethereum;

import 'dart:core';
import 'dart:typed_data';

import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:wallet_core_flutter/providers/ethereum.dart'
    as EthereumProvider;
import 'package:wallet_core_flutter/providers/ethereum.dart';
import 'package:wallet_core_flutter/utils/utils.dart';

part 'client.dart';

part 'src/sync_information.dart';
part 'src/block_number.dart';
