// @dart=2.9
part of 'web3_provider.dart';

class Web3ClientProvider with ChangeNotifier {
  static const BlockNum _defaultBlock = BlockNum.current();

  final EthereumProvider.RPCProvider _rpcProvider;

  Web3ClientProvider._(EthereumProvider.RPCProvider provider)
      : _rpcProvider = provider;

  factory Web3ClientProvider.fromHttp({@required String url}) =>
      Web3ClientProvider._(
        EthereumProvider.RPCProviderHttp(url),
      );

  factory Web3ClientProvider.fromWebSocket({@required String url}) =>
      Web3ClientProvider._(
        EthereumProvider.RPCProviderWebSocket(url),
      );

  factory Web3ClientProvider.of(BuildContext context) =>
      Provider.of<Web3ClientProvider>(context, listen: false);

  String _getBlockParam(BlockNum block) {
    return (block ?? _defaultBlock).toBlockParam();
  }

  /// Returns the version of the client we're sending requests to.
  Future<String> getClientVersion() {
    return _rpcProvider.sendRequest('web3_clientVersion');
  }

  /// Returns the id of the network the client is currently connected to.
  Future<int> getNetworkId() {
    return _rpcProvider.sendRequest<String>('net_version').then(int.parse);
  }

  Future<int> getChainID() {
    return _rpcProvider.sendRequest<String>('eth_chainId').then(int.parse);
  }

  /// Returns true if the node is actively listening for network connections.
  Future<bool> isListeningForNetwork() {
    return _rpcProvider.sendRequest('net_listening');
  }

  /// Returns the amount of Ethereum nodes currently connected to the client.
  Future<int> getPeerCount() async {
    final hex = await _rpcProvider.sendRequest<String>('net_peerCount');
    return hexToInt(hex).toInt();
  }

  /// Returns the version of the Ethereum-protocol the client is using.
  Future<int> getEtherProtocolVersion() async {
    final hex = await _rpcProvider.sendRequest<String>('eth_protocolVersion');
    return hexToInt(hex).toInt();
  }

  /// Returns an object indicating whether the node is currently synchronising
  /// with its network.
  ///
  /// If so, progress information is returned via [SyncInformation].
  Future<SyncInformation> getSyncStatus() async {
    final data = await _rpcProvider.sendRequest<dynamic>('eth_syncing');

    if (data is Map) {
      final startingBlock = hexToInt(data['startingBlock'] as String).toInt();
      final currentBlock = hexToInt(data['currentBlock'] as String).toInt();
      final highestBlock = hexToInt(data['highestBlock'] as String).toInt();

      return SyncInformation._(startingBlock, currentBlock, highestBlock);
    } else {
      return SyncInformation._(null, null, null);
    }
  }

  Future<String> coinbaseAddress() async {
    return await _rpcProvider.sendRequest<String>('eth_coinbase');
  }

  /// Returns true if the connected client is currently mining, false if not.
  Future<bool> isMining() {
    return _rpcProvider.sendRequest('eth_mining');
  }

  /// Returns the amount of hashes per second the connected node is mining with.
  Future<int> getMiningHashrate() {
    return _rpcProvider
        .sendRequest<String>('eth_hashrate')
        .then((s) => hexToInt(s).toInt());
  }

  /// Returns the amount of Ether typically needed to pay for one unit of gas.
  ///
  /// Although not strictly defined, this value will typically be a sensible
  /// amount to use.
  Future<BigInt> getGasPrice() async {
    return hexToInt(await _rpcProvider.sendRequest<String>('eth_gasPrice'));
  }

  /// Returns the number of the most recent block on the chain.
  Future<BigInt> getBlockNumber() {
    return _rpcProvider
        .sendRequest<String>('eth_blockNumber')
        .then((s) => hexToInt(s));
  }

  /// Gets the balance of the account with the specified address.
  ///
  /// This function allows specifying a custom block mined in the past to get
  /// historical data. By default, [BlockNum.current] will be used.
  Future<BigInt> getBalance(String address, {BlockNum atBlock}) {
    final blockParam = _getBlockParam(atBlock);
    return _rpcProvider.sendRequest<String>(
        'eth_getBalance', [address, blockParam]).then((data) {
      return hexToInt(data);
    });
  }

  /// Gets an element from the storage of the contract with the specified
  /// [address] at the specified [position].
  /// See https://github.com/ethereum/wiki/wiki/JSON-RPC#eth_getstorageat for
  /// more details.
  /// This function allows specifying a custom block mined in the past to get
  /// historical data. By default, [BlockNum.current] will be used.
  Future<Uint8List> getStorage(String address, BigInt position,
      {BlockNum atBlock}) {
    final blockParam = _getBlockParam(atBlock);

    return _rpcProvider.sendRequest<String>('eth_getStorageAt', [
      address,
      '0x${position.toRadixString(16)}',
      blockParam
    ]).then(hexToBytes);
  }

  /// Gets the amount of transactions issued by the specified [address].
  ///
  /// This function allows specifying a custom block mined in the past to get
  /// historical data. By default, [BlockNum.current] will be used.
  Future<int> getTransactionCount(String address, {BlockNum atBlock}) {
    final blockParam = _getBlockParam(atBlock);

    return _rpcProvider.sendRequest<String>('eth_getTransactionCount',
        [address, blockParam]).then((hex) => hexToInt(hex).toInt());
  }

  /// Returns the information about a transaction requested by transaction hash
  /// [transactionHash].
  Future<Map<String, dynamic>> getTransactionByHash(String transactionHash) {
    return _rpcProvider.sendRequest<Map<String, dynamic>>(
        'eth_getTransactionByHash', [transactionHash]);
  }

  /// Returns an receipt of a transaction based on its hash.
  Future<Map<String, dynamic>> getTransactionReceipt(String hash) {
    return _rpcProvider
        .sendRequest<Map<String, dynamic>>('eth_getTransactionReceipt', [hash]);
  }

  /// Gets the code of a contract at the specified [address]
  ///
  /// This function allows specifying a custom block mined in the past to get
  /// historical data. By default, [BlockNum.current] will be used.
  Future<Uint8List> getCode(String address, {BlockNum atBlock}) {
    return _rpcProvider.sendRequest<String>(
        'eth_getCode', [address, _getBlockParam(atBlock)]).then(hexToBytes);
  }

  /// Returns all logs matched by the filter in [options].
  ///
  /// See also:
  ///  - [events], which can be used to obtain a stream of log events
  ///  - https://github.com/ethereum/wiki/wiki/JSON-RPC#eth_getlogs
  // Future<List<FilterEvent>> getLogs(FilterOptions options) {
  //   final filter = _EventFilter(options);
  //   return _rpcProvider.sendRequest<List<dynamic>>(
  //       'eth_getLogs', [filter._createParamsObject(true)]).then((logs) {
  //     return logs?.map(filter.parseChanges)?.toList();
  //   });
  // }

  /// Signs the given transaction using the keys supplied in the [cred]
  /// object to upload it to the client so that it can be executed.
  ///
  /// Returns a hash of the transaction which, after the transaction has been
  /// included in a mined block, can be used to obtain detailed information
  /// about the transaction.
  Future<String> sendRawTransaction(String rawTransaction) async {
    return _rpcProvider.sendRequest('eth_sendRawTransaction', [rawTransaction]);
  }

  /// Calls a [function] defined in the smart [contract] and returns it's
  /// result.
  ///
  /// The connected node must be able to calculate the result locally, which
  /// means that the call can't write any data to the blockchain. Doing that
  /// would require a transaction which can be sent via [sendTransaction].
  /// As no data will be written, you can use the [sender] to specify any
  /// Ethereum address that would call that function. To use the address of a
  /// credential, call [Credentials.extractAddress].
  ///
  /// This function allows specifying a custom block mined in the past to get
  /// historical data. By default, [BlockNum.current] will be used.
  Future<String> call({
    @required String from,
    @required String to,
    @required Uint8List data,
    BigInt value,
    BlockNum atBlock,
  }) =>
      callRaw(
        from: from,
        to: to,
        data: data,
        value: value,
        atBlock: atBlock,
      );

  /// Estimate the amount of gas that would be necessary if the transaction was
  /// sent via [sendTransaction]. Note that the estimate may be significantly
  /// higher than the amount of gas actually used by the transaction.
  Future<BigInt> estimateGas({
    @required String from,
    @required String to,
    @required Uint8List data,
    BigInt value,
  }) async {
    final amountHex = await _rpcProvider.sendRequest<String>(
      'eth_estimateGas',
      [
        {
          if (from != null) 'from': from,
          if (to != null) 'to': to,
          if (data != null) 'data': bytesToHex(data, include0x: true),
          if (value != null && value > BigInt.zero) 'value': intToHex(value),
        },
      ],
    );
    return hexToInt(amountHex);
  }

  /// Sends a raw method call to a smart contract.
  ///
  /// The connected node must be able to calculate the result locally, which
  /// means that the call can't write any data to the blockchain. Doing that
  /// would require a transaction which can be sent via [sendTransaction].
  /// As no data will be written, you can use the [sender] to specify any
  /// Ethereum address that would call that function. To use the address of a
  /// credential, call [Credentials.extractAddress].
  ///
  /// This function allows specifying a custom block mined in the past to get
  /// historical data. By default, [BlockNum.current] will be used.
  ///
  /// See also:
  /// - [call], which automatically encodes function parameters and parses a
  /// response.
  Future<String> callRaw({
    @required String from,
    @required String to,
    @required Uint8List data,
    BigInt value,
    BlockNum atBlock,
  }) {
    final call = {
      'from': from,
      'to': to,
      'data': bytesToHex(data, include0x: true, padToEvenLength: true),
      if (value != null)
        'value': bytesToHex(intToBytes(value), include0x: true),
    };

    return _rpcProvider
        .sendRequest<String>('eth_call', [call, _getBlockParam(atBlock)]);
  }

  Future<bool> isBlackAddress(String address) {
    final isBlackAddress = ContractFunction(
      'isBlackAddress',
      [FunctionParameter(null, AddressType())],
      outputs: [FunctionParameter(null, BoolType())],
      type: ContractFunctionType.function,
      mutability: StateMutability.view,
    );

    final blackContract = "0x000000000000000000000000000000000000000D";

    return this
        .call(
          from: blackContract,
          to: blackContract,
          data: isBlackAddress.encodeCall([address]),
        )
        .then(isBlackAddress.decodeReturnValues)
        .then((outputs) => outputs.first as bool)
        .catchError((error) {
      return false;
    });
  }

  /// Listens for new blocks that are added to the chain. The stream will emit
  /// the hexadecimal hash of the block after it has been added.
  ///
  /// {@template web3dart:filter_streams_behavior}
  /// The stream can only be listened to once. The subscription must be disposed
  /// properly when no longer used. Failing to do so causes a memory leak in
  /// your application and uses unnecessary resources on the connected node.
  /// {@endtemplate}
  /// See also:
  /// - [hexToBytes] and [hexToInt], which can transform hex strings into a byte
  /// or integer representation.
  // Stream<String> addedBlocks() {
  //   return _filters.addFilter(_NewBlockFilter());
  // }

  /// Listens for pending transactions as they are received by the connected
  /// node. The stream will emit the hexadecimal hash of the pending
  /// transaction.
  ///
  /// {@macro web3dart:filter_streams_behavior}
  /// See also:
  /// - [hexToBytes] and [hexToInt], which can transform hex strings into a byte
  /// or integer representation.
  // Stream<String> pendingTransactions() {
  //   return _filters.addFilter(_PendingTransactionsFilter());
  // }

  /// Listens for logs emitted from transactions. The [options] can be used to
  /// apply additional filters.
  ///
  /// {@macro web3dart:filter_streams_behavior}
  /// See also:
  /// - https://solidity.readthedocs.io/en/develop/contracts.html#events, which
  /// explains more about how events are encoded.
  // Stream<FilterEvent> events(FilterOptions options) {
  //   return _filters.addFilter(_EventFilter(options));
  // }

  /// Closes resources managed by this client, such as the optional background
  /// isolate for calculations and managed streams.
  // Future<void> dispose() async {
  //   await _operations.stop();
  //   await _filters.dispose();
  //   await _streamRpcPeer?.close();
  // }
}
