// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "backbuttom_name": MessageLookupByLibrary.simpleMessage("Cancel"),
        "button_delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "button_next": MessageLookupByLibrary.simpleMessage("Next step"),
        "common_lately": MessageLookupByLibrary.simpleMessage("Lately"),
        "common_private_key":
            MessageLookupByLibrary.simpleMessage("The private key"),
        "copy_success": MessageLookupByLibrary.simpleMessage("Copied"),
        "create_title": MessageLookupByLibrary.simpleMessage("Create"),
        "create_wallet_backup_mnemonics_Wallet_address":
            MessageLookupByLibrary.simpleMessage("Wallet address"),
        "create_wallet_backup_mnemonics_confirm":
            MessageLookupByLibrary.simpleMessage(
                "Verify that the backup is complete"),
        "create_wallet_backup_mnemonics_tips1":
            MessageLookupByLibrary.simpleMessage(
                "·  Do not disclose mnemonics to anyone"),
        "create_wallet_backup_mnemonics_tips2":
            MessageLookupByLibrary.simpleMessage(
                "·  Once mnemonics are lost, assets cannot be recovered"),
        "create_wallet_backup_mnemonics_tips3":
            MessageLookupByLibrary.simpleMessage(
                "·  Please do not backup and save by screen capture or network transmission"),
        "create_wallet_backup_mnemonics_tips4":
            MessageLookupByLibrary.simpleMessage(
                "·  In any case, please do not uninstall the wallet app easily"),
        "create_wallet_backup_mnemonics_tips5":
            MessageLookupByLibrary.simpleMessage(
                "Please use paper and pen to copy mnemonics correctly in order and keep them properly."),
        "create_wallet_backup_mnemonics_tips6":
            MessageLookupByLibrary.simpleMessage("Remember:"),
        "create_wallet_backup_mnemonics_title":
            MessageLookupByLibrary.simpleMessage("Backup Mnemonics"),
        "create_wallet_batch_address_name":
            MessageLookupByLibrary.simpleMessage("Please enter account name"),
        "create_wallet_batch_fifty":
            MessageLookupByLibrary.simpleMessage("Request 50 addresses"),
        "create_wallet_batch_five":
            MessageLookupByLibrary.simpleMessage("Request 5 addresses"),
        "create_wallet_batch_hundred":
            MessageLookupByLibrary.simpleMessage("Request 100 addresses"),
        "create_wallet_batch_input": MessageLookupByLibrary.simpleMessage(
            "Please enter the remarks (account name)"),
        "create_wallet_batch_loading":
            MessageLookupByLibrary.simpleMessage("Building in progress..."),
        "create_wallet_batch_one":
            MessageLookupByLibrary.simpleMessage("Request 1 address"),
        "create_wallet_batch_ten":
            MessageLookupByLibrary.simpleMessage("Request 10 addresses"),
        "create_wallet_batch_title":
            MessageLookupByLibrary.simpleMessage("Request New Address"),
        "create_wallet_confirm_mnemonics_click_prompt":
            MessageLookupByLibrary.simpleMessage(
                "Please click mnemonics in order to confirm your correct backup"),
        "create_wallet_confirm_mnemonics_confirm":
            MessageLookupByLibrary.simpleMessage("Confirm"),
        "create_wallet_confirm_mnemonics_confirm_prompt_fail":
            MessageLookupByLibrary.simpleMessage(
                "Mnemonic error, please re select"),
        "create_wallet_confirm_mnemonics_confirm_prompt_success":
            MessageLookupByLibrary.simpleMessage(
                "Check in correctly and enter the wallet"),
        "create_wallet_confirm_mnemonics_title":
            MessageLookupByLibrary.simpleMessage("Confirmation Mnemonics"),
        "create_wallet_name":
            MessageLookupByLibrary.simpleMessage("Wallet Name"),
        "create_wallet_name_empty": MessageLookupByLibrary.simpleMessage(
            "*wallet name must be entered"),
        "create_wallet_name_placeholder": MessageLookupByLibrary.simpleMessage(
            "please enter the wallet name."),
        "create_wallet_pwd":
            MessageLookupByLibrary.simpleMessage("Set Password"),
        "create_wallet_pwd_confirm":
            MessageLookupByLibrary.simpleMessage("Confirm Password"),
        "create_wallet_pwd_confirm_different":
            MessageLookupByLibrary.simpleMessage(
                "*passwords you entered are inconsistent."),
        "create_wallet_pwd_confirm_empty": MessageLookupByLibrary.simpleMessage(
            "*The confirm password cannot be empty"),
        "create_wallet_pwd_confirm_placeholder":
            MessageLookupByLibrary.simpleMessage(
                "confirm the wallet password."),
        "create_wallet_pwd_empty": MessageLookupByLibrary.simpleMessage(
            "*password name must be entered"),
        "create_wallet_pwd_placeholder":
            MessageLookupByLibrary.simpleMessage("set the wallet password."),
        "create_wallet_tips": MessageLookupByLibrary.simpleMessage(
            "This password is only used as a security protection for the wallet.Hashpay does not save your password, so be sure to remember,And keep the password safe."),
        "create_wallet_tips_message":
            MessageLookupByLibrary.simpleMessage("Please tick the box above"),
        "create_wallet_tips_one": MessageLookupByLibrary.simpleMessage(
            "If I lose my mnemonic, my assets will be lost forever!"),
        "create_wallet_tips_three": MessageLookupByLibrary.simpleMessage(
            "The responsibility for keeping the mnemonic safe lies entirely with me!"),
        "create_wallet_tips_two": MessageLookupByLibrary.simpleMessage(
            "If I reveal or share my mnemonic with anyone, my assets could be stolen!"),
        "create_wallet_title": MessageLookupByLibrary.simpleMessage(
            "In the next step you will see the (12 word) mnemonic phrase that will restore your wallet, keep the following points in mind: "),
        "create_wallet_title_appbar":
            MessageLookupByLibrary.simpleMessage("Hint"),
        "deeplink_network_add":
            MessageLookupByLibrary.simpleMessage("Network management"),
        "deeplink_network_add_chain_name":
            MessageLookupByLibrary.simpleMessage("Chain name"),
        "deeplink_network_add_confirm_tips":
            MessageLookupByLibrary.simpleMessage(
                "Are you sure you want to add this DT chain?"),
        "deeplink_network_add_custom":
            MessageLookupByLibrary.simpleMessage("Add a user-defined network"),
        "deeplink_network_add_empty": MessageLookupByLibrary.simpleMessage(
            "The value of the item with * cannot be empty"),
        "deeplink_network_add_id":
            MessageLookupByLibrary.simpleMessage("Network ID"),
        "deeplink_network_add_identifier":
            MessageLookupByLibrary.simpleMessage("Chain identifier"),
        "deeplink_network_add_logo_link_address":
            MessageLookupByLibrary.simpleMessage("Logo link address"),
        "deeplink_network_add_mainSymbol":
            MessageLookupByLibrary.simpleMessage("MainSymbol"),
        "deeplink_network_add_node_server_address":
            MessageLookupByLibrary.simpleMessage("Node server address"),
        "deeplink_network_import_network_already_exist":
            MessageLookupByLibrary.simpleMessage(
                "The network already exists, please do not add it repeatedly"),
        "deeplink_network_import_network_decimals":
            MessageLookupByLibrary.simpleMessage("Accuracy"),
        "deeplink_network_import_network_disclaimers_agree":
            MessageLookupByLibrary.simpleMessage("Confirm add"),
        "deeplink_network_import_network_disclaimers_reject":
            MessageLookupByLibrary.simpleMessage("Do not add"),
        "deeplink_network_import_network_disclaimers_tips":
            MessageLookupByLibrary.simpleMessage(
                "I have read and agree to the《add network Disclaimer》"),
        "deeplink_network_import_network_name":
            MessageLookupByLibrary.simpleMessage("Network name"),
        "deeplink_network_import_network_scan":
            MessageLookupByLibrary.simpleMessage("Block browser"),
        "deeplink_network_import_network_symbol":
            MessageLookupByLibrary.simpleMessage("Symbol"),
        "deeplink_network_import_tips": MessageLookupByLibrary.simpleMessage(
            "Do you agree to add the network to your wallet?"),
        "deeplink_network_import_title":
            MessageLookupByLibrary.simpleMessage("Import network"),
        "ethereum_asset_management_hit_text":
            MessageLookupByLibrary.simpleMessage(
                "Enter token name / token ID / smart contract address"),
        "ethereum_asset_management_home_asset_balance":
            MessageLookupByLibrary.simpleMessage("balance: "),
        "ethereum_asset_management_home_asset_my_balance":
            MessageLookupByLibrary.simpleMessage("My assets"),
        "ethereum_asset_management_home_asset_title":
            MessageLookupByLibrary.simpleMessage("Home page assets"),
        "ethereum_asset_management_my_balance_assetList":
            MessageLookupByLibrary.simpleMessage("Assets list"),
        "ethereum_asset_management_my_balance_new_asset":
            MessageLookupByLibrary.simpleMessage("New assets"),
        "ethereum_asset_management_my_balance_show_ignore":
            MessageLookupByLibrary.simpleMessage("ignore"),
        "ethereum_asset_management_my_balance_show_num":
            MessageLookupByLibrary.simpleMessage(" new assets"),
        "ethereum_asset_management_search_nodata_loading":
            MessageLookupByLibrary.simpleMessage(
                "Searching on the chain, please wait"),
        "ethereum_asset_management_search_nodata_submit":
            MessageLookupByLibrary.simpleMessage("Submit Token Information"),
        "ethereum_asset_management_search_nodata_tips":
            MessageLookupByLibrary.simpleMessage(
                "No related tokens were found, you can"),
        "ethereum_asset_management_searchtype_first":
            MessageLookupByLibrary.simpleMessage("Hot"),
        "ethereum_asset_management_searchtype_second":
            MessageLookupByLibrary.simpleMessage("Searchresult"),
        "ethereum_asset_management_searchtype_third":
            MessageLookupByLibrary.simpleMessage("Token added"),
        "ethereum_assets_qrcode_save_to_album":
            MessageLookupByLibrary.simpleMessage("Save to album"),
        "ethereum_dapp_access_instructions":
            MessageLookupByLibrary.simpleMessage("Access instructions"),
        "ethereum_dapp_back": MessageLookupByLibrary.simpleMessage("Sign Out"),
        "ethereum_dapp_baritem_title":
            MessageLookupByLibrary.simpleMessage("Dapp"),
        "ethereum_dapp_brower":
            MessageLookupByLibrary.simpleMessage("Dapp Browser"),
        "ethereum_dapp_collection_collection_success":
            MessageLookupByLibrary.simpleMessage("Collection of success"),
        "ethereum_dapp_collection_create_checkout":
            MessageLookupByLibrary.simpleMessage("Please Tick DApp"),
        "ethereum_dapp_collection_create_newfile":
            MessageLookupByLibrary.simpleMessage("Create New Folder"),
        "ethereum_dapp_collection_create_newfile_name":
            MessageLookupByLibrary.simpleMessage("New Folder Name"),
        "ethereum_dapp_collection_create_not_input":
            MessageLookupByLibrary.simpleMessage(
                "Please fill in the folder name"),
        "ethereum_dapp_collection_file_message":
            MessageLookupByLibrary.simpleMessage("Folder Management"),
        "ethereum_dapp_collection_help":
            MessageLookupByLibrary.simpleMessage("How to collect DAPP?"),
        "ethereum_dapp_collection_message_dapp":
            MessageLookupByLibrary.simpleMessage("Manage My DApps"),
        "ethereum_dapp_collection_mycollection":
            MessageLookupByLibrary.simpleMessage("Collection"),
        "ethereum_dapp_collection_not_collection":
            MessageLookupByLibrary.simpleMessage("Please collect DAPP first"),
        "ethereum_dapp_collection_sheet_title_1":
            MessageLookupByLibrary.simpleMessage("Create"),
        "ethereum_dapp_collection_sheet_title_2":
            MessageLookupByLibrary.simpleMessage("Revise"),
        "ethereum_dapp_describe": MessageLookupByLibrary.simpleMessage(
            "Your use of the third-party page will be subject to your policies and regulations, as well as the user agreement and privacy policy of the third-party page. Third party links are directly and solely responsible to you"),
        "ethereum_dapp_history_nodata":
            MessageLookupByLibrary.simpleMessage("There is no usage record"),
        "ethereum_dapp_hostory_more":
            MessageLookupByLibrary.simpleMessage("More"),
        "ethereum_dapp_hostory_title":
            MessageLookupByLibrary.simpleMessage("Recently used DApps"),
        "ethereum_dapp_hot_search":
            MessageLookupByLibrary.simpleMessage("Hot search"),
        "ethereum_dapp_index_hot_search":
            MessageLookupByLibrary.simpleMessage("Hot search"),
        "ethereum_dapp_menu_browser_open":
            MessageLookupByLibrary.simpleMessage("Browser Open"),
        "ethereum_dapp_menu_browser_report":
            MessageLookupByLibrary.simpleMessage("Report"),
        "ethereum_dapp_menu_browser_tp":
            MessageLookupByLibrary.simpleMessage("TP Password"),
        "ethereum_dapp_menu_copy":
            MessageLookupByLibrary.simpleMessage("Copy Link"),
        "ethereum_dapp_menu_hot":
            MessageLookupByLibrary.simpleMessage("Popular"),
        "ethereum_dapp_menu_refresh":
            MessageLookupByLibrary.simpleMessage("Refresh"),
        "ethereum_dapp_menu_share":
            MessageLookupByLibrary.simpleMessage("Share"),
        "ethereum_dapp_search_history":
            MessageLookupByLibrary.simpleMessage("Search history"),
        "ethereum_dapp_search_loading": MessageLookupByLibrary.simpleMessage(
            "Going to a third-party website"),
        "ethereum_dapp_search_menu_bar_copy":
            MessageLookupByLibrary.simpleMessage("copy"),
        "ethereum_dapp_search_menu_bar_delete":
            MessageLookupByLibrary.simpleMessage("delete"),
        "ethereum_dapp_search_result":
            MessageLookupByLibrary.simpleMessage("Search result"),
        "ethereum_dapp_search_tips":
            MessageLookupByLibrary.simpleMessage("No DAPP found"),
        "ethereum_dapp_title": MessageLookupByLibrary.simpleMessage(
            "You are visiting a third party page"),
        "ethereum_dapp_xiala": MessageLookupByLibrary.simpleMessage(
            "Scroll down to see recently used"),
        "ethereum_home_address_selector_local":
            MessageLookupByLibrary.simpleMessage("Local Addresses"),
        "ethereum_home_address_selector_new_address":
            MessageLookupByLibrary.simpleMessage("New Address"),
        "ethereum_home_address_selector_placeholder":
            MessageLookupByLibrary.simpleMessage(
                "please enter or paste the address."),
        "ethereum_home_address_selector_recent":
            MessageLookupByLibrary.simpleMessage("Recent Addresses"),
        "ethereum_home_address_selector_title":
            MessageLookupByLibrary.simpleMessage("Select Address"),
        "ethereum_home_address_selector_unmatch":
            MessageLookupByLibrary.simpleMessage(
                "No matching results, please check the input."),
        "ethereum_home_asset_limit_warming":
            MessageLookupByLibrary.simpleMessage(
                "Home page asset addition has exceeded the limit"),
        "ethereum_home_baritem_title":
            MessageLookupByLibrary.simpleMessage("Asset"),
        "ethereum_home_detail_collection":
            MessageLookupByLibrary.simpleMessage("Collection"),
        "ethereum_home_detail_transfer":
            MessageLookupByLibrary.simpleMessage("Transfer"),
        "ethereum_home_detail_tx_state_call":
            MessageLookupByLibrary.simpleMessage("Invoke"),
        "ethereum_home_detail_tx_state_creation":
            MessageLookupByLibrary.simpleMessage("Creation"),
        "ethereum_home_detail_tx_state_failed":
            MessageLookupByLibrary.simpleMessage("Fail"),
        "ethereum_home_detail_tx_state_override":
            MessageLookupByLibrary.simpleMessage("Override"),
        "ethereum_home_detail_tx_state_succses":
            MessageLookupByLibrary.simpleMessage("Succses"),
        "ethereum_home_detail_txfilter_all":
            MessageLookupByLibrary.simpleMessage("All"),
        "ethereum_home_detail_txfilter_call":
            MessageLookupByLibrary.simpleMessage("Other"),
        "ethereum_home_detail_txfilter_in":
            MessageLookupByLibrary.simpleMessage("Receipted"),
        "ethereum_home_detail_txfilter_out":
            MessageLookupByLibrary.simpleMessage("Transfer"),
        "ethereum_home_qrcode_copy_address":
            MessageLookupByLibrary.simpleMessage("Copy address"),
        "ethereum_home_qrcode_share":
            MessageLookupByLibrary.simpleMessage("Share screenshots"),
        "ethereum_home_qrcode_tip1": MessageLookupByLibrary.simpleMessage(
            "Please do not divulge QR code"),
        "ethereum_home_qrcode_tip2": MessageLookupByLibrary.simpleMessage(
            "Click Present private key QR code"),
        "ethereum_home_qrcode_tips": MessageLookupByLibrary.simpleMessage(
            "Scan QR code to transfer in assets"),
        "ethereum_home_qrcode_title":
            MessageLookupByLibrary.simpleMessage("Collection"),
        "ethereum_home_qrcode_transfertips":
            MessageLookupByLibrary.simpleMessage(
                "Do not recharge any non chain "),
        "ethereum_home_qrcode_transfertips2":
            MessageLookupByLibrary.simpleMessage("assets to the address"),
        "ethereum_home_qrcode_wallet":
            MessageLookupByLibrary.simpleMessage("Wallet address"),
        "ethereum_home_scan_title": MessageLookupByLibrary.simpleMessage(
            "Please scan this wallet address QR code"),
        "ethereum_home_transfer_amount":
            MessageLookupByLibrary.simpleMessage("Sent Amount"),
        "ethereum_home_transfer_balance_tip":
            MessageLookupByLibrary.simpleMessage("Current balance: "),
        "ethereum_home_transfer_do_sent":
            MessageLookupByLibrary.simpleMessage("Sent"),
        "ethereum_home_transfer_fee":
            MessageLookupByLibrary.simpleMessage("Fee"),
        "ethereum_home_transfer_gas":
            MessageLookupByLibrary.simpleMessage("Gas"),
        "ethereum_home_transfer_granularity":
            MessageLookupByLibrary.simpleMessage(
                "The quantity must be a multiple of %d."),
        "ethereum_home_transfer_insufficient_handling_charges_one":
            MessageLookupByLibrary.simpleMessage("Insufficient "),
        "ethereum_home_transfer_insufficient_handling_charges_two":
            MessageLookupByLibrary.simpleMessage("Gas Fee"),
        "ethereum_home_transfer_lack_of_balance":
            MessageLookupByLibrary.simpleMessage("Lack of balance"),
        "ethereum_home_transfer_loading":
            MessageLookupByLibrary.simpleMessage("Loading..."),
        "ethereum_home_transfer_plaease_check_address_is_entered":
            MessageLookupByLibrary.simpleMessage(
                "Please check that the address is entered correctly"),
        "ethereum_home_transfer_plaease_input_adress":
            MessageLookupByLibrary.simpleMessage(
                "Please enter the transfer address"),
        "ethereum_home_transfer_plaease_input_number":
            MessageLookupByLibrary.simpleMessage(
                "Please enter the correct transfer quantity"),
        "ethereum_home_transfer_sent_all":
            MessageLookupByLibrary.simpleMessage("Send All"),
        "ethereum_nft_asset_management_hit_text":
            MessageLookupByLibrary.simpleMessage(
                "Enter smart contract address"),
        "ethereum_nft_asset_management_home_asset_my_balance":
            MessageLookupByLibrary.simpleMessage("All NFTs"),
        "ethereum_nft_asset_management_home_asset_title":
            MessageLookupByLibrary.simpleMessage("Home NFT"),
        "ethereum_nft_asset_management_nft_detail":
            MessageLookupByLibrary.simpleMessage("NFT Details"),
        "ethereum_nft_asset_management_searchtype_first":
            MessageLookupByLibrary.simpleMessage("Hot"),
        "ethereum_nft_asset_management_send_nft":
            MessageLookupByLibrary.simpleMessage("Send NFT"),
        "ethereum_nft_asset_management_transaction_record":
            MessageLookupByLibrary.simpleMessage("Transaction Record"),
        "ethereum_setting_about_us":
            MessageLookupByLibrary.simpleMessage("About us"),
        "ethereum_setting_about_us_current_version":
            MessageLookupByLibrary.simpleMessage("Current version"),
        "ethereum_setting_about_us_title":
            MessageLookupByLibrary.simpleMessage("About us"),
        "ethereum_setting_about_us_version_update":
            MessageLookupByLibrary.simpleMessage("Version update"),
        "ethereum_setting_backup_mnemonics":
            MessageLookupByLibrary.simpleMessage("Backup mnemonics"),
        "ethereum_setting_baritem_title":
            MessageLookupByLibrary.simpleMessage("My"),
        "ethereum_setting_blockchain_browser":
            MessageLookupByLibrary.simpleMessage("Blockchain browser"),
        "ethereum_setting_change_password":
            MessageLookupByLibrary.simpleMessage("Change password"),
        "ethereum_setting_change_password_confim_tips":
            MessageLookupByLibrary.simpleMessage(
                "Confirm password cannot be empty"),
        "ethereum_setting_change_password_forget_the_password":
            MessageLookupByLibrary.simpleMessage("Forget the password"),
        "ethereum_setting_change_password_incorrect":
            MessageLookupByLibrary.simpleMessage(
                "The old password is incorrect"),
        "ethereum_setting_change_password_incorrect_mnemonics":
            MessageLookupByLibrary.simpleMessage("Incorrect mnemonics"),
        "ethereum_setting_change_password_input_confim_tips":
            MessageLookupByLibrary.simpleMessage(
                "Please enter the confirmation password"),
        "ethereum_setting_change_password_input_new_tips":
            MessageLookupByLibrary.simpleMessage(
                "Please enter the new password"),
        "ethereum_setting_change_password_input_old_tips":
            MessageLookupByLibrary.simpleMessage(
                "Please enter the old password"),
        "ethereum_setting_change_password_new_tips":
            MessageLookupByLibrary.simpleMessage(
                "New password cannot be empty"),
        "ethereum_setting_change_password_not_consistent":
            MessageLookupByLibrary.simpleMessage(
                "The confirmation password is not consistent with the new password"),
        "ethereum_setting_change_password_old_tips":
            MessageLookupByLibrary.simpleMessage(
                "Old password cannot be empty"),
        "ethereum_setting_change_password_title":
            MessageLookupByLibrary.simpleMessage("Change password"),
        "ethereum_setting_change_wallet_name":
            MessageLookupByLibrary.simpleMessage("Change wallet name"),
        "ethereum_setting_clean_up_cache":
            MessageLookupByLibrary.simpleMessage("Clean up cache"),
        "ethereum_setting_d_s_server":
            MessageLookupByLibrary.simpleMessage("Data source server"),
        "ethereum_setting_gusture":
            MessageLookupByLibrary.simpleMessage("Pattern Settings"),
        "ethereum_setting_gusture_off":
            MessageLookupByLibrary.simpleMessage("Not Open"),
        "ethereum_setting_gusture_on":
            MessageLookupByLibrary.simpleMessage("Open"),
        "ethereum_setting_gusture_pattern_setting":
            MessageLookupByLibrary.simpleMessage("Pattern Setting"),
        "ethereum_setting_gusture_setting_confirm":
            MessageLookupByLibrary.simpleMessage("Confirm pattern password"),
        "ethereum_setting_gusture_setting_confirm_failed":
            MessageLookupByLibrary.simpleMessage(
                "The two drawings are inconsistent"),
        "ethereum_setting_gusture_setting_confirm_success":
            MessageLookupByLibrary.simpleMessage("Set successfully"),
        "ethereum_setting_gusture_setting_tips":
            MessageLookupByLibrary.simpleMessage("Draw pattern password"),
        "ethereum_setting_gusture_setting_title":
            MessageLookupByLibrary.simpleMessage("Pattern Setting"),
        "ethereum_setting_gusture_title":
            MessageLookupByLibrary.simpleMessage("Pattern Settings"),
        "ethereum_setting_gusture_turn_off":
            MessageLookupByLibrary.simpleMessage("Turn off pattern unlock"),
        "ethereum_setting_gusture_turn_off_tips":
            MessageLookupByLibrary.simpleMessage(
                "Are you sure you want to turn off the pattern unlock"),
        "ethereum_setting_gusture_verification_tips":
            MessageLookupByLibrary.simpleMessage("Draw a design"),
        "ethereum_setting_gusture_verification_tips_failed":
            MessageLookupByLibrary.simpleMessage("Pattern error"),
        "ethereum_setting_gusture_verification_title":
            MessageLookupByLibrary.simpleMessage("Pattern Certification"),
        "ethereum_setting_language_en_title":
            MessageLookupByLibrary.simpleMessage("English"),
        "ethereum_setting_language_settings":
            MessageLookupByLibrary.simpleMessage("Language settings"),
        "ethereum_setting_language_title":
            MessageLookupByLibrary.simpleMessage("Language Setting"),
        "ethereum_setting_language_zh_title":
            MessageLookupByLibrary.simpleMessage("Chinese(Simplified)"),
        "ethereum_setting_manage":
            MessageLookupByLibrary.simpleMessage("Manage your wallet"),
        "ethereum_setting_network_chain_iD":
            MessageLookupByLibrary.simpleMessage("Chain ID"),
        "ethereum_setting_network_information_net":
            MessageLookupByLibrary.simpleMessage("Delay"),
        "ethereum_setting_network_information_node":
            MessageLookupByLibrary.simpleMessage("Node"),
        "ethereum_setting_network_information_tips":
            MessageLookupByLibrary.simpleMessage(
                "Insecure network providers may lie about the status of the blockchain and record your network activity. Add only the networks you trust"),
        "ethereum_setting_network_information_title":
            MessageLookupByLibrary.simpleMessage("Information"),
        "ethereum_setting_network_network_terms":
            MessageLookupByLibrary.simpleMessage("Network terms"),
        "ethereum_setting_network_node":
            MessageLookupByLibrary.simpleMessage("Network node"),
        "ethereum_setting_network_node_block_height":
            MessageLookupByLibrary.simpleMessage("Block Height"),
        "ethereum_setting_network_node_fast":
            MessageLookupByLibrary.simpleMessage("fas"),
        "ethereum_setting_network_node_meesage_error":
            MessageLookupByLibrary.simpleMessage("Error"),
        "ethereum_setting_network_node_meesage_failed":
            MessageLookupByLibrary.simpleMessage(
                "Switching failed, node error"),
        "ethereum_setting_network_node_meesage_succeeses":
            MessageLookupByLibrary.simpleMessage("Node switching succeeded"),
        "ethereum_setting_network_node_recommend":
            MessageLookupByLibrary.simpleMessage("Recommended nodes"),
        "ethereum_setting_network_node_secondary":
            MessageLookupByLibrary.simpleMessage("secondary"),
        "ethereum_setting_network_node_slow":
            MessageLookupByLibrary.simpleMessage("slow"),
        "ethereum_setting_network_node_speed":
            MessageLookupByLibrary.simpleMessage("Node speed"),
        "ethereum_setting_network_node_tip": MessageLookupByLibrary.simpleMessage(
            "Block height: the higher the height value, the better the node data synchronization and stability. When the node speed is the same, it is better to choose a node with a high height value."),
        "ethereum_setting_network_settings":
            MessageLookupByLibrary.simpleMessage("Network settings"),
        "ethereum_setting_network_symbol":
            MessageLookupByLibrary.simpleMessage("Symbol"),
        "ethereum_setting_network_title":
            MessageLookupByLibrary.simpleMessage("Network settings"),
        "ethereum_setting_notice_setting":
            MessageLookupByLibrary.simpleMessage("Notice"),
        "ethereum_setting_share_scan_downloader":
            MessageLookupByLibrary.simpleMessage("Scan"),
        "ethereum_setting_share_setting":
            MessageLookupByLibrary.simpleMessage("Share"),
        "ethereum_setting_share_tips": MessageLookupByLibrary.simpleMessage(
            "Ethernet one-stop digital asset Wallet"),
        "ethereum_setting_title":
            MessageLookupByLibrary.simpleMessage("Setting"),
        "ethereum_setting_updata_buttom_title":
            MessageLookupByLibrary.simpleMessage("Update now"),
        "ethereum_setting_updata_find_title":
            MessageLookupByLibrary.simpleMessage("New Version"),
        "ethereum_setting_updata_tips":
            MessageLookupByLibrary.simpleMessage("This is the latest version"),
        "ethereum_setting_updata_title":
            MessageLookupByLibrary.simpleMessage("Update content:"),
        "ethereum_setting_update_alert_cancel":
            MessageLookupByLibrary.simpleMessage("Next Reminder"),
        "ethereum_setting_update_alert_confirm":
            MessageLookupByLibrary.simpleMessage("To Website"),
        "ethereum_setting_version_last":
            MessageLookupByLibrary.simpleMessage("New"),
        "ethereum_setting_version_number":
            MessageLookupByLibrary.simpleMessage("Version number"),
        "ethereum_setting_version_update":
            MessageLookupByLibrary.simpleMessage("New Version Update"),
        "ethereum_setting_wallet_name":
            MessageLookupByLibrary.simpleMessage("Wallet_name"),
        "ethereum_tab_bridge": MessageLookupByLibrary.simpleMessage("Bridge"),
        "ethereum_token_information_accuracy":
            MessageLookupByLibrary.simpleMessage("Accuracy"),
        "ethereum_token_information_asset_name":
            MessageLookupByLibrary.simpleMessage("Asset name"),
        "ethereum_token_information_asset_state_in_circulation":
            MessageLookupByLibrary.simpleMessage("In circulation"),
        "ethereum_token_information_asset_url":
            MessageLookupByLibrary.simpleMessage("URL"),
        "ethereum_token_information_contract_address":
            MessageLookupByLibrary.simpleMessage("Contract address"),
        "ethereum_token_information_introduce":
            MessageLookupByLibrary.simpleMessage("Project Introduction"),
        "ethereum_token_information_start_time":
            MessageLookupByLibrary.simpleMessage("Start time"),
        "ethereum_token_information_title":
            MessageLookupByLibrary.simpleMessage("Asset information"),
        "ethereum_token_information_total_issue":
            MessageLookupByLibrary.simpleMessage("Total issue"),
        "ethereum_trade_amount":
            MessageLookupByLibrary.simpleMessage("Transfer amount"),
        "ethereum_trade_available_balance":
            MessageLookupByLibrary.simpleMessage("Available balance"),
        "ethereum_trade_detail_block":
            MessageLookupByLibrary.simpleMessage("Block"),
        "ethereum_trade_detail_jump_title":
            MessageLookupByLibrary.simpleMessage(
                "Go to the block browser for details"),
        "ethereum_trade_detail_miners_fee":
            MessageLookupByLibrary.simpleMessage("Miner\'s fee"),
        "ethereum_trade_detail_receiving_address":
            MessageLookupByLibrary.simpleMessage("Receiving address"),
        "ethereum_trade_detail_sending_address":
            MessageLookupByLibrary.simpleMessage("Sending address"),
        "ethereum_trade_detail_success":
            MessageLookupByLibrary.simpleMessage("Successful trade"),
        "ethereum_trade_detail_title":
            MessageLookupByLibrary.simpleMessage("Transaction details"),
        "ethereum_trade_detail_transaction_number":
            MessageLookupByLibrary.simpleMessage("Transaction number"),
        "ethereum_trade_detail_transaction_time":
            MessageLookupByLibrary.simpleMessage("Transaction time"),
        "ethereum_trade_detail_transaction_type":
            MessageLookupByLibrary.simpleMessage("Transaction type"),
        "ethereum_trade_detail_transfer_accounts":
            MessageLookupByLibrary.simpleMessage("Transfer accounts"),
        "ethereum_trade_fingerprint_cancel":
            MessageLookupByLibrary.simpleMessage("Cancel"),
        "ethereum_trade_fingerprint_enable":
            MessageLookupByLibrary.simpleMessage(
                "Enable fingerprint verification"),
        "ethereum_trade_fingerprint_local":
            MessageLookupByLibrary.simpleMessage(
                "Please wait 30 seconds before doing this"),
        "ethereum_trade_fingerprint_not": MessageLookupByLibrary.simpleMessage(
            "The fingerprint has been cancelled or the device does not support fingerprints"),
        "ethereum_trade_fingerprint_notEnrolled":
            MessageLookupByLibrary.simpleMessage(
                "Please set fingerprint information in the mobile phone system"),
        "ethereum_trade_fingerprint_pay":
            MessageLookupByLibrary.simpleMessage("Fingerprint Payment"),
        "ethereum_trade_fingerprint_permanentlyLockedOut":
            MessageLookupByLibrary.simpleMessage(
                "Too many authentication times have been prohibited. Please go to the mobile phone system to confirm the fingerprint information."),
        "ethereum_trade_fingerprint_tip": MessageLookupByLibrary.simpleMessage(
            "Please perform fingerprint identification"),
        "ethereum_trade_fingerprint_title":
            MessageLookupByLibrary.simpleMessage("Fingerprint verification"),
        "ethereum_trade_head": MessageLookupByLibrary.simpleMessage("0verview"),
        "ethereum_trade_history_more":
            MessageLookupByLibrary.simpleMessage("View more records >"),
        "ethereum_trade_record_empty": MessageLookupByLibrary.simpleMessage(
            "There is no transaction record at present"),
        "ethereum_trade_toadress":
            MessageLookupByLibrary.simpleMessage("Transfer address"),
        "ethereum_trade_transactions": MessageLookupByLibrary.simpleMessage(
            "There is no transaction record"),
        "ethereum_utils_fee_config_board_fast":
            MessageLookupByLibrary.simpleMessage("Fast"),
        "ethereum_utils_fee_config_board_propose":
            MessageLookupByLibrary.simpleMessage("Propose"),
        "ethereum_utils_fee_config_board_slow":
            MessageLookupByLibrary.simpleMessage("Safe"),
        "ethereum_utils_fee_config_board_title":
            MessageLookupByLibrary.simpleMessage("Fee"),
        "ethereum_welcome_firstsubtitle": MessageLookupByLibrary.simpleMessage(
            "Blockchain ecological entrance, the latestThe hottest dapps are all here"),
        "ethereum_welcome_firsttitle":
            MessageLookupByLibrary.simpleMessage("One purse, the whole world"),
        "ethereum_welcome_secondsubtitle": MessageLookupByLibrary.simpleMessage(
            "Decentralised wallet, safe and assured Private key self holding, multiple encryption"),
        "ethereum_welcome_secondtitle": MessageLookupByLibrary.simpleMessage(
            "Private security, open source Wallet"),
        "ethereum_welcome_thirdsubtitle": MessageLookupByLibrary.simpleMessage(
            "Multi currency management, cross chain exchange One stop solution to your trading needs"),
        "ethereum_welcome_thirdtitle": MessageLookupByLibrary.simpleMessage(
            "Multi Chain Wallet, easy to use"),
        "flutter_totoast_againback": MessageLookupByLibrary.simpleMessage(
            "Click again to exit the application"),
        "help_appbar_title":
            MessageLookupByLibrary.simpleMessage("HashPay introduction?"),
        "help_how_collection":
            MessageLookupByLibrary.simpleMessage("How to store DApps"),
        "help_tips": MessageLookupByLibrary.simpleMessage(
            "The new wallet has a big revision in the [Discover] interface, but also added some new elements, users can use according to their own habits; In addition, dAPPS commonly used and loved in daily life can be classified into a folder for easy management and viewing, which is very similar to the operation on the mobile phone system. Let\'s explain a function of DApp collection below."),
        "help_title_1": MessageLookupByLibrary.simpleMessage(
            "1. DApp collection function:"),
        "help_title_content_1": MessageLookupByLibrary.simpleMessage(
            "1.1. Open HashPay wallet and click [Discover] to select the function of [DApp] or [Search bar] at the top. You can select  fill in the link to open it."),
        "help_title_content_2": MessageLookupByLibrary.simpleMessage(
            "1.2. After opening the link, click [Menu] in the upper right corner and select [Favorites] in the window that pops up at the bottom. After completing the operation, you can return to the [Discover] interface and see the records of favorites in my DApp."),
        "key_info_change_avatar":
            MessageLookupByLibrary.simpleMessage("Change wallet avatar"),
        "key_info_change_name":
            MessageLookupByLibrary.simpleMessage("Change name"),
        "key_info_change_name_empty":
            MessageLookupByLibrary.simpleMessage("Please enter a name"),
        "key_info_change_name_placeholder":
            MessageLookupByLibrary.simpleMessage("Give the wallet a new name"),
        "key_info_export_keystore":
            MessageLookupByLibrary.simpleMessage("Export keystore"),
        "key_info_export_privatekey":
            MessageLookupByLibrary.simpleMessage("Export private key"),
        "key_info_remove":
            MessageLookupByLibrary.simpleMessage("Remove this account"),
        "key_info_remove_tip_cancel":
            MessageLookupByLibrary.simpleMessage("Cancel"),
        "key_info_remove_tip_desc": MessageLookupByLibrary.simpleMessage(
            "Please make sure that the private key has been backed up, otherwise it cannot be retrieved after removal."),
        "key_info_remove_tip_title":
            MessageLookupByLibrary.simpleMessage("Warning"),
        "key_info_remove_tip_want_remove":
            MessageLookupByLibrary.simpleMessage("I understand"),
        "key_info_title":
            MessageLookupByLibrary.simpleMessage("Security Infomation"),
        "message_box_prompt_password_modified_successfully":
            MessageLookupByLibrary.simpleMessage(
                "Password modified successfully"),
        "message_box_prompt_password_not_yet_open":
            MessageLookupByLibrary.simpleMessage("Not yet open"),
        "message_box_prompt_password_reset_successful":
            MessageLookupByLibrary.simpleMessage("Password reset successful"),
        "message_box_prompt_password_update_tips":
            MessageLookupByLibrary.simpleMessage("This is the latest version"),
        "message_box_prompt_saved_to_phone_album":
            MessageLookupByLibrary.simpleMessage("Saved to phone album"),
        "message_tips_valid_adress": MessageLookupByLibrary.simpleMessage(
            "Please enter a valid address."),
        "net_selector_building":
            MessageLookupByLibrary.simpleMessage("Building"),
        "network_error": MessageLookupByLibrary.simpleMessage(
            "No network connection or anomalies detected"),
        "password_strength_middle":
            MessageLookupByLibrary.simpleMessage("middle"),
        "password_strength_strength":
            MessageLookupByLibrary.simpleMessage("strength"),
        "password_strength_weak": MessageLookupByLibrary.simpleMessage("weak"),
        "password_verify_faild": MessageLookupByLibrary.simpleMessage(
            "*It doesn\'t seem to be a valid password."),
        "remove_success":
            MessageLookupByLibrary.simpleMessage("removed successfully "),
        "sheet_cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "sheet_descration": MessageLookupByLibrary.simpleMessage(
            "Are you sure to clear the cache"),
        "sheet_descration_tips": MessageLookupByLibrary.simpleMessage(
            "The system will not automatically back up the imported private key. When restoring the wallet mnemonic, the private key you imported will not be restored. To ensure security, please back up the imported private key."),
        "sheet_ok": MessageLookupByLibrary.simpleMessage("OK"),
        "sheet_title": MessageLookupByLibrary.simpleMessage("confirm"),
        "sheet_verify_done": MessageLookupByLibrary.simpleMessage("Verify"),
        "sheet_verify_input_placeholder":
            MessageLookupByLibrary.simpleMessage("please input a password"),
        "sheet_verify_title":
            MessageLookupByLibrary.simpleMessage("Authentication"),
        "sheet_verify_verifing":
            MessageLookupByLibrary.simpleMessage("Verifying"),
        "sheet_wallet_name":
            MessageLookupByLibrary.simpleMessage("Modify wallet name"),
        "tron_home_asset_transfer_feelimit":
            MessageLookupByLibrary.simpleMessage("Fee Limit"),
        "version": MessageLookupByLibrary.simpleMessage("0.0.1"),
        "wallet_batch_import_edit":
            MessageLookupByLibrary.simpleMessage("Edit"),
        "wallet_batch_import_main_balance":
            MessageLookupByLibrary.simpleMessage("Main currency"),
        "wallet_batch_import_private_key":
            MessageLookupByLibrary.simpleMessage("Private key"),
        "wallet_batch_import_private_key_nums":
            MessageLookupByLibrary.simpleMessage("Number of private keys"),
        "wallet_batch_import_search":
            MessageLookupByLibrary.simpleMessage("Search query"),
        "wallet_batch_import_serial_number":
            MessageLookupByLibrary.simpleMessage("Sort"),
        "wallet_batch_import_sub_account_private_key":
            MessageLookupByLibrary.simpleMessage("Sub account private key"),
        "wallet_batch_import_succeeded":
            MessageLookupByLibrary.simpleMessage("Import succeeded"),
        "wallet_batch_import_title":
            MessageLookupByLibrary.simpleMessage("Search"),
        "wallet_delete_checkout": MessageLookupByLibrary.simpleMessage(
            "I have saved the private key"),
        "wallet_delete_message": MessageLookupByLibrary.simpleMessage(
            "Please tick, I have saved the private key"),
        "wallet_delete_tip": MessageLookupByLibrary.simpleMessage(
            "Are you sure you want to delete this wallet? It will be permanently deleted after 24 hours. Please make sure that you have saved the private key or mnemonic. After deletion, you can re-import the wallet through the private key or mnemonic, and the funds related to the wallet will not be affected."),
        "wallet_delete_title":
            MessageLookupByLibrary.simpleMessage("Delete Wallet"),
        "wallet_export_done": MessageLookupByLibrary.simpleMessage("OK"),
        "wallet_export_show_security":
            MessageLookupByLibrary.simpleMessage("OK, I know"),
        "wallet_export_this_is_your_security":
            MessageLookupByLibrary.simpleMessage(
                "Here is your mnemonic words :（Click content copy）"),
        "wallet_export_this_is_your_security_private_key":
            MessageLookupByLibrary.simpleMessage(
                "Here is your private key :（Click content copy）"),
        "wallet_export_tip0_desc": MessageLookupByLibrary.simpleMessage(
            "Use paper and pen to copy mnemonics correctly. If your mobile phone is lost, stolen or damaged, mnemonics will recover your assets."),
        "wallet_export_tip0_title":
            MessageLookupByLibrary.simpleMessage("Backup Mnemonics"),
        "wallet_export_tip1_desc": MessageLookupByLibrary.simpleMessage(
            "Keep it in a safe place with isolated network.\n\nPlease don\'t share and store mnemonics in Internet, such as email, photo album, social application, etc."),
        "wallet_export_tip1_title":
            MessageLookupByLibrary.simpleMessage("Offline Storage"),
        "wallet_export_tip2_desc": MessageLookupByLibrary.simpleMessage(
            "Screen capture sharing and storage may be collected by malware, resulting in asset loss."),
        "wallet_export_tip2_title":
            MessageLookupByLibrary.simpleMessage("No Capture"),
        "wallet_export_warrning": MessageLookupByLibrary.simpleMessage(
            "Warning: never disclose this mnemonic words. Anyone with your mnemonic words can steal any asset in your account."),
        "wallet_export_warrning_private_key": MessageLookupByLibrary.simpleMessage(
            "Warning: never disclose this private key. Anyone with your private key can steal any asset in your account."),
        "wallet_import_done": MessageLookupByLibrary.simpleMessage("Done"),
        "wallet_import_input_keystore_empty":
            MessageLookupByLibrary.simpleMessage("*please enter this item."),
        "wallet_import_input_keystore_invalid_formatter":
            MessageLookupByLibrary.simpleMessage(
                "*This does not seem to be the correct keystore file."),
        "wallet_import_input_keystore_placeholder":
            MessageLookupByLibrary.simpleMessage(
                "please paste the contents of keystore file."),
        "wallet_import_input_password":
            MessageLookupByLibrary.simpleMessage("Password"),
        "wallet_import_input_password_empty":
            MessageLookupByLibrary.simpleMessage("*please enter the password."),
        "wallet_import_input_password_placeholder":
            MessageLookupByLibrary.simpleMessage("please enter password"),
        "wallet_import_input_privatekey_empty":
            MessageLookupByLibrary.simpleMessage(
                "*Please input mnemonic words."),
        "wallet_import_input_privatekey_placeholder":
            MessageLookupByLibrary.simpleMessage(
                "Please input or paste your mnemonic words."),
        "wallet_import_input_privatekeys_empty":
            MessageLookupByLibrary.simpleMessage("*Please input privatekey."),
        "wallet_import_keystore_password_invaid":
            MessageLookupByLibrary.simpleMessage(
                "*this password cannot decrypt the keystore"),
        "wallet_import_nicke_empty": MessageLookupByLibrary.simpleMessage(
            "*Wallet Name cannot be empty."),
        "wallet_import_nicke_name":
            MessageLookupByLibrary.simpleMessage("Wallet Name"),
        "wallet_import_nicke_name_placeholder":
            MessageLookupByLibrary.simpleMessage(
                "please enter the name of the wallet."),
        "wallet_import_password":
            MessageLookupByLibrary.simpleMessage("Account password"),
        "wallet_import_password_empty":
            MessageLookupByLibrary.simpleMessage("Password cannot be empty."),
        "wallet_import_password_placeholder":
            MessageLookupByLibrary.simpleMessage(
                "please enter the password of the wallet."),
        "wallet_import_privatekey_already_exist":
            MessageLookupByLibrary.simpleMessage(
                "*This private key already exists."),
        "wallet_import_privatekey_invalid_formatter":
            MessageLookupByLibrary.simpleMessage(
                "*Please check whether the mnemonic words are filled in correctly"),
        "wallet_import_privatekey_invalid_formatter_private":
            MessageLookupByLibrary.simpleMessage(
                "*Please check whether the private key is filled in correctly"),
        "wallet_import_title":
            MessageLookupByLibrary.simpleMessage("Import Wallet"),
        "wallet_import_type_Keystore":
            MessageLookupByLibrary.simpleMessage("Keystore"),
        "wallet_import_type_Keystore_tips":
            MessageLookupByLibrary.simpleMessage(
                "Please paste or enter the private key here"),
        "wallet_import_type_mnemonic":
            MessageLookupByLibrary.simpleMessage("Mnemonic"),
        "wallet_import_type_privatekey":
            MessageLookupByLibrary.simpleMessage("PrivateKey"),
        "wallet_network_select_add":
            MessageLookupByLibrary.simpleMessage("Add Custom Network"),
        "wallet_network_select_title":
            MessageLookupByLibrary.simpleMessage("network switch"),
        "wallet_network_select_type_1":
            MessageLookupByLibrary.simpleMessage("Ethereum main network"),
        "wallet_network_select_type_2":
            MessageLookupByLibrary.simpleMessage("Ethereum Compatible Chain"),
        "wallet_network_select_type_3":
            MessageLookupByLibrary.simpleMessage("Customize"),
        "wallet_network_select_type_4":
            MessageLookupByLibrary.simpleMessage("Ethereum Layer 2 network"),
        "wallet_selector_generate":
            MessageLookupByLibrary.simpleMessage("Generate"),
        "wallet_selector_header_hd":
            MessageLookupByLibrary.simpleMessage("Generated"),
        "wallet_selector_header_import":
            MessageLookupByLibrary.simpleMessage("Imported"),
        "wallet_selector_import":
            MessageLookupByLibrary.simpleMessage("Import"),
        "wallet_selector_initial_address":
            MessageLookupByLibrary.simpleMessage("Initial Address"),
        "wallet_selector_new":
            MessageLookupByLibrary.simpleMessage("New Address"),
        "wallet_selector_new_desc": MessageLookupByLibrary.simpleMessage(
            "*After creation, make sure to make a backup. You can also import the private key of the wallet in other ways."),
        "wallet_selector_serial_number":
            MessageLookupByLibrary.simpleMessage("Serial Number"),
        "wallet_selector_shhet_create_1":
            MessageLookupByLibrary.simpleMessage("Create Wallet"),
        "wallet_selector_shhet_create_2":
            MessageLookupByLibrary.simpleMessage("Import Wallet"),
        "wallet_selector_shhet_create_3":
            MessageLookupByLibrary.simpleMessage("Watch"),
        "wallet_selector_shhet_create_see_input_tips":
            MessageLookupByLibrary.simpleMessage("Please enter address"),
        "wallet_selector_shhet_create_see_tips":
            MessageLookupByLibrary.simpleMessage(
                "The observation wallet does not need to import the private key, only the address is imported for daily viewing of account numbers, transaction records and receiving notifications。"),
        "wallet_selector_shhet_no_address":
            MessageLookupByLibrary.simpleMessage("Import Wallet"),
        "wallet_selector_shhet_title":
            MessageLookupByLibrary.simpleMessage("Select Wallet"),
        "wallet_selector_shhet_types_1":
            MessageLookupByLibrary.simpleMessage("Create"),
        "wallet_selector_shhet_types_2":
            MessageLookupByLibrary.simpleMessage("Import"),
        "wallet_selector_shhet_types_3":
            MessageLookupByLibrary.simpleMessage("Watch"),
        "wallet_selector_title":
            MessageLookupByLibrary.simpleMessage("Wallet Selector"),
        "wallet_watch_name":
            MessageLookupByLibrary.simpleMessage("Watch Wallet"),
        "welcome_create": MessageLookupByLibrary.simpleMessage("Create Wallet"),
        "welcome_create_desc": MessageLookupByLibrary.simpleMessage(
            "Create BTC/Eth/EOS/USDT/ETC wallets"),
        "welcome_import": MessageLookupByLibrary.simpleMessage("Import Wallet"),
        "welcome_import_desc": MessageLookupByLibrary.simpleMessage(
            "Support mainstream wallet mnemonics, \nprivate key, keysore import."),
        "welcome_language": MessageLookupByLibrary.simpleMessage("English")
      };
}
