// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "backbuttom_name": MessageLookupByLibrary.simpleMessage("取消"),
        "button_delete": MessageLookupByLibrary.simpleMessage("删除"),
        "button_next": MessageLookupByLibrary.simpleMessage("下一步"),
        "common_lately": MessageLookupByLibrary.simpleMessage("最近"),
        "common_private_key": MessageLookupByLibrary.simpleMessage("私钥"),
        "copy_success": MessageLookupByLibrary.simpleMessage("复制成功"),
        "create_title": MessageLookupByLibrary.simpleMessage("创建"),
        "create_wallet_backup_mnemonics_confirm":
            MessageLookupByLibrary.simpleMessage("备份完成，进行验证"),
        "create_wallet_backup_mnemonics_tips1":
            MessageLookupByLibrary.simpleMessage("·  请勿将助记词透露给任何一个人"),
        "create_wallet_backup_mnemonics_tips2":
            MessageLookupByLibrary.simpleMessage("·  助记词一旦丢失，资产将无法恢复"),
        "create_wallet_backup_mnemonics_tips3":
            MessageLookupByLibrary.simpleMessage("·  请勿通过截屏、网络传输的方式进行备份保存"),
        "create_wallet_backup_mnemonics_tips4":
            MessageLookupByLibrary.simpleMessage("·  遇到任何情况，请不要轻易卸载钱包App"),
        "create_wallet_backup_mnemonics_tips5":
            MessageLookupByLibrary.simpleMessage("请使用纸笔按顺序正确抄写助记词，妥善保管。"),
        "create_wallet_backup_mnemonics_tips6":
            MessageLookupByLibrary.simpleMessage("切记:"),
        "create_wallet_backup_mnemonics_title":
            MessageLookupByLibrary.simpleMessage("备份助记词"),
        "create_wallet_batch_address_name":
            MessageLookupByLibrary.simpleMessage("请输入账号名称"),
        "create_wallet_batch_fifty":
            MessageLookupByLibrary.simpleMessage("请求生成50个地址"),
        "create_wallet_batch_hundred":
            MessageLookupByLibrary.simpleMessage("请求生成100个地址"),
        "create_wallet_batch_input":
            MessageLookupByLibrary.simpleMessage("请输入备注内容（账号名称）"),
        "create_wallet_batch_loading":
            MessageLookupByLibrary.simpleMessage("正在生成当中......"),
        "create_wallet_batch_one":
            MessageLookupByLibrary.simpleMessage("请求生成1个地址"),
        "create_wallet_batch_ten":
            MessageLookupByLibrary.simpleMessage("请求生成10个地址"),
        "create_wallet_batch_title":
            MessageLookupByLibrary.simpleMessage("请求新地址"),
        "create_wallet_confirm_mnemonics_click_prompt":
            MessageLookupByLibrary.simpleMessage("请按顺序点击助记词，以确认您正确备份"),
        "create_wallet_confirm_mnemonics_confirm":
            MessageLookupByLibrary.simpleMessage("确认"),
        "create_wallet_confirm_mnemonics_confirm_prompt_fail":
            MessageLookupByLibrary.simpleMessage("助记词错误，请重新选入"),
        "create_wallet_confirm_mnemonics_confirm_prompt_success":
            MessageLookupByLibrary.simpleMessage("选入正确，进入钱包"),
        "create_wallet_confirm_mnemonics_title":
            MessageLookupByLibrary.simpleMessage("确认助记词"),
        "create_wallet_name": MessageLookupByLibrary.simpleMessage("钱包名称"),
        "create_wallet_name_empty":
            MessageLookupByLibrary.simpleMessage("*必须输入钱包名称"),
        "create_wallet_name_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入钱包名称 "),
        "create_wallet_pwd": MessageLookupByLibrary.simpleMessage("设置密码"),
        "create_wallet_pwd_confirm":
            MessageLookupByLibrary.simpleMessage("确认密码"),
        "create_wallet_pwd_confirm_different":
            MessageLookupByLibrary.simpleMessage("*输入的密码不一致 "),
        "create_wallet_pwd_confirm_empty":
            MessageLookupByLibrary.simpleMessage("*确认密码不能为空"),
        "create_wallet_pwd_confirm_placeholder":
            MessageLookupByLibrary.simpleMessage("确认钱包密码 "),
        "create_wallet_pwd_empty":
            MessageLookupByLibrary.simpleMessage("*必须输入密码"),
        "create_wallet_pwd_placeholder":
            MessageLookupByLibrary.simpleMessage("设置钱包密码 "),
        "create_wallet_tips": MessageLookupByLibrary.simpleMessage(
            "该密码仅作为钱包的安全保护作用。Hashpay不会保存您的密码，请您务必记得并保管好密码。"),
        "create_wallet_tips_message":
            MessageLookupByLibrary.simpleMessage("请勾选上方选择栏"),
        "create_wallet_tips_one":
            MessageLookupByLibrary.simpleMessage("如果我弄丢了助记词，我的资产将会永远丢失！"),
        "create_wallet_tips_three":
            MessageLookupByLibrary.simpleMessage("保护好助记词的安全的责任全部在于我！"),
        "create_wallet_tips_two": MessageLookupByLibrary.simpleMessage(
            "如果我向任何人透露或分享我的助记词， 我的资产可能会被盗！"),
        "create_wallet_title": MessageLookupByLibrary.simpleMessage(
            "在下一步里，您将看到可以恢复钱包的（12个 单词）助记词，请谨记以下要点： "),
        "create_wallet_title_appbar":
            MessageLookupByLibrary.simpleMessage("提示 "),
        "deeplink_network_add": MessageLookupByLibrary.simpleMessage("网络管理"),
        "deeplink_network_add_chain_name":
            MessageLookupByLibrary.simpleMessage("链名称"),
        "deeplink_network_add_confirm_tips":
            MessageLookupByLibrary.simpleMessage("你确定要增加该DT链吗？"),
        "deeplink_network_add_custom":
            MessageLookupByLibrary.simpleMessage("添加自定义网络"),
        "deeplink_network_add_empty":
            MessageLookupByLibrary.simpleMessage("带*项目的值不能为空"),
        "deeplink_network_add_id": MessageLookupByLibrary.simpleMessage("网络ID"),
        "deeplink_network_add_identifier":
            MessageLookupByLibrary.simpleMessage("链标识符"),
        "deeplink_network_add_logo_link_address":
            MessageLookupByLibrary.simpleMessage("Logo链接地址"),
        "deeplink_network_add_mainSymbol":
            MessageLookupByLibrary.simpleMessage("主币简称"),
        "deeplink_network_add_node_server_address":
            MessageLookupByLibrary.simpleMessage("节点服务器地址"),
        "deeplink_network_import_network_already_exist":
            MessageLookupByLibrary.simpleMessage("网络已经存在,请不要重复添加"),
        "deeplink_network_import_network_decimals":
            MessageLookupByLibrary.simpleMessage("准确性"),
        "deeplink_network_import_network_disclaimers_agree":
            MessageLookupByLibrary.simpleMessage("确认添加"),
        "deeplink_network_import_network_disclaimers_reject":
            MessageLookupByLibrary.simpleMessage("不要添加"),
        "deeplink_network_import_network_disclaimers_tips":
            MessageLookupByLibrary.simpleMessage("我已阅读并同意《添加网络免责声明》"),
        "deeplink_network_import_network_name":
            MessageLookupByLibrary.simpleMessage("网络名称"),
        "deeplink_network_import_network_scan":
            MessageLookupByLibrary.simpleMessage("Block Browser"),
        "deeplink_network_import_network_symbol":
            MessageLookupByLibrary.simpleMessage("符号"),
        "deeplink_network_import_tips":
            MessageLookupByLibrary.simpleMessage("你同意把网络加到你的钱包里吗？ "),
        "deeplink_network_import_title":
            MessageLookupByLibrary.simpleMessage("进入网络"),
        "ethereum_asset_management_hit_text":
            MessageLookupByLibrary.simpleMessage("输入令牌名称/令牌ID/智能合约地址"),
        "ethereum_asset_management_home_asset_balance":
            MessageLookupByLibrary.simpleMessage("余额: "),
        "ethereum_asset_management_home_asset_my_balance":
            MessageLookupByLibrary.simpleMessage("我的资产"),
        "ethereum_asset_management_home_asset_title":
            MessageLookupByLibrary.simpleMessage("首页资产"),
        "ethereum_asset_management_my_balance_assetList":
            MessageLookupByLibrary.simpleMessage("资产列表"),
        "ethereum_asset_management_my_balance_new_asset":
            MessageLookupByLibrary.simpleMessage("新增资产"),
        "ethereum_asset_management_my_balance_show_ignore":
            MessageLookupByLibrary.simpleMessage("忽略"),
        "ethereum_asset_management_my_balance_show_num":
            MessageLookupByLibrary.simpleMessage("个新的资产"),
        "ethereum_asset_management_search_nodata_loading":
            MessageLookupByLibrary.simpleMessage("链上搜索中，请等待····"),
        "ethereum_asset_management_search_nodata_submit":
            MessageLookupByLibrary.simpleMessage("提交代币信息"),
        "ethereum_asset_management_search_nodata_tips":
            MessageLookupByLibrary.simpleMessage("未搜索到相关代币，你可以"),
        "ethereum_asset_management_searchtype_first":
            MessageLookupByLibrary.simpleMessage("热门代币"),
        "ethereum_asset_management_searchtype_second":
            MessageLookupByLibrary.simpleMessage("搜索结果"),
        "ethereum_asset_management_searchtype_third":
            MessageLookupByLibrary.simpleMessage("已添加的代币"),
        "ethereum_assets_qrcode_save_to_album":
            MessageLookupByLibrary.simpleMessage("保存到相册"),
        "ethereum_dapp_access_instructions":
            MessageLookupByLibrary.simpleMessage("访问说明"),
        "ethereum_dapp_back": MessageLookupByLibrary.simpleMessage("退出"),
        "ethereum_dapp_baritem_title":
            MessageLookupByLibrary.simpleMessage("发现"),
        "ethereum_dapp_brower": MessageLookupByLibrary.simpleMessage("Dapp浏览器"),
        "ethereum_dapp_collection_collection_success":
            MessageLookupByLibrary.simpleMessage("收藏成功"),
        "ethereum_dapp_collection_create_checkout":
            MessageLookupByLibrary.simpleMessage("请勾选DApp"),
        "ethereum_dapp_collection_create_newfile":
            MessageLookupByLibrary.simpleMessage("创建新文件夹"),
        "ethereum_dapp_collection_create_newfile_name":
            MessageLookupByLibrary.simpleMessage("新文件夹名"),
        "ethereum_dapp_collection_create_not_input":
            MessageLookupByLibrary.simpleMessage("请填写文件夹名称"),
        "ethereum_dapp_collection_file_message":
            MessageLookupByLibrary.simpleMessage("文件夹管理"),
        "ethereum_dapp_collection_help":
            MessageLookupByLibrary.simpleMessage("如何收藏Dapp?"),
        "ethereum_dapp_collection_message_dapp":
            MessageLookupByLibrary.simpleMessage("管理我的DApps"),
        "ethereum_dapp_collection_mycollection":
            MessageLookupByLibrary.simpleMessage("收藏"),
        "ethereum_dapp_collection_not_collection":
            MessageLookupByLibrary.simpleMessage("请先收藏Dapp"),
        "ethereum_dapp_collection_sheet_title_1":
            MessageLookupByLibrary.simpleMessage("创建"),
        "ethereum_dapp_collection_sheet_title_2":
            MessageLookupByLibrary.simpleMessage("修改"),
        "ethereum_dapp_describe": MessageLookupByLibrary.simpleMessage(
            "你在第三方页面的使用行为将适用你所在的政策法规以及该第三方页面的《用户协议》《隐私政策》。由第三方链接直接并单独向你承担责任。"),
        "ethereum_dapp_history_nodata":
            MessageLookupByLibrary.simpleMessage("当前暂无使用记录"),
        "ethereum_dapp_hostory_more":
            MessageLookupByLibrary.simpleMessage("更多"),
        "ethereum_dapp_hostory_title":
            MessageLookupByLibrary.simpleMessage("最近使用的DApps"),
        "ethereum_dapp_hot_search": MessageLookupByLibrary.simpleMessage("热搜"),
        "ethereum_dapp_index_hot_search":
            MessageLookupByLibrary.simpleMessage("热门搜索"),
        "ethereum_dapp_menu_browser_open":
            MessageLookupByLibrary.simpleMessage("浏览器打开"),
        "ethereum_dapp_menu_browser_report":
            MessageLookupByLibrary.simpleMessage("举报"),
        "ethereum_dapp_menu_browser_tp":
            MessageLookupByLibrary.simpleMessage("TP口令"),
        "ethereum_dapp_menu_copy": MessageLookupByLibrary.simpleMessage("复制链接"),
        "ethereum_dapp_menu_hot": MessageLookupByLibrary.simpleMessage("热门推荐"),
        "ethereum_dapp_menu_refresh":
            MessageLookupByLibrary.simpleMessage("刷新"),
        "ethereum_dapp_menu_share": MessageLookupByLibrary.simpleMessage("分享"),
        "ethereum_dapp_search_history":
            MessageLookupByLibrary.simpleMessage("搜索历史"),
        "ethereum_dapp_search_loading":
            MessageLookupByLibrary.simpleMessage("正在跳转第三方网站"),
        "ethereum_dapp_search_menu_bar_copy":
            MessageLookupByLibrary.simpleMessage("复制"),
        "ethereum_dapp_search_menu_bar_delete":
            MessageLookupByLibrary.simpleMessage("删除"),
        "ethereum_dapp_search_result":
            MessageLookupByLibrary.simpleMessage("搜索结果"),
        "ethereum_dapp_search_tips":
            MessageLookupByLibrary.simpleMessage("没有发现DAPP"),
        "ethereum_dapp_title":
            MessageLookupByLibrary.simpleMessage("你正在访问第三方页面"),
        "ethereum_dapp_xiala": MessageLookupByLibrary.simpleMessage("下拉查看最近使用"),
        "ethereum_home_address_selector_local":
            MessageLookupByLibrary.simpleMessage("本地地址"),
        "ethereum_home_address_selector_new_address":
            MessageLookupByLibrary.simpleMessage("新地址"),
        "ethereum_home_address_selector_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入或粘贴地址 "),
        "ethereum_home_address_selector_recent":
            MessageLookupByLibrary.simpleMessage("最近地址"),
        "ethereum_home_address_selector_title":
            MessageLookupByLibrary.simpleMessage("选择地址"),
        "ethereum_home_address_selector_unmatch":
            MessageLookupByLibrary.simpleMessage("没有匹配结果,请检查输入 "),
        "ethereum_home_asset_limit_warming":
            MessageLookupByLibrary.simpleMessage("首页资产添加已超过限度"),
        "ethereum_home_baritem_title":
            MessageLookupByLibrary.simpleMessage("资产"),
        "ethereum_home_detail_collection":
            MessageLookupByLibrary.simpleMessage("收款"),
        "ethereum_home_detail_transfer":
            MessageLookupByLibrary.simpleMessage("转账"),
        "ethereum_home_detail_tx_state_call":
            MessageLookupByLibrary.simpleMessage("调用合约"),
        "ethereum_home_detail_tx_state_creation":
            MessageLookupByLibrary.simpleMessage("创建合约"),
        "ethereum_home_detail_tx_state_failed":
            MessageLookupByLibrary.simpleMessage("失败"),
        "ethereum_home_detail_tx_state_override":
            MessageLookupByLibrary.simpleMessage("Override"),
        "ethereum_home_detail_tx_state_succses":
            MessageLookupByLibrary.simpleMessage("成功"),
        "ethereum_home_detail_txfilter_all":
            MessageLookupByLibrary.simpleMessage("全部"),
        "ethereum_home_detail_txfilter_call":
            MessageLookupByLibrary.simpleMessage("其他"),
        "ethereum_home_detail_txfilter_in":
            MessageLookupByLibrary.simpleMessage("转入"),
        "ethereum_home_detail_txfilter_out":
            MessageLookupByLibrary.simpleMessage("转出"),
        "ethereum_home_qrcode_copy_address":
            MessageLookupByLibrary.simpleMessage("复制地址"),
        "ethereum_home_qrcode_share":
            MessageLookupByLibrary.simpleMessage("截图分享"),
        "ethereum_home_qrcode_tip1":
            MessageLookupByLibrary.simpleMessage("已确认周围无人及摄像头"),
        "ethereum_home_qrcode_tip2":
            MessageLookupByLibrary.simpleMessage("点击展示私钥二维码"),
        "ethereum_home_qrcode_tips":
            MessageLookupByLibrary.simpleMessage("扫二维码转入资产"),
        "ethereum_home_qrcode_title":
            MessageLookupByLibrary.simpleMessage("收款"),
        "ethereum_home_qrcode_transfertips":
            MessageLookupByLibrary.simpleMessage("请勿向地址充值任何非该链"),
        "ethereum_home_qrcode_transfertips2":
            MessageLookupByLibrary.simpleMessage("资产"),
        "ethereum_home_qrcode_wallet":
            MessageLookupByLibrary.simpleMessage("钱包地址"),
        "ethereum_home_scan_title":
            MessageLookupByLibrary.simpleMessage("请扫描此钱包地址二维码"),
        "ethereum_home_transfer_amount":
            MessageLookupByLibrary.simpleMessage("发送金额"),
        "ethereum_home_transfer_balance_tip":
            MessageLookupByLibrary.simpleMessage("当前余额:"),
        "ethereum_home_transfer_do_sent":
            MessageLookupByLibrary.simpleMessage("发送"),
        "ethereum_home_transfer_fee":
            MessageLookupByLibrary.simpleMessage("矿工费"),
        "ethereum_home_transfer_gas":
            MessageLookupByLibrary.simpleMessage("Gas"),
        "ethereum_home_transfer_granularity":
            MessageLookupByLibrary.simpleMessage("数量必须为%d的倍数。"),
        "ethereum_home_transfer_insufficient_handling_charges_one":
            MessageLookupByLibrary.simpleMessage(""),
        "ethereum_home_transfer_insufficient_handling_charges_two":
            MessageLookupByLibrary.simpleMessage("手续费不足"),
        "ethereum_home_transfer_lack_of_balance":
            MessageLookupByLibrary.simpleMessage("余额不足"),
        "ethereum_home_transfer_loading":
            MessageLookupByLibrary.simpleMessage("正在获取"),
        "ethereum_home_transfer_plaease_check_address_is_entered":
            MessageLookupByLibrary.simpleMessage("请检查地址是否正确输入"),
        "ethereum_home_transfer_plaease_input_adress":
            MessageLookupByLibrary.simpleMessage("请输入转账地址"),
        "ethereum_home_transfer_plaease_input_number":
            MessageLookupByLibrary.simpleMessage("请输入正确的转账数量"),
        "ethereum_home_transfer_sent_all":
            MessageLookupByLibrary.simpleMessage("发送全部"),
        "ethereum_nft_asset_management_hit_text":
            MessageLookupByLibrary.simpleMessage("请输入智能合约地址"),
        "ethereum_nft_asset_management_home_asset_my_balance":
            MessageLookupByLibrary.simpleMessage("所有NFT"),
        "ethereum_nft_asset_management_home_asset_title":
            MessageLookupByLibrary.simpleMessage("首页NFT"),
        "ethereum_nft_asset_management_nft_detail":
            MessageLookupByLibrary.simpleMessage("NFT详情"),
        "ethereum_nft_asset_management_searchtype_first":
            MessageLookupByLibrary.simpleMessage("热门代币"),
        "ethereum_nft_asset_management_send_nft":
            MessageLookupByLibrary.simpleMessage("发送NFT"),
        "ethereum_nft_asset_management_transaction_record":
            MessageLookupByLibrary.simpleMessage("交易记录"),
        "ethereum_setting_about_us":
            MessageLookupByLibrary.simpleMessage("关于我们"),
        "ethereum_setting_about_us_current_version":
            MessageLookupByLibrary.simpleMessage("当前版本"),
        "ethereum_setting_about_us_title":
            MessageLookupByLibrary.simpleMessage("关于我们"),
        "ethereum_setting_about_us_version_update":
            MessageLookupByLibrary.simpleMessage("版本更新"),
        "ethereum_setting_backup_mnemonics":
            MessageLookupByLibrary.simpleMessage("备份助记词"),
        "ethereum_setting_baritem_title":
            MessageLookupByLibrary.simpleMessage("我的"),
        "ethereum_setting_blockchain_browser":
            MessageLookupByLibrary.simpleMessage("Blockchain浏览器"),
        "ethereum_setting_change_password":
            MessageLookupByLibrary.simpleMessage("更改密码"),
        "ethereum_setting_change_password_confim_tips":
            MessageLookupByLibrary.simpleMessage("*确认密码不能为空"),
        "ethereum_setting_change_password_forget_the_password":
            MessageLookupByLibrary.simpleMessage("忘记密码"),
        "ethereum_setting_change_password_incorrect":
            MessageLookupByLibrary.simpleMessage("*旧密码不正确"),
        "ethereum_setting_change_password_incorrect_mnemonics":
            MessageLookupByLibrary.simpleMessage("*助记词不正确"),
        "ethereum_setting_change_password_input_confim_tips":
            MessageLookupByLibrary.simpleMessage("请输入确认密码"),
        "ethereum_setting_change_password_input_new_tips":
            MessageLookupByLibrary.simpleMessage("请输入新密码"),
        "ethereum_setting_change_password_input_old_tips":
            MessageLookupByLibrary.simpleMessage("请输入旧密码"),
        "ethereum_setting_change_password_new_tips":
            MessageLookupByLibrary.simpleMessage("*新密码不能为空"),
        "ethereum_setting_change_password_not_consistent":
            MessageLookupByLibrary.simpleMessage("*确认密码与新密码不一致"),
        "ethereum_setting_change_password_old_tips":
            MessageLookupByLibrary.simpleMessage("*旧密码不能为空"),
        "ethereum_setting_change_password_title":
            MessageLookupByLibrary.simpleMessage("更改密码"),
        "ethereum_setting_change_wallet_name":
            MessageLookupByLibrary.simpleMessage("更改钱包名称"),
        "ethereum_setting_clean_up_cache":
            MessageLookupByLibrary.simpleMessage("清理缓存"),
        "ethereum_setting_d_s_server":
            MessageLookupByLibrary.simpleMessage("数据源服务器"),
        "ethereum_setting_gusture":
            MessageLookupByLibrary.simpleMessage("图形解锁"),
        "ethereum_setting_gusture_off":
            MessageLookupByLibrary.simpleMessage("未开启"),
        "ethereum_setting_gusture_on":
            MessageLookupByLibrary.simpleMessage("已开启"),
        "ethereum_setting_gusture_pattern_setting":
            MessageLookupByLibrary.simpleMessage("图案设置"),
        "ethereum_setting_gusture_setting_confirm":
            MessageLookupByLibrary.simpleMessage("确认图案密码"),
        "ethereum_setting_gusture_setting_confirm_failed":
            MessageLookupByLibrary.simpleMessage("两次绘制不一致"),
        "ethereum_setting_gusture_setting_confirm_success":
            MessageLookupByLibrary.simpleMessage("设置成功"),
        "ethereum_setting_gusture_setting_tips":
            MessageLookupByLibrary.simpleMessage("绘制图案密码"),
        "ethereum_setting_gusture_setting_title":
            MessageLookupByLibrary.simpleMessage("图案设置"),
        "ethereum_setting_gusture_title":
            MessageLookupByLibrary.simpleMessage("图案解锁设置"),
        "ethereum_setting_gusture_turn_off":
            MessageLookupByLibrary.simpleMessage("关闭图案解锁"),
        "ethereum_setting_gusture_turn_off_tips":
            MessageLookupByLibrary.simpleMessage("你确定要关闭图案解锁吗？"),
        "ethereum_setting_gusture_verification_tips":
            MessageLookupByLibrary.simpleMessage("绘制图案"),
        "ethereum_setting_gusture_verification_tips_failed":
            MessageLookupByLibrary.simpleMessage("图案错误"),
        "ethereum_setting_gusture_verification_title":
            MessageLookupByLibrary.simpleMessage("图案认证"),
        "ethereum_setting_language_en_title":
            MessageLookupByLibrary.simpleMessage("英文"),
        "ethereum_setting_language_settings":
            MessageLookupByLibrary.simpleMessage("语言设置"),
        "ethereum_setting_language_title":
            MessageLookupByLibrary.simpleMessage("语言设置"),
        "ethereum_setting_language_zh_title":
            MessageLookupByLibrary.simpleMessage("中文（简体）"),
        "ethereum_setting_manage": MessageLookupByLibrary.simpleMessage("管理钱包"),
        "ethereum_setting_network_chain_iD":
            MessageLookupByLibrary.simpleMessage("链标识"),
        "ethereum_setting_network_information_net":
            MessageLookupByLibrary.simpleMessage("延迟"),
        "ethereum_setting_network_information_node":
            MessageLookupByLibrary.simpleMessage("节点"),
        "ethereum_setting_network_information_tips":
            MessageLookupByLibrary.simpleMessage(
                "不安全的网络提供商可能会谎报区块链的状态,并记录您的网络活动 只添加你信任的网络"),
        "ethereum_setting_network_information_title":
            MessageLookupByLibrary.simpleMessage("信息"),
        "ethereum_setting_network_network_terms":
            MessageLookupByLibrary.simpleMessage("网络术语"),
        "ethereum_setting_network_node":
            MessageLookupByLibrary.simpleMessage("网络节点"),
        "ethereum_setting_network_node_block_height":
            MessageLookupByLibrary.simpleMessage("区块高度"),
        "ethereum_setting_network_node_fast":
            MessageLookupByLibrary.simpleMessage("良好"),
        "ethereum_setting_network_node_meesage_error":
            MessageLookupByLibrary.simpleMessage("出错了"),
        "ethereum_setting_network_node_meesage_failed":
            MessageLookupByLibrary.simpleMessage("切换失败，节点异常"),
        "ethereum_setting_network_node_meesage_succeeses":
            MessageLookupByLibrary.simpleMessage("节点切换成功"),
        "ethereum_setting_network_node_recommend":
            MessageLookupByLibrary.simpleMessage("推荐节点"),
        "ethereum_setting_network_node_secondary":
            MessageLookupByLibrary.simpleMessage("一般"),
        "ethereum_setting_network_node_slow":
            MessageLookupByLibrary.simpleMessage("拥挤"),
        "ethereum_setting_network_node_speed":
            MessageLookupByLibrary.simpleMessage("节点速度"),
        "ethereum_setting_network_node_tip":
            MessageLookupByLibrary.simpleMessage(
                "区块高度：高度值越大，代表节点数据同步更完善，其稳定性更好。在节点速度差不多的情况下，选择高度值大的节点，体验更好。"),
        "ethereum_setting_network_settings":
            MessageLookupByLibrary.simpleMessage("网络设置"),
        "ethereum_setting_network_symbol":
            MessageLookupByLibrary.simpleMessage("符号"),
        "ethereum_setting_network_title":
            MessageLookupByLibrary.simpleMessage("网络设置"),
        "ethereum_setting_notice_setting":
            MessageLookupByLibrary.simpleMessage("公告"),
        "ethereum_setting_share_scan_downloader":
            MessageLookupByLibrary.simpleMessage("扫码立即下载"),
        "ethereum_setting_share_setting":
            MessageLookupByLibrary.simpleMessage("分享"),
        "ethereum_setting_share_tips":
            MessageLookupByLibrary.simpleMessage("以太系  一站式数字资产钱包"),
        "ethereum_setting_title": MessageLookupByLibrary.simpleMessage("设置"),
        "ethereum_setting_updata_buttom_title":
            MessageLookupByLibrary.simpleMessage("立即更新"),
        "ethereum_setting_updata_find_title":
            MessageLookupByLibrary.simpleMessage("发现新版本"),
        "ethereum_setting_updata_tips":
            MessageLookupByLibrary.simpleMessage("已是最新版本"),
        "ethereum_setting_updata_title":
            MessageLookupByLibrary.simpleMessage("更新内容:"),
        "ethereum_setting_update_alert_cancel":
            MessageLookupByLibrary.simpleMessage("下次提醒"),
        "ethereum_setting_update_alert_confirm":
            MessageLookupByLibrary.simpleMessage("前往官网更新"),
        "ethereum_setting_version_last":
            MessageLookupByLibrary.simpleMessage("最新"),
        "ethereum_setting_version_number":
            MessageLookupByLibrary.simpleMessage("版本号"),
        "ethereum_setting_version_update":
            MessageLookupByLibrary.simpleMessage("版本更新"),
        "ethereum_setting_wallet_name":
            MessageLookupByLibrary.simpleMessage("钱包名称"),
        "ethereum_tab_bridge": MessageLookupByLibrary.simpleMessage("时光桥"),
        "ethereum_token_information_accuracy":
            MessageLookupByLibrary.simpleMessage("代币精度"),
        "ethereum_token_information_asset_name":
            MessageLookupByLibrary.simpleMessage("项目名称"),
        "ethereum_token_information_asset_state_in_circulation":
            MessageLookupByLibrary.simpleMessage("流通中"),
        "ethereum_token_information_asset_url":
            MessageLookupByLibrary.simpleMessage("官网"),
        "ethereum_token_information_contract_address":
            MessageLookupByLibrary.simpleMessage("合约地址"),
        "ethereum_token_information_introduce":
            MessageLookupByLibrary.simpleMessage("项目介绍"),
        "ethereum_token_information_start_time":
            MessageLookupByLibrary.simpleMessage("开始时间"),
        "ethereum_token_information_title":
            MessageLookupByLibrary.simpleMessage("资产信息"),
        "ethereum_token_information_total_issue":
            MessageLookupByLibrary.simpleMessage("发行总量"),
        "ethereum_trade_amount": MessageLookupByLibrary.simpleMessage("转账数量"),
        "ethereum_trade_available_balance":
            MessageLookupByLibrary.simpleMessage("可用余额"),
        "ethereum_trade_detail_block":
            MessageLookupByLibrary.simpleMessage("区块"),
        "ethereum_trade_detail_jump_title":
            MessageLookupByLibrary.simpleMessage("到区块浏览器查看详细信息"),
        "ethereum_trade_detail_miners_fee":
            MessageLookupByLibrary.simpleMessage("矿工费"),
        "ethereum_trade_detail_receiving_address":
            MessageLookupByLibrary.simpleMessage("收货地址"),
        "ethereum_trade_detail_sending_address":
            MessageLookupByLibrary.simpleMessage("发送地址"),
        "ethereum_trade_detail_success":
            MessageLookupByLibrary.simpleMessage("成功的贸易"),
        "ethereum_trade_detail_title":
            MessageLookupByLibrary.simpleMessage("交易详情"),
        "ethereum_trade_detail_transaction_number":
            MessageLookupByLibrary.simpleMessage("交易编号"),
        "ethereum_trade_detail_transaction_time":
            MessageLookupByLibrary.simpleMessage("交易时间"),
        "ethereum_trade_detail_transaction_type":
            MessageLookupByLibrary.simpleMessage("交易类型"),
        "ethereum_trade_detail_transfer_accounts":
            MessageLookupByLibrary.simpleMessage("转账账户"),
        "ethereum_trade_fingerprint_cancel":
            MessageLookupByLibrary.simpleMessage("取消"),
        "ethereum_trade_fingerprint_enable":
            MessageLookupByLibrary.simpleMessage("开启指纹验证"),
        "ethereum_trade_fingerprint_local":
            MessageLookupByLibrary.simpleMessage("请等待30s后，再进行此操作"),
        "ethereum_trade_fingerprint_not":
            MessageLookupByLibrary.simpleMessage("已取消或该设备不支持指纹"),
        "ethereum_trade_fingerprint_notEnrolled":
            MessageLookupByLibrary.simpleMessage("请在手机系统设置指纹信息"),
        "ethereum_trade_fingerprint_pay":
            MessageLookupByLibrary.simpleMessage("指纹支付"),
        "ethereum_trade_fingerprint_permanentlyLockedOut":
            MessageLookupByLibrary.simpleMessage(
                "认证错误次数过多，已被锁定，请在手机手机系统认证指纹信息"),
        "ethereum_trade_fingerprint_tip":
            MessageLookupByLibrary.simpleMessage("请进行指纹识别"),
        "ethereum_trade_fingerprint_title":
            MessageLookupByLibrary.simpleMessage("指纹验证"),
        "ethereum_trade_head": MessageLookupByLibrary.simpleMessage("项目概况"),
        "ethereum_trade_history_more":
            MessageLookupByLibrary.simpleMessage("查看更多记录 >"),
        "ethereum_trade_toadress": MessageLookupByLibrary.simpleMessage("转账地址"),
        "ethereum_trade_transactions":
            MessageLookupByLibrary.simpleMessage("当前暂无交易记录"),
        "ethereum_utils_fee_config_board_fast":
            MessageLookupByLibrary.simpleMessage("快速"),
        "ethereum_utils_fee_config_board_propose":
            MessageLookupByLibrary.simpleMessage("推荐"),
        "ethereum_utils_fee_config_board_slow":
            MessageLookupByLibrary.simpleMessage("安全"),
        "ethereum_utils_fee_config_board_title":
            MessageLookupByLibrary.simpleMessage("矿工费"),
        "ethereum_welcome_firstsubtitle":
            MessageLookupByLibrary.simpleMessage("区块链生态入口,最新最热的dapp都在这里"),
        "ethereum_welcome_firsttitle":
            MessageLookupByLibrary.simpleMessage("一个钱包,整个世界"),
        "ethereum_welcome_secondsubtitle":
            MessageLookupByLibrary.simpleMessage("去中心化钱包,安全可靠,私钥自持,多重加密"),
        "ethereum_welcome_secondtitle":
            MessageLookupByLibrary.simpleMessage("私钥安全,开源代码钱包"),
        "ethereum_welcome_thirdsubtitle":
            MessageLookupByLibrary.simpleMessage("多币种管理,跨链交易,一站式解决您的交易需求"),
        "ethereum_welcome_thirdtitle":
            MessageLookupByLibrary.simpleMessage("多链钱包,使用方便"),
        "flutter_totoast_againback":
            MessageLookupByLibrary.simpleMessage("再点一次退出应用"),
        "help_appbar_title": MessageLookupByLibrary.simpleMessage("HashPay简介?"),
        "help_how_collection": MessageLookupByLibrary.simpleMessage("如何收藏DAPP"),
        "help_tips": MessageLookupByLibrary.simpleMessage(
            "新版钱包在【发现】界面中有很大的改版，同时也加入了一些全新元素，用户可以根据自己的使用习惯按需使用；还可以把日常中常用、爱用的DApp归类到一个文件夹中方便管理和查看，和手机系统上的操作非常相似，下面我们来对DApp收藏进行一个功能讲解。"),
        "help_title_1": MessageLookupByLibrary.simpleMessage("1、DApp收藏功能："),
        "help_title_content_1": MessageLookupByLibrary.simpleMessage(
            "1.1、打开HashPay钱包，点击DAPP或顶部【搜索栏】功能，填入链接打开。"),
        "help_title_content_2": MessageLookupByLibrary.simpleMessage(
            "1.2、打开链接后点击右上角【菜单】功能，底部弹出的窗口中选择【收藏】功能，完成操作后返回到【发现】界面就可以在我的DApp里看到收藏的记录。"),
        "key_info_change_avatar": MessageLookupByLibrary.simpleMessage("换钱包头像"),
        "key_info_change_name": MessageLookupByLibrary.simpleMessage("更改名称"),
        "key_info_change_name_empty":
            MessageLookupByLibrary.simpleMessage("请输入姓名"),
        "key_info_change_name_placeholder":
            MessageLookupByLibrary.simpleMessage("给钱包起个新名字"),
        "key_info_export_keystore":
            MessageLookupByLibrary.simpleMessage("导出Keystore"),
        "key_info_export_privatekey":
            MessageLookupByLibrary.simpleMessage("导出私钥"),
        "key_info_remove": MessageLookupByLibrary.simpleMessage("删除此账户"),
        "key_info_remove_tip_cancel":
            MessageLookupByLibrary.simpleMessage("取消"),
        "key_info_remove_tip_desc":
            MessageLookupByLibrary.simpleMessage("请确保私钥已备份,否则移除后无法检索 "),
        "key_info_remove_tip_title": MessageLookupByLibrary.simpleMessage("警告"),
        "key_info_remove_tip_want_remove":
            MessageLookupByLibrary.simpleMessage("我明白"),
        "key_info_title": MessageLookupByLibrary.simpleMessage("安全信息"),
        "message_box_prompt_password_modified_successfully":
            MessageLookupByLibrary.simpleMessage("密码修改成功"),
        "message_box_prompt_password_not_yet_open":
            MessageLookupByLibrary.simpleMessage("暂未开放"),
        "message_box_prompt_password_reset_successful":
            MessageLookupByLibrary.simpleMessage("密码重置成功"),
        "message_box_prompt_password_update_tips":
            MessageLookupByLibrary.simpleMessage("已是最新版本"),
        "message_box_prompt_saved_to_phone_album":
            MessageLookupByLibrary.simpleMessage("已保存到手机相册"),
        "message_tips_valid_adress":
            MessageLookupByLibrary.simpleMessage("请输入有效地址"),
        "net_selector_building": MessageLookupByLibrary.simpleMessage("搭建中"),
        "network_error": MessageLookupByLibrary.simpleMessage("检测不到网络连接或异常"),
        "password_strength_middle": MessageLookupByLibrary.simpleMessage("中"),
        "password_strength_strength": MessageLookupByLibrary.simpleMessage("强"),
        "password_strength_weak": MessageLookupByLibrary.simpleMessage("弱"),
        "password_verify_faild":
            MessageLookupByLibrary.simpleMessage("*这似乎不是一个有效的密码 "),
        "remove_success": MessageLookupByLibrary.simpleMessage("移除成功"),
        "sheet_cancel": MessageLookupByLibrary.simpleMessage("取消"),
        "sheet_descration": MessageLookupByLibrary.simpleMessage("你确定要清除缓存吗"),
        "sheet_descration_tips": MessageLookupByLibrary.simpleMessage(
            "系统不会自动备份导入的私钥。在恢复钱包助记词时，也不会恢复您导入的私钥。为了确保安全，请备份好导入的私钥。"),
        "sheet_ok": MessageLookupByLibrary.simpleMessage("确定"),
        "sheet_title": MessageLookupByLibrary.simpleMessage("确认"),
        "sheet_verify_done": MessageLookupByLibrary.simpleMessage("确认"),
        "sheet_verify_input_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入密码"),
        "sheet_verify_title": MessageLookupByLibrary.simpleMessage("密码认证"),
        "sheet_verify_verifing": MessageLookupByLibrary.simpleMessage("确认"),
        "sheet_wallet_name": MessageLookupByLibrary.simpleMessage("修改钱包名称"),
        "tron_home_asset_transfer_feelimit":
            MessageLookupByLibrary.simpleMessage("矿工费"),
        "version": MessageLookupByLibrary.simpleMessage("0.0.1"),
        "wallet_batch_import_edit": MessageLookupByLibrary.simpleMessage("编辑"),
        "wallet_batch_import_main_balance":
            MessageLookupByLibrary.simpleMessage("主币余额"),
        "wallet_batch_import_private_key":
            MessageLookupByLibrary.simpleMessage("私钥"),
        "wallet_batch_import_private_key_nums":
            MessageLookupByLibrary.simpleMessage("私钥数量"),
        "wallet_batch_import_search":
            MessageLookupByLibrary.simpleMessage("搜索查询"),
        "wallet_batch_import_serial_number":
            MessageLookupByLibrary.simpleMessage("序列"),
        "wallet_batch_import_sub_account_private_key":
            MessageLookupByLibrary.simpleMessage("子账户私钥"),
        "wallet_batch_import_succeeded":
            MessageLookupByLibrary.simpleMessage("导入成功"),
        "wallet_batch_import_title": MessageLookupByLibrary.simpleMessage("搜索"),
        "wallet_delete_checkout":
            MessageLookupByLibrary.simpleMessage("我已保存了私钥"),
        "wallet_delete_message":
            MessageLookupByLibrary.simpleMessage("请勾选，我已保存了私钥"),
        "wallet_delete_tip": MessageLookupByLibrary.simpleMessage(
            "您确定要删除这个钱包吗？它将在24小时后被永久删除。请确保您保存了私钥或助记词，删除后可通过私钥或者助记词重新导入钱包，与该钱包相关的资金不会受到影响。"),
        "wallet_delete_title": MessageLookupByLibrary.simpleMessage("删除钱包"),
        "wallet_export_done": MessageLookupByLibrary.simpleMessage("我已备份"),
        "wallet_export_show_security":
            MessageLookupByLibrary.simpleMessage("好的,我知道"),
        "wallet_export_this_is_your_security":
            MessageLookupByLibrary.simpleMessage("这是你的助记词:(点击内容复制)"),
        "wallet_export_this_is_your_security_private_key":
            MessageLookupByLibrary.simpleMessage("这是你的私钥:(点击内容复制)"),
        "wallet_export_tip0_desc": MessageLookupByLibrary.simpleMessage(
            "用纸和笔正确抄写助记词 如果您的手机丢失,被盗或损坏,助记词将收回您的资产 "),
        "wallet_export_tip0_title": MessageLookupByLibrary.simpleMessage("备份"),
        "wallet_export_tip1_desc": MessageLookupByLibrary.simpleMessage(
            "把它放在一个安全的地方,有隔离的网络请不要在互联网上分享和存储助记词,如电子邮件、相册、社交应用等 "),
        "wallet_export_tip1_title":
            MessageLookupByLibrary.simpleMessage("离线存储"),
        "wallet_export_tip2_desc":
            MessageLookupByLibrary.simpleMessage("网捕共享和存储可能被恶意软件收集,导致资产损失 "),
        "wallet_export_tip2_title": MessageLookupByLibrary.simpleMessage("未捕获"),
        "wallet_export_warrning": MessageLookupByLibrary.simpleMessage(
            "警告:永远不要泄露助记词，任何持有你的助记词的人都可以窃取你账户中的任何资产 "),
        "wallet_export_warrning_private_key":
            MessageLookupByLibrary.simpleMessage(
                "警告:永远不要泄露私钥，任何持有你的私钥的人都可以窃取你账户中的任何资产 "),
        "wallet_import_done": MessageLookupByLibrary.simpleMessage("完成"),
        "wallet_import_input_keystore_empty":
            MessageLookupByLibrary.simpleMessage("请输入这个项目 "),
        "wallet_import_input_keystore_invalid_formatter":
            MessageLookupByLibrary.simpleMessage("*这似乎不是正确的密钥存储文件 "),
        "wallet_import_input_keystore_placeholder":
            MessageLookupByLibrary.simpleMessage("请粘贴密钥存储文件的内容 "),
        "wallet_import_input_password":
            MessageLookupByLibrary.simpleMessage("密码"),
        "wallet_import_input_password_empty":
            MessageLookupByLibrary.simpleMessage("*请输入密码 "),
        "wallet_import_input_password_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入密码"),
        "wallet_import_input_privatekey_empty":
            MessageLookupByLibrary.simpleMessage("*请输入助记词 "),
        "wallet_import_input_privatekey_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入或粘贴您的助记词 "),
        "wallet_import_input_privatekeys_empty":
            MessageLookupByLibrary.simpleMessage("*请输入私钥"),
        "wallet_import_keystore_password_invaid":
            MessageLookupByLibrary.simpleMessage("*这个密码无法解密密钥存储"),
        "wallet_import_nicke_empty":
            MessageLookupByLibrary.simpleMessage("*名称不能是空的 "),
        "wallet_import_nicke_name":
            MessageLookupByLibrary.simpleMessage("钱包名称"),
        "wallet_import_nicke_name_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入钱包的名字 "),
        "wallet_import_password": MessageLookupByLibrary.simpleMessage("帐户密码"),
        "wallet_import_password_empty":
            MessageLookupByLibrary.simpleMessage("*密码不能为空 "),
        "wallet_import_password_placeholder":
            MessageLookupByLibrary.simpleMessage("请输入钱包的密码 "),
        "wallet_import_privatekey_already_exist":
            MessageLookupByLibrary.simpleMessage("*这个私钥已经存在了 "),
        "wallet_import_privatekey_invalid_formatter":
            MessageLookupByLibrary.simpleMessage("*请检查助记词是否填写正确 "),
        "wallet_import_privatekey_invalid_formatter_private":
            MessageLookupByLibrary.simpleMessage("*请检查私钥是否填写正确 "),
        "wallet_import_title": MessageLookupByLibrary.simpleMessage("导入钱包"),
        "wallet_import_type_Keystore":
            MessageLookupByLibrary.simpleMessage("Keystore"),
        "wallet_import_type_Keystore_tips":
            MessageLookupByLibrary.simpleMessage("请在此处粘贴或输入私钥"),
        "wallet_import_type_mnemonic":
            MessageLookupByLibrary.simpleMessage("助记词"),
        "wallet_import_type_privatekey":
            MessageLookupByLibrary.simpleMessage("私钥"),
        "wallet_network_select_add":
            MessageLookupByLibrary.simpleMessage("添加自定义网络"),
        "wallet_network_select_title":
            MessageLookupByLibrary.simpleMessage("网络切换"),
        "wallet_network_select_type_1":
            MessageLookupByLibrary.simpleMessage("以太坊主网络"),
        "wallet_network_select_type_2":
            MessageLookupByLibrary.simpleMessage("以太坊兼容链"),
        "wallet_network_select_type_3":
            MessageLookupByLibrary.simpleMessage("自定义"),
        "wallet_network_select_type_4":
            MessageLookupByLibrary.simpleMessage("以太坊二层网络"),
        "wallet_selector_generate": MessageLookupByLibrary.simpleMessage("生成"),
        "wallet_selector_header_hd": MessageLookupByLibrary.simpleMessage("生成"),
        "wallet_selector_header_import":
            MessageLookupByLibrary.simpleMessage("导入"),
        "wallet_selector_import": MessageLookupByLibrary.simpleMessage("导入"),
        "wallet_selector_initial_address":
            MessageLookupByLibrary.simpleMessage("初始地址"),
        "wallet_selector_new": MessageLookupByLibrary.simpleMessage("新地址"),
        "wallet_selector_new_desc":
            MessageLookupByLibrary.simpleMessage("*创建后,确保备份 还可以通过其他方式导入钱包的私钥 "),
        "wallet_selector_serial_number":
            MessageLookupByLibrary.simpleMessage("序号"),
        "wallet_selector_shhet_create_1":
            MessageLookupByLibrary.simpleMessage("创建钱包"),
        "wallet_selector_shhet_create_2":
            MessageLookupByLibrary.simpleMessage("导入钱包"),
        "wallet_selector_shhet_create_3":
            MessageLookupByLibrary.simpleMessage("观察"),
        "wallet_selector_shhet_create_see_input_tips":
            MessageLookupByLibrary.simpleMessage("请输入地址"),
        "wallet_selector_shhet_create_see_tips":
            MessageLookupByLibrary.simpleMessage(
                "观察钱包不需要导入私钥，只导入地址，进行日常查看账号、交易记录和接收通知。"),
        "wallet_selector_shhet_no_address":
            MessageLookupByLibrary.simpleMessage("导入钱包"),
        "wallet_selector_shhet_title":
            MessageLookupByLibrary.simpleMessage("选择钱包"),
        "wallet_selector_shhet_types_1":
            MessageLookupByLibrary.simpleMessage("创建"),
        "wallet_selector_shhet_types_2":
            MessageLookupByLibrary.simpleMessage("导入"),
        "wallet_selector_shhet_types_3":
            MessageLookupByLibrary.simpleMessage("观察"),
        "wallet_selector_title": MessageLookupByLibrary.simpleMessage("钱包选择器"),
        "wallet_watch_name": MessageLookupByLibrary.simpleMessage("观察钱包"),
        "welcome_create": MessageLookupByLibrary.simpleMessage("创建钱包"),
        "welcome_create_desc":
            MessageLookupByLibrary.simpleMessage("创建BTC/ETH/EOS/USDT/ETC钱包"),
        "welcome_import": MessageLookupByLibrary.simpleMessage("导入钱包"),
        "welcome_import_desc":
            MessageLookupByLibrary.simpleMessage("支持主流钱包助记器,\n私钥,keysore导入 "),
        "welcome_language": MessageLookupByLibrary.simpleMessage("中文")
      };
}
