// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `*It doesn't seem to be a valid password.`
  String get password_verify_faild {
    return Intl.message(
      '*It doesn\'t seem to be a valid password.',
      name: 'password_verify_faild',
      desc: '',
      args: [],
    );
  }

  /// `Authentication`
  String get sheet_verify_title {
    return Intl.message(
      'Authentication',
      name: 'sheet_verify_title',
      desc: '',
      args: [],
    );
  }

  /// `Verify`
  String get sheet_verify_done {
    return Intl.message(
      'Verify',
      name: 'sheet_verify_done',
      desc: '',
      args: [],
    );
  }

  /// `Verifying`
  String get sheet_verify_verifing {
    return Intl.message(
      'Verifying',
      name: 'sheet_verify_verifing',
      desc: '',
      args: [],
    );
  }

  /// `please input a password`
  String get sheet_verify_input_placeholder {
    return Intl.message(
      'please input a password',
      name: 'sheet_verify_input_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `confirm`
  String get sheet_title {
    return Intl.message(
      'confirm',
      name: 'sheet_title',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to clear the cache`
  String get sheet_descration {
    return Intl.message(
      'Are you sure to clear the cache',
      name: 'sheet_descration',
      desc: '',
      args: [],
    );
  }

  /// `The system will not automatically back up the imported private key. When restoring the wallet mnemonic, the private key you imported will not be restored. To ensure security, please back up the imported private key.`
  String get sheet_descration_tips {
    return Intl.message(
      'The system will not automatically back up the imported private key. When restoring the wallet mnemonic, the private key you imported will not be restored. To ensure security, please back up the imported private key.',
      name: 'sheet_descration_tips',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get sheet_ok {
    return Intl.message(
      'OK',
      name: 'sheet_ok',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get sheet_cancel {
    return Intl.message(
      'Cancel',
      name: 'sheet_cancel',
      desc: '',
      args: [],
    );
  }

  /// `Copied`
  String get copy_success {
    return Intl.message(
      'Copied',
      name: 'copy_success',
      desc: '',
      args: [],
    );
  }

  /// `removed successfully `
  String get remove_success {
    return Intl.message(
      'removed successfully ',
      name: 'remove_success',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get backbuttom_name {
    return Intl.message(
      'Cancel',
      name: 'backbuttom_name',
      desc: '',
      args: [],
    );
  }

  /// `Next step`
  String get button_next {
    return Intl.message(
      'Next step',
      name: 'button_next',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get button_delete {
    return Intl.message(
      'Delete',
      name: 'button_delete',
      desc: '',
      args: [],
    );
  }

  /// `Modify wallet name`
  String get sheet_wallet_name {
    return Intl.message(
      'Modify wallet name',
      name: 'sheet_wallet_name',
      desc: '',
      args: [],
    );
  }

  /// `Watch Wallet`
  String get wallet_watch_name {
    return Intl.message(
      'Watch Wallet',
      name: 'wallet_watch_name',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid address.`
  String get message_tips_valid_adress {
    return Intl.message(
      'Please enter a valid address.',
      name: 'message_tips_valid_adress',
      desc: '',
      args: [],
    );
  }

  /// `The private key`
  String get common_private_key {
    return Intl.message(
      'The private key',
      name: 'common_private_key',
      desc: '',
      args: [],
    );
  }

  /// `Lately`
  String get common_lately {
    return Intl.message(
      'Lately',
      name: 'common_lately',
      desc: '',
      args: [],
    );
  }

  /// `No network connection or anomalies detected`
  String get network_error {
    return Intl.message(
      'No network connection or anomalies detected',
      name: 'network_error',
      desc: '',
      args: [],
    );
  }

  /// `Click again to exit the application`
  String get flutter_totoast_againback {
    return Intl.message(
      'Click again to exit the application',
      name: 'flutter_totoast_againback',
      desc: '',
      args: [],
    );
  }

  /// `weak`
  String get password_strength_weak {
    return Intl.message(
      'weak',
      name: 'password_strength_weak',
      desc: '',
      args: [],
    );
  }

  /// `middle`
  String get password_strength_middle {
    return Intl.message(
      'middle',
      name: 'password_strength_middle',
      desc: '',
      args: [],
    );
  }

  /// `strength`
  String get password_strength_strength {
    return Intl.message(
      'strength',
      name: 'password_strength_strength',
      desc: '',
      args: [],
    );
  }

  /// `Saved to phone album`
  String get message_box_prompt_saved_to_phone_album {
    return Intl.message(
      'Saved to phone album',
      name: 'message_box_prompt_saved_to_phone_album',
      desc: '',
      args: [],
    );
  }

  /// `Password modified successfully`
  String get message_box_prompt_password_modified_successfully {
    return Intl.message(
      'Password modified successfully',
      name: 'message_box_prompt_password_modified_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Password reset successful`
  String get message_box_prompt_password_reset_successful {
    return Intl.message(
      'Password reset successful',
      name: 'message_box_prompt_password_reset_successful',
      desc: '',
      args: [],
    );
  }

  /// `This is the latest version`
  String get message_box_prompt_password_update_tips {
    return Intl.message(
      'This is the latest version',
      name: 'message_box_prompt_password_update_tips',
      desc: '',
      args: [],
    );
  }

  /// `Not yet open`
  String get message_box_prompt_password_not_yet_open {
    return Intl.message(
      'Not yet open',
      name: 'message_box_prompt_password_not_yet_open',
      desc: '',
      args: [],
    );
  }

  /// `Wallet Selector`
  String get wallet_selector_title {
    return Intl.message(
      'Wallet Selector',
      name: 'wallet_selector_title',
      desc: '',
      args: [],
    );
  }

  /// `New Address`
  String get wallet_selector_new {
    return Intl.message(
      'New Address',
      name: 'wallet_selector_new',
      desc: '',
      args: [],
    );
  }

  /// `*After creation, make sure to make a backup. You can also import the private key of the wallet in other ways.`
  String get wallet_selector_new_desc {
    return Intl.message(
      '*After creation, make sure to make a backup. You can also import the private key of the wallet in other ways.',
      name: 'wallet_selector_new_desc',
      desc: '',
      args: [],
    );
  }

  /// `Generate`
  String get wallet_selector_generate {
    return Intl.message(
      'Generate',
      name: 'wallet_selector_generate',
      desc: '',
      args: [],
    );
  }

  /// `Import`
  String get wallet_selector_import {
    return Intl.message(
      'Import',
      name: 'wallet_selector_import',
      desc: '',
      args: [],
    );
  }

  /// `Generated`
  String get wallet_selector_header_hd {
    return Intl.message(
      'Generated',
      name: 'wallet_selector_header_hd',
      desc: '',
      args: [],
    );
  }

  /// `Imported`
  String get wallet_selector_header_import {
    return Intl.message(
      'Imported',
      name: 'wallet_selector_header_import',
      desc: '',
      args: [],
    );
  }

  /// `Select Wallet`
  String get wallet_selector_shhet_title {
    return Intl.message(
      'Select Wallet',
      name: 'wallet_selector_shhet_title',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get wallet_selector_shhet_types_1 {
    return Intl.message(
      'Create',
      name: 'wallet_selector_shhet_types_1',
      desc: '',
      args: [],
    );
  }

  /// `Import`
  String get wallet_selector_shhet_types_2 {
    return Intl.message(
      'Import',
      name: 'wallet_selector_shhet_types_2',
      desc: '',
      args: [],
    );
  }

  /// `Watch`
  String get wallet_selector_shhet_types_3 {
    return Intl.message(
      'Watch',
      name: 'wallet_selector_shhet_types_3',
      desc: '',
      args: [],
    );
  }

  /// `Create Wallet`
  String get wallet_selector_shhet_create_1 {
    return Intl.message(
      'Create Wallet',
      name: 'wallet_selector_shhet_create_1',
      desc: '',
      args: [],
    );
  }

  /// `Import Wallet`
  String get wallet_selector_shhet_create_2 {
    return Intl.message(
      'Import Wallet',
      name: 'wallet_selector_shhet_create_2',
      desc: '',
      args: [],
    );
  }

  /// `Watch`
  String get wallet_selector_shhet_create_3 {
    return Intl.message(
      'Watch',
      name: 'wallet_selector_shhet_create_3',
      desc: '',
      args: [],
    );
  }

  /// `Please enter address`
  String get wallet_selector_shhet_create_see_input_tips {
    return Intl.message(
      'Please enter address',
      name: 'wallet_selector_shhet_create_see_input_tips',
      desc: '',
      args: [],
    );
  }

  /// `The observation wallet does not need to import the private key, only the address is imported for daily viewing of account numbers, transaction records and receiving notifications。`
  String get wallet_selector_shhet_create_see_tips {
    return Intl.message(
      'The observation wallet does not need to import the private key, only the address is imported for daily viewing of account numbers, transaction records and receiving notifications。',
      name: 'wallet_selector_shhet_create_see_tips',
      desc: '',
      args: [],
    );
  }

  /// `Import Wallet`
  String get wallet_selector_shhet_no_address {
    return Intl.message(
      'Import Wallet',
      name: 'wallet_selector_shhet_no_address',
      desc: '',
      args: [],
    );
  }

  /// `Serial Number`
  String get wallet_selector_serial_number {
    return Intl.message(
      'Serial Number',
      name: 'wallet_selector_serial_number',
      desc: '',
      args: [],
    );
  }

  /// `Initial Address`
  String get wallet_selector_initial_address {
    return Intl.message(
      'Initial Address',
      name: 'wallet_selector_initial_address',
      desc: '',
      args: [],
    );
  }

  /// `Building`
  String get net_selector_building {
    return Intl.message(
      'Building',
      name: 'net_selector_building',
      desc: '',
      args: [],
    );
  }

  /// `Security Infomation`
  String get key_info_title {
    return Intl.message(
      'Security Infomation',
      name: 'key_info_title',
      desc: '',
      args: [],
    );
  }

  /// `Change name`
  String get key_info_change_name {
    return Intl.message(
      'Change name',
      name: 'key_info_change_name',
      desc: '',
      args: [],
    );
  }

  /// `Give the wallet a new name`
  String get key_info_change_name_placeholder {
    return Intl.message(
      'Give the wallet a new name',
      name: 'key_info_change_name_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a name`
  String get key_info_change_name_empty {
    return Intl.message(
      'Please enter a name',
      name: 'key_info_change_name_empty',
      desc: '',
      args: [],
    );
  }

  /// `Change wallet avatar`
  String get key_info_change_avatar {
    return Intl.message(
      'Change wallet avatar',
      name: 'key_info_change_avatar',
      desc: '',
      args: [],
    );
  }

  /// `Export private key`
  String get key_info_export_privatekey {
    return Intl.message(
      'Export private key',
      name: 'key_info_export_privatekey',
      desc: '',
      args: [],
    );
  }

  /// `Export keystore`
  String get key_info_export_keystore {
    return Intl.message(
      'Export keystore',
      name: 'key_info_export_keystore',
      desc: '',
      args: [],
    );
  }

  /// `Remove this account`
  String get key_info_remove {
    return Intl.message(
      'Remove this account',
      name: 'key_info_remove',
      desc: '',
      args: [],
    );
  }

  /// `Warning`
  String get key_info_remove_tip_title {
    return Intl.message(
      'Warning',
      name: 'key_info_remove_tip_title',
      desc: '',
      args: [],
    );
  }

  /// `Please make sure that the private key has been backed up, otherwise it cannot be retrieved after removal.`
  String get key_info_remove_tip_desc {
    return Intl.message(
      'Please make sure that the private key has been backed up, otherwise it cannot be retrieved after removal.',
      name: 'key_info_remove_tip_desc',
      desc: '',
      args: [],
    );
  }

  /// `I understand`
  String get key_info_remove_tip_want_remove {
    return Intl.message(
      'I understand',
      name: 'key_info_remove_tip_want_remove',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get key_info_remove_tip_cancel {
    return Intl.message(
      'Cancel',
      name: 'key_info_remove_tip_cancel',
      desc: '',
      args: [],
    );
  }

  /// `Import Wallet`
  String get wallet_import_title {
    return Intl.message(
      'Import Wallet',
      name: 'wallet_import_title',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get wallet_import_done {
    return Intl.message(
      'Done',
      name: 'wallet_import_done',
      desc: '',
      args: [],
    );
  }

  /// `Wallet Name`
  String get wallet_import_nicke_name {
    return Intl.message(
      'Wallet Name',
      name: 'wallet_import_nicke_name',
      desc: '',
      args: [],
    );
  }

  /// `Account password`
  String get wallet_import_password {
    return Intl.message(
      'Account password',
      name: 'wallet_import_password',
      desc: '',
      args: [],
    );
  }

  /// `*Wallet Name cannot be empty.`
  String get wallet_import_nicke_empty {
    return Intl.message(
      '*Wallet Name cannot be empty.',
      name: 'wallet_import_nicke_empty',
      desc: '',
      args: [],
    );
  }

  /// `Password cannot be empty.`
  String get wallet_import_password_empty {
    return Intl.message(
      'Password cannot be empty.',
      name: 'wallet_import_password_empty',
      desc: '',
      args: [],
    );
  }

  /// `please enter the name of the wallet.`
  String get wallet_import_nicke_name_placeholder {
    return Intl.message(
      'please enter the name of the wallet.',
      name: 'wallet_import_nicke_name_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `please enter the password of the wallet.`
  String get wallet_import_password_placeholder {
    return Intl.message(
      'please enter the password of the wallet.',
      name: 'wallet_import_password_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `PrivateKey`
  String get wallet_import_type_privatekey {
    return Intl.message(
      'PrivateKey',
      name: 'wallet_import_type_privatekey',
      desc: '',
      args: [],
    );
  }

  /// `Mnemonic`
  String get wallet_import_type_mnemonic {
    return Intl.message(
      'Mnemonic',
      name: 'wallet_import_type_mnemonic',
      desc: '',
      args: [],
    );
  }

  /// `Keystore`
  String get wallet_import_type_Keystore {
    return Intl.message(
      'Keystore',
      name: 'wallet_import_type_Keystore',
      desc: '',
      args: [],
    );
  }

  /// `Please paste or enter the private key here`
  String get wallet_import_type_Keystore_tips {
    return Intl.message(
      'Please paste or enter the private key here',
      name: 'wallet_import_type_Keystore_tips',
      desc: '',
      args: [],
    );
  }

  /// `Please input or paste your mnemonic words.`
  String get wallet_import_input_privatekey_placeholder {
    return Intl.message(
      'Please input or paste your mnemonic words.',
      name: 'wallet_import_input_privatekey_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `*Please input mnemonic words.`
  String get wallet_import_input_privatekey_empty {
    return Intl.message(
      '*Please input mnemonic words.',
      name: 'wallet_import_input_privatekey_empty',
      desc: '',
      args: [],
    );
  }

  /// `*Please input privatekey.`
  String get wallet_import_input_privatekeys_empty {
    return Intl.message(
      '*Please input privatekey.',
      name: 'wallet_import_input_privatekeys_empty',
      desc: '',
      args: [],
    );
  }

  /// `*This private key already exists.`
  String get wallet_import_privatekey_already_exist {
    return Intl.message(
      '*This private key already exists.',
      name: 'wallet_import_privatekey_already_exist',
      desc: '',
      args: [],
    );
  }

  /// `*Please check whether the mnemonic words are filled in correctly`
  String get wallet_import_privatekey_invalid_formatter {
    return Intl.message(
      '*Please check whether the mnemonic words are filled in correctly',
      name: 'wallet_import_privatekey_invalid_formatter',
      desc: '',
      args: [],
    );
  }

  /// `*Please check whether the private key is filled in correctly`
  String get wallet_import_privatekey_invalid_formatter_private {
    return Intl.message(
      '*Please check whether the private key is filled in correctly',
      name: 'wallet_import_privatekey_invalid_formatter_private',
      desc: '',
      args: [],
    );
  }

  /// `please paste the contents of keystore file.`
  String get wallet_import_input_keystore_placeholder {
    return Intl.message(
      'please paste the contents of keystore file.',
      name: 'wallet_import_input_keystore_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `*please enter this item.`
  String get wallet_import_input_keystore_empty {
    return Intl.message(
      '*please enter this item.',
      name: 'wallet_import_input_keystore_empty',
      desc: '',
      args: [],
    );
  }

  /// `*This does not seem to be the correct keystore file.`
  String get wallet_import_input_keystore_invalid_formatter {
    return Intl.message(
      '*This does not seem to be the correct keystore file.',
      name: 'wallet_import_input_keystore_invalid_formatter',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get wallet_import_input_password {
    return Intl.message(
      'Password',
      name: 'wallet_import_input_password',
      desc: '',
      args: [],
    );
  }

  /// `please enter password`
  String get wallet_import_input_password_placeholder {
    return Intl.message(
      'please enter password',
      name: 'wallet_import_input_password_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `*please enter the password.`
  String get wallet_import_input_password_empty {
    return Intl.message(
      '*please enter the password.',
      name: 'wallet_import_input_password_empty',
      desc: '',
      args: [],
    );
  }

  /// `*this password cannot decrypt the keystore`
  String get wallet_import_keystore_password_invaid {
    return Intl.message(
      '*this password cannot decrypt the keystore',
      name: 'wallet_import_keystore_password_invaid',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get wallet_batch_import_title {
    return Intl.message(
      'Search',
      name: 'wallet_batch_import_title',
      desc: '',
      args: [],
    );
  }

  /// `Search query`
  String get wallet_batch_import_search {
    return Intl.message(
      'Search query',
      name: 'wallet_batch_import_search',
      desc: '',
      args: [],
    );
  }

  /// `Number of private keys`
  String get wallet_batch_import_private_key_nums {
    return Intl.message(
      'Number of private keys',
      name: 'wallet_batch_import_private_key_nums',
      desc: '',
      args: [],
    );
  }

  /// `Sub account private key`
  String get wallet_batch_import_sub_account_private_key {
    return Intl.message(
      'Sub account private key',
      name: 'wallet_batch_import_sub_account_private_key',
      desc: '',
      args: [],
    );
  }

  /// `Main currency`
  String get wallet_batch_import_main_balance {
    return Intl.message(
      'Main currency',
      name: 'wallet_batch_import_main_balance',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get wallet_batch_import_edit {
    return Intl.message(
      'Edit',
      name: 'wallet_batch_import_edit',
      desc: '',
      args: [],
    );
  }

  /// `Sort`
  String get wallet_batch_import_serial_number {
    return Intl.message(
      'Sort',
      name: 'wallet_batch_import_serial_number',
      desc: '',
      args: [],
    );
  }

  /// `Private key`
  String get wallet_batch_import_private_key {
    return Intl.message(
      'Private key',
      name: 'wallet_batch_import_private_key',
      desc: '',
      args: [],
    );
  }

  /// `Import succeeded`
  String get wallet_batch_import_succeeded {
    return Intl.message(
      'Import succeeded',
      name: 'wallet_batch_import_succeeded',
      desc: '',
      args: [],
    );
  }

  /// `OK, I know`
  String get wallet_export_show_security {
    return Intl.message(
      'OK, I know',
      name: 'wallet_export_show_security',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get wallet_export_done {
    return Intl.message(
      'OK',
      name: 'wallet_export_done',
      desc: '',
      args: [],
    );
  }

  /// `Backup Mnemonics`
  String get wallet_export_tip0_title {
    return Intl.message(
      'Backup Mnemonics',
      name: 'wallet_export_tip0_title',
      desc: '',
      args: [],
    );
  }

  /// `Use paper and pen to copy mnemonics correctly. If your mobile phone is lost, stolen or damaged, mnemonics will recover your assets.`
  String get wallet_export_tip0_desc {
    return Intl.message(
      'Use paper and pen to copy mnemonics correctly. If your mobile phone is lost, stolen or damaged, mnemonics will recover your assets.',
      name: 'wallet_export_tip0_desc',
      desc: '',
      args: [],
    );
  }

  /// `Offline Storage`
  String get wallet_export_tip1_title {
    return Intl.message(
      'Offline Storage',
      name: 'wallet_export_tip1_title',
      desc: '',
      args: [],
    );
  }

  /// `Keep it in a safe place with isolated network.\n\nPlease don't share and store mnemonics in Internet, such as email, photo album, social application, etc.`
  String get wallet_export_tip1_desc {
    return Intl.message(
      'Keep it in a safe place with isolated network.\n\nPlease don\'t share and store mnemonics in Internet, such as email, photo album, social application, etc.',
      name: 'wallet_export_tip1_desc',
      desc: '',
      args: [],
    );
  }

  /// `No Capture`
  String get wallet_export_tip2_title {
    return Intl.message(
      'No Capture',
      name: 'wallet_export_tip2_title',
      desc: '',
      args: [],
    );
  }

  /// `Screen capture sharing and storage may be collected by malware, resulting in asset loss.`
  String get wallet_export_tip2_desc {
    return Intl.message(
      'Screen capture sharing and storage may be collected by malware, resulting in asset loss.',
      name: 'wallet_export_tip2_desc',
      desc: '',
      args: [],
    );
  }

  /// `Here is your private key :（Click content copy）`
  String get wallet_export_this_is_your_security_private_key {
    return Intl.message(
      'Here is your private key :（Click content copy）',
      name: 'wallet_export_this_is_your_security_private_key',
      desc: '',
      args: [],
    );
  }

  /// `Warning: never disclose this private key. Anyone with your private key can steal any asset in your account.`
  String get wallet_export_warrning_private_key {
    return Intl.message(
      'Warning: never disclose this private key. Anyone with your private key can steal any asset in your account.',
      name: 'wallet_export_warrning_private_key',
      desc: '',
      args: [],
    );
  }

  /// `Here is your mnemonic words :（Click content copy）`
  String get wallet_export_this_is_your_security {
    return Intl.message(
      'Here is your mnemonic words :（Click content copy）',
      name: 'wallet_export_this_is_your_security',
      desc: '',
      args: [],
    );
  }

  /// `Warning: never disclose this mnemonic words. Anyone with your mnemonic words can steal any asset in your account.`
  String get wallet_export_warrning {
    return Intl.message(
      'Warning: never disclose this mnemonic words. Anyone with your mnemonic words can steal any asset in your account.',
      name: 'wallet_export_warrning',
      desc: '',
      args: [],
    );
  }

  /// `network switch`
  String get wallet_network_select_title {
    return Intl.message(
      'network switch',
      name: 'wallet_network_select_title',
      desc: '',
      args: [],
    );
  }

  /// `Ethereum main network`
  String get wallet_network_select_type_1 {
    return Intl.message(
      'Ethereum main network',
      name: 'wallet_network_select_type_1',
      desc: '',
      args: [],
    );
  }

  /// `Ethereum Layer 2 network`
  String get wallet_network_select_type_4 {
    return Intl.message(
      'Ethereum Layer 2 network',
      name: 'wallet_network_select_type_4',
      desc: '',
      args: [],
    );
  }

  /// `Ethereum Compatible Chain`
  String get wallet_network_select_type_2 {
    return Intl.message(
      'Ethereum Compatible Chain',
      name: 'wallet_network_select_type_2',
      desc: '',
      args: [],
    );
  }

  /// `Customize`
  String get wallet_network_select_type_3 {
    return Intl.message(
      'Customize',
      name: 'wallet_network_select_type_3',
      desc: '',
      args: [],
    );
  }

  /// `Add Custom Network`
  String get wallet_network_select_add {
    return Intl.message(
      'Add Custom Network',
      name: 'wallet_network_select_add',
      desc: '',
      args: [],
    );
  }

  /// `Delete Wallet`
  String get wallet_delete_title {
    return Intl.message(
      'Delete Wallet',
      name: 'wallet_delete_title',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this wallet? It will be permanently deleted after 24 hours. Please make sure that you have saved the private key or mnemonic. After deletion, you can re-import the wallet through the private key or mnemonic, and the funds related to the wallet will not be affected.`
  String get wallet_delete_tip {
    return Intl.message(
      'Are you sure you want to delete this wallet? It will be permanently deleted after 24 hours. Please make sure that you have saved the private key or mnemonic. After deletion, you can re-import the wallet through the private key or mnemonic, and the funds related to the wallet will not be affected.',
      name: 'wallet_delete_tip',
      desc: '',
      args: [],
    );
  }

  /// `I have saved the private key`
  String get wallet_delete_checkout {
    return Intl.message(
      'I have saved the private key',
      name: 'wallet_delete_checkout',
      desc: '',
      args: [],
    );
  }

  /// `Please tick, I have saved the private key`
  String get wallet_delete_message {
    return Intl.message(
      'Please tick, I have saved the private key',
      name: 'wallet_delete_message',
      desc: '',
      args: [],
    );
  }

  /// `Create Wallet`
  String get welcome_create {
    return Intl.message(
      'Create Wallet',
      name: 'welcome_create',
      desc: '',
      args: [],
    );
  }

  /// `Create BTC/Eth/EOS/USDT/ETC wallets`
  String get welcome_create_desc {
    return Intl.message(
      'Create BTC/Eth/EOS/USDT/ETC wallets',
      name: 'welcome_create_desc',
      desc: '',
      args: [],
    );
  }

  /// `Import Wallet`
  String get welcome_import {
    return Intl.message(
      'Import Wallet',
      name: 'welcome_import',
      desc: '',
      args: [],
    );
  }

  /// `Support mainstream wallet mnemonics, \nprivate key, keysore import.`
  String get welcome_import_desc {
    return Intl.message(
      'Support mainstream wallet mnemonics, \nprivate key, keysore import.',
      name: 'welcome_import_desc',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get welcome_language {
    return Intl.message(
      'English',
      name: 'welcome_language',
      desc: '',
      args: [],
    );
  }

  /// `Backup Mnemonics`
  String get create_wallet_backup_mnemonics_title {
    return Intl.message(
      'Backup Mnemonics',
      name: 'create_wallet_backup_mnemonics_title',
      desc: '',
      args: [],
    );
  }

  /// `Wallet address`
  String get create_wallet_backup_mnemonics_Wallet_address {
    return Intl.message(
      'Wallet address',
      name: 'create_wallet_backup_mnemonics_Wallet_address',
      desc: '',
      args: [],
    );
  }

  /// `Please use paper and pen to copy mnemonics correctly in order and keep them properly.`
  String get create_wallet_backup_mnemonics_tips5 {
    return Intl.message(
      'Please use paper and pen to copy mnemonics correctly in order and keep them properly.',
      name: 'create_wallet_backup_mnemonics_tips5',
      desc: '',
      args: [],
    );
  }

  /// `Remember:`
  String get create_wallet_backup_mnemonics_tips6 {
    return Intl.message(
      'Remember:',
      name: 'create_wallet_backup_mnemonics_tips6',
      desc: '',
      args: [],
    );
  }

  /// `·  Do not disclose mnemonics to anyone`
  String get create_wallet_backup_mnemonics_tips1 {
    return Intl.message(
      '·  Do not disclose mnemonics to anyone',
      name: 'create_wallet_backup_mnemonics_tips1',
      desc: '',
      args: [],
    );
  }

  /// `·  Once mnemonics are lost, assets cannot be recovered`
  String get create_wallet_backup_mnemonics_tips2 {
    return Intl.message(
      '·  Once mnemonics are lost, assets cannot be recovered',
      name: 'create_wallet_backup_mnemonics_tips2',
      desc: '',
      args: [],
    );
  }

  /// `·  Please do not backup and save by screen capture or network transmission`
  String get create_wallet_backup_mnemonics_tips3 {
    return Intl.message(
      '·  Please do not backup and save by screen capture or network transmission',
      name: 'create_wallet_backup_mnemonics_tips3',
      desc: '',
      args: [],
    );
  }

  /// `·  In any case, please do not uninstall the wallet app easily`
  String get create_wallet_backup_mnemonics_tips4 {
    return Intl.message(
      '·  In any case, please do not uninstall the wallet app easily',
      name: 'create_wallet_backup_mnemonics_tips4',
      desc: '',
      args: [],
    );
  }

  /// `Verify that the backup is complete`
  String get create_wallet_backup_mnemonics_confirm {
    return Intl.message(
      'Verify that the backup is complete',
      name: 'create_wallet_backup_mnemonics_confirm',
      desc: '',
      args: [],
    );
  }

  /// `Confirmation Mnemonics`
  String get create_wallet_confirm_mnemonics_title {
    return Intl.message(
      'Confirmation Mnemonics',
      name: 'create_wallet_confirm_mnemonics_title',
      desc: '',
      args: [],
    );
  }

  /// `Please click mnemonics in order to confirm your correct backup`
  String get create_wallet_confirm_mnemonics_click_prompt {
    return Intl.message(
      'Please click mnemonics in order to confirm your correct backup',
      name: 'create_wallet_confirm_mnemonics_click_prompt',
      desc: '',
      args: [],
    );
  }

  /// `Mnemonic error, please re select`
  String get create_wallet_confirm_mnemonics_confirm_prompt_fail {
    return Intl.message(
      'Mnemonic error, please re select',
      name: 'create_wallet_confirm_mnemonics_confirm_prompt_fail',
      desc: '',
      args: [],
    );
  }

  /// `Check in correctly and enter the wallet`
  String get create_wallet_confirm_mnemonics_confirm_prompt_success {
    return Intl.message(
      'Check in correctly and enter the wallet',
      name: 'create_wallet_confirm_mnemonics_confirm_prompt_success',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get create_wallet_confirm_mnemonics_confirm {
    return Intl.message(
      'Confirm',
      name: 'create_wallet_confirm_mnemonics_confirm',
      desc: '',
      args: [],
    );
  }

  /// `Request New Address`
  String get create_wallet_batch_title {
    return Intl.message(
      'Request New Address',
      name: 'create_wallet_batch_title',
      desc: '',
      args: [],
    );
  }

  /// `Request 1 address`
  String get create_wallet_batch_one {
    return Intl.message(
      'Request 1 address',
      name: 'create_wallet_batch_one',
      desc: '',
      args: [],
    );
  }

  /// `Request 5 addresses`
  String get create_wallet_batch_five {
    return Intl.message(
      'Request 5 addresses',
      name: 'create_wallet_batch_five',
      desc: '',
      args: [],
    );
  }

  /// `Request 50 addresses`
  String get create_wallet_batch_fifty {
    return Intl.message(
      'Request 50 addresses',
      name: 'create_wallet_batch_fifty',
      desc: '',
      args: [],
    );
  }

  /// `Request 10 addresses`
  String get create_wallet_batch_ten {
    return Intl.message(
      'Request 10 addresses',
      name: 'create_wallet_batch_ten',
      desc: '',
      args: [],
    );
  }

  /// `Request 100 addresses`
  String get create_wallet_batch_hundred {
    return Intl.message(
      'Request 100 addresses',
      name: 'create_wallet_batch_hundred',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the remarks (account name)`
  String get create_wallet_batch_input {
    return Intl.message(
      'Please enter the remarks (account name)',
      name: 'create_wallet_batch_input',
      desc: '',
      args: [],
    );
  }

  /// `Building in progress...`
  String get create_wallet_batch_loading {
    return Intl.message(
      'Building in progress...',
      name: 'create_wallet_batch_loading',
      desc: '',
      args: [],
    );
  }

  /// `Please enter account name`
  String get create_wallet_batch_address_name {
    return Intl.message(
      'Please enter account name',
      name: 'create_wallet_batch_address_name',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get create_title {
    return Intl.message(
      'Create',
      name: 'create_title',
      desc: '',
      args: [],
    );
  }

  /// `Wallet Name`
  String get create_wallet_name {
    return Intl.message(
      'Wallet Name',
      name: 'create_wallet_name',
      desc: '',
      args: [],
    );
  }

  /// `please enter the wallet name.`
  String get create_wallet_name_placeholder {
    return Intl.message(
      'please enter the wallet name.',
      name: 'create_wallet_name_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `*wallet name must be entered`
  String get create_wallet_name_empty {
    return Intl.message(
      '*wallet name must be entered',
      name: 'create_wallet_name_empty',
      desc: '',
      args: [],
    );
  }

  /// `Set Password`
  String get create_wallet_pwd {
    return Intl.message(
      'Set Password',
      name: 'create_wallet_pwd',
      desc: '',
      args: [],
    );
  }

  /// `set the wallet password.`
  String get create_wallet_pwd_placeholder {
    return Intl.message(
      'set the wallet password.',
      name: 'create_wallet_pwd_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `*password name must be entered`
  String get create_wallet_pwd_empty {
    return Intl.message(
      '*password name must be entered',
      name: 'create_wallet_pwd_empty',
      desc: '',
      args: [],
    );
  }

  /// `Confirm Password`
  String get create_wallet_pwd_confirm {
    return Intl.message(
      'Confirm Password',
      name: 'create_wallet_pwd_confirm',
      desc: '',
      args: [],
    );
  }

  /// `*The confirm password cannot be empty`
  String get create_wallet_pwd_confirm_empty {
    return Intl.message(
      '*The confirm password cannot be empty',
      name: 'create_wallet_pwd_confirm_empty',
      desc: '',
      args: [],
    );
  }

  /// `confirm the wallet password.`
  String get create_wallet_pwd_confirm_placeholder {
    return Intl.message(
      'confirm the wallet password.',
      name: 'create_wallet_pwd_confirm_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `*passwords you entered are inconsistent.`
  String get create_wallet_pwd_confirm_different {
    return Intl.message(
      '*passwords you entered are inconsistent.',
      name: 'create_wallet_pwd_confirm_different',
      desc: '',
      args: [],
    );
  }

  /// `This password is only used as a security protection for the wallet.Hashpay does not save your password, so be sure to remember,And keep the password safe.`
  String get create_wallet_tips {
    return Intl.message(
      'This password is only used as a security protection for the wallet.Hashpay does not save your password, so be sure to remember,And keep the password safe.',
      name: 'create_wallet_tips',
      desc: '',
      args: [],
    );
  }

  /// `Hint`
  String get create_wallet_title_appbar {
    return Intl.message(
      'Hint',
      name: 'create_wallet_title_appbar',
      desc: '',
      args: [],
    );
  }

  /// `In the next step you will see the (12 word) mnemonic phrase that will restore your wallet, keep the following points in mind: `
  String get create_wallet_title {
    return Intl.message(
      'In the next step you will see the (12 word) mnemonic phrase that will restore your wallet, keep the following points in mind: ',
      name: 'create_wallet_title',
      desc: '',
      args: [],
    );
  }

  /// `If I lose my mnemonic, my assets will be lost forever!`
  String get create_wallet_tips_one {
    return Intl.message(
      'If I lose my mnemonic, my assets will be lost forever!',
      name: 'create_wallet_tips_one',
      desc: '',
      args: [],
    );
  }

  /// `If I reveal or share my mnemonic with anyone, my assets could be stolen!`
  String get create_wallet_tips_two {
    return Intl.message(
      'If I reveal or share my mnemonic with anyone, my assets could be stolen!',
      name: 'create_wallet_tips_two',
      desc: '',
      args: [],
    );
  }

  /// `The responsibility for keeping the mnemonic safe lies entirely with me!`
  String get create_wallet_tips_three {
    return Intl.message(
      'The responsibility for keeping the mnemonic safe lies entirely with me!',
      name: 'create_wallet_tips_three',
      desc: '',
      args: [],
    );
  }

  /// `Please tick the box above`
  String get create_wallet_tips_message {
    return Intl.message(
      'Please tick the box above',
      name: 'create_wallet_tips_message',
      desc: '',
      args: [],
    );
  }

  /// `Home page asset addition has exceeded the limit`
  String get ethereum_home_asset_limit_warming {
    return Intl.message(
      'Home page asset addition has exceeded the limit',
      name: 'ethereum_home_asset_limit_warming',
      desc: '',
      args: [],
    );
  }

  /// `Asset`
  String get ethereum_home_baritem_title {
    return Intl.message(
      'Asset',
      name: 'ethereum_home_baritem_title',
      desc: '',
      args: [],
    );
  }

  /// `Transfer`
  String get ethereum_home_detail_transfer {
    return Intl.message(
      'Transfer',
      name: 'ethereum_home_detail_transfer',
      desc: '',
      args: [],
    );
  }

  /// `Collection`
  String get ethereum_home_detail_collection {
    return Intl.message(
      'Collection',
      name: 'ethereum_home_detail_collection',
      desc: '',
      args: [],
    );
  }

  /// `All`
  String get ethereum_home_detail_txfilter_all {
    return Intl.message(
      'All',
      name: 'ethereum_home_detail_txfilter_all',
      desc: '',
      args: [],
    );
  }

  /// `Receipted`
  String get ethereum_home_detail_txfilter_in {
    return Intl.message(
      'Receipted',
      name: 'ethereum_home_detail_txfilter_in',
      desc: '',
      args: [],
    );
  }

  /// `Transfer`
  String get ethereum_home_detail_txfilter_out {
    return Intl.message(
      'Transfer',
      name: 'ethereum_home_detail_txfilter_out',
      desc: '',
      args: [],
    );
  }

  /// `Other`
  String get ethereum_home_detail_txfilter_call {
    return Intl.message(
      'Other',
      name: 'ethereum_home_detail_txfilter_call',
      desc: '',
      args: [],
    );
  }

  /// `Succses`
  String get ethereum_home_detail_tx_state_succses {
    return Intl.message(
      'Succses',
      name: 'ethereum_home_detail_tx_state_succses',
      desc: '',
      args: [],
    );
  }

  /// `Fail`
  String get ethereum_home_detail_tx_state_failed {
    return Intl.message(
      'Fail',
      name: 'ethereum_home_detail_tx_state_failed',
      desc: '',
      args: [],
    );
  }

  /// `Invoke`
  String get ethereum_home_detail_tx_state_call {
    return Intl.message(
      'Invoke',
      name: 'ethereum_home_detail_tx_state_call',
      desc: '',
      args: [],
    );
  }

  /// `Creation`
  String get ethereum_home_detail_tx_state_creation {
    return Intl.message(
      'Creation',
      name: 'ethereum_home_detail_tx_state_creation',
      desc: '',
      args: [],
    );
  }

  /// `Override`
  String get ethereum_home_detail_tx_state_override {
    return Intl.message(
      'Override',
      name: 'ethereum_home_detail_tx_state_override',
      desc: '',
      args: [],
    );
  }

  /// `Sent Amount`
  String get ethereum_home_transfer_amount {
    return Intl.message(
      'Sent Amount',
      name: 'ethereum_home_transfer_amount',
      desc: '',
      args: [],
    );
  }

  /// `Send All`
  String get ethereum_home_transfer_sent_all {
    return Intl.message(
      'Send All',
      name: 'ethereum_home_transfer_sent_all',
      desc: '',
      args: [],
    );
  }

  /// `Fee`
  String get ethereum_home_transfer_fee {
    return Intl.message(
      'Fee',
      name: 'ethereum_home_transfer_fee',
      desc: '',
      args: [],
    );
  }

  /// `Gas`
  String get ethereum_home_transfer_gas {
    return Intl.message(
      'Gas',
      name: 'ethereum_home_transfer_gas',
      desc: '',
      args: [],
    );
  }

  /// `Current balance: `
  String get ethereum_home_transfer_balance_tip {
    return Intl.message(
      'Current balance: ',
      name: 'ethereum_home_transfer_balance_tip',
      desc: '',
      args: [],
    );
  }

  /// `Sent`
  String get ethereum_home_transfer_do_sent {
    return Intl.message(
      'Sent',
      name: 'ethereum_home_transfer_do_sent',
      desc: '',
      args: [],
    );
  }

  /// `Loading...`
  String get ethereum_home_transfer_loading {
    return Intl.message(
      'Loading...',
      name: 'ethereum_home_transfer_loading',
      desc: '',
      args: [],
    );
  }

  /// `The quantity must be a multiple of %d.`
  String get ethereum_home_transfer_granularity {
    return Intl.message(
      'The quantity must be a multiple of %d.',
      name: 'ethereum_home_transfer_granularity',
      desc: '',
      args: [],
    );
  }

  /// `Insufficient `
  String get ethereum_home_transfer_insufficient_handling_charges_one {
    return Intl.message(
      'Insufficient ',
      name: 'ethereum_home_transfer_insufficient_handling_charges_one',
      desc: '',
      args: [],
    );
  }

  /// `Gas Fee`
  String get ethereum_home_transfer_insufficient_handling_charges_two {
    return Intl.message(
      'Gas Fee',
      name: 'ethereum_home_transfer_insufficient_handling_charges_two',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the transfer address`
  String get ethereum_home_transfer_plaease_input_adress {
    return Intl.message(
      'Please enter the transfer address',
      name: 'ethereum_home_transfer_plaease_input_adress',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the correct transfer quantity`
  String get ethereum_home_transfer_plaease_input_number {
    return Intl.message(
      'Please enter the correct transfer quantity',
      name: 'ethereum_home_transfer_plaease_input_number',
      desc: '',
      args: [],
    );
  }

  /// `Please check that the address is entered correctly`
  String get ethereum_home_transfer_plaease_check_address_is_entered {
    return Intl.message(
      'Please check that the address is entered correctly',
      name: 'ethereum_home_transfer_plaease_check_address_is_entered',
      desc: '',
      args: [],
    );
  }

  /// `Lack of balance`
  String get ethereum_home_transfer_lack_of_balance {
    return Intl.message(
      'Lack of balance',
      name: 'ethereum_home_transfer_lack_of_balance',
      desc: '',
      args: [],
    );
  }

  /// `Select Address`
  String get ethereum_home_address_selector_title {
    return Intl.message(
      'Select Address',
      name: 'ethereum_home_address_selector_title',
      desc: '',
      args: [],
    );
  }

  /// `please enter or paste the address.`
  String get ethereum_home_address_selector_placeholder {
    return Intl.message(
      'please enter or paste the address.',
      name: 'ethereum_home_address_selector_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `Local Addresses`
  String get ethereum_home_address_selector_local {
    return Intl.message(
      'Local Addresses',
      name: 'ethereum_home_address_selector_local',
      desc: '',
      args: [],
    );
  }

  /// `Recent Addresses`
  String get ethereum_home_address_selector_recent {
    return Intl.message(
      'Recent Addresses',
      name: 'ethereum_home_address_selector_recent',
      desc: '',
      args: [],
    );
  }

  /// `No matching results, please check the input.`
  String get ethereum_home_address_selector_unmatch {
    return Intl.message(
      'No matching results, please check the input.',
      name: 'ethereum_home_address_selector_unmatch',
      desc: '',
      args: [],
    );
  }

  /// `New Address`
  String get ethereum_home_address_selector_new_address {
    return Intl.message(
      'New Address',
      name: 'ethereum_home_address_selector_new_address',
      desc: '',
      args: [],
    );
  }

  /// `Dapp`
  String get ethereum_dapp_baritem_title {
    return Intl.message(
      'Dapp',
      name: 'ethereum_dapp_baritem_title',
      desc: '',
      args: [],
    );
  }

  /// `Please scan this wallet address QR code`
  String get ethereum_home_scan_title {
    return Intl.message(
      'Please scan this wallet address QR code',
      name: 'ethereum_home_scan_title',
      desc: '',
      args: [],
    );
  }

  /// `Collection`
  String get ethereum_home_qrcode_title {
    return Intl.message(
      'Collection',
      name: 'ethereum_home_qrcode_title',
      desc: '',
      args: [],
    );
  }

  /// `Wallet address`
  String get ethereum_home_qrcode_wallet {
    return Intl.message(
      'Wallet address',
      name: 'ethereum_home_qrcode_wallet',
      desc: '',
      args: [],
    );
  }

  /// `Scan QR code to transfer in assets`
  String get ethereum_home_qrcode_tips {
    return Intl.message(
      'Scan QR code to transfer in assets',
      name: 'ethereum_home_qrcode_tips',
      desc: '',
      args: [],
    );
  }

  /// `Do not recharge any non chain `
  String get ethereum_home_qrcode_transfertips {
    return Intl.message(
      'Do not recharge any non chain ',
      name: 'ethereum_home_qrcode_transfertips',
      desc: '',
      args: [],
    );
  }

  /// `assets to the address`
  String get ethereum_home_qrcode_transfertips2 {
    return Intl.message(
      'assets to the address',
      name: 'ethereum_home_qrcode_transfertips2',
      desc: '',
      args: [],
    );
  }

  /// `Copy address`
  String get ethereum_home_qrcode_copy_address {
    return Intl.message(
      'Copy address',
      name: 'ethereum_home_qrcode_copy_address',
      desc: '',
      args: [],
    );
  }

  /// `Share screenshots`
  String get ethereum_home_qrcode_share {
    return Intl.message(
      'Share screenshots',
      name: 'ethereum_home_qrcode_share',
      desc: '',
      args: [],
    );
  }

  /// `Please do not divulge QR code`
  String get ethereum_home_qrcode_tip1 {
    return Intl.message(
      'Please do not divulge QR code',
      name: 'ethereum_home_qrcode_tip1',
      desc: '',
      args: [],
    );
  }

  /// `Click Present private key QR code`
  String get ethereum_home_qrcode_tip2 {
    return Intl.message(
      'Click Present private key QR code',
      name: 'ethereum_home_qrcode_tip2',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get ethereum_setting_title {
    return Intl.message(
      'Setting',
      name: 'ethereum_setting_title',
      desc: '',
      args: [],
    );
  }

  /// `Manage your wallet`
  String get ethereum_setting_manage {
    return Intl.message(
      'Manage your wallet',
      name: 'ethereum_setting_manage',
      desc: '',
      args: [],
    );
  }

  /// `My`
  String get ethereum_setting_baritem_title {
    return Intl.message(
      'My',
      name: 'ethereum_setting_baritem_title',
      desc: '',
      args: [],
    );
  }

  /// `Wallet_name`
  String get ethereum_setting_wallet_name {
    return Intl.message(
      'Wallet_name',
      name: 'ethereum_setting_wallet_name',
      desc: '',
      args: [],
    );
  }

  /// `Change wallet name`
  String get ethereum_setting_change_wallet_name {
    return Intl.message(
      'Change wallet name',
      name: 'ethereum_setting_change_wallet_name',
      desc: '',
      args: [],
    );
  }

  /// `Backup mnemonics`
  String get ethereum_setting_backup_mnemonics {
    return Intl.message(
      'Backup mnemonics',
      name: 'ethereum_setting_backup_mnemonics',
      desc: '',
      args: [],
    );
  }

  /// `Change password`
  String get ethereum_setting_change_password {
    return Intl.message(
      'Change password',
      name: 'ethereum_setting_change_password',
      desc: '',
      args: [],
    );
  }

  /// `Language settings`
  String get ethereum_setting_language_settings {
    return Intl.message(
      'Language settings',
      name: 'ethereum_setting_language_settings',
      desc: '',
      args: [],
    );
  }

  /// `Clean up cache`
  String get ethereum_setting_clean_up_cache {
    return Intl.message(
      'Clean up cache',
      name: 'ethereum_setting_clean_up_cache',
      desc: '',
      args: [],
    );
  }

  /// `Network settings`
  String get ethereum_setting_network_settings {
    return Intl.message(
      'Network settings',
      name: 'ethereum_setting_network_settings',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get ethereum_setting_share_setting {
    return Intl.message(
      'Share',
      name: 'ethereum_setting_share_setting',
      desc: '',
      args: [],
    );
  }

  /// `Scan`
  String get ethereum_setting_share_scan_downloader {
    return Intl.message(
      'Scan',
      name: 'ethereum_setting_share_scan_downloader',
      desc: '',
      args: [],
    );
  }

  /// `Ethernet one-stop digital asset Wallet`
  String get ethereum_setting_share_tips {
    return Intl.message(
      'Ethernet one-stop digital asset Wallet',
      name: 'ethereum_setting_share_tips',
      desc: '',
      args: [],
    );
  }

  /// `Notice`
  String get ethereum_setting_notice_setting {
    return Intl.message(
      'Notice',
      name: 'ethereum_setting_notice_setting',
      desc: '',
      args: [],
    );
  }

  /// `Version number`
  String get ethereum_setting_version_number {
    return Intl.message(
      'Version number',
      name: 'ethereum_setting_version_number',
      desc: '',
      args: [],
    );
  }

  /// `New Version Update`
  String get ethereum_setting_version_update {
    return Intl.message(
      'New Version Update',
      name: 'ethereum_setting_version_update',
      desc: '',
      args: [],
    );
  }

  /// `New`
  String get ethereum_setting_version_last {
    return Intl.message(
      'New',
      name: 'ethereum_setting_version_last',
      desc: '',
      args: [],
    );
  }

  /// `About us`
  String get ethereum_setting_about_us {
    return Intl.message(
      'About us',
      name: 'ethereum_setting_about_us',
      desc: '',
      args: [],
    );
  }

  /// `Language Setting`
  String get ethereum_setting_language_title {
    return Intl.message(
      'Language Setting',
      name: 'ethereum_setting_language_title',
      desc: '',
      args: [],
    );
  }

  /// `Chinese(Simplified)`
  String get ethereum_setting_language_zh_title {
    return Intl.message(
      'Chinese(Simplified)',
      name: 'ethereum_setting_language_zh_title',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get ethereum_setting_language_en_title {
    return Intl.message(
      'English',
      name: 'ethereum_setting_language_en_title',
      desc: '',
      args: [],
    );
  }

  /// `Network settings`
  String get ethereum_setting_network_title {
    return Intl.message(
      'Network settings',
      name: 'ethereum_setting_network_title',
      desc: '',
      args: [],
    );
  }

  /// `Information`
  String get ethereum_setting_network_information_title {
    return Intl.message(
      'Information',
      name: 'ethereum_setting_network_information_title',
      desc: '',
      args: [],
    );
  }

  /// `Insecure network providers may lie about the status of the blockchain and record your network activity. Add only the networks you trust`
  String get ethereum_setting_network_information_tips {
    return Intl.message(
      'Insecure network providers may lie about the status of the blockchain and record your network activity. Add only the networks you trust',
      name: 'ethereum_setting_network_information_tips',
      desc: '',
      args: [],
    );
  }

  /// `Node`
  String get ethereum_setting_network_information_node {
    return Intl.message(
      'Node',
      name: 'ethereum_setting_network_information_node',
      desc: '',
      args: [],
    );
  }

  /// `Delay`
  String get ethereum_setting_network_information_net {
    return Intl.message(
      'Delay',
      name: 'ethereum_setting_network_information_net',
      desc: '',
      args: [],
    );
  }

  /// `Network terms`
  String get ethereum_setting_network_network_terms {
    return Intl.message(
      'Network terms',
      name: 'ethereum_setting_network_network_terms',
      desc: '',
      args: [],
    );
  }

  /// `Chain ID`
  String get ethereum_setting_network_chain_iD {
    return Intl.message(
      'Chain ID',
      name: 'ethereum_setting_network_chain_iD',
      desc: '',
      args: [],
    );
  }

  /// `Symbol`
  String get ethereum_setting_network_symbol {
    return Intl.message(
      'Symbol',
      name: 'ethereum_setting_network_symbol',
      desc: '',
      args: [],
    );
  }

  /// `Blockchain browser`
  String get ethereum_setting_blockchain_browser {
    return Intl.message(
      'Blockchain browser',
      name: 'ethereum_setting_blockchain_browser',
      desc: '',
      args: [],
    );
  }

  /// `Data source server`
  String get ethereum_setting_d_s_server {
    return Intl.message(
      'Data source server',
      name: 'ethereum_setting_d_s_server',
      desc: '',
      args: [],
    );
  }

  /// `Network node`
  String get ethereum_setting_network_node {
    return Intl.message(
      'Network node',
      name: 'ethereum_setting_network_node',
      desc: '',
      args: [],
    );
  }

  /// `Node speed`
  String get ethereum_setting_network_node_speed {
    return Intl.message(
      'Node speed',
      name: 'ethereum_setting_network_node_speed',
      desc: '',
      args: [],
    );
  }

  /// `fas`
  String get ethereum_setting_network_node_fast {
    return Intl.message(
      'fas',
      name: 'ethereum_setting_network_node_fast',
      desc: '',
      args: [],
    );
  }

  /// `secondary`
  String get ethereum_setting_network_node_secondary {
    return Intl.message(
      'secondary',
      name: 'ethereum_setting_network_node_secondary',
      desc: '',
      args: [],
    );
  }

  /// `slow`
  String get ethereum_setting_network_node_slow {
    return Intl.message(
      'slow',
      name: 'ethereum_setting_network_node_slow',
      desc: '',
      args: [],
    );
  }

  /// `Block height: the higher the height value, the better the node data synchronization and stability. When the node speed is the same, it is better to choose a node with a high height value.`
  String get ethereum_setting_network_node_tip {
    return Intl.message(
      'Block height: the higher the height value, the better the node data synchronization and stability. When the node speed is the same, it is better to choose a node with a high height value.',
      name: 'ethereum_setting_network_node_tip',
      desc: '',
      args: [],
    );
  }

  /// `Recommended nodes`
  String get ethereum_setting_network_node_recommend {
    return Intl.message(
      'Recommended nodes',
      name: 'ethereum_setting_network_node_recommend',
      desc: '',
      args: [],
    );
  }

  /// `Node switching succeeded`
  String get ethereum_setting_network_node_meesage_succeeses {
    return Intl.message(
      'Node switching succeeded',
      name: 'ethereum_setting_network_node_meesage_succeeses',
      desc: '',
      args: [],
    );
  }

  /// `Switching failed, node error`
  String get ethereum_setting_network_node_meesage_failed {
    return Intl.message(
      'Switching failed, node error',
      name: 'ethereum_setting_network_node_meesage_failed',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get ethereum_setting_network_node_meesage_error {
    return Intl.message(
      'Error',
      name: 'ethereum_setting_network_node_meesage_error',
      desc: '',
      args: [],
    );
  }

  /// `Block Height`
  String get ethereum_setting_network_node_block_height {
    return Intl.message(
      'Block Height',
      name: 'ethereum_setting_network_node_block_height',
      desc: '',
      args: [],
    );
  }

  /// `Change password`
  String get ethereum_setting_change_password_title {
    return Intl.message(
      'Change password',
      name: 'ethereum_setting_change_password_title',
      desc: '',
      args: [],
    );
  }

  /// `Old password cannot be empty`
  String get ethereum_setting_change_password_old_tips {
    return Intl.message(
      'Old password cannot be empty',
      name: 'ethereum_setting_change_password_old_tips',
      desc: '',
      args: [],
    );
  }

  /// `New password cannot be empty`
  String get ethereum_setting_change_password_new_tips {
    return Intl.message(
      'New password cannot be empty',
      name: 'ethereum_setting_change_password_new_tips',
      desc: '',
      args: [],
    );
  }

  /// `Confirm password cannot be empty`
  String get ethereum_setting_change_password_confim_tips {
    return Intl.message(
      'Confirm password cannot be empty',
      name: 'ethereum_setting_change_password_confim_tips',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the old password`
  String get ethereum_setting_change_password_input_old_tips {
    return Intl.message(
      'Please enter the old password',
      name: 'ethereum_setting_change_password_input_old_tips',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the new password`
  String get ethereum_setting_change_password_input_new_tips {
    return Intl.message(
      'Please enter the new password',
      name: 'ethereum_setting_change_password_input_new_tips',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the confirmation password`
  String get ethereum_setting_change_password_input_confim_tips {
    return Intl.message(
      'Please enter the confirmation password',
      name: 'ethereum_setting_change_password_input_confim_tips',
      desc: '',
      args: [],
    );
  }

  /// `The confirmation password is not consistent with the new password`
  String get ethereum_setting_change_password_not_consistent {
    return Intl.message(
      'The confirmation password is not consistent with the new password',
      name: 'ethereum_setting_change_password_not_consistent',
      desc: '',
      args: [],
    );
  }

  /// `The old password is incorrect`
  String get ethereum_setting_change_password_incorrect {
    return Intl.message(
      'The old password is incorrect',
      name: 'ethereum_setting_change_password_incorrect',
      desc: '',
      args: [],
    );
  }

  /// `Forget the password`
  String get ethereum_setting_change_password_forget_the_password {
    return Intl.message(
      'Forget the password',
      name: 'ethereum_setting_change_password_forget_the_password',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect mnemonics`
  String get ethereum_setting_change_password_incorrect_mnemonics {
    return Intl.message(
      'Incorrect mnemonics',
      name: 'ethereum_setting_change_password_incorrect_mnemonics',
      desc: '',
      args: [],
    );
  }

  /// `About us`
  String get ethereum_setting_about_us_title {
    return Intl.message(
      'About us',
      name: 'ethereum_setting_about_us_title',
      desc: '',
      args: [],
    );
  }

  /// `Current version`
  String get ethereum_setting_about_us_current_version {
    return Intl.message(
      'Current version',
      name: 'ethereum_setting_about_us_current_version',
      desc: '',
      args: [],
    );
  }

  /// `Version update`
  String get ethereum_setting_about_us_version_update {
    return Intl.message(
      'Version update',
      name: 'ethereum_setting_about_us_version_update',
      desc: '',
      args: [],
    );
  }

  /// `Next Reminder`
  String get ethereum_setting_update_alert_cancel {
    return Intl.message(
      'Next Reminder',
      name: 'ethereum_setting_update_alert_cancel',
      desc: '',
      args: [],
    );
  }

  /// `To Website`
  String get ethereum_setting_update_alert_confirm {
    return Intl.message(
      'To Website',
      name: 'ethereum_setting_update_alert_confirm',
      desc: '',
      args: [],
    );
  }

  /// `Not Open`
  String get ethereum_setting_gusture_off {
    return Intl.message(
      'Not Open',
      name: 'ethereum_setting_gusture_off',
      desc: '',
      args: [],
    );
  }

  /// `Open`
  String get ethereum_setting_gusture_on {
    return Intl.message(
      'Open',
      name: 'ethereum_setting_gusture_on',
      desc: '',
      args: [],
    );
  }

  /// `Pattern Settings`
  String get ethereum_setting_gusture {
    return Intl.message(
      'Pattern Settings',
      name: 'ethereum_setting_gusture',
      desc: '',
      args: [],
    );
  }

  /// `Pattern Settings`
  String get ethereum_setting_gusture_title {
    return Intl.message(
      'Pattern Settings',
      name: 'ethereum_setting_gusture_title',
      desc: '',
      args: [],
    );
  }

  /// `Pattern Setting`
  String get ethereum_setting_gusture_pattern_setting {
    return Intl.message(
      'Pattern Setting',
      name: 'ethereum_setting_gusture_pattern_setting',
      desc: '',
      args: [],
    );
  }

  /// `Turn off pattern unlock`
  String get ethereum_setting_gusture_turn_off {
    return Intl.message(
      'Turn off pattern unlock',
      name: 'ethereum_setting_gusture_turn_off',
      desc: '',
      args: [],
    );
  }

  /// `Pattern Setting`
  String get ethereum_setting_gusture_setting_title {
    return Intl.message(
      'Pattern Setting',
      name: 'ethereum_setting_gusture_setting_title',
      desc: '',
      args: [],
    );
  }

  /// `Draw pattern password`
  String get ethereum_setting_gusture_setting_tips {
    return Intl.message(
      'Draw pattern password',
      name: 'ethereum_setting_gusture_setting_tips',
      desc: '',
      args: [],
    );
  }

  /// `Confirm pattern password`
  String get ethereum_setting_gusture_setting_confirm {
    return Intl.message(
      'Confirm pattern password',
      name: 'ethereum_setting_gusture_setting_confirm',
      desc: '',
      args: [],
    );
  }

  /// `The two drawings are inconsistent`
  String get ethereum_setting_gusture_setting_confirm_failed {
    return Intl.message(
      'The two drawings are inconsistent',
      name: 'ethereum_setting_gusture_setting_confirm_failed',
      desc: '',
      args: [],
    );
  }

  /// `Set successfully`
  String get ethereum_setting_gusture_setting_confirm_success {
    return Intl.message(
      'Set successfully',
      name: 'ethereum_setting_gusture_setting_confirm_success',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to turn off the pattern unlock`
  String get ethereum_setting_gusture_turn_off_tips {
    return Intl.message(
      'Are you sure you want to turn off the pattern unlock',
      name: 'ethereum_setting_gusture_turn_off_tips',
      desc: '',
      args: [],
    );
  }

  /// `Pattern Certification`
  String get ethereum_setting_gusture_verification_title {
    return Intl.message(
      'Pattern Certification',
      name: 'ethereum_setting_gusture_verification_title',
      desc: '',
      args: [],
    );
  }

  /// `Draw a design`
  String get ethereum_setting_gusture_verification_tips {
    return Intl.message(
      'Draw a design',
      name: 'ethereum_setting_gusture_verification_tips',
      desc: '',
      args: [],
    );
  }

  /// `Pattern error`
  String get ethereum_setting_gusture_verification_tips_failed {
    return Intl.message(
      'Pattern error',
      name: 'ethereum_setting_gusture_verification_tips_failed',
      desc: '',
      args: [],
    );
  }

  /// `Asset information`
  String get ethereum_token_information_title {
    return Intl.message(
      'Asset information',
      name: 'ethereum_token_information_title',
      desc: '',
      args: [],
    );
  }

  /// `Enter token name / token ID / smart contract address`
  String get ethereum_asset_management_hit_text {
    return Intl.message(
      'Enter token name / token ID / smart contract address',
      name: 'ethereum_asset_management_hit_text',
      desc: '',
      args: [],
    );
  }

  /// `Hot`
  String get ethereum_asset_management_searchtype_first {
    return Intl.message(
      'Hot',
      name: 'ethereum_asset_management_searchtype_first',
      desc: '',
      args: [],
    );
  }

  /// `Searchresult`
  String get ethereum_asset_management_searchtype_second {
    return Intl.message(
      'Searchresult',
      name: 'ethereum_asset_management_searchtype_second',
      desc: '',
      args: [],
    );
  }

  /// `Token added`
  String get ethereum_asset_management_searchtype_third {
    return Intl.message(
      'Token added',
      name: 'ethereum_asset_management_searchtype_third',
      desc: '',
      args: [],
    );
  }

  /// `No related tokens were found, you can`
  String get ethereum_asset_management_search_nodata_tips {
    return Intl.message(
      'No related tokens were found, you can',
      name: 'ethereum_asset_management_search_nodata_tips',
      desc: '',
      args: [],
    );
  }

  /// `Submit Token Information`
  String get ethereum_asset_management_search_nodata_submit {
    return Intl.message(
      'Submit Token Information',
      name: 'ethereum_asset_management_search_nodata_submit',
      desc: '',
      args: [],
    );
  }

  /// `Searching on the chain, please wait`
  String get ethereum_asset_management_search_nodata_loading {
    return Intl.message(
      'Searching on the chain, please wait',
      name: 'ethereum_asset_management_search_nodata_loading',
      desc: '',
      args: [],
    );
  }

  /// `Home page assets`
  String get ethereum_asset_management_home_asset_title {
    return Intl.message(
      'Home page assets',
      name: 'ethereum_asset_management_home_asset_title',
      desc: '',
      args: [],
    );
  }

  /// `balance: `
  String get ethereum_asset_management_home_asset_balance {
    return Intl.message(
      'balance: ',
      name: 'ethereum_asset_management_home_asset_balance',
      desc: '',
      args: [],
    );
  }

  /// `My assets`
  String get ethereum_asset_management_home_asset_my_balance {
    return Intl.message(
      'My assets',
      name: 'ethereum_asset_management_home_asset_my_balance',
      desc: '',
      args: [],
    );
  }

  /// ` new assets`
  String get ethereum_asset_management_my_balance_show_num {
    return Intl.message(
      ' new assets',
      name: 'ethereum_asset_management_my_balance_show_num',
      desc: '',
      args: [],
    );
  }

  /// `ignore`
  String get ethereum_asset_management_my_balance_show_ignore {
    return Intl.message(
      'ignore',
      name: 'ethereum_asset_management_my_balance_show_ignore',
      desc: '',
      args: [],
    );
  }

  /// `New assets`
  String get ethereum_asset_management_my_balance_new_asset {
    return Intl.message(
      'New assets',
      name: 'ethereum_asset_management_my_balance_new_asset',
      desc: '',
      args: [],
    );
  }

  /// `Assets list`
  String get ethereum_asset_management_my_balance_assetList {
    return Intl.message(
      'Assets list',
      name: 'ethereum_asset_management_my_balance_assetList',
      desc: '',
      args: [],
    );
  }

  /// `Hot`
  String get ethereum_nft_asset_management_searchtype_first {
    return Intl.message(
      'Hot',
      name: 'ethereum_nft_asset_management_searchtype_first',
      desc: '',
      args: [],
    );
  }

  /// `Enter smart contract address`
  String get ethereum_nft_asset_management_hit_text {
    return Intl.message(
      'Enter smart contract address',
      name: 'ethereum_nft_asset_management_hit_text',
      desc: '',
      args: [],
    );
  }

  /// `Home NFT`
  String get ethereum_nft_asset_management_home_asset_title {
    return Intl.message(
      'Home NFT',
      name: 'ethereum_nft_asset_management_home_asset_title',
      desc: '',
      args: [],
    );
  }

  /// `All NFTs`
  String get ethereum_nft_asset_management_home_asset_my_balance {
    return Intl.message(
      'All NFTs',
      name: 'ethereum_nft_asset_management_home_asset_my_balance',
      desc: '',
      args: [],
    );
  }

  /// `NFT Details`
  String get ethereum_nft_asset_management_nft_detail {
    return Intl.message(
      'NFT Details',
      name: 'ethereum_nft_asset_management_nft_detail',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Record`
  String get ethereum_nft_asset_management_transaction_record {
    return Intl.message(
      'Transaction Record',
      name: 'ethereum_nft_asset_management_transaction_record',
      desc: '',
      args: [],
    );
  }

  /// `Send NFT`
  String get ethereum_nft_asset_management_send_nft {
    return Intl.message(
      'Send NFT',
      name: 'ethereum_nft_asset_management_send_nft',
      desc: '',
      args: [],
    );
  }

  /// `Save to album`
  String get ethereum_assets_qrcode_save_to_album {
    return Intl.message(
      'Save to album',
      name: 'ethereum_assets_qrcode_save_to_album',
      desc: '',
      args: [],
    );
  }

  /// `In circulation`
  String get ethereum_token_information_asset_state_in_circulation {
    return Intl.message(
      'In circulation',
      name: 'ethereum_token_information_asset_state_in_circulation',
      desc: '',
      args: [],
    );
  }

  /// `Asset name`
  String get ethereum_token_information_asset_name {
    return Intl.message(
      'Asset name',
      name: 'ethereum_token_information_asset_name',
      desc: '',
      args: [],
    );
  }

  /// `URL`
  String get ethereum_token_information_asset_url {
    return Intl.message(
      'URL',
      name: 'ethereum_token_information_asset_url',
      desc: '',
      args: [],
    );
  }

  /// `Contract address`
  String get ethereum_token_information_contract_address {
    return Intl.message(
      'Contract address',
      name: 'ethereum_token_information_contract_address',
      desc: '',
      args: [],
    );
  }

  /// `Accuracy`
  String get ethereum_token_information_accuracy {
    return Intl.message(
      'Accuracy',
      name: 'ethereum_token_information_accuracy',
      desc: '',
      args: [],
    );
  }

  /// `Start time`
  String get ethereum_token_information_start_time {
    return Intl.message(
      'Start time',
      name: 'ethereum_token_information_start_time',
      desc: '',
      args: [],
    );
  }

  /// `Total issue`
  String get ethereum_token_information_total_issue {
    return Intl.message(
      'Total issue',
      name: 'ethereum_token_information_total_issue',
      desc: '',
      args: [],
    );
  }

  /// `Project Introduction`
  String get ethereum_token_information_introduce {
    return Intl.message(
      'Project Introduction',
      name: 'ethereum_token_information_introduce',
      desc: '',
      args: [],
    );
  }

  /// `There is no transaction record at present`
  String get ethereum_trade_record_empty {
    return Intl.message(
      'There is no transaction record at present',
      name: 'ethereum_trade_record_empty',
      desc: '',
      args: [],
    );
  }

  /// `Transfer address`
  String get ethereum_trade_toadress {
    return Intl.message(
      'Transfer address',
      name: 'ethereum_trade_toadress',
      desc: '',
      args: [],
    );
  }

  /// `Transfer amount`
  String get ethereum_trade_amount {
    return Intl.message(
      'Transfer amount',
      name: 'ethereum_trade_amount',
      desc: '',
      args: [],
    );
  }

  /// `View more records >`
  String get ethereum_trade_history_more {
    return Intl.message(
      'View more records >',
      name: 'ethereum_trade_history_more',
      desc: '',
      args: [],
    );
  }

  /// `Available balance`
  String get ethereum_trade_available_balance {
    return Intl.message(
      'Available balance',
      name: 'ethereum_trade_available_balance',
      desc: '',
      args: [],
    );
  }

  /// `Transaction details`
  String get ethereum_trade_detail_title {
    return Intl.message(
      'Transaction details',
      name: 'ethereum_trade_detail_title',
      desc: '',
      args: [],
    );
  }

  /// `Successful trade`
  String get ethereum_trade_detail_success {
    return Intl.message(
      'Successful trade',
      name: 'ethereum_trade_detail_success',
      desc: '',
      args: [],
    );
  }

  /// `Transaction time`
  String get ethereum_trade_detail_transaction_time {
    return Intl.message(
      'Transaction time',
      name: 'ethereum_trade_detail_transaction_time',
      desc: '',
      args: [],
    );
  }

  /// `Transaction type`
  String get ethereum_trade_detail_transaction_type {
    return Intl.message(
      'Transaction type',
      name: 'ethereum_trade_detail_transaction_type',
      desc: '',
      args: [],
    );
  }

  /// `Transfer accounts`
  String get ethereum_trade_detail_transfer_accounts {
    return Intl.message(
      'Transfer accounts',
      name: 'ethereum_trade_detail_transfer_accounts',
      desc: '',
      args: [],
    );
  }

  /// `Receiving address`
  String get ethereum_trade_detail_receiving_address {
    return Intl.message(
      'Receiving address',
      name: 'ethereum_trade_detail_receiving_address',
      desc: '',
      args: [],
    );
  }

  /// `Sending address`
  String get ethereum_trade_detail_sending_address {
    return Intl.message(
      'Sending address',
      name: 'ethereum_trade_detail_sending_address',
      desc: '',
      args: [],
    );
  }

  /// `Miner's fee`
  String get ethereum_trade_detail_miners_fee {
    return Intl.message(
      'Miner\'s fee',
      name: 'ethereum_trade_detail_miners_fee',
      desc: '',
      args: [],
    );
  }

  /// `Transaction number`
  String get ethereum_trade_detail_transaction_number {
    return Intl.message(
      'Transaction number',
      name: 'ethereum_trade_detail_transaction_number',
      desc: '',
      args: [],
    );
  }

  /// `Block`
  String get ethereum_trade_detail_block {
    return Intl.message(
      'Block',
      name: 'ethereum_trade_detail_block',
      desc: '',
      args: [],
    );
  }

  /// `Go to the block browser for details`
  String get ethereum_trade_detail_jump_title {
    return Intl.message(
      'Go to the block browser for details',
      name: 'ethereum_trade_detail_jump_title',
      desc: '',
      args: [],
    );
  }

  /// `Enable fingerprint verification`
  String get ethereum_trade_fingerprint_enable {
    return Intl.message(
      'Enable fingerprint verification',
      name: 'ethereum_trade_fingerprint_enable',
      desc: '',
      args: [],
    );
  }

  /// `Fingerprint verification`
  String get ethereum_trade_fingerprint_title {
    return Intl.message(
      'Fingerprint verification',
      name: 'ethereum_trade_fingerprint_title',
      desc: '',
      args: [],
    );
  }

  /// `Fingerprint Payment`
  String get ethereum_trade_fingerprint_pay {
    return Intl.message(
      'Fingerprint Payment',
      name: 'ethereum_trade_fingerprint_pay',
      desc: '',
      args: [],
    );
  }

  /// `Please perform fingerprint identification`
  String get ethereum_trade_fingerprint_tip {
    return Intl.message(
      'Please perform fingerprint identification',
      name: 'ethereum_trade_fingerprint_tip',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get ethereum_trade_fingerprint_cancel {
    return Intl.message(
      'Cancel',
      name: 'ethereum_trade_fingerprint_cancel',
      desc: '',
      args: [],
    );
  }

  /// `Please wait 30 seconds before doing this`
  String get ethereum_trade_fingerprint_local {
    return Intl.message(
      'Please wait 30 seconds before doing this',
      name: 'ethereum_trade_fingerprint_local',
      desc: '',
      args: [],
    );
  }

  /// `Please set fingerprint information in the mobile phone system`
  String get ethereum_trade_fingerprint_notEnrolled {
    return Intl.message(
      'Please set fingerprint information in the mobile phone system',
      name: 'ethereum_trade_fingerprint_notEnrolled',
      desc: '',
      args: [],
    );
  }

  /// `Too many authentication times have been prohibited. Please go to the mobile phone system to confirm the fingerprint information.`
  String get ethereum_trade_fingerprint_permanentlyLockedOut {
    return Intl.message(
      'Too many authentication times have been prohibited. Please go to the mobile phone system to confirm the fingerprint information.',
      name: 'ethereum_trade_fingerprint_permanentlyLockedOut',
      desc: '',
      args: [],
    );
  }

  /// `The fingerprint has been cancelled or the device does not support fingerprints`
  String get ethereum_trade_fingerprint_not {
    return Intl.message(
      'The fingerprint has been cancelled or the device does not support fingerprints',
      name: 'ethereum_trade_fingerprint_not',
      desc: '',
      args: [],
    );
  }

  /// `0verview`
  String get ethereum_trade_head {
    return Intl.message(
      '0verview',
      name: 'ethereum_trade_head',
      desc: '',
      args: [],
    );
  }

  /// `There is no transaction record`
  String get ethereum_trade_transactions {
    return Intl.message(
      'There is no transaction record',
      name: 'ethereum_trade_transactions',
      desc: '',
      args: [],
    );
  }

  /// `Dapp Browser`
  String get ethereum_dapp_brower {
    return Intl.message(
      'Dapp Browser',
      name: 'ethereum_dapp_brower',
      desc: '',
      args: [],
    );
  }

  /// `Hot search`
  String get ethereum_dapp_index_hot_search {
    return Intl.message(
      'Hot search',
      name: 'ethereum_dapp_index_hot_search',
      desc: '',
      args: [],
    );
  }

  /// `Hot search`
  String get ethereum_dapp_hot_search {
    return Intl.message(
      'Hot search',
      name: 'ethereum_dapp_hot_search',
      desc: '',
      args: [],
    );
  }

  /// `Search history`
  String get ethereum_dapp_search_history {
    return Intl.message(
      'Search history',
      name: 'ethereum_dapp_search_history',
      desc: '',
      args: [],
    );
  }

  /// `No DAPP found`
  String get ethereum_dapp_search_tips {
    return Intl.message(
      'No DAPP found',
      name: 'ethereum_dapp_search_tips',
      desc: '',
      args: [],
    );
  }

  /// `Search result`
  String get ethereum_dapp_search_result {
    return Intl.message(
      'Search result',
      name: 'ethereum_dapp_search_result',
      desc: '',
      args: [],
    );
  }

  /// `copy`
  String get ethereum_dapp_search_menu_bar_copy {
    return Intl.message(
      'copy',
      name: 'ethereum_dapp_search_menu_bar_copy',
      desc: '',
      args: [],
    );
  }

  /// `delete`
  String get ethereum_dapp_search_menu_bar_delete {
    return Intl.message(
      'delete',
      name: 'ethereum_dapp_search_menu_bar_delete',
      desc: '',
      args: [],
    );
  }

  /// `Going to a third-party website`
  String get ethereum_dapp_search_loading {
    return Intl.message(
      'Going to a third-party website',
      name: 'ethereum_dapp_search_loading',
      desc: '',
      args: [],
    );
  }

  /// `Access instructions`
  String get ethereum_dapp_access_instructions {
    return Intl.message(
      'Access instructions',
      name: 'ethereum_dapp_access_instructions',
      desc: '',
      args: [],
    );
  }

  /// `You are visiting a third party page`
  String get ethereum_dapp_title {
    return Intl.message(
      'You are visiting a third party page',
      name: 'ethereum_dapp_title',
      desc: '',
      args: [],
    );
  }

  /// `Sign Out`
  String get ethereum_dapp_back {
    return Intl.message(
      'Sign Out',
      name: 'ethereum_dapp_back',
      desc: '',
      args: [],
    );
  }

  /// `Refresh`
  String get ethereum_dapp_menu_refresh {
    return Intl.message(
      'Refresh',
      name: 'ethereum_dapp_menu_refresh',
      desc: '',
      args: [],
    );
  }

  /// `Copy Link`
  String get ethereum_dapp_menu_copy {
    return Intl.message(
      'Copy Link',
      name: 'ethereum_dapp_menu_copy',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get ethereum_dapp_menu_share {
    return Intl.message(
      'Share',
      name: 'ethereum_dapp_menu_share',
      desc: '',
      args: [],
    );
  }

  /// `Browser Open`
  String get ethereum_dapp_menu_browser_open {
    return Intl.message(
      'Browser Open',
      name: 'ethereum_dapp_menu_browser_open',
      desc: '',
      args: [],
    );
  }

  /// `TP Password`
  String get ethereum_dapp_menu_browser_tp {
    return Intl.message(
      'TP Password',
      name: 'ethereum_dapp_menu_browser_tp',
      desc: '',
      args: [],
    );
  }

  /// `Report`
  String get ethereum_dapp_menu_browser_report {
    return Intl.message(
      'Report',
      name: 'ethereum_dapp_menu_browser_report',
      desc: '',
      args: [],
    );
  }

  /// `Popular`
  String get ethereum_dapp_menu_hot {
    return Intl.message(
      'Popular',
      name: 'ethereum_dapp_menu_hot',
      desc: '',
      args: [],
    );
  }

  /// `Scroll down to see recently used`
  String get ethereum_dapp_xiala {
    return Intl.message(
      'Scroll down to see recently used',
      name: 'ethereum_dapp_xiala',
      desc: '',
      args: [],
    );
  }

  /// `Recently used DApps`
  String get ethereum_dapp_hostory_title {
    return Intl.message(
      'Recently used DApps',
      name: 'ethereum_dapp_hostory_title',
      desc: '',
      args: [],
    );
  }

  /// `More`
  String get ethereum_dapp_hostory_more {
    return Intl.message(
      'More',
      name: 'ethereum_dapp_hostory_more',
      desc: '',
      args: [],
    );
  }

  /// `Create New Folder`
  String get ethereum_dapp_collection_create_newfile {
    return Intl.message(
      'Create New Folder',
      name: 'ethereum_dapp_collection_create_newfile',
      desc: '',
      args: [],
    );
  }

  /// `Manage My DApps`
  String get ethereum_dapp_collection_message_dapp {
    return Intl.message(
      'Manage My DApps',
      name: 'ethereum_dapp_collection_message_dapp',
      desc: '',
      args: [],
    );
  }

  /// `Folder Management`
  String get ethereum_dapp_collection_file_message {
    return Intl.message(
      'Folder Management',
      name: 'ethereum_dapp_collection_file_message',
      desc: '',
      args: [],
    );
  }

  /// `Collection`
  String get ethereum_dapp_collection_mycollection {
    return Intl.message(
      'Collection',
      name: 'ethereum_dapp_collection_mycollection',
      desc: '',
      args: [],
    );
  }

  /// `Collection of success`
  String get ethereum_dapp_collection_collection_success {
    return Intl.message(
      'Collection of success',
      name: 'ethereum_dapp_collection_collection_success',
      desc: '',
      args: [],
    );
  }

  /// `New Folder Name`
  String get ethereum_dapp_collection_create_newfile_name {
    return Intl.message(
      'New Folder Name',
      name: 'ethereum_dapp_collection_create_newfile_name',
      desc: '',
      args: [],
    );
  }

  /// `Please Tick DApp`
  String get ethereum_dapp_collection_create_checkout {
    return Intl.message(
      'Please Tick DApp',
      name: 'ethereum_dapp_collection_create_checkout',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in the folder name`
  String get ethereum_dapp_collection_create_not_input {
    return Intl.message(
      'Please fill in the folder name',
      name: 'ethereum_dapp_collection_create_not_input',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get ethereum_dapp_collection_sheet_title_1 {
    return Intl.message(
      'Create',
      name: 'ethereum_dapp_collection_sheet_title_1',
      desc: '',
      args: [],
    );
  }

  /// `Revise`
  String get ethereum_dapp_collection_sheet_title_2 {
    return Intl.message(
      'Revise',
      name: 'ethereum_dapp_collection_sheet_title_2',
      desc: '',
      args: [],
    );
  }

  /// `Please collect DAPP first`
  String get ethereum_dapp_collection_not_collection {
    return Intl.message(
      'Please collect DAPP first',
      name: 'ethereum_dapp_collection_not_collection',
      desc: '',
      args: [],
    );
  }

  /// `How to collect DAPP?`
  String get ethereum_dapp_collection_help {
    return Intl.message(
      'How to collect DAPP?',
      name: 'ethereum_dapp_collection_help',
      desc: '',
      args: [],
    );
  }

  /// `Your use of the third-party page will be subject to your policies and regulations, as well as the user agreement and privacy policy of the third-party page. Third party links are directly and solely responsible to you`
  String get ethereum_dapp_describe {
    return Intl.message(
      'Your use of the third-party page will be subject to your policies and regulations, as well as the user agreement and privacy policy of the third-party page. Third party links are directly and solely responsible to you',
      name: 'ethereum_dapp_describe',
      desc: '',
      args: [],
    );
  }

  /// `There is no usage record`
  String get ethereum_dapp_history_nodata {
    return Intl.message(
      'There is no usage record',
      name: 'ethereum_dapp_history_nodata',
      desc: '',
      args: [],
    );
  }

  /// `Update content:`
  String get ethereum_setting_updata_title {
    return Intl.message(
      'Update content:',
      name: 'ethereum_setting_updata_title',
      desc: '',
      args: [],
    );
  }

  /// `Update now`
  String get ethereum_setting_updata_buttom_title {
    return Intl.message(
      'Update now',
      name: 'ethereum_setting_updata_buttom_title',
      desc: '',
      args: [],
    );
  }

  /// `New Version`
  String get ethereum_setting_updata_find_title {
    return Intl.message(
      'New Version',
      name: 'ethereum_setting_updata_find_title',
      desc: '',
      args: [],
    );
  }

  /// `This is the latest version`
  String get ethereum_setting_updata_tips {
    return Intl.message(
      'This is the latest version',
      name: 'ethereum_setting_updata_tips',
      desc: '',
      args: [],
    );
  }

  /// `One purse, the whole world`
  String get ethereum_welcome_firsttitle {
    return Intl.message(
      'One purse, the whole world',
      name: 'ethereum_welcome_firsttitle',
      desc: '',
      args: [],
    );
  }

  /// `Blockchain ecological entrance, the latestThe hottest dapps are all here`
  String get ethereum_welcome_firstsubtitle {
    return Intl.message(
      'Blockchain ecological entrance, the latestThe hottest dapps are all here',
      name: 'ethereum_welcome_firstsubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Private security, open source Wallet`
  String get ethereum_welcome_secondtitle {
    return Intl.message(
      'Private security, open source Wallet',
      name: 'ethereum_welcome_secondtitle',
      desc: '',
      args: [],
    );
  }

  /// `Decentralised wallet, safe and assured Private key self holding, multiple encryption`
  String get ethereum_welcome_secondsubtitle {
    return Intl.message(
      'Decentralised wallet, safe and assured Private key self holding, multiple encryption',
      name: 'ethereum_welcome_secondsubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Multi Chain Wallet, easy to use`
  String get ethereum_welcome_thirdtitle {
    return Intl.message(
      'Multi Chain Wallet, easy to use',
      name: 'ethereum_welcome_thirdtitle',
      desc: '',
      args: [],
    );
  }

  /// `Multi currency management, cross chain exchange One stop solution to your trading needs`
  String get ethereum_welcome_thirdsubtitle {
    return Intl.message(
      'Multi currency management, cross chain exchange One stop solution to your trading needs',
      name: 'ethereum_welcome_thirdsubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Fee`
  String get ethereum_utils_fee_config_board_title {
    return Intl.message(
      'Fee',
      name: 'ethereum_utils_fee_config_board_title',
      desc: '',
      args: [],
    );
  }

  /// `Safe`
  String get ethereum_utils_fee_config_board_slow {
    return Intl.message(
      'Safe',
      name: 'ethereum_utils_fee_config_board_slow',
      desc: '',
      args: [],
    );
  }

  /// `Propose`
  String get ethereum_utils_fee_config_board_propose {
    return Intl.message(
      'Propose',
      name: 'ethereum_utils_fee_config_board_propose',
      desc: '',
      args: [],
    );
  }

  /// `Fast`
  String get ethereum_utils_fee_config_board_fast {
    return Intl.message(
      'Fast',
      name: 'ethereum_utils_fee_config_board_fast',
      desc: '',
      args: [],
    );
  }

  /// `Bridge`
  String get ethereum_tab_bridge {
    return Intl.message(
      'Bridge',
      name: 'ethereum_tab_bridge',
      desc: '',
      args: [],
    );
  }

  /// `How to store DApps`
  String get help_how_collection {
    return Intl.message(
      'How to store DApps',
      name: 'help_how_collection',
      desc: '',
      args: [],
    );
  }

  /// `HashPay introduction?`
  String get help_appbar_title {
    return Intl.message(
      'HashPay introduction?',
      name: 'help_appbar_title',
      desc: '',
      args: [],
    );
  }

  /// `The new wallet has a big revision in the [Discover] interface, but also added some new elements, users can use according to their own habits; In addition, dAPPS commonly used and loved in daily life can be classified into a folder for easy management and viewing, which is very similar to the operation on the mobile phone system. Let's explain a function of DApp collection below.`
  String get help_tips {
    return Intl.message(
      'The new wallet has a big revision in the [Discover] interface, but also added some new elements, users can use according to their own habits; In addition, dAPPS commonly used and loved in daily life can be classified into a folder for easy management and viewing, which is very similar to the operation on the mobile phone system. Let\'s explain a function of DApp collection below.',
      name: 'help_tips',
      desc: '',
      args: [],
    );
  }

  /// `1. DApp collection function:`
  String get help_title_1 {
    return Intl.message(
      '1. DApp collection function:',
      name: 'help_title_1',
      desc: '',
      args: [],
    );
  }

  /// `1.1. Open HashPay wallet and click [Discover] to select the function of [DApp] or [Search bar] at the top. You can select  fill in the link to open it.`
  String get help_title_content_1 {
    return Intl.message(
      '1.1. Open HashPay wallet and click [Discover] to select the function of [DApp] or [Search bar] at the top. You can select  fill in the link to open it.',
      name: 'help_title_content_1',
      desc: '',
      args: [],
    );
  }

  /// `1.2. After opening the link, click [Menu] in the upper right corner and select [Favorites] in the window that pops up at the bottom. After completing the operation, you can return to the [Discover] interface and see the records of favorites in my DApp.`
  String get help_title_content_2 {
    return Intl.message(
      '1.2. After opening the link, click [Menu] in the upper right corner and select [Favorites] in the window that pops up at the bottom. After completing the operation, you can return to the [Discover] interface and see the records of favorites in my DApp.',
      name: 'help_title_content_2',
      desc: '',
      args: [],
    );
  }

  /// `Fee Limit`
  String get tron_home_asset_transfer_feelimit {
    return Intl.message(
      'Fee Limit',
      name: 'tron_home_asset_transfer_feelimit',
      desc: '',
      args: [],
    );
  }

  /// `Import network`
  String get deeplink_network_import_title {
    return Intl.message(
      'Import network',
      name: 'deeplink_network_import_title',
      desc: '',
      args: [],
    );
  }

  /// `Do you agree to add the network to your wallet?`
  String get deeplink_network_import_tips {
    return Intl.message(
      'Do you agree to add the network to your wallet?',
      name: 'deeplink_network_import_tips',
      desc: '',
      args: [],
    );
  }

  /// `Network name`
  String get deeplink_network_import_network_name {
    return Intl.message(
      'Network name',
      name: 'deeplink_network_import_network_name',
      desc: '',
      args: [],
    );
  }

  /// `Symbol`
  String get deeplink_network_import_network_symbol {
    return Intl.message(
      'Symbol',
      name: 'deeplink_network_import_network_symbol',
      desc: '',
      args: [],
    );
  }

  /// `Accuracy`
  String get deeplink_network_import_network_decimals {
    return Intl.message(
      'Accuracy',
      name: 'deeplink_network_import_network_decimals',
      desc: '',
      args: [],
    );
  }

  /// `Block browser`
  String get deeplink_network_import_network_scan {
    return Intl.message(
      'Block browser',
      name: 'deeplink_network_import_network_scan',
      desc: '',
      args: [],
    );
  }

  /// `I have read and agree to the《add network Disclaimer》`
  String get deeplink_network_import_network_disclaimers_tips {
    return Intl.message(
      'I have read and agree to the《add network Disclaimer》',
      name: 'deeplink_network_import_network_disclaimers_tips',
      desc: '',
      args: [],
    );
  }

  /// `Confirm add`
  String get deeplink_network_import_network_disclaimers_agree {
    return Intl.message(
      'Confirm add',
      name: 'deeplink_network_import_network_disclaimers_agree',
      desc: '',
      args: [],
    );
  }

  /// `Do not add`
  String get deeplink_network_import_network_disclaimers_reject {
    return Intl.message(
      'Do not add',
      name: 'deeplink_network_import_network_disclaimers_reject',
      desc: '',
      args: [],
    );
  }

  /// `The network already exists, please do not add it repeatedly`
  String get deeplink_network_import_network_already_exist {
    return Intl.message(
      'The network already exists, please do not add it repeatedly',
      name: 'deeplink_network_import_network_already_exist',
      desc: '',
      args: [],
    );
  }

  /// `Network management`
  String get deeplink_network_add {
    return Intl.message(
      'Network management',
      name: 'deeplink_network_add',
      desc: '',
      args: [],
    );
  }

  /// `Chain identifier`
  String get deeplink_network_add_identifier {
    return Intl.message(
      'Chain identifier',
      name: 'deeplink_network_add_identifier',
      desc: '',
      args: [],
    );
  }

  /// `Chain name`
  String get deeplink_network_add_chain_name {
    return Intl.message(
      'Chain name',
      name: 'deeplink_network_add_chain_name',
      desc: '',
      args: [],
    );
  }

  /// `Network ID`
  String get deeplink_network_add_id {
    return Intl.message(
      'Network ID',
      name: 'deeplink_network_add_id',
      desc: '',
      args: [],
    );
  }

  /// `MainSymbol`
  String get deeplink_network_add_mainSymbol {
    return Intl.message(
      'MainSymbol',
      name: 'deeplink_network_add_mainSymbol',
      desc: '',
      args: [],
    );
  }

  /// `Logo link address`
  String get deeplink_network_add_logo_link_address {
    return Intl.message(
      'Logo link address',
      name: 'deeplink_network_add_logo_link_address',
      desc: '',
      args: [],
    );
  }

  /// `Node server address`
  String get deeplink_network_add_node_server_address {
    return Intl.message(
      'Node server address',
      name: 'deeplink_network_add_node_server_address',
      desc: '',
      args: [],
    );
  }

  /// `The value of the item with * cannot be empty`
  String get deeplink_network_add_empty {
    return Intl.message(
      'The value of the item with * cannot be empty',
      name: 'deeplink_network_add_empty',
      desc: '',
      args: [],
    );
  }

  /// `Add a user-defined network`
  String get deeplink_network_add_custom {
    return Intl.message(
      'Add a user-defined network',
      name: 'deeplink_network_add_custom',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to add this DT chain?`
  String get deeplink_network_add_confirm_tips {
    return Intl.message(
      'Are you sure you want to add this DT chain?',
      name: 'deeplink_network_add_confirm_tips',
      desc: '',
      args: [],
    );
  }

  /// `0.0.1`
  String get version {
    return Intl.message(
      '0.0.1',
      name: 'version',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'zh'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
