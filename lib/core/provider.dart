// @dart=2.9
part of 'core.dart';

class CoreProvider extends ChangeNotifier {
  static CoreProvider _instance;

  /// 账户切换监听
  ValueNotifier<WalletKey> _accountListenable;
  ValueNotifier<WalletKey> get accountListenable => _accountListenable;

  /// 钱包切换监听
  ValueNotifier<Wallet> _walletListenable;
  ValueNotifier<Wallet> get walletListenable => _walletListenable;

  /// 网络类型切换监听
  ValueNotifier<NetworkConfig> _networkConfigListenable;
  ValueNotifier<NetworkConfig> get networkConfigListenable =>
      _networkConfigListenable;

  /// 节点切换监听
  ValueNotifier<EndPoint> _endPointListenable;
  ValueNotifier<EndPoint> get endPointListenable => _endPointListenable;

  factory CoreProvider.of(BuildContext context) =>
      Provider.of<CoreProvider>(context, listen: false);

  ChainCore get chainCore => _networkConfigListenable.value.chainCore;

  /// Account getter / setter
  WalletKey get account => _accountListenable.value;
  set account(WalletKey account) {
    _accountListenable.value = account;
    ConfigStorageUtils().write<int>(
      ConfigKey.SelectedKeyIndex,
      wallet.keysOf(chainCore).indexOf(account),
    );
  }

  /// Wallet getter / setter
  Wallet get wallet => _walletListenable.value;
  set wallet(Wallet value) {
    if (wallet == value) {
      return;
    }

    HDWalletStateMachine.loadWallet(mnemonic: value.mnemonic);

    ConfigStorageUtils().write<String>(
      ConfigKey.SelectedWalletIdentifiere,
      value.identifier,
    );

    /// 卸载之前的存储回调
    if (_walletListenable.value != null) {
      _walletListenable.value.storagedCallBack = null;
    }

    /// 当前对应的链类型中没有任何地址则自动生存，有则使用第一个地址
    if (value.keysOf(chainCore).length > 0) {
      account = value.keysOf(chainCore).first;
    } else {
      Future.sync(() async {
        account = await value.newKeypair(type: chainCore);
      });
    }

    /// 该方法回调用_walletListenable.notifyListeners()所以需要在装载好account后执行
    _walletListenable.value = value;

    /// 实现Wallet存储回调
    value.storagedCallBack = (wallet, [fromDelete]) {
      _accountListenable.notifyListeners();
    };
  }

  /// EndPoint getter / setter
  EndPoint get endPoint => _endPointListenable.value;
  set endPoint(EndPoint value) {
    if (endPoint == value) {
      return;
    }

    // final index = networkConfig.endpoints.indexOf(value);
    // if (index < 0) {
    //   throw 'set endPoint, not exist in currentt network config';
    // }
    _endPointListenable.notifyListeners();
    _endPointListenable.value = value;

    // networkConfig.save();
  }

  NetworkConfig get networkConfig => _networkConfigListenable.value;
  set networkConfig(NetworkConfig config) {
    if (_networkConfigListenable.value != null) {
      _networkConfigListenable.value.endPointChangedCallBack = null;
    }

    config.endPointChangedCallBack = () {
      _endPointListenable.notifyListeners();
    };

    _networkConfigListenable.value = config;

    /// 切换了网络时也该i正确赋值endpoint，让对应监听的页面重新获取数据
    _endPointListenable.value = _networkConfigListenable.value.currenEndPoint;
  }

  factory CoreProvider() {
    assert(
      _instance != null,
      '\nEnsure to initialize CoreProvider before accessing it.\nPlease execute the init method : CoreProvider.init()',
    );

    return _instance;
  }

  /// 恢复上一次退出程序时的状态
  CoreProvider._() {
    /// 网络配置相关, 若没有该对象，说明是第一次启动,使用配置文件中的第一个网络配置
    final networkIdentifier = ConfigStorageUtils().read<String>(
      ConfigKey.SelectedNetworkIdentifier,
      defaultValue: NetworkStorageUtils().defaultConfig.identifier,
    );

    if (NetworkStorageUtils().chainNames.contains(networkIdentifier)) {
      /// 存在相关配置
      _networkConfigListenable = ValueNotifier<NetworkConfig>(
        NetworkStorageUtils().networkConfigOf(networkIdentifier),
      );
    } else {
      /// 使用默认网络配置
      _networkConfigListenable = ValueNotifier<NetworkConfig>(
        NetworkStorageUtils().defaultConfig,
      );
    }

    assert(
      _networkConfigListenable.value != null,
      '\nThe default network configuration, please check the configuration file.',
    );

    _endPointListenable = ValueNotifier<EndPoint>(
      _networkConfigListenable.value.currenEndPoint,
    );

    /// 节点切换回调
    _networkConfigListenable.value.endPointChangedCallBack = () {
      _endPointListenable.notifyListeners();
    };

    /// 身份相关配置
    final identifier = ConfigStorageUtils().read<String>(
      ConfigKey.SelectedWalletIdentifiere,
      defaultValue: null,
    );

    _walletListenable = ValueNotifier<Wallet>(
      _WalletStorageUtils().walletOf(identifier, true),
    );

    if (_walletListenable.value != null) {
      /// 装载到状态机
      HDWalletStateMachine.loadWallet(
        mnemonic: _walletListenable.value.mnemonic,
      );

      /// 存储回调
      _walletListenable.value.storagedCallBack = (wallet, [fromDelete]) {
        _accountListenable.notifyListeners();
      };

      final index = ConfigStorageUtils().read<int>(
        ConfigKey.SelectedKeyIndex,
        defaultValue: 0,
      );

      final keys = wallet.keysOf(chainCore);
      print(keys.length);
      keys.length > 0
          ? _accountListenable = ValueNotifier<WalletKey>(
              index < keys.length && index > 0 ? keys[index] : keys.first,
              // keys.first,
            )
          : _accountListenable = ValueNotifier<WalletKey>(null);
    } else {
      _accountListenable = ValueNotifier<WalletKey>(null);
    }
  }

  static Future<void> init() async {
    await _WalletStorageUtils.init();
    await ConfigStorageUtils.init();
    await NetworkStorageUtils.init();
    await SeeWalletStorageUtils.init();

    _instance = CoreProvider._();
  }

  /// 钱包相关操作类型
  // List<String> get walletIdentifiers => _WalletStorageUtils().identifiers;

  void removeAccount(WalletKey accountKey) {
    accountKey.isDeleted = true;

    /// 处理边界问题
    final keys = wallet.keysOf(chainCore);
    int index = ConfigStorageUtils().read<int>(ConfigKey.SelectedKeyIndex);
    if (index >= keys.length) {
      index = keys.length - 1;
    }

    ConfigStorageUtils().write<int>(
      ConfigKey.SelectedKeyIndex,
      keys.length - 1,
    );

    account = wallet.keysOf(chainCore)[index];
  }

  Future<void> generateRootKeypair(String identifier, String security,
      [bool successOnload = false]) async {
    final newWallet =
        await _WalletStorageUtils().generateRootKeypair(identifier, security);

    /// 直接装载到Provider中
    if (successOnload) {
      wallet = newWallet;
    }
  }

  Future<void> importRootKeypair(
    String idenfitier,
    List<String> mnemonicWords,
    String security, [
    bool successOnload = false,
  ]) async {
    final newWallet = await _WalletStorageUtils().importRootKeypair(
      idenfitier,
      mnemonicWords.join(' '),
      security,
    );

    /// 直接装载到Provider中
    if (successOnload) {
      wallet = newWallet;
    }
  }

  Future<String> exportRootKeypairMnemonic(
          String identifieir, String security) =>
      _WalletStorageUtils().exportRootKeypairMnemonic(identifieir, security);
}
