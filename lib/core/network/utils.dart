// @dart=2.9
part of '../core.dart';

enum NetworkUtilsError {
  ConfigAlreadyExist,
  ConfigNotExist,
}

class NetworkStorageUtils {
  static NetworkStorageUtils _instance;

  Box<NetworkConfig> _networkSandBox;

  NetworkStorageUtils._();

  factory NetworkStorageUtils() {
    assert(
      _instance != null,
      '\nEnsure to initialize _NetworkUtils before accessing it.\nPlease execute the init method : _NetworkUtils.init()',
    );

    return _instance;
  }

  static Future<void> init() async {
    /// !! 此写法不可用，因为TypeAdapter<T>会变为<dynamic>所有类型都会满足 obj.type == dynamic
    /// 导致找不到对应的适配器（Adapter）
    /// <TypeAdapter>[
    ///   NetworkConfigAdapter(),
    ///   EndPointAdapter(),
    /// ].forEach(Hive.registerAdapter);
    Hive.registerAdapter(NetworkConfigAdapter());
    Hive.registerAdapter(EndPointAdapter());

    _instance ??= NetworkStorageUtils._();
    _instance._networkSandBox =
        await Hive.openBox<NetworkConfig>('NetworkConfigSandBox');

    List networkConfig =
        json.decode(await rootBundle.loadString('configuration/network.json'));

    //针对旧版hashpay本地数据存储问题
    if (_instance._networkSandBox.values.length > 0) {
      if (_instance._networkSandBox.values.first.source == null) {
        await _instance._networkSandBox.clear();
      }
    }

    for (var i = 0; i < networkConfig.length; i++) {
      final config = NetworkConfig.fromMap(networkConfig[i]);

      final existIndex =
          _instance._networkSandBox.values.toList().indexOf(config);

      if (_instance._networkSandBox.length >= networkConfig.length) {
        _instance._networkSandBox.putAt(i, config);
      } else {
        _instance._networkSandBox.add(config);
      }

      // if (existIndex >= 0) {

      // } else {
      //   _instance._networkSandBox.add(config);
      // }
    }
  }

  List<String> get chainNames =>
      _networkSandBox.values.map((e) => e.identifier).toList();

  List<int> get selectEndpointIndex =>
      _networkSandBox.values.map((e) => e.selectEndpointIndex).toList();

  Future<void> putNetwork(String identifier, NetworkConfig config,
      [bool replace = true]) {
    final int index = _networkSandBox.values.toList().indexWhere(
          (config) =>
              config.identifier.toLowerCase() == identifier.toLowerCase(),
        );

    // print(_networkSandBox.values.map((e) => print(e.identifier)));

    if (index >= 0) {
      if (!replace)
        throw NetworkUtilsError.ConfigAlreadyExist;
      else {
        _networkSandBox.deleteAt(index);
      }
    }

    return _networkSandBox.add(config);
    // return _networkSandBox.putAt(index, config);
  }

  List<NetworkConfig> networkConfiglist() {
    return _instance._networkSandBox.values.map((e) => e).toList();
  }

  Future<void> deleteNetwork(String identifier) {
    final int index = _networkSandBox.values.toList().indexWhere((config) =>
        config.identifier.toLowerCase() == identifier.toLowerCase());

    if (index < 0) {
      throw NetworkUtilsError.ConfigNotExist;
    }

    return _networkSandBox.deleteAt(index);
  }

  Future<NetworkConfig> removAtNetwork(int index) async {
    await _instance._networkSandBox.deleteAt(index);
  }

  Future<void> insertAtNetwork(int index, NetworkConfig value) async {
    return _networkSandBox.add(value);
  }

  NetworkConfig get defaultConfig => _networkSandBox.getAt(0);

  NetworkConfig networkConfigOf(String identifier) {
    final int index = _networkSandBox.values.toList().indexWhere((config) =>
        config.identifier.toLowerCase() == identifier.toLowerCase());
    if (index < 0) {
      throw NetworkUtilsError.ConfigNotExist;
    }

    return _networkSandBox.getAt(index);
  }

  Future<void> addEndPoint(NetworkConfig networkConfig, EndPoint endPoint) {
    final int index = _networkSandBox.values.toList().indexWhere((config) =>
        config.identifier.toLowerCase() ==
        networkConfig.identifier.toLowerCase());
    final int Enpointindex = networkConfig.endpoints
        .toList()
        .indexWhere((config) => config.nodeURL == endPoint.nodeURL);
    if (index < 0) {
      throw NetworkUtilsError.ConfigNotExist;
    }
    if (Enpointindex > 0) {
      return null;
    }
    networkConfig.endpoints.add(endPoint);
    return _networkSandBox.putAt(index, networkConfig);

    // print(_networkSandBox.values.map((e) => print(e.endpoints)));
  }

  NetworkConfig selectEndpointIndexof(int selectEndpointIndex) {
    final int index = _networkSandBox.values.toList().indexWhere(
        (element) => element.selectEndpointIndex == selectEndpointIndex);

    if (index < 0) {
      throw NetworkUtilsError.ConfigNotExist;
    }

    return _networkSandBox.getAt(index);
  }
}
