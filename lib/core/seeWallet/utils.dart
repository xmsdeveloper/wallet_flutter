// @dart=2.9
part of '../core.dart';

enum SeeWalletStorageUtilsS {
  ConfigAlreadyExist,
  ConfigNotExist,
}

class SeeWalletStorageUtils {
  static SeeWalletStorageUtils _instance;

  Box<WalletKey> _seeWalletSandBox;

  SeeWalletStorageUtils._();

  factory SeeWalletStorageUtils() {
    assert(
      _instance != null,
      '\nEnsure to initialize _NetworkUtils before accessing it.\nPlease execute the init method : _NetworkUtils.init()',
    );

    return _instance;
  }

  static Future<void> init() async {
    /// !! 此写法不可用，因为TypeAdapter<T>会变为<dynamic>所有类型都会满足 obj.type == dynamic
    /// 导致找不到对应的适配器（Adapter）
    /// <TypeAdapter>[
    ///   NetworkConfigAdapter(),
    ///   EndPointAdapter(),
    /// ].forEach(Hive.registerAdapter);
    // Hive.registerAdapter(NetworkConfigAdapter());
    // Hive.registerAdapter(EndPointAdapter());

    _instance ??= SeeWalletStorageUtils._();
    _instance._seeWalletSandBox =
        await Hive.openBox<WalletKey>('SeeWalletSandBoxs');

    // await _instance._networkSandBox.clear();

    // networkConfig.forEach((configJsonMap) {
    //   final config = NetworkConfig.fromMap(configJsonMap);

    //   final existIndex =
    //       _instance._networkSandBox.values.toList().indexOf(config);
    //   // print(existIndex);
    //   var i = 0;
    //   if (existIndex >= 0) {
    //     _instance._networkSandBox.putAt(existIndex, config);
    //   } else {
    //     _instance._networkSandBox.add(config);
    //   }
    // });
  }

  Future<void> seeWalletInsert(WalletKey key) async {
    final existIndex = _instance._seeWalletSandBox.values
        .toList()
        .indexWhere((element) => element.address == key.address);

    // _instance._networkSandBox.add(config);
    if (existIndex >= 0) {
    } else {
      _instance._seeWalletSandBox.add(key);
    }
  }

  Future<void> deleteSeeWallet(String address) {
    final int index = _seeWalletSandBox.values.toList().indexWhere(
        (config) => config.address.toLowerCase() == address.toLowerCase());

    print(index);
    if (index < 0) {
      throw NetworkUtilsError.ConfigNotExist;
    }

    return _instance._seeWalletSandBox.deleteAt(index);
  }

  WalletKey seeWalletConfigOf(String address) {
    final int index = _seeWalletSandBox.values.toList().indexWhere(
        (config) => config.address.toLowerCase() == address.toLowerCase());
    if (index < 0) {
      throw NetworkUtilsError.ConfigNotExist;
    }

    return _seeWalletSandBox.getAt(index);
  }

  List<String> get seeWalletaddress =>
      _instance._seeWalletSandBox.values.map((e) => e.address).toList();

  // WalletKey seewalletOf(String adress) {
  //   final int index = _seeWalletSandBox.values.toList().indexWhere(
  //       (config) => config.address.toLowerCase() == adress.toLowerCase());
  //   if (index < 0) {
  //     throw NetworkUtilsError.ConfigNotExist;
  //   }

  //   return _seeWalletSandBox.getAt(index);
  // }
  Future<List<WalletKey>> seeWalletlist() async {
    return _instance._seeWalletSandBox.values.map((e) => e).toList();
  }
}
