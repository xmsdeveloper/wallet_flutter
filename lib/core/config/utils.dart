// @dart=2.9
part of '../core.dart';

enum ConfigKey {
  SelectedWalletIdentifiere,
  SelectedKeyIndex,
  SelectedNetworkIdentifier,
  SelectedNetworknodeurl,
  Selecteaccount,

  /// 手续费推荐的默认模式 Safe,Propose,Fast
  DefaultSelectedFeeMode,

  /// 语言编码
  LanguageCode,

  IsAuth,
  //节点索引
  selectEndpointIndexList,

  IsPassWord,
  //dapp弹窗提醒
  isDappTips,

  //第一次使用
  IsFristUse,

  //第一次使用
  DisplayPassword,

//是否提示更新
  UpdatePrompt,

  //总金额
  TotalAmount,
  //手势
  Gesture
}

typedef WalletChangedBuilder = Widget Function(
  BuildContext context,
  WalletKey key,
);

class ConfigStorageUtils {
  static ConfigStorageUtils _instance;

  Box _configSandBox;

  ConfigStorageUtils._();

  factory ConfigStorageUtils() {
    assert(
      _instance != null,
      '\nEnsure to initialize ConfigStorageUtils before accessing it.\nPlease execute the init method : ConfigStorageUtils.init()',
    );

    return _instance;
  }

  static Future<void> init() async {
    _instance ??= ConfigStorageUtils._();
    _instance._configSandBox = await Hive.openBox('ConfigSandBox');
  }

  T read<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: defaultValue) as T;

  Future<void> write<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  T readnode<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: defaultValue) as T;

  Future<void> writenode<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  T readOfKey<T>(String key, {T defaultValue}) =>
      _configSandBox.get(key, defaultValue: defaultValue) as T;

  Future<void> writeOfKey<T>(String key, {T defaultValue}) =>
      _configSandBox.put(key, defaultValue);

  Future<void> deleteOfKey<T>(String key, {T defaultValue}) =>
      _configSandBox.delete(key);
  T readAuthOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: false) as T;

  Future<void> writeAuthOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  T readSelectEnpointIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: false) as T;

  Future<void> writeSelectEnpointIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

//是否需要输入密码
  T readIsPassWordIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: true) as T;

  Future<void> writeIsPassWordIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  //是否弹dapp出提示框
  T readIsDappTipsIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: true) as T;

  Future<void> writeIsDappTipsIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  //是否第一次使用
  T readIsFristUseIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: true) as T;
  Future<void> writeIsFristUseIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  //是否隐藏密码
  T readDisplayPasswordIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: false) as T;

  Future<void> writeDisplayPasswordIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  //是否提示更新
  T readUpdatePromptIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: false) as T;

  Future<void> writeUpdatePromptIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  //存储总金额
  T readTotalAmountIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: false) as T;

  Future<void> writeTotalAmountIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);

  //手势解锁是否开启
  T readGestureIndexOfKey<T>(ConfigKey key, {T defaultValue}) =>
      _configSandBox.get(key.index, defaultValue: false) as T;

  Future<void> writeGestureIndexOfKey<T>(ConfigKey key, T value) =>
      _configSandBox.put(key.index, value);
}

void noties() {
  ConfigStorageUtils()
      .readOfKey<DateTime>("NotifactionLastReadTime", defaultValue: null);

  ConfigStorageUtils()
      .writeOfKey("NotifactionLastReadTime", defaultValue: DateTime.now());
}
