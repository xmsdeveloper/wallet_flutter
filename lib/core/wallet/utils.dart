// @dart=2.9

part of '../core.dart';

enum WalletStorageError {
  ErrorWalletIdentifierAlreadyExist,
  ErrorWalletNotExist,
  ErrorMnemonicFormatWrong,
  ErrorSecurityCodeError,
  ErrorKeyNotExist,
  ErrorDecryptKeystore,
}

class _WalletStorageUtils {
  static _WalletStorageUtils _instance;

  Box<Wallet> _walletsSandBox;

  _WalletStorageUtils._();

  factory _WalletStorageUtils() {
    assert(
      _instance != null,
      '\nEnsure to initialize WalletStorageUtils before accessing it.\nPlease execute the init method : WalletStorageUtils.init()',
    );

    return _instance;
  }

  static Future<void> init() async {
    /// !! 此写法不可用，因为TypeAdapter<T>会变为<dynamic>所有类型都会满足 obj.type == dynamic
    /// 导致找不到对应的适配器（Adapter）
    /// <TypeAdapter>[
    ///   WalletAdapter(),
    ///   WalletKeyAdapter(),
    /// ].forEach(Hive.registerAdapter);
    Hive.registerAdapter(WalletAdapter());
    Hive.registerAdapter(WalletKeyAdapter());

    _instance ??= _WalletStorageUtils._();
    _instance._walletsSandBox = await Hive.openBox<Wallet>('WalletSandBox');
  }

  List<String> get identifiers =>
      _walletsSandBox.values.map((e) => e.identifier).toList();

  bool containsIdentifier(String identifier) =>
      identifiers.contains(identifier);

  Future<Wallet> generateRootKeypair(String name, String security) async {
    final String newIdentifier = bytesToHex(
        sha256.convert(utf8.encode(_walletsSandBox.length.toString())).bytes);

    if (_walletsSandBox.containsKey(newIdentifier)) {
      throw WalletStorageError.ErrorWalletIdentifierAlreadyExist;
    }

    await HDWalletStateMachine.createWallet(strength: 128);

    final newWallet = Wallet(
      newIdentifier,
      name,
      await HDWalletStateMachine.getMnemonic(),
      sha256.convert(utf8.encode(security)).bytes,
    );

    return _walletsSandBox.add(newWallet).then((_) => newWallet);
  }

  Future<Wallet> importRootKeypair(
    String name,
    String mnemonic,
    String security,
  ) async {
    final wordCount = mnemonic.trim().split(' ').toList().length;
    if (wordCount != 12 && wordCount != 16) {
      throw WalletStorageError.ErrorMnemonicFormatWrong;
    }

    final String identifier = bytesToHex(
        sha256.convert(utf8.encode(_walletsSandBox.length.toString())).bytes);

    if (_walletsSandBox.containsKey(identifier)) {
      throw WalletStorageError.ErrorWalletIdentifierAlreadyExist;
    }

    final wallet = Wallet(
      identifier,
      name,
      mnemonic,
      sha256.convert(utf8.encode(security)).bytes,
    );

    _walletsSandBox.add(wallet);

    return wallet;
  }

  Future<String> exportRootKeypairMnemonic(
    String identifieir,
    String security,
  ) async {
    if (!_walletsSandBox.containsKey(identifieir)) {
      throw WalletStorageError.ErrorWalletNotExist;
    }

    final wallet = _walletsSandBox.get(identifieir);
    if (sha256.convert(utf8.encode(security)).bytes != wallet.securitySHA256) {
      throw WalletStorageError.ErrorSecurityCodeError;
    }

    return Future.value(wallet.mnemonic);
  }

  Wallet walletOf(String identifier, [bool any = false]) {
    /// any = false，如果指定的identifier找不到对应的钱包，则返回false
    if (!any) {
      return identifier == null
          ? null
          : _walletsSandBox.values.firstWhere(
              (wallet) => wallet.identifier == identifier,
              orElse: () => null,
            );
    }

    /// any = true, 若指定的identifierr找不到对应的钱包，并且当前存储中有其他的有效钱包，则返回其中一个，否则返回null
    else {
      return _walletsSandBox.values.firstWhere(
        (wallet) => wallet.identifier == identifier,
        orElse: () => _walletsSandBox.values.length > 0
            ? _walletsSandBox.values.first
            : null,
      );
    }
  }
}
