// @dart=2.9

import 'package:hive/hive.dart';

part 'network_endpoint.g.dart';

@HiveType(typeId: 3)
class EndPoint extends HiveObject {
  /// 通讯节点API地址和端口号，预计支持http,https,ws,wss
  @HiveField(0)
  String nodeURL;

  /// 业务逻辑实现服务器API地址
  @HiveField(1)
  String resourcesServicesURL;

  /// 区块浏览器API地址
  @HiveField(2)
  String scanURL;

  /// 区块浏览器的APIKey（选填）
  @HiveField(3)
  String scanAPIKey;

  EndPoint(
    this.nodeURL,
    this.resourcesServicesURL,
    this.scanURL,
    this.scanAPIKey,
  );

  factory EndPoint.fromMap(Map jsonmap) => EndPoint(
        jsonmap['nodeURL'],
        jsonmap['resourcesServicesURL'],
        jsonmap['scanURL'],
        jsonmap['scanAPIKey'],
      );
}
