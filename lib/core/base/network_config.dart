// @dart=2.9

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';

import 'package:wallet_flutter/core/base/network_endpoint.dart';
import 'package:wallet_flutter/services/entity/_token_blog.dart';

part 'network_config.g.dart';

class FeeTokenBlog {
  final String symbol;

  final String name;

  final int decimals;

  const FeeTokenBlog({
    this.symbol,
    this.name,
    this.decimals,
  });
}

/// 所有数值记录链上的通讯单位
class GasConfig {
  final BigInt safe;
  final BigInt propose;
  final BigInt fast;

  const GasConfig({
    @required this.safe,
    @required this.propose,
    @required this.fast,
  });

  factory GasConfig.fromMap(Map jsonmap) => GasConfig(
        // safe: BigInt.from(double.parse(jsonmap['safe']).toInt()),
        // propose: BigInt.from(double.parse(jsonmap['propose']).toInt()),
        // fast: BigInt.from(double.parse(jsonmap['fast']).toInt()),

        safe: BigInt.from(double.parse(jsonmap['safe'])),
        propose: BigInt.from(double.parse(jsonmap['propose'])),
        fast: BigInt.from(double.parse(jsonmap['fast'])),
      );
}

enum FeeEstimatedMode {
  /// 未知类型
  Unsupport,

  /// 区块浏览器
  Scan,

  /// 源服务器
  DataProvider,

  /// 默认配置
  Default,

  /// 节点
  Noder,
}

FeeEstimatedMode feeEstimatedModeFromString(String s) {
  switch (s.trim().toLowerCase()) {
    case 'scan':
      return FeeEstimatedMode.Scan;
    case 'dataprovider':
      return FeeEstimatedMode.DataProvider;
    case 'default':
      return FeeEstimatedMode.Default;
    case 'node':
      return FeeEstimatedMode.Noder;
    default:
      return FeeEstimatedMode.Unsupport;
  }
}

@HiveType(typeId: 2)
class NetworkConfig extends HiveObject {
  /// 配置标识符,
  @HiveField(0)
  final String identifier;

  /// 名称，用于显示，即代表这个网络配置的可显示的名字
  @HiveField(1)
  String markName;

  /// 链名称
  @HiveField(2)
  String chainName;

  /// 网络类型，目前仅支持 'ethereum' : 60,公链底层技术名
  @HiveField(3)
  int _chainCore;

  /// 链上手续费符号，如ETH，TRX
  @HiveField(4)
  String mainSymbol;

  /// 主精度,如 ETH：对应18，TRX对应6
  @HiveField(5)
  int mainDecimals;

  @HiveField(6)
  String mainCoinName;

  /// Logo
  @HiveField(7)
  String logoURL;

  @HiveField(8)
  String coinLogoURL;

  @HiveField(9)
  List<EndPoint> endpoints;

  @HiveField(10)
  int _selectEndpointIndex;

  @HiveField(11)
  String networkID;

  @HiveField(12)
  final Map<String, dynamic> _defaultGasOracle;
  GasConfig get defaultGasOracle => GasConfig.fromMap(_defaultGasOracle);

  @HiveField(13)
  final String _feeEstimatedMode;

  @HiveField(14)
  final List<String> modules;
  @HiveField(15)
  final String source;

  List<FeeEstimatedMode> get feeEstimatedMode {
    print(_feeEstimatedMode);

    final list =
        _feeEstimatedMode.split('>').map(feeEstimatedModeFromString).toList();

    /// 如果中间配置了任意一个非标准的字段，则使用默认的执行顺序
    /// 即先尝试区块浏览器，在尝试数据服务器，在尝试使用默认字段
    /// 最后使用节点
    return list.contains(FeeEstimatedMode.Unsupport)
        ? [
            FeeEstimatedMode.Scan,
            FeeEstimatedMode.DataProvider,
            FeeEstimatedMode.Default,
            FeeEstimatedMode.Noder,
          ]
        : list;
  }

  VoidCallback endPointChangedCallBack;
  int get selectEndpointIndex => _selectEndpointIndex;
  set selectEndpointIndex(int value) {
    if (_selectEndpointIndex == value) {
      return;
    }
    _selectEndpointIndex = value;
    this.save().then((value) {
      if (endPointChangedCallBack != null) {
        endPointChangedCallBack();
      }
    });
  }

  EndPoint get currenEndPoint => selectEndpointIndex == null ||
          selectEndpointIndex < 0 ||
          selectEndpointIndex >= endpoints.length
      ? endpoints.first
      : endpoints[selectEndpointIndex];

  NetworkConfig(
    this.identifier,
    this.markName,
    this.chainName,
    this._chainCore,
    this.mainSymbol,
    this.mainDecimals,
    this.mainCoinName,
    this.logoURL,
    this.coinLogoURL,
    this._selectEndpointIndex,
    this.endpoints,
    this.networkID,
    this._defaultGasOracle,
    this._feeEstimatedMode,
    this.modules,
    this.source,
  );

  FeeTokenBlog get feeTokenBlog => FeeTokenBlog(
        decimals: mainDecimals,
        name: mainCoinName,
        symbol: mainSymbol,
      );
  TokenBlog get coinBlog => TokenBlog(
        symbol: mainSymbol,
        address: '',
        name: mainCoinName,
        decimals: mainDecimals,
        logo: logoURL,
      );
  ChainCore get chainCore => ChainCore.fromCoinID(_chainCore);

  factory NetworkConfig.fromMap(Map jsonmap) => NetworkConfig(
        jsonmap['identifier'] as String,
        jsonmap['markName'] as String,
        jsonmap['chainName'] as String,
        jsonmap['chainCore'] as int,
        jsonmap['mainSymbol'] as String,
        jsonmap['mainDecimals'] as int,
        jsonmap['mainCoinName'] as String,
        jsonmap['logoURL'] as String,
        jsonmap['coinLogoURL'] as String,
        jsonmap['selectEndpointIndex'] as int,
        (jsonmap['endpoints'] as List).map((e) => EndPoint.fromMap(e)).toList(),
        jsonmap['networkID'] as String,
        jsonmap['defaultGasOracle'],
        jsonmap['feeEstimatedMode'] as String,
        (jsonmap['modules'] as List).cast<String>(),
        jsonmap['source'] as String,
      );

  @override
  int get hashCode => 2;

  @override
  operator ==(dynamic other) =>
      other is NetworkConfig && identifier == other.identifier;
}
