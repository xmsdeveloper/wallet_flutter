// @dart=2.9

import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:decimal/decimal.dart';
part 'amount.g.dart';

@HiveType(typeId: 0)
class Amount extends HiveObject {
  @HiveField(0)
  BigInt value;

  @HiveField(1)
  int decimals;

  Amount({
    @required this.value,
    @required this.decimals,
  });

  factory Amount.fromString(
    String value, {
    @required int decimals,
    int valueDecimals = 0,
  }) {
    if (value == null || value.length <= 0) {
      return Amount(value: BigInt.zero, decimals: decimals);
    }

    return Amount(
      decimals: decimals,
      value: BigInt.parse(
        Decimal.parse(
          (double.parse(value) *
                  (BigInt.from(10).pow(valueDecimals).toDouble()))
              .toStringAsFixed(0),
        ).toStringAsFixed(0),
      ),
    );
  }

  String toStringAsFixed({int fractionDigits = 4, bool format = true}) {
    if (decimals == 0 && format) {
      return NumberFormat('###,###').format(value.toString());
    } else if (decimals == 0 && !format) {
      return value.toString();
    }

    final doubleValue =
        ((value / BigInt.from(10).pow(decimals - fractionDigits)).toDouble() /
            BigInt.from(10).pow(fractionDigits).toDouble());

    final numberFormat = format
        ? NumberFormat(
            '###,###' + (fractionDigits <= 0 ? '' : '.' + '#' * fractionDigits))
        : NumberFormat(
            '#' + (fractionDigits <= 0 ? '' : '.' + '#' * fractionDigits));

    return numberFormat.format(doubleValue);
  }

  @override
  String toString() => '('
      'value=$value, '
      'decimals=$decimals'
      ')';
}
