// @dart=2.9

import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';

part 'key.g.dart';

typedef SaveDelegateCallback = void Function(WalletKey key, bool isDeleted);

@HiveType(typeId: 1)
class WalletKey extends HiveObject {
  ValueNotifier<WalletKey> _listenable = ValueNotifier<WalletKey>(null);
  ValueNotifier<WalletKey> get listenable => _listenable;

  @HiveField(1)
  String _nickName;
  String get nickName => _nickName;
  set nickName(String value) {
    _nickName = value;
    _listenable.value = this;
    if (saveCallback != null) {
      saveCallback(this, false);
    }
  }

  @HiveField(2)
  String _avatarPath;
  String get avatarPath => _avatarPath;
  set avatarPath(String value) {
    _avatarPath = value;
    _listenable.value = this;
    if (saveCallback != null) {
      saveCallback(this, false);
    }
  }

  @HiveField(3)
  final int chainType;

  @HiveField(4)
  final String address;

  @HiveField(5)
  final int derivePathAccount;

  @HiveField(6)
  final int derivePathIndex;

  @HiveField(7)
  final Uint8List privateKeyRaw;

  @HiveField(8)
  final bool isHDKeypair;

  @HiveField(9)
  bool _isDeleted;
  bool get isDeleted => _isDeleted;
  set isDeleted(bool value) {
    _isDeleted = value;
    _listenable.value = this;
    if (saveCallback != null) {
      saveCallback(this, false);
    }
  }

  String get addressBlog => address.replaceRange(10, address.length - 8, '...');

  WalletKey(
    this._nickName,
    this.address,
    this._avatarPath,
    this.chainType,
    this.derivePathAccount,
    this.derivePathIndex,
    this.privateKeyRaw,
    this.isHDKeypair,
    this._isDeleted,
  ) {
    _listenable.value = this;
  }

  factory WalletKey.temp(String address) => WalletKey(
        null,
        address,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      );

  Future<String> encryptKeypairToJson({
    @required String passphrase,
  }) =>
      HDWalletStateMachine.encryptPrivateKeyToJson(
        coinType: ChainCore.fromCoinID(this.chainType),
        privateKey: bytesToHex(this.privateKeyRaw),
        passphrase: passphrase,
      );

  SaveDelegateCallback saveCallback;
}
