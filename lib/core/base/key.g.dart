// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of 'key.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WalletKeyAdapter extends TypeAdapter<WalletKey> {
  @override
  final int typeId = 1;

  @override
  WalletKey read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WalletKey(
      fields[1] as String,
      fields[4] as String,
      fields[2] as String,
      fields[3] as int,
      fields[5] as int,
      fields[6] as int,
      fields[7] as Uint8List,
      fields[8] as bool,
      fields[9] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, WalletKey obj) {
    writer
      ..writeByte(9)
      ..writeByte(1)
      ..write(obj._nickName)
      ..writeByte(2)
      ..write(obj._avatarPath)
      ..writeByte(3)
      ..write(obj.chainType)
      ..writeByte(4)
      ..write(obj.address)
      ..writeByte(5)
      ..write(obj.derivePathAccount)
      ..writeByte(6)
      ..write(obj.derivePathIndex)
      ..writeByte(7)
      ..write(obj.privateKeyRaw)
      ..writeByte(8)
      ..write(obj.isHDKeypair)
      ..writeByte(9)
      ..write(obj._isDeleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WalletKeyAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
