// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of 'dapp.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DappDetailAdapter extends TypeAdapter<DappDetail> {
  @override
  final int typeId = 33;

  @override
  DappDetail read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DappDetail(
      name: fields[0] as String,
      url: fields[1] as String,
      des: fields[2] as String,
      logo: fields[3] as String,
      isSelect: fields[4] as bool,
      type: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, DappDetail obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.url)
      ..writeByte(2)
      ..write(obj.des)
      ..writeByte(3)
      ..write(obj.logo)
      ..writeByte(4)
      ..write(obj.isSelect)
      ..writeByte(5)
      ..write(obj.type);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DappDetailAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
