// @dart=2.9

import 'dart:convert';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:wallet_core_flutter/wallet_core_flutter.dart';
import 'package:wallet_flutter/core/core.dart';

import 'key.dart';

part 'wallet.g.dart';

typedef StoragedCallBack = void Function(Wallet wallet, [bool isDelete]);

@HiveType(typeId: 4)
class Wallet extends HiveObject {
  @HiveField(0)
  final String identifier;

  @HiveField(1)
  final String mnemonic;

  @HiveField(2)
  Uint8List securitySHA256;

  @HiveField(3)
  String walletName;

  @HiveField(4)
  Map<int, List<WalletKey>> _keysMapping;

  Wallet(
    this.identifier,
    this.walletName,
    this.mnemonic,
    this.securitySHA256, [
    this._keysMapping,
  ]) {
    if (this._keysMapping == null) {
      this._keysMapping = <int, List<WalletKey>>{};
    }
  }

  StoragedCallBack storagedCallBack;

  /// 如果尝试请求list的列表中没有任何地址，则自动创建一个初始地址
  List<WalletKey> keysOf(ChainCore type) =>
      (_keysMapping.containsKey(type.coinID)
          ? _keysMapping[type.coinID].where((key) => !key.isDeleted).toList()
          : [])
        ..forEach(
          (key) {
            key.saveCallback = saveDelegateCallbackImpl;
          },
        );

  Future<WalletKey> importKeypairFromPrivateKey({
    @required ChainCore type,
    @required String privateKey,
    String walletName,
  }) async {
    final address = await HDWalletStateMachine.getAddressForPrivateKey(
      coinType: type,
      privateKey: privateKey,
    );

    WalletKey key;

    /// 若导入的账号是之前已经删除的则恢复状态
    if (_keysMapping[type.coinID].length > 0) {
      key = _keysMapping[type.coinID].firstWhere(
        (element) => element.address.toLowerCase() == address.toLowerCase(),
        orElse: () => null,
      );
    }

    if (key != null) {
      if (!key.isDeleted) {
        throw WalletStorageError.ErrorWalletIdentifierAlreadyExist;
      } else {
        key.isDeleted = false;
        key.nickName = walletName;
        _keysMapping[type.coinID].remove(key);
        _keysMapping[type.coinID].add(key);
        key.saveCallback = saveDelegateCallbackImpl;
      }
    } else {
      final count = _keysMapping[type.coinID].length;
      key = WalletKey(
        walletName ?? 'Imported-$count',
        address,
        'assets/imgs/user_icon_${count % 12}.png',
        type.coinID,
        0,
        0,
        hexToBytes(privateKey),
        false,
        false,
      );
      key.saveCallback = saveDelegateCallbackImpl;
      _keysMapping[type.coinID].add(key);
    }

    return this.save().then((_) {
      if (storagedCallBack != null) {
        storagedCallBack(this);
      }
      return key;
    });
  }

  Future<WalletKey> importKeypairFromJson({
    @required ChainCore type,
    @required String json,
    @required String passphrase,
    String walletName,
  }) async {
    var privateKey;

    try {
      privateKey = await HDWalletStateMachine.decryptPrivateKeyFromJson(
        json: json,
        passphrase: passphrase,
      );
    } catch (e) {
      throw WalletStorageError.ErrorDecryptKeystore;
    }

    return importKeypairFromPrivateKey(
      type: type,
      privateKey: privateKey,
      walletName: walletName,
    );
  }

  Future<String> encryptKeypairToJson({
    @required ChainCore type,
    @required WalletKey key,
    @required String passphrase,
  }) =>
      HDWalletStateMachine.encryptPrivateKeyToJson(
        coinType: type,
        privateKey: bytesToHex(key.privateKeyRaw),
        passphrase: passphrase,
      );

  Future<WalletKey> newKeypair({
    @required ChainCore type,
    int accountGroup = 0,
    int num = 0,
    String accountName = "Account-",
  }) async {
    final addressIndex = _keysMapping[type.coinID] == null
        ? 0
        : _keysMapping[type.coinID].length + num;

    // print("addressIndex:" + addressIndex.toString());

    final privateKeyHexString =
        await HDWalletStateMachine.getPrivateKeyForBIP44(
      chainCore: type,
      accountIndex: accountGroup,
      changeIndex: 0,
      addressIndex: addressIndex,
    );
    final address = await HDWalletStateMachine.getAddressForBIP44(
      chainCore: type,
      accountIndex: accountGroup,
      changeIndex: 0,
      addressIndex: addressIndex,
    );

    if (accountName == '') {
      accountName = "Account-";
    }

    final key = WalletKey(
      '$accountName' + '${addressIndex + 1}',
      address,
      'assets/imgs/user_icon_${addressIndex % 12}.png',
      type.coinID,
      accountGroup,
      addressIndex,
      hexToBytes(privateKeyHexString),
      true,
      false,
    );

    key.saveCallback = saveDelegateCallbackImpl;
    addressIndex == 0
        ? _keysMapping[type.coinID] = [key]
        : _keysMapping[type.coinID].add(key);

    return this.save().then((_) {
      if (storagedCallBack != null) {
        storagedCallBack(this);
      }
      return key;
    });
  }

  bool verifySecurity(String security) =>
      bytesToHex(sha256.convert(utf8.encode(security)).bytes) ==
      bytesToHex(securitySHA256);

  void saveDelegateCallbackImpl(WalletKey key, bool isDelete) {
    this.save().then((_) {
      if (storagedCallBack != null) {
        storagedCallBack(this, isDelete);
      }
      return key;
    });
  }
}
