// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.9

part of 'network_config.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NetworkConfigAdapter extends TypeAdapter<NetworkConfig> {
  @override
  final int typeId = 2;

  @override
  NetworkConfig read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return NetworkConfig(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
      fields[3] as int,
      fields[4] as String,
      fields[5] as int,
      fields[6] as String,
      fields[7] as String,
      fields[8] as String,
      fields[10] as int,
      (fields[9] as List)?.cast<EndPoint>(),
      fields[11] as String,
      (fields[12] as Map)?.cast<String, dynamic>(),
      fields[13] as String,
      (fields[14] as List)?.cast<String>(),
      fields[15] as String,
    );
  }

  @override
  void write(BinaryWriter writer, NetworkConfig obj) {
    writer
      ..writeByte(16)
      ..writeByte(0)
      ..write(obj.identifier)
      ..writeByte(1)
      ..write(obj.markName)
      ..writeByte(2)
      ..write(obj.chainName)
      ..writeByte(3)
      ..write(obj._chainCore)
      ..writeByte(4)
      ..write(obj.mainSymbol)
      ..writeByte(5)
      ..write(obj.mainDecimals)
      ..writeByte(6)
      ..write(obj.mainCoinName)
      ..writeByte(7)
      ..write(obj.logoURL)
      ..writeByte(8)
      ..write(obj.coinLogoURL)
      ..writeByte(9)
      ..write(obj.endpoints)
      ..writeByte(10)
      ..write(obj._selectEndpointIndex)
      ..writeByte(11)
      ..write(obj.networkID)
      ..writeByte(12)
      ..write(obj._defaultGasOracle)
      ..writeByte(13)
      ..write(obj._feeEstimatedMode)
      ..writeByte(14)
      ..write(obj.modules)
      ..writeByte(15)
      ..write(obj.source);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NetworkConfigAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
