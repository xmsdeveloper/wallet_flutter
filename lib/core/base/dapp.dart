// @dart=2.9

import 'package:hive/hive.dart';
import 'package:wallet_flutter/services/entity/_json_serialize.dart';
part 'dapp.g.dart';

@HiveType(typeId: 33)
class DappDetail implements JsonSerialize {
  @HiveField(0)
  String name;
  @HiveField(1)
  String url;
  @HiveField(2)
  String des;
  @HiveField(3)
  String logo;
  @HiveField(4)
  bool isSelect;
  @HiveField(5)
  String type;

  DappDetail({
    this.name,
    this.url,
    this.des,
    this.logo,
    this.isSelect = false,
    this.type,
  });

  void fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
    des = json['des'];
    logo = json['logo'];
    isSelect = json['isSelect'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['url'] = this.url;
    data['des'] = this.des;
    data['logo'] = this.logo;
    data['isSelect'] = this.isSelect;
    data['type'] = this.type;
    return data;
  }
}
